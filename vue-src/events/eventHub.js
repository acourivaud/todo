/*
 * You can use this event hub ONLY for the same entry file .js !
 * If you want to emit events between 2 entry/output files use window.bus = new Vue();
 */
import Vue from 'vue';

const eventHub = new Vue();
export default eventHub;