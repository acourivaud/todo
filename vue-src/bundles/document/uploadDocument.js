// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import VueFlatpickr from 'vue-flatpickr';
import 'vue-flatpickr/theme/material_blue.css';
import uploadDocument from './components/uploadDocument.vue';

Vue.use(VueFlatpickr);

if(document.getElementById("uploadDocument")){
  /* eslint-disable no-new */
  new Vue({
    el: '#uploadDocument',
    data() {
      return {
        token: null,
      };
    },

    template: '<uploadDocument :token="token" />',
    components: { uploadDocument },
    beforeMount() {
      this.token = this.$el.attributes.token.value;
    },
  });
}
