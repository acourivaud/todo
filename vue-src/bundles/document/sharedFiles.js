// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import sharedFiles from './components/sharedFiles.vue';

if(document.getElementById("sharedFiles")){
  /* eslint-disable no-new */
  new Vue({
    el: '#sharedFiles',
    data() {
      return {
        token: null,
      };
    },

    template: '<sharedFiles :token="token" />',
    components: { sharedFiles },
    beforeMount() {
      this.token = this.$el.attributes.token.value;
    },
  });
}
