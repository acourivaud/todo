import Vue from 'vue'
import catalogClearCache from './catalogCache.vue'

new Vue({
  el: '#app',
  components: {catalogClearCache}
})