import adherentMigration from './adherentMigration.vue'
import Vue from 'vue'
import nl2br from '../../filters/nl2br'

Vue.filter('nl2br', nl2br)

new Vue({
  el: '#app',
  components: {adherentMigration}
})