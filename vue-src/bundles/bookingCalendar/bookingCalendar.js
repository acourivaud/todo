import Vue from 'vue';
import JWTManager from './../../manager/JWTManager';
import App from './AppBookingCalendar.vue';
import moment from './../../../node_modules/moment';

/* eslint-disable no-new */
new Vue({
    el: '#bookingCalendar',
    template: '<App/>',
    components: {App},
    created: function() {
        moment.locale(JWTManager.locale);
        JWTManager.setApiManagerWithToken();
    }
});