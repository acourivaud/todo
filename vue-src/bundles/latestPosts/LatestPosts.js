import Vue from "vue";
import latestPosts from "./LatestPosts.vue";

/* eslint-disable no-new */
new Vue({
    el: '#latest-posts',
    template: '<latestPosts :token="token"/>',
    data: function () {
        return {
            token: null
        }
    },
    components: {latestPosts},
    beforeMount: function () {
        //console.log(this.$el.attributes);
        if (this.$el.attributes['data-token']) {
            this.token = this.$el.attributes['data-token'].value;
        }
    }
});