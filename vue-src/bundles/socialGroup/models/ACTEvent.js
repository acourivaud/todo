export default class ACTEvent{

    //todo: change default value !
    constructor( id, name, description, picture, numberParticipants, numberNotifications, startDate, endDate, location, capacity, emails) {
        this.id                  = parseInt( id, 10 );
        this.name                = name;
        this.description         = description;
        this.picture             = picture;
        this.numberParticipants  = numberParticipants || 0;
        this.numberNotifications = numberNotifications || 0;
        this.startDate           = startDate;
        this.endDate             = endDate;
        this.location            = location || '';
        this.capacity            = capacity || 0;
        this.emails              = emails || [];

        this.type                = 'event';
    }

    getRangeHours() {
        //todo: retourner '14h30 - 16h30' si c'est la même journée.
        //todo: s'occuper de l'affichage en fonction de la langue !
    }
}