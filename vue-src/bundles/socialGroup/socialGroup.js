import Vue from 'vue';
import Multiselect from 'vue-multiselect';
import VueLazyload from 'vue-lazyload'
import viewGroup from './components/socialGroup.vue';

Vue.component(Multiselect);
Vue.use(VueLazyload, {
  lazyComponent: true
});

/* eslint-disable no-new */
new Vue({
  el: '#social-group',
  template: '<viewGroup :token="token" />',
  data() {
    return {
      token: null,
    };
  },
  components: { viewGroup },
  beforeMount() {
    this.token = this.$el.attributes.token.value;
  },
});
