// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import VueFlatpickr from 'vue-flatpickr';
import 'vue-flatpickr/theme/material_blue.css';
import viewGroup from './components/viewGroup.vue';

Vue.use(VueFlatpickr);

/* eslint-disable no-new */
new Vue({
  el: '#viewGroup',
  data() {
    return {
      group_id: null,
      token: null,
    };
  },

  template: '<viewGroup :groupId="group_id" :token="token" />',
  components: { viewGroup },
  beforeMount() {
    this.token = this.$el.attributes.token.value;
    this.group_id = this.$el.attributes.group.value;
  },
});

