import Vue from "vue";
import postsToValidate from "./PostsToValidate.vue";

/* eslint-disable no-new */
new Vue({
    el: '#posts-to-validate',
    template: '<postsToValidate :token="token"/>',
    data: function () {
        return {
            token: null
        }
    },
    components: {postsToValidate},
    beforeMount: function () {
        //console.log(this.$el.attributes);
        if (this.$el.attributes['data-token']) {
            this.token = this.$el.attributes['data-token'].value;
        }
    }
});