import Vue from 'vue';
import SmartBannerApp from './components/SmartBannerApp.vue'

if(document.getElementById("smart-banner-app")){
  new Vue({
      el: '#smart-banner-app',
      delimiters: ['${', '}'],
      components: {
        'smart-banner-app': SmartBannerApp
      }
  });
}