import Vue from 'vue';
import JWTManager from './../../manager/JWTManager';
import App from './AppCatalog.vue';

/* eslint-disable no-new */
new Vue({
    el: '#catalogApp',
    template: '<App/>',
    components: {App},
    created: function() {
        JWTManager.setApiManagerWithToken();
    }
});