import moment from "./../../../node_modules/moment";

export default class ACTServiceBooking{

    constructor(rawStartDate, rawEndDate, proposalParties, siteId, siteName, dateFormat) {
        this.rawStartDate = rawStartDate;
        this.rawEndDate = rawEndDate;
        this.proposalParties = proposalParties;
        this.siteId = siteId;
        this.siteName = siteName;

        dateFormat = dateFormat || 'YYYY-MM-DD HH:mm:ss';
        this.startDate = moment(moment(rawStartDate), dateFormat);
        this.endDate   = moment(moment(rawEndDate), dateFormat);
    }

    getDatasForApi() {
        return {
            End: this.rawEndDate,
            ProposalParties: this.proposalParties,
            SiteId: this.siteId,
            SiteName: this.siteName,
            Start: this.rawStartDate,
        }
    }
}