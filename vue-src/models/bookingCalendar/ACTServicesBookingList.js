import moment from "./../../../node_modules/moment";
import ServiceBooking from "./ACTServiceBooking";

export default class ACTServicesBookingList {
    constructor() {
        this.servicesBooking = {};
        this.defaultDateFormat = 'YYYY-MM-DD';
    }

    resetServicesBooking() {
        this.servicesBooking = {};
    }

    getDefaultDateFormat() {
        return this.defaultDateFormat;
    }

    isEmpty() {
        for (let key in this.servicesBooking) {
            if(this.servicesBooking.hasOwnProperty(key)){
                return false;
            }
        }
        return true;
    }

    getServiceItemByDateAndIndex(_date, index) {
        let date = moment(_date).format(this.defaultDateFormat),
            value = this.servicesBooking[date];
        if ((typeof value !== 'undefined') && (typeof value[index] !== 'undefined')) {
            return this.servicesBooking[date][index];
        }
        return {empty: true};
    }

    getServicesLengthByDate(date) {
        return (this.servicesBooking.hasOwnProperty(date)) ? this.servicesBooking[date].length : 0;
    }

    setServicesBookingWithDatas(data) {
        this.servicesBooking = {};
        for (let i = 0, length = data.length; i < length; i++) {
            let serviceBooking = data[i],
                serviceBookingDate = moment(serviceBooking['Start']).format(this.defaultDateFormat);

            if (typeof this.servicesBooking[serviceBookingDate] === 'undefined') {
                this.servicesBooking[serviceBookingDate] = [];
            }

            this.servicesBooking[serviceBookingDate].push(
                new ServiceBooking(
                    serviceBooking['Start'],
                    serviceBooking['End'],
                    serviceBooking['ProposalParties'],
                    serviceBooking['SiteId'],
                    serviceBooking['SiteName'],
                )
            );
        }
    }
}