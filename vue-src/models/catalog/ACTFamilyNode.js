import ArrayList from "./../base/ACTArrayList";
import ProductQuantity from "./ACTProductQuantity";

const PRODUCT_KEY = '_products';
const FAMILY_ID_KEY = '_family_id';
const PICTO_URL_KEY = '_picto_url';
const IS_PLUGIN_FAMILY_KEY = '_is_plugin_family';
const PLUGIN_SLUG_KEY = '_plugin_slug';
const PLUGIN_MDI_ICON_KEY = '_plugin_mdi_icon';

export default class ACTFamilyNode {

  constructor(id, name, parentHierarchy, childs, picture, searchNodes, isPlugin, pluginSlug, pluginMdiIcon) {
    this.id = id;
    this.name = name;

    this.picture = picture;
    this.parentHierarchy = parentHierarchy || [];
    this.childsParentHierarchy = this.getChildsParentHierarchyWithParent();

    this.childsNodes = new ArrayList();
    this.products = new ArrayList();

    this.isPlugin = isPlugin || false;
    this.pluginSlug = (isPlugin) ? pluginSlug : null;
    this.pluginMdiIcon = (isPlugin) ? pluginMdiIcon : null;

    this.noPicto = (this.picture === null) && (pluginMdiIcon === null);

    let searchLabel = !this.isPlugin ? 'family' : 'plugin';
    searchNodes.addFamilyOrProductNode(searchLabel, this.getChildsParentHierarchyIds(), this.id, this.name, null, pluginSlug, pluginMdiIcon);

    this.setChildsNodes(childs, searchNodes);
    this.setProducts(childs, searchNodes);
  }

  getDirectFamilies() {
    let childsNodes = this.getChildNodes(),
      directFamilies = new ArrayList();

    for (let i = 0, length = childsNodes.length; i < length; i++) {
      let childNode = childsNodes[i];
      directFamilies.push({
        id: childNode.id,
        name: childNode.name,
        picture: childNode.picture,
        noPicto: childNode.noPicto,
        isPlugin: childNode.isPlugin,
        pluginSlug: childNode.pluginSlug,
        pluginMdiIcon: childNode.pluginMdiIcon,
      });
    }
    return directFamilies.getList();
  }

  getChildNodes() {
    return this.childsNodes.getList();
  }

  getChildFamily(familyId) {
    return this.childsNodes.findItemInList('id', familyId);
  }

  getProducts() {
    return this.products.getList();
  }

  getBreadcrumbList() {
    let breadcrumbLinks = [],
      linkToCategory = [];

    for (let i = 0, length = this.childsParentHierarchy.length; i < length; i++) {

      let parentHierarchy = this.childsParentHierarchy[i];
      linkToCategory.push(parentHierarchy.id);

      breadcrumbLinks.push({
        title: parentHierarchy.name,
        link: JSON.parse(JSON.stringify(linkToCategory))
      });
    }
    return breadcrumbLinks;
  }

  getChildsParentHierarchyIds() {
    let childsParentHierarchyIds = [];
    for (let i = 0, length = this.childsParentHierarchy.length; i < length; i++) {
      childsParentHierarchyIds.push(
        this.childsParentHierarchy[i].id
      );
    }
    return childsParentHierarchyIds;
  }

  //Start from level zero (0)
  getHierarchyLevel() {
    return (this.childsParentHierarchy.length - 1);
  }

  /* private method */
  setChildsNodes(childs, searchNodes) {

    let childsNodes = [];

    for (let key in childs) {

      if (childs.hasOwnProperty(key) && (key.charAt(0) !== '_')) {

        let childNode = new ACTFamilyNode(
          childs[key][FAMILY_ID_KEY],
          key,
          this.childsParentHierarchy,
          childs[key],
          childs[key][PICTO_URL_KEY],
          searchNodes,
          childs[key][IS_PLUGIN_FAMILY_KEY],
          childs[key][PLUGIN_SLUG_KEY],
          childs[key][PLUGIN_MDI_ICON_KEY]
        );
        childsNodes.push(childNode);
      }
    }
    this.childsNodes.setList(childsNodes);
  }

  /* private method */
  setProducts(childs, searchNodes) {

    let childsProducts = [];

    if (childs.hasOwnProperty(PRODUCT_KEY)) {

      let products = childs[PRODUCT_KEY];

      for (let i = 0, length = products.length; i < length; i++) {

        let product = products[i];

        if (product['product_type'] !== 'plugin_catalog') {
          let productQuantity = new ProductQuantity(
            product['item_id'],
            product['name_trans'],
            product['description_trans'],
            product['amount'],
            product['amount_excl_tax'],
            product['taxe'],
            product['picture'],
            product['language_trans'],
            product['bookable'],
            product['animation'],
            product['quotation'],
            product['crm_id_service'],
            product['stock'],
            0
          );

          searchNodes.addFamilyOrProductNode(
            'product',
            this.getChildsParentHierarchyIds(),
            productQuantity.id,
            productQuantity.name,
            productQuantity
          );

          childsProducts.push(productQuantity);
        }
      }
    }
    this.products.setList(childsProducts);
  }

  /* private method */
  getChildsParentHierarchyWithParent() {
    let childsParentHierarchy = JSON.parse(JSON.stringify(this.parentHierarchy));
    childsParentHierarchy.push({
      id: this.id,
      name: this.name
    });

    return childsParentHierarchy;
  }

}
