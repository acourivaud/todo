import ArrayList from './../base/ACTArrayList';

const MIN_CHARACTERS_TO_SEARCH = 3;

export default class ACTCatalogSearchingList {

  constructor() {
    this.nodes = new ArrayList();
    this.plugins = {};
  }

  addFamilyOrProductNode(type, hierarchy, id, name, _product, _pluginSlug, _pluginMdiIcon) {
    let product = _product || null;
    let pluginSlug = _pluginSlug || null;
    let pluginMdiIcon = _pluginMdiIcon || null;

    // To have only one occurrence of a plugin in search dropdown (and not 3 Blablacar)
    if(pluginSlug && !this.insertPlugin(pluginSlug)){
      return;
    }

    this.nodes.push({
      type: type,
      hierarchy: hierarchy,
      id: id,
      name: name,
      product: product,
      pluginSlug: pluginSlug,
      pluginMdiIcon: pluginMdiIcon
    });
  };

  /*
   * On commence par la fin de la liste + on utilise "unshift" pour avoir :
   * les catégories au début de la recherche puis les produits
   */
  search(stringToFind) {
    stringToFind = this.transformString(stringToFind);

    if (stringToFind.length < MIN_CHARACTERS_TO_SEARCH) {
      return [];
    }

    let itemsFoundList = new ArrayList(),
      catalogList = this.nodes.getList();

    for (let i = catalogList.length - 1; i >= 0; i--) {
      let item = catalogList[i],
        itemName = this.transformString(item.name),
        indexOf = itemName.indexOf(stringToFind);

      if (indexOf !== -1) {
        let occurence = {
          firstCharacPosition: indexOf,
          numberCharac: stringToFind.length,
          value: item
        };

        if (item.type === 'family' || item.type === 'plugin') {
          itemsFoundList.unshift(occurence);
        } else if (item.type === 'product') {
          itemsFoundList.push(occurence);
        }
      }
    }

    return itemsFoundList.getList();
  }

  /**
   * Insert pluginSlug is not already exist yet
   * Return true if insert was a success
   *
   * @param pluginSlug
   * @returns {boolean}
   */
  insertPlugin(pluginSlug){
    if(!this.plugins.hasOwnProperty(pluginSlug)){
      this.plugins[pluginSlug] = 'exist';
      return true
    }
    return false;
  }

  /**
   *
   * @param string
   * @returns {string}
   */
  transformString(string) {
    return string
      .replace(/[ÀÁÂÃÄÅàáâãäå]/g, "a")
      .replace(/[ÈÉÊËéèêë]/g, "e")
      .replace(/[Îî]/g, "i")
      .replace(/[Ôô]/g, "o")
      .replace(/[ÙÛùû]/g, "u")
      .replace(/[Çç]/g, "c")
      .toLowerCase();
  }
}
