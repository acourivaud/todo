import Product from "./ACTProduct";

export default class ACTProductWithQuantity extends Product{

    constructor(id, name, description, price, taxe, picture, lang, bookable, animation, quotation,
                serviceId, stock, quantity)
    {
        super(id, name, description, price, taxe, picture, lang, bookable, animation, quotation, serviceId);
        this.stock    = stock    || 0;
        this.quantity = quantity || 0;
    }
}