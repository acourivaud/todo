import CatalogSearchingList from './ACTCatalogSearchingList';
import FamilyNode from "./ACTFamilyNode";

const MAIN_FAMILY_NAME = 'main';
const CATALOG_ROUTE = Routing.generate('todotoday_catalog_index');

export default class ACTFamiliesAndProductsTree {

    constructor() {
        this.rootNode = {};
        this.actualNode = {};

        this.searchNodes = new CatalogSearchingList();
    }

    setTree(tree) {

        this.rootNode = new FamilyNode(
            MAIN_FAMILY_NAME,
            Object.keys(tree)[0],//first key the root node
            null,
            tree[Object.keys(tree)[0]],
            null,
            this.searchNodes
        );

        this.goToRootFamily(false);
    }

    searchFamilyOrProduct(stringToFind){
        return this.searchNodes.search(stringToFind);
    }

    actualNodeHasValues() {
        return !(Object.keys(this.actualNode).length === 0);
    }

    getActualNode() {
        let actualHasValue = this.actualNodeHasValues();
        return {
            'families': actualHasValue ? this.actualNode.getDirectFamilies() : [],
            'products': actualHasValue ? this.actualNode.getProducts() : [],
        };
    }

    getBreadcrumbList() {
        return (this.actualNodeHasValues()) ? this.actualNode.getBreadcrumbList() : [];
    }

    goToChildFamily(family, index) {
        let childNode = this.actualNode.getChildFamily(family.id);
        if (childNode) {
            this.setActualNode(childNode);
        }
        //todo: retourner une erreur si la sous-catégorie n'existe pas !
    }

    goToRootFamily(addToHistory) {
        this.setActualNode(this.rootNode, addToHistory);
    }

    goToParentFamilyLink(parentLink, addToHistory) {

        let node = this.rootNode;

        if (parentLink.length === 1) {//if 1 link ==> return the root
            this.goToRootFamily(addToHistory);
            return;
        }

        //Counter i start from 1 because we don't want to search root node.
        for (let i = 1, length = parentLink.length; i < length; i++) {
            let parentId = parentLink[i];
            node = node.getChildFamily(parentId);
            if (!node) {
                this.goToRootFamily();
                return false;
            }
        }
        this.setActualNode(node, addToHistory);
        return true;
    }

    getHierarchyLevel() {
        return (this.actualNodeHasValues()) ? this.actualNode.getHierarchyLevel() : 0;
    }

    getFamilyName() {
        return (this.actualNodeHasValues()) ? this.actualNode.name : '';
    }

    setActualNode(node, addToHistory) {
        if (typeof addToHistory === 'undefined') {
            addToHistory = true;
        }

        if (this.actualNode !== node) {
            this.actualNode = node;

            if (addToHistory) {
                this.saveNodeInHistory();
            }
        }
    }

    saveNodeInHistory() {
        let id = this.actualNode.id;

        if (history.state && history.state.id && (history.state.id === id)) {
            return;
        }

        let hierarchy = JSON.parse(JSON.stringify(this.actualNode.getChildsParentHierarchyIds()));
        history.pushState(
            {
                id: id,
                hierarchy: hierarchy
            },
            'family-' + id,
            CATALOG_ROUTE + '/' + hierarchy.join('+')
        );
    }
}