export default class ACTProduct {

    constructor(id, name, description, price, priceExclTax, taxe, picture, lang, bookable, animation,
                quotation, serviceId)
    {
        this.id = id + '';
        this.name = name;
        this.description = description;
        this.price = Number(parseFloat(price).toFixed(2));
        this.priceExclTax = Number(parseFloat(priceExclTax).toFixed(2));
        this.taxe = Number((parseFloat(taxe) / 100).toFixed(2));
        this.picture = picture;
        this.lang = lang;
        this.bookable = bookable;
        this.animation = animation;
        this.quotation = quotation;
        this.type = '';
        this.initType();
        this.serviceId = (this.type === 'service') ? serviceId : '';
    }

    /* private method */
    initType() {
        let type = 'item';

        if (this.bookable) {
            type = 'service'
        }else if (this.animation) {
            type = 'animation';
        } else if (this.quotation) {
            type = 'quotation';
        }
        this.type = type;
    }
}
