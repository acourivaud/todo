export default class ACTArrayList {
    constructor() {
        this.arraylist = [];
    }

    setList(list) {
        this.arraylist = list;
    }

    getList() {
        return this.arraylist;
    }

    isEmpty() {
        return (this.arraylist.length === 0);
    }

    clear() {
        this.arraylist = [];
    }

    push(item) {
        this.arraylist.push(item);
    }

    unshift(item){
        this.arraylist.unshift(item);
    }

    findItemInList(key, value) {
        for(let i = 0, length = this.arraylist.length; i < length; i++){
            let item = this.arraylist[i];

            if( item.hasOwnProperty(key) && (item[key] === value)){
                return item;
            }
        }
        return null;
    }
}