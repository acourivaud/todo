/**
 * Created by alexj on 26/03/2017.
 */

export default class ACTLinkPostAgencyFormatter {
    static format(linkPostsAgencies) {
        linkPostsAgencies.forEach(function (linkPostAgency) {
            let regResult = Routing
                .generate('cms_concierge_show_post', {slug: linkPostAgency.pslug}, true)
                .match(/^(https?:\/\/)(\w+)(\..*)/);
            linkPostAgency.previewUrl = regResult[1] + linkPostAgency.slug + regResult[3];
            linkPostAgency.editUrl = Routing.generate('admin_todotoday_cms_post_edit', {id: linkPostAgency.pid});
        });

        return linkPostsAgencies;
    }
}