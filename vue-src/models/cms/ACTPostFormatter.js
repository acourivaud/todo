/**
 * Created by alexj on 26/03/2017.
 */

export default class ACTPostFormatter {
    static format(posts) {
        posts.forEach(function (post) {
            post.previewUrl = Routing.generate('cms_concierge_show_preview_post', {slug: post.slug}, true);
            post.editUrl = Routing.generate('admin_todotoday_cms_post_edit', {id: post.id});
        });

        return posts;
    }
}