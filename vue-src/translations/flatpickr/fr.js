/* French locals for flatpickr */
let fr = {};
fr.firstDayOfWeek = 1;
fr.weekdays = {
    shorthand: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"],
    longhand: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"]
};
fr.months = {
    shorthand: ["Janv", "Févr", "Mars", "Avr", "Mai", "Juin", "Juil", "Août", "Sept", "Oct", "Nov", "Déc"],
    longhand: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"]
};
fr.ordinal = function(nth) {
    if (nth > 1)
        return "ème";


    return "er";
};
fr.rangeSeparator = " au ";
fr.weekAbbreviation = "Sem";
fr.scrollTitle = "Défiler pour augmenter la valeur";
fr.toggleTitle = "Cliquer pour basculer";

export default fr;