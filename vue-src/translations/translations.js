export default {

  languages: [
    {
      name: 'en',
      locales: ['en', 'en-US']
    },
    {
      name: 'fr',
      locales: ['fr']
    },
    {
      name: 'de',
      locales: ['de']
    },
  ],

  getContent(languageName) {
    return require('./' + languageName + '.json');
  },

  getFlatpickrLocale(languageName) {
      let content = require('./flatpickr/' + languageName + '.js').default;
      return content;
  }
}
