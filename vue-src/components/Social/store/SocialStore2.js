import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        messages: [
            {id: 1, message: 'message 1', user: 'user 1', date: 'the_date'},
            {id: 2, message: 'message 2', user: 'user 2', date: 'the_date'},
            {id: 3, message: 'message 3', user: 'user 3', date: 'the_date'},
            {id: 4, message: 'message 4', user: 'user 4', date: 'the_date'},
            {id: 5, message: 'message 5', user: 'user 5', date: 'the_date'},
            {id: 6, message: 'message 6', user: 'user 6', date: 'the_date'}
        ]
    },
    mutation: {
        PUSH_MESSAGE: (state, message) => {
            state.messages.push(message)
        }
    },
    action: {
      addMessage (context) {
          context.commit('PUSH_MESSAGE')
      }
    },
    getters: {
        messages: state => {
            return state.messages
        }
    }
})

