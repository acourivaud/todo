import Vue from 'vue';
import translate from './../manager/TranslationManager';

Vue.filter('$t', (value, names) => translate(value, names));
