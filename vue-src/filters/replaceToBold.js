import Vue from 'vue';

Vue.filter('$r2b', (string, value) => string.replace(value, `<b>${value}</b>`));
