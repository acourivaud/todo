import Vue from 'vue';
import JWTManager from './../manager/JWTManager';

window.currency = function (value) {
    let type = JWTManager.currencyCode;
    console.log(type);
    if (type === 'EUR') {
        return Number(value).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' €';
    } else if (type === 'USD') {
        return '$ ' + Number(value).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    } else if (type === 'CHF') {
        return Number(value).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,') + ' CHF';
    } else if (type === 'CAD') {
      return Number(value).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' $CA';
    }
    return Number(value).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,');
};

Vue.filter('currency', window.currency);
