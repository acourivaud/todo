import moment from 'moment'

export default (value, isShortMonth = true) => {
  moment.locale(jwtManager.locale);

  let format = isShortMonth ? 'll' : 'LL'

  return moment(value).format(format);
};