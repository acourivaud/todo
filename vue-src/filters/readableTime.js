import moment from 'moment'

export default (value) => {
  moment.locale(jwtManager.locale);
  if (value != null) {
    return moment(value).format('LT');
  }
};