import moment from 'moment'

export default (value) => {
    moment.locale(jwtManager.locale);
    if (value != null) {
     return moment.utc(String(value)).format('DD/MM/YYYY HH:mm')
    }
  };