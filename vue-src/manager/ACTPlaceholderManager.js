export default class ACTPlaceholderManager {

  static getRandomPlaceholder() {
    let imageId = Math.floor(Math.random() * 10)

    return `/images/placeholders/social/placeholder-${imageId}.jpg`
  }
}