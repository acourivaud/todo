import Axios from 'axios'

/**
 * Class apiManager
 */
class apiManager {
  constructor (/** string */ baseUrl = null,
               /** string */ token = null,
               /** object */ headerParameters = null,
               /** string */ login = null,
               /** string */ password = null) {
    this.axios = Axios.create()
    this.token = token
    this.baseUrl = baseUrl
    this.login = login
    this.pwd = password
    this.header = headerParameters
    this.subDomain = ''
  }

  initialiseDomain (/** String */ urlData = null) {
    const baseUrl = urlData || this.baseUrl
    this.axios.defaults.baseURL = baseUrl
  }

  initialiseSubDomain (/** string */ subDomainData = null) {
    const subDomain = subDomainData || this.subDomain
    this.axios.defaults.headers.common.Agency = subDomain
  }

  initializeBearer (/** String */ tokenData = null) {
    const token = tokenData || this.token
    this.axios.defaults.headers.common.Authorization = `Bearer ${token}`
  }

  setParameters (/** String */ token = this.token, /** String */ subdomain = this.subDomain, /** String */ baseURL = this.baseUrl) {
    this.token = token
    this.subDomain = subdomain
    this.baseUrl = baseURL
  }

  deleteAllHeaders() {
    for(let header in this.axios.defaults.headers.common){
      delete this.axios.defaults.headers.common[header]
    }
  }

  prepareParameters(params) {
    const formData = new FormData();

    for (const key of Object.keys(params)) {
      if (Array.isArray(params[key])) {
        params[key].forEach((item, index) => {
          formData.append(`${key}[${index}]`, item);
        });
      } else {
        formData.append(key, params[key]);
      }
    }

    return formData;
  }

  async callInit (/** boolean */ forceToken = false, /** String */ tokenData = null, /** String */ subDomainData = null, /** String */ baseUrl = null) {
    this.initialiseDomain(baseUrl)
    if ((this.token === null && tokenData == null) || forceToken === true) {
      await this.generateToken()
    }
    this.initializeBearer(tokenData)
    this.initialiseSubDomain(subDomainData)
  }

  async generateToken () {
    this.initialiseDomain()
    // Inutile pour le moment
    // const self = this
    // await Axios.post('/login_check', {
    //   _username: this.login,
    //   _password: this.pwd,
    //
    // })
    //   .then((response) => {
    //     self.token = response.data.token
    //     self.refresh_token = response.data.refresh_token
    //   })
    //   .catch((response) => {
    //     // Todo: Creer le Throw error
    //   })
  }

  // TODO : Fonction a finir
  refreshToken () {
    this.callInit()
    this.axios.post('/refresh', {
      _username: this.login,
    })
      .then((response) => {
        self.token = response.data.token
        self.refresh_token = response.data.refresh_token
      })
      .catch((response) => {
        // Todo: Creer le Throw error
      })
  }

  async api_post (/** string */ url, /** object */ parameterBag, /** boolean */ forceToken = false, /** String */ agency = null, options = {}) {
    await this.callInit(forceToken, null, agency)
    return new Promise(
      (resolve, reject) => {
        this.axios.post(url, parameterBag, options)
          .then((response) => {
            resolve(response)
          })
          .catch((response) => {
            reject(response)
          })
      },
    )
  }

  async api_form_data (/** string */ url, /** object */ parameterBag, /** boolean */ forceToken = false, /** String */ agency = null, options = {}) {
    const params = this.prepareParameters(parameterBag);

    return this.api_post(url, params, forceToken, agency, options);
  }

  async api_get (/** string */ url, /** object */ parameterBag, /** boolean */ forceToken = false, /** String */ agency = null) {
    await this.callInit(forceToken, null, agency)
    return new Promise(
      (resolve, reject) => {
        this.axios.get(url, parameterBag)
          .then((response) => {
            resolve(response)
          })
          .catch((response) => {
            reject(response)
          })
      },
    )
  }

  async api_put (/** string */ url, /** object */ parameterBag, /** boolean */ forceToken = false, /** String */ agency = null) {
    await this.callInit(forceToken, null, agency)
    return new Promise(
      (resolve, reject) => {
        this.axios.put(url, parameterBag)
          .then((response) => {
            resolve(response)
          })
          .catch((response) => {
            reject(response)
          })
      },
    )
  }

  async api_patch (/** string */ url, /** object */ parameterBag, /** boolean */ forceToken = false, /** String */ agency = null) {
    await this.callInit(forceToken, null, agency)
    return new Promise(
      (resolve, reject) => {
        this.axios.patch(url, parameterBag)
          .then((response) => {
            resolve(response)
          })
          .catch((response) => {
            reject(response)
          })
      },
    )
  }

  async api_delete (/** string */ url, /** object */ parameterBag, /** boolean */ forceToken = false, /** String */ agency = null) {
    await this.callInit(forceToken, null, agency)
    return new Promise(
      (resolve, reject) => {
        this.axios.delete(url, parameterBag)
          .then((response) => {
            resolve(response)
          })
          .catch((response) => {
            reject(response)
          })
      },
    )
  }

  async api_get_external(/** string */ url) {
    this.deleteAllHeaders()
    this.initialiseDomain(url)

    return new Promise(
      (resolve, reject) => {
        this.axios.get('')
          .then((response) => {
            resolve(response)
          })
          .catch((error) => {
            reject(error)
          })

      }
    )
  }
}

const hostname = window.location.hostname.split('.')
hostname.splice(0, 1)
let scheme = document.getElementById('parameters').getAttribute('scheme'),
  apiUrl = `${scheme}://api.${hostname.join('.')}`

const manager = new apiManager(
  apiUrl,
  null,
  null,
  'root',
  'root',
)

global.manager = manager

export default manager
