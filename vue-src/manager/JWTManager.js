import ApiManager from './apiManager';

class JWTManager {
    constructor() {
        this.subDomain = (window.location.host).split('.')[0];
        this.jwt = '';

        let tokenInfoElement = document.getElementById('getJwtToken');

        if (tokenInfoElement && tokenInfoElement.getAttribute('token') &&
            (tokenInfoElement.getAttribute('token') !== '')) {
            let tokenInfos = JSON.parse(tokenInfoElement.getAttribute('token'));

            this.locale = tokenInfos['locale'];
            this.currencyCode = tokenInfos['currency'];

            if (tokenInfos.user) {
                this.user = tokenInfos.user;
                this.userType = tokenInfos['user_type'];
                this.isCoAdherent = tokenInfos['is_co_adherent'];
                this.paymentMethodStatus = tokenInfos['payment_method_status'];
                this.paymentTypeSelected = tokenInfos['payment_type_selected'];
                this.jwt = tokenInfos.jwt;
                this.refreshToken = tokenInfos['refresh_token'];
                this.grantedAddCart = tokenInfos['grantedAddCart'];
                this.fgsModuleEnable = tokenInfos['fgs_module_enable'];
            }
            tokenInfoElement.parentElement.removeChild(tokenInfoElement);
        } else {
            console.log('JWT token is empty or not set !');
        }
    }

    //Use this method for each entries/output files because ApiManager is not set as a global variable (like window.apiManager)
    setApiManagerWithToken() {
        if ((this.jwt !== '') && this.subDomain) {
            ApiManager.setParameters(
                this.jwt,
                this.subDomain
            );
        }
    }
}

if (!window.jwtManager) {
    window.jwtManager = new JWTManager();
}
export default window.jwtManager;