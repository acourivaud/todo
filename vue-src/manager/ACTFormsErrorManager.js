class ACTFormErrorManager {

  constructor() {
    this.fieldsName = {}
  }

  addFieldsName(fieldsNameArray) {
    if (!(fieldsNameArray instanceof Array)) {
      fieldsNameArray = []
      console.log('ACTFormErrorManager addFieldsName : "fieldsNameArray" must be an array')
    }

    fieldsNameArray.forEach((fieldName) => {
      this.addFieldName(fieldName)
    })
  }

  addFieldName(fieldName) {
    if (!this.fieldsName.hasOwnProperty(fieldName)) {
      this.fieldsName[fieldName] = {hasError: null}
    } else {
      console.log('ACTFormErrorManager constructor : property "' + fieldName + '" already exist')
    }
  }

  getField(fieldName) {
    return this.fieldsName[fieldName]
  }

  hasAtLeastOneError() {
    for (let key in this.fieldsName) {
      if (this.fieldsName.hasOwnProperty(key) && this.fieldsName[key].hasError) {
        return true
      }
    }

    return false
  }
}

class ACTFormsErrorManager {
  constructor() {
    this.forms = {}
  }

  addFormName(formName) {
    if(this.forms.hasOwnProperty(formName)){
      return
    }
    this.forms[formName] = new ACTFormErrorManager()
  }

  removeFormName(formName){
    if(this.forms.hasOwnProperty(formName)){
      delete this.forms[formName]
    }
  }

  getFormErrorManager(formName) {
    if(!this.forms.hasOwnProperty(formName)){
      console.log('ACTFormsErrorManager getFormErrorManager : property "' + formName + '" does not exist')
    }

    return this.forms[formName]
  }
}

export default new ACTFormsErrorManager()