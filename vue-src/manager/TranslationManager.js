import translations from './../translations/translations';
import JWTManager from './JWTManager';

const WARNING_PREFIX = '[TranslationManager Warning] : ';

class TranslationManager {
    constructor() {
        this.language = null;
        this.content = null;
        this.flatpickrLocale = null;

        this.silent = true;

        if (!this.setLocale(JWTManager.locale)) {
            console.log(
                WARNING_PREFIX + 'Failed to set locale with JWTManager.\n' +
                '"' + JWTManager.locale + '" not found in \'/vue-src/translations/translations.js\''
            );
            return;
        }
        if (!this.setFlatpickrLocale()) {
            console.log(
                WARNING_PREFIX + 'Failed to set FlatpickrLocale with TranslationManager.\n' +
                'File \'' + this.language.name + '.js\' not found in \'/vue-src/translations/flatpickr/\''
            );
        }
    }

    /*
     string label
     object names
     */
    get(label, names) {
        if (this.content) {
            if (this.content.hasOwnProperty(label)) {
                return this.replaceNames(label, names);
            }
            this.log('The label "' + label + '" is not defined for the language "' + this.language.name + '"');
        } else {
            this.log('Set the locale with "setLocale(locale)" to use "get" method.');
        }
        return label;
    }

    /*
     string label
     object names
     */
    replaceNames(label, names) {
        let text = this.content[label];

        if ((typeof names === 'undefined')) {
            return text;
        }

        for (let name in names) {
            if (names.hasOwnProperty(name)) {
                text = text.replace(new RegExp(name, 'g'), names[name]);
            }
        }
        return text;
    }

    /*
     * string locale
     */
    setLocale(locale) {
        let languages = translations.languages;

        for (let i = 0, length = languages.length; i < length; i++) {
            let locales = languages[i].locales;

            for (let y = 0, length = locales.length; y < length; y++) {
                if (locales[y] === locale) {
                    this.language = languages[i];
                    this.content = translations.getContent(this.language.name);
                    return true;
                }
            }
        }
        return false;
    }

    setFlatpickrLocale(){
        if(this.language.name !== 'en'){
            this.flatpickrLocale = translations.getFlatpickrLocale(this.language.name);
            if(!this.flatpickrLocale.hasOwnProperty('firstDayOfWeek')){
                return false;
            }
        }
        return true;
    }

    /*
     * string text
     */
    log(text) {
        if(!this.silent){
            console.log(WARNING_PREFIX + text);
        }
    }
}

if (!window.$t) {
    JWTManager.setApiManagerWithToken();

    let translationManager = new TranslationManager();
    translationManager.silent = false;// <<<<<<<<<<<<<<<<<<<  todo: MUST SET TO TRUE IN PRODUCTION

    window.$t = translationManager.get.bind(translationManager);
    window.flatpickrLocale = translationManager.flatpickrLocale;
}

export default window.$t;