# Installation on macOS

## Requirements

- MacOS 10.15+ 
- nginx 1.17+
- PHP 7.4.x
- PostgreSQL 11.x
- Redis 5.0.x


## Setup Valet

```bash
composer global require laravel/valet
# Make sure the ~/.composer/vendor/bin directory is in your system's "PATH"
valet install
valet use php@7.4
```

cf. [Official Valet documentation for more infos](https://laravel.com/docs/8.x/valet)

## Install Sources

```bash
unzip todo-today.zip -d /path/to/desired/parent/folder
cd /path/to/your/fresh/todo-today/folder
valet link
valet secure
```

## Setup database

- On your postgresql 11.x instance create a empty "tdtd" database and a associated "tdtd_local" user with a "azerty" password. If you change names, update the ```app/config/parameters.yml```.
- Import the tdtd.dump file available in the root folder

## Setup Email

- Create an account at mailtrap.io.
- Copy your SMTP credentials and paste into ```app/config/parameters.yml``` (mailer_user et mailer_password)
- Mailtrap will capture all emails sent by the app. See your mailtrap inbox to open emails.

## Access

- After installation you should be able to access https://www.todo-today.test
- A super-admin user is available with id ```root```and password ```tdtd2017!```
- Demo Conciergerie is available at https://demo.todo-today.test
