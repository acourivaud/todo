var Encore = require('@symfony/webpack-encore');
const webpack = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

Encore
    .setOutputPath('web/build/')
    .setPublicPath('/build')

    .addStyleEntry('main', './app/Resources/assets/sass/app.scss')
    .enableSassLoader()
    .enablePostCssLoader()

    // VueCli
    .addEntry('postsToValidate', './vue-src/bundles/postsToValidate/PostsToValidate.js')
    .addEntry('latestPost', './vue-src/bundles/latestPosts/LatestPosts.js')
    .addEntry('socialGroup', './vue-src/bundles/socialGroup/socialGroup.js')
    .addEntry('viewGroup', './vue-src/bundles/socialGroup/viewGroup.js')
    .addEntry('catalog', './vue-src/bundles/catalog/singlePageCatalog.js')
    .addEntry('bookingCalendar', './vue-src/bundles/bookingCalendar/bookingCalendar.js')
    .addEntry('adherentMigration', './vue-src/bundles/adherentMigration/adherentMigration.js')
    .addEntry('catalogCache', './vue-src/bundles/catalogCache/catalogCache.js')
    .addEntry('uploadDocument', './vue-src/bundles/document/uploadDocument.js')
    .addEntry('sharedFiles', './vue-src/bundles/document/sharedFiles.js')
    .addEntry('smartBannerApp', './vue-src/bundles/smartBanner/smartBannerApp.js')

    // Vanilla Js
    .addEntry('translationManager', './vue-src/manager/TranslationManager.js')
    .addEntry('api_manager', './vue-src/manager/apiManager.js')
    .addEntry('jwt_manager', './vue-src/manager/JWTManager.js')
    .addEntry('depository_case', './src/Todotoday/CheckoutBundle/Resources/public/js/depository_case.js')
    .addEntry('accountSEPAInfoType', './src/Todotoday/PaymentBundle/Resources/public/js/AccountSEPAInfoType.js')
    .addEntry('AccountTransactionalInfoType', './src/Todotoday/AccountBundle/Resources/public/js/AccountTransactionalInfoType.js')
    .addEntry('AccountState', './src/Todotoday/AccountBundle/Resources/public/js/AccountState.js')
    .addEntry('AccountPhoneDialInfoType', './src/Todotoday/AccountBundle/Resources/public/js/AccountPhoneDialInfoType.js')
    .addEntry('linkPostAgencyAdmin', './src/Todotoday/AccountBundle/Resources/public/js/linkPostAgencyAdmin.js')

    .addEntry('burgermenu', './app/Resources/public/js/burgermenu.js')
    .addEntry('cart', './app/Resources/public/js/cart.js')
    .addEntry('dataLoader', './app/Resources/public/js/dataLoader.js')
    .addEntry('extraProfile', './app/Resources/public/js/extraProfile.js')
    .addEntry('horizontalNav', './app/Resources/public/js/horizontalNav.js')
    .addEntry('menuIcon', './app/Resources/public/js/menuIcon.js')
    .addEntry('sidebar', './app/Resources/public/js/sidebar.js')
    .addEntry('sidebarCounters', './app/Resources/public/js/sidebarCounters.js')
    .addEntry('sidebarOrderRefusedCounters', './app/Resources/public/js/sidebarOrderRefusedCounters.js')

    .addEntry('dateTimePickerType', './src/Actiane/ToolsBundle/Resources/public/js/dateTimePickerType.js')
    .addEntry('timePickerType', './src/Actiane/ToolsBundle/Resources/public/js/timePickerType.js')

    .addEntry('store/LayoutStore', './app/Resources/public/js/store/LayoutStore.js')

    .addEntry('bundles/buttons_form_flow', './app/Resources/public/js/bundles/buttons_form_flow.js')
    .addEntry('bundles/checkout_booking_success', './app/Resources/public/js/bundles/checkout_booking_success.js')
    .addEntry('bundles/order', './app/Resources/public/js/bundles/order.js')
    .addEntry('bundles/order_booked', './app/Resources/public/js/bundles/order_booked.js')
    .addEntry('bundles/stripe', './app/Resources/public/js/bundles/stripe.js')
    .addEntry('bundles/stripe_new_vue', './app/Resources/public/js/bundles/stripe_new_vue.js')

    .addEntry('bundles/animation', './app/Resources/public/js/bundles/animation.js')
    .addEntry('bundles/order_history', './app/Resources/public/js/bundles/order_history.js')

    .addEntry('filters/capitalize', './app/Resources/public/js/filters/capitalize.js')
    .addEntry('filters/currency', './app/Resources/public/js/filters/currency.js')
    .addEntry('filters/dateFormater', './app/Resources/public/js/filters/dateFormater.js')
    .addEntry('filters/translation', './app/Resources/public/js/filters/translation.js')

    .addEntry('components/tweeningValue', './app/Resources/public/js/components/tweeningValue.js')
    .addEntry('components/blablacar', './app/Resources/public/js/components/blablacar.js')

    .addEntry('components/animation/ACTAnimationCancellationButton', './app/Resources/public/js/components/animation/ACTAnimationCancellationButton.js')
    .addEntry('components/animation/ACTAnimationConfirmationModal', './app/Resources/public/js/components/animation/ACTAnimationConfirmationModal.js')

    .addEntry('components/order/ACTOrderCancelModal', './app/Resources/public/js/components/order/ACTOrderCancelModal.js')
    .addEntry('components/order/ACTOrderRatingModal', './app/Resources/public/js/components/order/ACTOrderRatingModal.js')
    .addEntry('components/order/ACTOrderEditModal', './app/Resources/public/js/components/order/ACTOrderEditModal.js')

    .createSharedEntry('vendor',
        [
            'vue',
            'vue-clickaway',
            'vue-flatpickr',
            'vue-mugen-scroll',
            'vue-multiselect',
            'vue-select',
            'axios',
            'moment',
            'babel-polyfill',
            'es6-promise',
        ])

    .enableVueLoader()
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction());

const webpackConfig = Encore.getWebpackConfig();

if (Encore.isProduction()) {
  webpackConfig.plugins = webpackConfig.plugins.filter(
    plugin => !(plugin instanceof webpack.optimize.UglifyJsPlugin)
  );

  webpackConfig.plugins.push(new UglifyJsPlugin());
}

// export the final configuration
module.exports = webpackConfig;
