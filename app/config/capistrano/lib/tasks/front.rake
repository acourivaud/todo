namespace :front do
   task :install do
     on roles(:web) do
        within release_path do
            execute *%w[yarn install]
        end
     end
   end
   task :vue do
     on roles(:web) do
        within release_path do
            execute *%w[yarn run encore production]
        end
     end
   end
   task :picto do
     on roles(:web) do
        within release_path do
            # invoke "symfony:console", "picto:create", "", :web
        end
     end
   end
end
