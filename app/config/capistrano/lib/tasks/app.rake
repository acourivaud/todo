namespace :app do
    task :reset do
        on roles(:db) do
            invoke "symfony:console", "doctrine:database:drop", "--force --if-exists", :db
            invoke "symfony:console", "doctrine:cache:clear-metadata", "", :db
            invoke "symfony:console", "doctrine:cache:clear-query", "", :db
            invoke "symfony:console", "doctrine:cache:clear-result", "", :db
            invoke "symfony:console", "doctrine:database:create", "", :db
            invoke "symfony:console", "doctrine:schema:create", "", :db
            invoke "symfony:console", "actiane:api:init-acl", "", :db
            invoke "symfony:console", "sonata:admin:setup-acl", "", :db
            invoke "symfony:console", "doctrine:fixtures:load", "-n", :db
        end
    end
    task :resetdump do
        on roles(:db) do
# uncomment if not in prod's cluster
#            execute "cd '#{release_path}'; scp -oProxyCommand=\"ssh -W %h:%p deploy@195.154.42.65\" deploy@db1.todotoday.local:/mnt/backups/tdtd-last.dump .", :db
            execute "cd '#{release_path}'; scp deploy@db1.todotoday.local:/mnt/backups/tdtd-last.dump .", :db
            execute "sudo service php7.1-fpm stop"
            invoke "symfony:console", "doctrine:database:drop", "--force --if-exists", :db
            invoke "symfony:console", "doctrine:cache:clear-metadata", "", :db
            invoke "symfony:console", "doctrine:cache:clear-query", "", :db
            invoke "symfony:console", "doctrine:cache:clear-result", "", :db
            invoke "symfony:console", "doctrine:database:create", "", :db
            execute "cd '#{release_path}'; pg_restore -d tdtd_preprod tdtd-last.dump", :db
            invoke "symfony:doctrine:migrate"
            execute "sudo service php7.1-fpm start"
            execute "scp -r deploy@web1.todotoday.local:/home/www-data/tdtd/shared/web /home/www-data/tdtd/shared"
        end
    end
end
