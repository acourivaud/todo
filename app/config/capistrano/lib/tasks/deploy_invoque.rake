namespace :deploy do

	Rake::Task['updated'].clear_actions
	task :updated do
	    invoke "front:install"
	    invoke "front:vue"
	    invoke "symfony:doctrine:clear"
		invoke "symfony:cache:clear"
# 		invoke "symfony:clear_controllers"
#		invoke "symfony:version:bump"
# 	    invoke "symfony:assetic:dump"
	    invoke "front:picto"
	end
end
