namespace :php do
    namespace :fpm do
        task :restart do
            on roles(:all) do
                execute 'sudo service php7.4-fpm restart'
            end
        end
    end
end
