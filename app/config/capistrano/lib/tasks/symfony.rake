namespace :symfony do
  namespace :doctrine do
      task :migrate do
        on roles(:db) do
          invoke 'symfony:console', 'doctrine:migrations:migrate', '--no-interaction', :db
        end
      end
      task :update do
        on roles(:db) do
          invoke 'symfony:console', 'doctrine:schema:update', '--force', :db
        end
      end
      task :validate do
          invoke 'symfony:console', 'doctrine:schema:validate'
      end
      task :dump do
        on roles(:db) do
          invoke 'symfony:console', 'doctrine:schema:update', '--dump-sql', :db
        end
      end
      task :create do
        on roles(:db) do
          invoke 'symfony:console', 'doctrine:schema:create', '', :db
        end
      end
      task :update do
        on roles(:db) do
          invoke 'symfony:console', 'CMSController.php ', '--force', :db
        end
      end
      task :drop do
        on roles(:db) do
          invoke 'symfony:console', 'doctrine:schema:drop', '--force', :db
        end
      end
      task :clear do
        on roles(:db) do
          invoke 'symfony:console', 'doctrine:cache:clear-metadata', '', :db
          invoke 'symfony:console', 'doctrine:cache:clear-query', '', :db
          invoke 'symfony:console', 'doctrine:cache:clear-result', '', :db
        end
      end
      task :fixtures do
        on roles(:db) do
          invoke 'symfony:console', 'doctrine:fixtures:load'
        end
      end
      task :reset do
        on roles(:db) do
          invoke 'symfony:doctrine:drop'
          invoke 'symfony:doctrine:clear'
          invoke 'symfony:doctrine:create'
          invoke 'symfony:doctrine:fixtures'
        end
      end
  end
  namespace :version do
    task :bump do
        on roles(:db) do
          invoke 'symfony:console', 'app:version:bump'
        end
    end
  end
  namespace :sonata do
    task :acl do
        on roles(:web) do
            invoke 'symfony:console', 'sonata:admin:setup-acl', '', :web
        end
    end
  end
end
