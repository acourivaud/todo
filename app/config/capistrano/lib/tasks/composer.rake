namespace :composerdeploy do
   task :install do
     on roles(:web) do
        Rake::Task["composer:run"].reenable
        invoke "composer:run", "run-script", "--no-dev install-deploy"
     end
   end
end