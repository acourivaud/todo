Vue.component('animated-integer', {
  template: '<span>{{ tweeningValue | currency }}</span>',
  props: {
    value: {
      type: Number,
      required: true,
    },
  },
  data() {
    return {
      tweeningValue: 0,
    };
  },
  watch: {
    value(newValue, oldValue) {
      this.tween(oldValue, newValue);
    },
  },
  mounted() {
    this.tween(0, this.value);
  },
  methods: {
    tween(startValue, endValue) {
      const vm = this;
      let animationFrame;

      function animate(time) {
        TWEEN.update(time);
        animationFrame = requestAnimationFrame(animate);
      }

      new TWEEN.Tween({ tweeningValue: startValue })
        .to({ tweeningValue: endValue }, 500)
        .onUpdate(function () {
          vm.tweeningValue = this.tweeningValue.toFixed(3);
        })
        .onComplete(() => {
          cancelAnimationFrame(animationFrame);
        })
        .start();
      animationFrame = requestAnimationFrame(animate);
    },
  },
});
