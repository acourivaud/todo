import Message from './../../../../../../vue-src/components/base/ACTMessage'
import Modal from './../../../../../../vue-src/components/base/ACTModal'
import LoaderContent from './../../../../../../vue-src/components/base/ACTLoaderContent'

export default {
  template: `<modal  :visible="annulationModalVisible"
                       size="lg"
                       @close="updateModalVisible(false)"
                       v-cloak
                >
                    <div slot="header">
                        <h3>
                            #{ 'order.annulation'|$t }
                             (ref: #{ salesOrderNumber })
                         </h3>
                    </div>
                    <p slot="body">
                        <template v-if="!bodyMessage">
                          #{ 'order.fee_if_cancel'|$t }
                          <br>
                          #{ 'order.contact_concierge_for_more_details'|$t }
                        </template>
                        
                        <template v-else>
                            #{ bodyMessage|$t }
                        </template>
                        
                        <message type="error"
                             :is-visible="orderCancelFailed">
                             
                            #{ 'order.error.canceling'|$t }
                        </message>
                        </div>
                    </p>         
                                            
                    <div slot="footer">
                    
                        <loader-content :is-loading="isSendingCancelRequest" loader-size="small"
                                        class="full-width">
                                        
                          <div class="btn-row-flex">
                              <button @click="updateModalVisible(false)"
                                      class="btn btn-blanc">
                                  #{ 'order.back_to_order_list'|$t }
                              </button>
  
                              <button @click="cancelOrder" class="btn btn-primaire">
                                  #{ 'order.confirm_annulation'|$t }
                              </button>
                          </div>
                          
                        </loader-content>
                    </div>
                </modal>`,

  delimiters: ['#{', '}'],

  components: {
    'message': Message,
    'modal': Modal,
    'loader-content': LoaderContent,
  },

  props: {
    salesOrderNumber: {type: String, default: ''},
    annulationModalVisible: {type: Boolean, default: false},
    bodyMessage: {type: String, default: null}
  },

  data () {
    return {
      orderCancelFailed: false,
      isSendingCancelRequest: false,
    }
  },

  methods: {
    updateModalVisible (isVisible) {
      this.$emit('updatemodalvisible', isVisible)
    },

    cancelOrder () {
      this.orderCancelFailed = false
      this.isSendingCancelRequest = true

      if (this.salesOrderNumber === '') {
        return
      }
      let self = this

      let url = Routing.generate(
        'todotoday.checkout.api.order.put_cancel_ref_order',
        {refOrder: this.salesOrderNumber}
      )

      manager.api_put(url, {},
      ).then(function (response) {
        self.$emit('ordercanceled')
      }).catch(function (error) {
        self.orderCancelFailed = true
        console.log('error:', error)
      }).then(() => {
        self.isSendingCancelRequest = false
      })
    }
  },
}
