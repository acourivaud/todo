import Message from './../../../../../../vue-src/components/base/ACTMessage'
import Modal from './../../../../../../vue-src/components/base/ACTModal'
import LoaderContent from './../../../../../../vue-src/components/base/ACTLoaderContent'

export default {
    template: `<modal  :visible="editModalVisible"
                       size="lg"
                       @close="updateModalVisible(false)"
                       v-cloak
                       class="modal-order-edit"
                >
                    <div slot="header">
                        <h3>
                            #{ 'order.edit_order'|$t }
                             (ref: #{ salesOrderNumber })
                         </h3>
                    </div>
                    <div slot="body">
                        <h2>#{ 'order.edit.modal_subtitle'|$t }</h2>
                        <div class="form-row">
                        
                          <label>#{ 'checkout.delivery_description_Casiers'|$t }</label>
                          <input type="text" v-model="lockerNumber">
                          <div v-for="line,index in lines" style="margin-top: 25px">
                              <h4 class="order-line-description">
                                <div class="description">#{ line.description}</div>
                                <div clas="quantity">x #{ line.quantity}</div>
                              </h4>
                              <label>#{ 'checkout.comment_depository'|$t }</label>
                              <textarea v-model="commentAdherent[index]"></textarea>
                          </div>
                        </div>
                        
                        <message type="error"
                             :is-visible="orderEditFailed">
                             
                            #{ 'order.error.editing'|$t }
                        </message>
                    </div>
                                            
                    <div slot="footer">
                    
                        <loader-content :is-loading="isSendingEditRequest" loader-size="small"
                                        class="full-width">
                                        
                          <div class="btn-row-flex">
                              <button @click="updateModalVisible(false)"
                                      class="btn btn-blanc">
                                  #{ 'order.back_to_order_list'|$t }
                              </button>
  
                              <button @click="editOrder" class="btn btn-primaire">
                                  #{ 'order.confirm_edit'|$t }
                              </button>
                          </div>
                          
                        </loader-content>
                    </div>
                </modal>`,

    delimiters: ['#{', '}'],

    components: {
        'message': Message,
        'modal': Modal,
        'loader-content': LoaderContent,
    },

    props: {
        salesOrderNumber: {type: String, default: ''},
        dataAreaId: {type: String, default: ''},
        lockerNumber: {type: String, default: ''},
        lines: {type: Array, default: []},
        editModalVisible: {type: Boolean, default: true}
    },

    data() {
        return {
            orderEditFailed: false,
            isSendingEditRequest: false,
            commentAdherent: []
        }
    },
    methods: {
        updateModalVisible(isVisible) {
            this.$emit('updatemodalvisible', isVisible)
        },

        editOrder() {
          this.orderEditFailed = false

            let orderUpdated = {
                SalesOrderHeader: {
                    id: {
                        salesOrderNumber: this.salesOrderNumber,
                        dataAreaId: this.dataAreaId,
                    },
                    customerRefDepot: this.lockerNumber
                },
                Lines: []
            }
            this.lines.forEach((line, index) => {
                orderUpdated.Lines.push(
                    {
                        id: {
                            inventoryLotId: line.inventoryLotId,
                            dataAreaId: this.dataAreaId,
                        },
                        customerRef: this.commentAdherent[index]
                })
            })

            let url = Routing.generate(
              'todotoday.checkout.api.order.post_edit_refused_order'
            )

            this.isSendingEditRequest = true

            manager.api_post(url, orderUpdated,
            ).then(() => {
              this.$emit('order-edited')
            }).catch((error) => {
              this.orderEditFailed = true
              console.log('error:', error)
            }).then(() => {
              this.isSendingEditRequest = false
            })
        }
    },
}
