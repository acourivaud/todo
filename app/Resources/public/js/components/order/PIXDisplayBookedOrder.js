import moment from 'moment-timezone';

export default {
    template: `
        <strong> #{ displayTime } </strong>
    `,
    delimiters: ['#{', '}'],
    props: {
        time: String,
        locale: 'fr',
    },
    computed: {
        displayTime() {
            moment.locale(this.locale);
            const timezone = moment.tz.guess();
            const initialDate = moment.tz(this.time.date, 'YYYY-MM-DD HH:mm:ss', timezone);

            if (initialDate.isValid()) {
                const offset = moment().utcOffset();
                const finalDate = initialDate.add(offset, 'minutes');

                return finalDate.format($t('booking.modal_confirmation_start_date_format'));
            }

            return this.time.date;
        }
    }
}
