import JWTManager from './../../../../../../vue-src/manager/JWTManager'
import Message from './../../../../../../vue-src/components/base/ACTMessage'
import Modal from './../../../../../../vue-src/components/base/ACTModal'
import LoaderContent from './../../../../../../vue-src/components/base/ACTLoaderContent'
import StarRating from 'vue-star-rating'

const ORDER_RATING_INIT = 4;
const NUMBER_STARS = 4;

export default {
    template: `<modal class="modal-order-rating"
                       :visible="ratingModalVisible"
                       :size="modalSize"
                       :disable-click-away="disableClickAway"
                       @close="updateModalVisible(false)"
                       v-cloak
                >
                    <div slot="header">
                        <h3 class="title-header">
                            #{ 'order.rating'|$t }
                            <br>
                            (ref: #{ salesOrderNumber })
                        </h3>
                    </div>
                    <div slot="body">
                    
                        <div v-if="!isAfterRatingChoices">
                            <p class="stars-rating">
                                <star-rating v-model="orderRating"
                                             class="stars"
                                             :star-size="40"
                                             :increment="1"
                                             :max-rating="numberStars"
                                             :show-rating="false"
                                             active-color="#F36F20">
    
                                </star-rating>
                            </p>
    
                            <label for="orderRateComment">#{ 'booking.comment_to_concierge'|$t }</label>
                            <textarea v-model="orderRateComment"
                                      :placeholder="'booking.comment_to_concierge'|$t"
                                      rows="7"
                                      id="orderRateComment">
                            </textarea>
    
                            <div class="order-is-receipted">
                                <input id="orderIsReceipted" v-model="orderIsReceipted" type="checkbox">
                                <label for="orderIsReceipted" class="checkbox">
                                    #{ 'order.confirm_reception'|$t }
                                </label>
                            </div>
    
                            <message type="warning"
                                     :is-visible="isReceiptedNotChecked">
                                #{ 'order.error.confirm_reception'|$t }
                            </message>
    
                            <message type="error"
                                     :is-visible="orderRateFailed">
                                #{ 'order.error.rating'|$t }
                            </message>
                        </div>
                        
                        <div v-else>
                            <div class="icon-fgs-or-satisfaction">
                                <template v-if="satisfactionPartEnable">
                                    <i class="mdi mdi-clipboard-check"></i>
                                </template>
                                
                                <template v-if="fgsPartEnable && fgsModuleEnable">
                                    <i class="mdi mdi-clipboard-alert"></i>
                                </template>
                            </div>
                            
                            <p>#{ 'general.your_satisfaction_our_priority'|$t }</p>
                            
                            <template v-if="satisfactionPartEnable">
                                <label for="order_satisfaction_subject">
                                    #{ 'order.satisfaction_motif' | $t }
                                </label>
                
                                <div class="styled-select">
                                    <select v-model="satisfactionSubjectSelected" id="order_satisfaction_subject">
                
                                        <option v-for="subject in satisfactionSubjects" :value="subject.value">
                                            #{ subject.text | $t }
                                        </option>
                
                                    </select>
                                </div>
                                
                                <message type="warning"
                                     :is-visible="noSatisfactionSubjetSelected">
                                #{ 'order.warning.no_satisfaction_subject_selected'|$t }
                                </message>
                            </template>

                            <template v-if="fgsPartEnable && fgsModuleEnable">
                                <p>
                                    #{ 'order.bad_rating_info' | $t }
                                </p>
                                
                                <p>
                                    #{ 'order.can_fill_fgs' | $t }
                                </p>
                                
                                <button @click="updateModalVisible(false)" class="btn btn-blanc full-width btn-ignore-fgs">
                                    #{ 'order.ignore_fgs_creation' | $t }
                                </button>
                            
                                <a :href="hrefFGS" class="btn btn-primaire full-width">
                                    #{ 'fgs.file_fgs' | $t }
                                </a>
                            </template>
                        </div>

                    </div>
                    <div slot="footer">
                    
                        <div v-if="!isAfterRatingChoices">
                            <loader-content :is-loading="isSendingRateRequest" loader-size="small"
                                      class="full-width">
                            <div class="btn-row-flex">
                                <button @click="updateModalVisible(false)"
                                        class="btn btn-blanc">
                                        #{ 'order.back_to_order_list'|$t }
                                </button>
                                <button @click="rateOrder" class="btn btn-primaire">
                                    #{ 'order.rate_this_order'|$t }
                                </button>
                            </div>
                            </loader-content>
                        </div>
                        
                        <div v-else>
                        
                            <template v-if="satisfactionPartEnable">

                                <div class="btn-row-flex btn-satisfaction">
        
                                    <loader-content :is-loading="sendingOrderSatisfactionMotif"
                                                    loader-size="small">
                                        <button @click="sendSatisfactionMotif" class="btn btn-primaire">
                                            #{ 'order.send_satisfaction'|$t }
                                        </button>
                                    </loader-content>
                                    
                                </div>
                                
                            </template>
                            
                        </div>
                        
                    </div>
                </modal>`,

    delimiters: ['#{', '}'],

    components: {
      'message': Message,
      'modal': Modal,
      'star-rating': StarRating,
      'loader-content': LoaderContent,
    },

    props: {
        salesOrderNumber: {type: String, default: ''},
        satisfactionSubjects: {
            type: Array,
            default: function () {
                return [];
            }
        },
        ratingModalVisible: {type: Boolean, default: false},
    },

    data() {
        return {
            orderIsReceipted: false,
            orderRating: ORDER_RATING_INIT,
            orderRateComment: '',
            orderRateFailed: false,
            numberStars: NUMBER_STARS,
            isReceiptedNotChecked: false,
            isAfterRatingChoices: false,
            satisfactionSubjectSelected: '',
            sendingOrderSatisfactionMotif: false,
            noSatisfactionSubjetSelected: false,
            isSendingRateRequest: false,
        }
    },

    created: function () {
        this.resetRatingValues();
    },

    methods: {
        resetRatingValues: function () {
            this.orderIsReceipted = false;
            this.orderRating = ORDER_RATING_INIT;
            this.orderRateComment = '';
            this.orderRateFailed = false;
            this.isReceiptedNotChecked = false;
            this.isAfterRatingChoices = false;
            this.satisfactionSubjectSelected = '';
            this.sendingOrderSatisfactionMotif = false;
            this.noSatisfactionSubjetSelected = false;
        },

        updateModalVisible(isVisible){
            this.$emit('updatemodalvisible', isVisible);
        },

        rateOrder: function () {
            this.orderRateFailed = false;

            if (this.salesOrderNumber === '') {
                return;
            }
            if (!this.orderIsReceipted) {
                this.isReceiptedNotChecked = true;
                return;
            }

            this.orderRateFailed = false;

            let self = this;
            this.isSendingRateRequest = true

            let url = Routing.generate(
                'todotoday.checkout.api.order.put_rate_ref_order',
                {refOrder: this.salesOrderNumber}
            );

            manager.api_put(url, {
                isReceipted: this.orderIsReceipted,
                rate: this.orderRating,
                comment: this.orderRateComment
            }).then(function (response) {
                console.log(response);
                self.showAfterRatingChoices();
            }).catch(function (error) {
                self.orderRateFailed = true;
                console.log('error:', error);
            }).then(() => {
              self.isSendingRateRequest = false
            });
        },

        showAfterRatingChoices(){
            this.isAfterRatingChoices = true;
        },

        sendSuccessRate() {
            this.$emit('orderrated');
        },

        sendSatisfactionMotif() {//TODO: FAIRE LE CALL API POUR ENVOYER LE MOTIF DE SATISFACTION CHEZ MICROSOFT
            if(this.satisfactionSubjectSelected === ''){
                this.noSatisfactionSubjetSelected = true;
                return;
            }

            this.sendingOrderSatisfactionMotif = true;

            // let self = this;

            // let url = Routing.generate(
            //     'todotoday.checkout.api.order.put_rate_ref_order',
            //     {refOrder: this.salesOrderNumber}
            // );
            //
            // manager.api_post()

            console.log('Satisfaction subject selected : ', this.satisfactionSubjectSelected);

            setTimeout(() => {
                this.sendingOrderSatisfactionMotif = false;
            }, 2000);

            this.updateModalVisible(false);
        },
    },

    computed: {
        modalSize(){
            if(this.satisfactionPartEnable){
                return 'md';
            } else if (this.fgsPartEnable){
                return 'sm';
            } else {
                return 'lg';
            }
        },
        disableClickAway(){
          return (this.isAfterRatingChoices);
        },
        satisfactionPartEnable(){
            return this.isAfterRatingChoices && (this.orderRating >= 3);
        },
        fgsPartEnable() {
            return this.isAfterRatingChoices && (this.orderRating < 3);
        },
        fgsModuleEnable() {
            return JWTManager.fgsModuleEnable
        },
        hrefFGS() {
            return Routing.generate(
                'fgs_new',
                {orderId: this.salesOrderNumber}
            );
        },
    },

    watch: {
        orderIsReceipted(newValue, oldValue){
            if (newValue) {
                this.isReceiptedNotChecked = false;
            }
        },
        ratingModalVisible(newValue, oldValue){
            if (!newValue) {

                if (this.isAfterRatingChoices) {
                    this.sendSuccessRate();
                }

                this.resetRatingValues();
            }
        },
      fgsPartEnable(newValue) {
          if(newValue && !this.fgsModuleEnable) {
            this.updateModalVisible(false);
          }
      }
    }
};
