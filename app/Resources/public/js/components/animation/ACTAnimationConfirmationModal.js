import Message from './../../../../../../vue-src/components/base/ACTMessage'
import LoaderContent from './../../../../../../vue-src/components/base/ACTLoaderContent'
import Modal from './../../../../../../vue-src/components/base/ACTModal'

export default {
    template: `<modal :visible="confirmationModalVisible"
               size="md"
               :disable-click-away="true"
               @close="updateModalVisible(false)"
               v-cloak
        >
            <div slot="header">
                <h3>#{ 'animation.registration_title'|$t }</h3>
            </div>
            <p slot="body">
                
                #{ animationTitle }<br>
                <i class="mdi mdi-calendar">#{ startDateTime }</i><br>
                <i class="mdi mdi-calendar">#{ endDateTime }</i><br>
                #{ 'animation.price'|$t }&nbsp;:&nbsp;<span v-html="price"></span>

                <slot name="payment-area"></slot>
                
                <message class="message-error"
                         type="error"
                         :is-visible="isPostAnimationFailed">
                    #{ 'animation.registration_failed'|$t }
                </message>
                
                <message class="message-error"
                         type="warning"
                         :is-visible="isStripeTokenNull">
                    #{ 'account.error_stripe_cart_not_added'|$t }
                </message>
            </p>

            <div slot="footer">
                <div class="btn-row-flex">
                    <button @click="updateModalVisible(false)"
                            class="btn btn-blanc">
                        #{ 'animation.back_to_animation_list'|$t }
                    </button>
                    <loader-content :is-loading="isSendPostAnimation"
                                    loader-size="small">
                        <button @click="confirmAnimation" class="btn btn-primaire">
                            #{ 'animation.confirm_registration'|$t }
                        </button>
                    </loader-content>
                    
                    <!-- HIDDEN FORM TO REDIRECT-->
                    <div id="hiddenFormToRedirect" style="display: none;"></div>
                </div>
            </div>
            
        </modal>`,

    delimiters: ['#{', '}'],

  components: {
    'message': Message,
    'modal': Modal,
    'loader-content': LoaderContent,
  },

    props: {
        animationId: {type: String, default: ''},
        animationTitle: {type: String, default: ''},
        startDateTime: {type: String, default: ''},
        endDateTime: {type: String, default: ''},
        price: {type: String, default: ''},
        priceWithoutCurrency: {type: String, default: ''},
        token: {type: String, default: null},

        confirmationModalVisible: {type: Boolean, default: false},
    },

    data() {
        return {
            isSendPostAnimation: false,
            isPostAnimationFailed: false,
            isStripeTokenNull: false
        }
    },

    methods: {
        updateModalVisible(isVisible){
            this.$emit('updatemodalvisible', isVisible);
        },

        updateToken(tokenValue){
            this.$emit('updatetoken', tokenValue);
        },

        confirmAnimation() {
            if (window.jwtManager.paymentTypeSelected === 'stripe_dir'
                && (this.priceWithoutCurrency !== '' && this.priceWithoutCurrency !== 0 && this.priceWithoutCurrency !== '0')
                && (this.token === null))
            {
                this.isStripeTokenNull = true;
                return;
            }

            const self = this;
            const url = Routing.generate(
                'todotoday.animation.api.animation.post_participation_animation',
                {animationId: this.animationId, token: this.token},
            );
            this.isStripeTokenNull = false;
            this.isPostAnimationFailed = false;
            this.isSendPostAnimation = true;

            manager.api_post(url).then((response) => {
                // self.isSendPostAnimation = false;
                self.updateToken(null);
                self.redirectToAnimationSuccessPage(response.data);
            }).catch((error) => {
                self.isSendPostAnimation = false;
                self.isPostAnimationFailed = true;
                self.updateToken(null);
                console.log('Error [confirmAnimation]:', error);
            });
        },

        redirectToAnimationSuccessPage(animation) {
            let redirectUrl = Routing.generate('order_checkout_animation_success');

            let form = '<form id="formToRedirect" action="' + redirectUrl + '" method="post">' +
                '<input type="text" name="animationName" value="' + animation['aps_animation1'] + '"/>' +
                '<input type="text" name="animationStartDate" value="' + animation['aps_dbutestim'] + '"/>' +
                '<input type="text" name="animationEndDate" value="' + animation['aps_finestime'] + '"/>' +
                '<input type="text" name="animationPrice" value="' + animation.price + '"/>' +
                '</form>';

            document.getElementById('hiddenFormToRedirect').innerHTML = form;
            document.getElementById('formToRedirect').submit();
        },
    },
};
