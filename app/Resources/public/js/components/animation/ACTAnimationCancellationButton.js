import Message from './../../../../../../vue-src/components/base/ACTMessage'
import LoaderContent from './../../../../../../vue-src/components/base/ACTLoaderContent'

export default {
    template: `<div>
                   <loader-content :is-loading="isCancellingAnimation"
                                   loader-size="small">
                                   
                        <button @click.prevent="cancelAnimation"
                                class="btn btn-action mdi mdi-account-remove">
                                
                                #{ 'animation_unregistration'|$t }
                        </button>
                   </loader-content>
                   
                   <message type="error"
                         :is-visible="cancellationFailed">
                         
                        #{ 'animation.unregistration_failed'|$t }
                    </message>
               </div>`,

    delimiters: ['#{', '}'],

  components: {
    'message': Message,
    'loader-content': LoaderContent,
  },

    props: {
        animationId: {type: String, default: ''},
        routeToRedirect: {type: String, default: 'animation_index'},
        routeOptions: {
            type: Object,
            default: function () {
                return {};
            }
        },
    },

    data() {
        return {
            isCancellingAnimation: false,
            cancellationFailed: false
        }
    },

    methods: {
        cancelAnimation(){
            this.cancellationFailed = false;

            if (this.animationId === '') {
                return;
            }

            this.isCancellingAnimation = true;

            const url = Routing.generate(
                'todotoday.animation.api.animation.put_cancel_participation_animation',
                {animationId: this.animationId},
            );

            let self = this;

            manager.api_put(url).then((response) => {
                let successUrl = Routing.generate(
                    'animation_cancel_success',
                    {
                        route: self.routeToRedirect,
                        routeOptions: self.routeOptions
                    },
                );

                window.location.href = successUrl;

            }).catch((error) => {
                console.log('Error [animation]:', error);
                self.cancellationFailed = true;
                self.isCancellingAnimation = false;
            });
        }
    },
};
