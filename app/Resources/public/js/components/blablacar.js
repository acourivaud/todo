import Vue from 'vue';
import flatPickr from 'vue-flatpickr';
import 'vue-flatpickr/theme/base16_flat.css';
import moment from 'moment';


const app = new Vue({
    el: '#main-blablacar',
    template: '#v-blablacar',
    delimiters: ['${', '}'],
    components: {
        flatPickr
    },
    data: {
        depart: '',
        startDate: '',
        destination: '',
        blablaReturn: {},
        error: '',
        formError: '',
        noResult: false,
        blablakey: 'b20e3d3370bb4826bad7566496716176',
        searching: false,
        flatoptions: {
            // 'utc': true,
            minDate: 'today',
            enableTime: false,
            dateFormat: 'Y-m-d',
            altInput: true,
            altFormat: 'd F Y',
            firstDayOfWeek: 1,
            locale: window.flatpickrLocale,
        },
    },
    mounted() {
        let locale = jwtManager.locale;

        if ( locale === 'fr' ) {
            this.locale = 'fr_FR'
        } else {
            this.locale = 'en_GB'
        }
    },
    methods: {
        searchApiBlablacar() {
            // console.log('je submit')
            this.formError = false
            this.noResult = false
            this.searching = true
            this.error = ''
            if ( !this.depart || !this.destination || !this.startDate ) {
                this.searching = false
                this.formError = true
            } else {
                blablapi.get(`https://public-api.blablacar.com/api/v2/trips?key=${this.blablakey}&fn=${this.depart}&tn=${this.destination}&db=${moment(this.startDate).format('YYYY-MM-DD HH:mm:ss')}&locale=${this.locale}&cur=EUR`)
                    .then(
                        (result) => {
                            // console.log(result)
                            this.noResult = false
                            this.blablaReturn = {}
                            if ( result.data.trips.length === 0 ) {
                                this.noResult = true
                                this.searching = false
                            } else {
                                this.blablaReturn = result.data
                                this.searching = false
                                this.noResult = false
                                this.formError = false
                            }
                        },
                        (error) => {
                            // console.log('Error blablacar API : ', error)
                            this.error = error.response.data
                            this.searching = false
                            this.noResult = false
                            this.formError = false
                        })
            }

        },
    },

})
