const horizontalNav =
  new Vue({
    el: '.td-horizontal-nav',
    data() {
      return {
        open: false,
      };
    },
    methods: {
      menuToggle() {
        this.open = !this.open;
        const container = document.querySelector('.td-horizontal-nav ul');
        this.open ? container.classList.add('open') : container.classList.remove('open');
      },
    },
    delimiters: ['${', '}'],
  });
