Vue.filter('currency', (value, type = 'auto') => {
  type = (type === 'auto') ? window.jwtManager.currencyCode : type;

  if (type === 'EUR') {
    return `${Number(value).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,')} €`;
  } else if (type === 'USD') {
    return `$ ${Number(value).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,')}`;
  } else if (type === 'CHF') {
    return `${Number(value).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,')} CHF`;
  } else if (type === 'CAD') {
    return Number(value).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' $CA';
  }
  return Number(value).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,');
});
