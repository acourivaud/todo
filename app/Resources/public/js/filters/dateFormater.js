var moment = require('moment');
require("moment/min/locales.min");

Vue.filter('dateFormater', function (value, locale = 'fr', format = 'D MMMM YYYY, H:mm') {
    locale = (locale === 'auto') ? window.jwtManager.locale : locale;
    if(locale === 'en'){
        format = 'MMMM Mo, YYYY, h:mm a'
    }
    moment.locale(locale);
    var date = moment(value);
    return date.format(format);
});