class LayoutStore {
    constructor() {
        this.state = {
            notifications: [],
            notificationsStatus: 'loading',
            notificationsCount: 0,
            chatCount: 0,
            cartCount: 0,
            cartTotal: 0,
            cartProducts: [],
            user:{
                name: '',
                fname: ''
            }
        };
    }

    setNotifications(values) {
        this.state.notifications = values;
        this.state.notificationsCount = values.length;
        this.state.notificationsStatus = 'loaded';
    }

    getAll() {
        return this.state.notificationsCount + this.state.cartCount + this.state.chatCount
    }

    setCartProducts(datas) {
        this.state.cartTotal  = 0;
        this.state.cartProducts = datas;
        this.state.cartProducts.forEach((product) => {
            this.state.cartTotal  += (product.quantity * product.amount);
        });
        this.resetCart();
        this.incrementCart(datas.length);
    }

    /**
     * Getter Notification
     * @param nb
     */
    incrementNotification(/** integer */ nb = 1) {
        this.state.notificationsCount += nb;
    }

    /**
     *
     * @param nb
     */
    decrementNotification(/** integer */ nb = 1) {
        this.state.notificationsCount -= nb;
    }

    resetNotification() {
        this.state.notificationsCount = 0;
    }

    markAllNotificationsAsRead() {
       this.state.notifications.forEach(function(el)
       {
         el.is_read = true
       })
      this.resetNotification()
    }

    /**
     *
     * @param nb
     */
    incrementChat(/** integer */ nb = 1) {
        this.state.chatCount += nb;
    }

    /**
     *
     * @param nb
     */
    decrementChat(/** integer */ nb = 1) {
        this.state.chatCount -= nb;
    }

    resetChat() {
        this.state.chatCount = 0;
    }

    /**
     *
     * @param nb
     */
    incrementCart(/** integer */ nb = 1) {
        this.state.cartCount += nb;
    }

    /**
     *
     * @param nb
     */
    decrementCart(/** integer */ nb = 1) {
        this.state.cartCount -= nb;
    }

    resetCart() {
        this.state.cartCount = 0;
    }

}

window.store = new LayoutStore();