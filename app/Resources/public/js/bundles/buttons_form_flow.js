import LoaderContent from './../../../../../vue-src/components/base/ACTLoaderContent'

let formButtons = new Vue({
    el: '#app-btn-formflow',
    components: {
        'loader-content': LoaderContent
    },
    data() {
        return {
            isFormSubmitting: false
        }
    }
});

let formToAddButtonLoader = document.querySelector('#form_btn_loader');
formToAddButtonLoader.addEventListener('submit', function(){
    formButtons.isFormSubmitting = true;
});