new Vue({
    el: '#fgs-app',
    delimiters: ['${', '}'],
    data() {
        return {
            subjectLevelOne: '',
            subjectLevelTwo: '',

            listSubjectsHierarchy: [],
            subjectsLevelTwoList: [],
        }
    },
    created() {
        this.listSubjectsHierarchy = JSON.parse(
            document.getElementById('fgs_listSubjectsHierarchy')
                .getAttribute('hierarchy')
        );
    },
    methods: {
        getSubjectsLevelTwoList() {
            for(let i = 0, length = this.listSubjectsHierarchy.length; i < length; i++) {
                if(parseInt(this.subjectLevelOne, 10) === this.listSubjectsHierarchy[i]['value']){
                    this.subjectsLevelTwoList = this.listSubjectsHierarchy[i]['childs'];
                    return;
                }
            }

            this.subjectsLevelTwoList = [];
        }
    },
    computed: {
        subjectLevelOnehasValue() {
            return this.subjectLevelOne !== '';
        },
        hasLevelTwoList() {
            return (this.subjectsLevelTwoList.length > 0);
        },
    },
    watch: {
        subjectLevelOne(newValue, oldValue){
            if (newValue) {
                this.getSubjectsLevelTwoList();
            }
        }
    }
});