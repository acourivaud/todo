import Message from './../../../../../vue-src/components/base/ACTMessage'
import Loader from './../../../../../vue-src/components/base/ACTLoader'
import OrderRatingModal from './../components/order/ACTOrderRatingModal'
import JWTManager from './../../../../../vue-src/manager/JWTManager';

let moment = require('moment'),
    momentLocales = require("moment/min/locales.min");

new Vue({
    el: '#app',
    delimiters: ['${', '}'],
    components: {
      'message': Message,
      'loader': Loader,
      'order-rating-modal': OrderRatingModal
    },
    data() {
        return {
            orders: [],
            isLoadingOrders: false,
            getOrderFailed: false,
            orderSelected: {},

            today: {},
            minYear: 2017,
            maxYear: 2017,
            maxMonth: 0,
            availableYears: [],
            availableMonths: [],
            currentMonthYear: '',

            ratingModalVisible: false,
        }
    },
    created() {
        moment.locale(window.jwtManager.locale);
        this.today = moment();
        this.maxYear = this.today.year();
        this.maxMonth = moment(this.today).format('MM');
        this.currentMonthYear = this.maxYear + '-' + this.maxMonth;
        this.createAvailableYears();
        this.createAllMonthsOfYear();

        this.getHistoryOrders();
    },
    methods: {
        getHistoryOrders() {
            this.isLoadingOrders = true;
            this.getOrderFailed = false;
            let self = this;

            let url = Routing.generate(
                'todotoday.checkout.api.order.get_order_history_month',
                {monthYear: this.currentMonthYear}
            );

            manager.api_get(url, {}
            ).then(function (response) {
                self.isLoadingOrders = false;
                self.orders = (response.data !== '') ? response.data : [];
            }).catch(function (error) {
                self.isLoadingOrders = false;
                self.getOrderFailed = true;
                console.log('error:', error);
            });
        },
        openRatingModal(order) {
            this.orderSelected = order;
            this.ratingModalVisible = true;
        },
        orderRated() {
            this.$set(this.orderSelected, 'ratable', false);
            this.$set(this.orderSelected, 'order_status', $t('order.status_rated'));
            this.orderSelected = {};
        },
        createAvailableYears() {
            let years = [];
            for (let year = this.maxYear; year >= this.minYear; year--) {
                years.push(year);
            }
            this.availableYears = years;
        },
        createAllMonthsOfYear() {
            let months = [],
                firstMonth = moment().month(0).date(1);
            for (let i = 11; i >= 0; i--) {
                let month = moment(firstMonth).add(i, 'M');

                months.push({
                    'value': month.format('MM'),
                    'name': month.format('MMMM')
                });
            }
            this.availableMonths = months;
        },
        isDateValid(year, month){
            if(year < this.maxYear){
                return true;
            }
            return (Number(month) <= Number(this.maxMonth));
        },
    },
    computed: {
        salesOrderNumber () {
            return (this.orderSelected.hasOwnProperty('sales_order_number'))
                ? this.orderSelected['sales_order_number'] : '';
        },
        noOrders(){
            return (this.orders.length === 0) && !this.isLoadingOrders;
        },
        dateTitleFormat(){
            return window.$t('order.date_title_format');
        },
        isCoAdherent() {
          return JWTManager.isCoAdherent;
        },
    },
    watch: {
        currentMonthYear(newValue, oldValue) {
            if (oldValue !== '') {
                this.getHistoryOrders();
            }
        },
    }
});