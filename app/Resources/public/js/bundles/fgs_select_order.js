let moment = require('moment'),
    momentLocales = require("moment/min/locales.min");

new Vue({
    el: '#app',
    delimiters: ['${', '}'],
    data() {
        return {
            orders: [],
            isLoadingOrders: false,
            getOrderFailed: false,
            orderSelected: {},

            today: {},
            minYear: 2017,
            maxYear: 2017,
            maxMonth: 0,
            availableYears: [],
            availableMonths: [],
            currentMonthYear: '',
        }
    },
    created() {
        moment.locale(window.jwtManager.locale);
        this.today = moment();
        this.maxYear = this.today.year();
        this.maxMonth = moment(this.today).format('MM');
        this.currentMonthYear = this.maxYear + '-' + this.maxMonth;
        this.createAvailableYears();
        this.createAllMonthsOfYear();

        this.getAllOrders();
    },
    methods: {
        getAllOrders() {
            this.isLoadingOrders = true;
            this.getOrderFailed = false;
            let self = this;

            let url = Routing.generate(
                'todotoday.checkout.api.order.get_order_month',
                {monthYear: this.currentMonthYear}
            );

            manager.api_get(url, {}
            ).then(function (response) {
                console.log(response);
                self.isLoadingOrders = false;
                self.orders = (response.data !== '') ? response.data : [];
            }).catch(function (error) {
                self.isLoadingOrders = false;
                self.getOrderFailed = true;
                console.log('error:', error);
            });
        },
        selectOrder(order) {
            this.orderSelected = order;

            let url = Routing.generate(
                'fgs_new',
                {orderId: this.orderSelected['sales_order_number']}
            );

            window.location.href = url;
        },


        createAvailableYears() {
            let years = [];
            for (let year = this.maxYear; year >= this.minYear; year--) {
                years.push(year);
            }
            this.availableYears = years;
        },
        createAllMonthsOfYear() {
            let months = [],
                firstMonth = moment().month(0).date(1);
            for (let i = 11; i >= 0; i--) {
                let month = moment(firstMonth).add(i, 'M');

                months.push({
                    'value': month.format('MM'),
                    'name': month.format('MMMM')
                });
            }
            this.availableMonths = months;
        },
        isDateValid(year, month){
            if(year < this.maxYear){
                return true;
            }
            return (Number(month) <= Number(this.maxMonth));
        },
    },
    computed: {
        noOrders(){
            return (this.orders.length === 0) && !this.isLoadingOrders;
        },
        dateTitleFormat(){
            return window.$t('order.date_title_format');
        }
    },
    watch: {
        currentMonthYear(newValue, oldValue) {
            if (oldValue !== '') {
                this.getAllOrders();
            }
        },
    }
});