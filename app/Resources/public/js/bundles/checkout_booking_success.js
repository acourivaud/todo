import BookingNotifyWishSlot from './../../../../../vue-src/components/bookingCalendar/ACTBookingNotifyWishSlot'

new Vue({
    el: '#app-notify',
    delimiters: ['${', '}'],
    components: {
      'booking-notify-wish-slot': BookingNotifyWishSlot
    }
});