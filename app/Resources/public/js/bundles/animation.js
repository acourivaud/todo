import AnimationCancellationButton from './../components/animation/ACTAnimationCancellationButton'
import AnimationConfirmationModal from './../components/animation/ACTAnimationConfirmationModal'

new Vue({
    el: '#app',
    props: ['body'],
    delimiters: ['${', '}'],
    components: {
        'animation-confirmation-modal': AnimationConfirmationModal,
        'animation-cancellation-button': AnimationCancellationButton,
    },
    data() {
        return {
            id: '',
            title: '',
            start: '',
            end: '',
            price: '',
            priceWithoutCurrency: '',
            token: null,
            confirmationModalVisible: false,
        };
    },
    methods: {
        openConfirmationModal(obj) {
            this.id = obj.id;
            this.title = obj.title;
            this.start = obj.start;
            this.end = obj.end;
            this.price = obj.priceWithCurrency;
            this.priceWithoutCurrency = obj.price;
            this.token = null;
            this.confirmationModalVisible = true;
        }
    },
    computed: {
        animationIsFree() {
            return this.priceWithoutCurrency === '' || this.priceWithoutCurrency === 0 || this.priceWithoutCurrency === '0';
        }
    }
});
