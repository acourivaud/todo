const cards = [
  {
    type: 'amex',
    icon: 'p-mastercard',
    pattern: /^3[47]/,
    format: /(\d{1,4})(\d{1,6})?(\d{1,5})?/,
    length: [15],
    cvcLength: [4],
    luhn: true,
  }, {
    type: 'dankort',
    pattern: /^5019/,
    length: [16],
    cvcLength: [3],
    luhn: true,
  }, {
    type: 'dinersclub',
    icon: 'p-diners-club',
    pattern: /^(36|38|30[0-5])/,
    format: /(\d{1,4})(\d{1,6})?(\d{1,4})?/,
    length: [14],
    cvcLength: [3],
    luhn: true,
  }, {
    type: 'discover',
    icon: 'p-discover',
    pattern: /^(6011|65|64[4-9]|622)/,
    length: [16],
    cvcLength: [3],
    luhn: true,
  }, {
    type: 'jcb',
    icon: 'p-jcb',
    pattern: /^35/,
    length: [16],
    cvcLength: [3],
    luhn: true,
  }, {
    type: 'laser',
    pattern: /^(6706|6771|6709)/,
    length: [16, 17, 18, 19],
    cvcLength: [3],
    luhn: true,
  }, {
    type: 'maestro',
    icon: 'p-maestro',
    pattern: /^(5018|5020|5038|6304|6703|6708|6759|676[1-3])/,
    length: [12, 13, 14, 15, 16, 17, 18, 19],
    cvcLength: [3],
    luhn: true,
  }, {
    type: 'mastercard',
    icon: 'p-mastercard',
    pattern: /^(5[1-5]|677189)|^(222[1-9]|2[3-6]\d{2}|27[0-1]\d|2720)/,
    length: [16],
    cvcLength: [3],
    luhn: true,
  }, {
    type: 'unionpay',
    icon: 'p-unionpay',
    pattern: /^62/,
    length: [16, 17, 18, 19],
    cvcLength: [3],
    luhn: false,
  }, {
    type: 'visaelectron',
    icon: 'p-visa-electron',
    pattern: /^4(026|17500|405|508|844|91[37])/,
    length: [16],
    cvcLength: [3],
    luhn: true,
  }, {
    type: 'elo',
    icon: 'p-elo2',
    pattern: /^(4011|438935|45(1416|76|7393)|50(4175|6699|67|90[4-7])|63(6297|6368))/,
    length: [16],
    cvcLength: [3],
    luhn: true,
  }, {
    type: 'visa',
    icon: 'p-visa',
    pattern: /^4/,
    length: [13, 16, 19],
    cvcLength: [3],
    luhn: true,
  },
];

function initializeStripeData() {
  return {
    messageError: null,
    cardCreated: false,
    number: null,
    cvc: null,
    month: null,
    year: null,
    cardType: null,
    cards,
  };
}

Vue.component('stripe', {
  data() {
    return initializeStripeData();
  },
  props: {
    value: {//value is the token. We use 'value' instead of 'token' so it works with v-model=""
      type: String,
      default: null,
    },
    public_key: {
      type: String,
    },
  },
  watch: {
    value(value) {
      if (value === null) {
        const data = initializeStripeData();
        let key;
        for (key in data) {
          this[key] = data[key];
        }
      }
    },
  },
  delimiters: ['${', '}'],
  computed: {
    cardTypeSelector() {
      this.cards.forEach((card) => {
        if (card.pattern.test(this.number)) {
          this.cardType = card.icon;
        }
      });
      return this.cardType;
    },
    formatedNumber() {
      if (this.number) {
        return this.chunk(this.number, 4).join(' ');
      }
      return '**** **** **** ****';
    },
    formatedMonth() {
      if (this.month) {
        return this.month;
      }
      return '**';
    },
    formatedYear() {
      if (this.year) {
        return this.year;
      }
      return '**';
    },
  },
  methods: {
    chunk(str, n) {
      if (str || str !== null) {
        const ret = [];
        let i;
        let len;

        for (i = 0, len = str.length; i < len; i += n) {
          ret.push(str.substr(i, n));
        }
        return ret;
      }
      return null;
    },
    createCard() {
      const $this = this;
      if (this.number && this.cvc && this.month && this.year) {
        this.cardCreated = null;
        Stripe.setPublishableKey(this.public_key);
        Stripe.card.createToken({
          number: this.number,
          cvc: this.cvc,
          exp_month: this.month,
          exp_year: this.year,
        }, (status, response) => {
          if (response.error) {
            $this.cardCreated = false;
            $this.messageError = response.error.message;
          } else {
            $this.cardCreated = true;
            $this.$emit('input', response.id);
          }
        });
      } else {
        this.messageError = 'All fields must be filled';
      }
    },
  },
  template: '#stripe-template',
});
