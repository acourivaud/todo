import OrderCancelModal from './../components/order/ACTOrderCancelModal'
import OrderRatingModal from './../components/order/ACTOrderRatingModal'
import OrderEditModal from '../components/order/ACTOrderEditModal'

new Vue({
    el: '#app',
    delimiters: ['${', '}'],
    components: {
        'order-cancel-modal': OrderCancelModal,
        'order-rating-modal': OrderRatingModal,
        'order-edit-modal': OrderEditModal
    },
    data: function () {
        return {
            order: {},
            lines: [],
            ratingModalVisible: false,
            annulationModalVisible: false,
            editModalVisible: false,
            cancelModalBodyMessage: ''
        }
    },
    methods: {
        openRatingModal: function (salesOrderNumber) {
            this.$set(this.order, 'salesOrderNumber', salesOrderNumber);
            this.ratingModalVisible = true;
        },
        orderRated() {
            this.hideRateButton(this.order.salesOrderNumber);
            this.setOrderStatus(this.order.salesOrderNumber, $t('order.status_rated'));
            this.order = {};
        },

        openAnnulationModal(salesOrderNumber, cancelBodyMessage) {
            this.cancelModalBodyMessage = cancelBodyMessage
            this.$set(this.order, 'salesOrderNumber', salesOrderNumber);
            this.annulationModalVisible = true;
        },
        orderCanceled() {
            this.setSectionClass(this.order.salesOrderNumber, 'canceled');
            this.hideButtons(this.order.salesOrderNumber);
            this.setOrderStatus(this.order.salesOrderNumber, $t('order.status_cancelled'));
            this.annulationModalVisible = false;
            this.order = {};
        },

        openEditModal(salesOrderNumber, dataAreaId, lockerNumber, orderLinesSumup) {
            this.$set(this.order, 'salesOrderNumber', salesOrderNumber)
            this.$set(this.order, 'lockerNumber', lockerNumber)
            this.$set(this.order, 'dataAreaId', dataAreaId)
            this.editModalVisible = true
            this.lines = orderLinesSumup
        },

        orderEdited() {
          let orderPending = Routing.generate('order.pending');

          window.location.href = orderPending
        },


        getOrderSectionId(salesOrderNumber) {
            return 'order-section-' + salesOrderNumber;
        },
        getOrderSectionElement(salesOrderNumber) {
            return document.getElementById(this.getOrderSectionId(salesOrderNumber));
        },
        setSectionClass(salesOrderNumber, className) {
            this.getOrderSectionElement(salesOrderNumber).className = className;
        },
        getRateButton(salesOrderNumber) {
            let selector = '#' + this.getOrderSectionId(salesOrderNumber) + ' .cmd-actions .btn-rate';
            return document.querySelector(selector);
        },

        setOrderStatus(salesOrderNumber, status) {
            let selector = '#' + this.getOrderSectionId(salesOrderNumber) + ' .order-status';
            return document.querySelector(selector).innerHTML = status;
        },

        hideRateButton(salesOrderNumber) {
            this.getRateButton(salesOrderNumber).style.display = 'none';
        },
        hideButtons(salesOrderNumber) {
            let selector = '#' + this.getOrderSectionId(salesOrderNumber) + ' .cmd-actions';
            document.querySelector(selector).style.display = 'none';
        },
    },
    computed: {
        salesOrderNumber() {
            return (this.order.hasOwnProperty('salesOrderNumber')) ? this.order.salesOrderNumber : '';
        },
        lockerNumber() {
            return (this.order.hasOwnProperty('lockerNumber')) ? this.order.lockerNumber : ''
        },
        dataAreaId() {
            return (this.order.hasOwnProperty('dataAreaId')) ? this.order.dataAreaId : ''
        }
    },
});