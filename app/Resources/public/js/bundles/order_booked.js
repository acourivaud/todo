import OrderCancelModal from './../components/order/ACTOrderCancelModal'
import OrderDisplayBooked from './../components/order/PIXDisplayBookedOrder'

new Vue({
    el: '#app',
    delimiters: ['${', '}'],
    components: {
      'order-cancel-modal': OrderCancelModal,
      'order-display-booked': OrderDisplayBooked,
    },
    data: function () {
        return {
            order: {},
            annulationModalVisible: false,
        }
    },
    methods: {
        openAnnulationModal: function (salesOrderNumber) {
            this.annulationModalVisible = true;
            this.$set(this.order, 'salesOrderNumber', salesOrderNumber);
        },

        getOrderSectionId: function (salesOrderNumber) {
            return 'order-section-' + salesOrderNumber;
        },
        getOrderSectionElement: function (salesOrderNumber) {
            return document.getElementById(this.getOrderSectionId(salesOrderNumber));
        },
        setSectionClass: function (salesOrderNumber, className) {
            this.getOrderSectionElement(salesOrderNumber).className = className;
        },

        setOrderStatus: function (salesOrderNumber, status) {
            let selector = '#' + this.getOrderSectionId(salesOrderNumber) + ' .order-status';
            return document.querySelector(selector).innerHTML = status;
        },
        hideButtons: function (salesOrderNumber) {
            let selector = '#' + this.getOrderSectionId(salesOrderNumber) + ' .cmd-actions';
            document.querySelector(selector).style.display = 'none';
        },
        orderCanceled() {
            this.setSectionClass(this.order.salesOrderNumber, 'canceled');
            this.hideButtons(this.order.salesOrderNumber);
            this.setOrderStatus(this.order.salesOrderNumber, $t('order.status_cancelled'));
            this.annulationModalVisible = false;
            this.order = {};
        },
    },
    computed: {
        salesOrderNumber() {
            return (this.order.hasOwnProperty('salesOrderNumber')) ? this.order.salesOrderNumber : '';
        },
    },
});
