manager.api_get('/notification/me').then((response) => {
  // console.log(response);
  store.setNotifications(response.data);
},
);
manager.api_get('/cart/products/me', {
  headers: {'Language': jwtManager.locale}
}).then((response) => {
  store.setCartProducts(response.data);
}).catch((error) => {
  console.log(error);
});
