Vue.component('chat-counter', {
  data() {
    return {
      state: store.state,
    };
  },
  delimiters: ['${', '}'],
  template: '<div class="td-counter"> ${ state.chatCount } </div> ',
});

Vue.component('notification-counter', {
  data() {
    return {
      state: store.state,
    };
  },
  computed: {
    totalNotification() {
      if (this.state.notifications !== '') {
        return this.state.notifications.filter(element => !element.is_read).length;
      } else {
        return 0;
      }
    },
  },
  delimiters: ['${', '}'],
  template: '<div class="td-counter"> ${ totalNotification } </div>',
});

Vue.component('notification-box', {
  props: {
    notification: {
      type: Object,
      default() {
        return { empty: true };
      },
    },
  },
  data() {
    return {
      notificationData: this.notification
    }
  },
  delimiters: ['${', '}'],
  template: '#notification-box',
  methods: {
    markAsRead() {
      self = this;
      let url = Routing.generate(
        'todotoday.notification.api.notification.put_read',
        { apsNotificationId: this.notificationData.aps_notificationid }
      );
      self.notificationData.is_read = true;
      manager.api_put(url)
        .catch((error) => {
          console.log('error :', error);
          self.notificationData.is_read = false
        });
    },
  },
  computed: {
    notificationMessage() {
      let messageKey = `aps_message${window.jwtManager.locale}`,
        defaultKey = 'aps_messageen';
      if (this.notificationData.hasOwnProperty(messageKey)) {
        return this.notificationData[messageKey];
      } else if (this.notificationData.hasOwnProperty(defaultKey)) {
        return this.notificationData[defaultKey];
      } else if (!this.notificationData.hasOwnProperty('empty')) {
        console.log(
          `[ERROR Notification]: no key '${messageKey}' or '${
            defaultKey}' for notification in sidebarCounters.js`, this.notificationData);
      }
      return '';
    },

    isReaded() {
      return this.notificationData.is_read;
    },
  },
});

const notif =
  new Vue({
    el: '#side-notification-component',
    data() {
      return {
        state: store.state,
      };
    },
    computed: {
      totalNotification() {
        return this.state.notifications.filter(element => !element.is_read).length;
      },
    },
    delimiters: ['${', '}'],
    methods: {
      markAllAsRead() {
        let url = Routing.generate('todotoday.notification.api.notification.post_read_all');
        console.log(url);
        store.markAllNotificationsAsRead();
        manager.api_post(url)
          .catch((error) => {
            console.log('error :', error);
          });
      },
    },
    template: '#notification-display',
  });

new Vue({
    el: '#order-to-rate-app',
    delimiters: ['${', '}'],
    data() {
        return {
            numberOrdersToRate: 0
        }
    },
    created() {
        let url = Routing.generate('todotoday.checkout.api.order.get_number_ratable_order');

        manager.api_get(url)
            .then((response) => {
                this.numberOrdersToRate = response.data;
            });
    }
});
