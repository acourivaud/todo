new Vue({
  el: '#order-refused-app',
  delimiters: ['${', '}'],
  data() {
    return {
      numberOrdersRefused: 0
    }
  },
  created() {
    let url = Routing.generate('todotoday.checkout.api.order.get_number_refused_order');

    manager.api_get(url)
      .then((response) => {
        this.numberOrdersRefused = response.data;
      });
  },
  computed: {
    hasOneOrderRefusedAtLeast() {
      return this.numberOrdersRefused > 0
    }
  }
});
