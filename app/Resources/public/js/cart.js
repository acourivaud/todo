Vue.prototype.$isDisplayPriceExclTax = function() {
  let isDisplayPriceExclTax = false;
  let displayPriceElement = document.getElementById('getDisplayPriceExclTax');

  if (displayPriceElement && displayPriceElement.getAttribute('price-excl-tax') &&
    (displayPriceElement.getAttribute('price-excl-tax') !== '')) {
    isDisplayPriceExclTax = JSON.parse(displayPriceElement.getAttribute('price-excl-tax'));
  }

  return isDisplayPriceExclTax;
}

Vue.component('cart-item', {
  props: ['product', 'index'],
  data: function () {
    return {
      state: store.state,
      animatedNumber: 0,
    }
  },
  computed: {
    productTotal: function () {
      if(this.$isDisplayPriceExclTax()){
        return this.product.amount_excl_tax * this.product.quantity
      }
      return this.product.amount * this.product.quantity
    },
    productPrice: function () {
      if(this.$isDisplayPriceExclTax()){
        return this.product.amount_excl_tax
      }
      return this.product.amount
    }
  },
  delimiters: ['${', '}'],
  template: '#actiane-cart-item'
})

Vue.component('actiane-cartdel', {
  props: ['productid'],
  delimiters: ['${', '}'],
  template: '#actiane-deleter',
  methods: {
    deleteProduct: function () {
      let self = this
      let idDiv = 'product-cart-' + this.productid
      manager.api_delete('/cart/products/me/' + this.productid, {}
      ).then(function (response) {
        store.setCartProducts(response.data)
        if (reponse.data.length > 0) {
          location.reload()
        }
      }).catch(function (error) {
        console.log(error)
      })
    }
  }
})

Vue.component('actiane-quantifier', {
  props: ['quantity', 'productId', 'index'],
  data: function () {
    return {
      state: store.state,
      status: 'loading',
    }
  },
  mounted: function () {
    this.status = 'OK'
  },
  delimiters: ['${', '}'],
  template: '#actiane-quantifier',
  methods: {
    decrementItems: function () {
      if (this.status === 'OK' && this.quantity > 1) {
        this.status = 'loading'
        let quantityToPut = this.quantity
        quantityToPut--
        this.putProductQuantity(this.productId, quantityToPut, 'increment')
        this.quantity--
      }
    },
    incrementItems: function () {
      if (this.status === 'OK') {
        this.status = 'loading'
        let quantityToPut = this.quantity
        quantityToPut++
        this.putProductQuantity(this.productId, quantityToPut, 'decrement')
        this.quantity++
      }
    },
    putProductQuantity(product, valueToPut, status){
      let self = this
      var params = new URLSearchParams()
      params.append('product', product)
      params.append('quantity', valueToPut)

      manager.api_put('/cart/products/me', params, {}
      ).then(function (response) {
        store.state.cartProducts = response.data
        self.status = 'OK'
      }).catch(function (error) {
        console.log(error)
        self.status = 'OK'
      })

      return valueToPut
    },
    removeProductQuantity(product){
      if (this.quantity > 0) {
        let self = this
        manager.api_delete('/cart/products/me/' + product.id, {}
        ).then(function (response) {

          console.log('Remove product quantity : ', response)
          self.products.setProductsQuantityWithDatas(response.data)

        }).catch(function (error) {
          console.log(error)
        })
      }
    }
  }
})

var fwCart =
  new Vue({
    el: '.td-cart',
    data: function () {
      return {
        state: store.state,
      }
    },
    computed: {
      totalCart: function () {

        return this.calculateCartTotal()
      }
    },
    methods: {
      calculateCartTotal: function () {
        let cartTotal = 0
        this.state.cartProducts.forEach((product) => {
          cartTotal += ( this.$isDisplayPriceExclTax() ? product.amount_excl_tax : product.amount) * product.quantity
        })

        return cartTotal
      }
    },
    delimiters: ['${', '}'],
  })
