const fwCart =
  new Vue({
    el: '.sidebar-toggle',
    data() {
      return {
        open: true,
      };
    },
    mounted(){
      if( window.innerWidth < 1040 && document.getElementById('rs-content')){
        this.open = false;
        const container = document.getElementById('rs-content');
        container.className = 'rs-content replie';
      }
    },
    methods: {
      menuSwitcher() {
        this.open = !this.open;
        const container = document.getElementById('rs-content');
        this.open ? container.className = 'rs-content sidebaropen' : container.className = 'rs-content replie';
      },
    },
    computed: {
      arrowClass() {
        return this.open ? 'mdi-arrow-right' : 'mdi-arrow-left';
      },
    },
    delimiters: ['${', '}'],
  });


const copyMainMenu =
  new Vue({
    el: '.header-bar-menu',
    data() {
    },
    mounted(){
      const mainNav = document.querySelector('.header-bar-menu');
      if(mainNav){
        const copy = mainNav.cloneNode(true);
        copy.className = "side-main-menu";
        if(document.querySelector('.right-sidebar')){
          document.querySelector('.right-sidebar').prepend(copy);
        }else{
          const rsContent = document.createElement('div');
          rsContent.className = "rs-content hide-desktop";
          rsContent.id = "rs-content";
          const aside = document.createElement('aside');
          aside.className = "right-sidebar";
          aside.append(copy);
          rsContent.append(aside);
          document.querySelector('.body-content').append(rsContent)
  
        }
      }
    },
    delimiters: ['${', '}'],
  });