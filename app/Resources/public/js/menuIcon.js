Vue.component('menu-element-ts', {
  props: {
    text: {type: String},
    icon: {type: String},
    counter: {type: Number},
    url: {type: String},
    targetBlank: {type: Boolean, default: false}
  },
  data: function () {
    return {
      state: store.state
    }
  },
  computed: {
    target() {
      return this.targetBlank ? '_blank' : '_self'
    }
  },
  delimiters: ['${', '}'],
  template: '<li><a :href="url" :target="target"><i class="mdi" v-bind:class="icon"></i><div v-if="counter > 0" class="td-counter-top-right"> ${ counter } </div> ${ text }</a> </li>'
});

Vue.component('menu-element-cart', {
  data: function () {
    return {
      drop_state: false,
      state: store.state
    }
  },
  delimiters: ['${', '}'],
  props: ['url', 'title'],
  methods: {
    test() {
      this.drop_state = !this.drop_state
    },
  },
  template: 
  '   <menu-element-ts :url="url" :text="title" :counter="state.cartCount" icon="mdi-basket">' +
  '   </menu-element-ts>'
});

var menuElement =
  new Vue({
    el: '#menu-element',
  });