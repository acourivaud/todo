import LoaderContent from './../../../../vue-src/components/base/ACTLoaderContent'
import Message from './../../../../vue-src/components/base/ACTMessage'
import Flatpickr from 'vue-flatpickr'
import moment from 'moment'
import readableDate from './../../../../vue-src/filters/readableDate'

/* #########################################################
 ####################### CHILDS ########################
 ######################################################### */

Vue.component('vExtraChild', {
  props: ['child'],
  delimiters: ['${', '}'],
  template: '#vt-extra-child',
  components: {
    'loader-content': LoaderContent,
  },
  filters: {
    'readableDate': readableDate
  },
  data: function () {
    return {
      locale: jwtManager.locale,
      edition: false,
      birthDate: null,
      firstName: this.child.first_name,
      birthDay: this.child.birth_day,
      birthMonth: this.child.birth_month,
      birthYear: this.child.birth_year,
      isSendingData: false,
      isDeletingData: false
    }
  },
  mounted() {
    this.birthDate = moment()
    this.birthDate.year(this.birthYear)
    this.birthDate.month(this.birthMonth)
    this.birthDate.date(this.birthDay)
  },
  methods: {
    editSwitch(bool) {
      this.edition = bool
    },
    sendData() {
      this.isSendingData = true;
      manager.api_put('/adherent/contact_person/' + this.child.contact_person_id,
        {
          firstName: this.child.first_name,
          birthDay: this.child.birth_day,
          birthMonth: this.child.birth_month,
          birthYear: this.child.birth_year,
        }
      ).then((response) => {
        this.isSendingData = false;
      }).catch((error) => {
        this.isSendingData = false;
        console.log('error : ', error)
      })
    },
    deleteData() {
      this.$emit('startdelete')

      this.isDeletingData = true;
      manager.api_delete('/adherent/contact_person/' + this.child.contact_person_id)
        .then((response) => {
          this.isDeletingData = false;
          this.$emit('deleted');
        })
        .catch((error) => {
          this.isDeletingData = false;
          console.log('error : ', error)
        })
    },
    resetForm() {
      this.child.first_name = this.firstName;
      this.child.birth_day = this.birthDay;
      this.child.birth_month = this.birthMonth;
      this.child.birth_year = this.birthYear;

      this.editSwitch(false)
    }
  }
});

Vue.component('vCollectionAdd', {
  delimiters: ['${', '}'],
  template: '#vt-collection-add',
  components: {
    'loader-content': LoaderContent,
    Flatpickr,
  },
  data: function () {
    return {
      childrens: [],
      edition: false,
      dateFormat: {
        // 'utc': true,
        altFormat: 'd F Y',
        dateFormat: 'Y-m-d',
        firstDayOfWeek: 1,
        locale: window.flatpickrLocale,
      },
      firstName: '',
      birthdate: '',
      isSendingData: false,
    }
  },
  methods: {
    editSwitch() {
      this.firstName = '';
      this.birthdate = '';
      this.edition = !this.edition
    },
    sendData() {
      if (this.firstName.trim() === '' || this.birthdate === '') {
        if (this.firstName.trim() === '') {
          document.getElementById('extra-child-firstname').focus()
        } else {
          document.getElementById('extra-child-birthdate').focus()
        }
        return;
      }

      this.isSendingData = true;

      let birthSplit = this.birthdate.split('-')

      manager.api_post('/adherent/contact_person',
        {
          firstName: this.firstName.trim(),
          birthDay: birthSplit[2],
          birthMonth: birthSplit[1],
          birthYear: birthSplit[0],
          contactType: 'Children',
        }
      ).then((response) => {
        this.isSendingData = false;
        this.$emit('new', response.data);
        this.edition = false
      }).catch((error) => {
        this.isSendingData = false;
        console.log('error : ', error)
      })
    },
  }
});

/* #########################################################
 ####################### ASSISTANTS ########################
 ######################################################### */

Vue.component('vExtraAssistant', {
  props: ['assistant'],
  delimiters: ['${', '}'],
  template: '#vt-extra-assistant',
  components: {
    'loader-content': LoaderContent,
  },
  data: function () {
    return {
      edition: false,
      firstName: this.assistant.first_name,
      lastName: this.assistant.last_name,
      email: this.assistant.primary_email_address,
      phone: this.assistant.primary_phone_number,
      isSendingData: false,
      isDeletingData: false
    }
  },
  methods: {
    editSwitch(bool) {
      this.edition = bool
    },
    sendData() {
      this.isSendingData = true;
      manager.api_put('/adherent/contact_person/' + this.assistant.contact_person_id,
        {
          firstName: this.assistant.first_name,
          lastName: this.assistant.last_name,
          primaryEmailAddress: this.assistant.primary_email_address,
          primaryPhoneNumber: this.assistant.primary_phone_number,
        }
      ).then((response) => this.isSendingData = false)
        .catch((error) => {
          this.isSendingData = false;
          console.log('error : ', error)
        })
    },
    deleteData() {
      this.$emit('startdelete')

      this.isDeletingData = true;
      manager.api_delete('/adherent/contact_person/' + this.assistant.contact_person_id)
        .then((response) => {
          this.isDeletingData = false;
          this.$emit('deleted');
        })
        .catch((error) => {
          this.isDeletingData = false;
          console.log('error : ', error)
        })
    },
    resetForm() {
      this.assistant.first_name = this.firstName;
      this.assistant.last_name = this.lastName;
      this.assistant.primary_email_address = this.primaryEmailAddress;
      this.assistant.phone = this.primaryPhoneNumber;

      this.editSwitch(false)
    }
  }
});

Vue.component('vCollectionAddAssistants', {
  delimiters: ['${', '}'],
  template: '#vt-collection-add-assistants',
  components: {
    'loader-content': LoaderContent,
  },
  data: function () {
    return {
      childrens: [],
      edition: false,
      firstName: '',
      lastName: '',
      email: '',
      phone: '',
      isSendingData: false,
    }
  },
  methods: {
    editSwitch() {
      this.firstName = '';
      this.lastName = '';
      this.email = '';
      this.phone = '';
      this.edition = !this.edition
    },
    sendData() {

      if (this.firstName.trim() === '' || this.lastName.trim() === '' || this.email.trim() === '') {
        if (this.firstName.trim() === '') {
          document.getElementById('extra-child-firstname').focus()
        } else if(this.lastName.trim() === '') {
          document.getElementById('extra-child-lastname').focus()
        } else if(this.email.trim() === '') {
          document.getElementById('extra-child-email').focus()
        }
        return;
      }

      this.isSendingData = true;
      manager.api_post('/adherent/contact_person',
        {
          firstName: this.firstName.trim(),
          lastName: this.lastName.trim(),
          primaryEmailAddress: this.email.trim(),
          primaryPhoneNumber: this.phone,
          contactType: 'Assistant',
        }
      ).then((response) => {
        this.isSendingData = false;
        this.$emit('new', response.data);
        this.edition = false
      }).catch((error) => {
        this.isSendingData = false;
        console.log('error : ', error)
      })
    },
  }
});

const extraProfile = new Vue(
  {
    el: '#v-extra-profile',
    delimiters: ['${', '}'],
    template: '#vt-extra-profile',
    components: {
      'message': Message,
    },
    data() {
      return {
        extra_data: {
          contact_children: [],
          contact_assistants: [],
          loading: true,
        },
        hasDeletedChild: false,
        hasDeletedAssistant: false,
      }
    },
    computed: {
      children() {
        return this.extra_data.contact_children
      },
      assistants() {
        return this.extra_data.contact_assistants
      },
    },
    methods: {
      pushChild(child) {
        this.extra_data.contact_children = this.extra_data.contact_children || [];
        this.extra_data.contact_children.push(child)
      },
      pushcontact(contact) {
        this.extra_data.contact_assistants = this.extra_data.contact_assistants || [];
        this.extra_data.contact_assistants.push(contact)
      },
      removeChild(index) {
        this.extra_data.contact_children.splice(index, 1);
        this.hasDeletedChild = true

      },
      removeAssistant(index) {
        this.extra_data.contact_assistants.splice(index, 1);
        this.hasDeletedAssistant = true
      }
    },
    mounted() {
      manager.api_get('/adherent/contact_person/extra').then(
        (response) => {
          this.extra_data = response.data;
          this.extra_data.loading = false
        }).catch((error) => {
        this.extra_data.loading = 'error'
      })
    },
  }
);
