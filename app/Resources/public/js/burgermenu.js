const burgerMenu =
  new Vue({
    el: '.burger-toggle',
    data() {
      return {
        openMobile: false,
      };
    },
    mounted(){
      var vh = window.innerHeight * 0.01;
      document.documentElement.style.setProperty('--vh', `${vh}px`);
      document.addEventListener('scroll', () => {
        var vh = window.innerHeight * 0.01;
        document.documentElement.style.setProperty('--vh', `${vh}px`);
      })
    },
    methods: {
      burgerSwitcher() {
        this.openMobile = !this.openMobile;
        const container = document.querySelector('#rs-content');
        this.openMobile ? container.className = 'rs-content sidebaropen' : container.className = 'rs-content replie';
        this.openMobile ? document.body.classList.add('sidebaropen') : document.body.classList.remove('sidebaropen');
      },
    },
    computed: {
      burgerClass() {
        return this.openMobile ? 'mdi-close' : 'mdi-menu';
      },
    },
    delimiters: ['${', '}'],
  });
