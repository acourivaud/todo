<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191118134339 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE chat.message DROP CONSTRAINT FK_9A496D18F675F31B');
        $this->addSql('ALTER TABLE chat.message ADD CONSTRAINT FK_9A496D18F675F31B FOREIGN KEY (author_id) REFERENCES account.account (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE chat.message DROP CONSTRAINT fk_9a496d18f675f31b');
        $this->addSql('ALTER TABLE chat.message ADD CONSTRAINT fk_9a496d18f675f31b FOREIGN KEY (author_id) REFERENCES account.account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
