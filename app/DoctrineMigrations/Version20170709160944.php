<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170709160944 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @SuppressWarnings(PHPMD.ShortMethodName)
     *
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'postgresql',
            'Migration can only be executed safely on \'postgresql\'.'
        );

        $this->addSql(
            'CREATE TABLE cms.cgv_translation (id SERIAL NOT NULL, object_id INT DEFAULT NULL, locale VARCHAR(8) NOT NULL, field VARCHAR(32) NOT NULL, content TEXT DEFAULT NULL, PRIMARY KEY(id))'
        );
        $this->addSql('CREATE INDEX IDX_376252B4232D562B ON cms.cgv_translation (object_id)');
        $this->addSql(
            'CREATE UNIQUE INDEX lookup_unique_cgv_translation_idx ON cms.cgv_translation (locale, object_id, field)'
        );
        $this->addSql(
            'ALTER TABLE cms.cgv_translation ADD CONSTRAINT FK_376252B4232D562B FOREIGN KEY (object_id) REFERENCES core.cgv (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE'
        );
        $this->addSql('ALTER TABLE core.agency ADD cgv_id INT DEFAULT NULL');
        $this->addSql(
            'ALTER TABLE core.agency ADD CONSTRAINT FK_9A837E20C3E49468 FOREIGN KEY (cgv_id) REFERENCES core.cgv (id) NOT DEFERRABLE INITIALLY IMMEDIATE'
        );
        $this->addSql('CREATE INDEX IDX_9A837E20C3E49468 ON core.agency (cgv_id)');
    }

    /**
     * @param Schema $schema
     *
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'postgresql',
            'Migration can only be executed safely on \'postgresql\'.'
        );

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE cms.cgv_translation');
        $this->addSql('ALTER TABLE core.agency DROP CONSTRAINT FK_9A837E20C3E49468');
        $this->addSql('DROP INDEX IDX_9A837E20C3E49468');
        $this->addSql('ALTER TABLE core.agency DROP cgv_id');
    }
}
