<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171012141135 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @SuppressWarnings(PHPMD.ShortMethodName)
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'postgresql',
            'Migration can only be executed safely on \'postgresql\'.'
        );

        $this->addSql('DROP INDEX plugin.uniq_d619bbb75e237e06');
        $this->addSql('ALTER TABLE plugin.plugin RENAME COLUMN name TO slug');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D619BBB7989D9B62 ON plugin.plugin (slug)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'postgresql',
            'Migration can only be executed safely on \'postgresql\'.'
        );

        $this->addSql('DROP INDEX plugin.UNIQ_D619BBB7989D9B62');
        $this->addSql('ALTER TABLE plugin.plugin RENAME COLUMN slug TO name');
        $this->addSql('CREATE UNIQUE INDEX uniq_d619bbb75e237e06 ON plugin.plugin (name)');
    }
}
