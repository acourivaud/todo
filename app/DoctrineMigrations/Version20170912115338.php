<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170912115338 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @SuppressWarnings(PHPMD.ShortMethodName)
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE media.file_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE cms.link_file_agency_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
//        $this->addSql('INSERT INTO classification.context (id, name, enabled, created_at, updated_at) VALUES (\'file\', \'file\', true, \'2017-09-12 13:54:37\', \'2017-09-12 13:54:40\');');
//        $this->addSql('INSERT INTO classification.context (id, name, enabled, created_at, updated_at) VALUES (\'social\', \'social\', true, \'2017-09-12 13:54:37\', \'2017-09-12 13:54:40\');');
//        $this->addSql('INSERT INTO classification.category (id, parent_id, media_id, context, description, position, slug, name, enabled) VALUES (8, null, null, \'file\', \'file default category\', 0, \'file\', \'file_default\', true);');
//        $this->addSql('INSERT INTO classification.category (id, parent_id, media_id, context, description, position, slug, name, enabled) VALUES (9, null, null, \'social\', \'social default category\', 0, \'social\', \'social_default\', true);');
        $this->addSql('CREATE TABLE media.file (id INT NOT NULL, media_id INT DEFAULT NULL, media_en_id INT DEFAULT NULL, media_de_id INT DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_8B6A20BCEA9FDD75 ON media.file (media_id)');
        $this->addSql('CREATE INDEX IDX_8B6A20BCCA105783 ON media.file (media_en_id)');
        $this->addSql('CREATE INDEX IDX_8B6A20BC2071F932 ON media.file (media_de_id)');
        $this->addSql('CREATE TABLE cms.file_translation (id SERIAL NOT NULL, object_id INT DEFAULT NULL, locale VARCHAR(8) NOT NULL, field VARCHAR(32) NOT NULL, content TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E5DF5F47232D562B ON cms.file_translation (object_id)');
        $this->addSql('CREATE UNIQUE INDEX lookup_unique_file_translation_idx ON cms.file_translation (locale, object_id, field)');
        $this->addSql('CREATE TABLE cms.link_file_agency (id INT NOT NULL, file_id INT DEFAULT NULL, agency_id INT DEFAULT NULL, visibility BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_765A884093CB796C ON cms.link_file_agency (file_id)');
        $this->addSql('CREATE INDEX IDX_765A8840CDEADB2A ON cms.link_file_agency (agency_id)');
        $this->addSql('ALTER TABLE media.file ADD CONSTRAINT FK_8B6A20BCEA9FDD75 FOREIGN KEY (media_id) REFERENCES media.media (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE media.file ADD CONSTRAINT FK_8B6A20BCCA105783 FOREIGN KEY (media_en_id) REFERENCES media.media (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE media.file ADD CONSTRAINT FK_8B6A20BC2071F932 FOREIGN KEY (media_de_id) REFERENCES media.media (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE cms.file_translation ADD CONSTRAINT FK_E5DF5F47232D562B FOREIGN KEY (object_id) REFERENCES media.file (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE cms.link_file_agency ADD CONSTRAINT FK_765A884093CB796C FOREIGN KEY (file_id) REFERENCES media.file (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE cms.link_file_agency ADD CONSTRAINT FK_765A8840CDEADB2A FOREIGN KEY (agency_id) REFERENCES core.agency (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE core.agency ADD header_img_en_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE core.agency ADD header_img_de_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE core.agency ADD CONSTRAINT FK_9A837E204DA47F7E FOREIGN KEY (header_img_en_id) REFERENCES media.media (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE core.agency ADD CONSTRAINT FK_9A837E20A7C5D1CF FOREIGN KEY (header_img_de_id) REFERENCES media.media (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_9A837E204DA47F7E ON core.agency (header_img_en_id)');
        $this->addSql('CREATE INDEX IDX_9A837E20A7C5D1CF ON core.agency (header_img_de_id)');
        $this->addSql('ALTER TABLE core.cgv ADD media_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE core.cgv ADD media_en_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE core.cgv ADD media_de_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE core.cgv ADD CONSTRAINT FK_C82694D7EA9FDD75 FOREIGN KEY (media_id) REFERENCES media.media (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE core.cgv ADD CONSTRAINT FK_C82694D7CA105783 FOREIGN KEY (media_en_id) REFERENCES media.media (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE core.cgv ADD CONSTRAINT FK_C82694D72071F932 FOREIGN KEY (media_de_id) REFERENCES media.media (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_C82694D7EA9FDD75 ON core.cgv (media_id)');
        $this->addSql('CREATE INDEX IDX_C82694D7CA105783 ON core.cgv (media_en_id)');
        $this->addSql('CREATE INDEX IDX_C82694D72071F932 ON core.cgv (media_de_id)');
    }

    /**
     * @param Schema $schema
     * @SuppressWarnings(PHPMD.ShortMethodName)
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE cms.file_translation DROP CONSTRAINT FK_E5DF5F47232D562B');
        $this->addSql('ALTER TABLE cms.link_file_agency DROP CONSTRAINT FK_765A884093CB796C');
        $this->addSql('DROP SEQUENCE media.file_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE cms.link_file_agency_id_seq CASCADE');
        $this->addSql('DROP TABLE media.file');
        $this->addSql('DROP TABLE cms.file_translation');
        $this->addSql('DROP TABLE cms.link_file_agency');
        $this->addSql('ALTER TABLE core.cgv DROP CONSTRAINT FK_C82694D7EA9FDD75');
        $this->addSql('ALTER TABLE core.cgv DROP CONSTRAINT FK_C82694D7CA105783');
        $this->addSql('ALTER TABLE core.cgv DROP CONSTRAINT FK_C82694D72071F932');
        $this->addSql('DROP INDEX IDX_C82694D7EA9FDD75');
        $this->addSql('DROP INDEX IDX_C82694D7CA105783');
        $this->addSql('DROP INDEX IDX_C82694D72071F932');
        $this->addSql('ALTER TABLE core.cgv DROP media_id');
        $this->addSql('ALTER TABLE core.cgv DROP media_en_id');
        $this->addSql('ALTER TABLE core.cgv DROP media_de_id');
        $this->addSql('ALTER TABLE core.agency DROP CONSTRAINT FK_9A837E204DA47F7E');
        $this->addSql('ALTER TABLE core.agency DROP CONSTRAINT FK_9A837E20A7C5D1CF');
        $this->addSql('DROP INDEX IDX_9A837E204DA47F7E');
        $this->addSql('DROP INDEX IDX_9A837E20A7C5D1CF');
        $this->addSql('ALTER TABLE core.agency DROP header_img_en_id');
        $this->addSql('ALTER TABLE core.agency DROP header_img_de_id');
    }
}
