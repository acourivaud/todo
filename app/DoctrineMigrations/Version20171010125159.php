<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171010125159 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @SuppressWarnings(PHPMD.ShortMethodName)
     */
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA plugin');
        $this->addSql('CREATE SEQUENCE plugin.link_agency_plugin_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE plugin.plugin_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE plugin.link_agency_plugin (id INT NOT NULL, agency_id INT DEFAULT NULL, plugin_id INT DEFAULT NULL, parameters JSON NOT NULL, enabled BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_199111B1CDEADB2A ON plugin.link_agency_plugin (agency_id)');
        $this->addSql('CREATE INDEX IDX_199111B1EC942BCF ON plugin.link_agency_plugin (plugin_id)');
        $this->addSql('CREATE UNIQUE INDEX link_index ON plugin.link_agency_plugin (agency_id, plugin_id)');
        $this->addSql('CREATE TABLE plugin.plugin (id INT NOT NULL, name VARCHAR(255) NOT NULL, enabled BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D619BBB75E237E06 ON plugin.plugin (name)');
        $this->addSql('ALTER TABLE plugin.link_agency_plugin ADD CONSTRAINT FK_199111B1CDEADB2A FOREIGN KEY (agency_id) REFERENCES core.agency (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE plugin.link_agency_plugin ADD CONSTRAINT FK_199111B1EC942BCF FOREIGN KEY (plugin_id) REFERENCES plugin.plugin (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE plugin.link_agency_plugin DROP CONSTRAINT FK_199111B1EC942BCF');
        $this->addSql('DROP SEQUENCE plugin.link_agency_plugin_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE plugin.plugin_id_seq CASCADE');
        $this->addSql('DROP TABLE plugin.link_agency_plugin');
        $this->addSql('DROP TABLE plugin.plugin');
    }
}
