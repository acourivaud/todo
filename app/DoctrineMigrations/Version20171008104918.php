<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171008104918 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @SuppressWarnings(PHPMD.ShortMethodName)
     *
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'postgresql',
            'Migration can only be executed safely on \'postgresql\'.'
        );
        $this->addSql('ALTER TABLE media.gallery_media DROP CONSTRAINT FK_1DB61DBD4E7AF8F');
        $this->addSql('ALTER TABLE media.gallery_media DROP CONSTRAINT FK_1DB61DBDEA9FDD75');
        $this->addSql(
            'ALTER TABLE media.gallery_media ADD CONSTRAINT FK_1DB61DBD4E7AF8F FOREIGN KEY (gallery_id) REFERENCES media.gallery (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE'
        );
        $this->addSql(
            'ALTER TABLE media.gallery_media ADD CONSTRAINT FK_1DB61DBDEA9FDD75 FOREIGN KEY (media_id) REFERENCES media.media (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE'
        );
    }

    /**
     * @param Schema $schema
     *
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'postgresql',
            'Migration can only be executed safely on \'postgresql\'.'
        );
        $this->addSql('ALTER TABLE media.gallery_media DROP CONSTRAINT fk_1db61dbd4e7af8f');
        $this->addSql('ALTER TABLE media.gallery_media DROP CONSTRAINT fk_1db61dbdea9fdd75');
        $this->addSql(
            'ALTER TABLE media.gallery_media ADD CONSTRAINT fk_1db61dbd4e7af8f FOREIGN KEY (gallery_id) REFERENCES media.gallery (id) NOT DEFERRABLE INITIALLY IMMEDIATE'
        );
        $this->addSql(
            'ALTER TABLE media.gallery_media ADD CONSTRAINT fk_1db61dbdea9fdd75 FOREIGN KEY (media_id) REFERENCES media.media (id) NOT DEFERRABLE INITIALLY IMMEDIATE'
        );
    }
}
