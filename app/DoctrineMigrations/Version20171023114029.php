<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171023114029 extends AbstractMigration
{
    /**
     * @param Schema $schema
     *
     * @SuppressWarnings(PHPMD.ShortMethodName)
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'postgresql',
            'Migration can only be executed safely on \'postgresql\'.'
        );

        $this->addSql('ALTER TABLE social.bot_comment ADD media_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE social.bot_comment DROP media');
        $this->addSql(
            'ALTER TABLE social.bot_comment ADD CONSTRAINT FK_A1C902FCEA9FDD75 FOREIGN KEY (media_id) REFERENCES media.media (id) NOT DEFERRABLE INITIALLY IMMEDIATE'
        );
        $this->addSql('CREATE INDEX IDX_A1C902FCEA9FDD75 ON social.bot_comment (media_id)');
    }

    /**
     * @param Schema $schema
     *
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'postgresql',
            'Migration can only be executed safely on \'postgresql\'.'
        );

        $this->addSql('ALTER TABLE social.bot_comment DROP CONSTRAINT FK_A1C902FCEA9FDD75');
        $this->addSql('DROP INDEX IDX_A1C902FCEA9FDD75');
        $this->addSql('ALTER TABLE social.bot_comment ADD media VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE social.bot_comment DROP media_id');
    }
}
