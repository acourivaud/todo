<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171023095916 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @SuppressWarnings(PHPMD.ShortMethodName)
     *
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE cms.post ADD plugin_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE cms.post DROP plugin_provenance');
        $this->addSql('ALTER TABLE cms.post ADD CONSTRAINT FK_59AE088FEC942BCF FOREIGN KEY (plugin_id) REFERENCES plugin.plugin (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_59AE088FEC942BCF ON cms.post (plugin_id)');
    }

    /**
     * @param Schema $schema
     *
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE cms.post DROP CONSTRAINT FK_59AE088FEC942BCF');
        $this->addSql('DROP INDEX IDX_59AE088FEC942BCF');
        $this->addSql('ALTER TABLE cms.post ADD plugin_provenance TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE cms.post DROP plugin_id');
    }
}
