<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171020163229 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @SuppressWarnings(PHPMD.ShortMethodName)
     *
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'postgresql',
            'Migration can only be executed safely on \'postgresql\'.'
        );

        $this->addSql('DROP SEQUENCE social.social_comment_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE social.comment_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE social.tag_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql(
            'CREATE TABLE social.comment (id INT NOT NULL, social_group_id INT DEFAULT NULL, content TEXT NOT NULL, createdAt TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updatedAt TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, deleted BOOLEAN NOT NULL, discr VARCHAR(255) NOT NULL, PRIMARY KEY(id))'
        );
        $this->addSql('CREATE INDEX IDX_7BF516FF70654D6B ON social.comment (social_group_id)');
        $this->addSql(
            'CREATE TABLE social.link_social_group_tag (social_group_id INT NOT NULL, tag_id INT NOT NULL, PRIMARY KEY(social_group_id, tag_id))'
        );
        $this->addSql('CREATE INDEX IDX_5E2B751070654D6B ON social.link_social_group_tag (social_group_id)');
        $this->addSql('CREATE INDEX IDX_5E2B7510BAD26311 ON social.link_social_group_tag (tag_id)');
        $this->addSql(
            'CREATE TABLE social.bot_comment (id INT NOT NULL, plugin_id INT DEFAULT NULL, media VARCHAR(255) NOT NULL, PRIMARY KEY(id))'
        );
        $this->addSql('CREATE INDEX IDX_A1C902FCEC942BCF ON social.bot_comment (plugin_id)');
        $this->addSql(
            'CREATE TABLE social.tag (id INT NOT NULL, name VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, PRIMARY KEY(id))'
        );
        $this->addSql(
            'CREATE TABLE plugin.link_plugin_tag (plugin_id INT NOT NULL, tag_id INT NOT NULL, PRIMARY KEY(plugin_id, tag_id))'
        );
        $this->addSql('CREATE INDEX IDX_2B5010EAEC942BCF ON plugin.link_plugin_tag (plugin_id)');
        $this->addSql('CREATE INDEX IDX_2B5010EABAD26311 ON plugin.link_plugin_tag (tag_id)');
        $this->addSql(
            'ALTER TABLE social.comment ADD CONSTRAINT FK_7BF516FF70654D6B FOREIGN KEY (social_group_id) REFERENCES social.social_group (id) NOT DEFERRABLE INITIALLY IMMEDIATE'
        );
        $this->addSql(
            'ALTER TABLE social.link_social_group_tag ADD CONSTRAINT FK_5E2B751070654D6B FOREIGN KEY (social_group_id) REFERENCES social.social_group (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE'
        );
        $this->addSql(
            'ALTER TABLE social.link_social_group_tag ADD CONSTRAINT FK_5E2B7510BAD26311 FOREIGN KEY (tag_id) REFERENCES social.tag (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE'
        );
        $this->addSql(
            'ALTER TABLE social.bot_comment ADD CONSTRAINT FK_A1C902FCEC942BCF FOREIGN KEY (plugin_id) REFERENCES plugin.plugin (id) NOT DEFERRABLE INITIALLY IMMEDIATE'
        );
        $this->addSql(
            'ALTER TABLE social.bot_comment ADD CONSTRAINT FK_A1C902FCBF396750 FOREIGN KEY (id) REFERENCES social.comment (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE'
        );
        $this->addSql(
            'ALTER TABLE plugin.link_plugin_tag ADD CONSTRAINT FK_2B5010EAEC942BCF FOREIGN KEY (plugin_id) REFERENCES plugin.plugin (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE'
        );
        $this->addSql(
            'ALTER TABLE plugin.link_plugin_tag ADD CONSTRAINT FK_2B5010EABAD26311 FOREIGN KEY (tag_id) REFERENCES social.tag (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE'
        );
        $this->addSql('ALTER TABLE social.social_comment DROP CONSTRAINT fk_537c6b6670654d6b');
//        $this->addSql('DROP INDEX idx_537c6b6670654d6b');
        $this->addSql('ALTER TABLE social.social_comment DROP social_group_id');
        $this->addSql('ALTER TABLE social.social_comment DROP content');
        $this->addSql('ALTER TABLE social.social_comment DROP createdat');
        $this->addSql('ALTER TABLE social.social_comment DROP updatedat');
        $this->addSql('ALTER TABLE social.social_comment DROP deleted');
        $this->addSql(
            'ALTER TABLE social.social_comment ADD CONSTRAINT FK_537C6B66BF396750 FOREIGN KEY (id) REFERENCES social.comment (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE'
        );
    }

    /**
     * @param Schema $schema
     *
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'postgresql',
            'Migration can only be executed safely on \'postgresql\'.'
        );

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE social.social_comment DROP CONSTRAINT FK_537C6B66BF396750');
        $this->addSql('ALTER TABLE social.bot_comment DROP CONSTRAINT FK_A1C902FCBF396750');
        $this->addSql('ALTER TABLE social.link_social_group_tag DROP CONSTRAINT FK_5E2B7510BAD26311');
        $this->addSql('ALTER TABLE plugin.link_plugin_tag DROP CONSTRAINT FK_2B5010EABAD26311');
        $this->addSql('DROP SEQUENCE social.comment_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE social.tag_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE social.social_comment_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('DROP TABLE social.comment');
        $this->addSql('DROP TABLE social.link_social_group_tag');
        $this->addSql('DROP TABLE social.bot_comment');
        $this->addSql('DROP TABLE social.tag');
        $this->addSql('DROP TABLE plugin.link_plugin_tag');
        $this->addSql('ALTER TABLE social.social_comment ADD social_group_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE social.social_comment ADD content TEXT NOT NULL');
        $this->addSql('ALTER TABLE social.social_comment ADD createdat TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL');
        $this->addSql('ALTER TABLE social.social_comment ADD updatedat TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL');
        $this->addSql('ALTER TABLE social.social_comment ADD deleted BOOLEAN NOT NULL');
        $this->addSql(
            'ALTER TABLE social.social_comment ADD CONSTRAINT fk_537c6b6670654d6b FOREIGN KEY (social_group_id) REFERENCES social.social_group (id) NOT DEFERRABLE INITIALLY IMMEDIATE'
        );
//        $this->addSql('CREATE INDEX idx_537c6b6670654d6b ON social.social_comment (social_group_id)');
    }
}
