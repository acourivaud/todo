<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170808144612 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @SuppressWarnings(PHPMD.ShortMethodName)
     *
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'postgresql',
            'Migration can only be executed safely on \'postgresql\'.'
        );

        $this->addSql(
            'ALTER TABLE account.account DROP CONSTRAINT account_payment_type_selected_check'
        );

        $this->addSql(
            'ALTER TABLE account.account
    ADD CHECK(tdtd.account.account.payment_type_selected
    IN (\'sepa\',\'stripe\',\'stripe_dir\',\'paypal\',\'postfin\'))'
        );

        $this->addSql(
            'COMMENT ON COLUMN account . account . payment_type_selected IS \'(DC2Type:PaymentTypeEnum)\''
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {

    }
}
