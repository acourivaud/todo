<?php declare(strict_types = 1);

namespace Application\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171120141459 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @SuppressWarnings(PHPMD.ShortMethodName)
     *
     */
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE account.link_account_agency DROP CONSTRAINT FK_651092ED9B6B5FBA');
        $this->addSql('ALTER TABLE account.link_account_agency ADD CONSTRAINT FK_651092ED9B6B5FBA FOREIGN KEY (account_id) REFERENCES account.account (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE cms.post DROP CONSTRAINT FK_59AE088FF675F31B');
        $this->addSql('ALTER TABLE cms.post ADD CONSTRAINT FK_59AE088FF675F31B FOREIGN KEY (author_id) REFERENCES account.account (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE cart.cart_product DROP CONSTRAINT FK_397DFBC225F06C53');
        $this->addSql('ALTER TABLE cart.cart_product ADD CONSTRAINT FK_397DFBC225F06C53 FOREIGN KEY (adherent_id) REFERENCES account.adherent (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE cart.wishlist DROP CONSTRAINT FK_E5A6B2CC25F06C53');
        $this->addSql('ALTER TABLE cart.wishlist ADD CONSTRAINT FK_E5A6B2CC25F06C53 FOREIGN KEY (adherent_id) REFERENCES account.adherent (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE social.social_group DROP CONSTRAINT FK_9C91FC69B03A8386');
        $this->addSql('ALTER TABLE social.social_group ADD CONSTRAINT FK_9C91FC69B03A8386 FOREIGN KEY (created_by_id) REFERENCES account.account (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE social.social_comment DROP CONSTRAINT FK_537C6B66F675F31B');
        $this->addSql('ALTER TABLE social.social_comment ADD CONSTRAINT FK_537C6B66F675F31B FOREIGN KEY (author_id) REFERENCES account.account (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE social.link_adherent_social_group DROP CONSTRAINT FK_18CD01125F06C53');
        $this->addSql('ALTER TABLE social.link_adherent_social_group ADD CONSTRAINT FK_18CD01125F06C53 FOREIGN KEY (adherent_id) REFERENCES account.adherent (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE customer_stripe_account DROP CONSTRAINT FK_CB9C53949B6B5FBA');
        $this->addSql('ALTER TABLE customer_stripe_account ADD CONSTRAINT FK_CB9C53949B6B5FBA FOREIGN KEY (account_id) REFERENCES account.account (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE alert DROP CONSTRAINT FK_17FD46C19B6B5FBA');
        $this->addSql('ALTER TABLE alert ADD CONSTRAINT FK_17FD46C19B6B5FBA FOREIGN KEY (account_id) REFERENCES account.adherent (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE cart.cart_product DROP CONSTRAINT fk_397dfbc225f06c53');
        $this->addSql('ALTER TABLE cart.cart_product ADD CONSTRAINT fk_397dfbc225f06c53 FOREIGN KEY (adherent_id) REFERENCES account.adherent (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE social.social_comment DROP CONSTRAINT fk_537c6b66f675f31b');
        $this->addSql('ALTER TABLE social.social_comment ADD CONSTRAINT fk_537c6b66f675f31b FOREIGN KEY (author_id) REFERENCES account.account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE social.social_group DROP CONSTRAINT fk_9c91fc69b03a8386');
        $this->addSql('ALTER TABLE social.social_group ADD CONSTRAINT fk_9c91fc69b03a8386 FOREIGN KEY (created_by_id) REFERENCES account.account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE social.link_adherent_social_group DROP CONSTRAINT fk_18cd01125f06c53');
        $this->addSql('ALTER TABLE social.link_adherent_social_group ADD CONSTRAINT fk_18cd01125f06c53 FOREIGN KEY (adherent_id) REFERENCES account.adherent (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE customer_stripe_account DROP CONSTRAINT fk_cb9c53949b6b5fba');
        $this->addSql('ALTER TABLE customer_stripe_account ADD CONSTRAINT fk_cb9c53949b6b5fba FOREIGN KEY (account_id) REFERENCES account.account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE cart.wishlist DROP CONSTRAINT fk_e5a6b2cc25f06c53');
        $this->addSql('ALTER TABLE cart.wishlist ADD CONSTRAINT fk_e5a6b2cc25f06c53 FOREIGN KEY (adherent_id) REFERENCES account.adherent (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE cms.post DROP CONSTRAINT fk_59ae088ff675f31b');
        $this->addSql('ALTER TABLE cms.post ADD CONSTRAINT fk_59ae088ff675f31b FOREIGN KEY (author_id) REFERENCES account.account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE account.link_account_Agency DROP CONSTRAINT fk_651092ed9b6b5fba');
        $this->addSql('ALTER TABLE account.link_account_Agency ADD CONSTRAINT fk_651092ed9b6b5fba FOREIGN KEY (account_id) REFERENCES account.account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE alert DROP CONSTRAINT fk_17fd46c19b6b5fba');
        $this->addSql('ALTER TABLE alert ADD CONSTRAINT fk_17fd46c19b6b5fba FOREIGN KEY (account_id) REFERENCES account.adherent (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
