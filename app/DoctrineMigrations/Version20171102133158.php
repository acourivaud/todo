<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171102133158 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @SuppressWarnings(PHPMD.ShortMethodName)
     *
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'postgresql',
            'Migration can only be executed safely on \'postgresql\'.'
        );

        $this->addSql('ALTER TABLE account.adherent ADD employer VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE account.adherent ADD relation VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE account.adherent ADD wedding_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     *
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'postgresql',
            'Migration can only be executed safely on \'postgresql\'.'
        );

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE account.adherent DROP employer');
        $this->addSql('ALTER TABLE account.adherent DROP relation');
        $this->addSql('ALTER TABLE account.adherent DROP wedding_date');
    }
}
