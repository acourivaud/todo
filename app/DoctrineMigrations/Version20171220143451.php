<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171220143451 extends AbstractMigration
{
    /**
     * @param Schema $schema
     *
     * @SuppressWarnings(PHPMD.ShortMethodName)
     *
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'postgresql',
            'Migration can only be executed safely on \'postgresql\'.'
        );

        $this->addSql('CREATE SCHEMA checkout');
        $this->addSql('CREATE SEQUENCE checkout.checkout_info_needed_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE checkout.delivery_delay_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql(
            'CREATE TABLE checkout.checkout_info_needed (id INT NOT NULL, agency_id INT DEFAULT NULL, product_family VARCHAR(255) NOT NULL, info_needed VARCHAR(255) NOT NULL, PRIMARY KEY(id))'
        );
        $this->addSql('CREATE INDEX IDX_3CFE6776CDEADB2A ON checkout.checkout_info_needed (agency_id)');
        $this->addSql(
            'CREATE TABLE checkout.delivery_delay (id INT NOT NULL, agency_id INT DEFAULT NULL, product_family VARCHAR(255) NOT NULL, delay SMALLINT NOT NULL, PRIMARY KEY(id))'
        );
        $this->addSql('CREATE INDEX IDX_CB414462CDEADB2A ON checkout.delivery_delay (agency_id)');
        $this->addSql(
            'ALTER TABLE checkout.checkout_info_needed ADD CONSTRAINT FK_3CFE6776CDEADB2A FOREIGN KEY (agency_id) REFERENCES core.agency (id) NOT DEFERRABLE INITIALLY IMMEDIATE'
        );
        $this->addSql(
            'ALTER TABLE checkout.delivery_delay ADD CONSTRAINT FK_CB414462CDEADB2A FOREIGN KEY (agency_id) REFERENCES core.agency (id) NOT DEFERRABLE INITIALLY IMMEDIATE'
        );
    }

    /**
     * @param Schema $schema
     *
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'postgresql',
            'Migration can only be executed safely on \'postgresql\'.'
        );

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE checkout.checkout_info_needed_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE checkout.delivery_delay_id_seq CASCADE');
        $this->addSql('DROP TABLE checkout.checkout_info_needed');
        $this->addSql('DROP TABLE checkout.delivery_delay');
    }
}
