<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180104141358 extends AbstractMigration
{
    /**
     * @param Schema $schema
     *
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     *
     * @SuppressWarnings(PHPMD.ShortMethodName)
     */
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'postgresql',
            'Migration can only be executed safely on \'postgresql\'.'
        );

        $this->addSql('CREATE SCHEMA chat');
        $this->addSql('CREATE SEQUENCE chat.message_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE chat.conversation_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql(
            'CREATE TABLE chat.message (id INT NOT NULL, conversation_id INT DEFAULT NULL, author_id INT DEFAULT NULL, content VARCHAR(255) NOT NULL, date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))'
        );
        $this->addSql('CREATE INDEX IDX_9A496D189AC0396 ON chat.message (conversation_id)');
        $this->addSql('CREATE INDEX IDX_9A496D18F675F31B ON chat.message (author_id)');
        $this->addSql(
            'CREATE TABLE chat.conversation (id INT NOT NULL, agency_id INT DEFAULT NULL, service VARCHAR(255) NOT NULL, start_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, end_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))'
        );
        $this->addSql('CREATE INDEX IDX_1307EA69CDEADB2A ON chat.conversation (agency_id)');
        $this->addSql(
            'CREATE TABLE chat.link_account_conversation (conversation_id INT NOT NULL, account_id INT NOT NULL, PRIMARY KEY(conversation_id, account_id))'
        );
        $this->addSql('CREATE INDEX IDX_BDAB22AE9AC0396 ON chat.link_account_conversation (conversation_id)');
        $this->addSql('CREATE INDEX IDX_BDAB22AE9B6B5FBA ON chat.link_account_conversation (account_id)');
        $this->addSql(
            'ALTER TABLE chat.message ADD CONSTRAINT FK_9A496D189AC0396 FOREIGN KEY (conversation_id) REFERENCES chat.conversation (id) NOT DEFERRABLE INITIALLY IMMEDIATE'
        );
        $this->addSql(
            'ALTER TABLE chat.message ADD CONSTRAINT FK_9A496D18F675F31B FOREIGN KEY (author_id) REFERENCES account.account (id) NOT DEFERRABLE INITIALLY IMMEDIATE'
        );
        $this->addSql(
            'ALTER TABLE chat.conversation ADD CONSTRAINT FK_1307EA69CDEADB2A FOREIGN KEY (agency_id) REFERENCES core.agency (id) NOT DEFERRABLE INITIALLY IMMEDIATE'
        );
        $this->addSql(
            'ALTER TABLE chat.link_account_conversation ADD CONSTRAINT FK_BDAB22AE9AC0396 FOREIGN KEY (conversation_id) REFERENCES chat.conversation (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE'
        );
        $this->addSql(
            'ALTER TABLE chat.link_account_conversation ADD CONSTRAINT FK_BDAB22AE9B6B5FBA FOREIGN KEY (account_id) REFERENCES account.account (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE'
        );
    }

    /**
     * @param Schema $schema
     *
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'postgresql',
            'Migration can only be executed safely on \'postgresql\'.'
        );

        $this->addSql('ALTER TABLE chat.message DROP CONSTRAINT FK_9A496D189AC0396');
        $this->addSql('ALTER TABLE chat.link_account_conversation DROP CONSTRAINT FK_BDAB22AE9AC0396');
        $this->addSql('DROP SEQUENCE chat.message_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE chat.conversation_id_seq CASCADE');
        $this->addSql('DROP TABLE chat.message');
        $this->addSql('DROP TABLE chat.conversation');
        $this->addSql('DROP TABLE chat.link_account_conversation');
        $this->addSql('DROP SCHEMA chat');
    }
}
