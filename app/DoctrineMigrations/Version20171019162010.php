<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171019162010 extends AbstractMigration
{
    /**
     * @param Schema $schema
     *
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     * @SuppressWarnings(PHPMD.ShortMethodName)
     */
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE social.social_plugin_content_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE social.social_plugin_content (id INT NOT NULL, plugin_id INT DEFAULT NULL, media_id INT DEFAULT NULL, plugin_content_id TEXT NOT NULL, plugin_url TEXT NOT NULL, content TEXT NOT NULL, title TEXT NOT NULL, createdAt TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updatedAt TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_6088331CEC942BCF ON social.social_plugin_content (plugin_id)');
        $this->addSql('CREATE INDEX IDX_6088331CEA9FDD75 ON social.social_plugin_content (media_id)');
        $this->addSql('ALTER TABLE social.social_plugin_content ADD CONSTRAINT FK_6088331CEC942BCF FOREIGN KEY (plugin_id) REFERENCES plugin.plugin (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE social.social_plugin_content ADD CONSTRAINT FK_6088331CEA9FDD75 FOREIGN KEY (media_id) REFERENCES media.media (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     *
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE social.social_plugin_content_id_seq CASCADE');
        $this->addSql('DROP TABLE social.social_plugin_content');
    }
}
