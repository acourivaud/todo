<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170713151741 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @SuppressWarnings(PHPMD.ShortMethodName)
     *
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'postgresql',
            'Migration can only be executed safely on \'postgresql\'.'
        );

        $this->addSql(
            'ALTER TABLE account.adherent DROP CONSTRAINT adherent_customer_group_check'
        );

        $this->addSql(
            'ALTER TABLE account.adherent
    ADD CHECK(customer_group IN (\'ADH\',\'NON_ADH\',\'ADH_CO\'))'
        );

        $this->addSql(
            'COMMENT ON COLUMN account . adherent . customer_group IS \'(DC2Type:AdherentTypeEnum)\''
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
    }
}
