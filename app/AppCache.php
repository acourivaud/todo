<?php declare(strict_types = 1);
/**
 * PHP version 7
 */

use Symfony\Bundle\FrameworkBundle\HttpCache\HttpCache;

/**
 * Class AppCache
 */
class AppCache extends HttpCache
{
}
