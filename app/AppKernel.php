<?php
/**
 * PHP version 7
 */
declare(strict_types=1);

use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\HttpKernel\Kernel;

/**
 * Class AppKernel
 */
class AppKernel extends Kernel
{
    /**
     * {@inheritdoc}
     */
    public function getCacheDir(): string
    {
        return dirname(__DIR__) . '/var/cache/' . $this->getEnvironment();
    }

    /**
     * {@inheritdoc}
     */
    public function getLogDir(): string
    {
        return dirname(__DIR__) . '/var/logs';
    }

    /**
     * {@inheritdoc}
     */
    public function getRootDir(): string
    {
        return __DIR__;
    }

    /**
     * {@inheritdoc}
     */
    public function registerBundles(): array
    {
        $bundles = [
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AclBundle\AclBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),

            //Configuration Vendors
            new FOS\UserBundle\FOSUserBundle(),
            //            new PUGX\MultiUserBundle\PUGXMultiUserBundle(),
            new Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle(),
            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
            new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle(),

            new Sonata\DoctrineORMAdminBundle\SonataDoctrineORMAdminBundle(),
            new Sonata\ClassificationBundle\SonataClassificationBundle(),
            new Sonata\TranslationBundle\SonataTranslationBundle(),
            new Sonata\EasyExtendsBundle\SonataEasyExtendsBundle(),
            new Sonata\BlockBundle\SonataBlockBundle(),
            new Sonata\MediaBundle\SonataMediaBundle(),
            new Sonata\AdminBundle\SonataAdminBundle(),
            new Knp\Bundle\MenuBundle\KnpMenuBundle(),
            new Sonata\CoreBundle\SonataCoreBundle(),
            new Sonata\IntlBundle\SonataIntlBundle(),
            new Knp\Bundle\SnappyBundle\KnpSnappyBundle(),

            new Lexik\Bundle\JWTAuthenticationBundle\LexikJWTAuthenticationBundle(),
            new JMS\SerializerBundle\JMSSerializerBundle(),
            new Gesdinet\JWTRefreshTokenBundle\GesdinetJWTRefreshTokenBundle(),
            new JMS\AopBundle\JMSAopBundle(),
            new JMS\SecurityExtraBundle\JMSSecurityExtraBundle(),
            new JMS\DiExtraBundle\JMSDiExtraBundle(),
            new Nelmio\ApiDocBundle\NelmioApiDocBundle(),
            new Nelmio\CorsBundle\NelmioCorsBundle(),
            new FOS\RestBundle\FOSRestBundle(),
            new FOS\JsRoutingBundle\FOSJsRoutingBundle(),
            new Pix\SortableBehaviorBundle\PixSortableBehaviorBundle(),

            new Ivory\CKEditorBundle\IvoryCKEditorBundle(),
            new Knp\Bundle\MarkdownBundle\KnpMarkdownBundle(),
            new Knp\Bundle\TimeBundle\KnpTimeBundle(),
            new Sonata\FormatterBundle\SonataFormatterBundle(),

            //Configuration Bundles
            new Todotoday\AccountBundle\TodotodayAccountBundle(),
            new Todotoday\ClassificationBundle\TodotodayClassificationBundle(),
            new Todotoday\MediaBundle\TodotodayMediaBundle(),
            new Todotoday\CoreBundle\TodotodayCoreBundle(),
            new Actiane\ApiDocBundle\ActianeApiDocBundle(),
            new Todotoday\CMSBundle\TodotodayCMSBundle(),
            new Todotoday\CartBundle\TodotodayCartBundle(),
            new Actiane\MailerBundle\ActianeMailerBundle(),
            new Actiane\PaginatorBundle\ActianePaginatorBundle(),
            new Todotoday\CatalogBundle\TodotodayCatalogBundle(),
            new Todotoday\SocialBundle\TodotodaySocialBundle(),
            new Actiane\ApiConnectorBundle\ActianeApiConnectorBundle(),
            new Actiane\ToolsBundle\ActianeToolsBundle(),
            new Fresh\DoctrineEnumBundle\FreshDoctrineEnumBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Craue\FormFlowBundle\CraueFormFlowBundle(),
            new Todotoday\PaymentBundle\TodotodayPaymentBundle(),
            new Snc\RedisBundle\SncRedisBundle(),
            new Todotoday\CheckoutBundle\TodotodayCheckoutBundle(),
            new Actiane\PaymentBundle\ActianePaymentBundle(),
            new DavidBadura\FakerBundle\DavidBaduraFakerBundle(),
            new Todotoday\NotificationBundle\TodotodayNotificationBundle(),
            new Todotoday\InvoiceBundle\TodotodayInvoiceBundle(),
            new Todotoday\QuotationBundle\TodotodayQuotationBundle(),
            new Todotoday\AnimationBundle\TodotodayAnimationBundle(),
            new Actiane\AlertBundle\ActianeAlertBundle(),
            new Cocur\Slugify\Bridge\Symfony\CocurSlugifyBundle(),
            new Todotoday\FGSBundle\TodotodayFGSBundle(),
            new Todotoday\PluginBundle\TodotodayPluginBundle(),
            new Actiane\EntityChangeWatchBundle\EntityChangeWatchBundle(),
            new Todotoday\ChatBundle\TodotodayChatBundle(),
            new Todotoday\AuthSSOBundle\TodotodayAuthSSOBundle(),
            new Todotoday\SmartBannerAppBundle\TodotodaySmartBannerAppBundle(),
        ];

        if (in_array($this->getEnvironment(), ['dev', 'test'], true)) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
            $bundles[] = new Liip\FunctionalTestBundle\LiipFunctionalTestBundle();
        }

        return $bundles;
    }

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function registerContainerConfiguration(LoaderInterface $loader): void
    {
        $loader->load($this->getRootDir() . '/config/config_' . $this->getEnvironment() . '.yml');
    }
}
