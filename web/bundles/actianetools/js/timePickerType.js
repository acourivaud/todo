import Vue from 'vue'
import VueTimepicker from 'vue2-timepicker'
import 'vue2-timepicker/dist/vue2-timepicker.min.css'

Vue.use(VueTimepicker)

const forms = document.querySelectorAll('.timepicker_form')

forms.forEach(el => {
  new Vue({
    el: '#' + el.getAttribute('id'),
    components: {VueTimepicker},
    data () {
      return {
        timeSelected: null,
        timeFormated: null
      }
    },
    methods: {
      formatTime (eventData) {
        this.timeFormated = eventData.data.HH + ':' + eventData.data.mm
      }
    }
  })
})
