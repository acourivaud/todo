// Add button to edit others (all publicationDate and all status)
// addApplyToAllButton('.state-to-copy', 'state')
addApplyToAllButton('.publication-date-to-copy', 'publication-date')

function addApplyToAllButton(selector, btnIdSuffix) {
  let element = document.querySelector(selector)
  element = findTdParentNode(element)

  let buttonId = 'btn-apply-other-' + btnIdSuffix

  element.innerHTML += '<div class="input-group">'
  element.innerHTML += '<a id="' + buttonId + '" class="btn btn-primary">Appliquer à toutes les conciergeries</a>'
  element.innerHTML += '</div>'

  let buttonCreated = document.getElementById(buttonId)
  buttonCreated.addEventListener('click', () => {
    let allOtherElement = document.querySelectorAll(selector)
    let referenceValue = allOtherElement[0]

    allOtherElement.forEach((element, index) => {
      if (index === 0) {
        return;
      }

      if (element.nodeName === 'INPUT') {
        element.value = referenceValue.value
      }
    })
  })
}

function findTdParentNode(element) {
  while (element.nodeName !== 'TD') {
    element = element.parentElement
  }

  return element
}
