import eventHub from './../../../../../../vue-src/events/eventHub'

var middleName = document.getElementById("account_personal_info_middleName");
var formState = document.getElementById("account_personal_info_addressState");
var formCountry = document.getElementById("account_personal_info_addressCountryRegionId");

eventHub.$on('accountphonedial-set-country', (countryCode) => {
  countryCode = countryCode.toUpperCase()
  document.querySelector('#account_personal_info_addressCountryRegionId option[data-alpha2-value="' + countryCode + '"]').selected = true
})

if(formCountry)
{
  switchState(formCountry);
  formCountry.onchange = function () {
    switchState(this)
  };
}

function switchState(select) {
  if (select.options[select.selectedIndex].value === 'USA') {
    formState.parentElement.parentElement.style.display = "block";
    formState.disabled = false;
    middleName.parentElement.style.display = "block";
    middleName.disabled = false;
  } else {
    formState.parentElement.parentElement.style.display = "none";
    formState.disabled = true;
    middleName.parentElement.style.display = "none";
    middleName.disabled = true;
  }
}