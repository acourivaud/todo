Vue.component('sepa', {
    delimiters: ['${', '}'],
    template: '#account_sepa_info_widget-template'
});

let sepaInstance = new Vue({
    el: '#sepa'
});