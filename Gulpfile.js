const gulp = require('gulp');
var cleanCSS = require('gulp-clean-css');
var plugins = require('gulp-load-plugins')();
var babel = require('gulp-babel');
var browserify = require('gulp-browserify');

const sassOptions = {
  errLogToConsole: true,
  outputStyle: 'expanded'
}

config = {
  assetPath: 'app/Resources/assets/sass',
  jsPath: 'app/Resources/public/js/**/*.js',
  cssDest: 'web/css',
  svgDest: 'web/css/svg',
  jsDest: 'web/dist/js',
  cssName: 'main.css',
  browserCompatibility: ['last 2 versions', '> 5%', 'Firefox ESR']
}

configSVG = {
  mode: {
    css: {     // Activate the «css» mode
      render: {
        css: true  // Activate CSS output (with default options)
      }
    }
  }
}

gulp.task('sass', function () {
  gulp.src('**/*.svg', {cwd: config.assetPath})
    .pipe(plugins.svgSprite(configSVG))
    .pipe(gulp.dest(config.svgDest))

  gulp.src('**/*.scss', {cwd: config.assetPath})
    .pipe(plugins.sass(sassOptions))
    .on('error', plugins.sass.logError)
    .pipe(plugins.autoprefixer({
      browsers: config.browserCompatibility,
      cascade: true
    }))
    .pipe(plugins.concatCss(config.cssName))
    //.pipe(cleanCSS()) //arg : {compatibility: 'ie8'}
    .pipe(gulp.dest(config.cssDest))
    .pipe(plugins.size())
    .pipe(plugins.livereload())
})

gulp.task('6to5', function () {
  return gulp.src(config.jsPath)
    .pipe(
      babel({
        presets: ['es2015'],
        plugins: ['transform-runtime']
      })
    )
    .pipe(browserify())
    .pipe(
      gulp.dest(
        config.jsDest
      )
    )
    .pipe(
      plugins.size()
    )
})

gulp.task('reload', function () {
  return gulp.src('src/**/*.twig').pipe(plugins.livereload())
})

gulp.task('default', function () {
  plugins.livereload.listen([{quiet: true}])
  gulp.watch(config.assetPath + '/*.scss', ['sass'])
  gulp.watch('app/**/*.twig', ['reload'])
})


