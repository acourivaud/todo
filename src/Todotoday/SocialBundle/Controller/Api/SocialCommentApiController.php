<?php declare(strict_types = 1);

namespace Todotoday\SocialBundle\Controller\Api;

use FOS\RestBundle\Request\ParamFetcher;
use Todotoday\CoreBundle\Controller\Api\AbstractApiController;
use Symfony\Component\HttpFoundation\Response;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\SocialBundle\Form\Api\SocialCommentType;
use FOS\RestBundle\Controller\Annotations as FosRest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Todotoday\SocialBundle\Entity\SocialComment;
use Exception;

/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 21/02/17
 * Time: 09:34
 */

/**
 * Class CartProductApiController
 *
 * @package Todotoday\SocialBundle\Controller\Api
 * @FosRest\NamePrefix("todotoday.social.api.social_comment.")
 * @FosRest\Prefix(value="/social/comment")
 */
class SocialCommentApiController extends AbstractApiController
{
    /**
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Operation on comments",
     *     description="Retrieves all comments",
     *     output={
     *           "class"="array<Todotoday\SocialBundle\Entity\SocialComment> as SocialComment",
     *           "groups"={"Default"}
     *     },
     *     views={"Default","social"},
     *     section="social/comment"
     * )
     *
     * @FosRest\Route("")
     * @FosRest\QueryParam(
     *     name="fields",
     *     requirements="((Default),?)*",
     *     default="",
     *     description="Fields you need"
     * )
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return Response
     * @throws \UnexpectedValueException
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \LogicException
     */
    public function getAllAction(ParamFetcher $paramFetcher): Response
    {
        if (!$this->isGranted('ROLE_MICROSOFT', $this)) {
            throw $this->createAccessDeniedException();
        }

        $view = $this->view(
            $this->get($this->getServiceRepository())->findAll()
        );

        $groups = $this->getDefaultGroups();
        if ($fields = explode(',', $paramFetcher->get('fields'))) {
            $groups = array_merge($groups, $fields);
        }
        $view->getContext()->addGroups($groups);
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * Retrieves all comments from given parent comment
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Operation on social group",
     *     description="Retrieves all comments from parent comment",
     *     output={
     *           "class"="array<Todotoday\SocialBundle\Entity\Comment> as SocialComment",
     *           "groups"={"Default"}
     *     },
     *     views={"Default","social"},
     *     section="social"
     * )
     *
     * @FosRest\Route("/{id}/children-comments", requirements={"id" = "\d+"}, options={"expose"=true})
     * @FosRest\QueryParam(
     *     name="fields",
     *     requirements="((Default),?)*",
     *     default="",
     *     description="Fields you need"
     * )
     *
     * @FosRest\QueryParam(
     *     nullable=true,
     *     name="limit",
     *     requirements="\d+",
     *     default="0",
     *     description="Limit"
     * )
     *
     * @FosRest\QueryParam(
     *     nullable=true,
     *     name="offset",
     *     requirements="\d+",
     *     default="",
     *     description="Offset"
     * )
     *
     * @param ParamFetcher $paramFetcher
     * @param int          $id
     *
     * @return Response
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \Todotoday\SocialBundle\Exceptions\SocialForbiddenException
     * @throws \LogicException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \UnexpectedValueException
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function getChildrenCommentsAction(ParamFetcher $paramFetcher, int $id): Response
    {
        $view = $this->view(
            $this->get('todotoday_social.comment_manager')->getChildrenComments(
                $id,
                $paramFetcher->get('limit') ? (int) $paramFetcher->get('limit') : null,
                $paramFetcher->get('offset') ? (int) $paramFetcher->get('offset') : null
            )
        );

        $groups = ['Social'];
        if ($fields = explode(',', $paramFetcher->get('fields'))) {
            $groups = array_merge($groups, $fields, ['Social', 'Default']);
        }

        $view->getContext()->addGroups($groups)->setSerializeNull(true)->enableMaxDepth();

        return $this->handleView($view);
    }

    /**
     * Delete a specific comment
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Operation on social group",
     *     description="Delete a specific comment",
     *     output={
     *           "class"="array<Todotoday\SocialBundle\Entity\Comment> as SocialComment",
     *           "groups"={"Default"}
     *     },
     *     views={"Default","social"},
     *     section="social"
     * )
     *
     * @FosRest\Route("/{id}/delete-comment", requirements={"id" = "\d+"}, options={"expose"=true})
     * @FosRest\QueryParam(
     *     name="fields",
     *     requirements="((Default),?)*",
     *     default="",
     *     description="Fields you need"
     * )
     *
     * @param ParamFetcher $paramFetcher
     * @param int          $id
     *
     * @return Response
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \Todotoday\SocialBundle\Exceptions\SocialForbiddenException
     * @throws \LogicException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \UnexpectedValueException
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function deleteCommentAction(ParamFetcher $paramFetcher, int $id): Response
    {
        if (!$id) {
            throw $this->createNotFoundException('No ID found');
        }

        $em = $this->getDoctrine()->getManager();

        $repo = $this->getDoctrine()->getRepository(SocialComment::class);
        $comment = $repo->find($id);

        if (!$this->isGranted('OWNER', $comment) && !$this->isGranted('DELETE', $comment)) {
            throw $this->createAccessDeniedException();
        }

        if ($comment != null) {
            if ($comment->getChildrenCount() > 0) {
                $children = $comment->getChildrens();
                foreach ($children as $child) {
                    $em->remove($child);
                }
            }

            $em->remove($comment);
            $em->flush();
        }

        $view = $this->view($id);
        $groups = ['Social'];
        $view->getContext()->addGroups($groups)->setSerializeNull(true)->enableMaxDepth();

        return $this->handleView($view);
    }

    /**
     * Get the formType for the current entity class link to the controller
     *
     * @return string
     */
    protected function getFormType(): string
    {
        return SocialCommentType::class;
    }

    /**
     * Get the serivce name from custum repository's entity
     *
     * @return string
     */
    protected function getServiceRepository(): string
    {
        return 'todotoday_social.repository.social_comment';
    }
}
