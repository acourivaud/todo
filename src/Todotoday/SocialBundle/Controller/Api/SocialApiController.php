<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 21/02/17
 * Time: 09:34
 */

namespace Todotoday\SocialBundle\Controller\Api;

use FOS\RestBundle\Controller\Annotations as FosRest;
use FOS\RestBundle\Request\ParamFetcher;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\TranslatorInterface;
use Todotoday\CoreBundle\Controller\Api\AbstractApiController;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\SocialBundle\Entity\SocialEvent;
use Todotoday\SocialBundle\Entity\SocialGroup;
use Todotoday\SocialBundle\Exceptions\SocialShortenURLException;
use Todotoday\SocialBundle\Exceptions\SocialYoutubeVideoException;
use Todotoday\SocialBundle\Form\Api\SocialCommentType;
use Todotoday\SocialBundle\Form\Api\SocialGroupType;

/**
 * Class SocialApiController
 *
 * @package Todotoday\SocialBundle\Controller\Api
 * @FosRest\NamePrefix("todotoday.social.api.social_group.")
 * @FosRest\Prefix(value="/social")
 */
class SocialApiController extends AbstractApiController
{
    /**
     * Retrieves specific group
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Operation on social group",
     *     description="Retrieve group by id",
     *     output={
     *           "class"="Todotoday\SocialBundle\Entity\SocialGroup",
     *           "groups"={"Default"}
     *     },
     *     views={"Default","social"},
     *     section="social"
     * )
     *
     * @FosRest\Route("/{id}", requirements={"id" = "\d+"})
     * @FosRest\QueryParam(
     *     name="fields",
     *     requirements="((Default),?)*",
     *     default="",
     *     description="Fields you need"
     * )
     *
     * @param ParamFetcher $paramFetcher
     * @param int          $id
     *
     * @return Response
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \Todotoday\SocialBundle\Exceptions\SocialForbiddenException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \UnexpectedValueException
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \LogicException
     */
    public function getOneGroupAction(ParamFetcher $paramFetcher, int $id): Response
    {
        if (!$this->isGranted('VIEW', $this)) {
            throw $this->createAccessDeniedException();
        }

        $view = $this->view(
            $this->get('todotoday_social.group_manager')->getOneGroup(
                $this->getAgency(),
                $id
            )
        );

        $groups = ['Social', 'LinkedEvents'];
        if ($fields = explode(',', $paramFetcher->get('fields'))) {
            $groups = array_merge($groups, $fields);
        }
        $view->getContext()->addGroups($groups)->setSerializeNull(true)->enableMaxDepth();

        return $this->handleView($view);
    }

    /**
     * Retrieves all available group
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Operation on social group",
     *     description="Retrieves all available group",
     *     output={
     *           "class"="array<Todotoday\SocialBundle\Entity\SocialGroup> as SocialGroup",
     *           "groups"={"Default"}
     *     },
     *     views={"Default","social"},
     *     section="social"
     * )
     *
     * @FosRest\Route("/{type}", requirements={"type" = "(group|event)?"})
     * @FosRest\QueryParam(
     *     name="fields",
     *     requirements="((Default),?)*",
     *     default="",
     *     description="Fields you need"
     * )
     *
     * @param ParamFetcher $paramFetcher
     *
     * @param string       $type
     *
     * @return Response
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \UnexpectedValueException
     * @throws \InvalidArgumentException
     * @throws \LogicException
     */
    public function getAvailableGroupsAction(ParamFetcher $paramFetcher, string $type = null): Response
    {
        if (!$this->isGranted('VIEW', $this)) {
            throw $this->createAccessDeniedException();
        }

        if ($type) {
            $type = $type === 'group' ? SocialGroup::class : SocialEvent::class;
        }

        $view = $this->view(
            $this->getDoctrine()->getRepository(SocialGroup::class)->getAvailableGroups(
                $this->getAgency(),
                $this->getUser(),
                $type
            )
        );

        $groups = ['Social'];
        if ($fields = explode(',', $paramFetcher->get('fields'))) {
            $groups = array_merge($groups, $fields);
        }
        $view->getContext()->addGroups($groups)->setSerializeNull(true)->enableMaxDepth();

        return $this->handleView($view);
    }

    /**
     * Retrieves all my groups
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Operation on social group",
     *     description="Retrieves all my groups",
     *     output={
     *           "class"="array<Todotoday\SocialBundle\Entity\SocialGroup> as SocialGroup",
     *           "groups"={"Default"}
     *     },
     *     views={"Default","social"},
     *     section="social"
     * )
     *
     * @FosRest\Route("/me/{type}", requirements={"type" = "(group|event)?"})
     * @FosRest\QueryParam(
     *     name="fields",
     *     requirements="((Default),?)*",
     *     default="",
     *     description="Fields you need"
     * )
     *
     * @param ParamFetcher $paramFetcher
     *
     * @param string       $type
     *
     * @return Response
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \UnexpectedValueException
     * @throws \InvalidArgumentException
     * @throws \LogicException
     */
    public function getMyGroupsAction(ParamFetcher $paramFetcher, string $type = null): Response
    {
        if (!$this->isGranted('VIEW', $this)) {
            throw $this->createAccessDeniedException();
        }

        if ($type) {
            $type = $type === 'group' ? SocialGroup::class : SocialEvent::class;
        }

        $view = $this->view(
            $this->getDoctrine()->getRepository(SocialGroup::class)->getMyGroups(
                $this->getAgency(),
                $this->getUser(),
                $type
            )
        );

        $groups = ['Social'];
        if ($fields = explode(',', $paramFetcher->get('fields'))) {
            $groups = array_merge($groups, $fields);
        }
        $view->getContext()->addGroups(array_merge($groups, ['Social']))->setSerializeNull(true)->enableMaxDepth();

        return $this->handleView($view);
    }

    /**
     * Retrieves all my invits
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Operation on social group",
     *     description="Retrieves all my invits",
     *     output={
     *           "class"="array<Todotoday\SocialBundle\Entity\SocialGroup> as SocialGroup",
     *           "groups"={"Default"}
     *     },
     *     views={"Default","social"},
     *     section="social"
     * )
     *
     * @FosRest\Route("/invit")
     * @FosRest\QueryParam(
     *     name="fields",
     *     requirements="((Default),?)*",
     *     default="",
     *     description="Fields you need"
     * )
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return Response
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \UnexpectedValueException
     * @throws \InvalidArgumentException
     * @throws \LogicException
     */
    public function getMyInvitsAction(ParamFetcher $paramFetcher): Response
    {
        if (!$this->isGranted('VIEW', $this)) {
            throw $this->createAccessDeniedException();
        }

        $view = $this->view(
            $this->getDoctrine()->getRepository(SocialGroup::class)->getMyInvits(
                $this->getAgency(),
                $this->getUser()
            )
        );

        $groups = ['Social'];
        if ($fields = explode(',', $paramFetcher->get('fields'))) {
            $groups = array_merge($groups, $fields);
        }
        $view->getContext()->addGroups($groups)->setSerializeNull(true)->enableMaxDepth();

        return $this->handleView($view);
    }

    /**
     * Retrieves all comments from given group
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Operation on social group",
     *     description="Retrieves all comments from social group",
     *     output={
     *           "class"="array<Todotoday\SocialBundle\Entity\Comment> as SocialComment",
     *           "groups"={"Default"}
     *     },
     *     views={"Default","social"},
     *     section="social"
     * )
     *
     * @FosRest\Route("/{id}/comments", requirements={"id" = "\d+"}, options={"expose"=true})
     * @FosRest\QueryParam(
     *     name="fields",
     *     requirements="((Default),?)*",
     *     default="",
     *     description="Fields you need"
     * )
     *
     * @FosRest\QueryParam(
     *     nullable=true,
     *     name="limit",
     *     requirements="\d+",
     *     default="0",
     *     description="Limit"
     * )
     *
     * @FosRest\QueryParam(
     *     nullable=true,
     *     name="offset",
     *     requirements="\d+",
     *     default="",
     *     description="Offset"
     * )
     *
     * @param ParamFetcher $paramFetcher
     * @param int          $id
     *
     * @return Response
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \Todotoday\SocialBundle\Exceptions\SocialForbiddenException
     * @throws \LogicException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \UnexpectedValueException
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function getCommentsAction(ParamFetcher $paramFetcher, int $id): Response
    {
        if (!$this->isGranted('VIEW', $this)) {
            throw $this->createAccessDeniedException();
        }

        $view = $this->view(
            $this->get('todotoday_social.group_manager')->getComments(
                $this->getAgency(),
                $id,
                $paramFetcher->get('limit') ? (int) $paramFetcher->get('limit') : null,
                $paramFetcher->get('offset') ? (int) $paramFetcher->get('offset') : null
            )
        );

        $groups = ['Social'];
        if ($fields = explode(',', $paramFetcher->get('fields'))) {
            $groups = array_merge($groups, $fields, ['Social', 'Default']);
        }

        $view->getContext()->addGroups($groups)->setSerializeNull(true)->enableMaxDepth();

        return $this->handleView($view);
    }

    /**
     * @param int $id
     *
     * @FosRest\Route("/{id}/members", requirements={"id" = "\d+"}, options={"expose"=true})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getMembersActions(int $id): Response
    {
        if (!$this->isGranted('VIEW', $this)) {
            throw $this->createAccessDeniedException();
        }

        $groups = ['Social'];
        $view = $this->view($this->get('todotoday_social.group_manager')->getMembers($this->getAgency(), $id));
        $view->getContext()->addGroups($groups)->setSerializeNull(true)->enableMaxDepth();

        return $this->handleView($view);
    }

    /**
     * Post comment in a specific group
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Operation on social group",
     *     description="Post new commment on a group",
     *     input={
     *           "class"="Todotoday\SocialBundle\Form\Api\SocialCommentType"
     *     },
     *     output={
     *           "class"="Todotoday\SocialBundle\Entity\SocialComment",
     *           "groups"={"Default"}
     *     },
     *     views={"Default","social"},
     *     section="social"
     * )
     *
     * @FosRest\Route("/{id}/comments", requirements={"id" = "\d+"})
     * @FosRest\QueryParam(
     *     name="fields",
     *     requirements="((Default),?)*",
     *     default="",
     *     description="Fields you need"
     * )
     *
     * @param Request $request
     * @param int     $id
     *
     * @return Response
     * @throws \Symfony\Component\Security\Acl\Exception\AclNotFoundException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \Todotoday\SocialBundle\Exceptions\SocialForbiddenException
     * @throws \Todotoday\SocialBundle\Exceptions\SocialBadRequestException
     * @throws \Todotoday\SocialBundle\Exceptions\SocialYoutubeVideoException
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Symfony\Component\Finder\Exception\AccessDeniedException
     * @throws \Symfony\Component\Security\Acl\Exception\InvalidDomainObjectException
     * @throws \Symfony\Component\Security\Acl\Exception\AclAlreadyExistsException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \UnexpectedValueException
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \LogicException
     */
    public function postCommentAction(Request $request, int $id): Response
    {
        if (!$this->isGranted('CREATE', $this)) {
            throw $this->createAccessDeniedException();
        }

        try {
            $form = $this->createForm(SocialCommentType::class);
            $form->handleRequest($request);
        } catch (SocialYoutubeVideoException $e) {
            $translator = $this->get('translator');

            return new JsonResponse([
                'errors' => $translator->trans(
                    'social.cannot_upload_video',
                    array(),
                    'todotoday'
                ),
            ], 422);
        }

        $data = $form;

        if ($form->isSubmitted() && $form->isValid()) {
            $manager = $this->get('todotoday_social.group_manager');
            $data = $manager->addComment($this->getAgency(), $id, $data->getData());
        }

        $groups = ['Social'];
        $view = $this->view($data);
        $view->getContext()->addGroups($groups)->setSerializeNull(true)->enableMaxDepth();

        return $this->handleView($view);
    }

    /**
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Operation on social group",
     *     description="Create a new social group",
     *     input={
     *           "class"="Todotoday\SocialBundle\Form\Api\SocialGroupType"},
     *     output={
     *           "class"="Todotoday\SocialGroupBundle\Entity\SocialGroup",
     *           "groups"={"Default"}
     *     },
     *     views={"default","social"},
     *     section="social"
     * )
     *
     * @param Request $request
     * @param string  $type
     *
     * @FosRest\Route("/{type}" , requirements={"type" = "(group|event)"})
     *
     * @return Response
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException
     * @throws \RuntimeException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Todotoday\SocialBundle\Exceptions\SocialForbiddenException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \Symfony\Component\Security\Acl\Exception\AclAlreadyExistsException
     * @throws \Symfony\Component\Security\Acl\Exception\InvalidDomainObjectException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    public function postGroupAction(Request $request, string $type): Response
    {
        if (!$this->isGranted('CREATE', $this)) {
            throw $this->createAccessDeniedException();
        }

        $dataClass = $type === 'group' ? SocialGroup::class : SocialEvent::class;

        $options = array(
            'type' => $type,
            'data_class' => $dataClass,
        );
        $requestData = $request->request->all();

        $data = $form = $this->createForm($this->getFormType(), null, $options);

        $form->submit($requestData);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager = $this->get('todotoday_social.group_manager');
            $data = $manager->addSocialGroup($this->getAgency(), $form->getData());
        }
        $groups = ['Social'];
        $view = $this->view($data);
        $view->getContext()->addGroups($groups)->setSerializeNull(true)->enableMaxDepth();

        return $this->handleView($view);
    }

    /**
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Operation on social group",
     *     description="Update a social group",
     *     input={
     *           "class"="Todotoday\SocialBundle\Form\Api\SocialGroupType"},
     *     output={
     *           "class"="Todotoday\SocialGroupBundle\Entity\SocialGroup",
     *           "groups"={"Default"}
     *     },
     *     views={"default","social"},
     *     section="social"
     * )
     *
     * @param Request $request
     * @param string  $type
     * @param int     $id
     *
     * @FosRest\Route("/edit/{type}/{id}" , requirements={"type" = "(group|event)"})
     *
     * @return Response
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException
     * @throws \RuntimeException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Todotoday\SocialBundle\Exceptions\SocialForbiddenException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \Symfony\Component\Security\Acl\Exception\AclAlreadyExistsException
     * @throws \Symfony\Component\Security\Acl\Exception\InvalidDomainObjectException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    public function postEditGroupAction(Request $request, string $type, int $id): Response
    {
        if (!$this->isGranted('CREATE', $this)) {
            throw $this->createAccessDeniedException();
        }

        /** @var \Todotoday\SocialBundle\Services\SocialGroupManager $manager */
        $manager = $this->get('todotoday_social.group_manager');
        $socialGroup = $manager->getOneGroup($this->getAgency(), $id);

        if (is_null($socialGroup)) {
            throw $this->createNotFoundException();
        }

        $dataClass = $type === 'group' ? SocialGroup::class : SocialEvent::class;

        $options = array(
            'type' => $type,
            'data_class' => $dataClass,
        );
        $requestData = $request->request->all();

        $data = $form = $this->createForm($this->getFormType(), $socialGroup, $options);

        $form->submit($requestData);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $manager->editSocialGroup($form->getData());
        }

        $groups = ['Social'];
        $view = $this->view($data);
        $view->getContext()->addGroups($groups)->setSerializeNull(true)->enableMaxDepth();

        return $this->handleView($view);
    }

    /**
     * Invit list of user to a group
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Operation on social group",
     *     description="Invit list of user to a group",
     *     output={
     *           "class"="array <Todotoday\SocialBundle\Entity\LinkAdherentSocialGroup> as LinkAdherentSocialGroup",
     *           "groups"={"Default"}
     *     },
     *     views={"Default","social"},
     *     section="social"
     * )
     *
     * @FosRest\Route("/{id}/action/{action}", requirements={
     *     "id" = "^[1-9]\d*$",
     *     "action" = "(invited|banned)"
     * })
     * @FosRest\QueryParam(
     *     name="fields",
     *     requirements="((Default),?)*",
     *     default="",
     *     description="Fields you need"
     * )
     *
     * @FosRest\RequestParam(
     *     name="emails",
     *     allowBlank=false,
     *     description="List of email's user"
     * )
     *
     * @param ParamFetcher $paramFetcher
     * @param int          $id
     * @param string       $action
     *
     * @return Response
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \Todotoday\SocialBundle\Exceptions\SocialForbiddenException
     * @throws \Todotoday\SocialBundle\Exceptions\SocialBadRequestException
     * @throws \Symfony\Component\Validator\Exception\MissingOptionsException
     * @throws \Symfony\Component\Validator\Exception\InvalidOptionsException
     * @throws \Symfony\Component\Validator\Exception\ConstraintDefinitionException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Symfony\Component\Finder\Exception\AccessDeniedException
     * @throws \Symfony\Component\Security\Acl\Exception\InvalidDomainObjectException
     * @throws \Symfony\Component\Security\Acl\Exception\AclAlreadyExistsException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \UnexpectedValueException
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \LogicException
     */
    public function postInvitOrBannedAction(ParamFetcher $paramFetcher, int $id, string $action): Response
    {
        $list = $paramFetcher->get('emails');

        $result = $this->get('todotoday_social.group_manager')->actionToUser(
            $this->getAgency(),
            $action,
            $id,
            $list
        );
        $groups = ['Social'];
        $view = $this->view($result);
        $view->getContext()->addGroups($groups)->setSerializeNull(true)->enableMaxDepth();

        return $this->handleView($view);
    }

    /**
     * Get list of the future event
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Operation on social group",
     *     description="Update participation status in a group by the user",
     *     output={
     *           "class"="array<Todotoday\SocialBundle\Entity\SocialEvent>",
     *           "groups"={"Social"}
     *     },
     *     views={"Default","social"},
     *     section="social"
     * )
     *
     * @FosRest\Route("/event/future", options={"expose"=true})
     *
     * @return Response
     * @throws \UnexpectedValueException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \InvalidArgumentException
     * @throws \Todotoday\SocialBundle\Exceptions\SocialForbiddenException
     * @throws \LogicException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function getFutureEventAction(): Response
    {
        if (!$this->isGranted('VIEW', $this)) {
            throw $this->createAccessDeniedException();
        }

        $agency = $this->getAgency();

        $repo = $this->getDoctrine()->getRepository(SocialEvent::class);
        $futureEvents = $repo->getFutureEvent($agency);
        $view = $this->view($futureEvents);
        $view->getContext()->addGroups(['Social'])->enableMaxDepth()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * Update participation status in a group by the user
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Operation on social group",
     *     description="Update participation status in a group by the user",
     *     output={
     *           "class"="Todotoday\SocialBundle\Entity\LinkAdherentSocialGroup",
     *           "groups"={"Default"}
     *     },
     *     views={"Default","social"},
     *     section="social"
     * )
     *
     * @FosRest\Route("/{id}/action/{action}", requirements={
     *     "id" = "^[1-9]\d*$",
     *     "action" = "(joined|refused|accepted|exited|muted)"
     * })
     * @FosRest\QueryParam(
     *     name="fields",
     *     requirements="((Default),?)*",
     *     default="",
     *     description="Fields you need"
     * )
     *
     * @param int    $id
     * @param string $action
     *
     * @return Response
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \Todotoday\SocialBundle\Exceptions\SocialForbiddenException
     * @throws \Todotoday\SocialBundle\Exceptions\SocialBadRequestException
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Symfony\Component\Finder\Exception\AccessDeniedException
     * @throws \Symfony\Component\Security\Acl\Exception\InvalidDomainObjectException
     * @throws \Symfony\Component\Security\Acl\Exception\AclAlreadyExistsException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \UnexpectedValueException
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \LogicException
     */
    public function postSocialAccessAction(int $id, string $action): Response
    {
        $result = $this->get('todotoday_social.group_manager')->actionFromUser($this->getAgency(), $action, $id);
        $view = $this->view($result);
        $groups = ['Social'];
        $view->getContext()->addGroups($groups)->setSerializeNull(true)->enableMaxDepth();

        return $this->handleView($view);
    }

    /**
     * Add Event to a group
     *
     * @param int $idGroup
     * @param int $idEvent
     *
     * @FosRest\Route("/{idGroup}/add-event/{idEvent}" , options={"expose"=true}, requirements={
     *     "idGroup" = "^[1-9]\d*$",
     *     "idEvent" = "^[1-9]\d*$"
     * })
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Operation on social group",
     *     description="Link event to a group",
     *     output={
     *           "class"="Todotoday\SocialBundle\Entity\SocialGroup",
     *           "groups"={"Social", "LinkedEvents"}
     *     },
     *     views={"Default","social"},
     *     section="social"
     * )
     * @return Response
     * @throws \UnexpectedValueException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \InvalidArgumentException
     * @throws \Todotoday\SocialBundle\Exceptions\SocialForbiddenException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \LogicException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function postSocialAddEventAction(int $idGroup, int $idEvent): Response
    {
        if (!$this->isGranted('CREATE', $this)) {
            throw $this->createAccessDeniedException();
        }

        $user = $this->getUser();
        $agency = $this->getAgency();

        $manager = $this->get('todotoday_social.group_manager');
        $group = $manager->getOneGroup($agency, $idGroup);
        $event = $manager->getOneGroup($agency, $idEvent);
        $result = $manager->addEventToGroup($user, $agency, $group, $event);

        $view = $this->view($result);
        $view->getContext()->addGroups(['Social', 'LinkedEvents']);

        return $this->handleView($view);
    }

    /**
     * Get accounts suggestions with limit 10 by default filtered by search parameter on full name
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Operations on accounts.",
     *     description="Get accounts suggestions with limit 10 by default filtered by search parameter on full name",
     *     output={
     *           "class"="Todotoday\SocialBundle\Entity\SocialGroup",
     *           "groups"={"Social", "Suggestions"}
     *     },
     *     views={"Default","social"},
     *     section="social"
     * )
     *
     * @FOSRest\Route("/{idGroup}/suggestions/{search}")
     *
     * @FOSRest\QueryParam(
     *     name="fields",
     *     requirements={
     *         "idGroup" = "^[1-9]\d*$",
     *         "((account|agency),?)*"
     *     },
     *     default="",
     *     description="Fields you need"
     * )
     *
     * @param ParamFetcher $paramFetcher
     * @param int          $idGroup
     * @param string       $search
     *
     * @return Response|null
     * @throws \Symfony\Component\Security\Acl\Exception\InvalidDomainObjectException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\Security\Acl\Exception\AclNotFoundException
     * @throws \Symfony\Component\Security\Acl\Exception\NoAceFoundException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    public function getSuggestionAction(ParamFetcher $paramFetcher, int $idGroup, string $search): Response
    {
        /** @var Agency $agency */
        if (!($agency = $this->get('todotoday.core.domain_context.api')->getAgency())
        ) {
            throw $this->createAccessDeniedException('You must specified agency in header');
        }

        if (!$search) {
            return null;
        }

        $search = urldecode($search);
        $excludedIds = [];

        $members = $this->getDoctrine()
                        ->getManager()
                        ->getRepository(SocialGroup::class)
                        ->getActiveListMembers($idGroup);

        $excludedIds = $members ? array_column($members, 'id') : [];

        $accounts = $this->getDoctrine()
                        ->getManager()
                        ->getRepository(Account::class)
                        ->findSuggestions($search, $agency, 10, $excludedIds);

        $view = $this->view($accounts);
        $groups = ['Default'];
        if ($fields = explode(',', $paramFetcher->get('fields'))) {
            $groups = array_merge($groups, $fields);
        }
        $view->getContext()->addGroups($groups);
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * Get the formType for the current entity class link to the controller
     *
     * @return string
     */
    protected function getFormType(): string
    {
        return SocialGroupType::class;
    }

    /**
     * Get the serivce name from custum repository's entity
     *
     * @return string
     */
    protected function getServiceRepository(): string
    {
        return 'todotoday_social.repository.social_group';
    }

    /**
     * @return Agency
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \UnexpectedValueException
     * @throws \InvalidArgumentException
     */
    private function getAgency(): ?Agency
    {
        if (!$agency = $this->get('todotoday.core.domain_context.api')->getAgency()) {
            throw new \InvalidArgumentException('Agency is missing');
        }

        return $agency;
    }
}
