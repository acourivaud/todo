<?php declare(strict_types=1);

namespace Todotoday\SocialBundle\Controller;

use Actiane\ToolsBundle\Exceptions\PublishedMessageExceptionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class SocialGroupController extends Controller
{
    /**
     * @param null $vueRoute
     *
     * @return Response
     */
    public function indexAction($vueRoute = null): Response
    {
        return $this->render(':Social:index.html.twig');
    }

    /**
     * @param int $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \Todotoday\SocialBundle\Exceptions\SocialForbiddenException
     * @throws \Exception
     * @throws \UnexpectedValueException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function viewGroupAction(int $id): Response
    {
        $groupManager = $this->get('todotoday_social.group_manager');
        $agency = $this->get('todotoday.core.domain_context.agency')->getAgency();

        try {
            if (!$groupManager->getOneGroup($agency, $id)) {
                return $this->redirectToRoute('todotoday_social_homepage');
            }
        } catch (PublishedMessageExceptionInterface $exception) {
            throw $this->createNotFoundException();
        }

        return $this->render(
            ':Social:view.html.twig',
            array(
                'idgroup' => $id,
            )
        );
    }
}
