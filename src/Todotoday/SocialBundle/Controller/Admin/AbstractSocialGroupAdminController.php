<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 23/11/17
 * Time: 17:23
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\SocialBundle\Controller\Admin
 *
 * @subpackage Todotoday\SocialBundle\Controller\Admin
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\SocialBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Todotoday\SocialBundle\Entity\LinkAdherentSocialGroup;
use Todotoday\SocialBundle\Entity\SocialGroup;

/**
 * Class AbstractSocialGroupAdminController
 */
abstract class AbstractSocialGroupAdminController extends CRUDController
{
    /**
     * {@inheritdoc}
     * @throws \LogicException
     */
    public function createAction(): Response
    {
        if (!$this->isGranted('ROLE_COMMUNITY_MANAGER') || $this->getRequest()->get('agencies_strategy')) {
            return parent::createAction();
        }

        return $this->render(
            '@TodotodayCMS/Admin/Post/select_option.html.twig',
            array(
                'action' => 'create',
                'options' => array(
                    'empty',
                    'all',
                    'us',
                    'fr',
                    'ch',
                ),
            )
        );
    }

    /**
     * @param int $id
     *
     * @return Response
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    public function commentsAction(int $id): Response
    {
        $adminComment = $this->container->get('todotoday_social.admin.social_comment');

        return new RedirectResponse(
            $adminComment->generateUrl(
                'list',
                [
                    'filter[socialGroup][value]' => $id,
                ]
            )
        );
    }

    /**
     * @param int $id
     *
     * @return Response
     * @throws \InvalidArgumentException
     * @throws \LogicException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\NoResultException
     */
    public function membersCountAction(int $id): Response
    {
        $repo = $this->getDoctrine()->getRepository(SocialGroup::class);

        return new Response($repo->getCountMembers($id));
    }

    /**
     * @param int $id
     *
     * @return Response
     * @throws \InvalidArgumentException
     */
    public function membersListAction(int $id): Response
    {
        return new RedirectResponse(
            $this->generateUrl('admin.social.group.members_list', ['id' => $id])
        );
    }

    /**
     * @param int $id
     *
     * @return Response
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    public function previewAction(int $id): Response
    {
        if (!$group = $this->getDoctrine()->getRepository(SocialGroup::class)->find($id)) {
            throw $this->createNotFoundException('Social group not found');
        }

        $groupUrl = false;
        $linkAgencies = $group->getLinkAgencies();

        foreach ($linkAgencies as $linkAgency) {
            $groupUrl = sprintf(
                '%s://%s.%s%s',
                $this->container->getParameter('scheme'),
                $linkAgency->getAgency()->getSlug(),
                $this->container->getParameter('domain'),
                $this->generateUrl('todotoday_social_view_group', ['id' => $group->getId()])
            );

            break;
        }

        if ($groupUrl) {
            return new RedirectResponse($groupUrl);
        }

        return new RedirectResponse(
            $this->generateUrl(
                'todotoday_social_view_group',
                array(
                    'id' => $group->getId(),
                )
            )
        );
    }
}
