<?php declare(strict_types=1);

namespace Todotoday\SocialBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;
use Todotoday\SocialBundle\Entity\SocialComment;

/**
 * Class SocialAdminNotificationAdminController
 *
 * @package Todotoday\SocialBundle\Controller\Admin
 */
class SocialAdminNotificationAdminController extends CRUDController
{
    /**
     * @param int $id
     *
     * @return RedirectResponse
     */
    public function changeStateAction($id)
    {
        $translator = $this->get('translator');

        $object = $this->admin->getSubject();

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id: %s', $id));
        }

        $object->setState(true);

        $this->getDoctrine()->getManager()->flush();

        $this->addFlash(
            'sonata_flash_success',
            $translator->trans('admin_notification_state_success_updated')
        );

        return new RedirectResponse($this->admin->generateUrl('list'));
    }

    /**
     * @param ProxyQueryInterface $selectedModelQuery
     * @param Request             $request
     *
     * @return RedirectResponse
     */
    public function batchActionChangeState(ProxyQueryInterface $selectedModelQuery, Request $request = null)
    {
        $this->admin->checkAccess('edit');
        $this->admin->checkAccess('delete');

        $translator = $this->get('translator');

        $selectedModels = $selectedModelQuery->execute();

        try {
            foreach ($selectedModels as $selectedModel) {
                $selectedModel->setState(true);
            }

            $this->getDoctrine()->getManager()->flush();
        } catch (\Exception $e) {
            $this->addFlash(
                'sonata_flash_error',
                $translator->trans('admin_notification_state_batch_error'),
            );

            return new RedirectResponse(
                $this->admin->generateUrl('list', [
                    'filter' => $this->admin->getFilterParameters(),
                ])
            );
        }

        $this->addFlash(
            'sonata_flash_success',
            $translator->trans('admin_notification_state_batch_success'),
        );

        return new RedirectResponse(
            $this->admin->generateUrl('list', [
                'filter' => $this->admin->getFilterParameters(),
            ])
        );
    }

    /**
     * @param int $id
     *
     * @return Response
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    public function commentAction(int $id): Response
    {
        $adminComment = $this->container->get('todotoday_social.admin.social_comment');

        $action = 'edit';

        return new RedirectResponse(
            $adminComment->generateUrl(
                $action,
                [
                    'id' => $id,
                ]
            )
        );
    }

    /**
     * @param int $id
     *
     * @return Response
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    public function groupAction(int $id): Response
    {
        $adminGroup = $this->container->get('todotoday_social.admin.social_group');

        return new RedirectResponse(
            $adminGroup->generateUrl(
                'edit',
                [
                    'id' => $id,
                ]
            )
        );
    }

    /**
     * @param int $id
     *
     * @return Response
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    public function eventAction(int $id): Response
    {
        $adminEvent = $this->container->get('todotoday_social.admin.social_event');

        return new RedirectResponse(
            $adminEvent->generateUrl(
                'edit',
                [
                    'id' => $id,
                ]
            )
        );
    }
}
