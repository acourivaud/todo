<?php declare(strict_types=1);

namespace Todotoday\SocialBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController;

/**
 * Class LinkAgencySocialGroupAdminController
 * @package Todotoday\SocialBundle\Controller\Admin
 */
class LinkAgencySocialGroupAdminController extends CRUDController
{
}
