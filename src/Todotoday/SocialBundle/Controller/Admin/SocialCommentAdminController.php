<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 20/10/17
 * Time: 09:49
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\SocialBundle\Controller\Admin
 *
 * @subpackage Todotoday\SocialBundle\Controller\Admin
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\SocialBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController;
use Todotoday\SocialBundle\Entity\AdminNotification;
use Todotoday\SocialBundle\Entity\SocialComment;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sonata\AdminBundle\Exception\ModelManagerException;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class SocialCommentAdminController
 */
class SocialCommentAdminController extends CRUDController
{
    /**
     * {@inheritdoc}
     * @throws \LogicException
     */
    public function selectmediasAction($id = null): Response
    {

        return $this->render(
            '@TodotodaySocial/Admin/SocialComment/select_medias.html.twig',
            array(
                'id' => $id,
                'options' => array(
                    'edit',
                    'edityoutube',
                    'editvimeo',
                ),
            )
        );
    }

    /**
     * @param int|string|null $id
     *
     * @throws NotFoundHttpException If the object does not exist
     * @throws \RuntimeException     If no editable field is defined
     * @throws AccessDeniedException If access is not granted
     *
     * @return Response|RedirectResponse
     */
    public function editAction($id = null)
    {
        $em = $this->getDoctrine()->getManager();

        $repo = $this->getDoctrine()->getRepository(AdminNotification::class);
        $adminNotification = $repo->findByRelatedId($id, 'comment');

        if ($adminNotification != null) {
            $adminNotification->setState(true);
            $em->flush();
        }

        return parent::editAction($id);
    }

    /**
     * @param int|string|null $id
     *
     * @throws NotFoundHttpException If the object does not exist
     * @throws \RuntimeException     If no editable field is defined
     * @throws AccessDeniedException If access is not granted
     *
     * @return Response|RedirectResponse
     */
    public function edityoutubeAction($id = null)
    {
        return $this->editAction($id);
    }

    /**
     * @param int|string|null $id
     *
     * @throws NotFoundHttpException If the object does not exist
     * @throws \RuntimeException     If no editable field is defined
     * @throws AccessDeniedException If access is not granted
     *
     * @return Response|RedirectResponse
     */
    public function editvimeoAction($id = null)
    {
        return $this->editAction($id);
    }

     /**
     * @param int $id
     *
     * @return Response
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    public function groupAction(int $id): Response
    {
        if (!$comment = $this->getDoctrine()->getRepository(SocialComment::class)->find($id)) {
            throw $this->createNotFoundException('Social comment not found');
        }

        $groupUrl = false;
        $group = $comment->getSocialGroup();
        $linkAgencies = $group->getLinkAgencies();

        foreach ($linkAgencies as $linkAgency) {
            $groupUrl = sprintf(
                '%s://%s.%s%s',
                $this->container->getParameter('scheme'),
                $linkAgency->getAgency()->getSlug(),
                $this->container->getParameter('domain'),
                $this->generateUrl('todotoday_social_view_group', ['id' => $group->getId()])
            );

            break;
        }

        if ($groupUrl) {
            return new RedirectResponse($groupUrl);
        }

        return new RedirectResponse(
            $this->generateUrl(
                'todotoday_social_view_group',
                array(
                    'id' => $group->getId(),
                )
            )
        );
    }

    /**
     * Delete children loop.
     *
     * @param int|string|null $id
     *
     * @throws NotFoundHttpException If the object does not exist
     * @throws AccessDeniedException If access is not granted
     *
     * @return null
     */
    public function deleteChildren($id)
    {
        $comment = $this->admin->getObject($id);
        if ($comment->getChildrenCount() > 0) {
            $children = $comment->getChildrens();
            foreach ($children as $child) {
                $childName = $this->admin->toString($child);

                try {
                    $this->admin->delete($child);
                } catch (ModelManagerException $e) {
                    $this->handleModelManagerException($e);

                    if ($this->isXmlHttpRequest()) {
                        return $this->renderJson(['result' => 'error'], Response::HTTP_OK, []);
                    }

                    $this->addFlash(
                        'sonata_flash_error',
                        $this->trans(
                            'flash_delete_error',
                            ['%name%' => $this->escapeHtml($childName)],
                            'SonataAdminBundle'
                        )
                    );
                }
            }
        }

        return null;
    }

    /**
     * Delete action.
     *
     * @param int|string|null $id
     *
     * @throws NotFoundHttpException If the object does not exist
     * @throws AccessDeniedException If access is not granted
     *
     * @return Response|RedirectResponse
     */
    public function deleteAction($id)
    {
        if (Request::METHOD_DELETE === $this->getRestMethod()) {
            // check the csrf token
            $this->validateCsrfToken('sonata.delete');

            $this->deleteChildren($id);
        }

        return parent::deleteAction($id);
    }

    /**
     * Execute a batch delete.
     *
     * @param ProxyQueryInterface $query
     *
     * @throws AccessDeniedException If access is not granted
     *
     * @return RedirectResponse
     */
    public function batchActionDelete(ProxyQueryInterface $query)
    {
        foreach ($query->getQuery()->iterate() as $object) {
            $this->deleteChildren($object[0]->getId());
        }

        return parent::batchActionDelete($query);
    }
}
