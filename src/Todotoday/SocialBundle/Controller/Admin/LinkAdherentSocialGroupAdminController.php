<?php declare(strict_types=1);

namespace Todotoday\SocialBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController;

/**
 * Class LinkAdherentSocialGroupAdminController
 *
 * @package Todotoday\SocialBundle\Controller\Admin
 */
class LinkAdherentSocialGroupAdminController extends CRUDController
{
}
