<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 24/11/17
 * Time: 12:33
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\SocialBundle\Admin
 *
 * @subpackage Todotoday\SocialBundle\Admin
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\SocialBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Todotoday\SocialBundle\Entity\LinkAdherentSocialGroup;
use Todotoday\SocialBundle\Entity\SocialEvent;
use Todotoday\SocialBundle\Entity\SocialGroup;

/**
 * Class SocialGroupMembersListController
 * @Route("admin/todotoday/social/")
 */
class SocialGroupMembersListController extends Controller
{
    /**
     * @param int $id
     * @Route(path="{id}/members-list", name="admin.social.group.members_list")
     *
     * @return Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \LogicException
     */
    public function socialGroupMembersListAction(int $id): Response
    {
        if (!$group = $this->getDoctrine()->getRepository(SocialGroup::class)->find($id)) {
            throw $this->createNotFoundException('Social group not found');
        }

        $repo = $this->getDoctrine()->getRepository(LinkAdherentSocialGroup::class);

        return $this->render(
            '@TodotodaySocial/Admin/SocialGroup/social_group_members.html.twig',
            [
                'type' => $group instanceof SocialEvent ? 'event' : 'group',
                'members' => $repo->getActiveMembers($group),
            ]
        );
    }
}
