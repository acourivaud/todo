<?php declare(strict_types=1);

namespace Todotoday\SocialBundle\Controller\Admin;

use Todotoday\SocialBundle\Entity\AdminNotification;

/**
 * Class SocialEventAdminController
 */
class SocialEventAdminController extends AbstractSocialGroupAdminController
{
    /**
     * @param int|string|null $id
     *
     * @throws NotFoundHttpException If the object does not exist
     * @throws \RuntimeException     If no editable field is defined
     * @throws AccessDeniedException If access is not granted
     *
     * @return Response|RedirectResponse
     */
    public function editAction($id = null)
    {
        $em = $this->getDoctrine()->getManager();

        $repo = $this->getDoctrine()->getRepository(AdminNotification::class);
        $adminNotification = $repo->findByRelatedId($id, 'event');

        if ($adminNotification != null) {
            $adminNotification->setState(true);
            $em->flush();
        }

        return parent::editAction($id);
    }
}
