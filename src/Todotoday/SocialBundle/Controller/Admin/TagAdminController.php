<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 20/10/17
 * Time: 16:50
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\SocialBundle\Controller\Admin
 *
 * @subpackage Todotoday\SocialBundle\Controller\Admin
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\SocialBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController;

/**
 * Class TagAdminController
 */
class TagAdminController extends CRUDController
{
}
