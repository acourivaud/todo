<?php declare(strict_types = 1);

namespace Todotoday\SocialBundle\Repository;

/**
 * LinkAgencySocialGroupRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class LinkAgencySocialGroupRepository extends \Doctrine\ORM\EntityRepository
{
}
