<?php declare(strict_types=1);

namespace Todotoday\SocialBundle\Repository;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\SocialBundle\Entity\LinkAdherentSocialGroup;
use Todotoday\SocialBundle\Entity\SocialEvent;
use Todotoday\SocialBundle\Entity\SocialGroup;
use Todotoday\SocialBundle\Enum\SocialGroupStatusEnum;

/**
 * Class SocialGroupRepository
 *
 * @package Todotoday\SocialBundle\Repository
 */
class SocialGroupRepository extends EntityRepository
{
    public const REDIS_CACHE_KEY = 'social_group_count_members';

    /**
     * Retourne les groupes que l'adhérent a le droit de voir et surlesquels il ne participe pas encore
     *  - groupe public et status EXITED / INVITED / REFUSED
     *  - groupe prive et status INVITED
     *  - groupe public et status null
     *
     * @param Agency  $agency
     * @param Account $account
     * @param string  $type
     *
     * @return SocialGroup|SocialGroup[]|SocialEvent|SocialEvent[]|null
     */
    public function getAvailableGroups(Agency $agency, Account $account, string $type = null): ?array
    {
        $qb = $this->groupDql($agency, $account, null, $type);
        $status = SocialGroupStatusEnum::notMyGroup();
        $qb->andWhere(
            $qb->expr()->orX(
                $qb->expr()->andX(
                    $qb->expr()->in('lasg2.status', $status),
                    $qb->expr()->eq('g.public', 'true')
                ),
                $qb->expr()->andX(
                    $qb->expr()->eq('lasg2.status', ':status2'),
                    $qb->expr()->eq('g.public', 'false')
                ),
                $qb->expr()->andX(
                    $qb->expr()->isNull('lasg2.status'),
                    $qb->expr()->eq('g.public', 'true')
                )
            )
        )
            ->setParameter('status2', SocialGroupStatusEnum::INVITED);

        return $qb->getQuery()->getResult();
    }

    /**
     * Retourne les groupes auxquels l'adhérent participe
     * -status JOINED / MUTED
     *
     * @param Agency  $agency
     * @param Account $account
     * @param string  $type
     *
     * @return SocialGroup|SocialGroup[]|SocialEvent|SocialEvent[]|null
     */
    public function getMyGroups(Agency $agency, Account $account, string $type = null): ?array
    {
        $qb = $this->groupDql($agency, $account, null, $type);
        $qb->andWhere('lasg2.status IN (:status)')
            ->setParameter(':status', SocialGroupStatusEnum::myGroup());

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Agency  $agency
     * @param Account $account
     *
     * @return SocialGroup|SocialGroup[]|SocialEvent|SocialEvent[]|null
     */
    public function getMyInvits(Agency $agency, Account $account)
    {
        $qb = $this->groupDql($agency, $account);
        $qb->andWhere('lasg2.status IN (:invit)')
            ->setParameter('invit', SocialGroupStatusEnum::INVITED);

        return $qb->getQuery()->getResult();
    }

    /**
     * Retourne le groupe si
     *  -groupe public et status de l'adhérent null
     *  -group privé et status de l'adhérent : JOINED / INVITED / MUTED
     *  -group public et status de l'adhérent : JOINED / INVITED / REFUSED / EXITED / MUTED
     *
     * @param Agency  $agency
     * @param Account $account
     * @param int     $id
     * @param bool    $isGrantedPreview
     *
     * @return null|SocialGroup
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getOneGroup(Agency $agency, Account $account, int $id, bool $isGrantedPreview = false): ?SocialGroup
    {
        $qb = $this->groupDql($agency, $account, $id, null, $isGrantedPreview);

        $qb->andWhere(
            $qb->expr()->orX(
                $qb->expr()->andX(
                    $qb->expr()->in('lasg2.status', SocialGroupStatusEnum::allowWhenPublic()),
                    $qb->expr()->eq('g.public', 'true')
                ),
                $qb->expr()->andX(
                    $qb->expr()->isNull('lasg2.status'),
                    $qb->expr()->eq('g.public', 'true')
                ),
                $qb->expr()->andX(
                    $qb->expr()->in('lasg2.status', SocialGroupStatusEnum::allowWhenPrivate()),
                    $qb->expr()->eq('g.public', 'false')
                )
            )
        );

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param int  $id
     * @param bool $usecache
     *
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\NoResultException
     */
    public function getCountMembers(int $id, bool $usecache = true): int
    {
        $result = $this
        ->getActiveMembers($id)
        ->select('count(g)')
        ->getQuery();

        if ($usecache) {
            $result->useResultCache(
                true,
                3600,
                self::REDIS_CACHE_KEY . $id
            );
        }

        return $result->getSingleScalarResult();
    }

    /**
     * @param Collection $tags
     *
     * @return array|null
     */
    public function findByTags(Collection $tags): ?array
    {
        return $this->createQueryBuilder('social_group')
            ->join('social_group.tags', 't')
            ->where('t in (:tags)')
            ->setParameter('tags', $tags)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param int $idGroup
     *
     * @return array|null
     */
    public function getMembers(int $idGroup): ?array
    {
        return $this->getActiveMembers($idGroup)->getQuery()->getResult();
    }

    /**
     * @param int $idGroup
     *
     * @return array|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getActiveFirstListMembers(int $idGroup): ?array
    {
        return $this->getActiveListMembersQuery($idGroup)->setMaxResults(5)->getQuery()->getResult();
    }

    /**
     * @param int $idGroup
     *
     * @return array|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getActiveListMembers(int $idGroup): ?array
    {
        return $this->getActiveListMembersQuery($idGroup)->getQuery()->getResult();
    }

    /**
     * @param int $idGroup
     *
     * @return QueryBuilder
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getActiveListMembersQuery(int $idGroup): ?QueryBuilder
    {
        return $this->getActiveMembers($idGroup)
            ->leftJoin(Account::class, 'account', 'WITH', 'lasg2.adherent = account.id')
            ->select('account.id, account.firstName, account.lastName');
    }

    /**
     * @param int $idGroup
     *
     * @return SocialGroup
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getListMembers(int $idGroup): ?SocialGroup
    {
        return $this->getActiveMembers($idGroup)->getQuery()->getOneOrNullResult();
    }

    /**
     * @param int $idGroup
     *
     * @return QueryBuilder
     */
    private function getActiveMembers(int $idGroup): QueryBuilder
    {
//        Les groupes sont décloisonnés, a priori plus besoin de filtrer par conciergerie pour avoir les membres
//        $qb = $this->selectAgency($agency);
        $qb = $this->createQueryBuilder('g');
        $qb->innerJoin('g.linkAdherents', 'lasg2')
            ->andWhere('g.id = :id')
            ->andWhere(
                $qb->expr()->in(
                    'lasg2.status',
                    SocialGroupStatusEnum::allowPostComment()
                )
            )
            ->setParameter('id', $idGroup)
            ->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->in(
                        'lasg2.id',
                        $this->getEntityManager()
                            ->getRepository(LinkAdherentSocialGroup::class)
                            ->getGlobalStatus()
                            ->getDQL()
                    ),
                    $qb->expr()->isNull('lasg2.id')
                )
            );

        return $qb;
    }

    /**
     * @param Agency  $agency
     * @param Account $account
     * @param int     $id
     * @param string  $type
     * @param bool    $isGrantedPreview
     *
     * @return QueryBuilder
     */
    private function groupDql(
        Agency $agency,
        Account $account,
        int $id = null,
        string $type = null,
        bool $isGrantedPreview = false
    ): QueryBuilder {

        $qb = $this->selectAgency($agency, $isGrantedPreview);
        $qb->leftJoin('g.linkAdherents', 'lasg2', Join::WITH, 'lasg2.adherent = :adherent')
            ->setParameter('adherent', $account)
            ->select('g, partial lasg2.{id, status}')
            ->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->in(
                        'lasg2.id',
                        $this->getEntityManager()
                            ->getRepository(LinkAdherentSocialGroup::class)
                            ->getLastStatus()
                            ->getDQL()
                    ),
                    $qb->expr()->isNull('lasg2.id')
                )
            );

        if ($type) {
            $qb->andWhere($qb->expr()->isInstanceOf('g', $type));
        }

        $qb->setParameter('adherent', $account)
            ->orderBy('g.id', 'ASC');
        if ($id) {
            $qb->andWhere('g.id = :id')
                ->setParameter('id', $id);
        }

        return $qb;
    }

    /**
     * @param Agency $agency
     * @param bool   $isGrantedPreview
     *
     * @return QueryBuilder
     */
    private function selectAgency(Agency $agency, $isGrantedPreview = false): QueryBuilder
    {
        $qb = $this->createQueryBuilder('g')
            ->join('g.linkAgencies', 'la')
            ->andWhere('la.agency = :agency')
            ->setParameters(
                [
                    'agency' => $agency,
                ]
            );

        if (!$isGrantedPreview) {
            $qb->andWhere('g.enabled = true');
        }

        return $qb;
    }
}
