<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 22/11/17
 * Time: 09:22
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\SocialBundle\Repository
 *
 * @subpackage Todotoday\SocialBundle\Repository
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\SocialBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class UnregisteredSocialGroupInvitationRepository
 */
class UnregisteredSocialGroupInvitationRepository extends EntityRepository
{
}
