<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 26/02/17
 * Time: 16:17
 */

namespace Todotoday\SocialBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Todotoday\SocialBundle\Entity\SocialPluginContent;

/**
 * Class SocialPluginContentRepository
 * @package Todotoday\SocialBundle\Repository
 */
class SocialPluginContentRepository extends EntityRepository
{
    /**
     * @param SocialPluginContent $socialPluginContent
     *
     * @return null|SocialPluginContent
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function socialPluginContentExist(SocialPluginContent $socialPluginContent): ?SocialPluginContent
    {
        return $this->createQueryBuilder('social_plugin_content')
            ->andWhere('social_plugin_content.plugin = :plugin')
            ->andWhere('social_plugin_content.pluginContentId = :pluginContentId')
            ->setParameter('plugin', $socialPluginContent->getPlugin())
            ->setParameter('pluginContentId', $socialPluginContent->getPluginContentId())
            ->getQuery()
            ->getOneOrNullResult();
    }
}
