<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 26/02/17
 * Time: 16:17
 */

namespace Todotoday\SocialBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class CommentRepository
 *
 * @package Todotoday\SocialBundle\Repository
 */
class SocialCommentRepository extends EntityRepository
{
}
