<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 19/07/17
 * Time: 17:56
 */

namespace Todotoday\SocialBundle\Serializer;

use JMS\Serializer\EventDispatcher\Events;
use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\ObjectEvent;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\SocialBundle\Entity\SocialComment;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * Class SocialGroupHandler
 *
 * @package Sonata\CoreBundle\Serializer
 */
class SocialCommentHandler implements EventSubscriberInterface
{

    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * SocialCommentHandler constructor.
     *
     * @param AuthorizationCheckerInterface $authorizationChecker
     */
    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            [
                'event' => Events::PRE_SERIALIZE,
                'format' => 'json',
                'class' => SocialComment::class,
                'method' => 'onPreSerialize',
            ],
        ];
    }

    /**
     * @param ObjectEvent $event
     *
     */
    public function onPreSerialize(ObjectEvent $event): void
    {
        /** @var SocialComment $socialComment */
        $socialComment = $event->getObject();

        if ($socialComment->getAuthor() === null) {
            $adherent = new Adherent();
            $adherent->setFirstName('?')
                ->setLastName('?');

            $socialComment->setAuthor($adherent);
        }

        $isGranted =  ($this->authorizationChecker->isGranted('OWNER', $socialComment)
        || $this->authorizationChecker->isGranted('DELETE', $socialComment));

        $socialComment->setDeletePermission($isGranted);

        $socialComment->setIsAdmin($this->authorizationChecker->isGranted('ROLE_BACKOFFICE', $socialComment));
    }
}
