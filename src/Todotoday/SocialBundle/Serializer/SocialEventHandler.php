<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 19/07/17
 * Time: 17:56
 */

namespace Todotoday\SocialBundle\Serializer;

use Doctrine\Common\Persistence\ObjectManager;
use JMS\Serializer\Context;
use JMS\Serializer\EventDispatcher\Events;
use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\ObjectEvent;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\JsonSerializationVisitor;
use Todotoday\CoreBundle\Services\DomainContextManager;
use Todotoday\SocialBundle\Entity\SocialEvent;
use Todotoday\SocialBundle\Entity\SocialGroup;
use Todotoday\SocialBundle\Event\SocialNewCommentEvent;
use Todotoday\SocialBundle\Serializer\Entity\SocialGroupParticipantNumber;

/**
 * Class SocialEventHandler
 *
 * @package Sonata\CoreBundle\Serializer
 */
class SocialEventHandler extends AbstractSocialGroupHandler implements EventSubscriberInterface
{
    /**
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            [
                'event' => Events::POST_SERIALIZE,
                'format' => 'json',
                'class' => SocialEvent::class,
                'method' => 'onPostSerialize',
            ],
        ];
    }
}
