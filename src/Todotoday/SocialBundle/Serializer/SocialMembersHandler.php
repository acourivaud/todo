<?php declare(strict_types=1);

namespace Todotoday\SocialBundle\Serializer;

use JMS\Serializer\EventDispatcher\Events;
use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\ObjectEvent;
use Todotoday\SocialBundle\Entity\LinkAdherentSocialGroup;

/**
 * Class SocialMembersHandlers
 *
 * @package Sonata\CoreBundle\Serializer
 */
class SocialMembersHandler implements EventSubscriberInterface
{
    /**
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            [
                'event' => Events::POST_SERIALIZE,
                'format' => 'json',
                'class' => LinkAdherentSocialGroup::class,
                'method' => 'onPostSerialize',
            ],
        ];
    }

    public function onPostSerialize(ObjectEvent $event)
    {
        /** @var \JMS\Serializer\JsonSerializationVisitor $visitor */
        $visitor = $event->getVisitor();

        $visitor->setData('social_group', null);
    }
}
