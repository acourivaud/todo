<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 19/07/17
 * Time: 17:56
 */

namespace Todotoday\SocialBundle\Serializer;

use JMS\Serializer\EventDispatcher\Events;
use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use Todotoday\SocialBundle\Entity\SocialGroup;

/**
 * Class SocialGroupHandler
 *
 * @package Sonata\CoreBundle\Serializer
 */
class SocialGroupHandler extends AbstractSocialGroupHandler implements EventSubscriberInterface
{
    /**
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            [
                'event' => Events::POST_SERIALIZE,
                'format' => 'json',
                'class' => SocialGroup::class,
                'method' => 'onPostSerialize',
            ],
        ];
    }
}
