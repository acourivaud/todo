<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 17/11/17
 * Time: 14:19
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\SocialBundle\Serializer
 *
 * @subpackage Todotoday\SocialBundle\Serializer
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\SocialBundle\Serializer;

use Doctrine\Common\Persistence\ObjectManager;
use JMS\Serializer\EventDispatcher\ObjectEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Todotoday\SocialBundle\Entity\SocialEvent;
use Todotoday\SocialBundle\Entity\SocialGroup;

abstract class AbstractSocialGroupHandler
{
    /**
     * @var ObjectManager
     */
    private $manager;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * SocialGroupParticipantNumberHandler constructor.
     *
     * @param ObjectManager         $manager
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(ObjectManager $manager, TokenStorageInterface $tokenStorage)
    {
        $this->manager = $manager;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param ObjectEvent $event
     *
     * @throws \UnexpectedValueException
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function onPostSerialize(ObjectEvent $event): void
    {
        /** @var SocialGroup|SocialEvent $group */
        $group = $event->getObject();

        $repoGroup = $this->manager->getRepository('TodotodaySocialBundle:SocialGroup');
        $linkAdherentRepo = $this->manager->getRepository('TodotodaySocialBundle:LinkAdherentSocialGroup');

        $participants = $repoGroup->getCountMembers($group->getId(), false);
        $event->getVisitor()->addData('participants', $participants);

        $listFirstParticipants = $repoGroup->getActiveFirstListMembers($group->getId());
        $event->getVisitor()->addData('list_first_participants', $listFirstParticipants);

        $listParticipants = $repoGroup->getActiveListMembers($group->getId());
        $event->getVisitor()->addData('list_participants', $listParticipants);

        $isCreator = (($token = $this->tokenStorage->getToken())
            && ($creator = $group->getCreatedBy())
            && $creator->getId() === $token->getUser()->getId());

        if ($token->getUser()->hasRole('ROLE_ADMIN')
            || $token->getUser()->hasRole('ROLE_COMMUNITY_MANAGER')
            || $token->getUser()->hasRole('ROLE_CONCIERGE')
        ) {
            $isCreator = true;
        }

        $event->getVisitor()->addData('isCreator', $isCreator);

        $statusString = null;
        if ($status = $linkAdherentRepo->getLastStatusFromAdherentGroup($token->getUser(), $group)) {
            $statusString = $status->getStatus()->get();
        }

        $event->getVisitor()->addData(
            'status',
            $statusString
        );
    }
}
