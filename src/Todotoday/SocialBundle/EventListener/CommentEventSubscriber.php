<?php declare(strict_types=1);
/**
 * User: Pix
 * Date: 19/02/21
 */


namespace Todotoday\SocialBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Todotoday\SocialBundle\Entity\Comment;
use Todotoday\SocialBundle\Exceptions\SocialShortenURLException;
use Todotoday\SocialBundle\Services\ShortenURLManager;

/**
 * Class CommentEventSubscriber
 *
 * @package Todotoday\PaymentBundle\EventListener
 */
class CommentEventSubscriber implements EventSubscriber
{
    /**
     * @var ShortenURLManager
     */
    private $shortenURLManager;

    /**
     * CommentEventSubscriber constructor.
     * @param ShortenURLManager $shortenURLManager
     */
    public function __construct(ShortenURLManager $shortenURLManager)
    {
        $this->shortenURLManager = $shortenURLManager;
    }

    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return array
     */
    public function getSubscribedEvents(): array
    {
        return array(
            'prePersist',
            'preUpdate',
        );
    }

    /**
     *
     * @param LifecycleEventArgs $args
     *
     * @throws DBALException
     * @throws ORMException
     * @throws SocialShortenURLException
     */
    public function prePersist(LifecycleEventArgs $args): void
    {
        /** @var Comment $entity */
        $entity = $args->getObject();
        if ($entity instanceof Comment) {
            $this->replaceWithShortenURLs($entity);
        }
    }

    /**
     *
     * @param LifecycleEventArgs $args
     *
     * @throws DBALException
     * @throws ORMException
     * @throws SocialShortenURLException
     */
    public function preUpdate(LifecycleEventArgs $args): void
    {
        /** @var Comment $entity */
        $entity = $args->getObject();
        if ($entity instanceof Comment) {
            $this->replaceWithShortenURLs($entity);
        }
    }

    /**
     * Replace long URls with shorten URLs
     *
     * @param Comment $entity
     */
    private function replaceWithShortenURLs(Comment $entity): void
    {
        $commentContent = $entity->getContent();
        if (empty($commentContent)) {
            return;
        }
        $longURLsArray = $this->getLongURLsArray($commentContent);

        if ($longURLsArray) {
            $cleanedLongURLsArray = $this->getCleanedLongURLsArray($longURLsArray);

            if ($cleanedLongURLsArray) {
                $commentNewContent = $commentContent;

                foreach ($cleanedLongURLsArray as $longUrl) {
                    $shortUrl = $this->shortenURLManager->createShortenURL($longUrl);

                    if ($shortUrl['success']) {
                        $commentNewContent = str_replace($longUrl, $shortUrl['shorten_url'], $commentNewContent);
                    }
                }
                $entity->setContent($commentNewContent);
                if ($commentNewContent !== $commentContent) {
                    $entity->setContentRaw($commentContent);
                }

                return;
            }
        }
    }

    /**
     * Extract URLs from comment content
     *
     * @param string $content
     * @return array|null
     */
    private function getLongURLsArray(string $content): ?array
    {
        $pattern = '~[a-z]+://\S+~';
        $longURLsArray = array();

        if (preg_match_all($pattern, $content, $longURLsArray)) {
            return $longURLsArray[0];
        }

        return null;
    }

    /**
     * Clean Urls with shorten domain name
     *
     * @param array $longURLsArray
     * @return array|null
     */
    private function getCleanedLongURLsArray(array $longURLsArray): ?array
    {
        if (is_array($longURLsArray) && (bool) $longURLsArray) {

            /** @todo add $strToFind to config and parameters */
            $strToFind = 'bit.ly';

            return array_filter($longURLsArray, function ($e) use ($strToFind) {
                return (bool) !strpos($e, $strToFind);
            });
        }

        return null;
    }
}
