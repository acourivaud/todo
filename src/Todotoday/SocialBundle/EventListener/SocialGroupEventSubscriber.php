<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 10/11/17
 * Time: 14:53
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\SocialBundle\EventListener
 *
 * @subpackage Todotoday\SocialBundle\EventListener
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\SocialBundle\EventListener;

use Actiane\ApiConnectorBundle\Services\Connections\Microsoft\MicrosoftDynamicsConnection;
use Actiane\MailerBundle\Services\ActianeMailer;
use Actiane\ToolsBundle\Enum\LoggerEnum;
use Doctrine\ORM\EntityManagerInterface;
use Monolog\Logger;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\NotificationBundle\Entity\Api\MicrosoftCRM\Notification;
use Todotoday\SocialBundle\Entity\SocialComment;
use Todotoday\SocialBundle\Entity\SocialEvent;
use Todotoday\SocialBundle\Entity\SocialGroup;
use Todotoday\SocialBundle\Entity\Comment;
use Todotoday\SocialBundle\Entity\AdminNotification;
use Todotoday\SocialBundle\Entity\UnregisteredSocialGroupInvitation;
use Todotoday\SocialBundle\Event\SocialNewGroupEvent;
use Todotoday\SocialBundle\Event\SocialAnonymousNewInviteEvent;
use Todotoday\SocialBundle\Event\SocialNewCommentEvent;
use Todotoday\SocialBundle\Event\SocialNewInvitEvent;
use Todotoday\SocialBundle\Event\SocialNotAdherentNewInvitEvent;

/**
 * Class SocialGroupEventSubscriber
 */
class SocialGroupEventSubscriber implements EventSubscriberInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var MicrosoftDynamicsConnection
     */
    private $connection;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var ActianeMailer
     */
    private $actianeMailer;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var string
     */
    private $scheme;

    /**
     * @var string
     */
    private $domain;

    /**
     * SocialGroupEventSubscriber constructor.
     *
     * @param EntityManagerInterface      $entityManager
     * @param MicrosoftDynamicsConnection $connection
     * @param TranslatorInterface         $translator
     * @param ActianeMailer               $actianeMailer
     * @param TokenStorageInterface       $tokenStorage
     * @param RouterInterface             $router
     * @param Logger                      $logger
     * @param string                      $scheme
     * @param string                      $domain
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        MicrosoftDynamicsConnection $connection,
        TranslatorInterface $translator,
        ActianeMailer $actianeMailer,
        TokenStorageInterface $tokenStorage,
        RouterInterface $router,
        Logger $logger,
        string $scheme,
        string $domain
    ) {
        $this->entityManager = $entityManager;
        $this->connection = $connection;
        $this->translator = $translator;
        $this->actianeMailer = $actianeMailer;
        $this->tokenStorage = $tokenStorage;
        $this->logger = $logger;
        $this->router = $router;
        $this->scheme = $scheme;
        $this->domain = $domain;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents(): array
    {
        return [
            SocialNewGroupEvent::GROUP_ADD => 'addNotificationToSocialGroup',
            SocialNewCommentEvent::COMMENT_ADD => 'addCommentToSocialGroup',
            SocialNewInvitEvent::INVIT_RECEIVED => 'inviteReceivedToSocialGroup',
            SocialNotAdherentNewInvitEvent::INVIT_NOT_ADHERENT_RECEIVED => 'inviteNotAdherentReceivedToSocialGroup',
            SocialAnonymousNewInviteEvent::INVITE_ANONYMOUS_RECEIVED => 'inviteAnonymousReceivedToSocialGroup',
        ];
    }

    /**
     * @param SocialNotAdherentNewInvitEvent $event
     *
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     */
    public function inviteNotAdherentReceivedToSocialGroup(SocialNotAdherentNewInvitEvent $event)
    {
        $group = $event->getGroup();
        $agency = $event->getAgency();
        $email = mb_convert_case($event->getEmail(), MB_CASE_LOWER);

        $repo = $this->entityManager->getRepository(UnregisteredSocialGroupInvitation::class);

        if (!$pendingInvit = $repo->findOneBy(
            [
                'group' => $group,
                'emailCanonical' => $email,
            ]
        )) {
            $pendingInvit = (new UnregisteredSocialGroupInvitation())
                ->setEmailCanonical($email)
                ->setGroup($group);
            $this->entityManager->persist($pendingInvit);
            $this->entityManager->flush();
        }

        $this->sendEmail($agency, $group, $email);
    }

    /**
     * @param \Todotoday\SocialBundle\Event\SocialAnonymousNewInviteEvent $event
     *
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function inviteAnonymousReceivedToSocialGroup(SocialAnonymousNewInviteEvent $event)
    {
        $group = $event->getGroup();
        $agency = $event->getAgency();
        $email = mb_convert_case($event->getEmail(), MB_CASE_LOWER);

        $repo = $this->entityManager->getRepository(UnregisteredSocialGroupInvitation::class);

        if (!$pendingInvit = $repo->findOneBy(
            [
                'group' => $group,
                'emailCanonical' => $email,
            ]
        )) {
            $pendingInvit = (new UnregisteredSocialGroupInvitation())
                ->setEmailCanonical($email)
                ->setGroup($group);
            $this->entityManager->persist($pendingInvit);
            $this->entityManager->flush();
        }

        $this->sendEmail($agency, $group, $email);
    }

    /**
     * @param SocialNewInvitEvent $event
     *
     * @throws \InvalidArgumentException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     */
    public function inviteReceivedToSocialGroup(SocialNewInvitEvent $event)
    {
        $group = $event->getGroup();
        $agency = $event->getAgency();
        $invitedAdherent = $event->getAdherent();
        $keyText = 'notification.new_invit_' . ($group instanceof SocialEvent ? 'event' : 'group');

        $this->sendAdherentEmail($agency, $group, $invitedAdherent->getEmail(), $invitedAdherent->getFullName());

        if (($crmIdAdherent = $invitedAdherent->getCrmIdAdherent())
            && ($crmIdAgency = $agency->getCrmServiceClient())) {
            $this->createNotification(
                'social.group.newComment',
                'Nouvelle invitation à un groupe',
                $keyText,
                $crmIdAdherent,
                $crmIdAgency,
                $group->getTitle(),
                $invitedAdherent
            );
            $this->connection->flush();
        }
    }

    /**
     * @param SocialNewGroupEvent $event
     *
     * @throws \InvalidArgumentException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     */
    public function addNotificationToSocialGroup(SocialNewGroupEvent $event)
    {
        $group = $event->getGroup();
        $agency = $event->getAgency();
        $author = $group->getCreatedBy();

        $object = ($group instanceof SocialEvent) ? 'Nouvelle activité ' : 'Nouveau Groupe ';
        $type = ($group instanceof SocialEvent) ? 'event' : 'group';
        $this->createAdminNotification(
            $object . $group->getTitle(),
            $type,
            $group->getId()
        );

        $this->sendAdminEmail($agency, $group, null, $author);

        $this->connection->flush();
    }

    /**
     * @param \Todotoday\SocialBundle\Event\SocialNewCommentEvent $event
     *
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function addCommentToSocialGroup(SocialNewCommentEvent $event)
    {
        $group = $event->getGroup();
        $agency = $event->getAgency();
        $author = $event->getComment()->getAuthor();
        $comment = $event->getComment();
        $members = $this->entityManager->getRepository(Adherent::class)->getMembersOfSocialGroup($group);
        $keyText = 'notification.new_message_' . ($group instanceof SocialEvent ? 'event' : 'group');

        foreach ($members as $member) {
            if (($crmIdAdherent = $member->getCrmIdAdherent())
                && ($crmIdAgency = $agency->getCrmServiceClient())
                && $member->getId() !== $author->getId()
            ) {
                $this->createNotification(
                    'social.group.newComment',
                    'Nouveau commentaire Groupe',
                    $keyText,
                    $crmIdAdherent,
                    $crmIdAgency,
                    $group->getTitle(),
                    $author
                );
            }
        }

        $this->createAdminNotification(
            'Nouveau commentaire Groupe ' . $group->getTitle(),
            'comment',
            $comment->getId()
        );

        try {
            $this->sendEmailTaggedMembers($agency, $group, $author, $comment);
        } catch (\Exception $e) {
            $this->logger->addError('tagged.email', ['exception' => $e]);
        }

        $this->sendAdminEmail($agency, $group, $comment, $author);

        $this->connection->flush();
    }

    /**
     * @param string  $title
     * @param string  $name
     * @param string  $contentMessageKey
     * @param string  $crmIdAdherent
     * @param string  $crmIdAgency
     * @param string  $groupTitle
     * @param Account $author
     *
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     */
    private function createNotification(
        string $title,
        string $name,
        string $contentMessageKey,
        string $crmIdAdherent,
        string $crmIdAgency,
        string $groupTitle,
        ?Account $author
    ): void {
        $notification = (new Notification())
            ->setApsMotifdelanotification($title)
            ->setApsName($name)
            ->setApsMessagefr(
                $this->translator->trans($contentMessageKey, [
                    '%groupName%' => $groupTitle,
                    '%userName%' => $author->getFullName(),
                ], 'todotoday', 'fr')
            )
            ->setApsMessageen(
                $this->translator->trans($contentMessageKey, [
                    '%groupName%' => $groupTitle,
                    '%userName%' => $author->getFullName(),
                ], 'todotoday', 'en')
            )
            ->setApsRead(100000000)
            ->setApsStatutdelanotif(100000000)
            ->setApsClientadherentValueGuid($crmIdAdherent)
            ->setOwnerIdValueGuid($crmIdAgency);

        $this->connection->persist($notification);
    }

    /**
     * @param string $subject
     * @param string $type
     * @param int    $relatedId
     *
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     */
    private function createAdminNotification(
        string $subject,
        string $type,
        int $relatedId
    ): void {
        $adminNotification = (new AdminNotification())
            ->setSubject($subject)
            ->setType($type)
            ->setRelatedId($relatedId);

        $this->entityManager->persist($adminNotification);
        $this->entityManager->flush();
    }

    /**
     * @param Agency      $agency
     * @param SocialGroup $group
     * @param string      $email
     *
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \Twig_Error_Syntax
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Loader
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     * @throws \Psr\Log\InvalidArgumentException
     */
    private function sendEmail(Agency $agency, SocialGroup $group, string $email)
    {
        $registerUrl = sprintf(
            '%s://%s.%s%s',
            $this->scheme,
            $agency->getSlug(),
            $this->domain,
            $this->router->generate('todotoday_account_registration_new')
        );

        $groupUrl = sprintf(
            '%s://%s.%s%s',
            $this->scheme,
            $agency->getSlug(),
            $this->domain,
            $this->router->generate('todotoday_social_view_group', ['id' => $group->getId()])
        );

        $type = $group instanceof SocialEvent ? 'event' : 'group';

        $vars = [
            'type' => $type,
            'currentUser' => $this->tokenStorage->getToken()->getUser()->getFullName(),
            'groupName' => $group->getTitle(),
            'enrollGroupLink' => $registerUrl,
            'joinGroupLink' => $groupUrl,
        ];

        $bodyTxt =
            $this->actianeMailer->renderTemplate(
                '@TodotodaySocial/Email/social_invit_not_adherent_email.txt.twig',
                $vars
            );

        $bodyHtml =
            $this->actianeMailer->renderTemplate(
                '@TodotodaySocial/Email/social_invit_not_adherent_email.html.twig',
                $vars
            );

        $objectKey = sprintf('notification.new_invit_%s_email_object', $type);

        $object = $this->translator->trans(
            $objectKey,
            ['%groupName%' => $group->getTitle()],
            'todotoday'
        );

        $this->actianeMailer->createMail($email, $object);
        $this->actianeMailer->getMessage()->setBody($bodyHtml, 'text/html')
            ->addPart($bodyTxt, 'text/plain');
        $this->actianeMailer->send();
    }

    /**
     * @param Agency      $agency
     * @param SocialGroup $group
     * @param Comment     $comment
     * @param Account     $author
     *
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \Twig_Error_Syntax
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Loader
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     * @throws \Psr\Log\InvalidArgumentException
     */
    private function sendAdminEmail(Agency $agency, SocialGroup $group, Comment $comment = null, Account $author)
    {
        $groupType = ($group instanceof SocialEvent) ? 'event' : 'group';
        $type = ($comment) ? 'comment' : $groupType;
        $id = ($comment) ?  $comment->getId() :  $group->getId();

        $routers = [
            'comment' => 'admin_todotoday_social_comment_edit',
            'group' => 'admin_todotoday_social_socialgroup_edit',
            'event' => 'admin_todotoday_social_socialevent_edit',
        ];

        $url = sprintf(
            '%s://%s.%s%s',
            $this->scheme,
            $agency->getSlug(),
            $this->domain,
            $this->router->generate($routers[$type], ['id' => $id])
        );

        $this->translator->setLocale('fr');

        $groupTypeTxt = $this->translator->trans(
            sprintf('notification.social_admin_notifications_%s_email_grouptype', $groupType),
            [],
            'todotoday'
        );

        $vars = [
            'authorName' => $author->getFullName(),
            'groupName' => $group->getTitle(),
            'url' => $url,
            'type' => $type,
            'groupType' => $groupTypeTxt,
        ];

        $bodyTxt =
            $this->actianeMailer->renderTemplate(
                '@TodotodaySocial/Email/social_admin_notifications_email.txt.twig',
                $vars
            );

        $bodyHtml =
            $this->actianeMailer->renderTemplate(
                '@TodotodaySocial/Email/social_admin_notifications_email.html.twig',
                $vars
            );

        $objectKey = sprintf('notification.new_%s_email_object', $type);
        $object = $this->translator->trans(
            $objectKey,
            ['%groupName%' => $group->getTitle()],
            'todotoday'
        );

        /** @var AccountRepository $accountRepo */
        $accountRepo = $this->entityManager->getRepository(Account::class);
        $adminAccounts = $accountRepo->findByRole('ROLE_COMMUNITY_MANAGER');

        if (count($adminAccounts) > 0) {
            foreach ($adminAccounts as $adminAccount) {
                if ($author->getId() != $adminAccount->getId()) {
                    $this->actianeMailer->createMail($adminAccount->getEmail(), $object);
                    $this->actianeMailer->getMessage()->setBody($bodyHtml, 'text/html')
                        ->addPart($bodyTxt, 'text/plain');
                    $this->actianeMailer->send();
                }
            }
        }
    }

    /**
     * @param \Todotoday\CoreBundle\Entity\Agency        $agency
     * @param \Todotoday\SocialBundle\Entity\SocialGroup $group
     * @param string                                     $email
     * @param string                                     $fullName
     *
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    private function sendAdherentEmail(Agency $agency, SocialGroup $group, string $email, string $fullName)
    {
        $registerUrl = sprintf(
            '%s://%s.%s%s',
            $this->scheme,
            $agency->getSlug(),
            $this->domain,
            $this->router->generate('todotoday_account_registration_new')
        );

        $groupUrl = sprintf(
            '%s://%s.%s%s',
            $this->scheme,
            $agency->getSlug(),
            $this->domain,
            $this->router->generate('todotoday_social_view_group', ['id' => $group->getId()])
        );

        $type = $group instanceof SocialEvent ? 'event' : 'group';

        $vars = [
            'type' => $type,
            'currentUser' => $this->tokenStorage->getToken()->getUser()->getFullName(),
            'groupName' => $group->getTitle(),
            'userName' => $fullName,
            'enrollGroupLink' => $registerUrl,
            'joinGroupLink' => $groupUrl,
        ];

        $bodyTxt =
            $this->actianeMailer->renderTemplate(
                '@TodotodaySocial/Email/social_invite_adherent_email.txt.twig',
                $vars
            );

        $bodyHtml =
            $this->actianeMailer->renderTemplate(
                '@TodotodaySocial/Email/social_invite_adherent_email.html.twig',
                $vars
            );

        $objectKey = sprintf('notification.new_invit_%s_email_object', $type);

        $object = $this->translator->trans(
            $objectKey,
            ['%groupName%' => $group->getTitle()],
            'todotoday'
        );

        $this->actianeMailer->createMail($email, $object);
        $this->actianeMailer->getMessage()
            ->setBody($bodyHtml, 'text/html')
            ->addPart($bodyTxt, 'text/plain');
        $this->actianeMailer->send();
    }

    /**
     * @param Agency        $agency
     * @param SocialGroup   $group
     * @param Account       $author
     * @param SocialComment $comment
     *
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \Twig_Error_Syntax
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Loader
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     * @throws \Psr\Log\InvalidArgumentException
     */
    private function sendEmailTaggedMembers(
        Agency $agency,
        SocialGroup $group,
        Account $author,
        SocialComment $comment
    ) {
        $members = $comment->getTaggedMembers();

        if (is_null($members)) {
            return;
        }

        $urlGroup = sprintf(
            '%s://%s.%s%s',
            $this->scheme,
            $agency->getSlug(),
            $this->domain,
            $this->router->generate('todotoday_social_view_group', ['id' => $group->getId()])
        );

        $type = $group instanceof SocialEvent ? 'event' : 'group';

        $object = $this->translator->trans(
            'notification.new_tagged_object',
            ['%groupName%' => $group->getTitle()],
            'todotoday'
        );

        /** @var \Todotoday\AccountBundle\Repository\AccountRepository $accountRepo */
        $accountRepo = $this->entityManager->getRepository(Account::class);
        $taggedMembers = $accountRepo->findByIds(explode(',', $members));

        if (count($taggedMembers) > 0) {
            foreach ($taggedMembers as $taggedMember) {
                $vars = [
                    'type' => $type,
                    'groupName' => $group->getTitle(),
                    'author' => $author->getFullName(),
                    'url' => $urlGroup,
                    'firstName' => $taggedMember->getFirstName(),
                ];

                $bodyTxt =
                    $this->actianeMailer->renderTemplate(
                        '@TodotodaySocial/Email/social_comment_tagged_user.txt.twig',
                        $vars
                    );

                $bodyHtml =
                    $this->actianeMailer->renderTemplate(
                        '@TodotodaySocial/Email/social_comment_tagged_user.html.twig',
                        $vars
                    );

                $this->actianeMailer->createMail($taggedMember->getEmail(), $object);
                $this->actianeMailer->getMessage()->setBody($bodyHtml, 'text/html')->addPart($bodyTxt, 'text/plain');
                $this->actianeMailer->send();

                if (method_exists($taggedMember, 'getCrmIdAdherent')
                    && ($crmIdAdherent = $taggedMember->getCrmIdAdherent())
                    && ($crmIdAgency = $agency->getCrmServiceClient())
                    && $taggedMember->getId() !== $author->getId()
                ) {
                    $this->createNotification(
                        'social.group.newComment',
                        'Nouveau tagué Groupe',
                        'notification.new_tagged_in_app',
                        $crmIdAdherent,
                        $crmIdAgency,
                        $group->getTitle(),
                        $author
                    );
                }
            }
        }
    }
}
