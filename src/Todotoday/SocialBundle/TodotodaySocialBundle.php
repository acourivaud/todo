<?php declare(strict_types = 1);

namespace Todotoday\SocialBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class TodotodaySocialBundle
 * @package Todotoday\SocialBundle
 */
class TodotodaySocialBundle extends Bundle
{
}
