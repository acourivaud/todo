<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 22/02/17
 * Time: 09:28
 */

namespace Todotoday\SocialBundle\Tests\Services;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAccountData;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Entity\LinkAccountAgency;
use Todotoday\AccountBundle\Enum\AdherentTypeEnum;
use Todotoday\AccountBundle\Event\RegistrationSuccessEvent;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyData;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\SocialBundle\DataFixtures\ORM\LoadSocialGroupData;
use Todotoday\SocialBundle\Entity\LinkAdherentSocialGroup;
use Todotoday\SocialBundle\Entity\LinkAgencySocialGroup;
use Todotoday\SocialBundle\Entity\SocialEvent;
use Todotoday\SocialBundle\Entity\SocialGroup;
use Todotoday\SocialBundle\Entity\UnregisteredSocialGroupInvitation;
use Todotoday\SocialBundle\Enum\SocialGroupStatusEnum;
use Todotoday\SocialBundle\Exceptions\SocialBadRequestException;
use Todotoday\SocialBundle\Exceptions\SocialForbiddenException;
use Todotoday\SocialBundle\Repository\LinkAdherentSocialGroupRepository;
use Todotoday\SocialBundle\Repository\UnregisteredSocialGroupInvitationRepository;
use Todotoday\SocialBundle\Services\SocialGroupManager;
use Todotoday\SocialBundle\Entity\SocialComment;

/**
 * Class SocialGroupManagerTest
 *
 * @package Todotoday\SocialBundle\Tests\Services
 */
class SocialGroupManagerTest extends WebTestCase
{
    /**
     * @var SocialGroupManager
     */
    private $manager;

    /**
     * @dataProvider adherentProvider
     * @small
     *
     * @param $adherentSlug
     */
    public function testAcceptInvitSuccess($adherentSlug)
    {
        /** @var Adherent $adherent */
        $adherent = $this->getDoctrine()->getManager()->merge(self::getFRR()->getReference($adherentSlug));
        /** @var Agency $agency */
        $agency = $adherent->getLinkAgencies()->first()->getAgency();
        /** @var SocialGroup $group */
        $this->tokenAs($adherent, 'main');
        $group = self::getFRR()->getReference('SocialGroup2_' . $agency->getSlug());
        $result = $this->manager->actionFromUser($agency, SocialGroupStatusEnum::ACCEPTED, $group->getId());
        $this->assertInstanceOf(LinkAdherentSocialGroup::class, $result);
        $this->assertEquals($group->getId(), $result->getSocialGroup()->getId());
        $this->assertEquals($adherent->getId(), $result->getAdherent()->getId());
        $this->assertEquals(SocialGroupStatusEnum::JOINED, $result->getStatus()->get());
    }

    /**
     * @small
     */
    public function testInvitAnotherAdherentSuccess(): void
    {
        /** @var Adherent $adherent0 */
        $adherent0 = self::getFRR()->getReference('adherent0');
        /** @var Agency $agency */
        $agency0 = $adherent0->getLinkAgencies()->first()->getAgency();
        /** @var Adherent $adherent12 */
        $adherent12 = self::getFRR()->getReference('adherent12');

        $this->tokenAs($adherent0, 'main');
        /** @var SocialGroup $group */
        $group = self::getFRR()->getReference('SocialGroup1_demo0');

        $result = $this->manager->actionToUser(
            $agency0,
            SocialGroupStatusEnum::INVITED,
            $group->getId(),
            $adherent12->getEmailCanonical()
        );

        $repo = $this->getDoctrine()->getRepository('TodotodaySocialBundle:LinkAdherentSocialGroup');
        $resultRepo = $repo->findOneBy(
            [
                'adherent' => $adherent12,
                'socialGroup' => $group,
            ]
        );

        $this->assertArrayHasKey('adherents', $result);
        $this->assertCount(1, $result['adherents']);
        $this->assertEquals(SocialGroupStatusEnum::INVITED, $resultRepo->getStatus());
    }

    /**
     * @small
     * @runInSeparateProcess
     */
    public function testInvitUnknownAdherentSuccess(): void
    {
        /** @var Adherent $adherent0 */
        $adherent0 = self::getFRR()->getReference('adherent0');
        /** @var Agency $agency */
        $agency0 = $adherent0->getLinkAgencies()->first()->getAgency();

        $this->tokenAs($adherent0, 'main');
        /** @var SocialGroup $group */
        $group = self::getFRR()->getReference('SocialGroup1_demo0');

        $emailToInvit = ['toto@actiane.com', 'tata@actiane.com'];

        $this->manager->actionToUser(
            $agency0,
            SocialGroupStatusEnum::INVITED,
            $group->getId(),
            $emailToInvit
        );

        /** @var UnregisteredSocialGroupInvitationRepository $repoInvitation */
        $repoInvitation = $this->getDoctrine()->getRepository(UnregisteredSocialGroupInvitation::class);
        $dispatcher = $this->getContainer()->get('event_dispatcher');

        foreach ($emailToInvit as $email) {
            $emailCanonical = mb_convert_case($email, MB_CASE_LOWER);
            /** @var UnregisteredSocialGroupInvitation $result */
            $result = $repoInvitation->findOneBy(
                [
                    'group' => $group,
                    'emailCanonical' => $emailCanonical,
                ]
            );
            $this->assertEquals($email, $result->getEmailCanonical());
            $this->assertEquals($group->getId(), $result->getGroup()->getId());

            $newUser = (new Adherent())
                ->setCustomerGroup(AdherentTypeEnum::getNewInstance(AdherentTypeEnum::ADH))
                ->setEmail($emailCanonical)
                ->setUsername($emailCanonical)
                ->setFirstName('toto')
                ->setLastName('titi')
                ->setPlainPassword('toto')
                ->addLinkAgency(
                    (new LinkAccountAgency())
                        ->setAgency($agency0)
                        ->addRole('ROLE_ADHERENT')
                );

            $em = $this->getDoctrine()->getManager();
            $em->persist($newUser);
            $em->flush();

            $event = new RegistrationSuccessEvent($newUser);
            $dispatcher->dispatch(RegistrationSuccessEvent::USER_REGISTRATION_SUCCESS, $event);

            /** @var LinkAdherentSocialGroupRepository $groupRepo */
            $groupRepo = $this->getDoctrine()->getRepository(LinkAdherentSocialGroup::class);
            $resultStatus = $groupRepo->getLastStatusFromAdherentGroup($newUser, $group);
            $this->assertEquals($group->getId(), $resultStatus->getSocialGroup()->getId());
            $this->assertEquals($newUser->getEmailCanonical(), $resultStatus->getAdherent()->getEmailCanonical());
            $this->assertEquals(SocialGroupStatusEnum::INVITED, $resultStatus->getStatus()->get());
            $resultPendingInvit = $repoInvitation->findOneBy(
                [
                    'group' => $group,
                    'emailCanonical' => $newUser->getUsername(),
                ]
            );
            $this->assertNull($resultPendingInvit);
        }
    }

    /**
     * @small
     */
    public function testBanAdherentSuccess(): void
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Adherent $adherent0 */
        $adherent0 = $em->merge(self::getFRR()->getReference('adherent0'));
        /** @var Agency $agency0 */
        $agency0 = $adherent0->getLinkAgencies()->first()->getAgency();
        /** @var Adherent $adherent12 */
        $adherent12 = $em->merge(self::getFRR()->getReference('adherent12'));

        $link = (new LinkAgencySocialGroup())
            ->setAgency($agency0);

        $group = (new SocialGroup())
            ->addLinkAgency($link)
            ->setCreatedBy($adherent0)
            ->setTitle('test pour bannir pour quelqu\'un')
            ->setDescription('test');

        $em = $this->getDoctrine()->getManager();
        $em->persist($group);
        $em->flush();

        $manager = $this->getContainer()->get('todotoday_social.group_manager');
        $this->tokenAs($adherent0, 'main');
        $manager->actionFromUser($agency0, SocialGroupStatusEnum::JOINED, $group->getId());
        $this->tokenAs($adherent12, 'main');
        // on fait rejoindre au groupe l'adhérent 12
        $manager->actionFromUser($agency0, SocialGroupStatusEnum::JOINED, $group->getId());
        $this->tokenAs($adherent0, 'main');
        $result = $manager->actionToUser(
            $agency0,
            SocialGroupStatusEnum::BANNED,
            $group->getId(),
            [$adherent12->getEmailCanonical()]
        );

        $this->assertArrayHasKey('adherentsBanned', $result);
        $this->assertCount(1, $result['adherentsBanned']);

        $repo = $this->getDoctrine()->getRepository('TodotodaySocialBundle:LinkAdherentSocialGroup');
        $resultRepo = $repo->findOneBy(
            [
                'adherent' => $adherent12,
                'socialGroup' => $group,
            ]
        );

        $this->assertEquals(SocialGroupStatusEnum::BANNED, $resultRepo->getStatus()->get());
    }

    /**
     * @small
     */
    public function testActionToUserWhenNotInGroup()
    {
        $this->expectException(SocialForbiddenException::class);
        $this->expectExceptionMessage('Adherent must belongs to group to perform action invited');
        /** @var Adherent $adherent0 */
        $adherent0 = self::getFRR()->getReference('adherent0');
        /** @var Agency $agency0 */
        $agency0 = self::getFRR()->getReference('agency_demo0');

        $this->tokenAs($adherent0, 'main');
        /** @var SocialGroup $group */
        $group = self::getFRR()->getReference('SocialGroup4_demo0');

        $this->manager->actionToUser($agency0, SocialGroupStatusEnum::INVITED, $group->getId(), ['osef@osef.com']);
    }

    /**
     * @small
     */
    public function testBanFailNotGroupCreator()
    {
        $this->expectException(SocialForbiddenException::class);
        $this->expectExceptionMessage('User must be the creator of the group to ban other adherent');
        /** @var Adherent $adherent0 */
        $adherent0 = self::getFRR()->getReference('adherent0');

        /** @var Agency $agency */
        $agency0 = $adherent0->getLinkAgencies()->first()->getAgency();
        /** @var Adherent $adherent12 */
        $adherent12 = $this->getDoctrine()->getManager()->merge(self::getFRR()->getReference('adherent12'));
        /** @var SocialGroup $group */
        $group = self::getFRR()->getReference('SocialEvent1_demo0');

        $manager = $this->getContainer()->get('todotoday_social.group_manager');
        $this->tokenAs($adherent12, 'main');
        // on fait rejoindre au groupe l'adhérent 12
        $manager->actionFromUser($agency0, SocialGroupStatusEnum::JOINED, $group->getId());
        $this->tokenAs($adherent0, 'main');
        $manager->actionToUser(
            $agency0,
            SocialGroupStatusEnum::BANNED,
            $group->getId(),
            [$adherent12->getEmailCanonical()]
        );
    }

    /**
     * @dataProvider adherentProvider
     * @small
     *
     * @param $adherentSlug
     */
    public function testActionFromUserWithBadAction($adherentSlug)
    {
        /** @var Adherent $adherent */
        $adherent = $this->getDoctrine()->getManager()->merge(self::getFRR()->getReference($adherentSlug));
        /** @var Agency $agency */
        $agency = $adherent->getLinkAgencies()->first()->getAgency();
        /** @var SocialGroup $group */
        $this->tokenAs($adherent, 'main');
        $group = self::getFRR()->getReference('SocialGroup2_' . $agency->getSlug());
        $actions = array_diff(SocialGroupStatusEnum::toArray(), SocialGroupStatusEnum::actionFromUser());
        foreach ($actions as $action) {
            $this->expectException(SocialBadRequestException::class);
            $this->expectExceptionMessage('User can\'t perform action ' . $action . ' to himself');
            $this->manager->actionFromUser($agency, $action, $group->getId());
        }
    }

    /**
     * @small
     */
    public function testAddEventToGroupFailWrongAuthor(): void
    {
        $this->expectException(SocialForbiddenException::class);
        $this->expectExceptionMessage('User can\'t create group to this agency');
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent0');
        /** @var Agency $agency */
        $agency = $adherent->getLinkAgencies()->first()->getAgency();

        /** @var SocialGroup $group */
        $group = self::getFRR()->getReference('SocialGroup1_demo0');
        /** @var SocialEvent $event */
        $event = self::getFRR()->getReference('SocialEvent1_demo0');
        $manager = $this->getContainer()->get('todotoday_social.group_manager');
        $manager->addEventToGroup($adherent, $agency, $group, $event);
    }

    /**
     * @small
     */
    public function testAddEventToAnotherEvent(): void
    {
        $this->expectException(SocialForbiddenException::class);
        $this->expectExceptionMessage('Can\'t add group to another group');
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent0');
        /** @var Agency $agency */
        $agency = $adherent->getLinkAgencies()->first()->getAgency();

        /** @var SocialGroup $group */
        $group = self::getFRR()->getReference('SocialEvent1_demo0');
        /** @var SocialEvent $event */
        $event = self::getFRR()->getReference('SocialEvent2_demo0');
        $manager = $this->getContainer()->get('todotoday_social.group_manager');
        $manager->addEventToGroup($adherent, $agency, $group, $event);
    }

    /**
     * @runInSeparateProcess
     * @small
     */
    public function testAddEventToGroupSuccess(): void
    {
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent0');
        /** @var Agency $agency */
        $agency = $adherent->getLinkAgencies()->first()->getAgency();

        $group = (new SocialGroup())
            ->setDescription('test')
            ->setTitle('test')
            ->setPublic(true)
            ->setCreatedBy($adherent);

        $linkAgency = (new LinkAgencySocialGroup())
            ->setSocialGroup($group)
            ->setAgency($agency);
        $group->addLinkAgency($linkAgency);

        /** @var SocialEvent $event */
        $event = self::getFRR()->getReference('SocialEvent2_demo0');
        $manager = $this->getContainer()->get('todotoday_social.group_manager');
        $manager->addEventToGroup($adherent, $agency, $group, $event);
        $this->assertCount(1, $group->getEvents());
    }

    /**
     * @runInSeparateProcess
     * @small
     */
    public function testAddEventInThePastFail(): void
    {
        $this->expectException(SocialForbiddenException::class);
        $this->expectExceptionMessage('You can only add future event');

        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent0');
        /** @var Agency $agency */
        $agency = $adherent->getLinkAgencies()->first()->getAgency();

        $group = (new SocialGroup())
            ->setDescription('test')
            ->setTitle('test')
            ->setPublic(true)
            ->setCreatedBy($adherent);

        /** @var SocialEvent $event */
        $event = self::getFRR()->getReference('SocialEvent2_demo0');
        $event->setStartDate(new \DateTime('2015-01-01'))
            ->setEndDate(new \DateTime('2015-01-10'));

        $this->manager->addEventToGroup($adherent, $agency, $group, $event);
    }

    /**
     * @small
     */
    public function testAccessToAllGroup(): void
    {
        // TODO DA FUCK le provider ne fonctionne pas avec LoadSocialGroup::getGroups()
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent0');
        /** @var Agency $agency */
        $agency = $adherent->getLinkAgencies()->first()->getAgency();

        $groupSLug = 'SocialGroup1_demo1';
        /** @var SocialGroup $group */
        $group = self::getFRR()->getReference($groupSLug);

        $this->tokenAs($adherent, 'main');
        $this->expectException(SocialForbiddenException::class);
        $this->expectExceptionMessage('User can\'t access this group');
        $this->manager->getOneGroup($agency, $group->getId());
    }

    /**
     * Test when user attempt to perform action to a group that's not in his agencies
     *
     * @small
     */
    public function testGroupActionFromUserWhereGroupIsNotIsHisAgency()
    {
        $this->expectException(SocialForbiddenException::class);
        $this->expectExceptionMessage('User can\'t access this group');
        // reference SocialGroupX_demo0
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent0');
        /** @var Agency $agency */
        $agency = $adherent->getLinkAgencies()->first()->getAgency();
        /** @var SocialGroup $group */

        $this->tokenAs($adherent, 'main');

        $group = self::getFRR()->getReference('SocialGroup1_demo1');
        $this->manager->actionFromUser($agency, 'blabla', $group->getId());
    }

    /**
     * Test when user attempt to perform action to a group that's not in his agencies
     *
     * @small
     */
    public function testGroupActionThatDontMatch()
    {
        $action = SocialGroupStatusEnum::JOINED;
        // reference SocialGroupX_demo0
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent0');
        /** @var Agency $agency */
        $agency = $adherent->getLinkAgencies()->first()->getAgency();
        /** @var SocialGroup $group */
        $group = self::getFRR()->getReference('SocialGroup1_demo0');

        $this->tokenAs($adherent, 'main');

        $this->expectException(SocialBadRequestException::class);
        $this->expectExceptionMessage('Invalid status ' . $action . '. Current status is ' . $action);
        $this->manager->actionFromUser($agency, $action, $group->getId());
    }

    /**
     * Test when user attempt to perform action to a group that's not in his agencies
     *
     * @small
     */
    public function testGroupActionFailedJoinedWhenPrivate()
    {
        // reference SocialGroupX_demo0
        /** @var Adherent $adherent */
        $adherent = $this->getEntityManager()->merge(self::getFRR()->getReference('adherent0'));
        /** @var Agency $agency */
        $agency = $adherent->getLinkAgencies()->first()->getAgency();
        /** @var SocialGroup $group */
        $group = self::getFRR()->getReference('SocialGroup7_demo0');

        $this->tokenAs($adherent, 'main');
        $this->expectException(SocialForbiddenException::class);
        $this->expectExceptionMessage('User can\'t access this group');
        $this->manager->actionFromUser($agency, SocialGroupStatusEnum::JOINED, $group->getId());
    }

    /**
     * Test when user attempt to perform action to a group that's not in his agencies
     *
     * @small
     */
    public function testInvalidActionToAnotherUser()
    {
        $action = SocialGroupStatusEnum::JOINED;
        // reference SocialGroupX_demo0
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent0');
        /** @var Agency $agency */
        $agency = $adherent->getLinkAgencies()->first()->getAgency();
        /** @var SocialGroup $group */
        $group = self::getFRR()->getReference('SocialGroup1_demo0');

        $this->tokenAs($adherent, 'main');
        $this->expectException(SocialBadRequestException::class);
        $this->expectExceptionMessage('User can\'t perform action ' . $action . ' to another user');
        $this->manager->actionToUser($agency, $action, $group->getId(), '');
    }

    /**
     * Test when user attempt to perform invalid action to himself
     *
     * @small
     */
    public function testInvalidActionToHimself()
    {
        $action = SocialGroupStatusEnum::INVITED;
        // reference SocialGroupX_demo0
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent0');
        /** @var Agency $agency */
        $agency = $adherent->getLinkAgencies()->first()->getAgency();
        /** @var SocialGroup $group */
        $group = self::getFRR()->getReference('SocialGroup1_demo0');

        $this->tokenAs($adherent, 'main');
        $this->expectException(SocialBadRequestException::class);
        $this->expectExceptionMessage('User can\'t perform action ' . $action . ' to himself');
        $this->manager->actionFromUser($agency, $action, $group->getId());
    }

    /**
     * @dataProvider groupeTypeProvider
     * @small
     *
     * @param SocialGroup|SocialEvent $class
     */
    public function testCreateSuccessGroupFromAdherent($class)
    {
        /** @var Adherent $adherent */
        $adherent = $this->getEntityManager()->merge(self::getFRR()->getReference('adherent0'));
        /** @var Agency $agency */
        $agency = $adherent->getLinkAgencies()->first()->getAgency();

        /** @var SocialGroup|SocialEvent $group */
        $group = new $class();
        $group
            ->setTitle('Test')
            ->setDescription('Blablabla');

        $this->tokenAs($adherent, 'main');

        $result = $this->manager->addSocialGroup($agency, $group);

        $this->assertEquals($group->getTitle(), $result->getTitle());
        $this->assertEquals($group->getDescription(), $result->getDescription());
    }

    /**
     * @dataProvider groupeTypeProvider
     * @small
     *
     * @param SocialGroup|SocialEvent $class
     */
    public function testCreateFromAdherentInWrongAgency($class)
    {
        /** @var Adherent $adherent */
        $adherent = $this->getEntityManager()->merge(self::getFRR()->getReference('adherent0'));
        /** @var Agency $agency */
        $agency = $this->getEntityManager()->merge(self::getFRR()->getReference('agency_demo1'));

        /** @var SocialGroup|SocialEvent $group */
        $group = new $class();
        $group->setTitle('Test')
            ->setDescription('Blablabla');

        $this->tokenAs($adherent, 'main');

        $this->expectExceptionMessage(SocialForbiddenException::class);
        $this->expectExceptionMessage('User can\'t create group to this agency');
        $this->manager->addSocialGroup($agency, $group);
    }

    /**
     * Test success add commment to group
     *
     * @small
     */
    public function testAddCommentSuccess()
    {
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent0');
        /** @var Agency $agency */
        $agency = self::getFRR()->getReference('agency_demo0');
        /** @var SocialGroup $group */
        $group = self::getFRR()->getReference('SocialGroup1_demo0');

        $comment = new SocialComment();
        $comment->setContent('blabla');

        $this->tokenAs($adherent, 'main');
        $result = $this->manager->addComment($agency, $group->getId(), $comment);

        $this->assertEquals($comment->getContent(), $result->getContent());
        $this->assertEquals($adherent->getUsername(), $result->getAuthor()->getUsername());
        $this->assertEquals($group->getId(), $result->getSocialGroup()->getId());
    }

    /**
     * @small
     */
    public function testMembersInGroup()
    {
        $group = self::getFRR()->getReference('SocialGroup1_demo0');
        $repository = $this->getDoctrine()->getRepository('TodotodaySocialBundle:SocialGroup');
        $data = $repository->getCountMembers($group->getId());
        $this->assertNotNull($data);
    }

    /**
     * Test failed when adding comment to inexistant group
     *
     * @small
     */
    public function testAddCommentInvalidGroupFailed()
    {
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent0');
        /** @var Agency $agency */
        $agency = self::getFRR()->getReference('agency_demo0');

        $comment = new SocialComment();
        $comment->setContent('blabla');

        $this->tokenAs($adherent, 'main');
        $this->expectException(SocialForbiddenException::class);
        $this->expectExceptionMessage('User can\'t access this group');
        $this->manager->addComment($agency, 999, $comment);
    }

    /**
     * do setup
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->manager = $this->getContainer()->get('todotoday_social.group_manager');
    }

    /**
     * Data provider for simple adherent
     *
     * @return array
     */
    public function adherentProvider(): array
    {
        $adherentSlug = LoadAccountData::getAdherentsSlugs();
        $provider = [];
        foreach ($adherentSlug as $adherent) {
            $provider[$adherent] = array($adherent);
        }

        return $provider;
    }

    /**
     * Provider that return all type of groups
     *
     * @return array
     */
    public function groupeTypeProvider(): array
    {
        return array(
            'group' => array(SocialGroup::class),
            'event' => array(SocialEvent::class),
        );
    }

    /**
     * Provider that rreturn all status
     *
     * @return array
     */
    public function statusProvider(): array
    {
        $provider = array();
        foreach (SocialGroupStatusEnum::toArray() as $key => $val) {
            if ($val !== SocialGroupStatusEnum::__EMPTY) {
                $provider[$key] = array($val);
            }
        }

        return $provider;
    }

    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAgencyData::class,
            LoadAccountData::class,
            LoadSocialGroupData::class,
        );
    }
}
