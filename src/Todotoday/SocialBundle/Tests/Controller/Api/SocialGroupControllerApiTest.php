<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 24/02/17
 * Time: 09:34
 */

namespace Todotoday\SocialBundle\Tests\Controller\Api;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAccountData;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyData;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\SocialBundle\DataFixtures\ORM\LoadSocialGroupData;
use Todotoday\SocialBundle\Entity\SocialGroup;

/**
 * Class SocialGroupControllerApiTest
 * @package Todotoday\SocialBundle\Tests\Controller\Api
 */
class SocialGroupControllerApiTest extends WebTestCase
{
    /**
     * @var string
     */
    private $domain;

    /**
     * @dataProvider resultProvider
     * Test we get the 3 groups that belong to the correct agency
     * @small
     *
     * @param array  $route
     * @param string $agencySlug
     * @param string $adherentSlug
     * @param int    $code
     * @param int    $result
     *
     */
    public function testPrettyMuchAllGetRoute(
        array $route,
        string $agencySlug,
        string $adherentSlug,
        int $code,
        ?int $result
    ) {
        /** @var Agency $agency */
        $agency = self::getFRR()->getReference($agencySlug);
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference($adherentSlug);

        $this->loginAs($adherent, 'api');
        $url = $this->getUrl($route['route'], $route['params']);
        $client = $this->makeClient();
        $client->request('GET', $url, array(), array(), array('HTTP_AGENCY' => $agency->getSlug()));
        $this->assertStatusCode($code, $client);
        if ($result) {
            $this->assertCount($result, json_decode($client->getResponse()->getContent()));
        }
    }

    /**
     * Provides (route, agency, adherent, status code expected, number of group expected)
     *
     * @return mixed
     */
    public function resultProvider()
    {
        return array(
            'getAvailableAllSuccess' => array(
                array(
                    'route' => 'todotoday.social.api.social_group.get_available_groups',
                    'params' => array('type' => null),
                ),
                'agency_demo0',
                'adherent0',
                200,
                5,
            ),
            'getAvailableAllFail' => array(
                array(
                    'route' => 'todotoday.social.api.social_group.get_available_groups',
                    'params' => array('type' => null),
                ),
                'agency_demo0',
                'adherent1',
                403,
                null,
            ),
            'getAvailableGroupSuccess' => array(
                array(
                    'route' => 'todotoday.social.api.social_group.get_available_groups',
                    'params' => array('type' => 'group'),
                ),
                'agency_demo0',
                'adherent0',
                200,
                4,
            ),
            'getAvailableGroupFail' => array(
                array(
                    'route' => 'todotoday.social.api.social_group.get_available_groups',
                    'params' => array('type' => 'group'),
                ),
                'agency_demo0',
                'adherent1',
                403,
                null,
            ),
            'getAvailableEventSuccess' => array(
                array(
                    'route' => 'todotoday.social.api.social_group.get_available_groups',
                    'params' => array('type' => 'event'),
                ),
                'agency_demo0',
                'adherent0',
                200,
                1,
            ),
            'getAvailableEventFail' => array(
                array(
                    'route' => 'todotoday.social.api.social_group.get_available_groups',
                    'params' => array('type' => 'event'),
                ),
                'agency_demo0',
                'adherent1',
                403,
                null,
            ),
            'getMyAllSuccess' => array(
                array(
                    'route' => 'todotoday.social.api.social_group.get_my_groups',
                    'params' => array('type' => null),
                ),
                'agency_demo0',
                'adherent0',
                200,
                4,
            ),
            'getMyAllFail' => array(
                array(
                    'route' => 'todotoday.social.api.social_group.get_my_groups',
                    'params' => array('type' => null),
                ),
                'agency_demo0',
                'adherent1',
                403,
                null,
            ),
            'getMyGroupsSuccess' => array(
                array(
                    'route' => 'todotoday.social.api.social_group.get_my_groups',
                    'params' => array('type' => 'group'),
                ),
                'agency_demo0',
                'adherent0',
                200,
                3,
            ),
            'getMyGroupsFail' => array(
                array(
                    'route' => 'todotoday.social.api.social_group.get_my_groups',
                    'params' => array('type' => 'group'),
                ),
                'agency_demo0',
                'adherent1',
                403,
                null,
            ),
            'getMyEventsSuccess' => array(
                array(
                    'route' => 'todotoday.social.api.social_group.get_my_groups',
                    'params' => array('type' => 'event'),
                ),
                'agency_demo0',
                'adherent0',
                200,
                1,
            ),
            'getMyEventsFail' => array(
                array(
                    'route' => 'todotoday.social.api.social_group.get_my_groups',
                    'params' => array('type' => 'event'),
                ),
                'agency_demo0',
                'adherent1',
                403,
                null,
            ),
            'getMyInvitsSuccess' => array(
                array(
                    'route' => 'todotoday.social.api.social_group.get_my_invits',
                    'params' => array(),
                ),
                'agency_demo0',
                'adherent0',
                200,
                2,
            ),
            'getMyInvitsFail' => array(
                array(
                    'route' => 'todotoday.social.api.social_group.get_my_invits',
                    'params' => array(),
                ),
                'agency_demo0',
                'adherent1',
                403,
                null,
            ),
        );
    }

    /**
     * Provider for partner
     *
     * @return array
     */
    public function partnerProvider()
    {
        return array(
            'microsoft' => array('microsoft'),
            'noosphere' => array('noosphere'),
        );
    }

    /**
     * @return array
     */
    public function agencyProvider()
    {
        $provider = [];
        $agenciesSlug = LoadAgencyData::getAgenciesSlug();
        $i = 0;
        foreach ($agenciesSlug as $slug) {
            $provider[$slug] = array(
                $slug,
                'adherent' => LoadAccountData::getAdherentsSlugs(),
                'concierge' => LoadAccountData::getConciergesSlugs(),
            );
            $i++;
        }

        return $provider;
    }

    /**
     * Provider for adherent
     *
     * @return array
     */
    public function adherentConciergeProvider()
    {
        $adherentsSlug = LoadAccountData::getAllAccountsSlugs();
        $provider = [];
        foreach ($adherentsSlug as $adherentSlug) {
            $provider[$adherentSlug] = array($adherentSlug);
        }

        return $provider;
    }

    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAgencyData::class,
            LoadAccountData::class,
            LoadSocialGroupData::class,
        );
    }

    /**
     * Do Setup
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->domain = $this->getContainer()->getParameter('domain');
    }

    /**
     * @param string $url
     */
    private function expectAgencyIsMissing(string $url)
    {
        $client = $this->makeClient();
        $client->request('GET', $url);
        $response = json_decode($client->getResponse()->getContent());
        $this->assertStatusCode(500, $client);
        $this->assertEquals('Agency is missing', $response->error->exception[0]->message);
    }
}
