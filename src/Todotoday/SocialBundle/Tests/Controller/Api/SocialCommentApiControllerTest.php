<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 26/02/17
 * Time: 17:04
 */

namespace Todotoday\SocialBundle\Tests\Controller\Api;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAccountData;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyData;
use Todotoday\SocialBundle\DataFixtures\ORM\LoadSocialCommentData;
use Todotoday\SocialBundle\DataFixtures\ORM\LoadSocialGroupData;
use Todotoday\SocialBundle\Entity\SocialGroup;

/**
 * Class SocialCommentApiControllerTest
 * @package Todotoday\SocialBundle\Tests\Controller\Api
 */
class SocialCommentApiControllerTest extends WebTestCase
{
    /**
     * @var string
     */
    private $domain;

    /**
     * @dataProvider partnerAccountProvider
     *
     * @param string $slug
     */
    public function testPostByPartner(string $slug): void
    {
        /** @var SocialGroup $group */
        $group = self::getFRR()->getReference('SocialGroup1_demo0');
        /** @var Account $account */
        $account = self::getFRR()->getReference($slug);
        $this->loginAs($account, 'api');
        $client = $this->makeClient();
        $url = $this->getUrl(
            'todotoday.social.api.social_group.post_comment',
            array(
                'id' => $group->getId(),
            )
        );
        $client->request('POST', $url);
        $this->assertStatusCode(403, $client);
    }

    /**
     * Get all comments only authorized for microsoft
     *
     * @small
     * @dataProvider allAccountProvider
     *
     * @param string $slug
     */
    public function testGetAllRefusedForAnyAccountExceptMicrosoft(string $slug): void
    {
        if ($slug === 'microsoft') {
            self::assertTrue(true);

            return;
        }
        /** @var Account $user */
        $user = self::getFRR()->getReference($slug);
        $this->loginAs($user, 'api');
        $client = $this->makeClient();
        $url = $this->getUrl('todotoday.social.api.social_comment.get_all');
        $client->request('GET', $url);
        $this->assertStatusCode(403, $client);
    }

    /**
     * Granted for microsoft
     * @small
     */
    public function testGetAllGrantedForMicrosoft(): void
    {
        $comments = LoadSocialCommentData::getComments();
        /** @var Account $user */
        $user = self::getFRR()->getReference('microsoft');
        $this->loginAs($user, 'api');
        $client = $this->makeClient();
        $url = $this->getUrl('todotoday.social.api.social_comment.get_all');
        $client->request('GET', $url);
        $response = json_decode($client->getResponse()->getContent());
        $this->assertStatusCode(200, $client);
        static::assertEquals(count($response), count($comments));
    }

    /**
     * Provider that give all adherent & concierge account
     *
     * @return array
     */
    public function adherentConciergeAccountProvider(): array
    {
        $provider = [];
        foreach (LoadAccountData::getAllAccountsSlugs() as $slug) {
            $provider[$slug] = array($slug);
        }

        return $provider;
    }

    /**
     * Partner provider account
     *
     * @return array
     */
    public function partnerAccountProvider(): array
    {
        $provider = [];
        $accounts = array('noosphere', 'microsoft');
        foreach ($accounts as $account) {
            $provider[$account] = array($account);
        }

        return $provider;
    }

    /**
     * @return array
     */
    public function allAccountProvider(): array
    {
        $provider = [];
        $accounts = array('root', 'admin', 'noosphere', 'microsoft');
        $accounts = array_merge($accounts, LoadAccountData::getAllAccountsSlugs());
        foreach ($accounts as $slug) {
            $provider[$slug] = array($slug);
        }

        return $provider;
    }

    /**
     * Do setup
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->domain = $this->getContainer()->getParameter('domain');
    }

    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAgencyData::class,
            LoadAccountData::class,
            LoadSocialGroupData::class,
            LoadSocialCommentData::class,
        );
    }
}
