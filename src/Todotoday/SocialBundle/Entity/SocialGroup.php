<?php declare(strict_types=1);

namespace Todotoday\SocialBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\MediaBundle\Entity\Media;

/**
 * SocialGroup
 *
 * @ORM\Table(name="social_group", schema="social")
 * @ORM\Entity(repositoryClass="Todotoday\SocialBundle\Repository\SocialGroupRepository")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({
 *     "group" = "Todotoday\SocialBundle\Entity\SocialGroup",
 *     "event" = "Todotoday\SocialBundle\Entity\SocialEvent",
 *     })
 * @Serializer\ExclusionPolicy("all")
 * @Serializer\AccessorOrder("custom",
 *      custom = {
 *          "id","test","title","description","public","status","participants","header_img","createdAt"
 *     })
 * @Serializer\Discriminator(
 *     field = "discr", disabled = false,
 *     map = {
 *      "group": "Todotoday\SocialBundle\Entity\SocialGroup",
 *      "event": "Todotoday\SocialBundle\Entity\SocialEvent"
 *     },
 *     groups={"Social"})
 */
class SocialGroup
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Expose()
     * @Serializer\Groups({"Social", "Default"})
     */
    protected $id;

    /**
     * @ORM\ManyToMany(targetEntity="SocialEvent")
     * @ORM\JoinTable(name="link_social_group_event", schema="social")
     * @ORM\OrderBy({"startDate": "ASC"})
     */
    protected $events;

    /**
     * @var Collection
     * @ORM\ManyToMany(targetEntity="Tag", inversedBy="groups", cascade={"PERSIST"})
     * @ORM\JoinTable(name="link_social_group_tag", schema="social")
     */
    private $tags;

    /**
     * @ORM\OneToMany(targetEntity="LinkAgencySocialGroup", mappedBy="socialGroup",
     *     cascade={"PERSIST"}, orphanRemoval=true)
     * @Assert\Count(min="1",minMessage="social.agency.min.error.message", groups={"admin.submit"})
     */
    protected $linkAgencies;

    /**
     * @ORM\OneToMany(targetEntity="LinkAdherentSocialGroup", mappedBy="socialGroup")
     * @ORM\OrderBy({"id" =  "DESC"})
     */
    protected $linkAdherents;

    /**
     * @ORM\ManyToOne(targetEntity="Todotoday\AccountBundle\Entity\Account", inversedBy="socialGroupsAuthor")
     * @Serializer\Expose()
     * @Serializer\Groups({"Social", "Default"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $createdBy;

    /**
     * @ORM\OneToOne(targetEntity="Todotoday\MediaBundle\Entity\Media", cascade={
     *     "persist",
     *     "refresh",
     *     "remove"
     * })
     * @ORM\JoinColumn(name="header_id", referencedColumnName="id")
     * @Serializer\Expose()
     * @Serializer\Groups({"Social", "Default"})
     * @Assert\NotBlank(groups={"admin.submit"}, message="social.group.no_image")
     */
    protected $headerImg;

    /**
     * @ORM\OneToMany(targetEntity="Todotoday\SocialBundle\Entity\Comment", mappedBy="socialGroup", cascade={"remove"})
     * @ORM\OrderBy({"id" = "DESC"})
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    protected $comments;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="title", type="string", length=255)
     * @Serializer\Expose()
     * @Serializer\Groups({"Social", "Default"})
     */
    protected $title;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="description", type="text")
     * @Serializer\Expose()
     * @Serializer\Groups({"Social", "Default"})
     */
    protected $description;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     * @Serializer\Expose()
     * @Serializer\Groups({"Social", "Default"})
     */
    protected $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     * @Serializer\Expose()
     * @Serializer\Groups({"Social", "Default"})
     */
    protected $updatedAt;

    /**
     * @ORM\Column(name="public", type="boolean", nullable=false)
     * @Serializer\Expose()
     * @Serializer\Groups({"Social", "Default"})
     */
    protected $public = true;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $enabled = true;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->linkAdherents = new ArrayCollection();
        $this->linkAgencies = new ArrayCollection();
        $this->events = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->tags = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->getTitle();
    }

    /**
     * @param SocialComment $comment
     *
     * @return SocialGroup
     */
    public function addComment(SocialComment $comment): self
    {
        $this->comments[] = $comment;
        $comment->setSocialGroup($this);

        return $this;
    }

    /**
     * @param \Todotoday\SocialBundle\Entity\SocialEvent $event
     *
     * @return SocialGroup
     */
    public function addEvent(SocialEvent $event): self
    {
        $this->events[] = $event;

        return $this;
    }

    /**
     * @param LinkAdherentSocialGroup $linkAdherent
     *
     * @return SocialGroup
     */
    public function addLinkAdherent(LinkAdherentSocialGroup $linkAdherent): self
    {
        $this->linkAdherents[] = $linkAdherent;
        $linkAdherent->setSocialGroup($this);

        return $this;
    }

    /**
     * Add linkAgency
     *
     * @param LinkAgencySocialGroup $linkAgency
     *
     * @return SocialGroup
     */
    public function addLinkAgency(LinkAgencySocialGroup $linkAgency): self
    {
        $this->linkAgencies[] = $linkAgency;
        $linkAgency->setSocialGroup($this);

        return $this;
    }

    /**
     * @return Collection|SocialComment[]
     */
    public function getComments(): ?Collection
    {
        return $this->comments;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return SocialGroup
     */
    public function setCreatedAt($createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return Account
     */
    public function getCreatedBy(): ?Account
    {
        return $this->createdBy;
    }

    /**
     * @param Account $createdBy
     *
     * @return SocialGroup
     */
    public function setCreatedBy(Account $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return SocialGroup
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|SocialEvent[]
     */
    public function getEvents(): ?Collection
    {
        return $this->events;
    }

    /**
     * @return Media|null
     */
    public function getHeaderImg(): ?Media
    {
        return $this->headerImg;
    }

    /**
     * @param Media|null $headerImg
     *
     * @return SocialGroup
     */
    public function setHeaderImg(?Media $headerImg): self
    {
        if (!is_null($headerImg)) {
            $this->headerImg = $headerImg;
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @Serializer\VirtualProperty()
     * @Serializer\SerializedName("last_comments")
     * @Serializer\Groups({"Social", "Default"})
     * @Serializer\MaxDepth(3)
     * @return array
     */
    public function getLastComments(): ?array
    {
        return array_values(array_filter(
            \array_slice($this->comments->toArray(), 0, 20),
            function ($comment) {
                /** @var BotComment $comment */

                return (((!$comment instanceof BotComment) || $comment->getPublicationDate() < new \DateTime())
                    && !is_object($comment->getParent()));
            }
        ));
    }

    /**
     * @return Collection|LinkAdherentSocialGroup[]
     */
    public function getLinkAdherents(): ?Collection
    {
        return $this->linkAdherents;
    }

    /**
     * Get linkAgencies
     *
     * @return Collection|LinkAgencySocialGroup[]
     */
    public function getLinkAgencies(): ?Collection
    {
        return $this->linkAgencies;
    }

    /**
     * @return int
     * @Serializer\VirtualProperty()
     * @Serializer\SerializedName("count_message")
     * @Serializer\Groups({"Social", "Default"})
     */
    public function getCountMessage(): int
    {
        $comments = $this->getComments()->filter(function (Comment $comment) {
            return $comment->getParent() === null;
        });

        return \count($comments);
    }

    /**
     * @Serializer\VirtualProperty()
     * @Serializer\SerializedName("futureEvents")
     * @Serializer\Type("array<Todotoday\SocialBundle\Entity\SocialEvent>")
     * @Serializer\Groups({"LinkedEvents"})
     * @return null|Collection
     */
    public function getFuturEvents(): ?Collection
    {
        if ($events = $this->getEvents()) {
            return $events->filter(
                function ($event) {
                    /** @var SocialEvent $event */

                    return $event->getStartDate() > new \DateTime();
                }
            );
        }

        return null;
    }

    /**
     * @Serializer\VirtualProperty()
     * @Serializer\SerializedName("pastEvents")
     * @Serializer\Type("array<Todotoday\SocialBundle\Entity\SocialEvent>")
     * @Serializer\Groups({"LinkedEvents"})
     * @return null|SocialEvent[]
     */
    public function getPastEvents(): ?array
    {
        if ($events = $this->getEvents()) {
            return array_reverse(
                $events->filter(
                    function ($event) {
                        /** @var SocialEvent $event */

                        return $event->getStartDate() <= new \DateTime();
                    }
                )->toArray()
            );
        }

        return null;
    }

    /**
     * @return mixed
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * Set titre
     *
     * @param string $title
     *
     * @return SocialGroup
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt(): ? \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return SocialGroup
     */
    public function setUpdatedAt($updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isPublic(): ?bool
    {
        return $this->public;
    }

    /**
     * @param SocialComment $comment
     *
     * @return SocialGroup
     */
    public function removeComment(SocialComment $comment): self
    {
        $this->comments->removeElement($comment);

        return $this;
    }

    /**
     * @param \Todotoday\SocialBundle\Entity\SocialEvent $event
     *
     * @return SocialGroup
     */
    public function removeEvent(SocialEvent $event): self
    {
        $this->events->removeElement($event);

        return $this;
    }

    /**
     * @param LinkAdherentSocialGroup $linkAdherent
     *
     * @return SocialGroup
     */
    public function removeLinkAdherent(LinkAdherentSocialGroup $linkAdherent): self
    {
        $this->linkAdherents->removeElement($linkAdherent);
        $linkAdherent->setSocialGroup(null);

        return $this;
    }

    /**
     * Remove linkAgency
     *
     * @param LinkAgencySocialGroup $linkAgency
     *
     * @return SocialGroup
     */
    public function removeLinkAgency(LinkAgencySocialGroup $linkAgency): self
    {
        $this->linkAgencies->removeElement($linkAgency);
        $linkAgency->setSocialGroup(null);

        return $this;
    }

    /**
     * @param boolean $public
     *
     * @return SocialGroup
     */
    public function setPublic(bool $public): self
    {
        $this->public = $public;

        return $this;
    }

    /**
     * @param mixed $tags
     *
     * @return SocialGroup
     */
    public function setTags($tags): self
    {
        $this->tags = $tags;

        return $this;
    }

    /**
     * @param Tag $tag
     *
     * @return SocialGroup
     */
    public function addTag(Tag $tag): self
    {
        $this->tags[] = $tag;
        $tag->addGroup($this);

        return $this;
    }

    /**
     * @param Tag $tag
     *
     * @return SocialGroup
     */
    public function removeTag(Tag $tag): self
    {
        $this->tags->removeElement($tag);
        $tag->removeGroup($this);

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     *
     * @return SocialGroup
     */
    public function setEnabled(bool $enabled): SocialGroup
    {
        $this->enabled = $enabled;

        return $this;
    }
}
