<?php declare(strict_types=1);

namespace Todotoday\SocialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;
use Doctrine\Common\Collections\Collection;
use Todotoday\SocialBundle\Entity\SocialComment;

/**
 * Comment
 *
 * @ORM\Table(name="comment", schema="social")
 * @ORM\Entity(repositoryClass="Todotoday\SocialBundle\Repository\CommentRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({
 *     "bot" = "Todotoday\SocialBundle\Entity\BotComment",
 *     "human" = "Todotoday\SocialBundle\Entity\SocialComment"
 * })
 * @Serializer\ExclusionPolicy("all")
 * @Serializer\Discriminator(
 *     field = "discr", disabled = false,
 *     map = {
 *      "bot": "Todotoday\SocialBundle\Entity\BotComment",
 *      "human": "Todotoday\SocialBundle\Entity\SocialComment"
 *     },
 *     groups={"Default","Social"})
 */
abstract class Comment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Expose()
     * @Serializer\Groups({"Default", "Social"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Todotoday\SocialBundle\Entity\SocialGroup", inversedBy="comments")
     */
    private $socialGroup;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"Default", "Social"})
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="content_raw", type="text", nullable=true)
     */
    private $contentRaw;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     * @Serializer\Expose()
     * @Serializer\Groups({"Default", "Social"})
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="deleted", type="boolean")
     */
    private $deleted = false;

    /**
     * @var Comment
     *
     * @ORM\ManyToOne(targetEntity="Todotoday\SocialBundle\Entity\Comment", inversedBy="childrens")
     */
    protected $parent;

    /**
     * @var Collection|Comment[]
     *
     * @ORM\OneToMany(targetEntity="Todotoday\SocialBundle\Entity\Comment", mappedBy="parent", cascade={"persist"})
     * @ORM\OrderBy({"id" = "DESC"})
     */
    protected $childrens;

    /**
     * @var bool
     *
     * @Serializer\Expose()
     * @Serializer\Groups({"Default", "Social"})
     */
    private $deletePermission = false;

    /**
     * @var bool
     *
     * @Serializer\Expose()
     * @Serializer\Groups({"Default", "Social"})
     */
    private $isAdmin = false;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Comment
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set content raw
     *
     * @param string $contentRaw
     *
     * @return Comment
     */
    public function setContentRaw($contentRaw)
    {
        $this->contentRaw = $contentRaw;

        return $this;
    }

    /**
     * Get content raw
     *
     * @return string
     */
    public function getContentRaw()
    {
        return $this->contentRaw;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Comment
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Comment
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return Comment
     */
    public function setDeleted(bool $deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return bool
     */
    public function isDeleted(): bool
    {
        return $this->deleted;
    }

    /**
     * @return SocialGroup
     */
    public function getSocialGroup(): ?SocialGroup
    {
        return $this->socialGroup;
    }

    /**
     * @param SocialGroup $socialGroup
     *
     * @return Comment|BotComment
     */
    public function setSocialGroup(SocialGroup $socialGroup): Comment
    {
        $this->socialGroup = $socialGroup;

        return $this;
    }

    /**
     * @return null|Comment
     */
    public function getParent(): ?self
    {
        return $this->parent;
    }

    /**
     * Set parent
     *
     * @param Comment $parent
     *
     * @return Comment
     */
    public function setParent(?Comment $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get childrens
     *
     * @return Collection|Comment[]
     */
    public function getChildrens(): Collection
    {
        return $this->childrens;
    }

    /**
     * Set childrens
     *
     * @param Collection|Comment[] $childrens
     *
     * @return Comment
     */
    public function setChildren($childrens): self
    {
        $this->childrens = $childrens;

        return $this;
    }

    /**
     * Add child
     *
     * @param Comment $child
     *
     * @return Comment
     */
    public function addChild(Comment $child): self
    {
        $this->childrens[] = $child;
        $child->setParent($this);

        return $this;
    }

    /**
     * Remove child
     *
     * @param Comment $child
     *
     * @return Comment
     */
    public function removeChild(Comment $child): self
    {
        $this->childrens->removeElement($child);
        $child->setParent(null);

        return $this;
    }

    /**
     * @Serializer\VirtualProperty()
     * @Serializer\SerializedName("children_comments")
     * @Serializer\Groups({"Social", "Default"})
     * @Serializer\MaxDepth(2)
     * @return array
     */
    public function getChildrenList(): ?array
    {
        $childrens = array();
        if ($this->childrens) {
            foreach ($this->childrens->toArray() as $child) {
                $childrens[] = $child;
            }
            $childrens = array_slice($childrens, 0, 5);
        }

        return $childrens;
    }

    /**
     * @Serializer\VirtualProperty()
     * @Serializer\SerializedName("children_count")
     * @Serializer\Groups({"Social", "Default"})
     * @return int
     */
    public function getChildrenCount(): ?int
    {
        return ($this->childrens) ? count($this->childrens) : 0;
    }

    /**
     * Set deletePermission
     *
     * @param boolean $deletePermission
     *
     * @return Comment
     */
    public function setDeletePermission(bool $deletePermission)
    {
        $this->deletePermission = $deletePermission;

        return $this;
    }

    /**
     * Set isAdmin
     *
     * @param boolean $isAdmin
     *
     * @return Comment
     */
    public function setIsAdmin(bool $isAdmin)
    {
        $this->isAdmin = $isAdmin;

        return $this;
    }
}
