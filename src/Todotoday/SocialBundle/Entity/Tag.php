<?php declare(strict_types=1);

namespace Todotoday\SocialBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Todotoday\ClassificationBundle\Entity\Collection;
use Todotoday\PluginBundle\Entity\Plugin;

/**
 * Tag
 *
 * @ORM\Table(name="tag", schema="social")
 * @ORM\Entity(repositoryClass="Todotoday\SocialBundle\Repository\TagRepository")
 */
class Tag
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Collection
     * @ORM\ManyToMany(targetEntity="SocialGroup", mappedBy="tags", cascade={"PERSIST"})
     */
    private $groups;

    /**
     * @var Collection
     * @ORM\ManyToMany(targetEntity="Todotoday\PluginBundle\Entity\Plugin", mappedBy="socialTags",
     *     cascade={"persist"})
     */
    private $plugins;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     * @Gedmo\Slug(fields={"name"})
     */
    private $slug;

    /**
     * Tag constructor.
     */
    public function __construct()
    {
        $this->groups = new ArrayCollection();
        $this->plugins = new ArrayCollection();
    }

    /**
     * @return Collection
     */
    public function getGroups(): ?Collection
    {
        return $this->groups;
    }

    /**
     * Get id
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Tag
     */
    public function setName($name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param Collection $groups
     *
     * @return Tag
     */
    public function setGroups(Collection $groups): self
    {
        $this->groups = $groups;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getPlugins(): ?Collection
    {
        return $this->plugins;
    }

    /**
     * @param Collection $plugins
     *
     * @return Tag
     */
    public function setPlugins(Collection $plugins): self
    {
        $this->plugins = $plugins;

        return $this;
    }

    /**
     * @param Plugin $plugin
     *
     * @return Tag
     */
    public function addPlugin(Plugin $plugin): self
    {
        $this->plugins[] = $plugin;

        return $this;
    }

    /**
     * @param Plugin $plugin
     *
     * @return Tag
     */
    public function removePlugin(Plugin $plugin): self
    {
        $this->plugins->removeElement($plugin);

        return $this;
    }

    /**
     * @param SocialGroup $group
     *
     * @return Tag
     */
    public function addGroup(SocialGroup $group): self
    {
        $this->groups[] = $group;

        return $this;
    }

    /**
     * @param SocialGroup $group
     *
     * @return Tag
     */
    public function removeGroup(SocialGroup $group): self
    {
        $this->groups->removeElement($group);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     *
     * @return Tag
     */
    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->slug;
    }
}
