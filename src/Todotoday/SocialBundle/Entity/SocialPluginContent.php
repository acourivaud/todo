<?php declare(strict_types=1);

namespace Todotoday\SocialBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Gedmo\Mapping\Annotation as Gedmo;
use Todotoday\MediaBundle\Entity\Media;
use Todotoday\PluginBundle\Entity\Plugin;

/**
 * SocialPluginContent
 *
 * @ORM\Table(name="social_plugin_content", schema="social")
 * @ORM\Entity(repositoryClass="Todotoday\SocialBundle\Repository\SocialPluginContentRepository")
 * @Serializer\ExclusionPolicy("all")
 */
class SocialPluginContent
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Todotoday\PluginBundle\Entity\Plugin")
     */
    private $plugin;

    /**
     * @var string
     *
     * @ORM\Column(name="plugin_content_id", type="text")
     * @Serializer\Expose()
     */
    private $pluginContentId;

    /**
     * @var string
     *
     * @ORM\Column(name="plugin_url", type="text")
     * @Serializer\Expose()
     */
    private $pluginUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     * @Serializer\Expose()
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="text")
     * @Serializer\Expose()
     */
    private $title;

    /**
     * @ORM\ManyToOne(targetEntity="Todotoday\MediaBundle\Entity\Media", cascade={"persist", "remove", "refresh"})
     * @Assert\NotNull()
     * @Serializer\Expose()
     */
    private $media;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     * @Serializer\Expose()
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param null|string $title
     *
     * @return SocialPluginContent
     */
    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Set content
     *
     * @param null|string $content
     *
     * @return SocialPluginContent
     */
    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return SocialPluginContent
     */
    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return SocialPluginContent
     */
    public function setUpdatedAt(\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @return mixed
     */
    public function getPlugin()
    {
        return $this->plugin;
    }

    /**
     * @param Plugin $plugin
     *
     * @return SocialPluginContent
     */
    public function setPlugin(Plugin $plugin): self
    {
        $this->plugin = $plugin;

        return $this;
    }

    /**
     * @return string
     */
    public function getPluginContentId(): ?string
    {
        return $this->pluginContentId;
    }

    /**
     * @param string $pluginContentId
     *
     * @return SocialPluginContent
     */
    public function setPluginContentId(string $pluginContentId): self
    {
        $this->pluginContentId = $pluginContentId;

        return $this;
    }

    /**
     * @return string
     */
    public function getPluginUrl(): ?string
    {
        return $this->pluginUrl;
    }

    /**
     * @param null|string $pluginUrl
     *
     * @return SocialPluginContent
     */
    public function setPluginUrl(?string $pluginUrl): self
    {
        $this->pluginUrl = $pluginUrl;

        return $this;
    }

    /**
     * @return null|Media
     */
    public function getMedia(): ?Media
    {
        return $this->media;
    }

    /**
     * @param null|Media $media
     *
     * @return SocialPluginContent
     */
    public function setMedia(?Media $media): self
    {
        $this->media = $media;

        return $this;
    }

    /**
     * @return null|string
     */
    public function __toString(): string
    {
        return (string) $this->getTitle();
    }
}
