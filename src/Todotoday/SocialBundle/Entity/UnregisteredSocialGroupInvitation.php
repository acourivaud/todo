<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 22/11/17
 * Time: 09:13
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\SocialBundle\Entity
 *
 * @subpackage Todotoday\SocialBundle\Entity
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\SocialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Todotoday\SocialBundle\Repository\UnregisteredSocialGroupInvitationRepository")
 * @ORM\Table(name="unregistrered_social_group_invitation", schema="social")
 */
class UnregisteredSocialGroupInvitation
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $emailCanonical;

    /**
     * @var SocialGroup
     * @ORM\ManyToOne(targetEntity="Todotoday\SocialBundle\Entity\SocialGroup")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $group;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return UnregisteredSocialGroupInvitation
     */
    public function setId(int $id): UnregisteredSocialGroupInvitation
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmailCanonical(): ?string
    {
        return $this->emailCanonical;
    }

    /**
     * @param string $emailCanonical
     *
     * @return UnregisteredSocialGroupInvitation
     */
    public function setEmailCanonical(string $emailCanonical): UnregisteredSocialGroupInvitation
    {
        $this->emailCanonical = $emailCanonical;

        return $this;
    }

    /**
     * @return SocialGroup|null
     */
    public function getGroup(): ?SocialGroup
    {
        return $this->group;
    }

    /**
     * @param SocialGroup $group
     *
     * @return UnregisteredSocialGroupInvitation
     */
    public function setGroup(SocialGroup $group): UnregisteredSocialGroupInvitation
    {
        $this->group = $group;

        return $this;
    }
}
