<?php declare(strict_types=1);

namespace Todotoday\SocialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\SocialBundle\Enum\SocialGroupStatusEnum;

/**
 * LinkAdherentSocialGroup
 *
 * @ORM\Table(name="link_adherent_social_group", schema="social")
 * @ORM\Entity(repositoryClass="Todotoday\SocialBundle\Repository\LinkAdherentSocialGroupRepository")
 * @Serializer\ExclusionPolicy("all")
 */
class LinkAdherentSocialGroup
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @Serializer\Expose()
     * @Serializer\Groups({"Social", "Default"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Todotoday\AccountBundle\Entity\Account", inversedBy="linkSocialGroups")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @Serializer\Expose()
     * @Serializer\Groups({"Social", "Default"})
     */
    private $adherent;

    /**
     * @ORM\ManyToOne(targetEntity="Todotoday\SocialBundle\Entity\SocialGroup", inversedBy="linkAdherents",
     *     cascade={"persist", "remove"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @Serializer\Expose()
     * @Serializer\Groups({"Social", "Default"})
     */
    private $socialGroup;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     * @Serializer\Expose()
     * @Serializer\Groups({"Social", "Default"})
     */
    private $createdAt;

    /**
     * @ORM\Column(name="status", type="SocialGroupStatusEnum", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"Social", "Default"})
     */
    private $status;

    /**
     * LinkAdherentSocialGroup constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Get account
     *
     * @return Account
     */
    public function getAdherent(): ?Account
    {
        return $this->adherent;
    }

    /**
     * Set account
     *
     * @param Account $account
     *
     * @return LinkAdherentSocialGroup
     */
    public function setAdherent(Account $account = null): self
    {
        $this->adherent = $account;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get group
     *
     * @return SocialGroup
     */
    public function getSocialGroup(): ?SocialGroup
    {
        return $this->socialGroup;
    }

    /**
     * Set group
     *
     * @param SocialGroup $socialGroup
     *
     * @return LinkAdherentSocialGroup
     */
    public function setSocialGroup(SocialGroup $socialGroup = null): self
    {
        $this->socialGroup = $socialGroup;

        return $this;
    }

    /**
     * @return SocialGroupStatusEnum
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param SocialGroupStatusEnum $status
     *
     * @return LinkAdherentSocialGroup
     */
    public function setStatus(SocialGroupStatusEnum $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $date
     *
     * @return self
     */
    public function setCreatedAt(\DateTime $date): self
    {
        $this->createdAt = $date;

        return $this;
    }
}
