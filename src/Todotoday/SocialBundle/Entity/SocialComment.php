<?php declare(strict_types=1);

namespace Todotoday\SocialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\MediaBundle\Entity\Media;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Comment
 *
 * @ORM\Table(name="social_comment", schema="social")
 * @ORM\Entity(repositoryClass="Todotoday\SocialBundle\Repository\SocialCommentRepository")
 * @Serializer\ExclusionPolicy("all")
 */
class SocialComment extends Comment
{
    /**
     * @var Account
     * @ORM\ManyToOne(targetEntity="Todotoday\AccountBundle\Entity\Account", inversedBy="comments")
     * @ORM\JoinColumn(onDelete="SET NULL")
     * @Serializer\Expose()
     * @Serializer\Groups({"Default", "Social"})
     */
    private $author;

    /**
     * @var Media
     *
     * @ORM\ManyToOne(targetEntity="Todotoday\MediaBundle\Entity\Media", cascade={"persist", "remove", "refresh"})
     * @Serializer\Expose()
     * @Serializer\Groups({"Default", "Social"})
     *
     */
    protected $media;

    /**
     * @var string
     *
     * @ORM\Column(name="tagged_members", type="json", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"Default", "Social"})
     */
    protected $taggedMembers;

    /**
     * @var string
     */
    protected $discr = 'human';

    /**
     * @return Account|null
     */
    public function getAuthor(): ?Account
    {
        return $this->author;
    }

    /**
     * @param Account $author
     *
     * @return SocialComment
     */
    public function setAuthor(Account $author): self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return Media|null
     */
    public function getMedia(): ?Media
    {
        return $this->media;
    }

    /**
     * @param mixed $media
     *
     * @return SocialComment
     */
    public function setMedia(?Media $media): SocialComment
    {
        $this->media = $media;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTaggedMembers(): ?string
    {
        return $this->taggedMembers;
    }

    /**
     * @param array $taggedMembers
     *
     * @return SocialComment
     */
    public function setTaggedMembers($taggedMembers): SocialComment
    {
        $this->taggedMembers = $taggedMembers;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDiscr(): ?string
    {
        return $this->discr;
    }
}
