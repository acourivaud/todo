<?php declare(strict_types=1);

namespace Todotoday\SocialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Actiane\ToolsBundle\Constraints\DateInterval;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Event
 *
 * @ORM\Table(name="social_event", schema="social")
 * @ORM\Entity(repositoryClass="Todotoday\SocialBundle\Repository\SocialEventRepository")
 * @DateInterval(startDate="startDate", endDate="endDate")
 */
class SocialEvent extends SocialGroup
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startDate", type="datetime", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"Social", "Default"})
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\DateTime()
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="endDate", type="datetime", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"Social", "Default"})
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\DateTime()
     */
    private $endDate;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"Social", "Default"})
     */
    private $address;

    /**
     * @var int
     *
     * @ORM\Column(name="slot", type="integer", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"Social", "Default"})
     */
    private $capacity;

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return self
     */
    public function setStartDate($startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate(): ?\DateTime
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return self
     */
    public function setEndDate($endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getEndDate(): ?\DateTime
    {
        return $this->endDate;
    }

    /**
     * @param string $address
     *
     * @return SocialEvent
     */
    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * Set slot
     *
     * @param integer $capacity
     *
     * @return self
     */
    public function setCapacity(int $capacity): self
    {
        $this->capacity = $capacity;

        return $this;
    }

    /**
     * Get slot
     *
     * @return int
     */
    public function getCapacity(): ?int
    {
        return $this->capacity;
    }
}
