<?php declare(strict_types=1);

namespace Todotoday\SocialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * AdminNotification
 *
 * @ORM\Table(name="admin_notification", schema="social")
 * @ORM\Entity(repositoryClass="Todotoday\SocialBundle\Repository\AdminNotificationRepository")
 */
class AdminNotification
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="string", length=255)
     */
    private $subject;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdat", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdat;

    /**
     * @var bool
     *
     * @ORM\Column(name="state", type="boolean")
     */
    private $state = false;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var int
     *
     * @ORM\Column(name="related_id", type="integer")
     */
    private $relatedId;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set subject.
     *
     * @param string $subject
     *
     * @return AdminNotification
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject.
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set createdat.
     *
     * @param \DateTime $createdat
     *
     * @return AdminNotification
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat.
     *
     * @return \DateTime
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }

    /**
     * Set state.
     *
     * @param bool $state
     *
     * @return AdminNotification
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state.
     *
     * @return bool
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return AdminNotification
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set relatedId.
     *
     * @param int $relatedId
     *
     * @return AdminNotification
     */
    public function setRelatedId($relatedId)
    {
        $this->relatedId = $relatedId;

        return $this;
    }

    /**
     * Get relatedId.
     *
     * @return int
     */
    public function getRelatedId()
    {
        return $this->relatedId;
    }
}
