<?php declare(strict_types=1);

namespace Todotoday\SocialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * LinkAgencySocialGroup
 *
 * @ORM\Table(name="link_agency_social_group", schema="social")
 * @ORM\Entity(repositoryClass="Todotoday\SocialBundle\Repository\LinkAgencySocialGroupRepository")
 */
class LinkAgencySocialGroup
{
    /**
     * @var SocialGroup
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="SocialGroup", inversedBy="linkAgencies", cascade={"remove"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $socialGroup;

    /**
     * @var Agency
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="Todotoday\CoreBundle\Entity\Agency", inversedBy="linkSocialGroups")
     */
    private $agency;

    /**
     * Set socialGroup
     *
     * @param SocialGroup|null $socialGroup
     *
     * @return LinkAgencySocialGroup
     */
    public function setSocialGroup(?SocialGroup $socialGroup = null): self
    {
        $this->socialGroup = $socialGroup;

        return $this;
    }

    /**
     * Get socialGroup
     *
     * @return SocialGroup|null
     */
    public function getSocialGroup(): ?SocialGroup
    {
        return $this->socialGroup;
    }

    /**
     * Set agency
     *
     * @param Agency $agency
     *
     * @return LinkAgencySocialGroup
     */
    public function setAgency(Agency $agency = null): self
    {
        $this->agency = $agency;

        return $this;
    }

    /**
     * Get agency
     *
     * @return Agency|null
     */
    public function getAgency(): ?Agency
    {
        return $this->agency;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->getAgency()->getId() . '.' . $this->getSocialGroup()->getId();
    }

    /**
     * @return null|string
     */
    public function __toString()
    {
        return (string) $this->getSocialGroup()->getId() . '.' . $this->getAgency()->getSlug();
    }
}
