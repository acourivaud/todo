<?php declare(strict_types=1);

namespace Todotoday\SocialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Todotoday\MediaBundle\Entity\Media;
use Todotoday\PluginBundle\Entity\Plugin;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Comment
 *
 * @ORM\Table(name="bot_comment", schema="social")
 * @ORM\Entity(repositoryClass="Todotoday\SocialBundle\Repository\BotCommentRepository")
 * @Serializer\ExclusionPolicy("all")
 */
class BotComment extends Comment
{
    /**
     * @var Plugin
     * @ORM\ManyToOne(targetEntity="Todotoday\PluginBundle\Entity\Plugin")
     * @Serializer\Expose()
     * @Serializer\Groups({"Default", "Social"})
     * @Serializer\MaxDepth(1)
     */
    private $plugin;

    /**
     * @var Media
     *
     * @ORM\ManyToOne(targetEntity="Todotoday\MediaBundle\Entity\Media", cascade={"persist", "remove", "refresh"})
     * @Assert\NotNull()
     * @Serializer\Expose()
     * @Serializer\Groups({"Default", "Social"})
     *
     */
    private $media;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"Default", "Social"})
     */
    private $url;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $publicationDate;

    /**
     * @var string
     */
    protected $discr = 'bot';

    /**
     * @return string|null
     */
    public function getDiscr(): ?string
    {
        return $this->discr;
    }

    /**
     * @return Plugin|null
     */
    public function getPlugin(): ?Plugin
    {
        return $this->plugin;
    }

    /**
     * @param mixed $plugin
     *
     * @return BotComment
     */
    public function setPlugin(Plugin $plugin): BotComment
    {
        $this->plugin = $plugin;

        return $this;
    }

    /**
     * @return Media|null
     */
    public function getMedia(): ?Media
    {
        return $this->media;
    }

    /**
     * @param mixed $media
     *
     * @return BotComment
     */
    public function setMedia(Media $media): BotComment
    {
        $this->media = $media;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     *
     * @return BotComment
     */
    public function setUrl($url): ?BotComment
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPublicationDate(): ?\DateTime
    {
        return $this->publicationDate;
    }

    /**
     * @param \DateTime $publicationDate
     *
     * @return BotComment
     */
    public function setPublicationDate(\DateTime $publicationDate): BotComment
    {
        $this->publicationDate = $publicationDate;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return 'BotComment.' . $this->getId();
    }
}
