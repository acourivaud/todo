<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 18/03/17
 * Time: 10:02
 */

namespace Todotoday\SocialBundle\Exceptions;

use Actiane\ToolsBundle\Exceptions\PublishedMessageExceptionInterface;

/**
 * Class SocialBadRequestException
 * @package Todotoday\SocialBundle\Exceptions
 */
class SocialEventExpiredException extends \Exception implements PublishedMessageExceptionInterface
{
    /**
     * @return mixed
     */
    public function getMessages()
    {
        parent::getMessage();
    }

    /**
     * @return int
     */
    public function getCustomCode(): int
    {
        return 403;
    }
}
