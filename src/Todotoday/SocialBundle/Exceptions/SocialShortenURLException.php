<?php declare(strict_types=1);
/**
 * User: Pix
 * Date: 22/02/21
 */

namespace Todotoday\SocialBundle\Exceptions;

use Actiane\ToolsBundle\Exceptions\PublishedMessageExceptionInterface;

/**
 * Class SocialShortenURLException
 * @package Todotoday\SocialBundle\Exceptions
 */
class SocialShortenURLException extends \Exception implements PublishedMessageExceptionInterface
{
    /**
     * @return mixed
     */
    public function getMessages()
    {
        parent::getMessage();
    }

    /**
     * @return int
     */
    public function getCustomCode(): int
    {
        return 403;
    }
}
