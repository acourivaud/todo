<?php declare(strict_types=1);

namespace Todotoday\SocialBundle\Admin;

use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Todotoday\SocialBundle\Entity\SocialEvent;
use Sonata\AdminBundle\Route\RouteCollection;

/**
 * Class SocialGroupAdmin
 */
class SocialGroupAdmin extends AbstractSocialGroupAdmin
{
    /**
     * @param string $context
     *
     * @return \Doctrine\ORM\QueryBuilder|\Sonata\AdminBundle\Datagrid\ProxyQueryInterface
     * @throws \UnexpectedValueException
     */
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);

        $query->andWhere(
            $query->getRootAliases()[0] . ' NOT INSTANCE OF TodotodaySocialBundle:SocialEvent'
        );

        return $query;
    }

    /**
     * @param RouteCollection $collection
     */
    public function configureRoutes(RouteCollection $collection)
    {
        parent::configureRoutes($collection);

        $collection->add('preview', $this->getRouterIdParameter() . '/preview');
    }

    /**
     * @param ListMapper $listMapper
     *
     * @throws \RuntimeException
     */
    public function configureListFields(ListMapper $listMapper)
    {
        parent::configureListFields($listMapper);
        $listMapper->remove('startDate')->remove('endDate');
    }

    /**
     * @param ShowMapper $showMapper
     *
     * @throws \RuntimeException
     */
    public function configureShowFields(ShowMapper $showMapper)
    {
        parent::configureShowFields($showMapper);
        $showMapper
            ->remove('startDate')
            ->remove('endDate');
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        parent::configureFormFields($formMapper);

        if ($this->isGranted('ROLE_COMMUNITY_MANAGER')) {
            $formMapper
                ->add(
                    'events',
                    'sonata_type_model',
                    array(
                        'class' => SocialEvent::class,
                        'property' => 'title',
                        'multiple' => true,
                        'required' => false,
                        'label' => 'Activités',
                        'btn_add' => false,
                    )
                );
        }
    }
}
