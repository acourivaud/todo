<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 22/11/17
 * Time: 16:23
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\SocialBundle\Admin
 *
 * @subpackage Todotoday\SocialBundle\Admin
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\SocialBundle\Admin;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\QueryBuilder;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Sonata\MediaBundle\Form\Type\MediaType;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Todotoday\CoreBundle\Admin\MultiAgencyAdminAbstract;
use Todotoday\CoreBundle\Services\DomainContextManager;
use Todotoday\SocialBundle\Entity\LinkAgencySocialGroup;
use Todotoday\SocialBundle\Entity\SocialGroup;
use Sonata\Form\Validator\ErrorElement;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

/**
 * Class AbstractSocialGroupAdmin
 */
class AbstractSocialGroupAdmin extends MultiAgencyAdminAbstract
{
    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @var array
     */
    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'DESC',
        '_sort_by' => 'id',
    );

    /**
     * @var array
     */
    protected $formOptions = [
        'validation_groups' => ['Default', 'admin.submit'],
    ];

    /**
     * AbstractSocialGroupAdmin constructor.
     *
     * @param string                $code
     * @param string                $class
     * @param string                $baseControllerName
     * @param DomainContextManager  $domainContextManager
     * @param ObjectManager         $objectManager
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(
        string $code,
        string $class,
        string $baseControllerName,
        DomainContextManager $domainContextManager,
        ObjectManager $objectManager,
        TokenStorageInterface $tokenStorage
    ) {
        parent::__construct($code, $class, $baseControllerName, $domainContextManager, $objectManager);

        $this->tokenStorage = $tokenStorage;
    }

    /**
     * {@inheritdoc}
     * @throws \UnexpectedValueException
     */
    public function createQuery($context = 'list')
    {
        /** @var ProxyQueryInterface|QueryBuilder $query */
        $query = parent::createQuery($context);

        if ($agency = $this->domainContextManager->getCurrentContext()->getAgency()) {
            $query
                ->join($query->getRootAliases()[0] . '.linkAgencies', 'link_agency_social_group')
                ->andWhere('link_agency_social_group.agency = :agency')
                ->setParameter('agency', $agency);
        }

        return $query;
    }

    /**
     * Do getNewInstance
     *
     * @return SocialGroup
     * @throws \RuntimeException
     * @throws \UnexpectedValueException
     */
    public function getNewInstance(): SocialGroup
    {
        /** @var SocialGroup $socialGroup */
        $socialGroup = parent::getNewInstance();

        $socialGroup->setCreatedBy(
            $this->tokenStorage->getToken()->getUser()
        );

        if (!$socialGroup->getId()
            && $socialGroup->getLinkAgencies()->isEmpty()
            && $agency = $this->domainContextManager->getCurrentContext()->getAgency()
        ) {
            $socialGroup->addLinkAgency(
                $link = (new LinkAgencySocialGroup())
                    ->setSocialGroup($socialGroup)
                    ->setAgency($agency)
            );
        }

        if ($this->isGranted('ROLE_COMMUNITY_MANAGER')
            && $agencies = $this->getAgencyByStrategy(
                $this->getRequest()->get('agencies_strategy')
            )
        ) {
            foreach ($agencies as $agency) {
                $socialGroup->addLinkAgency(
                    $link = (new LinkAgencySocialGroup())
                        ->setSocialGroup($socialGroup)
                        ->setAgency($agency)
                );
            }
        }

        return $socialGroup;
    }

    /**
     * @param RouteCollection $collection
     */
    public function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->add('comments', $this->getRouterIdParameter() . '/comments')
            ->add('members_count', $this->getRouterIdParameter() . '/members-count')
            ->add('members_list', $this->getRouterIdParameter() . '/members-list');
    }

    /**
     * @param ErrorElement            $errorElement
     * @param SocialGroup|SocialEvent $object
     */
    public function validate(ErrorElement $errorElement, $object)
    {
        $errorElement
            ->with('description')
                ->assertLength(['max' => 500])
            ->end()
        ;
    }

    /**
     * @param DatagridMapper $datagridMapper
     *
     * @throws \RuntimeException
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('title')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('tags')
            ->add('enabled')
            ->add('linkAgencies.agency');
    }

    /**
     * @param ListMapper $listMapper
     *
     * @throws \RuntimeException
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('title')
            ->add('createdBy')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('tags')
            ->add('enabled', null, ['editable' => true])
            ->add('startDate')
            ->add('endDate')
            ->add(
                'CountMessage',
                'string',
                ['template' => 'TodotodaySocialBundle:Admin/SocialGroup:social_group_count_message.html.twig']
            )
            ->add(
                'CountMembers',
                'string',
                ['template' => 'TodotodaySocialBundle:Admin/SocialGroup:social_group_count_members.html.twig']
            )
            ->add(
                '_action',
                null,
                array(
                    'actions' => array(
                        'preview' => array(
                            'template' => 'TodotodaySocialBundle:Admin/SocialGroup:preview.html.twig',
                        ),
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    ),
                )
            );
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title')
            ->add('description', TextareaType::class, [
                'label' => 'admin_social_group_description_label',
                'attr' => ['maxlength' => '500'],
            ])
            ->add('tags')
            ->add('enabled')
            ->add(
                'headerImg',
                MediaType::class,
                [
                    'provider' => 'sonata.media.provider.image',
                    'context' => 'social',
                    'label' => 'group_image',
                ]
            );

        if ($this->isGranted('ROLE_COMMUNITY_MANAGER')) {
            $formMapper
                ->add(
                    'linkAgencies',
                    CollectionType::class,
                    array(
                        'by_reference' => false,
                        'label' => 'Agencies',
                    ),
                    array(
                        'edit' => 'inline',
                        'inline' => 'table',
                        'sortable' => 'position',
                    )
                );
        }
    }

    /**
     * @param ShowMapper $showMapper
     *
     * @throws \RuntimeException
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('title')
            ->add('description')
            ->add(
                'headerImg',
                MediaType::class,
                [
                    'provider' => 'sonata.media.provider.image',
                    'context' => 'social',
                    'label' => 'group_image',
                ]
            )
            ->add('enabled')
            ->add('createdBy')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('tags')
            ->add('startDate')
            ->add('endDate');
    }
}
