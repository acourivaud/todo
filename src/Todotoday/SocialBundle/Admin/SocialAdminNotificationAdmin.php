<?php declare(strict_types=1);

namespace Todotoday\SocialBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Todotoday\SocialBundle\Entity\SocialComment;
use Todotoday\SocialBundle\Entity\SocialEvent;
use Todotoday\SocialBundle\Entity\SocialGroup;

/**
 * Class SocialGroupAdmin
 */
class SocialAdminNotificationAdmin extends AbstractAdmin
{
    public $commentRepo;
    public $groupRepo;
    public $eventRepo;

    /**
     * @var array
     */
    protected $datagridValues = [
        '_page' => 1,
        '_sort_order' => 'DESC',
        '_sort_by' => 'id',
    ];

    /**
     * @param ListMapper $list
     *
     * @throws \RuntimeException
     */
    protected function configureListFields(ListMapper $list)
    {

        $list
            ->add('id')
            ->add('subject', null, [
                'label' => 'Title',
                'catalogue' => 'SonataMediaBundle',
            ])
            ->add('type', null, [
                'template' => 'TodotodaySocialBundle:Admin/AdminNotification:admin_notification_type.html.twig',
            ])
            ->add('related_id', null, [
                'label' => 'admin_notification_related_id_label',
                'catalogue' => 'messages',
                'template' => 'TodotodaySocialBundle:Admin/AdminNotification:admin_notification_relatid_id.html.twig',
            ])
            ->add('createdAt', 'datetime')
            ->add('state', null, [
                'label' => 'admin_notification_state_label',
                'catalogue' => 'messages',
            ])
            ->add('_action', null, [
                'actions' => [
                    'change-state' => [
                        'template' => '@TodotodaySocial/CRUD/list__action_change_state.html.twig',
                    ],
                ],
            ]);

        $container = $this->getConfigurationPool()->getContainer();
        $em = $container->get('doctrine.orm.entity_manager');

        $this->commentRepo = $em->getRepository(SocialComment::class);
        $this->groupRepo = $em->getRepository(SocialGroup::class);
        $this->eventRepo = $em->getRepository(SocialEvent::class);
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('delete')
            ->remove('edit')
            ->add('comment', $this->getRouterIdParameter() . '/comment')
            ->add('group', $this->getRouterIdParameter() . '/group')
            ->add('event', $this->getRouterIdParameter() . '/event')
            ->add('change-state', $this->getRouterIdParameter() . '/change-state');
    }

    /**
     * @param array $actions
     *
     * @return array
     */
    protected function configureBatchActions($actions)
    {
        $actions['change-state'] = [
            'ask_confirmation' => true,
            'label' => 'admin_notification_action_change_state',
            'translation_domain' => 'messages',
        ];

        return $actions;
    }

    /**
     * @param DatagridMapper $filter
     *
     * @throws \RuntimeException
     */
    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('state', null, [
                'label' => 'admin_notification_state_label',
                'catalogue' => 'messages',
            ])
            ->add('createdAt', 'doctrine_orm_date_range');
    }
}
