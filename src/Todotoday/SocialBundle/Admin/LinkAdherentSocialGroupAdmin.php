<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 23/11/17
 * Time: 16:07
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\SocialBundle\Admin
 *
 * @subpackage Todotoday\SocialBundle\Admin
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\SocialBundle\Admin;

use Doctrine\ORM\QueryBuilder;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\SocialBundle\Entity\LinkAdherentSocialGroup;

/**
 * Class LinkAdherentSocialGroupAdmin
 */
class LinkAdherentSocialGroupAdmin extends AbstractAdmin
{
    /**
     * @var array
     */
    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'DESC',
        '_sort_by' => 'id',
    );

//    public function createQuery($context = 'list')
//    {
//        /** @var ProxyQueryInterface|QueryBuilder $query */
//        $repo = $this->getConfigurationPool()
//            ->getContainer()
//            ->get('doctrine')
//            ->getRepository(LinkAdherentSocialGroup::class);
//
//        $subQuery = $repo->getLastStatus();
//
//        $query = parent::createQuery();
////        dump($query->getQuery());
//        $query->andWhere($query->expr()->)
//
////            ->join($query->getRootAliases()[0] . '.adherent', 'a')
////            ->andWhere('a.email = :email')
////            ->andWhere($query->getRootAliases()[0].'.adherent.email = :email')
////            ->setParameter('email', 'babar@actiane.com');
////        $query->addSelect('MAX(' . $query->getRootAlias   es()[0] . '.id) maxId')
////            ->groupBy($query->getRootAliases()[0] . '.adherent,' . $query->getRootAliases()[0] . '.socialGroup');
//
//        return $query;
//    }

    /**
     * @param ListMapper $list
     *
     * @throws \RuntimeException
     */
    protected function configureListFields(ListMapper $list)
    {
        $list
            ->add('id')
            ->add('adherent')
            ->add('socialGroup')
            ->add('createdAt')
            ->add('status');
    }

    /**
     * @param DatagridMapper $filter
     *
     * @throws \RuntimeException
     */
    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('adherent.email')
            ->add('adherent.customerAccount', null, ['label' => 'Customer Account'])
            ->add('adherent.lastName', null, ['label' => 'Last Name'])
            ->add('adherent.firstName', null, ['label' => 'First Name'])
            ->add('socialGroup');
    }
}
