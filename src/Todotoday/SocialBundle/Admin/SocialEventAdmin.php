<?php declare(strict_types=1);

namespace Todotoday\SocialBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;
use Sonata\AdminBundle\Route\RouteCollection;

/**
 * Class SocialEventAdmin
 */
class SocialEventAdmin extends AbstractSocialGroupAdmin
{
    /**
     * @param RouteCollection $collection
     */
    public function configureRoutes(RouteCollection $collection)
    {
        parent::configureRoutes($collection);

        $collection->add('preview', $this->getRouterIdParameter() . '/preview');
    }

    /**
     * @param DatagridMapper $datagridMapper
     *
     * @throws \RuntimeException
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        parent::configureDatagridFilters($datagridMapper);
        $datagridMapper
            ->add('startDate', 'doctrine_orm_date_range')
            ->add('endDate', 'doctrine_orm_date_range');
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        parent::configureFormFields($formMapper);
        $formMapper
            ->add(
                'startDate',
                DateTimePickerType::class,
                [
                    'dp_use_seconds' => false,
                    'dp_side_by_side' => true,
                ]
            )
            ->add(
                'endDate',
                DateTimePickerType::class,
                [
                    'dp_use_seconds' => false,
                    'dp_side_by_side' => true,
                ]
            )
            ->add('address')
            ->add('capacity');
    }
}
