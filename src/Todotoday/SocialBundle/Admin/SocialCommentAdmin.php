<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 19/10/17
 * Time: 18:15
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\SocialBundle\Admin
 *
 * @subpackage Todotoday\SocialBundle\Admin
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\SocialBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;
use Sonata\MediaBundle\Form\Type\MediaType;
use Sonata\MediaBundle\Provider\MediaProviderInterface;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Todotoday\SocialBundle\Entity\BotComment;
use Todotoday\SocialBundle\Entity\Comment;
use Todotoday\SocialBundle\Entity\SocialComment;
use Todotoday\PluginBundle\Entity\Plugin;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Exception\AclNotFoundException;
use Todotoday\MediaBundle\Entity\Media;

/**
 * Class SocialCommentAdmin
 */
class SocialCommentAdmin extends AbstractAdmin
{
    /**
     * @var bool
     */
    protected $persistFilters;

    /**
     * @var array
     */
    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'DESC',
        '_sort_by' => 'id',
    );

    /**
     * @var string
     */
    public $mediaProvider;

    /**
     * @param object $object
     */
    public function postUpdate($object)
    {
        if ($object->getMedia()) {
            $this->setAcl($object->getMedia());
        }
    }

    /**
     * @param string $action
     * @param object $object
     *
     * @return array
     */
    public function getActionButtons($action, $object = null)
    {
        $actions = parent::getActionButtons($action, $object);
        if ($action == 'edit') {
            if (!$object->getMedia()) {
                $actions['commentedit'] = [
                    'template' => "TodotodaySocialBundle:Admin/SocialComment:social_comment_edit.html.twig",
                ];
            }
        }

        return $actions;
    }

    /**
     * @param ListMapper $list
     *
     * @throws \RuntimeException
     */
    protected function configureListFields(ListMapper $list)
    {
        $list
            ->add('id')
            ->add('socialGroup')
            ->add('author', null, [
                'header_style' => 'width: 20%',
            ])
            ->add('plugin')
            ->add('content', null, [
                'template' => "TodotodaySocialBundle:Admin/SocialComment:social_comment_content.html.twig",
                'header_style' => 'width: 250px',
            ])
            ->add('discr')
            ->add('createdAt')
            ->add('publicationDate', 'datetime')
            ->add(
                '_action',
                null,
                array(
                    'actions' => array(
                        'group' => array(
                            'template' => 'TodotodaySocialBundle:Admin/SocialComment:group.html.twig',
                        ),
                        'edit' => array(),
                        'delete' => array(
                            'template' => '@TodotodaySocial/CRUD/list__action_comment_delete.html.twig',
                        ),
                    ),
                )
            );
    }

    /**
     * @param FormMapper $form
     *
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     */
    protected function configureFormFields(FormMapper $form)
    {
        /** @var Comment|BotComment|SocialComment $comment */
        $comment = $this->getSubject();

        $providerName = 'sonata.media.provider.image';
        $context = 'social_comments';

        if ($comment && $media = $comment->getMedia()) {
            $providerName = $media->getProviderName();
            $context = $media->getContext();
        } else {
            if ($this->isCurrentRoute('edityoutube')) {
                $providerName = 'sonata.media.provider.youtube';
                $context = 'comments_video';
            }

            if ($this->isCurrentRoute('editvimeo')) {
                $providerName = 'sonata.media.provider.vimeo';
                $context = 'comments_video';
            }
        }

        $formMedia = $form->create(
            'media',
            MediaType::class,
            [
                'provider' => $providerName,
                'context' => $context,
                'required' => false,
            ]
        );

        if ($providerName != 'sonata.media.provider.image') {
            $binaryContentLabel = [
                'label' => 'admin_widget_label_binary_content',
                'translation_domain' => 'messages',
            ];

            if ($comment && $media = $comment->getMedia()) {
                $binaryContentLabel['attr'] = [
                    'value' => $media->getProviderReference(),
                ];
            }

            $formMedia->add('binaryContent', TextType::class, $binaryContentLabel);
        }
        $this->mediaProvider = $providerName;

        /** @var MediaProviderInterface $provider */
        if ($comment && $comment instanceof SocialComment) {
            $form->add(
                'author',
                TextType::class,
                ['disabled' => true]
            );
        } else {
            // BotComment then
            $form
                ->add('socialGroup')
                ->add(
                    'plugin',
                    Plugin::class,
                    ['disabled' => true]
                )
                ->add('publicationDate', DateTimePickerType::class);
        }
        $form->add('content')
            ->add(
                $formMedia
            )
            ->add(
                'createdAt',
                DateTimeType::class,
                ['disabled' => true, 'widget' => 'single_text', 'format' => \IntlDateFormatter::FULL]
            )
            ->add(
                'updatedAt',
                DateTimeType::class,
                ['disabled' => true, 'widget' => 'single_text', 'format' => \IntlDateFormatter::FULL]
            );
    }

    /**
     * @param DatagridMapper $filter
     *
     * @throws \RuntimeException
     */
    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('socialGroup')
            ->add('socialGroup.linkAgencies.agency')
            ->add(
                'type',
                'doctrine_orm_class',
                array('sub_classes' => $this->getSubClasses())
            )
            ->add('createdAt', 'doctrine_orm_date_range');
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('create')
            ->add('group', $this->getRouterIdParameter() . '/group')
            ->add('selectmedias', $this->getRouterIdParameter() . '/selectmedias')
            ->add('edityoutube', $this->getRouterIdParameter() . '/edityoutube')
            ->add('editvimeo', $this->getRouterIdParameter() . '/editvimeo');
    }

    /**
     * @param Media $media
     *
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException
     */
    private function setAcl($media)
    {
        $aclProvider = $this->getConfigurationPool()->getContainer()->get('security.acl.provider');
        $objectIdentity = ObjectIdentity::fromDomainObject($media);
        try {
            $aclProvider->findAcl($objectIdentity);
        } catch (AclNotFoundException $e) {
            $acl = $aclProvider->createAcl($objectIdentity);
            $aclProvider->updateAcl($acl);
        }
    }
}
