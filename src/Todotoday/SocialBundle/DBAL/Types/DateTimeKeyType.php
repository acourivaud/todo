<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 25/02/17
 * Time: 17:09
 */

namespace Todotoday\SocialBundle\DBAL\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\DateTimeType;

/**
 * Class DateTimeKeyType
 * @package Todotoday\SocialBundle\DBAL\Types
 */
class DateTimeKeyType extends DateTimeType
{
    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return DateTimeKey
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $dateTime = parent::convertToPHPValue($value, $platform);

        if (!$dateTime) {
            return $dateTime;
        }

        return new DateTimeKey('@' . $dateTime->format('U'));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'datetime_key';
    }
}
