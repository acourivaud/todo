<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 25/02/17
 * Time: 11:23
 */

namespace Todotoday\SocialBundle\DBAL\Types;

/**
 * Class DateTimeKey
 * @package Todotoday\SocialBundle\DBAL\Types
 */
class DateTimeKey extends \DateTime
{
    /**
     * @return string
     */
    public function __toString()
    {
        return $this->format('c');
    }
}
