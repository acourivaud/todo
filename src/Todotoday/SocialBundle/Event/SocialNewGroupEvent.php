<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\SocialBundle\Event
 *
 * @subpackage Todotoday\SocialBundle\Event
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\SocialBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\SocialBundle\Entity\SocialGroup;

/**
 * Class SocialGroupEvent
 */
class SocialNewGroupEvent extends Event
{
    public const GROUP_ADD = 'group.group_add';

    /**
     * @var SocialGroup
     */
    private $group;

    /**
     * @var Agency
     */
    private $agency;

    /**
     * SocialGroupEvent constructor.
     *
     * @param SocialGroup $group
     * @param Agency      $agency
     */
    public function __construct(SocialGroup $group, Agency $agency)
    {
        $this->group = $group;
        $this->agency = $agency;
    }

    /**
     * @return SocialGroup
     */
    public function getGroup(): SocialGroup
    {
        return $this->group;
    }

    /**
     * @return Agency
     */
    public function getAgency(): Agency
    {
        return $this->agency;
    }
}
