<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 13/11/17
 * Time: 14:27
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\SocialBundle\Event
 *
 * @subpackage Todotoday\SocialBundle\Event
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\SocialBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\SocialBundle\Entity\SocialGroup;

/**
 * Class SocialNewInvitEvent
 */
class SocialNewInvitEvent extends Event
{
    public const INVIT_RECEIVED = 'group.invit_received';

    /**
     * @var SocialGroup
     */
    private $group;

    /**
     * @var Adherent
     */
    private $adherent;

    /**
     * @var Agency
     */
    private $agency;

    /**
     * SocialNewInvitEvent constructor.
     *
     * @param SocialGroup $group
     * @param Adherent    $adherent
     * @param Agency      $agency
     */
    public function __construct(SocialGroup $group, Adherent $adherent, Agency $agency)
    {
        $this->group = $group;
        $this->adherent = $adherent;
        $this->agency = $agency;
    }

    /**
     * @return SocialGroup
     */
    public function getGroup(): SocialGroup
    {
        return $this->group;
    }

    /**
     * @return Adherent
     */
    public function getAdherent(): Adherent
    {
        return $this->adherent;
    }

    /**
     * @return Agency
     */
    public function getAgency(): Agency
    {
        return $this->agency;
    }
}
