<?php declare(strict_types=1);

namespace Todotoday\SocialBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\SocialBundle\Entity\SocialGroup;

/**
 * Class SocialAnonymousNewInviteEvent
 */
class SocialAnonymousNewInviteEvent extends Event
{
    public const INVITE_ANONYMOUS_RECEIVED = 'group.invite_anonymous_received';

    /**
     * @var SocialGroup
     */
    private $group;

    /**
     * @var string
     */
    private $email;

    /**
     * @var Agency
     */
    private $agency;

    /**
     * SocialAnonymousNewInviteEvent constructor.
     *
     * @param SocialGroup $group
     * @param string      $email
     * @param Agency      $agency
     */
    public function __construct(SocialGroup $group, string $email, Agency $agency)
    {
        $this->group = $group;
        $this->email = $email;
        $this->agency = $agency;
    }

    /**
     * @return SocialGroup
     */
    public function getGroup(): SocialGroup
    {
        return $this->group;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return Agency
     */
    public function getAgency(): Agency
    {
        return $this->agency;
    }
}
