<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 10/11/17
 * Time: 14:49
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\SocialBundle\Event
 *
 * @subpackage Todotoday\SocialBundle\Event
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\SocialBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\SocialBundle\Entity\SocialComment;
use Todotoday\SocialBundle\Entity\SocialGroup;

/**
 * Class SocialGroupEvent
 */
class SocialNewCommentEvent extends Event
{
    public const COMMENT_ADD = 'group.comment_add';
    public const INVIT_RECEIVED = 'group.invit_received';

    /**
     * @var SocialGroup
     */
    private $group;

    /**
     * @var SocialComment
     */
    private $comment;

    /**
     * @var Agency
     */
    private $agency;

    /**
     * SocialGroupEvent constructor.
     *
     * @param SocialGroup   $group
     * @param SocialComment $comment
     * @param Agency        $agency
     */
    public function __construct(SocialGroup $group, SocialComment $comment, Agency $agency)
    {
        $this->group = $group;
        $this->comment = $comment;
        $this->agency = $agency;
    }

    /**
     * @return SocialGroup
     */
    public function getGroup(): SocialGroup
    {
        return $this->group;
    }

    /**
     * @return SocialComment
     */
    public function getComment(): SocialComment
    {
        return $this->comment;
    }

    /**
     * @return Agency
     */
    public function getAgency(): Agency
    {
        return $this->agency;
    }
}
