<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 24/02/17
 * Time: 15:49
 */

namespace Todotoday\SocialBundle\Enum;

use Actiane\ToolsBundle\Enum\AbstractEnum;

/**
 * Class SocialGroupStatusEnum
 *
 * @package Todotoday\SocialBundle\Enum
 */
class SocialGroupStatusEnum extends AbstractEnum
{
    public const JOINED = 'joined';
    public const INVITED = 'invited';
    public const REFUSED = 'refused';
    public const ACCEPTED = 'accepted';
    public const EXITED = 'exited';
    public const MUTED = 'muted';
    public const BANNED = 'banned';
    public const __EMPTY = 'empty';

    /**
     * Action that can be performed by concierge or adherent to another adherent
     *
     * @return array
     */
    public static function actionToUsers(): array
    {
        return array(
            self::INVITED,
            self::BANNED,
        );
    }

    /**
     * Action that can be performed by adherent to himself
     *
     * @return array
     */
    public static function actionFromUser(): array
    {
        return array(
            self::JOINED,
            self::REFUSED,
            self::ACCEPTED,
            self::EXITED,
            self::MUTED,
        );
    }

    /**
     * Define status that allow posting a comment
     *
     * @return array
     */
    public static function allowPostComment(): array
    {
        return array(
            self::JOINED,
            self::MUTED,
        );
    }

    /**
     * Status that grant access to group when public
     *
     * @return array
     */
    public static function allowWhenPublic(): array
    {
        return array(
            self::JOINED,
            self::INVITED,
            self::REFUSED,
            self::EXITED,
            self::MUTED,
        );
    }

    /**
     * Status that grant acces to group when private
     *
     * @return array
     */
    public static function allowWhenPrivate(): array
    {
        return array(
            self::JOINED,
            self::INVITED,
            self::MUTED,
        );
    }

    /**
     * Status that don't give belonging to a group
     *
     * @return array
     */
    public static function notMyGroup(): array
    {
        return array(
            self::EXITED,
            self::INVITED,
            self::REFUSED,
        );
    }

    /**
     * Status needed to participate to a group
     *
     * @return array
     */
    public static function myGroup(): array
    {
        return array(
            self::JOINED,
            self::MUTED,
        );
    }

    /**
     * @return array
     */
    public static function nextStatus(): array
    {
        return array(
            self::JOINED => array(
                self::EXITED,
                self::MUTED,
                self::BANNED,
            ),
            self::INVITED => array(
                self::ACCEPTED,
                self::REFUSED,
            ),
            self::REFUSED => array(
                self::INVITED,
                self::JOINED,
            ),
            self::EXITED => array(
                self::JOINED,
                self::INVITED,
            ),
            self::MUTED => array(
                self::ACCEPTED,
                self::JOINED,
            ),
            self::ACCEPTED => array(
                self::JOINED,
            ),
            self::BANNED => array(
                self::INVITED,
            ),
            self::__EMPTY => array(
                self::JOINED,
                self::INVITED,
            ),
        );
    }

    /**
     * @return array
     */
    public static function changeCountMembers(): array
    {
        return [
            self::JOINED,
            self::BANNED,
            self::EXITED,
        ];
    }
}
