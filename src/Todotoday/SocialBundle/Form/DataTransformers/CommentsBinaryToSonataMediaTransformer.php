<?php declare(strict_types=1);

namespace Todotoday\SocialBundle\Form\DataTransformers;

use Sonata\MediaBundle\Entity\MediaManager;
use Symfony\Component\Config\Definition\Exception\InvalidTypeException;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\HttpFoundation\File\File;
use Todotoday\MediaBundle\Entity\Media;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Exception\AclNotFoundException;
use Todotoday\SocialBundle\Exceptions\SocialYoutubeVideoException;

class CommentsBinaryToSonataMediaTransformer implements DataTransformerInterface
{
    /**
     * @var MediaManager
     */
    private $mediaManager;

    /**
     * BinaryToSonataMediaTransformer constructor.
     *
     * @param MediaManager $mediaManager
     */
    public function __construct(MediaManager $mediaManager)
    {
        $this->mediaManager = $mediaManager;
    }

    /**
     * @param mixed $media
     *
     * @return mixed
     */
    public function transform($media)
    {
        return $media;
    }

    /**
     * @param mixed $binary
     *
     * @return null|Media
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException
     */
    public function reverseTransform($binary)
    {
        if (!$binary) {
            return null;
        }

        return $this->guessMediaType($binary);
    }

    /**
     * @param mixed $data
     *
     * @return null|Media
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException
     */
    private function guessMediaType($data)
    {
        if ($this->isImage($data)) {
            return $this->insertImage($data);
        } else {
            return $this->videoType($data);
        }
    }

    /**
     * @param mixed $data
     *
     * @return bool
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException
     */
    private function isImage($data)
    {
        if (strpos($data, 'data:image') === 0) {
            return true;
        }

        return false;
    }

    /**
     * @param Media $media
     *
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException
     */
    private function setAcl($media)
    {
        global $kernel;

        $aclProvider = $kernel->getContainer()->get('security.acl.provider');
        $objectIdentity = ObjectIdentity::fromDomainObject($media);
        try {
            $aclProvider->findAcl($objectIdentity);
        } catch (AclNotFoundException $e) {
            $acl = $aclProvider->createAcl($objectIdentity);
            $aclProvider->updateAcl($acl);
        }
    }

    /**
     * @param mixed $binary
     *
     * @return Media
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException
     */
    private function insertImage($binary): Media
    {
        $data = explode(',', $binary);
        $response = base64_decode($data[1]);
        $name = uniqid('social_comments_', true);
        $path = sys_get_temp_dir() . '/' . $name;
        $file = fopen($path, "wb");
        fwrite($file, $response);
        fclose($file);
        $mediaInfos = getimagesize($path);

        $header = new File($path);
        $media = new Media();
        $media->setName($name);
        $media->setBinaryContent($header);
        $media->setWidth($mediaInfos[0]);
        $media->setHeight($mediaInfos[1]);
        $this->mediaManager->save($media, 'social_comments', 'sonata.media.provider.image');

        $this->setAcl($media);

        return $media;
    }

    /**
     * @param string $url
     *
     * @return Media
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException
     */
    private function isYoutube($url)
    {
        try {
            $media = new Media();
            $media->setBinaryContent($url);
            $media->setEnabled(true);
            $media->setName($url);
            $this->mediaManager->save($media, 'comments_video', 'sonata.media.provider.youtube');

            $this->setAcl($media);

            return $media;
        } catch (\Exception $e) {
            throw new SocialYoutubeVideoException();
        }
    }

    /**
     * @param string $url
     *
     * @return Media
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException
     */
    private function isVimeo($url)
    {
        try {
            $media = new Media();
            $media->setBinaryContent($url);
            $media->setEnabled(true);
            $media->setName($url);
            $this->mediaManager->save($media, 'comments_video', 'sonata.media.provider.vimeo');

            $this->setAcl($media);

            return $media;
        } catch (\Exception $e) {
            throw new SocialYoutubeVideoException();
        }
    }

    /**
     * @param string $url
     *
     * @return null|Media
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException
     */
    private function videoType($url)
    {
        if (strpos($url, 'youtube') > 0 || strpos($url, 'youtu.be') > 0) {
            return $this->isYoutube($url);
        } elseif (strpos($url, 'vimeo') > 0) {
            return $this->isVimeo($url);
        } else {
            return null;
        }
    }
}
