<?php declare(strict_types=1);

namespace Todotoday\SocialBundle\Form\DataTransformers;

use Sonata\MediaBundle\Entity\MediaManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\Stream;
use Todotoday\MediaBundle\Entity\Media;

class BinaryToSonataMediaTransformer implements DataTransformerInterface
{
    /**
     * @var MediaManager
     */
    private $mediaManager;

    /**
     * BinaryToSonataMediaTransformer constructor.
     *
     * @param MediaManager $mediaManager
     */
    public function __construct(MediaManager $mediaManager)
    {
        $this->mediaManager = $mediaManager;
    }

    /**
     * @param mixed $media
     *
     * @return mixed
     */
    public function transform($media)
    {
        return $media;
    }

    /**
     * @param mixed $binary
     *
     * @return null|Media
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException
     */
    public function reverseTransform($binary)
    {
        if (!$binary) {
            return null;
        }

        return $this->guessMediaType($binary);
    }

    private function guessMediaType($data)
    {
        if ($this->isImage($data)){
            return $this->insertImage($data);
        } else {
            return $this->videoType($data);
        }
    }

    private function isImage($data)
    {
        if (strpos($data, 'data:image') === 0) {
            return true;
        }

        return false;
    }

    private function insertImage($binary): Media
    {
        $data = explode(',', $binary);
        $response = base64_decode($data[1]);
        $name = uniqid('social_header_', true);
        $path = sys_get_temp_dir() . '/'  . $name;
        $file = fopen($path, "wb");
        fwrite($file, $response);
        fclose($file);
        $mediaInfos = getimagesize($path);

        $header = new File($path);
        $media = new Media();
        $media->setName($name);
        $media->setBinaryContent($header);
        $media->setWidth($mediaInfos[0]);
        $media->setHeight($mediaInfos[1]);
        $this->mediaManager->save($media, 'social', 'sonata.media.provider.image');

        return $media;
    }

    private function isYoutube($url)
    {
        $media = new Media();
        $media->setBinaryContent($url);
        $media->setEnabled(true);
        $media->setName($url);
        $this->mediaManager->save($media, 'social', 'sonata.media.provider.youtube');

        return $media;
    }

    private function isVimeo($url)
    {
        $media = new Media();
        $media->setBinaryContent($url);
        $media->setEnabled(true);
        $media->setName($url);
        $this->mediaManager->save($media, 'social', 'sonata.media.provider.vimeo');

        return $media;
    }

    private function videoType($url) {
        if (strpos($url, 'youtube') > 0) {
            return $this->isYoutube($url);
        } elseif (strpos($url, 'vimeo') > 0) {
            return $this->isVimeo($url);
        } else {
            return null;
        }
    }
}
