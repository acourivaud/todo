<?php declare(strict_types=1);

namespace Todotoday\SocialBundle\Form\Api;

use Actiane\ToolsBundle\Constraints\DateInterval;
use Sonata\MediaBundle\Entity\MediaManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\NotBlank;
use Todotoday\SocialBundle\Entity\SocialGroup;
use Todotoday\SocialBundle\Form\DataTransformers\BinaryToSonataMediaTransformer;

class SocialGroupType extends AbstractType
{
    /**
     * @var MediaManager
     */
    private $em;

    /**
     * SocialGroupType constructor.
     *
     * @param MediaManager $em
     */
    public function __construct(MediaManager $em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     * @throws \Symfony\Component\Form\Exception\InvalidArgumentException
     * @throws \Symfony\Component\Validator\Exception\ConstraintDefinitionException
     * @throws \Symfony\Component\Validator\Exception\InvalidOptionsException
     * @throws \Symfony\Component\Validator\Exception\MissingOptionsException
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('description')
            ->add('public')
            ->add(
                $builder
                    ->create(
                        'headerImg',
                        TextType::class,
                        array(
                            'data_class' => null,
                        )
                    )
                    ->addViewTransformer(new BinaryToSonataMediaTransformer($this->em))
            );

        if ($options['type'] === 'event') {
            $builder
                ->add(
                    'startDate',
                    DateTimeType::class,
                    array(
                        'date_format' => 'yyyy-MM-dd HH:mm:ss',
                        'widget' => 'single_text',
                        'input' => 'datetime',
                    )
                )
                ->add(
                    'endDate',
                    DateTimeType::class,
                    array(
                        'date_format' => 'yyyy-MM-dd HH:mm:ss',
                        'widget' => 'single_text',
                        'input' => 'datetime',
                    )
                )
                ->add('address')
                ->add('capacity');
        }
    }

    /**
     * {@inheritdoc}
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => SocialGroup::class,
                'csrf_protection' => false,
                'type' => 'group',
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }
}
