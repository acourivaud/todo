<?php declare(strict_types = 1);

namespace Todotoday\SocialBundle\Form\Api;

use Sonata\MediaBundle\Entity\MediaManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Todotoday\SocialBundle\Form\DataTransformers\CommentsBinaryToSonataMediaTransformer;

class SocialCommentType extends AbstractType
{
    /**
     * @var MediaManager
     */
    private $em;

    /**
     * SocialGroupType constructor.
     *
     * @param MediaManager $em
     */
    public function __construct(MediaManager $em)
    {
        $this->em = $em;
    }
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('content')
            ->add('taggedMembers')
            ->add(
                $builder
                    ->create(
                        'media',
                        TextType::class,
                        array(
                            'data_class' => null,
                        )
                    )
                    ->addViewTransformer(new CommentsBinaryToSonataMediaTransformer($this->em))
            )
            ->add('parent');
    }

    /**
     * {@inheritdoc}
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Todotoday\SocialBundle\Entity\SocialComment',
                'csrf_protection' => false,
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }
}
