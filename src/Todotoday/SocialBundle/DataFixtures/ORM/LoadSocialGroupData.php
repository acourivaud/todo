<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 20/02/17
 * Time: 14:19
 */

namespace Todotoday\SocialBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Generator;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAccountData;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAdherentLinkedMicrosoftData;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyData;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyLinkedMicrosoft;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\SocialBundle\Entity\LinkAdherentSocialGroup;
use Todotoday\SocialBundle\Entity\LinkAgencySocialGroup;
use Todotoday\SocialBundle\Entity\SocialEvent;
use Todotoday\SocialBundle\Entity\SocialGroup;
use Todotoday\SocialBundle\Enum\SocialGroupStatusEnum;

/**
 * Class LoadSocialGroupData
 * @SuppressWarnings(PHPMD)
 * @package Todotoday\SocialBundle\DataFixtures\ORM
 */
class LoadSocialGroupData extends AbstractFixture implements DependentFixtureInterface, ContainerAwareInterface
{
    /**
     * @var string[]
     */
    protected static $groupsSlugs;

    /**
     * @var string[]
     */
    protected static $eventsSLugs;

    /**
     * @var ObjectManager
     */
    private $manager;

    /**
     * @var Generator
     */
    private $faker;

    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * @return \string[]
     */
    public static function getEvents(): array
    {
        return self::$eventsSLugs;
    }

    /**
     * @return \string[]
     */
    public static function getGroups(): array
    {
        return self::$groupsSlugs;
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return array(
            LoadAgencyData::class,
            LoadAccountData::class,
//            LoadAdherentLinkedMicrosoftData::class,
//            LoadAgencyLinkedMicrosoft::class,
        );
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     *
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Exception
     * @throws \ReflectionException
     */
    public function load(ObjectManager $manager): void
    {
        if ($this->kernel->getEnvironment() === 'prod') {
            return;
        }

        $this->manager = $manager;
        $groups = array(
            array(
                'type' => SocialGroup::class,
                'status' => array(SocialGroupStatusEnum::JOINED),
                'key' => 1,
                'name' => 'Atelier Poterie',
                'desc' => 'Apprenez la poterie avec vos collègues amateurs',
            ),
            array(
                'type' => SocialGroup::class,
                'status' => array(SocialGroupStatusEnum::INVITED),
                'key' => 2,
                'name' => 'Les adeptes du footing',
                'desc' => 'Amateur de running matinal ? ça se passe ici',
            ),
            array(
                'type' => SocialGroup::class,
                'status' => array(
                    SocialGroupStatusEnum::JOINED,
                    SocialGroupStatusEnum::EXITED,
                ),
                'key' => 3,
                'name' => 'Enfants : Activités periscolaires',
                'desc' => 'Des idées pour occuper vos enfants',
            ),
            array(
                'type' => SocialGroup::class,
                'status' => array(
                    SocialGroupStatusEnum::INVITED,
                    SocialGroupStatusEnum::REFUSED,
                ),
                'key' => 4,
                'name' => 'Passionés d\'avitation',
                'desc' => 'Aviateurs privés ou AVGeeks, bienvenue !',
            ),
            array(
                'type' => SocialGroup::class,
                'status' => array(
                    SocialGroupStatusEnum::JOINED,
                    SocialGroupStatusEnum::MUTED,
                ),
                'key' => 5,
                'name' => 'Scrapbooking pour les nuls',
                'desc' => 'Tout savoir sur le Scrapbooking',
            ),
            array(
                'type' => SocialGroup::class,
                'status' => array(
                    SocialGroupStatusEnum::JOINED,
                    SocialGroupStatusEnum::BANNED,
                ),
                'key' => 6,
                'name' => 'Soirée de fin d\'année',
                'desc' => 'Préparatifs et idées pour notre grande soirée de fin \'année',
            ),
            array(
                'type' => SocialGroup::class,
                'status' => array(),
                'public' => false,
                'key' => 7,
                'name' => 'La grande vadrouille',
                'desc' => 'Ce groupé est dédié à l\'organisation de promenades tous niveaux',
            ),
            array(
                'type' => SocialGroup::class,
                'status' => array(
                    SocialGroupStatusEnum::INVITED,
                ),
                'public' => false,
                'key' => 8,
                'name' => 'Le coin de lecteurs',
                'desc' => 'Partagez les ouvrages que vous adorez !',
            ),
            array(
                'type' => SocialGroup::class,
                'status' => array(
                    SocialGroupStatusEnum::INVITED,
                    SocialGroupStatusEnum::ACCEPTED,
                    SocialGroupStatusEnum::JOINED,
                ),
                'public' => false,
                'key' => 9,
                'name' => 'Pause café',
                'desc' => 'Discuter de tout et de rien',
            ),
            array(
                'type' => SocialGroup::class,
                'status' => array(
                    SocialGroupStatusEnum::INVITED,
                    SocialGroupStatusEnum::REFUSED,
                ),
                'public' => false,
                'key' => 10,
                'name' => 'Pause café',
                'desc' => 'Discuter de tout et de rien',
            ),
            array(
                'type' => SocialGroup::class,
                'status' => array(
                    SocialGroupStatusEnum::INVITED,
                    SocialGroupStatusEnum::ACCEPTED,
                    SocialGroupStatusEnum::JOINED,
                    SocialGroupStatusEnum::EXITED,
                ),
                'public' => false,
                'key' => 11,
                'name' => 'Les brasseurs',
                'desc' => 'Communauté de brasseurs amateurs. Astuces et conseils',
            ),
            array(
                'type' => SocialGroup::class,
                'status' => array(
                    SocialGroupStatusEnum::INVITED,
                    SocialGroupStatusEnum::ACCEPTED,
                    SocialGroupStatusEnum::JOINED,
                    SocialGroupStatusEnum::BANNED,
                ),
                'public' => false,
                'key' => 12,
                'name' => 'Histoire',
                'desc' => '',
            ),
            array(
                'type' => SocialEvent::class,
                'status' => array(
                    SocialGroupStatusEnum::INVITED,
                    SocialGroupStatusEnum::ACCEPTED,
                    SocialGroupStatusEnum::JOINED,
                ),
                'key' => 1,
                'name' => 'Ateliers de formation jardinage',
                'desc' => 'Venez participez à ces ateliers proposés par des professionels du jardinage',
            ),
            array(
                'type' => SocialEvent::class,
                'status' => array(),
                'key' => 2,
                'name' => 'Spectacle de marionettes',
                'desc' => 'Tous les jeudi après midi',
            ),
            array(
                'type' => SocialEvent::class,
                'status' => array(),
                'key' => 3,
                'public' => false,
                'name' => 'Nouvel an entre collègues',
                'desc' => 'Rejoignez nous pour passer une soirée memmorable. Petits et grands !',
            ),
        );
        $this->createSocialGroup($groups);
        $this->setLinkSocialEvent();
    }

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     *
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     */
    public function setContainer(ContainerInterface $container = null): void
    {
        $this->faker = $container->get('davidbadura_faker.faker');
        $this->kernel = $container->get('kernel');
    }

    /**
     * @param array $groups
     *
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Exception
     * @throws \ReflectionException
     */
    private function createSocialGroup(array $groups): void
    {
        $agenciesSlug = LoadAgencyData::getAgenciesSlug();
        $adherentSlug = LoadAccountData::getAdherentsSlugs();

        // Seulement si on veut créer les groupes dans les conciergeries Microsoft (/!\ ralenti fortement les tests)
//        $agenciesSlug[] = 'arevalta';
//        $adherentSlug[] = 'adherent_linked_real_1_arevalta';
        /** @var Account $admin */
        $admin = $this->getReference('admin');
        $linkAgencySocialGroupModel = new LinkAgencySocialGroup();
        foreach ($groups as $group) {
            /** @var SocialGroup|SocialEvent $socialGroupModel */
            $class = $group['type'];
            $socialGroupModel = new $class();
            $socialGroupModel->setCreatedBy($admin);
            $key = 0;
            foreach ($agenciesSlug as $agencySlug) {
                /** @var Agency $agency */
                $agency = $this->getReference('agency_' . $agencySlug);
                $socialGroup = clone $socialGroupModel;
                $reflect = new \ReflectionClass($socialGroup);

                /** @var Adherent $adherent */
                $adherent = $this->getReference($adherentSlug[$key]);
                $linkAgencySocialGroup = clone $linkAgencySocialGroupModel;
                $linkAgencySocialGroup->setAgency($agency);

                $socialGroup
                    ->setTitle($group['name'])
                    ->setDescription($group['desc']);

                if ($reflect->getShortName() === 'SocialEvent') {
                    $startDate = (new \DateTime())->add(new \DateInterval('P10D'));
                    $endDate = (new \DateTime())->add(new \DateInterval('P11D'));
                    $socialGroup->setStartDate($startDate)
                        ->setEndDate($endDate);
                }

                if (array_key_exists('public', $group)) {
                    $socialGroup->setPublic($group['public']);
                }
                $socialGroup->addLinkAgency($linkAgencySocialGroup);
                $this->manager->persist($socialGroup);
                $this->manager->persist($linkAgencySocialGroup);

                /** @var array[] $group */
                foreach ($group['status'] as $status) {
                    $linkAdherentSocialGroup = new LinkAdherentSocialGroup();
                    /** @var SocialGroupStatusEnum $statusInstance */
                    $statusInstance = SocialGroupStatusEnum::getNewInstance($status);
                    $linkAdherentSocialGroup
                        ->setSocialGroup($socialGroup)
                        ->setStatus($statusInstance)
                        ->setAdherent($adherent);
                    $this->manager->persist($linkAdherentSocialGroup);
                }

                // reference SocialGroupX_demo0
                $slug = $reflect->getShortName() . $group['key'] . '_' . $agencySlug;
                $this->addReference(
                    $slug,
                    $socialGroup
                );

                if ($reflect->getShortName() === 'SocialGroup') {
                    self::$groupsSlugs[] = $slug;
                } else {
                    self::$eventsSLugs[] = $slug;
                }

                $this->manager->persist($socialGroup);
                $key++;
            }
        }
        $this->manager->flush();
    }

    /**
     * Create link between social group and event
     */
    private function setLinkSocialEvent(): void
    {
        /** @var SocialEvent $event */
        $event = $this->manager->merge($this->getReference('SocialEvent1_demo0'));
        /** @var SocialGroup $group */
        $group = $this->getReference('SocialGroup1_demo0');
        $group->addEvent($event);

        //        foreach (self::$eventsSLugs as $slug) {
        //            var_dump($slug);
        //            /** @var SocialEvent $event */
        //            $event = $this->getReference($slug);
        //            $agencySLug = substr($slug, -5);
        //            /** @var SocialGroup $group */
        //            $group = $this->getReference('SocialGroup2_' . $agencySLug);
        //            var_dump($group->getTitle());
        //            $group->addEvent($event);
        //            $this->manager->persist($group);
        //        }
        $this->manager->flush();
    }
}
