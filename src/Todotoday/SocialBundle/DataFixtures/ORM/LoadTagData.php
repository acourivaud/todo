<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 23/10/17
 * Time: 10:57
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\SocialBundle\DataFixtures\ORM
 *
 * @subpackage Todotoday\SocialBundle\DataFixtures\ORM
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\SocialBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Todotoday\SocialBundle\Entity\Tag;

/**
 * Class LoadTagData
 */
class LoadTagData extends AbstractFixture
{
    /**
     * @var array
     */
    public static $tags = [
        'Voyage',
        'Lecture',
    ];

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        foreach (self::$tags as $tag) {
            $tagToInsert = (new Tag())->setName($tag);
            $manager->persist($tagToInsert);
        }
        $manager->flush();
    }
}
