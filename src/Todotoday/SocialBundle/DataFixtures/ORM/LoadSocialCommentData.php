<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 26/02/17
 * Time: 15:52
 */

namespace Todotoday\SocialBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Generator;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAccountData;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyData;
use Todotoday\SocialBundle\Entity\SocialComment;
use Todotoday\SocialBundle\Entity\SocialGroup;

/**
 * Class LoadCommentData
 * @package Todotoday\SocialBundle\DataFixtures\ORM
 */
class LoadSocialCommentData extends AbstractFixture implements DependentFixtureInterface, ContainerAwareInterface
{
    /**
     * @var string[]
     */
    private static $comments;

    /**
     * @var Generator
     */
    private $faker;

    /**
     * @var ObjectManager
     */
    private $manager;

    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * @return mixed
     */
    public static function getComments()
    {
        return self::$comments;
    }

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->faker = $container->get('davidbadura_faker.faker');
        $this->kernel = $container->get('kernel');
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return array(
            LoadAgencyData::class,
            LoadAccountData::class,
            LoadSocialGroupData::class,
        );
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        if ($this->kernel->getEnvironment() === 'prod') {
            return;
        }
        $this->manager = $manager;
        $socialGroups = array_merge(
            LoadSocialGroupData::getGroups(),
            LoadSocialGroupData::getEvents()
        );
        $this->createComment($socialGroups);
    }

    /**
     * @param string[] $socialGroupsSlug
     */
    private function createComment(array $socialGroupsSlug): void
    {
        $commentModel = new SocialComment();
        foreach ($socialGroupsSlug as $slug) {
            $key = substr($slug, -1);
            if ($key === 'a') {
                $key = '_linked_arevalta';
            }
            /** @var Account $admin */
            $admin = $this->getReference('admin');
            /** @var SocialGroup $group */
            $group = $this->getReference($slug);
            $commentAdmin = clone $commentModel;
            $commentAdmin->setAuthor($admin)
                ->setContent($this->faker->text(100));
            $group->addComment($commentAdmin);
            $this->manager->persist($commentAdmin);
            $commentSlug = 'comment_' . $key . 'admin';
            $this->setReference($commentSlug, $commentAdmin);
            self::$comments[] = $commentSlug;

            for ($i = 0; $i < 5; $i++) {
                $adherent = $this->getReference('adherent' . $key);
                $commentAdherent = clone $commentModel;
                $commentAdherent->setAuthor($adherent)
                    ->setContent('Bonjour merci pour ce groupe ' . $i);
                $group->addComment($commentAdherent);
                $this->manager->persist($commentAdherent);
                $commentSlug = 'comment_' . $key . 'admin';
                $this->setReference($commentSlug, $commentAdmin);
                self::$comments[] = $commentSlug;
            }
        }
        $this->manager->flush();
    }
}
