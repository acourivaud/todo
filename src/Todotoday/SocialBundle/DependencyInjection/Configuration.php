<?php declare(strict_types = 1);

namespace Todotoday\SocialBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     * * @throws \RuntimeException
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('todotoday_social');

        $rootNode
            ->children()
                ->arrayNode('shorten_api')
                    ->children()
                        ->scalarNode('token')->end()
                        ->scalarNode('base_url')->end()
                        ->scalarNode('shorten_endpoint')->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
