<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 22/02/17
 * Time: 12:09
 */

namespace Todotoday\SocialBundle\Services;

use Doctrine\ORM\EntityManagerInterface;
use Sonata\MediaBundle\Entity\MediaManager;
use Sonata\MediaBundle\Provider\Pool;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Exception\AclNotFoundException;
use Symfony\Component\Security\Acl\Model\MutableAclProviderInterface;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validation;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Services\AccountManager;
use Todotoday\AccountBundle\Services\EmailWhitelistManager;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\MediaBundle\Entity\Media;
use Todotoday\SocialBundle\Entity\Comment;
use Todotoday\SocialBundle\Entity\LinkAdherentSocialGroup;
use Todotoday\SocialBundle\Entity\LinkAgencySocialGroup;
use Todotoday\SocialBundle\Entity\SocialComment;
use Todotoday\SocialBundle\Entity\SocialEvent;
use Todotoday\SocialBundle\Entity\SocialGroup;
use Todotoday\SocialBundle\Entity\UnregisteredSocialGroupInvitation;
use Todotoday\SocialBundle\Enum\SocialGroupStatusEnum;
use Todotoday\SocialBundle\Event\SocialAnonymousNewInviteEvent;
use Todotoday\SocialBundle\Event\SocialNewGroupEvent;
use Todotoday\SocialBundle\Event\SocialNewCommentEvent;
use Todotoday\SocialBundle\Event\SocialNewInvitEvent;
use Todotoday\SocialBundle\Event\SocialNotAdherentNewInvitEvent;
use Todotoday\SocialBundle\Exceptions\SocialEventExpiredException;
use Todotoday\SocialBundle\Exceptions\SocialForbiddenException;
use Todotoday\SocialBundle\Exceptions\SocialBadRequestException;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Todotoday\CoreBundle\Services\DomainContextManager;

/**
 * Class SocialGroupManager
 *
 * @package Todotoday\SocialBundle\Tests\Services
 *
 * @SuppressWarnings(PHPMD)
 *
 */
class SocialGroupManager
{
    /**
     * @var MutableAclProviderInterface
     */
    private $aclProvider;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var TokenStorageInterface
     */
    private $token;

    /**
     * @var AccountManager
     */
    private $accountManager;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * @var Pool
     */
    private $pool;

    /**
     * @var MediaManager
     */
    private $mediaManager;

    /**
     * @var string
     */
    private $root;

    /**
     * @var EmailWhitelistManager
     */
    private $emailWhitelistManager;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * @var DomainContextManager
     */
    private $domainContext;

    /**
     * CartProductManager constructor.
     *
     * @param TokenStorageInterface         $token
     * @param EntityManagerInterface        $entityManager
     * @param MutableAclProviderInterface   $aclProvider
     * @param AccountManager                $accountManager
     * @param EventDispatcherInterface      $dispatcher
     * @param Pool                          $pool
     * @param MediaManager                  $mediaManager
     * @param string                        $rootDir
     * @param EmailWhitelistManager         $emailWhitelistManager
     * @param TranslatorInterface           $translator
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param DomainContextManager          $domainContext
     */
    public function __construct(
        TokenStorageInterface $token,
        EntityManagerInterface $entityManager,
        MutableAclProviderInterface $aclProvider,
        AccountManager $accountManager,
        EventDispatcherInterface $dispatcher,
        Pool $pool,
        MediaManager $mediaManager,
        string $rootDir,
        EmailWhitelistManager $emailWhitelistManager,
        TranslatorInterface $translator,
        AuthorizationCheckerInterface $authorizationChecker,
        DomainContextManager $domainContext
    ) {
        $this->token = $token;
        $this->em = $entityManager;
        $this->aclProvider = $aclProvider;
        $this->accountManager = $accountManager;
        $this->dispatcher = $dispatcher;
        $this->pool = $pool;
        $this->mediaManager = $mediaManager;
        $this->root = $rootDir;
        $this->emailWhitelistManager = $emailWhitelistManager;
        $this->translator = $translator;
        $this->authorizationChecker = $authorizationChecker;
        $this->domainContext = $domainContext;
    }

    /**
     * Perform action on current user
     *
     * @param Agency $agency
     * @param string $action
     * @param int    $groupId
     *
     * @return LinkAdherentSocialGroup
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \InvalidArgumentException
     * @throws SocialBadRequestException
     * @throws SocialForbiddenException
     */
    public function actionFromUser(Agency $agency, string $action, int $groupId): LinkAdherentSocialGroup
    {
        $group = $this->getOneGroup($agency, $groupId);

        if (!\in_array($action, SocialGroupStatusEnum::actionFromUser(), true)) {
            throw new SocialBadRequestException(
                'User can\'t perform action ' . $action . ' to himself'
            );
        }

        $account = $this->token->getToken()->getUser();
        $this->checkStatus($group, $account, $action);

        $method = 'processStatus' . ucfirst($action);
        if (!method_exists($this, $method)) {
            throw new \InvalidArgumentException('Method' . $method . ' doesn\'t exist');
        }

        $result = $this->$method($group, $account);
        $this->em->flush();

        return $result;
    }

    /**
     * Perform action on current user
     *
     * @param Agency          $agency
     * @param string          $action
     * @param int             $groupId
     * @param string|string[] $userEmailList
     *
     * @return array
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Symfony\Component\Validator\Exception\MissingOptionsException
     * @throws \Symfony\Component\Validator\Exception\InvalidOptionsException
     * @throws \Symfony\Component\Validator\Exception\ConstraintDefinitionException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws SocialBadRequestException
     * @throws SocialForbiddenException
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function actionToUser(Agency $agency, string $action, int $groupId, $userEmailList): array
    {
        /** @var Adherent $adherent */
        $adherent = $this->token->getToken()->getUser();

        /** @var SocialGroup $group */
        $group = $this->getOneGroup($agency, $groupId);

        $linkRepo = $this->em->getRepository(LinkAdherentSocialGroup::class);
        /** @var LinkAdherentSocialGroup $linkAdherent */
        $linkAdherent = $linkRepo->getLastStatusFromAdherentGroup($adherent, $group);
        // pour pouvoir inviter ou bannir l'adhérent qui envoi l'invitation doit faire partie du groupe
        if (!$linkAdherent
            || !\in_array($linkAdherent->getStatus()->get(), SocialGroupStatusEnum::myGroup(), true)) {
            throw new SocialForbiddenException('Adherent must belongs to group to perform action ' . $action);
        }

        // on check si l'action est valide
        if (!\in_array($action, SocialGroupStatusEnum::actionToUsers(), true)) {
            throw new SocialBadRequestException(
                'User can\'t perform action ' . $action . ' to another user'
            );
        }

        $method = $action . 'Adherent';
        if (!method_exists($this, $method)) {
            throw new SocialBadRequestException(sprintf('Method %s doesn\'t exist', $method));
        }

        return $this->$method($agency, $group, $userEmailList);
    }

    /**
     * @param Agency        $agency
     * @param int           $id
     * @param SocialComment $form
     *
     * @return SocialComment
     * @throws \Symfony\Component\Security\Acl\Exception\AclNotFoundException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Symfony\Component\Security\Acl\Exception\AclAlreadyExistsException
     * @throws \Symfony\Component\Security\Acl\Exception\InvalidDomainObjectException
     * @throws SocialBadRequestException
     * @throws SocialForbiddenException
     */
    public function addComment(Agency $agency, int $id, SocialComment $form): SocialComment
    {
        $author = $this->token->getToken()->getUser();

        $group = $this->getOneGroup($agency, $id);

        /** @var LinkAdherentSocialGroup $linkASGStatus */
        $linkASGStatus =
            $this->em->getRepository(LinkAdherentSocialGroup::class)->getLastStatusFromAdherentGroup($author, $group);
        $status = $linkASGStatus ? $linkASGStatus->getStatus()->get() : SocialGroupStatusEnum::__EMPTY;

        if (!\in_array($status, SocialGroupStatusEnum::allowPostComment(), true)) {
            throw new SocialBadRequestException(
                'This user\'s status (' . $status . ') don\'t allow him to post comment'
            );
        }

        $form->setSocialGroup($group)
            ->setAuthor($this->em->merge($author));
        $this->em->persist($form);

        $objectIdentity = ObjectIdentity::fromDomainObject($form);
        try {
            $this->aclProvider->findAcl($objectIdentity);
        } catch (AclNotFoundException $e) {
            $acl = $this->aclProvider->createAcl($objectIdentity);
            $acl->insertObjectAce(
                UserSecurityIdentity::fromAccount($author),
                MaskBuilder::MASK_OWNER
            );
            $this->aclProvider->updateAcl($acl);
        }
        $this->em->flush();

        try {
            $event = new SocialNewCommentEvent($group, $form, $agency);
            $this->dispatcher->dispatch($event::COMMENT_ADD, $event);
        } catch (\Exception $e) {
            // just to prevent Exception when sending notification to microsoft
        }

        return $form;
    }

    /**
     * @param Agency                  $agency
     * @param SocialGroup|SocialEvent $socialGroup
     *
     * @return SocialEvent|SocialGroup
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException
     * @throws \RuntimeException
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Symfony\Component\Security\Acl\Exception\AclAlreadyExistsException
     * @throws \Symfony\Component\Security\Acl\Exception\InvalidDomainObjectException
     * @throws SocialForbiddenException
     */
    public function addSocialGroup(Agency $agency, $socialGroup)
    {
        $author = $this->token->getToken()->getUser();

        /*if (!$this->accountManager->hasAgency($author, $agency)) {
            throw new SocialForbiddenException('User can\'t create group to this agency');
        }*/

        $linkAgencySocialGroup = new LinkAgencySocialGroup();
        $linkAgencySocialGroup->setAgency($agency);
        $socialGroup
            ->addLinkAgency($linkAgencySocialGroup)
            ->setCreatedBy($author);

        if ($author instanceof Account) {
//            $socialGroup->setPublic(true);
            $this->addStatus($socialGroup, $author, SocialGroupStatusEnum::JOINED);
        }

        if (!$socialGroup->getHeaderImg() && ($oldMedia = $this->headerRamdomiser($this->em))) {
            $media = new Media();

            $pool = $this->pool;
            $provider = $pool->getProvider($oldMedia->getProviderName());
            $media->setBinaryContent(
                new File(
                    $this->root . '/../web/uploads/media/' . $provider->getReferenceFile($oldMedia)
                        ->getName()
                )
            );

            $media->setProviderName($oldMedia->getProviderName());
            $media->setContext('social');

            $this->mediaManager->save($media, 'social', 'sonata.media.provider.image');
            $socialGroup->setHeaderImg($media);
        }

        $this->em->persist($socialGroup);

        $acl = $this->aclProvider->createAcl(ObjectIdentity::fromDomainObject($socialGroup));
        $acl->insertObjectAce(
            UserSecurityIdentity::fromAccount(
                $author
            ),
            MaskBuilder::MASK_OWNER
        );
        $this->aclProvider->updateAcl($acl);
        $this->em->flush();

        try {
            $event = new SocialNewGroupEvent($socialGroup, $agency);
            $this->dispatcher->dispatch($event::GROUP_ADD, $event);
        } catch (\Exception $e) {
            // just to prevent Exception when sending notification to microsoft
        }

        return $socialGroup;
    }

    /**
     * @param SocialGroup $socialGroup
     *
     * @return mixed
     */
    public function editSocialGroup($socialGroup)
    {
        $this->em->persist($socialGroup);
        $this->em->flush();

        return $socialGroup;
    }

    /**
     * @param Account     $account
     * @param Agency      $agency
     * @param SocialGroup $group
     * @param SocialEvent $event
     *
     * @return SocialGroup
     * @throws SocialForbiddenException
     */
    public function addEventToGroup(
        Account $account,
        Agency $agency,
        SocialGroup $group,
        SocialEvent $event
    ): SocialGroup {
        if ($group instanceof SocialEvent) {
            throw new SocialForbiddenException('Can\'t add group to another group');
        }

        // check if the group is in his agency
        if ($account instanceof Adherent
            && !$this->accountManager->hasAgency($account, $agency)) {
            throw new SocialForbiddenException('User can\'t create group to this agency');
        }

        if ($event->getStartDate() < new \DateTime()) {
            throw new SocialForbiddenException('You can only add future event');
        }

        $group->addEvent($event);
        $this->em->flush();

        return $group;
    }

    /**
     * @param Agency $agency
     * @param int    $id
     *
     * @return SocialGroup
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws SocialForbiddenException
     */
    public function getOneGroup(Agency $agency, int $id): SocialGroup
    {
        if (!$group = $this->em->getRepository(SocialGroup::class)->getOneGroup(
            $agency,
            $this->token->getToken()->getUser(),
            $id,
            $this->authorizationChecker->isGranted('ROLE_TODOTODAY_SOCIAL_GROUP_PREVIEW')
        )
        ) {
            throw new SocialForbiddenException('User can\'t access this group');
        }

        return $group;
    }

    /**
     * @param Agency   $agency
     * @param int      $id
     * @param int|null $limit
     * @param int|null $offset
     *
     * @return array
     * @throws \Todotoday\SocialBundle\Exceptions\SocialForbiddenException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getComments(Agency $agency, int $id, ?int $limit = null, ?int $offset = null): array
    {
        return $this->em->getRepository(Comment::class)->findByParentNotNull(
            $this->getOneGroup($agency, $id),
            array(
                'id' => 'DESC',
            ),
            $limit,
            $offset
        );
    }

    /**
     * @param \Todotoday\CoreBundle\Entity\Agency $agency
     * @param int                                 $id
     *
     * @return array
     */
    public function getMembers(Agency $agency, int $id): array
    {
        return $this->em->getRepository(LinkAdherentSocialGroup::class)
            ->getActiveMembers($this->getOneGroup($agency, $id));
    }

    /**
     * @param Adherent $adherent
     *
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function processPendingInvitations(Adherent $adherent): void
    {
        $repo = $this->em->getRepository(UnregisteredSocialGroupInvitation::class);
        $pendingInvitations = $repo->findBy(
            [
                'emailCanonical' => $adherent->getEmailCanonical(),
            ]
        );

        foreach ($pendingInvitations as $pendingInvitation) {
            try {
                $this->processStatusInvited($pendingInvitation->getGroup(), $adherent);
                $this->em->remove($pendingInvitation);
            } catch (\Exception $e) {
                //we prevent exception
            }
        }
        $this->em->flush();
    }

    /**
     * @param SocialGroup $socialGroup
     * @param Adherent    $adherent
     *
     * @return LinkAdherentSocialGroup
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function processStatusAccepted(SocialGroup $socialGroup, Adherent $adherent): LinkAdherentSocialGroup
    {
        $this->addStatus($socialGroup, $adherent, SocialGroupStatusEnum::ACCEPTED);

        return $this->addStatus($socialGroup, $adherent, SocialGroupStatusEnum::JOINED);
    }

    /**
     * @param SocialGroup $socialGroup
     * @param Adherent    $adherent
     *
     * @return LinkAdherentSocialGroup
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function processStatusBanned(SocialGroup $socialGroup, Adherent $adherent): LinkAdherentSocialGroup
    {
        return $this->addStatus($socialGroup, $adherent, SocialGroupStatusEnum::BANNED);
    }

    /**
     * @param SocialGroup $socialGroup
     * @param Adherent    $adherent
     *
     * @return LinkAdherentSocialGroup
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function processStatusExited(SocialGroup $socialGroup, Adherent $adherent): LinkAdherentSocialGroup
    {
        return $this->addStatus($socialGroup, $adherent, SocialGroupStatusEnum::EXITED);
    }

    /**
     * @param SocialGroup $socialGroup
     * @param Adherent    $adherent
     *
     * @return LinkAdherentSocialGroup
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function processStatusInvited(SocialGroup $socialGroup, Adherent $adherent): LinkAdherentSocialGroup
    {
        if (!$agency = $this->domainContext->getCurrentContext()->getAgency()) {
            $agency = $adherent->getLinkAgencies()->first()->getAgency();
        }

        $event = new SocialNewInvitEvent($socialGroup, $adherent, $agency);
        $this->dispatcher->dispatch(SocialNewInvitEvent::INVIT_RECEIVED, $event);

        return $this->addStatus($socialGroup, $adherent, SocialGroupStatusEnum::INVITED);
    }

    /**
     * @param SocialGroup $socialGroup
     * @param Account     $account
     *
     * @return LinkAdherentSocialGroup
     * @throws \Todotoday\SocialBundle\Exceptions\SocialForbiddenException
     * @throws \Todotoday\SocialBundle\Exceptions\SocialEventExpiredException
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     * @throws SocialForbiddenException
     */
    protected function processStatusJoined(SocialGroup $socialGroup, Account $account): LinkAdherentSocialGroup
    {
        if (!$socialGroup->isPublic()) {
            throw new SocialForbiddenException('User can\'t join private group without invitation');
        }
        if (($socialGroup instanceof SocialEvent) && (new \DateTime() >= $socialGroup->getStartDate())) {
            throw new SocialEventExpiredException('User can\'t join event because is past');
        }

        return $this->addStatus($socialGroup, $account, SocialGroupStatusEnum::JOINED);
    }

    /**
     * @param SocialGroup $socialGroup
     * @param Adherent    $adherent
     *
     * @return LinkAdherentSocialGroup
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function processStatusMuted(SocialGroup $socialGroup, Adherent $adherent): LinkAdherentSocialGroup
    {
        return $this->addStatus($socialGroup, $adherent, SocialGroupStatusEnum::MUTED);
    }

    /**
     * @param SocialGroup $socialGroup
     * @param Adherent    $adherent
     *
     * @return LinkAdherentSocialGroup
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function processStatusRefused(SocialGroup $socialGroup, Adherent $adherent): LinkAdherentSocialGroup
    {
        return $this->addStatus($socialGroup, $adherent, SocialGroupStatusEnum::REFUSED);
    }

    /**
     * @param Agency          $agency
     * @param SocialGroup     $group
     * @param string|string[] $userEmailList
     *
     * @return array
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     * @throws \Symfony\Component\Validator\Exception\MissingOptionsException
     * @throws \Symfony\Component\Validator\Exception\InvalidOptionsException
     * @throws \Symfony\Component\Validator\Exception\ConstraintDefinitionException
     */
    protected function invitedAdherent(Agency $agency, SocialGroup $group, $userEmailList): array
    {
        $adherents = [];
        $exceptions = [];

        /** @var string[] $userEmailList */
        if (!\is_array($userEmailList)) {
            $userEmailList = [$userEmailList];
        }

        // on vérifie que les emails sont valides (email correctement formatée + appartenance à la conciergerie)
        if (\count($errors = $this->validateEmailList($userEmailList))) {
            return array('errors' => $errors);
        }

        $adherentRepo = $this->em->getRepository(Adherent::class);

        foreach ($userEmailList as $adherentToInvit) {
            try {
                // on recherche l'adherent par conciergerie
                if ($adherentInvited = $adherentRepo->getAdherentByEmail($agency, $adherentToInvit)) {
                    $this->checkStatus($group, $adherentInvited, SocialGroupStatusEnum::INVITED);
                    $this->processStatusInvited($group, $adherentInvited);
                    $this->em->flush();
                    $adherents[] = $adherentToInvit;
                } elseif ($adherentNotInConciergerie = $adherentRepo->findOneBy(
                    [
                        'usernameCanonical' => strtolower($adherentToInvit),
                        'enabled' => true,
                    ]
                )) {
                    $exceptions[$adherentNotInConciergerie] = $this->translator->trans(
                        'social.group.invited.error.adherentnotconciergerie',
                        array(),
                        'todotoday'
                    );
                } else {
                    $adherents[] = $adherentToInvit;

                    $nonAdherentExists = $adherentRepo->getAdherentByEmail($agency, $adherentToInvit);

                    if ($nonAdherentExists) {
                        $event = new SocialNotAdherentNewInvitEvent($group, $adherentToInvit, $agency);
                        $this->dispatcher
                            ->dispatch(SocialNotAdherentNewInvitEvent::INVIT_NOT_ADHERENT_RECEIVED, $event);
                    } else {
                        $event = new SocialAnonymousNewInviteEvent($group, $adherentToInvit, $agency);
                        $this->dispatcher
                            ->dispatch(SocialAnonymousNewInviteEvent::INVITE_ANONYMOUS_RECEIVED, $event);
                    }
                }
            } catch (\Exception $e) {
                $exceptions[$adherentToInvit] = $e->getMessage();
            }
        }

        return array(
            'adherents' => $adherents,
            'exception' => $exceptions,
        );
    }

    /**
     * @param Agency          $agency
     * @param SocialGroup     $group
     * @param string|string[] $userEmailList
     *
     * @return array
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     * @throws \Todotoday\SocialBundle\Exceptions\SocialForbiddenException
     * @throws \Symfony\Component\Validator\Exception\MissingOptionsException
     * @throws \Symfony\Component\Validator\Exception\InvalidOptionsException
     * @throws \Symfony\Component\Validator\Exception\ConstraintDefinitionException
     */
    protected function bannedAdherent(Agency $agency, SocialGroup $group, $userEmailList): array
    {
        // l'adhérent qui ban doit être le créateur du groupe
        /** @var Adherent $adherent */
        $adherent = $this->token->getToken()->getUser();
        if ($group->getCreatedBy()->getId() !== $adherent->getId()) {
            throw new SocialForbiddenException('User must be the creator of the group to ban other adherent');
        }

        /** @var string[] $userEmailList */
        if (!\is_array($userEmailList)) {
            $userEmailList = [$userEmailList];
        }

        $unknownUsers = [];
        $adherents = [];
        $exceptions = [];

        // on vérifie que les emails sont valides (email correctement formatée + appartenance à la conciergerie)
        if (\count($errors = $this->validateEmailList($userEmailList))) {
            return $errors;
        }

        $adherentRepo = $this->em->getRepository(Adherent::class);

        foreach ($userEmailList as $adherentToBan) {
            // on va checker si l'adhérent à bannir existe
            try {
                if ($adherentToBanned = $adherentRepo->getAdherentByEmail($agency, $adherentToBan)) {
                    $this->checkStatus($group, $adherentToBanned, SocialGroupStatusEnum::BANNED);
                    $this->processStatusBanned($group, $adherentToBanned);
                    $this->em->flush();
                    $adherents[] = $adherentToBanned;
                } else {
                    $unknownUsers[] = $adherentToBan;
                }
            } catch (\Exception $e) {
                $exceptions[] = $e->getMessage();
            }
        }

        return array(
            'adherentsBanned' => $adherents,
            'non-adherent' => $unknownUsers,
            'exception' => $exceptions,
        );
    }

    /**
     * @param SocialGroup $socialGroup
     * @param Account     $account
     * @param string      $action
     *
     * @return LinkAdherentSocialGroup
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     */
    private function addStatus(SocialGroup $socialGroup, Account $account, string $action): LinkAdherentSocialGroup
    {
        $linkAdherent = new LinkAdherentSocialGroup();
        $linkAdherent
            ->setAdherent($account)
            ->setSocialGroup($socialGroup)
            ->setStatus(SocialGroupStatusEnum::getNewInstance($action));
        $this->em->persist($linkAdherent);

        return $linkAdherent;
    }

    /**
     * Return exception if status we want to give dont match current status
     *
     * @param SocialGroup $group
     * @param Account     $account
     * @param string      $action
     *
     * @return SocialGroupManager
     * @throws SocialBadRequestException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function checkStatus(SocialGroup $group, Account $account, string $action): SocialGroupManager
    {
        $linkRepo = $this->em->getRepository(LinkAdherentSocialGroup::class);
        $lastStatus = 'empty';

        /** @var LinkAdherentSocialGroup $linkAdherent */
        if ($linkAdherent = $linkRepo->getLastStatusFromAdherentGroup($account, $group)) {
            $lastStatus = $linkAdherent->getStatus()->get();
        }

        if (!\in_array($action, SocialGroupStatusEnum::nextStatus()[$lastStatus], true)) {
            throw new SocialBadRequestException(
                'Invalid status ' . $action . '. Current status is ' . $lastStatus
            );
        }

        return $this;
    }

    /**
     * @param EntityManagerInterface $em
     *
     * @return Media|null
     */
    private function headerRamdomiser(EntityManagerInterface $em): ?Media
    {
        if (!$images = $em->getRepository(Media::class)->findBy(
            [
                'context' => 'headerImg',
            ]
        )) {
            return null;
        }

        $picker = random_int(0, \count($images) - 1);

        return $images[$picker];
    }

    /**
     * @param string $user
     *
     * @return array
     * @throws \Symfony\Component\Validator\Exception\MissingOptionsException
     * @throws \Symfony\Component\Validator\Exception\InvalidOptionsException
     * @throws \Symfony\Component\Validator\Exception\ConstraintDefinitionException
     */
    private function validateEmail(string $user): array
    {
        $validator = Validation::createValidator();
        $violations = $validator->validate(
            trim($user),
            array(
                new Email(),
                new NotBlank(),
            )
        );
        $errors = [];
        if (\count($violations)) {
            foreach ($violations as $violation) {
                $errors[] = $violation->getMessage();
            }
        }

        return $errors;
    }

    /**
     * Do validateEmailList
     *
     * @param string[] $userEmailList
     *
     * @return array
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Validator\Exception\MissingOptionsException
     * @throws \Symfony\Component\Validator\Exception\InvalidOptionsException
     * @throws \Symfony\Component\Validator\Exception\ConstraintDefinitionException
     */
    private function validateEmailList(array $userEmailList): array
    {
        $errors = [];

        foreach ($userEmailList as $user) {
            $error = $this->validateEmail($user);

            if (!$this->emailWhitelistManager->isEmailValidWhitelist($user)) {
                $error[] = $this->translator->trans('social.invite.error.email_whitelist', array(), 'todotoday');
            }

            if ($error) {
                $errors[$user] = $error;
            }
        }

        return $errors;
    }
}
