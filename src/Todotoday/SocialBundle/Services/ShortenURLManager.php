<?php declare(strict_types=1);

/**
 * User: Pix
 * Date: 19/02/21
 */

namespace Todotoday\SocialBundle\Services;

use Actiane\ApiConnectorBundle\Traits\LoggedCurlInfoTrait;
use Exception;
use Psr\Log\LoggerInterface;
use Todotoday\SocialBundle\Exceptions\SocialShortenURLException;

class ShortenURLManager
{
    use LoggedCurlInfoTrait;

    /**
     * @var string
     */
    private $baseUrl;

    /**
     * @var string
     */
    private $shortenEndpoint;

    /**
     * @var array
     */
    private $headers;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var string
     */
    private $loggerRef;

    /**
     * @var string
     */
    private $key = 'Bitly';

    /**
     * ShortenURLManager constructor.
     *
     * @param LoggerInterface $logger
     * @param string          $token
     * @param string          $baseUrl
     * @param string          $shortenEndpoint
     */
    public function __construct(LoggerInterface $logger, string $token, string $baseUrl, string $shortenEndpoint)
    {
        $this->baseUrl = $baseUrl;
        $this->shortenEndpoint = $shortenEndpoint;
        $this->headers = array('Content-Type: application/json', 'Authorization: Bearer ' . $token);
        $this->logger = $logger;
    }

    /**
     * Shorten URL with external API
     *
     * @param string $longUrl
     * @return array|false[]|null
     */
    public function createShortenURL(string $longUrl): ?array
    {
        $errorMessage = 'Veuillez réessayer afin de raccourcir vos URLs.';

        try {
            $url = $this->baseUrl . $this->shortenEndpoint;
            $data = json_encode(
                array(
                    'long_url' => $longUrl,
                )
            );
            $curl = $this->sendCurl($url, $data, true);
            $curlResponse = $curl['response'];
            $shortenURL = $curlResponse['link'];

            return array(
                'success' => true,
                'shorten_url' => $shortenURL,
            );
        } catch (Exception $e) {
            $message = $e->getMessage() . ' ' . $errorMessage;

            if ($e instanceof SocialShortenURLException) {
                $this->logger->error('HTTP Code : ' . var_export($e->getCode(), true));
                $this->logger->error('Errors : ' . var_export($message, true));
            }

            return array(
                'success' => false,
            );
        }

        return $url;
    }

    /**
     * @param string $url
     * @param null   $datas
     * @param bool   $isPost
     *
     * @return array
     * @throws Exception
     */
    public function sendCurl(string $url, $datas = null, bool $isPost = false)
    {
        $this->loggerRef = sprintf('(%s) (%s)', uniqid(), $this->key);
        $curl = curl_init($url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $datas);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $this->headers);
        if ($isPost) {
            $this->lastHttpMethodUsed = 'POST';
            curl_setopt($curl, CURLOPT_POST, true);
        }

        $curlResponse = curl_exec($curl);
        $info = curl_getinfo($curl);
        if ($curlResponse === false) {
            curl_close($curl);
            throw new Exception('Error occured during curl exec. Additional info: ' . var_export($info, true));
        }

        $this->loggedCurlInfo($info, $curlResponse);

        curl_close($curl);

        if (!$this->isJson($curlResponse)) {
            throw new Exception('The response is not a valid JSON.');
        }

        $response = json_decode($curlResponse, true);
        if (isset($response['errors']) && (intdiv($info['http_code'], 100) !== 2)) {
            throw new SocialShortenURLException($curlResponse, $info['http_code']);
        }

        return array(
            'response' => $response,
            'http_code' => $info['http_code'],
        );
    }

    /**
     * @param string $string
     *
     * @return bool
     */
    public function isJson(string $string): bool
    {
        json_decode($string);

        return (json_last_error() === JSON_ERROR_NONE);
    }
}
