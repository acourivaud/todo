<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 22/02/17
 * Time: 12:09
 */

namespace Todotoday\SocialBundle\Services;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Doctrine\ORM\EntityManagerInterface;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Services\AccountManager;
use Todotoday\MediaBundle\Entity\Media;
use Todotoday\SocialBundle\Entity\Comment;
use Todotoday\SocialBundle\Entity\SocialComment;
use Todotoday\SocialBundle\Entity\SocialGroup;
use Todotoday\SocialBundle\Repository\SocialCommentRepository;
use Todotoday\SocialBundle\Repository\SocialGroupRepository;
use Symfony\Component\Security\Acl\Model\MutableAclProviderInterface;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;

/**
 * Class SocialGroupManager
 * @package Todotoday\SocialBundle\Tests\Services
 */
class SocialCommentManager
{
    /**
     * @var MutableAclProviderInterface
     */
    private $aclProvider;

    /**
     * @var TokenStorageInterface
     */
    private $token;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * CartProductManager constructor.
     *
     * @param TokenStorageInterface       $token
     * @param EntityManagerInterface      $entityManager
     * @param MutableAclProviderInterface $aclProvider
     */
    public function __construct(
        TokenStorageInterface $token,
        EntityManagerInterface $entityManager,
        MutableAclProviderInterface $aclProvider
    ) {
        $this->token = $token;
        $this->em = $entityManager;
        $this->aclProvider = $aclProvider;
    }

    /**
     *
     * @param SocialGroup $socialGroup
     *
     * @return null|SocialComment[]
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \LogicException
     */
    public function getAllCommentsFromGroup(SocialGroup $socialGroup)
    {
        return $this->em->getRepository(SocialComment::class)->findBy(
            array(
                'socialGroup' => $socialGroup,
            )
        );
    }

    /**
     * @param int      $id
     * @param int|null $limit
     * @param int|null $offset
     *
     * @return array
     * @throws \Todotoday\SocialBundle\Exceptions\SocialForbiddenException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getChildrenComments(int $id, ?int $limit = null, ?int $offset = null): array
    {
        return $this->em->getRepository(Comment::class)->findBy(
            array(
                'parent' => $id,
            ),
            array(
                'id' => 'DESC',
            ),
            $limit,
            $offset
        );
    }
}
