<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 24/11/17
 * Time: 15:19
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\SocialBundle\Services\ChangeWatch
 *
 * @subpackage Todotoday\SocialBundle\Services\ChangeWatch
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\SocialBundle\Services\ChangeWatch;

use Actiane\EntityChangeWatchBundle\Interfaces\InterfaceHelper;
use Predis\Client;
use Todotoday\SocialBundle\Entity\LinkAdherentSocialGroup;
use Todotoday\SocialBundle\Enum\SocialGroupStatusEnum;
use Todotoday\SocialBundle\Repository\SocialGroupRepository;

/**
 * Class SocialGroupChangeWatch
 */
class SocialGroupChangeWatch implements InterfaceHelper
{
    public const KEY = 'LinkAdherentSocialGroup';

    /**
     * @var Client
     */
    private $redis;

    /**
     * SocialGroupChangeWatch constructor.
     *
     * @param Client $redis
     */
    public function __construct(Client $redis)
    {
        $this->redis = $redis;
    }

    /**
     * Compute the signature of the callable, used to avoid a callable to be called multiple times in a row
     *
     * @param array $callable
     * @param array $parameters
     *
     * @return mixed
     */
    public function computeSignature(array $callable, array $parameters): string
    {
        return self::KEY . $callable[1] . ':' . $parameters['entity']->getId();
    }

    /**
     * @param LinkAdherentSocialGroup $link
     *
     * @return string|null
     */
    public function clearCountMembersCache(LinkAdherentSocialGroup $link): ?string
    {
        if (!\in_array($link->getStatus()->get(), SocialGroupStatusEnum::changeCountMembers(), true)) {
            return null;
        }

        $this->redis->del(['[' . SocialGroupRepository::REDIS_CACHE_KEY . $link->getSocialGroup()->getId() . '][1]']);

        return self::KEY . 'clearCountMembersCache:' . $link->getId();
    }
}
