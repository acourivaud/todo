<?php declare(strict_types=1);

namespace Todotoday\SocialBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Todotoday\ClassificationBundle\Entity\Category;
use Todotoday\ClassificationBundle\Entity\Context;
use Todotoday\MediaBundle\Entity\Media;
use Symfony\Component\HttpFoundation\File\File;

class CreatePlaceholderCommand extends ContainerAwareCommand
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var string
     */
    private $directory;

    /**
     * CreatePlaceholderCommand constructor.
     *
     * @param null $name
     *
     * @throws \Symfony\Component\Console\Exception\LogicException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \LogicException
     */
    public function __construct($name = null)
    {
        parent::__construct($name);
    }

    /**
     * {@inheritDoc}
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function configure()
    {
        $this
            ->setName('tdtd:social:create-headers')
            ->setDescription('Create the medium for social group default placeHolders')
            ->setHelp(
                'This command check into "web/images/placeholders/social" to get new placeholders as new Media'
            );

        $this->addOption(
            'create-context',
            'c',
            InputOption::VALUE_NONE,
            'Create the context in db'
        );
    }

    /**
     * {@inheritDoc}
     *
     * @SuppressWarnings(PHPMD)
     *
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \LogicException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->em = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        $this->directory = $this->getContainer()->get('kernel')->getRootDir() . '/../web/images/placeholders/social/';

        if ($input->getOption('create-context')) {
            $this->createMediaContext($this->em, $output);

            return;
        }

        $output->writeln(
            [
                'Header Image Creator',
                '====================',
                '',
            ]
        );

        $images = $this->getPlaceholderList();
        $oldImages = $this->getOldPlaceholderList($this->em);

        $pendingImages = $this->checkImagesOutput($output, $images, $oldImages);

        $this->createNewHeadersImage($this->em, $pendingImages);
    }

    /**
     * @param EntityManagerInterface $em
     * @param array                  $selection
     *
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \LogicException
     */
    private function createNewHeadersImage(EntityManagerInterface $em, array $selection)
    {
        $mediaManager = $this->getContainer()->get('sonata.media.manager.media');

        foreach ($selection as $image) {
            $file = new File($this->getPlaceholdersDirectory() . $image);
            $media = new Media();
            $media->setContext('headerImg');
            $media->setProviderName('sonata.media.provider.image');
            $media->setBinaryContent($file);

            $mediaManager->save($media, 'headerImg', 'sonata.media.provider.image');
        }
    }

    /**
     * @param EntityManagerInterface $em
     * @param OutputInterface        $output
     */
    private function createMediaContext(EntityManagerInterface $em, OutputInterface $output)
    {
        $push = false;

        if ($contest = $em->getRepository(Context::class)->find('headerImg')) {
            $output->writeln(' - The context already exist');
        } else {
            $output->writeln('Creation of context');
            $output->writeln('');

            $contest = (new Context())
                ->setId('headerImg')
                ->setName('headerImg')
                ->setEnabled(true);

            $em->persist($contest);

            $push = true;
        }

        if ($category = $em->getRepository(Category::class)->findBy(['slug' => 'header-img'])) {
            $output->writeln(' - The category already exist');
        } else {
            $output->writeln('Creation of category');
            $output->writeln('');

            $category = (new Category())
                ->setName('Header Img')
                ->setSlug('header-img')
                ->setDescription('Header Img for social randomisation')
                ->setContext($contest)
                ->setEnabled(true);

            $em->persist($category);

            $push = true;
        }

        if ($push) {
            $em->flush();
        }
    }

    /**
     * @param OutputInterface $output
     * @param array           $images
     * @param array           $oldImages
     *
     * @return array
     */
    private function checkImagesOutput(OutputInterface $output, array $images, array $oldImages): array
    {
        $sortedImages = [];
        $totalImages = \count($images);
        $totalOldImages = \count($oldImages);

        $output->writeln($totalImages . ' image(s) detected - ' . $totalOldImages . ' image(s) already registred');

        foreach ($images as $image) {
            $exist = array_filter(
                $oldImages,
                function (Media $media) use ($image) {
                    return $media->getName() === $image;
                },
                ARRAY_FILTER_USE_BOTH
            );

            $icon = $exist ? '<bg=green>  </>' : '<error>  </error>';

            $output->writeln(' - ' . $icon . ' ' . $image);

            if (!$exist) {
                $sortedImages[] = $image;
            }
        }

        $output->writeln('');

        return $sortedImages;
    }

    /**
     * @param EntityManagerInterface $em
     *
     * @return array|Media[]
     */
    private function getOldPlaceholderList(EntityManagerInterface $em): array
    {
        return $em->getRepository(Media::class)->findBy(
            [
                'context' => 'headerImg',
            ]
        );
    }

    /**
     * @return array
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \LogicException
     */
    private function getPlaceholderList(): array
    {
        return preg_grep(
            '~\.(jpeg|jpg|png)$~',
            scandir(
                $this->getPlaceholdersDirectory(),
                SCANDIR_SORT_ASCENDING
            )
        );
    }

    /**
     * @return string
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \LogicException
     */
    private function getPlaceholdersDirectory(): string
    {
        return $this->directory;
    }
}
