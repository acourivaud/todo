<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 30/03/2017
 * Time: 16:37
 */

namespace Todotoday\CoreBundle\Twig;

use Todotoday\CoreBundle\Services\DomainContextManager;

/**
 * Class ModuleExtension
 *
 * @category   Todo-Todev
 * @package    Todotoday\CoreBundle
 * @subpackage Todotoday\CoreBundle\Twig
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class ModuleExtension extends \Twig_Extension
{
    /**
     * @var DomainContextManager
     */
    private $domainContextManager;

    /**
     * ModuleExtension constructor.
     *
     * @param DomainContextManager $domainContextManager
     */
    public function __construct(DomainContextManager $domainContextManager)
    {
        $this->domainContextManager = $domainContextManager;
    }

    /**
     * Do getFunctions
     *
     * @return array
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('is_module_granted', array($this, 'isModuleGranted'), array()),
        );
    }

    /**
     * Do isModuleGranted
     *
     * @param string $moduleEnum
     *
     * @return bool
     * @throws \UnexpectedValueException
     */
    public function isModuleGranted(string $moduleEnum): bool
    {
        if ($currentContext = $this->domainContextManager->getCurrentContext()->getAgency()) {
            return $currentContext->hasModule($moduleEnum);
        }

        return false;
    }
}
