<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 30/03/2017
 * Time: 16:50
 */

namespace Todotoday\CoreBundle\Enum;

use Actiane\ToolsBundle\Enum\AbstractEnum;

/**
 * Class AgencyModuleEnum
 *
 * @category   Todo-Todev
 * @package    Todotoday\CoreBundle
 * @subpackage Todotoday\CoreBundle\Enum
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class AgencyModuleEnum extends AbstractEnum
{
    public const CHAT = 'chat';
    public const SOCIAL = 'social';
    public const IWM = 'I-Want-More';
    public const COADH = 'Co-Adherent';
    public const FGS = 'FGS';
}
