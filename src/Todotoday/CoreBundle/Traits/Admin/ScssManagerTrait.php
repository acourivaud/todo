<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 03/02/2017
 * Time: 16:13
 */

namespace Todotoday\CoreBundle\Traits\Admin;

use Todotoday\CMSBundle\Services\ContextCssManager;
use Todotoday\CoreBundle\Entity\Agency;

trait ScssManagerTrait
{
    /**
     * @var ContextCssManager
     */
    private $cCssM;

    /**
     * @param Agency | mixed $object
     *
     * @throws \Doctrine\ORM\EntityNotFoundException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     */
    public function postPersist($object)
    {
//        $this->postUpdate($object);
    }

    /**
     * @param Agency | mixed $object
     *
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \Doctrine\ORM\EntityNotFoundException
     */
    public function postUpdate($object)
    {
//        $sCss = $this->cCssM->mergeDBScss($object);
//
//        $this->cCssM->createCss($sCss, $object);
    }

    /**
     * @param Agency $object
     */
    public function preRemove($object)
    {
//        $this->cCssM->removeScss($object);
    }
}
