<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 06/02/17
 * Time: 17:51
 */

namespace Todotoday\CoreBundle\Traits\Api;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;

trait ApiControllerPostTrait
{
    /**
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Resource description to post new entity",
     *     description="API Rest description to post new entity",
     *     input={
     *           "class"="Todotoday\CoreBundle\Form\Api\..."
     *     },
     *     output={
     *           "class"="Todotoday\CoreBundle\Entity\...",
     *           "groups"={"Default"}
     *     },
     *     views={"default"},
     *     section="Default"
     * )
     *
     * @param Request $request
     *
     * @return null|object
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \Symfony\Component\Security\Acl\Exception\AclAlreadyExistsException
     * @throws \Symfony\Component\Security\Acl\Exception\InvalidDomainObjectException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    public function postRoute(Request $request)
    {
        if (!$this->isGranted('CREATE', $this)) {
            throw $this->createAccessDeniedException();
        }

        $form = $this->createForm($this->getFormType());

        $form->handleRequest($request);
        $data = $form;

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($data);
            $em->flush();
            $aclProvider = $this->get('security.acl.provider');
            $acl = $aclProvider->createAcl(ObjectIdentity::fromDomainObject($data));
            $acl->insertObjectAce(UserSecurityIdentity::fromAccount($this->getUser()), MaskBuilder::MASK_OWNER);
            $aclProvider->updateAcl($acl);
        }

        $view = $this->view($data);
        $view->getContext()->addGroups($this->getDefaultGroups());
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }
}
