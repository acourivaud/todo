<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 06/02/17
 * Time: 17:51
 */

namespace Todotoday\CoreBundle\Traits\Api;

use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Response;

trait ApiControllerGetTrait
{
    /**
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Resource description to get all entity",
     *     description="API Rest description to get all entity",
     *     output={
     *           "class"="array<Todotoday\CoreBundle\Entity\...> as ...",
     *           "groups"={"Default"}
     *     },
     *     views={"default"},
     *     section="Default"
     * )
     *
     * @FosRest\Route("")
     * @FosRest\QueryParam(
     *     name="fields",
     *     requirements="((Default),?)*",
     *     default="",
     *     description="Fields you need"
     * )
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return Response
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \LogicException
     */
    public function getAllRoute(ParamFetcher $paramFetcher)
    {
        if (!$this->isGranted('VIEW', $this)) {
            throw $this->createAccessDeniedException();
        }

        $view = $this->view($this->get($this->getServiceRepository())->findAll());

        $groups = $this->getDefaultGroups();
        if ($fields = explode(',', $paramFetcher->get('fields'))) {
            $groups = array_merge($groups, $fields);
        }
        $view->getContext()->addGroups($groups);
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Resource description to get single entity",
     *     description="API Rest description to get single entity",
     *     output={
     *           "class"="Todotoday\CoreBundle\Entity\...",
     *           "groups"={"Default"}
     *     },
     *     views={"default"},
     *     section="Default"
     * )
     *
     * @FosRest\QueryParam(
     *     name="fields",
     *     requirements="((Default),?)*",
     *     default="",
     *     description="Fields you need"
     * )
     *
     * @param ParamFetcher $paramFetcher
     * @param int          $id
     *
     * @return Response
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \LogicException
     */
    public function getRoute(ParamFetcher $paramFetcher, int $id)
    {
        $entity = $this->getServiceRepository()->findOneBy(array('id' => $id));
        if (!$this->isGranted('VIEW', $entity)) {
            throw $this->createAccessDeniedException();
        }
        $view = $this->view($entity);
        $groups = $this->getDefaultGroups();
        if ($fields = explode(',', $paramFetcher->get('fields'))) {
            $groups = array_merge($groups, $fields);
        }
        $view->getContext()->addGroups($groups);
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }
}
