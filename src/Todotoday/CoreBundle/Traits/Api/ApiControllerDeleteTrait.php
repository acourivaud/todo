<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 06/02/17
 * Time: 17:51
 */

namespace Todotoday\CoreBundle\Traits\Api;

use Symfony\Component\HttpFoundation\Request;

trait ApiControllerDeleteTrait
{
    /**
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Resource description to delete existing entity",
     *     description="API Rest description delete existing entity",
     *     input={
     *           "class"="Todotoday\CoreBundle\Form\Api\..."
     *     },
     *     output={
     *           "class"="Todotoday\CoreBundle\Entity\...",
     *           "groups"={"Default"}
     *     },
     *     views={"default"},
     *     section="Default"
     * )
     *
     * @param Request $request
     * @param int     $id
     *
     * @return Response
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \InvalidArgumentException
     * @throws \LogicException
     */
    public function deleteRoute(Request $request, int $id)
    {
        $entity = $this->get($this->getServiceRepository())->findOneBy(array('id' => $id));
        if (!$this->isGranted('DELETE', $entity)) {
            throw $this->createAccessDeniedException();
        }
        $form = $this->createForm(
            $this->getFormType(),
            $entity,
            array('method' => 'DELETE')
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($entity);
            $em->flush();
        }

        return $this->handleView();
    }
}
