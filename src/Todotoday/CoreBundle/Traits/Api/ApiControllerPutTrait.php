<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 06/02/17
 * Time: 17:51
 */

namespace Todotoday\CoreBundle\Traits\Api;

use Symfony\Component\HttpFoundation\Request;

trait ApiControllerPutTrait
{
    /**
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Resource description to put new entity",
     *     description="API Rest description to put new entity",
     *     input={
     *           "class"="Todotoday\CoreBundle\Form\Api\..."
     *     },
     *     output={
     *           "class"="Todotoday\CoreBundle\Entity\...",
     *           "groups"={"Default"}
     *     },
     *     views={"default"},
     *     section="Default"
     * )
     *
     * @param Request $request
     * @param int     $id
     *
     * @return Response
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \InvalidArgumentException
     * @throws \LogicException
     */
    public function putRoute(Request $request, int $id)
    {
        $entity = $this->get($this->getServiceRepository())->findOneBy(array('id' => $id));
        if (!$this->isGranted('EDIT', $entity)) {
            throw $this->createAccessDeniedException();
        }
        $form = $this->createForm(
            $this->getFormType(),
            $entity,
            array(
                'method' => 'PUT',
            )
        );

        $form->handleRequest($request);
        $data = $form;

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $data = $entity;
        }

        $view = $this->view($data);
        $view->getContext()->addGroups($this->getDefaultGroups());
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }
}
