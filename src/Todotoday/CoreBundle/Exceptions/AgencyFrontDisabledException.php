<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 11/06/17
 * Time: 15:48
 */

namespace Todotoday\CoreBundle\Exceptions;

use Throwable;

/**
 * Class AgencyFrontDisabledException
 * @package Todotoday\CoreBundle\Exceptions
 */
class AgencyFrontDisabledException extends \Exception
{
    /**
     * AgencyFronDisabled constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = 'Agency is not enabled', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
