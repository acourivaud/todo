<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 11/06/17
 * Time: 15:51
 */

namespace Todotoday\CoreBundle\Exceptions;

use Throwable;

/**
 * Class AgencyWithoutFrontException
 * @package Todotoday\CoreBundle\Exceptions
 */
class AgencyWithoutFrontException extends \Exception
{
    /**
     * AgencyWithoutFront constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = 'Agency without front', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
