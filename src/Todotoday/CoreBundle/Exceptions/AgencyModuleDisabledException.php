<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 11/06/17
 * Time: 15:48
 */

namespace Todotoday\CoreBundle\Exceptions;

use Throwable;

/**
 * Class AgencyModuleDisabledException
 * @package Todotoday\CoreBundle\Exceptions
 */
class AgencyModuleDisabledException extends \Exception
{
    /**
     * AgencyModuleDisabledException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = 'Agency  Module is not enabled', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
