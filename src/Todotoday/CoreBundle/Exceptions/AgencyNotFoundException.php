<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 10/10/17
 * Time: 15:30
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\CoreBundle\Exceptions
 *
 * @subpackage Todotoday\CoreBundle\Exceptions
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\CoreBundle\Exceptions;

use Throwable;

/**
 * Class AgencyNotFoundException
 */
class AgencyNotFoundException extends \Exception
{
    /**
     * AgencyNotFoundException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = 'Agency not found', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
