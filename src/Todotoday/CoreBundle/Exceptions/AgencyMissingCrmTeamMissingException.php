<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 11/05/17
 * Time: 14:14
 */

namespace Todotoday\CoreBundle\Exceptions;

use Throwable;

/**
 * Class AgencyMissingCrmTeamMissingException
 * @package Todotoday\CoreBundle\Exceptions
 */
class AgencyMissingCrmTeamMissingException extends \Exception
{
    /**
     * AgencyMissingCrmIdTeamMissing constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = 'Agency CRM Team missing', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
