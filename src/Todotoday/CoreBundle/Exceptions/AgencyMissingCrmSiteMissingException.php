<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 11/05/17
 * Time: 14:14
 */

namespace Todotoday\CoreBundle\Exceptions;

use Throwable;

/**
 * Class AgencyMissingCrmSiteMissingException
 * @package Todotoday\CoreBundle\Exceptions
 */
class AgencyMissingCrmSiteMissingException extends \Exception
{
    /**
     * AgencyMissingCrmIdTeamMissing constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = 'Agency CRM Site missing', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
