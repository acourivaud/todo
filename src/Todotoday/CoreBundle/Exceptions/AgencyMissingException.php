<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 02/04/17
 * Time: 16:19
 */

namespace Todotoday\CoreBundle\Exceptions;

use Actiane\ToolsBundle\Exceptions\PublishedMessageExceptionInterface;

/**
 * Class AgencyMIssingException
 * @package Todotoday\CoreBundle\Exceptions
 */
class AgencyMissingException extends \Exception implements PublishedMessageExceptionInterface
{
    /**
     * @return mixed
     */
    public function getMessages()
    {
        parent::getMessage();
    }

    /**
     * @return int
     */
    public function getCustomCode(): int
    {
        return 500;
    }
}
