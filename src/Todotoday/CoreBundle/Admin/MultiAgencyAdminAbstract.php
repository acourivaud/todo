<?php declare(strict_types=1);

namespace Todotoday\CoreBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Todotoday\CoreBundle\Entity\Agency;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Todotoday\CoreBundle\Services\DomainContextManager;
use Doctrine\Common\Persistence\ObjectManager;

abstract class MultiAgencyAdminAbstract extends AbstractAdmin
{
    /**
     * @var DomainContextManager
     */
    protected $domainContextManager;

    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * LinkPostAgencyAdmin constructor.
     *
     * @param string               $code
     * @param string               $class
     * @param string               $baseControllerName
     * @param DomainContextManager $domainContextManager
     * @param ObjectManager        $objectManager
     */
    public function __construct(
        string $code,
        string $class,
        string $baseControllerName,
        DomainContextManager $domainContextManager,
        ObjectManager $objectManager
    ) {
        parent::__construct($code, $class, $baseControllerName);

        $this->domainContextManager = $domainContextManager;
        $this->objectManager = $objectManager;
    }

    /**
     * {@inheritdoc}
     * @throws \RuntimeException
     */
    public function generateUrl(
        $name,
        array $parameters = array(),
        $absolute = UrlGeneratorInterface::ABSOLUTE_PATH
    ): string {
        if ($name === 'create') {
            try {
                if ($agenciesStrategy = $this->getRequest()->get('agencies_strategy')) {
                    $parameters = array_merge($parameters, ['agencies_strategy' => $agenciesStrategy]);
                }
            } catch (\RuntimeException $e) {
            }
        }

        return parent::generateUrl($name, $parameters, $absolute);
    }

    /**
     * @param string $strategy
     *
     * @return array|null
     */
    protected function getAgencyByStrategy(?string $strategy = 'empty'): ?array
    {

        // TODO : A Factoriser
        $repo = $this->objectManager->getRepository(Agency::class);

        if ($strategy === 'all') {
            return $repo->findAll();
        }

        if ($strategy === 'us') {
            return $repo->getEPAAgencies(['EUS']);
        }

        if ($strategy === 'ch') {
            return $repo->getEPAAgencies(['ECH']);
        }

        if ($strategy === 'fr') {
            return $repo->getEPAAgencies(['EPA', 'ERA']);
        }

        return null;
    }
}
