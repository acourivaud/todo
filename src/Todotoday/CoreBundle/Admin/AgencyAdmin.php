<?php declare(strict_types=1);

namespace Todotoday\CoreBundle\Admin;

use Actiane\ApiConnectorBundle\Services\Connections\Microsoft\MicrosoftDynamicsConnection;
use Actiane\PaymentBundle\Enum\PaymentTypeEnum;
use Actiane\ToolsBundle\Form\ArraySelectType;
use Monolog\Logger;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\FormatterBundle\Form\Type\SimpleFormatterType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Entity\Api\Microsoft\Agency as MicrosoftAgency;
use Todotoday\CoreBundle\Enum\AgencyModuleEnum;
use Todotoday\PaymentBundle\Services\OneDriveManager;

/**
 * Class AgencyAdmin
 * @SuppressWarnings(PHPMD)
 *
 * @package    Todotoday\CoreBundle
 * @subpackage Todotoday\CoreBundle\Admin
 */
class AgencyAdmin extends AbstractAdmin
{
    /**
     * @var int
     */
    protected $maxPerPage = 100;

    /**
     * @var array
     */
    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'name',
    );

    /**
     * @var MicrosoftDynamicsConnection
     */
    private $microsoftDynamicsConnection;

    /**
     * @var OneDriveManager
     */
    private $oneDriveManager;

    /**
     * @var Logger
     */
    private $adminLogger;

    /**
     * @var null|string
     */
    private $googleAnalyticsId;

    /**
     * AgencyAdmin constructor.
     *
     * @param string                      $code
     * @param string                      $class
     * @param string                      $baseControllerName
     * @param MicrosoftDynamicsConnection $microsoftDynamicsConnection
     * @param OneDriveManager             $oneDriveManager
     * @param Logger                      $adminLogger
     * @param null|string                 $googleAnalyticsId
     */
    public function __construct(
        $code,
        $class,
        $baseControllerName,
        MicrosoftDynamicsConnection $microsoftDynamicsConnection,
        OneDriveManager $oneDriveManager,
        Logger $adminLogger,
        ?string $googleAnalyticsId
    ) {
        parent::__construct($code, $class, $baseControllerName);
        $this->microsoftDynamicsConnection = $microsoftDynamicsConnection;
        $this->oneDriveManager = $oneDriveManager;
        $this->adminLogger = $adminLogger;
        $this->googleAnalyticsId = $googleAnalyticsId;
    }

    /**
     * {@inheritdoc}
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getNewInstance()
    {
        $this->adminLogger->addInfo('getNewInstance 1 ' . $this->getRequest()->get('retailChannelId'));
        $this->adminLogger->addInfo('$this->getUniqid()' . $this->getUniqid());
        $this->adminLogger->addInfo('$this->getRequest()', (array) $this->getRequest()->request);
        $this->adminLogger->addInfo('$this->getRequest()', (array) $this->getRequest()->query);
        $this->adminLogger->addInfo('$_POST ', (array) $_POST);
        if ($retailChannelId = $this->getRequest()->get('retailChannelId') ??
            $this->getRequest()->request->get($this->getUniqid())['retailChannelId'] ?? false
        ) {
            $microsoftAgency = $this->microsoftDynamicsConnection->getRepository(MicrosoftAgency::class)->find(
                array('retailChannelId' => $retailChannelId)
            );
            $agency = $this->microsoftDynamicsConnection->findOrCreateDoctrineEntityByEntity($microsoftAgency);

            /** @var Agency $agency */

            return $agency->setIdAnalytics($this->googleAnalyticsId);
        }

        return parent::getNewInstance();
    }

    /**
     * @param Agency $object
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \InvalidArgumentException
     */
    public function preRemove($object)
    {
        if (!$microsoftAgency = $this->getMicrosoftAgency($object)) {
            return;
        }

        $microsoftAgency->setIdActiane();
        $this->microsoftDynamicsConnection->flush();
    }

    /**
     * {@inheritdoc}
     * @param Agency $object
     *
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Doctrine\ORM\EntityNotFoundException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     */
    public function postPersist($object): void
    {
        if (!$microsoftAgency = $this->getMicrosoftAgency($object)) {
            return;
        }
        $microsoftAgency->setIdActiane((string) $object->getSlug());
        $this->microsoftDynamicsConnection->flush();
        $this->postUpdate($object);
        $this->oneDriveManager->createFolderInSepaFolder($object->getName());
    }

    /**
     * @param DatagridMapper $datagridMapper
     *
     * @throws \RuntimeException
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('name')
            ->add('isDigital')
            ->add('enabledFront')
            ->add('allowAdherent')
            ->add('allowRegistration')
            ->add('phone')
            ->add('address')
            ->add('cgv');
    }

    /**
     * @param FormMapper $formMapper
     *
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \Symfony\Component\Form\Exception\LogicException
     * @throws \Symfony\Component\Form\Exception\UnexpectedTypeException
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \RuntimeException
     */
    protected function configureFormFields(FormMapper $formMapper): void
    {
        /** @var Agency $agency */
        $agency = $this->getSubject();
        $choiceLanguage = array();
        foreach ($agency->getLanguages() as $language) {
            $language = $language === 'en-US' ? 'en' : $language;
            $choiceLanguage[$language] = $language;
        }

        $paymentMethods = array();
        foreach (PaymentTypeEnum::toArray() as $key => $value) {
            $paymentMethods[strtolower($key)] = $value;
        }

        /** @var MicrosoftAgency $agencyMicrosoft */
        $agencyMicrosoft = $this->microsoftDynamicsConnection->getRepository(MicrosoftAgency::class)->find(
            array('retailChannelId' => $agency->getRetailChannelId())
        );
        $deliveryModesAvailable = explode(';', $agencyMicrosoft->getDlvModes());

        $deliveryModes = [];
        foreach ($deliveryModesAvailable as $value) {
            $deliveryModes[$value] = $value;
        }

        $formMapper
            ->add(
                'retailChannelId',
                TextType::class,
                array(
                    'attr' => array(
                        'readonly' => 'readonly',
                    ),
                )
            )
            ->add('name')
            ->add('isDigital')
            ->add('slug', TextType::class, array('label' => 'Domaine'))
            ->add('enabledFront')
            ->add('allowAdherent')
            ->add('allowRegistration')
            ->add('phone')
            ->add('idAnalytics')
            ->add('address', SimpleFormatterType::class, array('format' => 'markdown', 'required' => false))
            ->add('email')
            ->add('information', SimpleFormatterType::class, array('format' => 'markdown', 'required' => false))
            ->add('quickInfo', SimpleFormatterType::class, array('format' => 'markdown', 'required' => false));
//            ->add(
//                'logo',
//                'sonata_media_type',
//                array(
//                    'provider' => 'sonata.media.provider.image',
//                    'context' => 'default',
//                )
//            )
        if (($tl = $this->getRequest()->get('tl')) && $tl !== 'fr') {
            $formMapper
                ->add(
                    'headerImg' . ucfirst($tl),
                    'sonata_media_type',
                    array(
                        'provider' => 'sonata.media.provider.image',
                        'context' => 'default',
                    )
                );
        } elseif (($loc = $this->getRequest()->getLocale()) !== 'fr') {
            $formMapper
                ->add(
                    'headerImg' . ucfirst($loc),
                    'sonata_media_type',
                    array(
                        'provider' => 'sonata.media.provider.image',
                        'context' => 'default',
                    )
                );
        } else {
            $formMapper
                ->add(
                    'headerImg',
                    'sonata_media_type',
                    array(
                        'provider' => 'sonata.media.provider.image',
                        'context' => 'default',
                    )
                );
        }
        $formMapper
            ->add(
                'imageBackground',
                'sonata_media_type',
                array(
                    'provider' => 'sonata.media.provider.image',
                    'context' => 'default',
                )
            )
            ->add(
                'imagePresentation',
                'sonata_media_type',
                array(
                    'provider' => 'sonata.media.provider.image',
                    'context' => 'default',
                )
            )
            ->add(
                'modules',
                ChoiceType::class,
                array(
                    'multiple' => true,
                    'expanded' => true,
                    'choices' => AgencyModuleEnum::toArray(),
                )
            )
            ->add('domainWhitelist', ArraySelectType::class)
            ->add(
                'defaultLanguage',
                ChoiceType::class,
                array(
                    'choices' => $choiceLanguage,
                )
            )
            ->add('currency')
            ->add(
                'authorizedPaymentMethods',
                ChoiceType::class,
                array(
                    'choices' => $paymentMethods,
                    'multiple' => true,
                    'translation_domain' => 'messages',
                )
            )
            ->add('cgv')
            ->add(
                'crmServiceClient',
                TextType::class,
                array(
                    'attr' => array(
                        'readonly' => 'readonly',
                    ),
                )
            )
            ->add(
                'crmTeam',
                TextType::class,
                array(
                    'attr' => array(
                        'readonly' => 'readonly',
                    ),
                )
            )
            ->add(
                'crmSite',
                TextType::class,
                array(
                    'attr' => array(
                        'readonly' => 'readonly',
                    ),
                )
            )
            ->add(
                'deliveryMode',
                ChoiceType::class,
                array(
                    'choices' => $deliveryModes,
                    'multiple' => true,
                )
            )
            ->add(
                'dataAreaId',
                TextType::class,
                array(
                    'attr' => array(
                        'readonly' => 'readonly',
                    ),
                )
            );
    }

    /**
     * @param ListMapper $listMapper
     *
     * @throws \RuntimeException
     */
    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('name')
            ->add('slug')
            ->add(
                'enabledFront',
                null,
                array(
                    'editable' => true,
                )
            )
            ->add(
                'allowAdherent',
                null,
                array(
                    'editable' => true,
                )
            )
            ->add(
                'allowRegistration',
                null,
                array(
                    'editable' => true,
                )
            )
            ->add('cgv')
            ->add(
                '_action',
                null,
                array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'operation' => array(
                            'template' => 'TodotodayCoreBundle:Action:action_agency_operation.html.twig',
                        ),
                    ),
                )
            );
    }

    /**
     * @param ShowMapper $showMapper
     *
     * @throws \RuntimeException
     */
    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('id')
            ->add('name')
            ->add('enabledFront')
            ->add('allowAdherent')
            ->add('allowRegistration')
            ->add('phone')
            ->add('address')
            ->add('cgv');
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('operation', $this->getRouterIdParameter() . '/operation');
    }

    /**
     * @param Agency $agency
     *
     * @return null|MicrosoftAgency
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    private function getMicrosoftAgency(Agency $agency): ?MicrosoftAgency
    {
        return $this
            ->microsoftDynamicsConnection
            ->getRepository(MicrosoftAgency::class)
            ->find(array('retailChannelId' => $agency->getRetailChannelId()));
    }
}
