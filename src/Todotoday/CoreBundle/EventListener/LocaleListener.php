<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 25/04/2017
 * Time: 09:20
 */

namespace Todotoday\CoreBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Todotoday\CoreBundle\Services\DomainContextManager;

/**
 * Class LocaleListener
 *
 * @category   Todo-Todev
 * @package    Todotoday\CoreBundle
 * @subpackage Todotoday\CoreBundle\EventListener
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class LocaleListener implements EventSubscriberInterface
{
    /**
     * @var DomainContextManager
     */
    private $domainContextManager;

    /**
     * LocaleListener constructor.
     *
     * @param DomainContextManager $domainContextManager
     */
    public function __construct(DomainContextManager $domainContextManager)
    {
        $this->domainContextManager = $domainContextManager;
    }

    /**
     * Do getSubscribedEvents
     *
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return array(
            // must be registered after the default Locale listener
            KernelEvents::REQUEST => array(array('onKernelRequest', 15)),
        );
    }

    /**
     * Do onKernelRequest
     *
     * @param GetResponseEvent $event
     *
     * @return void
     * @throws \UnexpectedValueException
     */
    public function onKernelRequest(GetResponseEvent $event): void
    {
        $request = $event->getRequest();

        if ($agency = $this->domainContextManager->getCurrentContext()->getAgency()
        ) {
            $request->setLocale(
                $request->getSession()->get(
                    '_locale',
                    $agency->getDefaultLanguage()
                )
            );
        }
        if ($locale = $request->attributes->get('_locale') ?? $request->headers->get('Language')) {
            $request->setLocale($locale);
        }
    }
}
