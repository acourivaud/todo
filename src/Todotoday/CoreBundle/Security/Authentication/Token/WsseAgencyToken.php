<?php declare(strict_types = 1);

namespace Todotoday\CoreBundle\Security\Authentication\Token;

use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;

/**
 * Class WsseAgencyToken
 *
 * @package    Todotoday\CoreBundle
 * @subpackage Todotoday\CoreBundle\Security\Authentication\Token
 */
class WsseAgencyToken extends AbstractToken
{
    public $created;

    public $digest;

    public $nonce;

    /**
     * WsseAgencyToken constructor.
     *
     * @param array $roles
     *
     * @throws \InvalidArgumentException
     */
    public function __construct(array $roles = array())
    {
        parent::__construct($roles);

        // If the user has roles, consider it authenticated
        $this->setAuthenticated(count($roles) > 0);
    }

    /**
     * @return string
     */
    public function getCredentials()
    {
        return '';
    }
}
