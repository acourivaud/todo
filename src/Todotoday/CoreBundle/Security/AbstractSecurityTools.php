<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 31/03/2017
 * Time: 14:39
 */

namespace Todotoday\CoreBundle\Security;

use Symfony\Component\Security\Core\Role\Role;

/**
 * Class AbstractSecurityTools
 *
 * @category   Todo-Todev
 * @package    Todotoday\CoreBundle
 * @subpackage Todotoday\CoreBundle\Security
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
abstract class AbstractSecurityTools
{
    /**
     * Do extractRoles
     *
     * @param array  $attributes
     * @param string $prefix
     *
     * @return array
     */
    public static function extractRoles(array $attributes, string $prefix = 'ROLE_'): array
    {
        $roles = array();

        foreach ($attributes as $attribute) {
            if (mb_strpos($attribute, $prefix) === 0) {
                $roles[] = $attribute;
            }
        }

        return $roles;
    }

    /**
     * Do formatRoles
     *
     * @param array $roles
     *
     * @return array
     */
    public static function formatRoles(array $roles): array
    {
        $rolesFormatted = array();

        foreach ($roles as $role) {
            $rolesFormatted[] = new Role($role);
        }

        return $rolesFormatted;
    }
}
