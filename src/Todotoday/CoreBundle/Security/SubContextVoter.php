<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 16/01/2017
 * Time: 08:55
 */

namespace Todotoday\CoreBundle\Security;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;
use Todotoday\CoreBundle\Services\DomainContextManager;

/**
 * Class SubContextVoter
 *
 * @category   Todo-Todev
 * @package    Todotoday\CoreBundle
 * @subpackage Todotoday\CoreBundle\Security
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class SubContextVoter implements VoterInterface
{
    /**
     * @var DomainContextManager
     */
    protected $domainContextManager;

    /**
     * SubContextVoter constructor.
     *
     * @param DomainContextManager $domainContextManager
     */
    public function __construct(DomainContextManager $domainContextManager)
    {
        $this->domainContextManager = $domainContextManager;
    }

    /**
     * {@inheritdoc}
     * @throws \UnexpectedValueException
     */
    public function vote(TokenInterface $token, $subject, array $attributes)
    {
        if (!($context = $this->domainContextManager->getCurrentSubContext())) {
            return static::ACCESS_ABSTAIN;
        }

        return $context->vote($token, $subject, $attributes);
    }
}
