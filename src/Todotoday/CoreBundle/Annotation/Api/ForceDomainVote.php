<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 07/02/2017
 * Time: 11:45
 */

namespace Todotoday\CoreBundle\Annotation\Api;

/**
 * Class ForceDomainVote
 *
 * @Annotation
 * @Target("METHOD")
 *
 * @category   Todo-Todev
 * @package    Todotoday\CoreBundle
 * @subpackage Todotoday\CoreBundle\Annotation\Api
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class ForceDomainVote
{
    /**
     * Voter value. Default: Voter::ACCESS_ABSTAIN
     *
     * @var int
     */
    public $value;
}
