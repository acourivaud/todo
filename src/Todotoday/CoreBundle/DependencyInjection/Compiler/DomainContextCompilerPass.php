<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 16/01/2017
 * Time: 09:21
 */

namespace Todotoday\CoreBundle\DependencyInjection\Compiler;

use Sonata\AdminBundle\Security\Acl\Permission\AdminPermissionMap;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class DomainContextCompilerPass
 *
 * @category   Todo-Todev
 * @package    Todotoday\CoreBundle
 * @subpackage Todotoday\CoreBundle\DependencyInjection\Compiler
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class DomainContextCompilerPass implements CompilerPassInterface
{
    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     *
     * @throws InvalidArgumentException
     * @throws ServiceNotFoundException
     */
    public function process(ContainerBuilder $container)
    {
        $secMap = $container->getDefinition('security.acl.permission.map');
        $secMap->setClass(AdminPermissionMap::class);
        $domainContextManager = $container->getDefinition('todotoday.core.domain_context');
        $services = $container->findTaggedServiceIds('domain_context');

        /** @var array $tagAttributes */
        foreach ($services as $id => $tagAttributes) {
            foreach ($tagAttributes as $attributes) {
                if (!array_key_exists('key', $attributes)) {
                    throw new InvalidArgumentException(
                        'You need to specify attribute key in tag named domain_context.'
                    );
                }
                $domainContextManager->addMethodCall('addContext', array(new Reference($id), $attributes['key']));
            }
        }
        $services = $container->findTaggedServiceIds('sub_context');

        /** @var array $tagAttributes */
        foreach ($services as $id => $tagAttributes) {
            foreach ($tagAttributes as $attributes) {
                if (!array_key_exists('key', $attributes)) {
                    throw new InvalidArgumentException('You need to specify attribute key in tag named sub_context.');
                }
                $domainContextManager->addMethodCall('addSubContext', array(new Reference($id), $attributes['key']));
            }
        }
    }
}
