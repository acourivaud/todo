<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 20/01/2017
 * Time: 15:18
 */

namespace Todotoday\CoreBundle\DataCollector;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\DataCollector\DataCollector;
use Todotoday\CoreBundle\Services\DomainContextManager;

/**
 * Class DomainContextCollector
 *
 * @category   Todo-Todev
 * @package    Todotoday\CoreBundle
 * @subpackage Todotoday\CoreBundle\DataCollector
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class DomainContextCollector extends DataCollector
{
    /**
     * @var DomainContextManager
     */
    private $domainContextManager;

    /**
     * DomainContextCollector constructor.
     *
     * @param DomainContextManager $domainContextManager
     */
    public function __construct(DomainContextManager $domainContextManager)
    {
        $this->domainContextManager = $domainContextManager;
    }

    /**
     * Collects data for the given Request and Response.
     *
     * @param Request    $request   A Request instance
     * @param Response   $response  A Response instance
     * @param \Exception $exception An Exception instance
     *
     * @throws \UnexpectedValueException
     */
    public function collect(Request $request, Response $response, \Exception $exception = null)
    {
        $agency = $this->domainContextManager->getCurrentContext()->getAgency();
        $this->data = array(
            'current_Agency' => $agency ? $agency->getName() : null,
            'current_domain_context' => $this->domainContextManager->getCurrentContextKey(),
            'all_domain_context' => array_keys($this->domainContextManager->getContexts()),
        );
    }

    /**
     * Do getAllDomainContext
     *
     * @return string[]
     */
    public function getAllDomainContext(): array
    {
        return $this->data['all_domain_context'];
    }

    /**
     * Do getCurrentAgency
     *
     * @return null|string
     */
    public function getCurrentAgency(): ?string
    {
        return $this->data['current_Agency'];
    }

    /**
     * Do getCurrentDomainContext
     *
     * @return string
     */
    public function getCurrentDomainContext(): string
    {
        return $this->data['current_domain_context'];
    }

    /**
     * Returns the name of the collector.
     *
     * @return string The collector name
     */
    public function getName()
    {
        return 'domain_context';
    }

    /**
     *
     */
    public function reset()
    {
        $this->data = [];
    }
}
