<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 18/07/17
 * Time: 21:36
 */

namespace Todotoday\CoreBundle\Tests\Controller\Api;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAccountData;

/**
 * Class MicrosoftApiControllerTest
 * @package Todotoday\CoreBundle\Tests\Controller\Api
 */
class MicrosoftApiControllerTest extends WebTestCase
{
    /**
     * @small
     * @dataProvider envMicrosoftProvider
     *
     * @param string $env
     */
    public function testAccessGrantedForNoosphere(string $env): void
    {
        $url = $this->getUrl('todotoday.core.api.microsoft.get_token', array('env' => $env));
        $this->loginAs(self::getFRR()->getReference('noosphere'), 'api');
        $client = $this->makeClient();
        $client->request('GET', $url);
        $this->assertStatusCode(200, $client);
        $token = json_decode($client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('access_token', $token);
    }

    /**
     * @dataProvider accountProvider
     *
     * @small
     *
     * @param string $accountSlug
     */
    public function testAccessDeniedForAllExceptNoosphere(string $accountSlug): void
    {
        $url = $this->getUrl('todotoday.core.api.microsoft.get_token');
        $this->loginAs(self::getFRR()->getReference($accountSlug), 'api');
        $client = $this->makeClient();
        $client->request('GET', $url);
        $this->assertStatusCode(403, $client);
    }

    /**
     * @return array
     */
    public function envMicrosoftProvider(): array
    {
        return [
            'ax' => ['ax'],
            'crm' => ['crm'],
        ];
    }

    /**
     * @return array
     */
    public function accountProvider(): array
    {
        $provider = [];
        foreach (LoadAccountData::getAllAccountsSlugs() as $accountsSlug) {
            $provider[$accountsSlug] = array($accountsSlug);
        }

        return $provider;
    }

    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return [LoadAccountData::class];
    }
}
