<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 26/01/2017
 * Time: 14:53
 */

namespace Todotoday\CoreBundle\Tests\Services\SubContext;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAccountData;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyData;
use Todotoday\CoreBundle\Interfaces\Services\DomainContext\SubContextInterface;
use Todotoday\CoreBundle\Services\DomainContextManager;

/**
 * Class AdminTest
 *
 * @category   Todo-Todev
 * @package    Todotoday\CoreBundle\Tests\Services\SubContext
 * @subpackage Todotoday\CoreBundle\Tests\Services\SubContext
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class AdminTest extends WebTestCase
{
    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAccountData::class,
        );
    }

    /**
     * Do conciergeProvider
     *
     * @return array[]
     */
    public function conciergeProvider(): array
    {
        $conciergesSlugs = LoadAccountData::getConciergesSlugs();
        $agenciesSlugs = LoadAgencyData::getAgenciesSlug();
        $params = array();

        foreach ($conciergesSlugs as $conciergeKey => $conciergeSlug) {
            foreach ($agenciesSlugs as $agencyKey => $agencySlug) {
                $params[] = array(
                    $agencySlug,
                    $conciergeSlug,
                    $conciergeKey === $agencyKey ? Voter::ACCESS_GRANTED : Voter::ACCESS_DENIED,
                );
            }
        }

        return $params;
    }

    /**
     * Do testVoteAdmin
     *
     * @small
     *
     * @return void
     */
    public function testVoteAdmin()
    {
        $admin = static::getFRR()->getReference('admin');
        /** @var SubContextInterface $subContextAdmin */
        $context = $this->get('todotoday.core.domain_context.www');

        /** @var PHPUnit_Framework_MockObject_MockObject|TokenInterface $token */
        $token = $this->createMock(TokenInterface::class);
        $token->method('getUser')->willReturn($admin);
        static::assertEquals(Voter::ACCESS_ABSTAIN, $context->vote($token, null, array()));
    }

    /**
     * Do testVoteAdmin
     *
     * @dataProvider conciergeProvider
     * @small
     *
     * @param string $agency
     * @param string $conciergeRefId
     * @param int    $expected
     *
     * @return void
     * @internal     param int $key
     */
    public function testVoteConcierge(string $agency, string $conciergeRefId, int $expected)
    {
        $concierge = static::getFRR()->getReference($conciergeRefId);
        $requestStack = $this->getContainer()->get('request_stack');
        $requestStack->push(Request::create('http://' . $agency . '.todotoday.local'));
        /** @var DomainContextManager $contextDomain */
        $context = $this->get('todotoday.core.domain_context.agency');

        /** @var PHPUnit_Framework_MockObject_MockObject|TokenInterface $token */
        $token = $this->createMock(TokenInterface::class);
        $token->method('getUser')->willReturn($concierge);
        static::assertEquals($expected, $context->vote($token, null, array('ROLE_CONCIERGE')));
    }
}
