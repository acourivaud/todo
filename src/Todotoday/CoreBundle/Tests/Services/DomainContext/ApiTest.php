<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 23/01/2017
 * Time: 12:03
 */

namespace Todotoday\CoreBundle\Tests\Services\DomainContext;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyData;

/**
 * Class ApiTest
 *
 * @category   Todo-Todev
 * @package    Todotoday\CoreBundle
 * @subpackage Todotoday\CoreBundle\Tests\Services\DomainContext
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class ApiTest extends WebTestCase
{
    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAgencyData::class,
        );
    }

    /**
     * Do agencyProvider
     *
     * @return array[]
     */
    public function agencyProvider(): array
    {
        $agenciesSlugs = LoadAgencyData::getAgenciesSlug();
        $newAgenciesSlugs = array();

        foreach ($agenciesSlugs as $agencySlug) {
            $newAgenciesSlugs[] = array(
                $agencySlug,
            );
        }

        return $newAgenciesSlugs;
    }

    /**
     * Do testGetAgency
     *
     * @dataProvider agencyProvider
     * @small
     *
     * @param string $agencySlug
     *
     * @return void
     */
    public function testGetAgency(string $agencySlug)
    {
        $requestStack = $this->getContainer()->get('request_stack');

        $request = Request::create('http://api.todotoday.local');
        $request->headers->set('agency', $agencySlug);
        $requestStack->push($request);

        static::assertNotNull($this->getContainer()->get('todotoday.core.domain_context.api')->getAgency());
    }

    /**
     * Do testGetAgency
     *
     * @dataProvider agencyProvider
     * @small
     *
     * @param string $agencySlug
     *
     * @return void
     */
    public function testGetAgencyFail(string $agencySlug)
    {
        $requestStack = $this->getContainer()->get('request_stack');

        $request = Request::create('http://api.todotoday.local');
        $request->headers->set('agency', $agencySlug . 'fail');
        $requestStack->push($request);

        static::assertNull($this->getContainer()->get('todotoday.core.domain_context.api')->getAgency());
    }
}
