<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 23/01/2017
 * Time: 12:03
 */

namespace Todotoday\CoreBundle\Tests\Services\DomainContext;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyData;

/**
 * Class AgencyTest
 *
 * @category   Todo-Todev
 * @package    Todotoday\CoreBundle
 * @subpackage Todotoday\CoreBundle\Tests\Services\DomainContext
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class AgencyTest extends WebTestCase
{
    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAgencyData::class,
        );
    }

    /**
     * Do AgencyProvider
     *
     * @return array[]
     */
    public function agencyProvider(): array
    {
        $agencysSlugs = LoadAgencyData::getAgenciesSlug();
        $newAgencysSlugs = array();

        foreach ($agencysSlugs as $agencySlug) {
            $newAgencysSlugs[] = array(
                $agencySlug,
            );
        }

        return $newAgencysSlugs;
    }

    /**
     * Do testGetAgency
     *
     * @dataProvider agencyProvider
     * @small
     *
     * @param string $agencySlug
     *
     * @return void
     */
    public function testGetAgency(string $agencySlug)
    {
        $requestStack = $this->getContainer()->get('request_stack');

        $request = Request::create('http://' . $agencySlug . '.todotoday.local');
        $requestStack->push($request);

        static::assertNotNull($this->getContainer()->get('todotoday.core.domain_context.agency')->getAgency());
    }

    /**
     * Do testGetAgency
     *
     * @dataProvider agencyProvider
     * @small
     *
     * @param string $agencySlug
     *
     * @return void
     */
    public function testGetAgencyFail(string $agencySlug)
    {
        $requestStack = $this->getContainer()->get('request_stack');

        $request = Request::create('http://' . $agencySlug . 'fail.todotoday.local');
        $requestStack->push($request);

        static::assertNull($this->getContainer()->get('todotoday.core.domain_context.agency')->getAgency());
    }
}
