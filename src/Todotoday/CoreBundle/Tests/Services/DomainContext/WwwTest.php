<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 23/01/2017
 * Time: 12:03
 */

namespace Todotoday\CoreBundle\Tests\Services\DomainContext;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyData;

/**
 * Class WwwTest
 *
 * @category   Todo-Todev
 * @package    Todotoday\CoreBundle
 * @subpackage Todotoday\CoreBundle\Tests\Services\DomainContext
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class WwwTest extends WebTestCase
{
    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAgencyData::class,
        );
    }

    /**
     * Do AgencyProvider
     *
     * @return array[]
     */
    public function agencyProvider(): array
    {
        $agenciesSlugs = LoadAgencyData::getAgenciesSlug();
        $newAgenciesSlugs = array();

        foreach ($agenciesSlugs as $agencySlug) {
            $newAgenciesSlugs[] = array(
                $agencySlug,
            );
        }

        return $newAgenciesSlugs;
    }

    /**
     * Do testGetAgency
     *
     * @small
     * TODO: a faire quand le getAgency admin sera done
     *
     * @return void
     */
    public function testGetAgency()
    {
        static::assertNull($this->getContainer()->get('todotoday.core.domain_context.www')->getAgency());
    }
}
