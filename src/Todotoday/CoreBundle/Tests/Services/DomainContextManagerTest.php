<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 16/01/2017
 * Time: 09:19
 */

namespace Todotoday\CoreBundle\Tests\Services;

use Liip\FunctionalTestBundle\Test\WebTestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Symfony\Component\HttpFoundation\Request;
use Todotoday\CoreBundle\Interfaces\Services\DomainContext\DomainContextInterface;
use Todotoday\CoreBundle\Services\DomainContextManager;

/**
 * Class DomainContextManagerTest
 *
 * @category   Todo-Todev
 * @package    Todotoday\CoreBundle
 * @subpackage Todotoday\CoreBundle\Tests\Services
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class DomainContextManagerTest extends WebTestCase
{
    /**
     * @var DomainContextManager
     */
    protected $domainContextManager;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|DomainContextInterface
     */
    protected $mockContext;

    /**
     * Do testAddContext
     *
     * @group small
     *
     * @return void
     */
    public function testAddContextExceptionKeyExist()
    {
        $this->expectException(\UnexpectedValueException::class);

        /** @var DomainContextInterface $mockContext */
        $mockContext = $this->createMock(DomainContextInterface::class);
        $this->domainContextManager->addContext($mockContext, 'mock');
    }

    /**
     * Do testGetCurrentContext
     *
     * @group small
     *
     * @return void
     */
    public function testGetCurrentContext()
    {
        $requestStack = $this->getContainer()->get('request_stack');
        $requestStack->push(Request::create('http://mock.todotoday.local'));

        static::assertEquals($this->mockContext, $this->domainContextManager->getCurrentContext());
    }

    /**
     * Do testGetSubContext
     *
     * @group small
     *
     * @return void
     */
    public function testGetCurrentSubContext()
    {
        $requestStack = $this->getContainer()->get('request_stack');
        $requestStack->push(Request::create('http://mock.todotoday.local/admin/dashboard'));

        static::assertEquals('admin', $this->domainContextManager->getSubContextKey());
    }

    /**
     * Do testGetSubContext
     *
     * @group small
     *
     * @return void
     */
    public function testGetSubContextNull()
    {
        $requestStack = $this->getContainer()->get('request_stack');
        $requestStack->push(Request::create('http://mock.todotoday.local/'));

        static::assertEquals(null, $this->domainContextManager->getSubContextKey());
    }

    /**
     * {@inheritdoc}
     */
    protected function setUp() : void
    {
        parent::setUp();
        $this->domainContextManager = $this->getContainer()->get('todotoday.core.domain_context');

        /** @var DomainContextInterface $mockContext */
        $this->mockContext = $this->createMock(DomainContextInterface::class);
        $this->domainContextManager->addContext($this->mockContext, 'mock');
    }
}
