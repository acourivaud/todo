<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 28/02/2017
 * Time: 17:30
 */

namespace Todotoday\CoreBundle\Tests\Repository\Api\Microsoft;

use Liip\FunctionalTestBundle\Test\WebTestCase;

/**
 * Class AgencyRepositoryTest
 *
 * @category   Todo-Todev
 * @package    Todotoday\CoreBundle
 * @subpackage Todotoday\CoreBundle\Tests\Repository\Api\Microsoft
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class AgencyRepositoryTest extends WebTestCase
{
    /**
     * Do testFindAllAgenciesWithNoActianeId
     *
     * @return void
     */
    public function testFindAllAgenciesWithNoActianeId()
    {
        $repo = $this->getContainer()->get('todotoday.core.repository.api.agency');

        static::assertInternalType('array', $repo->findAllAgenciesWithNoActianeId());
    }
}
