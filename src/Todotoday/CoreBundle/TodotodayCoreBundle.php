<?php declare(strict_types = 1);
/**
 * PHP version 7
 */

namespace Todotoday\CoreBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Todotoday\CoreBundle\DependencyInjection\Compiler\DomainContextCompilerPass;

/**
 * Class TodotodayCoreBundle
 *
 * @package    Todotoday\CoreBundle
 * @subpackage Todotoday\CoreBundle
 */
class TodotodayCoreBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new DomainContextCompilerPass());
    }
}
