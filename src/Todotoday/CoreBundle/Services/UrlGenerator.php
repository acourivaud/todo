<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: riwan
 * Date: 13/06/17
 * Time: 16:43
 */

namespace Todotoday\CoreBundle\Services;

class UrlGenerator
{
    /**
     * @var string
     */
    private $scheme;

    /**
     * @var string
     */
    private $domain;

    /**
     * UrlGenerator constructor.
     *
     * @param string $scheme
     * @param string $domain
     *
     */
    public function __construct(string $scheme, string $domain)
    {
        $this->scheme = $scheme;
        $this->domain = $domain;
    }

    /**
     * @param string $subdomain
     * @param string $path
     *
     * @return string
     */
    public function generateAbsoluteUrl(string $subdomain, string $path = ''): string
    {
        return $this->scheme . '://' . $subdomain . '.' . $this->domain . $path;
    }
}
