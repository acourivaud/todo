<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 13/01/2017
 * Time: 19:17
 */

namespace Todotoday\CoreBundle\Services;

use Actiane\MailerBundle\Interfaces\MailSenderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Todotoday\CoreBundle\Interfaces\Services\DomainContext\DomainContextInterface;
use Todotoday\CoreBundle\Interfaces\Services\DomainContext\SubContextInterface;

/**
 * Class DomainContextListener
 *
 * @category   Todo-Todev
 * @package    Todotoday\CoreBundle
 * @subpackage Todotoday\CoreBundle\Services
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class DomainContextManager implements MailSenderInterface
{
    /**
     * @var DomainContextInterface[]
     */
    protected $contexts;

    /**
     * @var SubContextInterface[]
     */
    protected $subContexts;

    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * DomainContextManager constructor.
     *
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
        $this->contexts = array();
        $this->subContexts = array();
    }

    /**
     * Do addContext
     *
     * @param DomainContextInterface $domainContext
     * @param string                 $key
     *
     * @return DomainContextManager
     * @throws \UnexpectedValueException
     */
    public function addContext(DomainContextInterface $domainContext, string $key): self
    {
        if (array_key_exists($key, $this->contexts)) {
            throw new \UnexpectedValueException('The context key ' . $key . ' already exist.');
        }

        $this->contexts[$key] = $domainContext;

        return $this;
    }

    /**
     * Do addSubContext
     *
     * @param SubContextInterface $domainContext
     * @param string              $key
     *
     * @return DomainContextManager
     * @throws \UnexpectedValueException
     */
    public function addSubContext(SubContextInterface $domainContext, string $key): self
    {
        if (array_key_exists($key, $this->subContexts)) {
            throw new \UnexpectedValueException('The sub context key ' . $key . ' already exist.');
        }

        $this->subContexts[$key] = $domainContext;
        $domainContext->setDomainContextManager($this);

        return $this;
    }

    /**
     * Do getContext
     *
     * @param string $subDomain
     *
     * @return DomainContextInterface
     */
    public function getContext(string $subDomain): DomainContextInterface
    {
        return $this->contexts[$subDomain];
    }

    /**
     * Get contexts
     *
     * @return array
     */
    public function getContexts(): array
    {
        return $this->contexts;
    }

    /**
     * Set contexts
     *
     * @param array $contexts
     *
     * @return DomainContextManager
     */
    public function setContexts(array $contexts): self
    {
        $this->contexts = $contexts;

        return $this;
    }

    /**
     * Do onKernelRequest
     *
     * @return DomainContextInterface
     * @throws \UnexpectedValueException
     */
    public function getCurrentContext(): DomainContextInterface
    {
        return $this->contexts[$this->getCurrentContextKey()];
    }

    /**
     * Do getCurrentContextKey
     *
     * @return string
     * @throws \UnexpectedValueException
     */
    public function getCurrentContextKey(): string
    {
        $host = $this->getCurrentRequest()->getHost();
        $subDomain = substr($host, 0, strpos($host, '.'));

        return isset($this->contexts[$subDomain]) ? $subDomain : 'default';
    }

    /**
     * @return Request|null
     */
    public function getCurrentRequest(): ?Request
    {
        return $this->requestStack->getCurrentRequest();
    }

    /**
     * Do onKernelRequest
     *
     * @return null|SubContextInterface
     * @throws \UnexpectedValueException
     */
    public function getCurrentSubContext(): ?SubContextInterface
    {
        if ($subContext = $this->getSubContextKey()) {
            return $this->subContexts[$subContext];
        }

        return null;
    }

    /**
     * Do getContext
     *
     * @param string $subDomain
     *
     * @return SubContextInterface
     */
    public function getSubContext(string $subDomain): SubContextInterface
    {
        return $this->subContexts[$subDomain];
    }

    /**
     * Do getCurrentContextKey
     *
     * @return null|string
     * @throws \UnexpectedValueException
     */
    public function getSubContextKey(): ?string
    {
        $matches = array();
        if (preg_match('#\w+#', $this->getCurrentRequest()->getPathInfo(), $matches)) {
            $subContext = $matches[0];

            return isset($this->subContexts[$subContext]) ? $subContext : null;
        }

        return null;
    }

    /**
     * Get contexts
     *
     * @return array
     */
    public function getSubContexts(): array
    {
        return $this->subContexts;
    }

    /**
     * Set contexts
     *
     * @param array $contexts
     *
     * @return DomainContextManager
     */
    public function setSubContexts(array $contexts): self
    {
        $this->subContexts = $contexts;

        return $this;
    }

    /**
     * @return string
     * @throws \UnexpectedValueException
     */
    public function getName(): ?string
    {
        if (!$agency = $this->getCurrentContext()->getAgency()) {
            return null;
        }

        return $agency->getName();
    }

    /**
     * @return string
     * @throws \UnexpectedValueException
     */
    public function getEmail(): ?string
    {
        if (!$agency = $this->getCurrentContext()->getAgency()) {
            return null;
        }

        return $agency->getEmail();
    }

    /**
     * @return string
     */
    public function getCorporate(): ?string
    {
        return 'TO DO TODAY';
    }
}
