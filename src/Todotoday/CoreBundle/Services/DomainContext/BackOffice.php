<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 14/01/2017
 * Time: 03:21
 */

namespace Todotoday\CoreBundle\Services\DomainContext;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Interfaces\Services\DomainContext\DomainContextInterface;

/**
 * Class BackOffice
 *
 * @category   Todo-Todev
 * @package    Todotoday\CoreBundle
 * @subpackage Todotoday\CoreBundle\Services\DomainContext
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class BackOffice implements DomainContextInterface
{
    /**
     * @var string
     */
    protected $domain;

    /**
     * BackOffice constructor.
     *
     * @param string $domain
     */
    public function __construct(string $domain)
    {
        $this->domain = $domain;
    }

    /**
     * Do getAgency
     *
     * @return Agency|null
     */
    public function getAgency()
    {
        return (new Agency())->setSlug('backoffice')->setName('BackOffice')->setEmail('no-reply@' . $this->domain);
    }

    /**
     * {@inheritdoc}
     */
    public function vote(TokenInterface $token, $subject, array $attributes): int
    {
        return Voter::ACCESS_ABSTAIN;
    }
}
