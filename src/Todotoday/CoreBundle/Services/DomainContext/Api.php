<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 13/01/2017
 * Time: 23:53
 */

namespace Todotoday\CoreBundle\Services\DomainContext;

use Doctrine\Common\Annotations\Reader;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\Role\RoleHierarchy;
use Symfony\Component\Security\Core\Role\RoleInterface;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\AccountBundle\Entity\LinkAccountAgency;
use Todotoday\AccountBundle\Repository\LinkAccountAgencyRepository;
use Todotoday\CoreBundle\Annotation\Api\ForceDomainVote;
use Todotoday\CoreBundle\Entity\Agency as EntityAgency;
use Todotoday\CoreBundle\Interfaces\Services\DomainContext\DomainContextInterface;
use Todotoday\CoreBundle\Repository\AgencyRepository;
use Todotoday\CoreBundle\Security\AbstractSecurityTools;

/**
 * Class Api
 *
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @category   Todo-Todev
 * @package    Todotoday\CoreBundle
 * @subpackage Todotoday\CoreBundle\Services\DomainContext
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class Api implements DomainContextInterface
{
    /**
     * @var AgencyRepository
     */
    protected $agencyRepository;

    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @var EntityAgency
     */
    protected $currentAgency;

    /**
     * @var RoleHierarchy
     */
    protected $roleHierarchy;

    /**
     * @var LinkAccountAgencyRepository
     */
    protected $linkAccountAgencyRepository;

    /**
     * Api constructor.
     *
     * @var Reader
     */
    protected $reader;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * Api constructor.
     *
     * @param EntityManager               $entityManager
     * @param AgencyRepository            $agencyRepository
     * @param LinkAccountAgencyRepository $linkAccountAgencyRepository
     * @param Reader                      $reader
     * @param RequestStack                $requestStack
     * @param array                       $roles
     */
    public function __construct(
        EntityManager $entityManager,
        AgencyRepository $agencyRepository,
        LinkAccountAgencyRepository $linkAccountAgencyRepository,
        Reader $reader,
        RequestStack $requestStack,
        array $roles
    ) {
        $this->agencyRepository = $agencyRepository;
        $this->linkAccountAgencyRepository = $linkAccountAgencyRepository;
        $this->requestStack = $requestStack;
        $this->roleHierarchy = new RoleHierarchy($roles);
        $this->reader = $reader;
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritdoc}
     * @throws \UnexpectedValueException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     */
    public function getAgency()//: ?EntityAgency
    {
        if (!$this->currentAgency) {
            $subDomain = $this->requestStack->getCurrentRequest()->headers->get('Agency');

            $this->currentAgency = $this->agencyRepository
                ->findOneBy(
                    array(
                        'slug' => $subDomain,
                    )
                );
        }

        return $this->currentAgency;
    }

    /**
     * {@inheritdoc}
     * @throws \UnexpectedValueException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function vote(TokenInterface $token, $subject, array $attributes): int
    {
        if ($subject instanceof Request && !in_array('IS_AUTHENTICATED_ANONYMOUSLY', $attributes, true)) {
            if ($vote = $this->getForceVote($subject->get('_controller'))) {
                return $vote;
            }

            /** @var Account $account */
            $account = $token->getUser();
            /** @var RoleInterface[] $accountRoles */
            $accountRoles = $this->roleHierarchy->getReachableRoles($this->formatRoles($account->getRoles()));

            foreach ($accountRoles as $accountRole) {
                if ($accountRole->getRole() === 'ROLE_PARTNER') {
                    return Voter::ACCESS_ABSTAIN;
                }
            }

            if (!($agency = $this->getAgency())) {
                return Voter::ACCESS_ABSTAIN;
            }

            if (!$agency->getEnabledFront() || !$agency->isAllowAdherent()) {
                return Voter::ACCESS_DENIED;
            }

            /** @var LinkAccountAgency $linkAccountAgency */
            if (!$linkAccountAgency = $this->linkAccountAgencyRepository->findOneBy(
                array(
                    'account' => $account->getId(),
                    'agency' => $agency->getId(),
                )
            )
            ) {
                return Voter::ACCESS_DENIED;
            }

            $roles = AbstractSecurityTools::extractRoles($attributes);

            if (!$roles) {
                return Voter::ACCESS_ABSTAIN;
            }

            return $this->processVote($linkAccountAgency, $account, $agency, $roles);
        }

        return Voter::ACCESS_ABSTAIN;
    }

    /**
     * Do formatRoles
     *
     * @param array $roles
     *
     * @return array
     */
    protected function formatRoles(array $roles): array
    {
        $rolesFormatted = array();

        foreach ($roles as $role) {
            $rolesFormatted[] = new Role($role);
        }

        return $rolesFormatted;
    }

    /**
     * Do getForceVote
     *
     * @param string $controller
     *
     * @return null|int
     * @throws \ReflectionException
     */
    protected function getForceVote(string $controller): ?int
    {
        preg_match('#(.*)::(.*)#', $controller, $matches);
        if (!array_key_exists(1, $matches)) {
            return null;
        }
        $reflect = new \ReflectionClass($matches[1]);
        $method = $reflect->getMethod($matches[2]);
        /** @var ForceDomainVote $annotation */
        $annotation = $this->reader->getMethodAnnotation($method, ForceDomainVote::class);

        if ($annotation) {
            if ($annotation->value) {
                return (int) $annotation->value;
            }

            return Voter::ACCESS_ABSTAIN;
        }

        return null;
    }

    /**
     * Do processVote
     *
     * @param LinkAccountAgency $linkAccountAgency
     * @param Account           $account
     * @param EntityAgency      $agency
     * @param array             $roles
     *
     * @return int
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function processVote(
        LinkAccountAgency $linkAccountAgency,
        Account $account,
        EntityAgency $agency,
        array $roles
    ) {
        /** @var RoleInterface[] $accountRoles */
        $accountRoles = $this->roleHierarchy->getReachableRoles(
            AbstractSecurityTools::formatRoles($linkAccountAgency->getRoles())
        );

        foreach ($accountRoles as $accountRole) {
            foreach ($roles as $role) {
                if ($accountRole->getRole() === $role) {
                    if (!$account->getLastAgencyUsed()
                        || $account->getLastAgencyUsed()->getId() !== $agency->getId()
                    ) {
                        $account->setLastAgencyUsed($agency);
                        $this->entityManager->flush();
                    }

                    return Voter::ACCESS_GRANTED;
                }
            }
        }

        return Voter::ACCESS_ABSTAIN;
    }
}
