<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 13/01/2017
 * Time: 23:53
 */

namespace Todotoday\CoreBundle\Services\DomainContext;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Role\RoleHierarchy;
use Symfony\Component\Security\Core\Role\RoleInterface;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\AccountBundle\Entity\Admin as EntityAdmin;
use Todotoday\AccountBundle\Entity\LinkAccountAgency;
use Todotoday\AccountBundle\Repository\LinkAccountAgencyRepository;
use Todotoday\CoreBundle\Entity\Agency as EntityAgency;
use Todotoday\CoreBundle\Interfaces\Services\DomainContext\DomainContextInterface;
use Todotoday\CoreBundle\Repository\AgencyRepository;
use Todotoday\CoreBundle\Security\AbstractSecurityTools;

/**
 * Class Agency
 *
 * @category   Todo-Todev
 * @package    Todotoday\CoreBundle
 * @subpackage Todotoday\CoreBundle\Services\DomainContext
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class Agency implements DomainContextInterface
{
    /**
     * @var EntityManagerInterface
     */
    protected $agencyRepository;

    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @var EntityAgency
     */
    protected $currentAgency;

    /**
     * @var RoleHierarchy
     */
    protected $roleHierarchy;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * Agency constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param AgencyRepository       $agencyRepository
     * @param RequestStack           $requestStack
     * @param array                  $roles
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        AgencyRepository $agencyRepository,
        RequestStack $requestStack,
        array $roles
    ) {
        $this->agencyRepository = $agencyRepository;
        $this->requestStack = $requestStack;
        $this->roleHierarchy = new RoleHierarchy($roles);
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritdoc}
     * @throws \UnexpectedValueException
     */
    public function getAgency()//: ?EntityAgency
    {
        if (!$this->currentAgency) {
            $host = $this->requestStack->getCurrentRequest()->getHost();
            $subDomain = substr($host, 0, strpos($host, '.'));

            $this->currentAgency = $this->agencyRepository
                ->findOneBy(
                    array(
                        'slug' => $subDomain,
                        'enabledFront' => true,
                    )
                );
        }

        return $this->currentAgency;
    }

    /**
     * {@inheritdoc}
     * @throws \UnexpectedValueException
     */
    public function vote(TokenInterface $token, $subject, array $attributes): int
    {
        $roles = AbstractSecurityTools::extractRoles($attributes);

        if (!$roles) {
            return Voter::ACCESS_ABSTAIN;
        }

        /** @var Account $account */
        if (!($account = $token->getUser()) instanceof Account) {
            return Voter::ACCESS_ABSTAIN;
        }

        if ($account instanceof EntityAdmin) {
            return Voter::ACCESS_GRANTED;
        }
        /** @var LinkAccountAgencyRepository $repo */
        $repo = $this->entityManager->getRepository(LinkAccountAgency::class);
        if (!($agency = $this->getAgency())) {
            return Voter::ACCESS_DENIED;
        }

        /** @var LinkAccountAgency $linkAccountAgency */
        if (!($linkAccountAgency = $repo->findOneBy(
            array(
                'account' => $account->getId(),
                'agency' => $agency->getId(),
            )
        ))
        ) {
            return Voter::ACCESS_DENIED;
        }

        return $this->processVote($linkAccountAgency, $account, $agency, $roles);
    }

    /**
     * Do processVote
     *
     * @param LinkAccountAgency $linkAccountAgency
     * @param Account           $account
     * @param EntityAgency      $agency
     * @param array             $roles
     *
     * @return int
     */
    protected function processVote(
        LinkAccountAgency $linkAccountAgency,
        Account $account,
        EntityAgency $agency,
        array $roles
    ) {
        /** @var RoleInterface[] $accountRoles */
        $accountRoles = $this->roleHierarchy->getReachableRoles(
            AbstractSecurityTools::formatRoles($linkAccountAgency->getRoles())
        );

        foreach ($accountRoles as $accountRole) {
            foreach ($roles as $role) {
                if ($accountRole->getRole() === $role) {
                    if (!$account->getLastAgencyUsed()
                        || $account->getLastAgencyUsed()->getId() !== $agency->getId()
                    ) {
                        $account->setLastAgencyUsed($agency);
                        $this->entityManager->flush();
                    }

                    return Voter::ACCESS_GRANTED;
                }
            }
        }

        return Voter::ACCESS_DENIED;
    }
}
