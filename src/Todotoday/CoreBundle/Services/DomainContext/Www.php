<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 14/01/2017
 * Time: 03:21
 */

namespace Todotoday\CoreBundle\Services\DomainContext;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Interfaces\Services\DomainContext\DomainContextInterface;

/**
 * Class Www
 *
 * @category   Todo-Todev
 * @package    Todotoday\CoreBundle
 * @subpackage Todotoday\CoreBundle\Services\DomainContext
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class Www implements DomainContextInterface
{
    /**
     * Do getAgency
     *
     * @return Agency|null
     */
    public function getAgency()//: ?Agency
    {
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function vote(TokenInterface $token, $subject, array $attributes): int
    {
        // TODO faire le vote en fonction de la Agencyrie courante.
        return Voter::ACCESS_ABSTAIN;
    }
}
