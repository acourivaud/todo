<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 14/01/2017
 * Time: 03:24
 */

namespace Todotoday\CoreBundle\Services\SubContext;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Role\RoleHierarchy;
use Todotoday\CoreBundle\Interfaces\Services\DomainContext\SubContextInterface;
use Todotoday\CoreBundle\Services\DomainContextManager;

/**
 * Class Admin
 *
 * @category   Todo-Todev
 * @package    Todotoday\CoreBundle
 * @subpackage Todotoday\CoreBundle\Services\DomainContext
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class Admin implements SubContextInterface
{
    /**
     * @var RoleHierarchy
     */
    protected $roleHierarchy;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var DomainContextManager
     */
    protected $domainContextManager;

    /**
     * Admin constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param array                  $roles
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        array $roles
    ) {
        $this->entityManager = $entityManager;
        $this->roleHierarchy = new RoleHierarchy($roles);
    }

    /**
     * {@inheritdoc}
     */
    public function getDomainContextManager(): DomainContextManager
    {
        return $this->domainContextManager;
    }

    /**
     * {@inheritdoc}
     */
    public function setDomainContextManager(DomainContextManager $domainContextManager): SubContextInterface
    {
        $this->domainContextManager = $domainContextManager;

        return $this;
    }

    /**
     * {@inheritdoc}
     * @throws \UnexpectedValueException
     */
    public function vote(TokenInterface $token, $subject, array $attributes): int
    {
        return Voter::ACCESS_ABSTAIN;
    }
}
