<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 02/03/2017
 * Time: 14:46
 */

namespace Todotoday\CoreBundle\Services\Block;

use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\BlockBundle\Block\Service\AbstractBlockService;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Response;
use Todotoday\CoreBundle\Admin\AgencyAdmin;
use Todotoday\CoreBundle\Repository\Api\Microsoft\AgencyRepository;

/**
 * Class AgencyNotCreatedBlockService
 *
 * @category   Todo-Todev
 * @package    Todotoday\CoreBundle
 * @subpackage Todotoday\CoreBundle\Services\Block
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class AgencyNotCreatedBlockService extends AbstractBlockService
{
    /**
     * @var AgencyRepository
     */
    private $agencyRepository;

    /**
     * @var AgencyAdmin
     */
    private $agencyAdmin;

    /**
     * AgencyNotCreatedBlockService constructor.
     *
     * @param null                 $name
     * @param EngineInterface|null $templating
     * @param AgencyRepository     $agencyRepository
     * @param AgencyAdmin          $agencyAdmin
     */
    public function __construct(
        $name = null,
        EngineInterface $templating = null,
        AgencyRepository $agencyRepository,
        AgencyAdmin $agencyAdmin
    ) {
        parent::__construct($name, $templating);
        $this->agencyRepository = $agencyRepository;
        $this->agencyAdmin = $agencyAdmin;
    }

    /**
     * {@inheritdoc}
     * @throws \InvalidArgumentException
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null): Response
    {
        return $this->renderResponse('@TodotodayCore/Blocks/agency_not_created.html.twig', array(
            'block_context' => $blockContext,
            'block' => $blockContext->getBlock(),
            'agencies' => $this->agencyRepository->findAllAgenciesWithNoActianeId(),
            'agencyAdmin' => $this->agencyAdmin,
        ), $response);
    }
}
