<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 11/01/18
 * Time: 11:49
 *
 * @category   tdtd
 *
 * @package    Todotoday\CoreBundle\Services\ChangeWatch
 *
 * @subpackage Todotoday\CoreBundle\Services\ChangeWatch
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\CoreBundle\Services\ChangeWatch;

use Actiane\EntityChangeWatchBundle\Interfaces\InterfaceHelper;
use Todotoday\CatalogBundle\Services\CatalogManager;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class AgencyChangeWatch
 */
class AgencyChangeWatch implements InterfaceHelper
{
    public const KEY = 'Agency';

    /**
     * @var CatalogManager
     */
    private $catalogManager;

    /**
     * AgencyChangeWatch constructor.
     *
     * @param CatalogManager $catalogManager
     */
    public function __construct(CatalogManager $catalogManager)
    {
        $this->catalogManager = $catalogManager;
    }

    /**
     * Compute the signature of the callable, used to avoid a callable to be called multiple times in a row
     *
     * @param array $callable
     * @param array $parameters
     *
     * @return mixed
     */
    public function computeSignature(array $callable, array $parameters)
    {
        return self::KEY . $callable[1] . ':' . $parameters['entity']->getId();
    }

    /**
     * @param Agency $agency
     */
    public function clearCacheCatalog(Agency $agency): void
    {
        foreach ($agency->getLanguages() as $lang) {
            $this->catalogManager->resetCache($agency, $lang);
        }
    }
}
