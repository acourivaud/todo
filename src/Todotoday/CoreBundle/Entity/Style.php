<?php declare(strict_types = 1);

namespace Todotoday\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Style
 *
 * @ORM\Table(name="tdtd_style")
 * @ORM\Entity(repositoryClass="Todotoday\CoreBundle\Repository\StyleRepository")
 */
class Style
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

//    /**
//     * @var Agency
//     * @ORM\OneToOne(targetEntity="Agency", mappedBy="style")
//     */
//    private $agency;

    /**
     * @var string
     *
     * @ORM\Column(name="mainBackground", type="string", length=7, nullable=true)
     * @Assert\NotBlank()
     */
    private $mainBackground;

    /**
     * @var string
     *
     * @ORM\Column(name="mainFontColor", type="string", length=7, nullable=true)
     * @Assert\NotBlank()
     */
    private $mainFontColor;

    /**
     * @return string
     */
    public function __toString()
    {
        return 'style';
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Get mainBackground
     *
     * @return string
     */
    public function getMainBackground(): ?string
    {
        return $this->mainBackground;
    }

    /**
     * Set mainBackground
     *
     * @param string $mainBackground
     *
     * @return Style
     */
    public function setMainBackground(?string $mainBackground): Style
    {
        $this->mainBackground = $mainBackground;

        return $this;
    }

    /**
     * Get mainFontColor
     *
     * @return string
     */
    public function getMainFontColor(): ?string
    {
        return $this->mainFontColor;
    }

    /**
     * Set mainFontColor
     *
     * @param string $mainFontColor
     *
     * @return Style
     */
    public function setMainFontColor(?string $mainFontColor): Style
    {
        $this->mainFontColor = $mainFontColor;

        return $this;
    }

//    /**
//     * Set agency
//     *
//     * @param Agency $agency
//     *
//     * @return Style
//     */
//    public function setAgency(Agency $agency = null): Style
//    {
//        $this->agency = $agency;
//
//        return $this;
//    }
//
//    /**
//     * Get agency
//     *
//     * @return Agency
//     */
//    public function getAgency(): Agency
//    {
//        return $this->agency;
//    }
}
