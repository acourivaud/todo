<?php declare(strict_types=1);

namespace Todotoday\CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;
use Todotoday\AccountBundle\Entity\LinkAccountAgency;
use Todotoday\CartBundle\Entity\CartProduct;
use Todotoday\CartBundle\Entity\Wishlist;
use Todotoday\CheckoutBundle\Entity\CheckoutInfoNeeded;
use Todotoday\CheckoutBundle\Entity\DeliveryDelay;
use Todotoday\CMSBundle\Entity\Cgv;
use Todotoday\CMSBundle\Entity\LinkFileAgency;
use Todotoday\CMSBundle\Entity\LinkPostAgency;
use Todotoday\CMSBundle\Enum\LinkPostAgencyStateEnum;
use Todotoday\CMSBundle\Enum\PostStateEnum;
use Todotoday\MediaBundle\Entity\Media;
use Todotoday\PluginBundle\Entity\LinkAgencyPlugin;
use Todotoday\SocialBundle\Entity\LinkAgencySocialGroup;

/**
 * Agency
 *
 * @SuppressWarnings(PHPMD)
 * @ORM\Table(name="agency", schema="core")
 * @ORM\Entity(repositoryClass="Todotoday\CoreBundle\Repository\AgencyRepository")
 * @Gedmo\TranslationEntity(class="Todotoday\CoreBundle\Entity\AgencyTranslation")
 * @Serializer\ExclusionPolicy("all")
 *
 */
class Agency extends AbstractPersonalTranslatable implements TranslatableInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @Gedmo\Slug(fields={"name"}, updatable=false)
     * @ORM\Column(name="slug", type="string", length=128)
     * @Serializer\Expose()
     */
    protected $slug;

    /**
     * @var string
     * @Serializer\Expose()
     * @ORM\Column(name="id_analytics", type="string", length=254, nullable=true)
     */
    protected $idAnalytics;

    /**
     * @var string[]
     * @Serializer\Expose()
     * @ORM\Column(name="modules", type="simple_array", nullable=true)
     */
    protected $modules;

    /**
     * @var string
     * @Serializer\Expose()
     * @ORM\Column(name="retail_channel_id", type="string", nullable=true)
     */
    protected $retailChannelId;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=254, nullable=true)
     * @Serializer\Expose()
     */
    protected $email;

    /**
     * fields : media
     * Media with the Agency's image
     *
     * @var Media
     *
     * @ORM\ManyToOne(targetEntity="Todotoday\MediaBundle\Entity\Media", cascade={"persist", "remove", "refresh"})
     * @Serializer\Expose()
     */
    protected $logo;

    /**
     * fields : media
     * Media with the Agency's image
     *
     * @var Media
     *
     * @ORM\ManyToOne(targetEntity="Todotoday\MediaBundle\Entity\Media", cascade={"persist", "remove", "refresh"})
     * @Serializer\Expose()
     *
     */
    protected $headerImgEn;

    /**
     * fields : media
     * Media with the Agency's image
     *
     * @var Media
     *
     * @ORM\ManyToOne(targetEntity="Todotoday\MediaBundle\Entity\Media", cascade={"persist", "remove", "refresh"})
     * @Serializer\Expose()
     */
    protected $headerImgDe;

    /**
     * fields : media
     * Media with the Agency's image
     *
     * @var Media
     *
     * @ORM\ManyToOne(targetEntity="Todotoday\MediaBundle\Entity\Media", cascade={"persist", "remove", "refresh"})
     * @Serializer\Expose()
     */
    protected $headerImg;

    /**
     * @var Media
     * @ORM\ManyToOne(targetEntity="Todotoday\MediaBundle\Entity\Media", cascade={"persist", "remove", "refresh"})
     * @ORM\JoinColumn(nullable=true)
     * @Serializer\Expose()
     */
    protected $imageBackground;

    /**
     * @var Media
     * @ORM\ManyToOne(targetEntity="Todotoday\MediaBundle\Entity\Media", cascade={"persist", "remove", "refresh"})
     * @ORM\JoinColumn(nullable=true)
     * @Serializer\Expose()
     */
    protected $imagePresentation;

    /**
     * @var Collection|LinkPostAgency[]
     *
     * @ORM\OneToMany(targetEntity="Todotoday\CMSBundle\Entity\LinkPostAgency", mappedBy="agency", cascade={"remove"})
     * @ORM\OrderBy({"publicationDate" = "DESC"})
     */
    protected $posts;

    /**
     * @var Collection|LinkFileAgency[]
     *
     * @ORM\OneToMany(targetEntity="Todotoday\CMSBundle\Entity\LinkFileAgency", mappedBy="agency", cascade={"remove"})
     * @ORM\OrderBy({"id" = "DESC"})
     */
    protected $files;

    /**
     * @var Collection|LinkAccountAgency[]
     *
     * @ORM\OneToMany(targetEntity="Todotoday\AccountBundle\Entity\LinkAccountAgency", mappedBy="agency")
     */
    protected $linkAccounts;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150)
     * @Serializer\Expose()
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=64, nullable=true)
     * @Serializer\Expose()
     */
    protected $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     * @Serializer\Expose()
     */
    protected $address;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="enabled_front", type="boolean")
     * @Serializer\Expose()
     */
    protected $enabledFront = false;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", name="allow_adherent")
     * @Serializer\Expose()
     */
    protected $allowAdherent = true;

    /**
     * @var bool
     * @ORM\Column(type="boolean", name="allow_registration")
     * @Serializer\Expose()
     */
    protected $allowRegistration = true;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=15, nullable=true)
     * @Serializer\Expose()
     */
    protected $status;

    /**
     * @var string
     *
     * @ORM\Column(name="information", type="text", nullable=true)
     * @Gedmo\Translatable()
     * @Serializer\Expose()
     */
    protected $information;

    /**
     * @var Collection|Corporate[]
     *
     * @ORM\ManyToMany(targetEntity="Todotoday\CoreBundle\Entity\Corporate", inversedBy="agencies")
     * @ORM\JoinTable(name="link_agency_corporate", schema="core")
     */
    protected $corporates;

    /**
     * @var string[]
     *
     * @ORM\Column(name="domain_whitelist", type="simple_array", nullable=true)
     */
    protected $domainWhitelist;

    /**
     * @var string[]
     *
     * @ORM\Column(name="authorized_payment_methods", type="simple_array", nullable=true)
     * @Serializer\Expose()
     */
    protected $authorizedPaymentMethods;

    /**
     * @var string
     *
     * @ORM\Column(name="currency", type="string", length=15)
     * @Serializer\Expose()
     */
    protected $currency;

//    /**
//     * @ORM\OneToOne(targetEntity="Style", inversedBy="agency",
//     *     cascade={"persist", "remove", "refresh"})
//     * @Serializer\Expose()
//     */
//    protected $style;

    /**
     * @var Collection|CartProduct[]
     *
     * @ORM\OneToMany(targetEntity="Todotoday\CartBundle\Entity\CartProduct", mappedBy="agency")
     */
    protected $cartProducts;

    /**
     * @var Collection|Wishlist[]
     *
     * @ORM\OneToMany(targetEntity="Todotoday\CartBundle\Entity\Wishlist", mappedBy="agency")
     */
    protected $wishlists;

    /**
     * @var Collection|LinkAgencySocialGroup[]
     *
     * @ORM\OneToMany(targetEntity="Todotoday\SocialBundle\Entity\LinkAgencySocialGroup", mappedBy="agency")
     */
    protected $linkSocialGroups;

    /**
     * @var array
     *
     * @ORM\Column(type="simple_array", name="languages")
     * @Serializer\Expose()
     */
    protected $languages;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="default_language", nullable=false)
     */
    protected $defaultLanguage;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="crm_service_client")
     * @Serializer\Expose()
     */
    protected $crmServiceClient;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="crm_team")
     * @Serializer\Expose()
     */
    protected $crmTeam;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="crm_site", nullable=true)
     */
    protected $crmSite;

    /**
     * @var array
     *
     * @ORM\Column(type="simple_array", name="delivery_mode")
     * @Serializer\Expose()
     * @Serializer\Type("array<Todotoday\CoreBundle\Entity\DeliveryMode>")
     */
    protected $deliveryMode;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="data_area_id")
     */
    protected $dataAreaId;

    /**
     * @var string
     * @ORM\Column(type="text", name="schedule", nullable=true)
     * @Gedmo\Translatable()
     * @Serializer\Expose()
     */
    protected $schedule;

    /**
     * @var string
     * @ORM\Column(type="text", name="quick_info", nullable=true)
     * @Gedmo\Translatable()
     * @Serializer\Expose()
     */
    protected $quickInfo;

    /**
     * @ORM\OneToMany(
     *      targetEntity="Todotoday\CoreBundle\Entity\AgencyTranslation",
     *      mappedBy="object",
     *      cascade={"persist","remove"}
     * )
     */
    protected $translations;

    /**
     * @ORM\ManyToOne(targetEntity="Todotoday\CMSBundle\Entity\Cgv")
     * @Serializer\Expose()
     * @Serializer\Groups({"account"})
     */
    protected $cgv;

    /**
     * @var Collection|LinkAgencyPlugin[]
     *
     * @ORM\OneToMany(targetEntity="Todotoday\PluginBundle\Entity\LinkAgencyPlugin", mappedBy="agency",
     *     cascade={"persist"})
     * @Serializer\Expose()
     * @Serializer\Groups({"plugin"})
     */
    protected $linkPlugins;

    /**
     * @var Collection|CheckoutInfoNeeded[]
     *
     * @ORM\OneToMany(targetEntity="Todotoday\CheckoutBundle\Entity\CheckoutInfoNeeded", mappedBy="agency")
     */
    protected $checkoutInfos;

    /**
     * @var Collection|DeliveryDelay[]
     *
     * @ORM\OneToMany(targetEntity="Todotoday\CheckoutBundle\Entity\DeliveryDelay", mappedBy="agency")
     */
    protected $deliveryDelays;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", name="is_digital")
     * @Serializer\Expose()
     */
    protected $isDigital = false;

    /**
     * Agency constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->createdAt = new \DateTime();
        $this->cartProducts = new ArrayCollection();
        $this->wishlists = new ArrayCollection();
        $this->posts = new ArrayCollection();
        $this->linkSocialGroups = new ArrayCollection();
        $this->modules = array();
        $this->domainWhitelist = array();
        $this->authorizedPaymentMethods = array();
        $this->languages = array();
        $this->deliveryMode = array();
        $this->linkPlugins = new ArrayCollection();
        $this->checkoutInfos = new ArrayCollection();
        $this->deliveryDelays = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString(): ?string
    {
        return (string) $this->getName();
    }

    /**
     * Do addAuthorizedPaymentMethods
     *
     * @param string $authorizedPaymentMethod
     *
     * @return Agency
     */
    public function addAuthorizedPaymentMethods(string $authorizedPaymentMethod): self
    {
        if (!\in_array($authorizedPaymentMethod, $this->authorizedPaymentMethods, true)) {
            $this->authorizedPaymentMethods[] = $authorizedPaymentMethod;
        }

        return $this;
    }

    /**
     * Add cartProduct
     *
     * @param CartProduct $cartProduct
     *
     * @return Agency
     */
    public function addCartProduct(CartProduct $cartProduct): self
    {
        $this->cartProducts[] = $cartProduct;
        $cartProduct->setAgency($this);

        return $this;
    }

    /**
     * Add corporate
     *
     * @param Corporate $corporate
     *
     * @return Agency
     */
    public function addCorporate(Corporate $corporate): self
    {
        $this->corporates[] = $corporate;

        return $this;
    }

    /**
     * Do addDomainWhitelist
     *
     * @param string $domainWhite
     *
     * @return Agency
     */
    public function addDomainWhitelist(string $domainWhite): self
    {
        if (!\in_array($domainWhite, $this->domainWhitelist, true)) {
            $this->domainWhitelist[] = $domainWhite;
        }

        return $this;
    }

    /**
     * Add linkAccount
     *
     * @param LinkAccountAgency $linkAccount
     *
     * @return Agency
     */
    public function addLinkAccount(LinkAccountAgency $linkAccount): self
    {
        $this->linkAccounts[] = $linkAccount;
        $linkAccount->setAgency($this);

        return $this;
    }

    /**
     * Add linkPlugin
     *
     * @param LinkAgencyPlugin $linkPlugin
     *
     * @return Agency
     */
    public function addLinkPlugin(LinkAgencyPlugin $linkPlugin): self
    {
        $this->linkPlugins[] = $linkPlugin;
        $linkPlugin->setAgency($this);

        return $this;
    }

    /**
     * Add linkSocialGroup
     *
     * @param LinkAgencySocialGroup $linkSocialGroup
     *
     * @return Agency
     */
    public function addLinkSocialGroup(LinkAgencySocialGroup $linkSocialGroup): self
    {
        $this->linkSocialGroups[] = $linkSocialGroup;
        $linkSocialGroup->setAgency($this);

        return $this;
    }

    /**
     * Do addModule
     *
     * @param string $module
     *
     * @return Agency
     */
    public function addModule(string $module): self
    {
        $this->modules[] = $module;

        return $this;
    }

    /**
     * Add post
     *
     * @param LinkPostAgency $post
     *
     * @return Agency
     */
    public function addPost(LinkPostAgency $post): self
    {
        $this->posts[] = $post;

        return $this;
    }

    /**
     * Add wishlist
     *
     * @param Wishlist $wishlist |Wishlist
     *
     * @return Agency
     */
    public function addWishlist(Wishlist $wishlist): self
    {
        $this->wishlists[] = $wishlist;
        $wishlist->setAgency($this);

        return $this;
    }

    /**
     * @return string
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string $address
     *
     * @return Agency
     */
    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get authorizedPaymentMethods
     *
     * @return string[]
     */
    public function getAuthorizedPaymentMethods(): array
    {
        return $this->authorizedPaymentMethods;
    }

    /**
     * Set authorizedPaymentMethods
     *
     * @param string[] $authorizedPaymentMethods
     *
     * @return Agency
     */
    public function setAuthorizedPaymentMethods(array $authorizedPaymentMethods): self
    {
        $this->authorizedPaymentMethods = $authorizedPaymentMethods;

        return $this;
    }

    /**
     * Get cartProducts
     *
     * @return Collection|CartProduct[]
     */
    public function getCartProducts()
    {
        return $this->cartProducts;
    }

    /**
     * Get corporates
     *
     * @return Collection|Corporate[]
     */
    public function getCorporates(): Collection
    {
        return $this->corporates;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Agency
     */
    public function setCreatedAt(\DateTime $createdAt = null): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return string
     */
    public function getCrmServiceClient(): ?string
    {
        return $this->crmServiceClient;
    }

    /**
     * @param string $crmServiceClient
     *
     * @return Agency
     */
    public function setCrmServiceClient(?string $crmServiceClient): self
    {
        $this->crmServiceClient = $crmServiceClient;

        return $this;
    }

    /**
     * @return string
     */
    public function getCrmSite(): ?string
    {
        return $this->crmSite;
    }

    /**
     * @param string $crmSite
     *
     * @return Agency
     */
    public function setCrmSite(?string $crmSite): Agency
    {
        $this->crmSite = $crmSite;

        return $this;
    }

    /**
     * @return string
     */
    public function getCrmTeam(): ?string
    {
        return $this->crmTeam;
    }

    /**
     * @param string $crmTeam
     *
     * @return Agency
     */
    public function setCrmTeam(?string $crmTeam): Agency
    {
        $this->crmTeam = $crmTeam;

        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     *
     * @return Agency
     */
    public function setCurrency(?string $currency): Agency
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * @return string
     */
    public function getDataAreaId(): ?string
    {
        return $this->dataAreaId;
    }

    /**
     * @param string $dataAreaId
     *
     * @return Agency
     */
    public function setDataAreaId(?string $dataAreaId): Agency
    {
        $this->dataAreaId = $dataAreaId;

        return $this;
    }

    /**
     * @return string
     */
    public function getDefaultLanguage(): ?string
    {
        return $this->defaultLanguage;
    }

    /**
     * @param string $defaultLanguage
     *
     * @return Agency
     */
    public function setDefaultLanguage(?string $defaultLanguage): self
    {
        $this->defaultLanguage = $defaultLanguage;

        return $this;
    }

    /**
     * @return array
     */
    public function getDeliveryMode(): ?array
    {
        return $this->deliveryMode;
    }

    /**
     * @param array $modes
     *
     * @return Agency
     */
    public function setDeliveryMode(array $modes): self
    {
        $this->deliveryMode = $modes;

        return $this;
    }

    /**
     * @param string $mode
     *
     * @return Agency
     */
    public function addDeliveryMode(string $mode): self
    {
        if (!\in_array($mode, $this->deliveryMode, true)) {
            $this->deliveryMode[] = $mode;
        }

        return $this;
    }

    /**
     * @return null|string
     */
    public function getDeliveryModeString(): ?string
    {
        return implode(';', $this->deliveryMode);
    }

    /**
     * Get domainWhitelist
     *
     * @return string[]
     */
    public function getDomainWhitelist(): ?array
    {
        return $this->domainWhitelist;
    }

    /**
     * Set domainWhitelist
     *
     * @param string[] $domainWhitelist
     *
     * @return Agency
     */
    public function setDomainWhitelist(?array $domainWhitelist): self
    {
        $this->domainWhitelist = $domainWhitelist;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     *
     * @return Agency
     */
    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return Collection|LinkFileAgency[]
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Get headerImg
     *
     * @return Media
     */
    public function getHeaderImg(): ?Media
    {
        return $this->headerImg;
    }

    /**
     * Set headerImg
     *
     * @param Media $headerImg
     *
     * @return Agency
     */
    public function setHeaderImg(Media $headerImg = null): self
    {
        $this->headerImg = $headerImg;

        return $this;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function getIdAnalytics(): ?string
    {
        return $this->idAnalytics;
    }

    /**
     * @return Media
     */
    public function getImageBackground(): ?Media
    {
        return $this->imageBackground;
    }

    /**
     * @param Media $imageBackground
     *
     * @return Agency
     */
    public function setImageBackground(Media $imageBackground): self
    {
        $this->imageBackground = $imageBackground;

        return $this;
    }

    /**
     * @return Media
     */
    public function getImagePresentation(): ?Media
    {
        return $this->imagePresentation;
    }

    /**
     * @param Media $imagePresentation
     *
     * @return Agency
     */
    public function setImagePresentation(Media $imagePresentation): self
    {
        $this->imagePresentation = $imagePresentation;

        return $this;
    }

    /**
     * @return string
     */
    public function getInformation(): ?string
    {
        return $this->information;
    }

    /**
     * @param string $information
     *
     * @return Agency
     */
    public function setInformation(?string $information): Agency
    {
        $this->information = $information;

        return $this;
    }

    /**
     * @return array
     */
    public function getLanguages(): ?array
    {
        return array_map(function ($lang) {
            return strpos($lang, 'en-') === false ? $lang : 'en';
        }, $this->languages);
    }

//    /**
//     * Get style
//     *
//     * @return Style
//     */
//    public function getStyle()
//    {
//        return $this->style;
//    }

//    /**
//     * Set style
//     *
//     * @param Style $style
//     *
//     * @return Agency
//     */
//    public function setStyle(Style $style = null)
//    {
//        $this->style = $style;
//
//        return $this;
//    }

    /**
     * @param array $languages
     *
     * @return Agency
     */
    public function setLanguages(?array $languages): Agency
    {
        $this->languages = $languages;

        return $this;
    }

    /**
     * @return string
     */
    public function getLanguagesString(): ?string
    {
        return implode(';', $this->languages);
    }

    /**
     * @Serializer\VirtualProperty()
     *
     * @return array
     */
    public function getLastNews(): array
    {
        $publishedNews = $this->getPosts()->filter(
            function ($data) {
                /** @var LinkPostAgency $data */

                return
                    $data->getPublicationDate() < new \DateTime()
                    && $data->getState()->get() === LinkPostAgencyStateEnum::PUBLISHED
                    && $data->getPost()
                    && $data->getPost()->getState()
                    && $data->getPost()->getState()->get() === PostStateEnum::VALIDATE;
            }
        );

        return \array_slice($publishedNews->toArray(), 0, 20);
    }

    /**
     * @Serializer\VirtualProperty()
     *
     * @return array
     */
    public function getAllFiles(): array
    {
        $files = $this->getFiles()->filter(
            function ($data) {
                return ($data->getVisibility() === true) && $data->getFile();
            }
        );

        return $files->getValues();
    }

    /**
     * Get linkAccounts
     *
     * @return Collection|LinkAccountAgency[]
     */
    public function getLinkAccounts(): Collection
    {
        return $this->linkAccounts;
    }

    /**
     * Get linkPlugins
     *
     * @return Collection|LinkAgencyPlugin[]
     */
    public function getLinkPlugins(): Collection
    {
        return $this->linkPlugins;
    }

    /**
     * Get linkSocialGroups
     *
     * @return Collection|LinkAgencySocialGroup[]
     */
    public function getLinkSocialGroups()
    {
        return $this->linkSocialGroups;
    }

    /**
     * Get logo
     *
     * @return Media
     */
    public function getLogo(): ?Media
    {
        return $this->logo;
    }

    /**
     * Set logo
     *
     * @param Media $logo
     *
     * @return Agency
     */
    public function setLogo(?Media $logo): Agency
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * @return Media
     */
    public function getHeaderImgDe(): ?Media
    {
        return $this->headerImgDe;
    }

    /**
     * @return Media
     */
    public function getHeaderImgEn(): ?Media
    {
        return $this->headerImgEn;
    }

    /**
     * Get modules
     *
     * @return string[]
     */
    public function getModules(): array
    {
        return $this->modules;
    }

    /**
     * Set modules
     *
     * @param string[] $modules
     *
     * @return Agency
     */
    public function setModules(array $modules): self
    {
        $this->modules = $modules;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Agency
     */
    public function setName(?string $name = null): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Agency
     */
    public function setPhone(?string $phone): Agency
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get posts
     *
     * @return Collection|LinkPostAgency[]
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    /**
     * Get retailChannelId
     *
     * @return string
     */
    public function getRetailChannelId(): ?string
    {
        return $this->retailChannelId;
    }

    /**
     * Set retailChannelId
     *
     * @param string $retailChannelId
     *
     * @return Agency
     */
    public function setRetailChannelId(?string $retailChannelId = null): self
    {
        $this->retailChannelId = $retailChannelId;

        return $this;
    }

    /**
     * @return string
     */
    public function getSchedule(): ?string
    {
        return $this->schedule;
    }

    /**
     * @param string $schedule
     *
     * @return Agency
     */
    public function setSchedule(?string $schedule): Agency
    {
        $this->schedule = $schedule;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Agency
     */
    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * Remove corporate
     *
     * @param string $status
     *
     * @return Agency
     */
    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt(): ?\Datetime
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Agency
     */
    public function setUpdatedAt(\DateTime $updatedAt = null): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get wishlist
     *
     * @return Collection|Wishlist[]
     */
    public function getWishlists(): Collection
    {
        return $this->wishlists;
    }

    /**
     * Do hasModule
     *
     * @param string $module
     *
     * @return bool
     */
    public function hasModule(string $module): bool
    {
        return \in_array($module, $this->modules, true);
    }

    /**
     * Get isFront
     *
     * @return bool
     */
    public function isEnabledFront(): bool
    {
        return $this->enabledFront;
    }

    /**
     * Get enabledFront
     *
     * @return bool
     */
    public function getEnabledFront(): bool
    {
        return $this->enabledFront;
    }

    /**
     * @param bool $enabledFront
     *
     * @return Agency
     */
    public function setEnabledFront(bool $enabledFront): self
    {
        $this->enabledFront = $enabledFront;

        return $this;
    }

    /**
     * Remove cartProduct
     *
     * @param CartProduct $cartProduct
     *
     * @return Agency
     */
    public function removeCartProduct(CartProduct $cartProduct): self
    {
        $this->cartProducts->removeElement($cartProduct);
        $cartProduct->setAgency(null);

        return $this;
    }

    /**
     * Remove corporate
     *
     * @param Corporate $corporate
     *
     * @return Agency
     */
    public function removeCorporate(Corporate $corporate): self
    {
        $this->corporates->removeElement($corporate);

        return $this;
    }

    /**
     * Remove linkAccount
     *
     * @param LinkAccountAgency $linkAccount
     *
     * @return Agency
     */
    public function removeLinkAccount(LinkAccountAgency $linkAccount): self
    {
        $this->linkAccounts->removeElement($linkAccount);
        $linkAccount->setAgency(null);

        return $this;
    }

    /**
     * Remove linkPlugin
     *
     * @param LinkAgencyPlugin $linkPlugin
     *
     * @return Agency
     */
    public function removeLinkPlugin(LinkAgencyPlugin $linkPlugin): self
    {
        $this->linkPlugins->removeElement($linkPlugin);
        $linkPlugin->setAgency(null);

        return $this;
    }

    /**
     * Remove linkSocialGroup
     *
     * @param LinkAgencySocialGroup $linkSocialGroup
     *
     * @return Agency
     */
    public function removeLinkSocialGroup(LinkAgencySocialGroup $linkSocialGroup): self
    {
        $this->linkSocialGroups->removeElement($linkSocialGroup);
        $linkSocialGroup->setAgency(null);

        return $this;
    }

    /**
     * Remove post
     *
     * @param LinkPostAgency $post
     *
     * @return Agency
     */
    public function removePost(LinkPostAgency $post): self
    {
        $this->posts->removeElement($post);
        $post->setAgency(null);

        return $this;
    }

    /**
     * Remove wishlist
     *
     * @param Wishlist $wishlist
     *
     * @return Agency
     */
    public function removeWishlist(Wishlist $wishlist): self
    {
        $this->wishlists->removeElement($wishlist);
        $wishlist->setAgency(null);

        return $this;
    }

    /**
     * @param string $deliveryMode
     *
     * @return Agency
     */
    public function setDeliveryModeString(?string $deliveryMode): Agency
    {
        $this->deliveryMode = explode(';', $deliveryMode);

        return $this;
    }

    /**
     * @param Collection|LinkFileAgency[] $files
     *
     * @return Agency
     */
    public function setFiles($files): self
    {
        $this->files = $files;

        return $this;
    }

    /**
     * @param string $idAnalytics
     *
     * @return Agency
     */
    public function setIdAnalytics(?string $idAnalytics): self
    {
        $this->idAnalytics = $idAnalytics;

        return $this;
    }

    /**
     * @param null|string $languages
     *
     * @return Agency
     */
    public function setLanguagesString(?string $languages): self
    {
        if ($languages) {
            $languages = $languages === 'en-US' ? 'en' : $languages;
            $this->languages = explode(';', $languages);
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isAllowAdherent(): bool
    {
        return $this->allowAdherent;
    }

    /**
     * @param bool $allowAdherent
     *
     * @return Agency
     */
    public function setAllowAdherent(bool $allowAdherent): Agency
    {
        $this->allowAdherent = $allowAdherent;

        return $this;
    }

    /**
     * @return string
     */
    public function getQuickInfo(): ?string
    {
        return $this->quickInfo;
    }

    /**
     * @param string $quickInfo
     *
     * @return Agency
     */
    public function setQuickInfo(?string $quickInfo): Agency
    {
        $this->quickInfo = $quickInfo;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAllowRegistration(): bool
    {
        return $this->allowRegistration;
    }

    /**
     * @param bool $allowRegistration
     *
     * @return Agency
     */
    public function setAllowRegistration(bool $allowRegistration): Agency
    {
        $this->allowRegistration = $allowRegistration;

        return $this;
    }

    /**
     * @return Cgv|null
     */
    public function getCgv(): ?Cgv
    {
        return $this->cgv;
    }

    /**
     * @param mixed $cgv
     *
     * @return Agency
     */
    public function setCgv(Cgv $cgv): self
    {
        $this->cgv = $cgv;

        return $this;
    }

    /**
     * @param Media $headerImgDe
     *
     * @return Agency
     */
    public function setHeaderImgDe(Media $headerImgDe): Agency
    {
        $this->headerImgDe = $headerImgDe;

        return $this;
    }

    /**
     * @param Media $headerImgEn
     *
     * @return Agency
     */
    public function setHeaderImgEn(Media $headerImgEn): Agency
    {
        $this->headerImgEn = $headerImgEn;

        return $this;
    }

    /**
     * @return Collection|CheckoutInfoNeeded[]
     */
    public function getCheckoutInfos(): Collection
    {
        return $this->checkoutInfos;
    }

    /**
     * @param CheckoutInfoNeeded $checkoutInfoNeeded
     *
     * @return Agency
     */
    public function addCheckoutInfo(CheckoutInfoNeeded $checkoutInfoNeeded): Agency
    {
        if (!\in_array($checkoutInfoNeeded, $this->checkoutInfos, true)) {
            $this->checkoutInfos[] = $checkoutInfoNeeded;

            $checkoutInfoNeeded->setAgency($this);
        }

        return $this;
    }

    /**
     * @param CheckoutInfoNeeded $checkoutInfoNeeded
     *
     * @return Agency
     */
    public function removeCheckoutInfo(CheckoutInfoNeeded $checkoutInfoNeeded): Agency
    {
        $this->checkoutInfos->removeElement($checkoutInfoNeeded);
        $checkoutInfoNeeded->setAgency();

        return $this;
    }

    /**
     * @return Collection|DeliveryDelay[]
     */
    public function getDeliveryDelays(): Collection
    {
        return $this->deliveryDelays;
    }

    /**
     * @param DeliveryDelay $deliveryDelays
     *
     * @return Agency
     */
    public function addDeliveryDelay(DeliveryDelay $deliveryDelays): self
    {
        if (!\in_array($deliveryDelays, $this->deliveryDelays, true)) {
            $this->deliveryDelays[] = $deliveryDelays;
            $deliveryDelays->setAgency($this);
        }

        return $this;
    }

    /**
     * @param DeliveryDelay $deliveryDelay
     *
     * @return Agency
     */
    public function removeDeliveryDelay(DeliveryDelay $deliveryDelay): self
    {
        $this->deliveryDelays->removeElement($deliveryDelay);
        $deliveryDelay->setAgency();

        return $this;
    }

    /**
     * @return bool
     */
    public function isDigital(): bool
    {
        return $this->isDigital;
    }

    /**
     * @param bool $isDigital
     *
     * @return Agency
     */
    public function setIsDigital(bool $isDigital): self
    {
        $this->isDigital = $isDigital;

        return $this;
    }

    /**
     * @return bool
     * @todo export in a dedicated yml config data area ids who display price with excluding taxes
     *
     * @throws \InvalidArgumentException
     */
    public function getDisplayPriceExclTaxes(): bool
    {
        if (!$agencyDataAreaId = $this->getDataAreaId()) {
            throw new \InvalidArgumentException('Agendy Data Area Id is missing');
        }

        return ($agencyDataAreaId === 'ECA') ? true : false;
    }
}
