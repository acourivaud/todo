<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 24/02/2017
 * Time: 14:28
 */

namespace Todotoday\CoreBundle\Entity\Api\Microsoft;

use Actiane\ApiConnectorBundle\Lib\EntityProxyTrait;
use Actiane\ApiConnectorBundle\Lib\Mapping as APIConnector;

/**
 * Class AgencyAddressBook
 *
 * @APIConnector\Entity(path="RetailStoreAddressBooks", repositoryId="todotoday.core.repository.api.agency_address_book")
 *
 * @category   Todo-Todev
 * @package    Todotoday\CoreBundle
 * @subpackage Todotoday\CoreBundle\Entity\Api\Microsoft
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class AgencyAddressBook
{
    use EntityProxyTrait;

    /**
     * @var string
     *
     * @APIConnector\Column(name="RetailChannelId", type="string")
     */
    protected $retailChannelId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="AddressBookType", type="string")
     */
    protected $addressBookType;

    /**
     * @var string
     *
     * @APIConnector\Column(name="AddressBookName", type="string")
     */
    protected $addressBookName;

    /**
     * Get addressBookName
     *
     * @return string
     */
    public function getAddressBookName(): ?string
    {
        return $this->addressBookName;
    }

    /**
     * Set addressBookName
     *
     * @param string $addressBookName
     *
     * @return AgencyAddressBook
     */
    public function setAddressBookName(string $addressBookName): AgencyAddressBook
    {
        return $this->setTrigger('addressBookName', $addressBookName);
    }

    /**
     * Get addressBookType
     *
     * @return string
     */
    public function getAddressBookType(): ?string
    {
        return $this->addressBookType;
    }

    /**
     * Set addressBookType
     *
     * @param string $addressBookType
     *
     * @return AgencyAddressBook
     */
    public function setAddressBookType(string $addressBookType): AgencyAddressBook
    {
        return $this->setTrigger('addressBookType', $addressBookType);
    }

    /**
     * Get retailChannelId
     *
     * @return string
     */
    public function getRetailChannelId(): ?string
    {
        return $this->retailChannelId;
    }

    /**
     * Set retailChannelId
     *
     * @param string $retailChannelId
     *
     * @return AgencyAddressBook
     */
    public function setRetailChannelId(string $retailChannelId): AgencyAddressBook
    {
        return $this->setTrigger('retailChannelId', $retailChannelId);
    }
}
