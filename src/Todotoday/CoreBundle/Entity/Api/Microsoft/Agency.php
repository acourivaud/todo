<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 23/02/2017
 * Time: 17:28
 */

namespace Todotoday\CoreBundle\Entity\Api\Microsoft;

use Actiane\ApiConnectorBundle\Lib\EntityProxyTrait;
use Actiane\ApiConnectorBundle\Lib\Mapping as APIConnector;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class Agency
 *
 * @SuppressWarnings(PHPMD)
 * @APIConnector\Entity(
 *     path="TDTDRetailStores",
 *     repositoryId="todotoday.core.repository.api.agency",
 *     doctrineEntityClass="Todotoday\CoreBundle\Entity\Agency",
 *     doctrineEntityRepo="todotoday_core.repository.agency_repository"
 * )
 *
 * @category   Todo-Todev
 * @package    Todotoday\CoreBundle
 * @subpackage Todotoday\CoreBundle\Entity\Api\Microsoft
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 * @Serializer\ExclusionPolicy("all")
 */
class Agency
{
    use EntityProxyTrait;

    /**
     * @var string
     *
     * @APIConnector\Column(name="OperatingUnitNumber", type="string")
     */
    protected $operatingUnitNumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="SQLServerName", type="string")
     */
    protected $sqlServerName;

    /**
     * @var int
     *
     * @APIConnector\Column(name="MaximumTextLengthOnReceipt", type="int")
     */
    protected $maximumTextLengthOnReceipt;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DefaultCustomerLegalEntity", type="string")
     */
    protected $defaultCustomerLegalEntity;

    /**
     * @var string
     *
     * @APIConnector\Column(name="TermsOfPayment", type="string")
     */
    protected $termsOfPayment;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DisplayTaxPerTaxComponent", type="string")
     */
    protected $displayTaxPerTaxComponent;

    /**
     * @var string
     *
     * @APIConnector\Column(name="InventoryLookup", type="string")
     */
    protected $inventoryLookup;

    /**
     * @var string
     *
     * @APIConnector\Column(name="WarehouseIdForCustomerOrder", type="string")
     */
    protected $warehouseIdForCustomerOrder;

    /**
     * @var string
     *
     * @APIConnector\Column(name="StatementMethod", type="string")
     */
    protected $statementMethod;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DefaultCustomerAccount", type="string")
     */
    protected $defaultCustomerAccount;

    /**
     * @var string
     *
     * @APIConnector\Column(name="MaxRoundingTaxAmount", type="string")
     */
    protected $maxRoundingTaxAmount;

    /**
     * @var int
     *
     * @APIConnector\Column(name="StoreArea", type="int")
     */
    protected $storeArea;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ProductCategoryHierarchyName", type="string")
     */
    protected $productCategoryHierarchyName;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ServiceChargePrompt", type="string")
     */
    protected $serviceChargePrompt;

    /**
     * @var string
     *
     * @APIConnector\Column(name="GeneratesItemLabels", type="string")
     */
    protected $generatesItemLabels;

    /**
     * @var string
     *
     * @APIConnector\Column(name="OneStatementPerDay", type="string")
     */
    protected $oneStatementPerDay;

    /**
     * @var string
     *
     * @APIConnector\Column(name="PaymentMethodToRemoveOrAdd", type="string")
     */
    protected $paymentMethodToRemoveOrAdd;

    /**
     * @var string
     *
     * @APIConnector\Column(name="LiveDatabaseConnectionProfileName", type="string")
     */
    protected $liveDatabaseProfileName;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="LastModification", type="datetime")
     */
    protected $lastModification;

    /**
     * @var string
     *
     * @APIConnector\Column(name="LayoutId", type="string")
     */
    protected $layoutId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ElectronicFundsTransferStoreNumber", type="string")
     */
    protected $fundsTransferStoreNumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="StatementPostAsBusinessDay", type="string")
     */
    protected $statementPostAsBusinessDay;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @APIConnector\Column(name="RetailChannelId", type="string", doctrineAttribute="retailChannelId")
     */
    protected $retailChannelId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CreateLabelsForZeroPrice", type="string")
     */
    protected $createLabelsForZeroPrice;

    /**
     * @var string
     *
     * @APIConnector\Column(name="TransactionServiceProfile", type="string")
     */
    protected $transactionServiceProfile;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DatabaseName", type="string")
     */
    protected $databaseName;

    /**
     * @var string
     *
     * @APIConnector\Column(name="IdActiane", type="string")
     */
    protected $idActiane;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DefaultDimensionDisplayValue", type="string")
     */
    protected $defaultDimensionDisplayValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="UseDefaultCustomerAccount", type="string")
     */
    protected $useDefaultCustomerAccount;

    /**
     * @var string
     *
     * @APIConnector\Column(name="EventNotificationProfileId", type="string")
     */
    protected $eventNotificationProfileId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DlvModes", type="string", doctrineAttribute="DeliveryModeString")
     * @Serializer\Expose()
     */
    protected $dlvModes;

    /**
     * @var int
     *
     * @APIConnector\Column(name="NumberOfTopOrBottomLines", type="int")
     */
    protected $numberOfTopOrBottomLines;

    /**
     * @var string
     *
     * @APIConnector\Column(name="crm_id_serviceclient", type="string", doctrineAttribute="crmServiceClient")
     * @Serializer\Expose()
     */
    protected $crmIdServiceclient;

    /**
     * @var string
     *
     * @APIConnector\Column(name="WarehouseLegalEntity", type="string", doctrineAttribute="dataAreaId")
     */
    protected $warehouseLegalEntity;

    /**
     * @var int
     *
     * @APIConnector\Column(name="EndOfBusinessDay", type="int")
     */
    protected $endOfBusinessDay;

    /**
     * @var string
     *
     * @APIConnector\Column(name="RoundingTaxAccount", type="string")
     */
    protected $roundingTaxAccount;

    /**
     * @var string
     *
     * @APIConnector\Column(name="TaxGroupCode", type="string")
     */
    protected $taxGroupCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="GeneratesShelfLabels", type="string")
     */
    protected $generatesShelfLabels;

    /**
     * @var string
     *
     * @APIConnector\Column(name="UseCustomerBasedTax", type="string")
     */
    protected $useCustomerBasedTax;

    /**
     * @var int
     *
     * @APIConnector\Column(name="MaxRoundingAmount", type="int")
     */
    protected $maxRoundingAmount;

    /**
     * @var int
     *
     * @APIConnector\Column(name="ServiceChargePercentage", type="int")
     */
    protected $serviceChargePercentage;

    /**
     * @var string
     *
     * @APIConnector\Column(name="OperatingUnitPartyNumber", type="string")
     */
    protected $operatingUnitPartyNumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="OfflineProfileName", type="string")
     */
    protected $offlineProfileName;

    /**
     * @var string
     *
     * @APIConnector\Column(name="PriceIncludesSalesTax", type="string")
     */
    protected $priceIncludesSalesTax;

    /**
     * @var string
     *
     * @APIConnector\Column(name="Phone", type="string", doctrineAttribute="phone")
     */
    protected $phone;

    /**
     * @var string
     *
     * @APIConnector\Column(name="PaymentMethodName", type="string")
     */
    protected $paymentMethodName;

    /**
     * @var string
     *
     * @APIConnector\Column(name="FunctionalityProfile", type="string")
     */
    protected $functionalityProfile;

    /**
     * @var int
     *
     * @APIConnector\Column(name="OpenTo", type="int")
     */
    protected $openTo;

    /**
     * @var string
     *
     * @APIConnector\Column(name="TaxOverrideGroupCode", type="string")
     */
    protected $taxOverrideGroupCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ChannelTimeZoneInfoId", type="string")
     */
    protected $channelTimeZoneInfoId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="TaxGroupLegalEntity", type="string")
     */
    protected $taxGroupLegalEntity;

    /**
     * @var string
     *
     * @APIConnector\Column(name="WarehouseId", type="string")
     */
    protected $warehouseId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="TaxOverrideGroupCodeLegalEntity", type="string")
     */
    protected $taxOverrideGroupCodeEntity;

    /**
     * @var string
     *
     * @APIConnector\Column(name="RoundingAccountLedgerDimensionDisplayValue", type="string")
     */
    protected $roundingAccountLedgerValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="TaxIdentificationNumber", type="string")
     */
    protected $taxIdentificationNumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ProductNumberOnReceipt", type="string")
     */
    protected $productNumberOnReceipt;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ChannelTimeZone", type="string")
     */
    protected $channelTimeZone;

    /**
     * @var string
     *
     * @APIConnector\Column(name="PurchaseOrderItemFilter", type="string")
     */
    protected $purchaseOrderItemFilter;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ClosingMethod", type="string")
     */
    protected $closingMethod;

    /**
     * @var string
     *
     * @APIConnector\Column(name="crm_id_site", type="string", doctrineAttribute="crmSite")
     */
    protected $crmIdSite;

    /**
     * @var int
     *
     * @APIConnector\Column(name="MaxTransactionDifferenceAmount", type="int")
     */
    protected $maxTransactionDifferenceAmount;

    /**
     * @var string
     *
     * @APIConnector\Column(name="HideTrainingMode", type="string")
     */
    protected $hideTrainingMode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ChannelProfileName", type="string")
     */
    protected $channelProfileName;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CultureName", type="string")
     */
    protected $cultureName;

    /**
     * @var int
     *
     * @APIConnector\Column(name="MaxShiftDifferenceAmount", type="int")
     */
    protected $maxShiftDifferenceAmount;

    /**
     * @var string
     *
     * @APIConnector\Column(name="TenderDeclarationCalculation", type="string")
     */
    protected $tenderDeclarationCalculation;

    /**
     * @var string
     *
     * @APIConnector\Column(name="Currency", type="string", doctrineAttribute="currency")
     */
    protected $currency;

    /**
     * @var string
     *
     * @APIConnector\Column(name="crm_id_team", type="string",doctrineAttribute="crmTeam")
     * @Serializer\Expose()
     */
    protected $crmIdTeam;

    /**
     * @var int
     *
     * @APIConnector\Column(name="MaximumPostingDifference", type="int")
     */
    protected $maximumPostingDifference;

    /**
     * @var string
     *
     * @APIConnector\Column(name="StoreName", type="string", doctrineAttribute="name")
     * @Serializer\Expose()
     */
    protected $storeName;

    /**
     * @var string
     *
     * @APIConnector\Column(name="Languages", type="string", doctrineAttribute="languagesString")
     */
    protected $languages;

    /**
     * @var string
     *
     * @APIConnector\Column(name="SeparateStatementPerStaffTerminal", type="string")
     */
    protected $statementPerStaffTerminal;

    /**
     * @var string
     *
     * @APIConnector\Column(name="UseDestinationBasedTax", type="string")
     */
    protected $useDestinationBasedTax;

    /**
     * @var string
     *
     * @APIConnector\Column(name="StoreNumber", type="string")
     */
    protected $storeNumber;

    /**
     * @var int
     *
     * @APIConnector\Column(name="OpenFrom", type="int")
     */
    protected $openFrom;

    /**
     * @var string
     *
     * @APIConnector\Column(name="Email", type="string", doctrineAttribute="email")
     */
    protected $email;

    /**
     * @var string
     *
     * @APIConnector\Column(name="Address", type="string", doctrineAttribute="address")
     */
    protected $address;

    /**
     * @var string
     *
     * @APIConnector\Column(name="IsDigital", type="string")
     */
    protected $isDigital;

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Agency
     */
    public function setAddress(?string $address): self
    {
        return $this->setTrigger('address', $address);
    }

    /**
     * Get channelProfileName
     *
     * @return string
     */
    public function getChannelProfileName(): ?string
    {
        return $this->channelProfileName;
    }

    /**
     * Set channelProfileName
     *
     * @param string $channelProfileName
     *
     * @return Agency
     */
    public function setChannelProfileName(string $channelProfileName): Agency
    {
        return $this->setTrigger('channelProfileName', $channelProfileName);
    }

    /**
     * Get channelTimeZone
     *
     * @return string
     */
    public function getChannelTimeZone(): ?string
    {
        return $this->channelTimeZone;
    }

    /**
     * Set channelTimeZone
     *
     * @param string $channelTimeZone
     *
     * @return Agency
     */
    public function setChannelTimeZone(string $channelTimeZone): Agency
    {
        return $this->setTrigger('channelTimeZone', $channelTimeZone);
    }

    /**
     * Get channelTimeZoneInfoId
     *
     * @return string
     */
    public function getChannelTimeZoneInfoId(): ?string
    {
        return $this->channelTimeZoneInfoId;
    }

    /**
     * Set channelTimeZoneInfoId
     *
     * @param string $channelTimeZoneInfoId
     *
     * @return Agency
     */
    public function setChannelTimeZoneInfoId(string $channelTimeZoneInfoId): Agency
    {
        return $this->setTrigger('channelTimeZoneInfoId', $channelTimeZoneInfoId);
    }

    /**
     * Get closingMethod
     *
     * @return string
     */
    public function getClosingMethod(): ?string
    {
        return $this->closingMethod;
    }

    /**
     * Set closingMethod
     *
     * @param string $closingMethod
     *
     * @return Agency
     */
    public function setClosingMethod(string $closingMethod): Agency
    {
        return $this->setTrigger('closingMethod', $closingMethod);
    }

    /**
     * Get createLabelsForZeroPrice
     *
     * @return string
     */
    public function getCreateLabelsForZeroPrice(): ?string
    {
        return $this->createLabelsForZeroPrice;
    }

    /**
     * Set createLabelsForZeroPrice
     *
     * @param string $createLabelsForZeroPrice
     *
     * @return Agency
     */
    public function setCreateLabelsForZeroPrice(string $createLabelsForZeroPrice): Agency
    {
        return $this->setTrigger('createLabelsForZeroPrice', $createLabelsForZeroPrice);
    }

    /**
     * @return string
     */
    public function getCrmIdServiceclient(): ?string
    {
        return $this->crmIdServiceclient;
    }

    /**
     * @param string $crmIdServiceclient
     *
     * @return Agency
     */
    public function setCrmIdServiceclient(?string $crmIdServiceclient): Agency
    {
        return $this->setTrigger('crmIdServiceclient', $crmIdServiceclient);
    }

    /**
     * @return string
     */
    public function getCrmIdSite(): ?string
    {
        return $this->crmIdSite;
    }

    /**
     * @param string $crmIdSite
     *
     * @return Agency
     */
    public function setCrmIdSite(string $crmIdSite): Agency
    {
        return $this->setTrigger('crmIdSite', $crmIdSite);
    }

    /**
     * @return string
     */
    public function getCrmIdTeam(): ?string
    {
        return $this->crmIdTeam;
    }

    /**
     * @param string $crmIdTeam
     *
     * @return Agency
     */
    public function setCrmIdTeam(?string $crmIdTeam): Agency
    {
        return $this->setTrigger('crmIdTeam', $crmIdTeam);
    }

    /**
     * Get cultureName
     *
     * @return string
     */
    public function getCultureName(): ?string
    {
        return $this->cultureName;
    }

    /**
     * Set cultureName
     *
     * @param string $cultureName
     *
     * @return Agency
     */
    public function setCultureName(string $cultureName): Agency
    {
        return $this->setTrigger('cultureName', $cultureName);
    }

    /**
     * Get currency
     *
     * @return string
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * Set currency
     *
     * @param string $currency
     *
     * @return Agency
     */
    public function setCurrency(string $currency): Agency
    {
        return $this->setTrigger('currency', $currency);
    }

    /**
     * Get databaseName
     *
     * @return string
     */
    public function getDatabaseName(): ?string
    {
        return $this->databaseName;
    }

    /**
     * Set databaseName
     *
     * @param string $databaseName
     *
     * @return Agency
     */
    public function setDatabaseName(string $databaseName): Agency
    {
        return $this->setTrigger('databaseName', $databaseName);
    }

    /**
     * Get defaultCustomerAccount
     *
     * @return string
     */
    public function getDefaultCustomerAccount(): ?string
    {
        return $this->defaultCustomerAccount;
    }

    /**
     * Set defaultCustomerAccount
     *
     * @param string $defaultCustomerAccount
     *
     * @return Agency
     */
    public function setDefaultCustomerAccount(string $defaultCustomerAccount): Agency
    {
        return $this->setTrigger('defaultCustomerAccount', $defaultCustomerAccount);
    }

    /**
     * Get defaultCustomerLegalEntity
     *
     * @return string
     */
    public function getDefaultCustomerLegalEntity(): ?string
    {
        return $this->defaultCustomerLegalEntity;
    }

    /**
     * Set defaultCustomerLegalEntity
     *
     * @param string $defaultCustomerLegalEntity
     *
     * @return Agency
     */
    public function setDefaultCustomerLegalEntity(string $defaultCustomerLegalEntity): Agency
    {
        return $this->setTrigger('defaultCustomerLegalEntity', $defaultCustomerLegalEntity);
    }

    /**
     * Get defaultDimensionDisplayValue
     *
     * @return string
     */
    public function getDefaultDimensionDisplayValue(): ?string
    {
        return $this->defaultDimensionDisplayValue;
    }

    /**
     * Set defaultDimensionDisplayValue
     *
     * @param string $defaultDimensionDisplayValue
     *
     * @return Agency
     */
    public function setDefaultDimensionDisplayValue(string $defaultDimensionDisplayValue): Agency
    {
        return $this->setTrigger('defaultDimensionDisplayValue', $defaultDimensionDisplayValue);
    }

    /**
     * Get displayTaxPerTaxComponent
     *
     * @return string
     */
    public function getDisplayTaxPerTaxComponent(): ?string
    {
        return $this->displayTaxPerTaxComponent;
    }

    /**
     * Set displayTaxPerTaxComponent
     *
     * @param string $displayTaxPerTaxComponent
     *
     * @return Agency
     */
    public function setDisplayTaxPerTaxComponent(string $displayTaxPerTaxComponent): Agency
    {
        return $this->setTrigger('displayTaxPerTaxComponent', $displayTaxPerTaxComponent);
    }

    /**
     * @return string
     */
    public function getDlvModes(): ?string
    {
        return $this->dlvModes;
    }

    /**
     * @param string $dlvModes
     *
     * @return Agency
     */
    public function setDlvModes(?string $dlvModes): Agency
    {
        return $this->setTrigger('dlvModes', $dlvModes);
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Agency
     */
    public function setEmail(?string $email): self
    {
        return $this->setTrigger('email', $email);
    }

    /**
     * Get endOfBusinessDay
     *
     * @return int
     */
    public function getEndOfBusinessDay(): ?int
    {
        return $this->endOfBusinessDay;
    }

    /**
     * Set endOfBusinessDay
     *
     * @param int $endOfBusinessDay
     *
     * @return Agency
     */
    public function setEndOfBusinessDay(int $endOfBusinessDay): Agency
    {
        return $this->setTrigger('endOfBusinessDay', $endOfBusinessDay);
    }

    /**
     * Get eventNotificationProfileId
     *
     * @return string
     */
    public function getEventNotificationProfileId(): ?string
    {
        return $this->eventNotificationProfileId;
    }

    /**
     * Set eventNotificationProfileId
     *
     * @param string $eventNotificationProfileId
     *
     * @return Agency
     */
    public function setEventNotificationProfileId(string $eventNotificationProfileId): Agency
    {
        return $this->setTrigger('eventNotificationProfileId', $eventNotificationProfileId);
    }

    /**
     * Get functionalityProfile
     *
     * @return string
     */
    public function getFunctionalityProfile(): ?string
    {
        return $this->functionalityProfile;
    }

    /**
     * Set functionalityProfile
     *
     * @param string $functionalityProfile
     *
     * @return Agency
     */
    public function setFunctionalityProfile(string $functionalityProfile): Agency
    {
        return $this->setTrigger('functionalityProfile', $functionalityProfile);
    }

    /**
     * Get fundsTransferStoreNumber
     *
     * @return string
     */
    public function getFundsTransferStoreNumber(): ?string
    {
        return $this->fundsTransferStoreNumber;
    }

    /**
     * Set fundsTransferStoreNumber
     *
     * @param string $fundsTransferStoreNumber
     *
     * @return Agency
     */
    public function setFundsTransferStoreNumber(string $fundsTransferStoreNumber): Agency
    {
        return $this->setTrigger('fundsTransferStoreNumber', $fundsTransferStoreNumber);
    }

    /**
     * Get generatesItemLabels
     *
     * @return string
     */
    public function getGeneratesItemLabels(): ?string
    {
        return $this->generatesItemLabels;
    }

    /**
     * Set generatesItemLabels
     *
     * @param string $generatesItemLabels
     *
     * @return Agency
     */
    public function setGeneratesItemLabels(string $generatesItemLabels): Agency
    {
        return $this->setTrigger('generatesItemLabels', $generatesItemLabels);
    }

    /**
     * Get generatesShelfLabels
     *
     * @return string
     */
    public function getGeneratesShelfLabels(): ?string
    {
        return $this->generatesShelfLabels;
    }

    /**
     * Set generatesShelfLabels
     *
     * @param string $generatesShelfLabels
     *
     * @return Agency
     */
    public function setGeneratesShelfLabels(string $generatesShelfLabels): Agency
    {
        return $this->setTrigger('generatesShelfLabels', $generatesShelfLabels);
    }

    /**
     * Get hideTrainingMode
     *
     * @return string
     */
    public function getHideTrainingMode(): ?string
    {
        return $this->hideTrainingMode;
    }

    /**
     * Set hideTrainingMode
     *
     * @param string $hideTrainingMode
     *
     * @return Agency
     */
    public function setHideTrainingMode(string $hideTrainingMode): Agency
    {
        return $this->setTrigger('hideTrainingMode', $hideTrainingMode);
    }

    /**
     * Get idActiane
     *
     * @return string
     */
    public function getIdActiane(): ?string
    {
        return $this->idActiane;
    }

    /**
     * Set idActiane
     *
     * @param string|null $idActiane
     *
     * @return Agency
     */
    public function setIdActiane(?string $idActiane = null): Agency
    {
        return $this->setTrigger('idActiane', $idActiane);
    }

    /**
     * Get inventoryLookup
     *
     * @return string
     */
    public function getInventoryLookup(): ?string
    {
        return $this->inventoryLookup;
    }

    /**
     * Set inventoryLookup
     *
     * @param string $inventoryLookup
     *
     * @return Agency
     */
    public function setInventoryLookup(string $inventoryLookup): Agency
    {
        return $this->setTrigger('inventoryLookup', $inventoryLookup);
    }

    /**
     * Get languages
     *
     * @return string
     */
    public function getLanguages(): ?string
    {
        return $this->languages;
    }

    /**
     * Set languages
     *
     * @param string $languages
     *
     * @return Agency
     */
    public function setLanguages(string $languages): Agency
    {
        return $this->setTrigger('languages', $languages);
    }

    /**
     * Get lastModification
     *
     * @return \DateTime
     */
    public function getLastModification(): ?\DateTime
    {
        return $this->lastModification;
    }

    /**
     * Set lastModification
     *
     * @param \DateTime $lastModification
     *
     * @return Agency
     */
    public function setLastModification(\DateTime $lastModification): self
    {
        return $this->setTrigger('lastModification', $lastModification);
    }

    /**
     * Get layoutId
     *
     * @return string
     */
    public function getLayoutId(): ?string
    {
        return $this->layoutId;
    }

    /**
     * Set layoutId
     *
     * @param string $layoutId
     *
     * @return Agency
     */
    public function setLayoutId(string $layoutId): Agency
    {
        return $this->setTrigger('layoutId', $layoutId);
    }

    /**
     * Get liveDatabaseProfileName
     *
     * @return string
     */
    public function getLiveDatabaseProfileName(): ?string
    {
        return $this->liveDatabaseProfileName;
    }

    /**
     * Set liveDatabaseProfileName
     *
     * @param string $liveDatabaseProfileName
     *
     * @return Agency
     */
    public function setLiveDatabaseProfileName(string $liveDatabaseProfileName): Agency
    {
        return $this->setTrigger('liveDatabaseProfileName', $liveDatabaseProfileName);
    }

    /**
     * Get maxRoundingAmount
     *
     * @return int
     */
    public function getMaxRoundingAmount(): ?int
    {
        return $this->maxRoundingAmount;
    }

    /**
     * Set maxRoundingAmount
     *
     * @param int $maxRoundingAmount
     *
     * @return Agency
     */
    public function setMaxRoundingAmount(int $maxRoundingAmount): Agency
    {
        return $this->setTrigger('maxRoundingAmount', $maxRoundingAmount);
    }

    /**
     * Get maxRoundingTaxAmount
     *
     * @return string
     */
    public function getMaxRoundingTaxAmount(): ?string
    {
        return $this->maxRoundingTaxAmount;
    }

    /**
     * Set maxRoundingTaxAmount
     *
     * @param string $maxRoundingTaxAmount
     *
     * @return Agency
     */
    public function setMaxRoundingTaxAmount(string $maxRoundingTaxAmount): Agency
    {
        return $this->setTrigger('maxRoundingTaxAmount', $maxRoundingTaxAmount);
    }

    /**
     * Get maxShiftDifferenceAmount
     *
     * @return int
     */
    public function getMaxShiftDifferenceAmount(): ?int
    {
        return $this->maxShiftDifferenceAmount;
    }

    /**
     * Set maxShiftDifferenceAmount
     *
     * @param int $maxShiftDifferenceAmount
     *
     * @return Agency
     */
    public function setMaxShiftDifferenceAmount(int $maxShiftDifferenceAmount): Agency
    {
        return $this->setTrigger('maxShiftDifferenceAmount', $maxShiftDifferenceAmount);
    }

    /**
     * Get maxTransactionDifferenceAmount
     *
     * @return int
     */
    public function getMaxTransactionDifferenceAmount(): ?int
    {
        return $this->maxTransactionDifferenceAmount;
    }

    /**
     * Set maxTransactionDifferenceAmount
     *
     * @param int $maxTransactionDifferenceAmount
     *
     * @return Agency
     */
    public function setMaxTransactionDifferenceAmount(int $maxTransactionDifferenceAmount): Agency
    {
        return $this->setTrigger('maxTransactionDifferenceAmount', $maxTransactionDifferenceAmount);
    }

    /**
     * Get maximumPostingDifference
     *
     * @return int
     */
    public function getMaximumPostingDifference(): ?int
    {
        return $this->maximumPostingDifference;
    }

    /**
     * Set maximumPostingDifference
     *
     * @param int $maximumPostingDifference
     *
     * @return Agency
     */
    public function setMaximumPostingDifference(int $maximumPostingDifference): Agency
    {
        return $this->setTrigger('maximumPostingDifference', $maximumPostingDifference);
    }

    /**
     * Get maximumTextLengthOnReceipt
     *
     * @return int
     */
    public function getMaximumTextLengthOnReceipt(): ?int
    {
        return $this->maximumTextLengthOnReceipt;
    }

    /**
     * Set maximumTextLengthOnReceipt
     *
     * @param int $maximumTextLengthOnReceipt
     *
     * @return Agency
     */
    public function setMaximumTextLengthOnReceipt(int $maximumTextLengthOnReceipt): Agency
    {
        return $this->setTrigger('maximumTextLengthOnReceipt', $maximumTextLengthOnReceipt);
    }

    /**
     * Get numberOfTopOrBottomLines
     *
     * @return int
     */
    public function getNumberOfTopOrBottomLines(): ?int
    {
        return $this->numberOfTopOrBottomLines;
    }

    /**
     * Set numberOfTopOrBottomLines
     *
     * @param int $numberOfTopOrBottomLines
     *
     * @return Agency
     */
    public function setNumberOfTopOrBottomLines(int $numberOfTopOrBottomLines): Agency
    {
        return $this->setTrigger('numberOfTopOrBottomLines', $numberOfTopOrBottomLines);
    }

    /**
     * Get offlineProfileName
     *
     * @return string
     */
    public function getOfflineProfileName(): ?string
    {
        return $this->offlineProfileName;
    }

    /**
     * Set offlineProfileName
     *
     * @param string $offlineProfileName
     *
     * @return Agency
     */
    public function setOfflineProfileName(string $offlineProfileName): Agency
    {
        return $this->setTrigger('offlineProfileName', $offlineProfileName);
    }

    /**
     * Get oneStatementPerDay
     *
     * @return string
     */
    public function getOneStatementPerDay(): ?string
    {
        return $this->oneStatementPerDay;
    }

    /**
     * Set oneStatementPerDay
     *
     * @param string $oneStatementPerDay
     *
     * @return Agency
     */
    public function setOneStatementPerDay(string $oneStatementPerDay): Agency
    {
        return $this->setTrigger('oneStatementPerDay', $oneStatementPerDay);
    }

    /**
     * Get openFrom
     *
     * @return int
     */
    public function getOpenFrom(): ?int
    {
        return $this->openFrom;
    }

    /**
     * Set openFrom
     *
     * @param int $openFrom
     *
     * @return Agency
     */
    public function setOpenFrom(int $openFrom): Agency
    {
        return $this->setTrigger('openFrom', $openFrom);
    }

    /**
     * Get openTo
     *
     * @return int
     */
    public function getOpenTo(): ?int
    {
        return $this->openTo;
    }

    /**
     * Set openTo
     *
     * @param int $openTo
     *
     * @return Agency
     */
    public function setOpenTo(int $openTo): Agency
    {
        return $this->setTrigger('openTo', $openTo);
    }

    /**
     * Get operatingUnitNumber
     *
     * @return string
     */
    public function getOperatingUnitNumber(): ?string
    {
        return $this->operatingUnitNumber;
    }

    /**
     * Set operatingUnitNumber
     *
     * @param string $operatingUnitNumber
     *
     * @return Agency
     */
    public function setOperatingUnitNumber(string $operatingUnitNumber): Agency
    {
        return $this->setTrigger('operatingUnitNumber', $operatingUnitNumber);
    }

    /**
     * Get operatingUnitPartyNumber
     *
     * @return string
     */
    public function getOperatingUnitPartyNumber(): ?string
    {
        return $this->operatingUnitPartyNumber;
    }

    /**
     * Set operatingUnitPartyNumber
     *
     * @param string $operatingUnitPartyNumber
     *
     * @return Agency
     */
    public function setOperatingUnitPartyNumber(string $operatingUnitPartyNumber): Agency
    {
        return $this->setTrigger('operatingUnitPartyNumber', $operatingUnitPartyNumber);
    }

    /**
     * Get paymentMethodName
     *
     * @return string
     */
    public function getPaymentMethodName(): ?string
    {
        return $this->paymentMethodName;
    }

    /**
     * Set paymentMethodName
     *
     * @param string $paymentMethodName
     *
     * @return Agency
     */
    public function setPaymentMethodName(string $paymentMethodName): Agency
    {
        return $this->setTrigger('paymentMethodName', $paymentMethodName);
    }

    /**
     * Get paymentMethodToRemoveOrAdd
     *
     * @return string
     */
    public function getPaymentMethodToRemoveOrAdd(): ?string
    {
        return $this->paymentMethodToRemoveOrAdd;
    }

    /**
     * Set paymentMethodToRemoveOrAdd
     *
     * @param string $paymentMethodToRemoveOrAdd
     *
     * @return Agency
     */
    public function setPaymentMethodToRemoveOrAdd(string $paymentMethodToRemoveOrAdd): Agency
    {
        return $this->setTrigger('paymentMethodToRemoveOrAdd', $paymentMethodToRemoveOrAdd);
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Agency
     */
    public function setPhone(string $phone): Agency
    {
        return $this->setTrigger('phone', $phone);
    }

    /**
     * Get priceIncludesSalesTax
     *
     * @return string
     */
    public function getPriceIncludesSalesTax(): ?string
    {
        return $this->priceIncludesSalesTax;
    }

    /**
     * Set priceIncludesSalesTax
     *
     * @param string $priceIncludesSalesTax
     *
     * @return Agency
     */
    public function setPriceIncludesSalesTax(string $priceIncludesSalesTax): Agency
    {
        return $this->setTrigger('priceIncludesSalesTax', $priceIncludesSalesTax);
    }

    /**
     * Get productCategoryHierarchyName
     *
     * @return string
     */
    public function getProductCategoryHierarchyName(): ?string
    {
        return $this->productCategoryHierarchyName;
    }

    /**
     * Set productCategoryHierarchyName
     *
     * @param string $productCategoryHierarchyName
     *
     * @return Agency
     */
    public function setProductCategoryHierarchyName(string $productCategoryHierarchyName): Agency
    {
        return $this->setTrigger('productCategoryHierarchyName', $productCategoryHierarchyName);
    }

    /**
     * Get productNumberOnReceipt
     *
     * @return string
     */
    public function getProductNumberOnReceipt(): ?string
    {
        return $this->productNumberOnReceipt;
    }

    /**
     * Set productNumberOnReceipt
     *
     * @param string $productNumberOnReceipt
     *
     * @return Agency
     */
    public function setProductNumberOnReceipt(string $productNumberOnReceipt): Agency
    {
        return $this->setTrigger('productNumberOnReceipt', $productNumberOnReceipt);
    }

    /**
     * Get purchaseOrderItemFilter
     *
     * @return string
     */
    public function getPurchaseOrderItemFilter(): ?string
    {
        return $this->purchaseOrderItemFilter;
    }

    /**
     * Set purchaseOrderItemFilter
     *
     * @param string $purchaseOrderItemFilter
     *
     * @return Agency
     */
    public function setPurchaseOrderItemFilter(string $purchaseOrderItemFilter): Agency
    {
        return $this->setTrigger('purchaseOrderItemFilter', $purchaseOrderItemFilter);
    }

    /**
     * Get retailChannelId
     *
     * @return string
     */
    public function getRetailChannelId(): ?string
    {
        return $this->retailChannelId;
    }

    /**
     * Set retailChannelId
     *
     * @param string $retailChannelId
     *
     * @return Agency
     */
    public function setRetailChannelId(string $retailChannelId): Agency
    {
        return $this->setTrigger('retailChannelId', $retailChannelId);
    }

    /**
     * Get roundingAccountLedgerValue
     *
     * @return string
     */
    public function getRoundingAccountLedgerValue(): ?string
    {
        return $this->roundingAccountLedgerValue;
    }

    /**
     * Set roundingAccountLedgerValue
     *
     * @param string $roundingAccountLedgerValue
     *
     * @return Agency
     */
    public function setRoundingAccountLedgerValue(string $roundingAccountLedgerValue): Agency
    {
        return $this->setTrigger('roundingAccountLedgerValue', $roundingAccountLedgerValue);
    }

    /**
     * Get roundingTaxAccount
     *
     * @return string
     */
    public function getRoundingTaxAccount(): ?string
    {
        return $this->roundingTaxAccount;
    }

    /**
     * Set roundingTaxAccount
     *
     * @param string $roundingTaxAccount
     *
     * @return Agency
     */
    public function setRoundingTaxAccount(string $roundingTaxAccount): Agency
    {
        return $this->setTrigger('roundingTaxAccount', $roundingTaxAccount);
    }

    /**
     * Get serviceChargePercentage
     *
     * @return int
     */
    public function getServiceChargePercentage(): ?int
    {
        return $this->serviceChargePercentage;
    }

    /**
     * Set serviceChargePercentage
     *
     * @param int $serviceChargePercentage
     *
     * @return Agency
     */
    public function setServiceChargePercentage(int $serviceChargePercentage): Agency
    {
        return $this->setTrigger('serviceChargePercentage', $serviceChargePercentage);
    }

    /**
     * Get serviceChargePrompt
     *
     * @return string
     */
    public function getServiceChargePrompt(): ?string
    {
        return $this->serviceChargePrompt;
    }

    /**
     * Set serviceChargePrompt
     *
     * @param string $serviceChargePrompt
     *
     * @return Agency
     */
    public function setServiceChargePrompt(string $serviceChargePrompt): Agency
    {
        return $this->setTrigger('serviceChargePrompt', $serviceChargePrompt);
    }

    /**
     * Get sqlServerName
     *
     * @return string
     */
    public function getSqlServerName(): ?string
    {
        return $this->sqlServerName;
    }

    /**
     * Set sqlServerName
     *
     * @param string $sqlServerName
     *
     * @return Agency
     */
    public function setSqlServerName(string $sqlServerName): Agency
    {
        return $this->setTrigger('sqlServerName', $sqlServerName);
    }

    /**
     * Get statementMethod
     *
     * @return string
     */
    public function getStatementMethod(): ?string
    {
        return $this->statementMethod;
    }

    /**
     * Set statementMethod
     *
     * @param string $statementMethod
     *
     * @return Agency
     */
    public function setStatementMethod(string $statementMethod): Agency
    {
        return $this->setTrigger('statementMethod', $statementMethod);
    }

    /**
     * Get statementPerStaffTerminal
     *
     * @return string
     */
    public function getStatementPerStaffTerminal(): ?string
    {
        return $this->statementPerStaffTerminal;
    }

    /**
     * Set statementPerStaffTerminal
     *
     * @param string $statementPerStaffTerminal
     *
     * @return Agency
     */
    public function setStatementPerStaffTerminal(string $statementPerStaffTerminal): Agency
    {
        return $this->setTrigger('statementPerStaffTerminal', $statementPerStaffTerminal);
    }

    /**
     * Get statementPostAsBusinessDay
     *
     * @return string
     */
    public function getStatementPostAsBusinessDay(): ?string
    {
        return $this->statementPostAsBusinessDay;
    }

    /**
     * Set statementPostAsBusinessDay
     *
     * @param string $statementPostAsBusinessDay
     *
     * @return Agency
     */
    public function setStatementPostAsBusinessDay(string $statementPostAsBusinessDay): Agency
    {
        return $this->setTrigger('statementPostAsBusinessDay', $statementPostAsBusinessDay);
    }

    /**
     * Get storeArea
     *
     * @return int
     */
    public function getStoreArea(): ?int
    {
        return $this->storeArea;
    }

    /**
     * Set storeArea
     *
     * @param int $storeArea
     *
     * @return Agency
     */
    public function setStoreArea(int $storeArea): Agency
    {
        return $this->setTrigger('storeArea', $storeArea);
    }

    /**
     * Get storeName
     *
     * @return string
     */
    public function getStoreName(): ?string
    {
        return $this->storeName;
    }

    /**
     * Set storeName
     *
     * @param string $storeName
     *
     * @return Agency
     */
    public function setStoreName(string $storeName): self
    {
        $this->storeName = $storeName;

        return $this;
    }

    /**
     * Get storeNumber
     *
     * @return string
     */
    public function getStoreNumber(): ?string
    {
        return $this->storeNumber;
    }

    /**
     * Set storeNumber
     *
     * @param string $storeNumber
     *
     * @return Agency
     */
    public function setStoreNumber(string $storeNumber): Agency
    {
        return $this->setTrigger('storeNumber', $storeNumber);
    }

    /**
     * Get taxGroupCode
     *
     * @return string
     */
    public function getTaxGroupCode(): ?string
    {
        return $this->taxGroupCode;
    }

    /**
     * Set taxGroupCode
     *
     * @param string $taxGroupCode
     *
     * @return Agency
     */
    public function setTaxGroupCode(string $taxGroupCode): Agency
    {
        return $this->setTrigger('taxGroupCode', $taxGroupCode);
    }

    /**
     * Get taxGroupLegalEntity
     *
     * @return string
     */
    public function getTaxGroupLegalEntity(): ?string
    {
        return $this->taxGroupLegalEntity;
    }

    /**
     * Set taxGroupLegalEntity
     *
     * @param string $taxGroupLegalEntity
     *
     * @return Agency
     */
    public function setTaxGroupLegalEntity(string $taxGroupLegalEntity): Agency
    {
        return $this->setTrigger('taxGroupLegalEntity', $taxGroupLegalEntity);
    }

    /**
     * Get taxIdentificationNumber
     *
     * @return string
     */
    public function getTaxIdentificationNumber(): ?string
    {
        return $this->taxIdentificationNumber;
    }

    /**
     * Set taxIdentificationNumber
     *
     * @param string $taxIdentificationNumber
     *
     * @return Agency
     */
    public function setTaxIdentificationNumber(string $taxIdentificationNumber): Agency
    {
        return $this->setTrigger('taxIdentificationNumber', $taxIdentificationNumber);
    }

    /**
     * Get taxOverrideGroupCode
     *
     * @return string
     */
    public function getTaxOverrideGroupCode(): ?string
    {
        return $this->taxOverrideGroupCode;
    }

    /**
     * Set taxOverrideGroupCode
     *
     * @param string $taxOverrideGroupCode
     *
     * @return Agency
     */
    public function setTaxOverrideGroupCode(string $taxOverrideGroupCode): Agency
    {
        return $this->setTrigger('taxOverrideGroupCode', $taxOverrideGroupCode);
    }

    /**
     * Get taxOverrideGroupCodeEntity
     *
     * @return string
     */
    public function getTaxOverrideGroupCodeEntity(): ?string
    {
        return $this->taxOverrideGroupCodeEntity;
    }

    /**
     * Set taxOverrideGroupCodeEntity
     *
     * @param string $taxOverrideGroupCodeEntity
     *
     * @return Agency
     */
    public function setTaxOverrideGroupCodeEntity(string $taxOverrideGroupCodeEntity): Agency
    {
        return $this->setTrigger('taxOverrideGroupCodeEntity', $taxOverrideGroupCodeEntity);
    }

    /**
     * Get tenderDeclarationCalculation
     *
     * @return string
     */
    public function getTenderDeclarationCalculation(): ?string
    {
        return $this->tenderDeclarationCalculation;
    }

    /**
     * Set tenderDeclarationCalculation
     *
     * @param string $tenderDeclarationCalculation
     *
     * @return Agency
     */
    public function setTenderDeclarationCalculation(string $tenderDeclarationCalculation): Agency
    {
        return $this->setTrigger('tenderDeclarationCalculation', $tenderDeclarationCalculation);
    }

    /**
     * Get termsOfPayment
     *
     * @return string
     */
    public function getTermsOfPayment(): ?string
    {
        return $this->termsOfPayment;
    }

    /**
     * Set termsOfPayment
     *
     * @param string $termsOfPayment
     *
     * @return Agency
     */
    public function setTermsOfPayment(string $termsOfPayment): Agency
    {
        return $this->setTrigger('termsOfPayment', $termsOfPayment);
    }

    /**
     * Get transactionServiceProfile
     *
     * @return string
     */
    public function getTransactionServiceProfile(): ?string
    {
        return $this->transactionServiceProfile;
    }

    /**
     * Set transactionServiceProfile
     *
     * @param string $transactionServiceProfile
     *
     * @return Agency
     */
    public function setTransactionServiceProfile(string $transactionServiceProfile): Agency
    {
        return $this->setTrigger('transactionServiceProfile', $transactionServiceProfile);
    }

    /**
     * Get useCustomerBasedTax
     *
     * @return string
     */
    public function getUseCustomerBasedTax(): ?string
    {
        return $this->useCustomerBasedTax;
    }

    /**
     * Set useCustomerBasedTax
     *
     * @param string $useCustomerBasedTax
     *
     * @return Agency
     */
    public function setUseCustomerBasedTax(string $useCustomerBasedTax): Agency
    {
        return $this->setTrigger('useCustomerBasedTax', $useCustomerBasedTax);
    }

    /**
     * Get useDefaultCustomerAccount
     *
     * @return string
     */
    public function getUseDefaultCustomerAccount(): ?string
    {
        return $this->useDefaultCustomerAccount;
    }

    /**
     * Set useDefaultCustomerAccount
     *
     * @param string $useDefaultCustomerAccount
     *
     * @return Agency
     */
    public function setUseDefaultCustomerAccount(string $useDefaultCustomerAccount): Agency
    {
        return $this->setTrigger('useDefaultCustomerAccount', $useDefaultCustomerAccount);
    }

    /**
     * Get useDestinationBasedTax
     *
     * @return string
     */
    public function getUseDestinationBasedTax(): ?string
    {
        return $this->useDestinationBasedTax;
    }

    /**
     * Set useDestinationBasedTax
     *
     * @param string $useDestinationBasedTax
     *
     * @return Agency
     */
    public function setUseDestinationBasedTax(string $useDestinationBasedTax): Agency
    {
        return $this->setTrigger('useDestinationBasedTax', $useDestinationBasedTax);
    }

    /**
     * Get warehouseId
     *
     * @return string
     */
    public function getWarehouseId(): ?string
    {
        return $this->warehouseId;
    }

    /**
     * Set warehouseId
     *
     * @param string $warehouseId
     *
     * @return Agency
     */
    public function setWarehouseId(string $warehouseId): Agency
    {
        return $this->setTrigger('warehouseId', $warehouseId);
    }

    /**
     * Get warehouseIdForCustomerOrder
     *
     * @return string
     */
    public function getWarehouseIdForCustomerOrder(): ?string
    {
        return $this->warehouseIdForCustomerOrder;
    }

    /**
     * Set warehouseIdForCustomerOrder
     *
     * @param string $warehouseIdForCustomerOrder
     *
     * @return Agency
     */
    public function setWarehouseIdForCustomerOrder(string $warehouseIdForCustomerOrder): Agency
    {
        return $this->setTrigger('warehouseIdForCustomerOrder', $warehouseIdForCustomerOrder);
    }

    /**
     * Get warehouseLegalEntity
     *
     * @return string
     */
    public function getWarehouseLegalEntity(): ?string
    {
        return $this->warehouseLegalEntity;
    }

    /**
     * Set warehouseLegalEntity
     *
     * @param string $warehouseLegalEntity
     *
     * @return Agency
     */
    public function setWarehouseLegalEntity(string $warehouseLegalEntity): Agency
    {
        return $this->setTrigger('warehouseLegalEntity', $warehouseLegalEntity);
    }

    /**
     * @return null|string
     */
    public function getIsDigital(): ?string
    {
        return $this->isDigital;
    }

    /**
     * @return bool|null
     */
    public function isDigital(): ?bool
    {
        return $this->isDigital && (strtolower($this->isDigital) === 'yes');
    }

    /**
     * @param null|string $isDigital
     *
     * @return Agency
     */
    public function setIsDigital(?string $isDigital): self
    {
        return $this->setTrigger('isDigital', $isDigital);
    }
}
