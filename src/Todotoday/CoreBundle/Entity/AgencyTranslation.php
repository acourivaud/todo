<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 27/06/17
 * Time: 12:29
 */

namespace Todotoday\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Translatable\Entity\MappedSuperclass\AbstractPersonalTranslation;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class AgencyTranslation
 * @package Todotoday\CoreBundle\Entity
 * @Serializer\ExclusionPolicy("all")
 * @ORM\Entity()
 * @ORM\Table(schema="core",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="lookup_unique_agency_translation_idx", columns={
 *         "locale", "object_id", "field"
 * })})
 */
class AgencyTranslation extends AbstractPersonalTranslation
{
    /**
     * @ORM\ManyToOne(
     *     targetEntity="Todotoday\CoreBundle\Entity\Agency",
     *      inversedBy="translations"
     * )
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $object;
}
