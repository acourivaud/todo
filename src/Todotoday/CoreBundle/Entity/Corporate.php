<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 27/01/2017
 * Time: 13:05
 */

namespace Todotoday\CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Todotoday\AccountBundle\Entity\Account;

/**
 * Class Corporate
 *
 * @ORM\Entity()
 * @ORM\Table(name="corporate", schema="core")
 *
 * @category   Todo-Todev
 * @package    Todotoday\CoreBundle\Entity
 * @subpackage Todotoday\CoreBundle\Entity
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class Corporate
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer", options={"unsigned"=true})
     */
    protected $id;

    /**
     * @var string
     *
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(name="slug", type="string", unique=true)
     */
    protected $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     */
    protected $name;

    /**
     * @var Collection|Agency[]
     *
     * @ORM\ManyToMany(targetEntity="Todotoday\CoreBundle\Entity\Agency", mappedBy="corporates")
     */
    protected $agencies;

    /**
     * @var Collection|Account[]
     *
     * @ORM\OneToMany(targetEntity="Todotoday\AccountBundle\Entity\Account", mappedBy="corporate")
     */
    protected $accounts;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->agencies = new ArrayCollection();
        $this->accounts = new ArrayCollection();
    }

    /**
     * Add account
     *
     * @param Account $account
     *
     * @return Corporate
     */
    public function addAccount(Account $account): self
    {
        $this->accounts[] = $account;
        $account->setCorporate($this);

        return $this;
    }

    /**
     * Add agency
     *
     * @param Agency $agency
     *
     * @return Corporate
     */
    public function addAgency(Agency $agency): self
    {
        $this->agencies[] = $agency;
        $agency->addCorporate($this);

        return $this;
    }

    /**
     * Get accounts
     *
     * @return Collection|Account[]
     */
    public function getAccounts(): Collection
    {
        return $this->accounts;
    }

    /**
     * Get agencies
     *
     * @return Collection|Agency[]
     */
    public function getAgencies(): Collection
    {
        return $this->agencies;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Corporate
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Corporate
     */
    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Remove account
     *
     * @param Account $account
     *
     * @return Corporate
     */
    public function removeAccount(Account $account): self
    {
        $this->accounts->removeElement($account);
        $account->setCorporate(null);

        return $this;
    }

    /**
     * Remove agency
     *
     * @param Agency $agency
     *
     * @return Corporate
     */
    public function removeAgency(Agency $agency): self
    {
        $this->agencies->removeElement($agency);
        $agency->removeCorporate($this);

        return $this;
    }
}
