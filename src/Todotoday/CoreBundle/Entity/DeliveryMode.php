<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 08/05/17
 * Time: 17:33
 */

namespace Todotoday\CoreBundle\Entity;

/**
 * Just use to send JMS a type for serializing DeliveryMode
 *
 * Class DeliveryMode
 * @package Todotoday\CoreBundle\Entity
 */
class DeliveryMode
{
}
