<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 23/02/2017
 * Time: 17:33
 */

namespace Todotoday\CoreBundle\Repository\Api\Microsoft;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiRepository;
use Todotoday\CoreBundle\Entity\Api\Microsoft\Agency;

/**
 * Class AgencyRepository
 *
 * @category   Todo-Todev
 * @package    Todotoday\CoreBundle
 * @subpackage Todotoday\CoreBundle\Repository\Api\Microsoft
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class AgencyRepository extends AbstractApiRepository
{
    /**
     * Do findAllAgenciesWithNoActianeId
     *
     * @return Agency[]|null
     * @throws \InvalidArgumentException
     */
    public function findAllAgenciesWithNoActianeId(): ?array
    {
        $filter = 'IdActiane eq \'\'';

        return $this->findBy($filter);
    }

    /**
     * @param string $idActiane
     *
     * @return Agency[]|null
     * @throws \InvalidArgumentException
     */
    public function findFilledIdActiane(string $idActiane): ?array
    {
        $filter = 'IdActiane eq \'' . $idActiane . '\'';

        return $this->findBy($filter);
    }

    /**
     * @param string $name
     *
     * @return Agency[]|null
     * @throws \InvalidArgumentException
     */
    public function findByStoreName(string $name): ?array
    {
        $filter = 'StoreName eq \'' . $name . '\'';

        return $this->findBy($filter);
    }

    /**
     * @param string $filter
     *
     * @return Agency[]|null
     * @throws \InvalidArgumentException
     */
    private function findBy(string $filter): ?array
    {
        return $this
            ->createQueryBuilder()
            ->addCustomQuery(
                '$filter',
                $filter
            )
            ->addCustomQuery(
                '$orderby',
                'StoreName'
            )
            ->getRequest()
            ->execute();
    }
}
