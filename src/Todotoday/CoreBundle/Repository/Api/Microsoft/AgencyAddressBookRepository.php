<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 23/02/2017
 * Time: 17:33
 */

namespace Todotoday\CoreBundle\Repository\Api\Microsoft;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiRepository;
use Todotoday\CoreBundle\Entity\Api\Microsoft\Agency;
use Todotoday\CoreBundle\Entity\Api\Microsoft\AgencyAddressBook;

/**
 * Class AgencyAddressBookRepository
 *
 * @category   Todo-Todev
 * @package    Todotoday\CoreBundle
 * @subpackage Todotoday\CoreBundle\Repository\Api\Microsoft
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class AgencyAddressBookRepository extends AbstractApiRepository
{
    /**
     * Do findAddressBooksByAgency
     *
     * @param Agency $agency
     * @param string $type
     *
     * @return AgencyAddressBook[]
     * @throws \InvalidArgumentException
     */
    public function findAddressBooksByAgency(Agency $agency, string $type = 'Customer')
    {
        return $this->findAddressBooksByAgencyRetailChannelId($agency->getRetailChannelId(), $type);
    }

    /**
     * Do findAddressBooksByAgency
     *
     * @param string $retailChannelId
     * @param string $type
     *
     * @return AgencyAddressBook[]
     * @throws \InvalidArgumentException
     */
    public function findAddressBooksByAgencyRetailChannelId(string $retailChannelId, string $type = 'Customer')
    {
        $filter = 'RetailChannelId eq \'' . $retailChannelId;
        $filter .= '\' and AddressBookType eq Microsoft.Dynamics.DataEntities.RetailAddressBookType\'' . $type . '\'';

        return $this
            ->createQueryBuilder()
            ->addCustomQuery(
                '$filter',
                $filter
            )
            ->getRequest()
            ->execute()
            ;
    }
}
