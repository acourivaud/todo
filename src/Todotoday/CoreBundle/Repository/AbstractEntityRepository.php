<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 31/01/2017
 * Time: 18:02
 */

namespace Todotoday\CoreBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Todotoday\CoreBundle\Services\DomainContextManager;

/**
 * Class AbstractEntityRepository
 *
 * @package    Todotoday\CoreBundle
 * @subpackage Todotoday\CoreBundle\Repository
 */
abstract class AbstractEntityRepository extends EntityRepository
{
    /**
     * @var DomainContextManager
     */
    protected $cm;

    /**
     * @deprecated "One use findBy, find is incompatible with the domainContext"
     *
     * @param mixed $id
     * @param null  $lockMode
     * @param null  $lockVersion
     *
     * @return null|object
     * @throws \Doctrine\ORM\TransactionRequiredException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\ORMException
     */
    public function find($id, $lockMode = null, $lockVersion = null)
    {
        return $this->_em->find($this->_entityName, $id, $lockMode, $lockVersion);
    }

    /**
     * @param array      $criteria
     * @param array|null $orderBy
     * @param null       $limit
     * @param null       $offset
     *
     * @return array
     * @throws \LogicException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     */
    public function findBy(
        array $criteria,
        array $orderBy = null,
        $limit = null,
        $offset = null
    ) {
        $persister = $this->_em->getUnitOfWork()->getEntityPersister($this->_entityName);

        return $persister->loadAll($this->addContext($criteria), $orderBy, $limit, $offset);
    }

    /**
     * @param array      $criteria
     * @param array|null $orderBy
     *
     * @return null|object
     * @throws \LogicException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     */
    public function findOneBy(array $criteria, array $orderBy = null)
    {
        $persister = $this->_em->getUnitOfWork()->getEntityPersister($this->_entityName);

        return $persister->load($this->addContext($criteria), null, null, array(), null, 1, $orderBy);
    }

    /**
     * @param DomainContextManager $cmanager
     *
     * @return AbstractEntityRepository
     */
    public function setDomainContextManager(DomainContextManager $cmanager): self
    {
        $this->cm = $cmanager;

        return $this;
    }

    /**
     * @param array $critera
     *
     * @return array
     * @throws \LogicException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \UnexpectedValueException
     */
    protected function addContext(array $critera = []): array
    {
        if ($context = $this->getDomainContextManager()->getCurrentContext()->getAgency()) {
            $critera['agency'] = $context;
        }

        return $critera;
    }

    /**
     * @return DomainContextManager
     * @throws \LogicException
     */
    protected function getDomainContextManager()
    {
        if (!$this->cm instanceof DomainContextManager) {
            $message = array(
                'trigger' => 'The "cm" attribute (DomainContextManager) is NULL.',
                'possibleReason' => 'Did you retrieved this repository using "$doctrine->getRepository()"?',
                'remedy' => 'If so, retrieve it instead directly from the container',
            );
            throw new \LogicException(implode(' ', $message));
        }

        return $this->cm;
    }
}
