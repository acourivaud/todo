<?php

declare(strict_types=1);
/**
 * PHP version 7
 */

namespace Todotoday\CoreBundle\Controller\Api;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Security\Acl\Model\DomainObjectInterface;

/**
 * Class AbstractApiController
 *
 * @package Todotoday\CoreBundle\Controller\Api
 */
abstract class AbstractApiController extends FOSRestController implements DomainObjectInterface
{
    /**
     * {@inheritdoc}
     */
    public function getObjectIdentifier(): string
    {
        return 'class';
    }

    /**
     * Get default groups to display from the current entity class
     *
     * @return string[]
     */
    protected function getDefaultGroups(): array
    {
        return ['Default'];
    }

    /**
     * Get the formType for the current entity class link to the controller
     *
     * @return string
     */
    abstract protected function getFormType(): string;

    /**
     * Get the serivce name from custum repository's entity
     *
     * @return string
     */
    abstract protected function getServiceRepository(): string;
}
