<?php

declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 18/07/17
 * Time: 21:33
 */

namespace Todotoday\CoreBundle\Controller\Api;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiConnection;
use Actiane\ApiConnectorBundle\Security\OAuth2ApiToken;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Acl\Model\DomainObjectInterface;

/**
 * Class MicrosoftApiController
 * @package Todotoday\CoreBundle\Controller\Api
 * @Rest\NamePrefix(value="todotoday.core.api.microsoft.")
 */
class MicrosoftApiController extends FOSRestController implements DomainObjectInterface
{
    /**
     * @param ParamFetcher $paramFetcher
     *
     * @return Response
     * @throws \LogicException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Psr\Log\InvalidArgumentException
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Get token for microsoft oauth2",
     *     section="Microsoft"
     * )
     *
     * @Rest\QueryParam(
     *     name="env",
     *     requirements="(ax|crm)",
     *     default="ax",
     *     description="Choose microsoft environnment"
     * )
     *
     * @Rest\Route("/token")
     */
    public function getTokenAction(ParamFetcher $paramFetcher): Response
    {
        if (!$this->isGranted('VIEW', $this)) {
            throw $this->createAccessDeniedException();
        }

        $env = $paramFetcher->get('env');
        $env = $env === 'ax' ? 'microsoft' : $env;

        /** @var AbstractApiConnection $connection */
        $connection = $this->get('actiane.api_connector.connection.' . $env);

        /** @var OAuth2ApiToken $apiToken */
        $apiToken = $connection->connect();

        $token = [
            'token_type' => $apiToken->getType(),
            'scope' => $apiToken->getScope(),
            'expires_in' => $apiToken->getExpiresIn(),
            'ext_expires_in' => $apiToken->getExtExpiresIn(),
            'expires_on' => $apiToken->getExpiresOn(),
            'not_before' => $apiToken->getNotBefore(),
            'resource' => $apiToken->getResource(),
            'access_token' => $apiToken->getAccessToken(),
            'refresh_token' => $apiToken->getRefreshToken(),
        ];

        $view = $this->view($token);

        return $this->handleView($view);
    }

    /**
     * Returns a unique identifier for this domain object.
     *
     * @return string
     */
    public function getObjectIdentifier(): string
    {
        return 'class';
    }
}
