<?php declare(strict_types = 1);
/**
 * PHP version 7
 */

namespace Todotoday\CoreBundle\Controller\Api;

use FOS\RestBundle\Controller\Annotations as FosRest;
use FOS\RestBundle\Request\ParamFetcher;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CoreBundle\Annotation\Api\ForceDomainVote;
use Todotoday\CoreBundle\Form\Api\AgencyType;
use Todotoday\CoreBundle\Traits\Api\ApiControllerGetTrait;
use Todotoday\CoreBundle\Traits\Api\ApiControllerPostTrait;
use Todotoday\CoreBundle\Traits\Api\ApiControllerPutTrait;

/**
 * Class AgencyApiController
 *
 * @package Todotoday\CoreBundle\Controller\Api
 * @FosRest\Prefix("/agency")
 * @FOSRest\NamePrefix(value="todotoday.core.api.agency.")
 */
class AgencyApiController extends AbstractApiController
{
    use ApiControllerGetTrait, ApiControllerPostTrait, ApiControllerPutTrait;

    /**
     *
     * Get single agency by id
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Operations on agencies.",
     *     description="Get single agency by id",
     *     output={
     *           "class"="Todotoday\CoreBundle\Entity\Agency",
     *           "groups"={"Default","agency", "avatar"}
     *     },
     *     views={"default","agency"},
     *     section="Agency"
     * )
     *
     * @FosRest\Route("/{id}", requirements={"id" = "\d+"})
     *
     * @FosRest\QueryParam(
     *     name="fields",
     *     requirements="((agency|avatar),?)*",
     *     default="",
     *     description="Fields you need"
     * )
     *
     * {@inheritdoc}
     * @throws \LogicException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function getAction(ParamFetcher $paramFetcher, int $id)
    {
        return $this->getRoute($paramFetcher, $id);
    }

    /**
     *
     * Get all agencies
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Operations on agencies.",
     *     description="Get all agencies",
     *     output={
     *           "class"="array<Todotoday\CoreBundle\Entity\Agency> as agencies",
     *           "groups"={"Default","agency", "avatar"}
     *     },
     *     views={"default","agency"},
     *     section="Agency"
     * )
     *
     * @FosRest\Route("")
     * @FosRest\QueryParam(
     *     name="fields",
     *     requirements="((agency|avatar),?)*",
     *     default="",
     *     description="Fields you need"
     * )
     *
     * {@inheritdoc}
     * @throws \LogicException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function getAllAction(ParamFetcher $paramFetcher)
    {
        return $this->getAllRoute($paramFetcher);
    }

    /**
     *
     * Get all agencies for adherent
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Operations on agencies.",
     *     description="Get all agencies for adherent",
     *     output={
     *           "class"="array<Todotoday\CoreBundle\Entity\Agency> as agencies",
     *           "groups"={"Default","agency", "avatar"}
     *     },
     *     views={"default","agency"},
     *     section="Agency"
     * )
     *
     * @FosRest\Route("/me")
     * @FosRest\QueryParam(
     *     name="fields",
     *     requirements="((agency|avatar),?)*",
     *     default="",
     *     description="Fields you need"
     * )
     * @ForceDomainVote()
     *
     * {@inheritdoc}
     * @throws \LogicException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function getMeAllAction(ParamFetcher $paramFetcher)
    {
        $user = $this->getUser();
        if (!$user instanceof Adherent) {
            throw $this->createAccessDeniedException();
        }

        $view = $this->view($this->get($this->getServiceRepository())->getAgencyByAccount($user));

        $groups = $this->getDefaultGroups();
        if ($fields = explode(',', $paramFetcher->get('fields'))) {
            $groups = array_merge($groups, $fields);
        }
        $view->getContext()->addGroups($groups);
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     *
     * Post a new agency
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Operations on agencies.",
     *     description="Post a new agency",
     *     input={
     *           "class"="Todotoday\CoreBundle\Form\Api\AgencyType"
     *     },
     *     output={
     *           "class"="Todotoday\CoreBundle\Entity\Agency",
     *           "groups"={"Default","agency", "avatar"}
     *     },
     *     views={"default","agency"},
     *     section="Agency"
     * )
     *
     * {@inheritdoc}
     * @throws \InvalidArgumentException
     * @throws \LogicException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \Symfony\Component\Security\Acl\Exception\AclAlreadyExistsException
     * @throws \Symfony\Component\Security\Acl\Exception\InvalidDomainObjectException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function postAction(Request $request)
    {
        return $this->postRoute($request);
    }

    /**
     * Update single agency by id
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Operations on agencies.",
     *     description="Update single agency by id",
     *     input={
     *           "class"="Todotoday\CoreBundle\Form\Api\AgencyType"
     *     },
     *     output={
     *           "class"="Todotoday\CoreBundle\Entity\Agency",
     *           "groups"={"Default","agency", "avatar"}
     *     },
     *     views={"default","agency"},
     *     section="Agency"
     * )
     * @FosRest\Route("/{id}", requirements={"id" = "\d+"})
     *
     * {@inheritdoc}
     * @throws \InvalidArgumentException
     * @throws \LogicException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function putAction(Request $request, int $id): Response
    {
        return $this->putRoute($request, $id);
    }

    /**
     * {@inheritdoc}
     */
    protected function getFormType(): string
    {
        return AgencyType::class;
    }

    /**
     * Get the serivce name from custum repository's entity
     *
     * @return string
     */
    protected function getServiceRepository(): string
    {
        return 'todotoday_core.repository.agency_repository';
    }
}
