<?php declare(strict_types=1);

namespace Todotoday\CoreBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AgencyAdminController
 *
 * @package    Todotoday\CoreBundle
 * @subpackage Todotoday\CoreBundle\Controller\Admin
 */
class AgencyAdminController extends CRUDController
{
    /**
     * @param int $id
     *
     * @return Response
     * @throws \InvalidArgumentException
     */
    public function operationAction(int $id): Response
    {
        return new RedirectResponse(
            $this->generateUrl('admin.agency_operation', ['id' => $id])
        );
    }

    /**
     * @param Request $request
     * @param mixed   $object
     *
     * @return null
     */
    protected function preCreate(Request $request, $object)
    {
        $logger = $this->get('monolog.logger.admin');
        $logger->addInfo('AgencyAdminController preCreate request', (array) $request->request);
        $logger->addInfo('AgencyAdminController preCreate query', (array) $request->query);
        $logger->addInfo('AgencyAdminController preCreate object', (array) $object);

        return null;
    }
}
