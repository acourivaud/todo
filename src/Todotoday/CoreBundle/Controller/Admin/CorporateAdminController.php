<?php declare(strict_types = 1);

namespace Todotoday\CoreBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController;

/**
 * Class CorporateAdminController
 *
 * @package    Todotoday\CoreBundle
 * @subpackage Todotoday\CoreBundle\Controller\Admin
 */
class CorporateAdminController extends CRUDController
{
}
