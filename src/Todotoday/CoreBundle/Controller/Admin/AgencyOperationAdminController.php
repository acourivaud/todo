<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 10/10/17
 * Time: 09:50
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\CoreBundle\Controller\Admin
 *
 * @subpackage Todotoday\CoreBundle\Controller\Admin
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\CoreBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Exceptions\AgencyNotFoundException;
use Todotoday\CoreBundle\Form\Admin\AgencyAdminFormType;
use Todotoday\CoreBundle\Repository\AgencyRepository;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Enum\AdherentOptinEnum;

/**
 * @Route("/admin/todotoday/agency/operation")
 * Class AgencyOperationAdminController
 */
class AgencyOperationAdminController extends Controller
{
    /**
     * @Route("/{id}", name="admin.agency_operation")
     * @param int $id
     *
     * @return Response|HttpException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyNotFoundException
     * @throws \InvalidArgumentException
     */
    public function indexAction(int $id)
    {
        $repo = $this->getRepo();
        $agency = $this->getAgency($id);
        $count = $repo->getCountAdherent($agency);

        return $this->render(
            'TodotodayCoreBundle:Admin/AgencyOperation:agency_operation.html.twig',
            [
                'agency' => $agency,
                'count' => $count['countAdherent'],
            ]
        );
    }

    /**
     * @Route("/{id}/copy", name="admin.agency_operation_copy")
     *
     * @param int     $id
     *
     * @param Request $request
     *
     * @return Response
     * @throws \Symfony\Component\Form\Exception\UnexpectedTypeException
     * @throws \Symfony\Component\Form\Exception\LogicException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyNotFoundException
     * @throws \Todotoday\AccountBundle\Exceptions\AdherentMigrateEpaDontMatchException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \LogicException
     */
    public function copyAdherentAction(int $id, Request $request): Response
    {
        $agencyFrom = $this->getAgency($id);
        $form = $this->createForm(
            AgencyAdminFormType::class,
            null,
            [
                'agencyFrom' => $agencyFrom,
            ]
        );
        $form->add(
            'valider',
            SubmitType::class,
            [
                'label' => 'submit',
                'translation_domain' => 'messages',
            ]
        );
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $agencyTo = $this->getAgency($form->getData()['agencyTo']);
            $operationService = $this->get('todotoday.account.services.adherent_operation');
            $nbAdherentCopied = $operationService->giveAdherentAccessToAnotherAgency($agencyFrom, $agencyTo);

            $this->addFlash(
                'success',
                sprintf(
                    '%d adhérent(s) copié(s) de %s à %s',
                    $nbAdherentCopied,
                    $agencyFrom->getName(),
                    $agencyTo->getName()
                )
            );

            return $this->redirectToRoute('admin.agency_operation', ['id' => $id]);
        }

        return $this->render(
            'TodotodayCoreBundle:Admin/AgencyOperation:agency_operation_copy.html.twig',
            [
                'agency' => $agencyFrom,
                'form' => $form->createView(),
            ]
        );
    }

//    /**
//     * @Route("/{id}/compare", name="admin.agency_operation_compare")
//     *
//     * @param int     $id
//     *
//     * @param Request $request
//     *
//     * @return Response
//     * @throws \Symfony\Component\Form\Exception\UnexpectedTypeException
//     * @throws \Symfony\Component\Form\Exception\LogicException
//     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
//     * @throws \Todotoday\CoreBundle\Exceptions\AgencyNotFoundException
//     * @throws \Psr\Log\InvalidArgumentException
//     * @throws \LogicException
//     */
//    public function compareAdherent(int $id, Request $request): Response
//    {
//        $accounts = null;
//        $agencyFrom = $this->getAgency($id);
//        $form = $this->createForm(
//            AgencyAdminFormType::class,
//            null,
//            [
//                'agencyFrom' => $agencyFrom,
//            ]
//        );
//        $form->add(
//            'valider',
//            SubmitType::class,
//            [
//                'label' => 'submit',
//                'translation_domain' => 'messages',
//            ]
//        );
//        $form->handleRequest($request);
//        if ($form->isSubmitted() && $form->isValid()) {
//            /** @var AdherentRepository $repo */
//            $repo = $this->get('todotoday_account.repository.adherent');
//            $accounts = $repo->compareAdherent($agencyFrom, $this->getAgency($form->getData()['agencyTo']));
//        }
//
//        return $this->render(
//            'TodotodayCoreBundle:Admin/AgencyOperation:agency_operation_compare.html.twig',
//            [
//                'agency' => $agencyFrom,
//                'form' => $form->createView(),
//                'data' => $accounts,
//            ]
//        );
//    }

    /**
     *
     * @Route("/{id}/import", name="admin.agency_operation_import_adherent")
     *
     * @param int $id
     *
     * @return Response
     * @throws AgencyNotFoundException
     */
    public function importAdherentFromMicrosoftAction(int $id): Response
    {
        $agency = $this->getAgency($id);
        $schema = $this->getParameter('scheme');

        return $this->render(
            '@TodotodayCore/Admin/AgencyOperation/agency_operation_import_adherent.html.twig',
            [
                'scheme' => $schema,
                'agency' => $agency,
            ]
        );
    }

    /**
     * @Route("/{id}/mailchimp", name="admin.agency_operation_mailchimp")
     *
     * @param int $id
     *
     * @return Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \LogicException
     */
    public function mailchimpAction($id): Response
    {
        if (!$this->isGranted('VIEW', $this)) {
            throw $this->createAccessDeniedException('Fail');
        }

        $em = $this->getDoctrine()->getManager();

        $agency = $em->getRepository(Agency::class)->find($id);

        if (!$agency) {
            throw $this->createNotFoundException();
        }

        $optinStats = [];

        /** @var AdherentRepository $adherentRepo */
        $adherentRepo = $em->getRepository(Adherent::class);
        $optinTypes = AdherentOptinEnum::toArray();
        if (count($optinTypes) > 0) {
            foreach ($optinTypes as $optinType) {
                $optinStat = [];
                $optinStat['label'] = 'mailchimp.export.label.' . $optinType;
                $optinStat['count'] = $adherentRepo->getCountAdherentsOptinByAgency($agency, $optinType);

                $optinStats[] = $optinStat;
            }
        }

        $optinStat = [];
        $optinStat['label'] = 'mailchimp.export.label.null';
        $optinStat['count'] = $adherentRepo->getCountAdherentsOptinNullByAgency($agency);

        $optinStats[] = $optinStat;

        return $this->render(
            '@TodotodayCore/Admin/AgencyOperation/agency_operation_mailchimp.html.twig',
            [
                'agency' => $agency,
                'optinStats' => $optinStats,
            ]
        );
    }

    /**
     * @param int $id
     *
     * @Route("/{id}/catalog-cache", name="admin.agency_operation_cache_catalog_handler")
     *
     * @return Response
     * @throws \LogicException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws AgencyNotFoundException
     */
    public function catalogCacheHandlerAction(int $id): Response
    {
        if (!$this->isGranted('ROLE_ADMIN')) {
            throw $this->createAccessDeniedException();
        }

        return $this->render(
            '@TodotodayCore/Admin/AgencyOperation/agency_operation_cache_handler.html.twig',
            [
                'scheme' => $this->getParameter('scheme'),
                'agency' => $this->getAgency($id),
            ]
        );
    }

    /**
     * @param int $id
     *
     * @return Agency
     * @throws AgencyNotFoundException
     */
    private function getAgency(int $id): Agency
    {
        $repo = $this->getRepo();
        if (!$agency = $repo->find($id)) {
            throw new AgencyNotFoundException();
        }

        return $agency;
    }

    /**
     * @return AgencyRepository
     */
    private function getRepo(): AgencyRepository
    {
        return $this->get('todotoday_core.repository.agency_repository');
    }
}
