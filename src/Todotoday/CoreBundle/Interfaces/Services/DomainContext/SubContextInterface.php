<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 13/01/2017
 * Time: 23:51
 */

namespace Todotoday\CoreBundle\Interfaces\Services\DomainContext;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Todotoday\CoreBundle\Services\DomainContextManager;

/**
 * Interface SubContextInterface
 *
 * @category   Todo-Todev
 * @package    Todotoday\CoreBundle
 * @subpackage Todotoday\CoreBundle\Interfaces\Services\DomainContext
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
interface SubContextInterface
{
    /**
     * Get domainContextManager
     *
     * @return DomainContextManager
     */
    public function getDomainContextManager(): DomainContextManager;

    /**
     * Set domainContextManager
     *
     * @param DomainContextManager $domainContextManager
     *
     * @return SubContextInterface
     */
    public function setDomainContextManager(DomainContextManager $domainContextManager): self;

    /**
     * Do vote
     *
     * @param TokenInterface $token
     * @param object         $subject
     * @param array          $attributes
     *
     * @return int
     */
    public function vote(TokenInterface $token, $subject, array $attributes): int;
}
