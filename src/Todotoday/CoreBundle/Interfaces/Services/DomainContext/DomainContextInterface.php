<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 13/01/2017
 * Time: 23:51
 */

namespace Todotoday\CoreBundle\Interfaces\Services\DomainContext;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Interface DomainContextInterface
 *
 * @category   Todo-Todev
 * @package    Todotoday\CoreBundle
 * @subpackage Todotoday\CoreBundle\Interfaces\Services\DomainContext
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
interface DomainContextInterface
{
    /**
     * Do getAgency
     *
     * @return Agency|null
     */
    public function getAgency();

    /**
     * Do vote
     *
     * @param TokenInterface $token
     * @param object         $subject
     * @param array          $attributes
     *
     * @return int
     */
    public function vote(TokenInterface $token, $subject, array $attributes): int;
}
