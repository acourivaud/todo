<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 30/05/2017
 * Time: 16:47
 */

namespace Todotoday\CoreBundle\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ChoiceTypeExtension
 *
 * @category   Todo-Todev
 * @package    Todotoday\CoreBundle
 * @subpackage Todotoday\CoreBundle\Form\Extension
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class ChoiceTypeExtension extends AbstractTypeExtension
{
    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        $view->vars['horizontal'] = $options['horizontal'];
    }

    /**
     * {@inheritdoc}
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefault('horizontal', false)
            ->setAllowedTypes('horizontal', 'bool')
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getExtendedType(): string
    {
        return ChoiceType::class;
    }
}
