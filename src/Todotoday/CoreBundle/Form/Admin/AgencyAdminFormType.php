<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 11/10/17
 * Time: 16:46
 *
 * @category   Todo-Todev
 *
 * @package    CoreBundle\Form\Admin
 *
 * @subpackage CoreBundle\Form\Admin
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\CoreBundle\Form\Admin;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Repository\AgencyRepository;

/**
 * Class AgencyAdminFormType
 */
class AgencyAdminFormType extends AbstractType
{
    /**
     * @var AgencyRepository
     */
    private $agencyRepository;

    /**
     * AgencyAdminFormType constructor.
     *
     * @param AgencyRepository $agencyRepository
     */
    public function __construct(AgencyRepository $agencyRepository)
    {
        $this->agencyRepository = $agencyRepository;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @throws \Symfony\Component\Validator\Exception\ConstraintDefinitionException
     * @throws \Symfony\Component\Validator\Exception\InvalidOptionsException
     * @throws \Symfony\Component\Validator\Exception\MissingOptionsException
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $agencies = $this->agencyRepository->getAgencyFromEpa($options['agencyFrom']);

        $data['Sélectionner une conciergerie de destination'] = null;
        foreach ($agencies as $agency) {
            $data[$agency->getName()] = $agency->getId();
        }

        $builder->add(
            'agencyTo',
            ChoiceType::class,
            [
                'choices' => $data,
                'label' => 'agency_destination',
                'required' => true,
                'translation_domain' => 'messages',
                'constraints' => [new NotBlank(['message' => 'Veuillez choisir une conciergerie de destination'])],
            ]
        );
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setRequired('agencyFrom');
        $resolver->setAllowedTypes('agencyFrom', Agency::class);
    }
}
