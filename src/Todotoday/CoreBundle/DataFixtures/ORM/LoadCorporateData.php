<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 27/01/2017
 * Time: 13:49
 */

namespace Todotoday\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Entity\Corporate;

/**
 * Class LoadCorporateData
 *
 * @category   Todo-Todev
 * @package    Todotoday\CoreBundle
 * @subpackage Todotoday\CoreBundle\DataFixtures\ORM
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class LoadCorporateData extends AbstractFixture implements DependentFixtureInterface, ContainerAwareInterface
{
    /**
     * @var string[]
     */
    protected static $corporatesSlugs;

    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * Do getAgencysSlug
     *
     * @return string[]
     */
    public static function getCorporatesSlug()
    {
        if (!static::$corporatesSlugs) {
            static::$corporatesSlugs = array();
            $iteration = 0;

            while ($iteration < 3) {
                static::$corporatesSlugs[$iteration] = 'corporate' . $iteration;
                $iteration++;
            }
        }

        return static::$corporatesSlugs;
    }

    /**
     * {@inheritdoc}
     */
    public function getDependencies()
    {
        return array(
            LoadAgencyData::class,
        );
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        if ($this->kernel->getEnvironment() === 'prod') {
            return;
        }
        $corporatesSlugs = static::getCorporatesSlug();
        $agenciesSlugs = LoadAgencyData::getAgenciesSlug();

        foreach ($corporatesSlugs as $key => $corporateSlug) {
            /** @var Agency $agency */
            $agency = $this->getReference('agency_' . $agenciesSlugs[$key]);
            $corporate = (new Corporate())
                ->setName($corporateSlug)
                ->addAgency($agency);

            $manager->persist($corporate);
            $manager->flush();
            $this->setReference($corporateSlug, $corporate);
        }
    }

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->kernel = $container->get('kernel');
    }
}
