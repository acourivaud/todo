<?php declare(strict_types=1);
/**
 * PHP version 7
 */

namespace Todotoday\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Generator;
use Sonata\MediaBundle\Entity\MediaManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Todotoday\ClassificationBundle\DataFixtures\ORM\LoadCategoryData;
use Todotoday\ClassificationBundle\DataFixtures\ORM\LoadContextData;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Repository\Api\Microsoft\AgencyRepository;
use Todotoday\MediaBundle\Entity\Media;

/**
 * Class LoadUserData
 *
 * @package    Todotoday\CoreBundle
 * @subpackage Todotoday\CoreBundle\DataFixtures\ORM
 */
class LoadAgencyData extends AbstractFixture implements DependentFixtureInterface, ContainerAwareInterface
{
    /**
     * @var string[]
     */
    protected static $agenciesSlugs;

    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * @var MediaManager
     */
    private $media;

    /**
     * @var Generator
     */
    private $faker;

    /**
     * @var AgencyRepository
     */
    private $agencyRepo;

    /**
     * Do getAgencysSlug
     *
     * @return string[]
     */
    public static function getAgenciesSlug(): array
    {
        if (!static::$agenciesSlugs) {
            static::$agenciesSlugs = array();
            $iteration = 0;

            while ($iteration < 3) {
                static::$agenciesSlugs[$iteration] = 'demo' . $iteration;
                $iteration++;
            }
        }

        return static::$agenciesSlugs;
    }

    /**
     * @param string $agencySlug
     *
     * @return Agency
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function createAgency(string $agencySlug): Agency
    {
        $logoFile = 'logo_' . $agencySlug . '.png';
        $logoPath = $this->kernel->locateResource('@TodotodayCoreBundle/Resources/public/img/' . $logoFile);

        $category = $this->getReference('agency_category');

        $img = new Media();
        $img->setName('logo' . $agencySlug);
        $img->setContext($this->getReference('default_context')->getId());
        $img->setCategory($category);
        $img->setProviderName('sonata.media.provider.image');
        $img->setBinaryContent($logoPath);

        $agency = (new Agency())
            ->setName($agencySlug)
            ->setEmail($agencySlug . '@todotoday.local')
            ->setCurrency('EUR')
            ->setDefaultLanguage('fr')
            ->setLanguagesString('fr;en')
            ->setDataAreaId('EPA')
            ->setEnabledFront(true)
            ->setStatus('PUBLISHED')
            ->setAddress($this->faker->address())
            ->setPhone($this->faker->phoneNumber())
            ->setInformation('Conciergerie ouverte du lundi au vendredi de 9h à 17h.')
            ->setLogo($img)
            ->setRetailChannelId('000008')
            ->setCrmTeam('empty')
            ->setCrmServiceClient('empty')
            ->setDeliveryModeString('empty')
            ->setDomainWhitelist(['test.fr', 'todotoday.com', 'pix-digital.com']);

        return $agency;
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return array(
            LoadContextData::class,
            LoadCategoryData::class,
        );
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     *
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function load(ObjectManager $manager): void
    {
        if ($this->kernel->getEnvironment() === 'prod') {
            return;
        }

        $agenciesSlugs = static::getAgenciesSlug();

        foreach ($agenciesSlugs as $agencySlug) {
            $agency = $this->createAgency($agencySlug);
            $manager->persist($agency);
            $this->setReference('agency_' . $agencySlug, $agency);
        }

        $manager->flush();
    }

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     *
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     */
    public function setContainer(ContainerInterface $container = null): void
    {
        $this->kernel = $container->get('kernel');
        $this->faker = $container->get('davidbadura_faker.faker');
        $this->media = $container->get('sonata.media.manager.media');
        $this->agencyRepo = $container->get('todotoday.core.repository.api.agency');
    }
}
