<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 28/04/17
 * Time: 11:44
 */

namespace Todotoday\CoreBundle\DataFixtures\ORM;

use Actiane\ApiConnectorBundle\Services\Connections\Microsoft\MicrosoftDynamicsConnection;
use Actiane\PaymentBundle\Enum\PaymentTypeEnum;
use Actiane\PaymentBundle\Services\PaymentMethodManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Sonata\MediaBundle\PHPCR\MediaManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Todotoday\ClassificationBundle\DataFixtures\ORM\LoadCategoryData;
use Todotoday\ClassificationBundle\DataFixtures\ORM\LoadContextData;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Entity\Api\Microsoft\Agency as AgencyMicrosoft;
use Todotoday\CoreBundle\Enum\AgencyModuleEnum;
use Todotoday\CoreBundle\Repository\Api\Microsoft\AgencyRepository;
use Todotoday\MediaBundle\DataFixtures\ORM\LoadMediaData;
use Todotoday\MediaBundle\Entity\Media;
use Todotoday\PaymentBundle\Services\OneDriveManager;

/**
 * Class LoadAgencyLinkedMicrosoft
 *
 * @SuppressWarnings(PHPMD)
 * @package                 Todotoday\CoreBundle\DataFixtures\ORM
 */
class LoadAgencyLinkedMicrosoft extends AbstractFixture implements DependentFixtureInterface, ContainerAwareInterface
{
    // @codingStandardsIgnoreStart
    /**
     * @var string
     */
    protected static $information = <<<EOF
Chez TO DO TODAY, nous œuvrons pour l’épanouissement des individus en milieu professionnel. 
Nos équipes travaillent quotidiennement dans un seul but : vous permettre de Vivre Pleinement ! 

La notion de Vivre Pleinement s'incarne à travers nos 3 piliers fondamentaux:
une vie libérée des contraintes
une vie en pleine forme
une vie engagée

C’est autour de ces trois piliers que se forme notre offre de produits et services. 

Chaque jour, nous effectuons notre travail
Avec conviction. Parce que c’est notre mission, nous ferons tout ce qui est en notre possible pour vous rendre la vie plus facile
Avec respect. Pour nos clients, pour nos collaborateurs, pour nos fournisseurs et pour l’environnement.
Avec agilité. Parce que chacun de nos adhérents a des besoins différents, nous enrichissons en permanence notre offre de services et adaptons notre système de livraison afin de répondre à vos attentes.
EOF;

    // @codingStandardsIgnoreEnd

    /**
     * @var string[]
     */
    protected static $agenciesLinkedSlugs;

    /**
     * @var Media
     */
    protected $mediaBackground;

    /**
     * @var MicrosoftDynamicsConnection
     */
    private $connection;

    /**
     * @var PaymentMethodManager
     */
    private $paymentMethods;

    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * @var MediaManager
     */
    private $media;

    /**
     * @var OneDriveManager
     */
    private $oneDriveManager;

    /**
     * @param null|string $env
     *
     * @return array|\string[]
     */
    public static function getAgenciesLinkedSlug(?string $env = 'test'): array
    {
        if (!static::$agenciesLinkedSlugs) {
            static::$agenciesLinkedSlugs = array(
                'carrefour-massy' => array(
                    'name' => 'CARREFOUR CAMPUS MASSY',
                    'retailStore' => 'CAMPUS CARREFOUR',
                    'languages' => array('fr', 'en'),
                    'information' => static::$information,
                    'schedule' => '',
                    'quickInfo' => <<<EOF
*Votre Conciergerie TO DO TODAY met à votre disposition deux espaces. La Conciergerie principale au Rez-de-jardin et 
une Conciergerie dans le Hall C*

*Nos horaires d'ouverture:*
Bâtiment B Rez-de-jardin
du lundi au vendredi, 8h-19h30
Hall C
du lundi au vendredi, 8h30-17h30"
EOF
                ,
                    'email' => 'massy_conciergerie@carrefour.com',
                    'phone' => '01 64 50 99 45',
                    'address' => '93 Avenue de Paris, 91300 Massy',
                    'allowRegistration' => true,
                    'allowAdherent' => true,
                ),
                'arevalta' => array(
                    'name' => 'AREVA LTA',
                    'retailStore' => 'AREVA LTA DEFENSE',
                    'languages' => array('fr', 'en'),
                    'information' => static::$information,
                    'schedule' => '',
                    'quickInfo' => <<<EOF
*Votre Conciergerie TO DO TODAY est située à l'espace Ramsès, étage -1*

*Nos horaires d'ouverture:*
les lundis, mardis, jeudis & vendredis
8h30-14h / 17h30-18h30
Le mercredi
9h-14h
EOF
                ,
                    'email' => 'areva.lta@todotoday.com',
                    'phone' => '01 34 96 80 80',
                    'address' => 'Tour Areva, 1 place Jean Millier 92084 PARIS LA DEFENSE CEDEX',
                    'allowRegistration' => true,
                    'allowAdherent' => true,
                ),
            );
            if ($env !== 'test') {
                static::$agenciesLinkedSlugs = array_merge(
                    static::$agenciesLinkedSlugs,
                    array(
                        'lodh' => array(
                            'name' => 'LOMBARD ODIER',
                            'retailStore' => 'LOMBARD ODIER',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
Les armoires en libre service se situent au :
• Bâtiment Petit-Lancy : 4 avenue des Morgines
au rez-de-chaussée dans le hall d’entrée du bâtiment B
• Bâtiment Corraterie, Saussure : 13 rue de la Corraterie
Bâtiment 13, COR 1er étage, 20CI-10.10
EOF
                        ,
                            'email' => 'lombardodier@todotoday.com',
                            'phone' => '022 770 00 30',
                            'address' => 'Petit-Lancy : 4 avenue des Morgines Corraterie : 13 rue de la Corraterie',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'lausanne' => array(
                            'name' => 'PHILIP MORRIS',
                            'retailStore' => 'PHILIP MORRIS',
                            'languages' => array('en', 'fr'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
Your Concierge team is available from Monday to Friday. 
 from 8:30am to 6:30pm.
EOF
                        ,
                            'email' => 'pmi@todotoday.com',
                            'phone' => '022 770 00 30',
                            'address' => 'Chemin de Brillancourt 4',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'domus' => array(
                            'name' => 'DOMUS',
                            'retailStore' => 'DOMUS',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
*Votre Conciergerie TO DO TODAY est située au rez-de-chaussée du centre commercial.*

*Nos horaires d'ouverture*:
du lundi au vendredi (sauf le mardi)
11h-14h / 15h-20h
Samedis et dimanches
11h-20h
EOF
                        ,
                            'email' => 'conciergerie.domus@todotoday.com',
                            'phone' => '01.48.12.18.60 / 06.64.46.71.99',
                            'address' => '16, rue de Lisbonne 93110 Rosny sous bois',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'airbussuresnes' => array(
                            'name' => 'AIRBUS GROUP Suresnes',
                            'retailStore' => 'AIRBUS GROUP SURESNES',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
*Votre Conciergerie TO DO TODAY est située dans le hall d'accueil du bâtiment SU2* 

*Nos horaires d'ouverture:*
du lundi au vendredi
9h- 19h
EOF
                        ,
                            'email' => 'conciergerie.su@airbus.com',
                            'phone' => '01 46 97 37 37',
                            'address' => '12 rue Pasteur 92150 SURESNES',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'arevalta' => array(
                            'name' => 'AREVA LTA',
                            'retailStore' => 'AREVA LTA DEFENSE',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
*Votre Conciergerie TO DO TODAY est située à l'espace Ramsès, étage -1*

*Nos horaires d'ouverture:*
les lundis, mardis, jeudis & vendredis
8h30-14h / 17h30-18h30
Le mercredi
9h-14h
EOF
                        ,
                            'email' => 'areva.lta@todotoday.com',
                            'phone' => '01 34 96 80 80',
                            'address' => 'Tour Areva, 1 place Jean Millier 92084 PARIS LA DEFENSE CEDEX',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'adp' => array(
                            'name' => 'ATELIERS DU PARC',
                            'retailStore' => 'ATELIERS DU PARC',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
*Votre Conciergerie TO DO TODAY est située entre le hall du bâtiment Alsace et la cafétéria*

*Nos horaires d'ouverture:*
du lundi au vendredi
11h30-14h30 / 15h30-19h30
EOF
                        ,
                            'email' => 'ateliersduparc@todotoday.fr',
                            'phone' => '01 55 46 70 17',
                            'address' => '62 rue d\'Alsace, 92110 CLICHY',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'avivacarpediem' => array(
                            'name' => 'AVIVA CARPE DIEM',
                            'retailStore' => 'TOUR CARPE DIEM/AVIVA',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'quickInfo' => <<<EOF
*Votre Conciergerie TO DO TODAY est située au niveau 1 de la Tour Carpe Diem, face a jardin d'hiver*

Nos horaires d'ouverture:
*du lundi au vendredi*
8h30-14h / 15h-16h30"
EOF
                        ,
                            'schedule' => '',
                            'email' => 'conciergerie-carpediem@todotoday.com',
                            'phone' => '01 47 44 03 28',
                            'address' => '31, place des Corolles 92400 COURBEVOIE',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'bnp' => array(
                            'name' => 'BNP PARIBAS',
                            'retailStore' => 'BNP EQUITIES',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'quickInfo' => <<<EOF
*Votre Conciergerie TO DO TODAY est située au 5è étage*

*Nos horaires d'ouverture:*
du lundi au vendredi
8h30-14h30
EOF
                        ,
                            'schedule' => '',
                            'email' => 'conciergeriegecd@todotoday.fr',
                            'phone' => '01 40 14 49 49',
                            'address' => '20 bd des italiens, 75009 PARIS',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'capital8' => array(
                            'name' => 'CAPITAL 8',
                            'retailStore' => 'CAPITAL 8',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'quickInfo' => <<<EOF
*Votre Conciergerie TO DO TODAY est située dans le hall principal de l'immeuble*

*Nos horaires d'ouverture:*
du lundi au vendredi
8h30h-14h / 15h-16h30
EOF
                        ,
                            'schedule' => '',
                            'email' => 'conciergeriecapital8@todotoday.com',
                            'phone' => '01 42 89 32 39',
                            'address' => '32 Rue de Monceau, 75008 Paris',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'carrefourboulogne' => array(
                            'name' => 'CARREFOUR BOULOGNE',
                            'retailStore' => 'CARREFOUR BOULOGNE',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
*Votre Conciergerie TO DO TODAY est située au 1er étage.*

*Nos horaires d'ouverture:*
les lundis, mardis, jeudis & vendredis
9h-14h / 16h30-18h
les mercredis
9h-14h / 16h30-17h
EOF
                        ,
                            'email' => 'conciergerie_boulogne@carrefour.com',
                            'phone' => '01 41 04 29 74',
                            'address' => '33, Avenue Emile Zola',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'carrefourevry' => array(
                            'name' => 'CARREFOUR EVRY',
                            'retailStore' => 'CARREFOUR EVRY',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
*Votre Conciergerie TO DO TODAY est située dans la rotonde.*

*Nos horaires d'ouverture:*
du lundi au vendredi 
8h30-14h / 15h30-17h30  
le mercredi
8h30-14h / 15h30-16h30  
EOF
                        ,
                            'email' => 'evry_conciergerie@carrefour.com',
                            'phone' => '01 60 91 24 26 ',
                            'address' => 'ZAE St-Guénault, 1 rue Jean Mermoz, 91080 Evry courcouronnes Cedex',
                            'allowRegistration' => true,
                            'allowAdherent' => false,
                        ),
                        'cbreh' => array(
                            'name' => 'CBRE H',
                            'retailStore' => 'CBRE HOLDING',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
*Votre Conciergerie TO DO TODAY est située au rez-de-chaussée, à côté du salon d'attente.*

 *horaires d'ouverture*:
les lundis & jeudis
8h30-13h
les mardis, mercredi, vendredis
9h-13h
EOF
                        ,
                            'email' => 'conciergerie@cbre.fr',
                            'phone' => '01 53 64 36 54',
                            'address' => '145 rue de Courcelles, 75017 PARIS',
                            'allowRegistration' => true,
                            'allowAdherent' => false,
                        ),
                        'dassault' => array(
                            'name' => 'DASSAULT',
                            'retailStore' => 'DASSAULT',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
*Votre Conciergerie TO DO TODAY est située au rez-de-chaussée, à côté du salon d'attente.*

 *horaires d'ouverture:*
les lundis & jeudis
8h30-13h
les mardis, mercredi, vendredis
9h-13h
EOF
                        ,
                            'email' => 'technique-tdtd@actiane.com',
                            'phone' => '01 61 62 32 10',
                            'address' => '10 rue Marcel Dassault, 78140 VELIZY VILLACOUBLAY',
                            'allowRegistration' => true,
                            'allowAdherent' => false,
                        ),
                        'kering' => array(
                            'name' => 'KERING',
                            'retailStore' => 'KERING',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => '',
                            'email' => 'technique-tdtd@actiane.com',
                            'phone' => '',
                            'address' => '',
                            'allowRegistration' => true,
                            'allowAdherent' => false,
                        ),
                        'crystalpark' => array(
                            'name' => 'CRYSTAL PARK',
                            'retailStore' => 'CRYSTAL PARK',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
*Votre Conciergerie TO DO TODAY est située La Conciergerie est située dans le hall d’accueil côté parc.
Les espaces Bien-Être se situent devant la sortie centrale du centre
de conférence B, à côté des ascenseurs B.*


*Nos horaires d'ouverture:*
du lundi au vendredi
8h30-19h30
EOF
                        ,
                            'email' => 'ConciergerieCrystalPark@todotoday.com',
                            'phone' => '01.41.34.39.47',
                            'address' => '61 rue de Villiers, 92200 NEUILLY SUR SEINE',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'd2' => array(
                            'name' => 'D2',
                            'retailStore' => 'D2/SOGECAP',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
*Votre Conciergerie TO DO TODAY est située au niveau 1 -Hall bas*

*Nos horaires d'ouverture:*
du lundi au vendredi
8h30-10h / 12h-14h30 / 15h30-16h30
EOF
                        ,
                            'email' => 'conciergerie-d2@todotoday.com',
                            'phone' => '01 40 81 02 37',
                            'address' => '17 Bis Place des Reflets 92400 Courbevoie',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'deloitteparis' => array(
                            'name' => 'DELOITTE PARIS',
                            'retailStore' => 'DELOITTE Paris',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
*Votre Conciergerie TO DO TODAY est située au 1er étage.*

*Nos horaires d'ouverture:*
du lundi au vendredi
8h-12h / 13h-17h"
EOF
                        ,
                            'email' => 'technique-tdtd@actiane.com',
                            'phone' => '',
                            'address' => '',
                            'allowRegistration' => true,
                            'allowAdherent' => false,
                        ),
                        'deloittelyon' => array(
                            'name' => 'DELOITTE LYON',
                            'retailStore' => 'DELOITTE Lyon',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
*Votre Conciergerie TO DO TODAY se situe au 1er étage.*

*Nos horaires d'ouverture*
du lundi au vendredi 
8h - 12h / 13h - 17h.
EOF
                        ,
                            'email' => 'frlyonconciergerie@deloitte.f',
                            'phone' => '04 72 43 47 88',
                            'address' => '106 Cours Charlemagne 69286 LYON',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'landy' => array(
                            'name' => 'ENGIE LANDY',
                            'retailStore' => 'ENGIE LANDY',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
*Votre Conciergerie TO DO TODAY est située au rez-de-chaussée.*

*Nos horaires d'ouverture:*
Lundis, mercredis, vendredis
9h-14h
Mardis
15h-18h
Jeudis
15h-17h
EOF
                        ,
                            'email' => 'conciergerie-landy@todotoday.fr',
                            'phone' => '01 49 22 59 39',
                            'address' => '361 Av du Président Wilson, 93200 SAINT DENIS',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'engiet1' => array(
                            'name' => 'ENGIE T1',
                            'retailStore' => 'ENGIE T1',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
*Votre Conciergerie TO DO TODAY est située au rez-de-chaussée.*

*Nos horaires d'ouverture:*
du lundi au jeudi
9h-14h / 15h-18h
Vendredis
9h-14h/ 15h-17h
EOF
                        ,
                            'email' => 'conciergeriet1@todotoday.com',
                            'phone' => '01 56 65 73 90',
                            'address' => <<<EOF

1-2 Place Samuel de Champlain - Faubourg de l\'arche
92390 PARIS LA  DEFENSE CEDEX
EOF
                        ,
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'euroatrium' => array(
                            'name' => 'ENGIE EUROATRIUM',
                            'retailStore' => 'ENGIE EUROATRIUM',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
*Votre Conciergerie TO DO TODAY est située au rez-de-chaussée.*

*Nos horaires d'ouverture:*
lundis, mercredis, vendredis
15h-18h
mardis et jeudis
11h-14h
EOF
                        ,
                            'email' => 'conciergerie_euroatrium@todotoday.com',
                            'phone' => '01 56 65 73 90',
                            'address' => '7 rue Emmy Noether 93400 Saint Ouen',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'eqho' => array(
                            'name' => 'EQHO',
                            'retailStore' => 'EQHO',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
*Votre Conciergerie TO DO TODAY est située dans le hall d'accueil, accès principal côté La Défense.*

*Nos horaires d'ouverture:*
du lundi au vendredi
8h-20h
EOF
                        ,
                            'email' => 'Conciergerie.EQHO@todotoday.com',
                            'phone' => '01 55 24 45 21',
                            'address' => '2 avenue Gambetta - 92700 Courbevoie',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'millenaire' => array(
                            'name' => 'ICADE MILLENAIRE',
                            'retailStore' => 'ICADE',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
*Votre Conciergerie TO DO TODAY est située au rez-de-chaussée du Millénaire 1*

*Nos horaires d'ouverture:*
du lundi au vendredi
9h-19h
EOF
                        ,
                            'email' => 'conciergerielemillenaire@todotoday.com',
                            'phone' => '01 48 34 12 64',
                            'address' => '35 rue de la Gare, Immeuble Millénaire 1, 75019 PARIS',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        /*'icadepn2' => array(
                            'name' => 'ICADE PN2',
                            'retailStore' => 'ICADE PN2',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
                    *Votre Conciergerie TO DO TODAY est située sur l' îlot R au RDC de l’immeuble Rabelais.*

                    *Nos horaires d'ouverture:*
                    du lundi au vendredi
                    9h-19h
                    EOF
                            ,
                            'email' => 'conciergeriepn2R@todotoday.com',
                            'phone' => '01.48.63.25.07',
                            'address' => '45 rue des 3 sœurs - 93420 Villepinte',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),*/
                        'icadepp' => array(
                            'name' => 'ICADE PORTE DE PARIS',
                            'retailStore' => 'ICADE PORTE DE PARIS',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
*Votre Conciergerie TO DO TODAY est située au rez-de-chaussée du bâtiment 264*

*Nos horaires d'ouverture:*
du lundi au vendredi
9h-14h30 / 15h30-18h
EOF
                        ,
                            'email' => 'conciergerieportesdeparis@todotoday.fr',
                            'phone' => '01 48 39 26 73',
                            'address' => '"43/45 avenue Victor Hugo 93300 Aubervilliers',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'majunga' => array(
                            'name' => 'MAJUNGA',
                            'retailStore' => 'TOUR MAJUNGA',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
*Votre Conciergerie TO DO TODAY est située au niveau D*

*Nos horaires d'ouverture:*
du lundi au vendredi
9h-15h
EOF
                        ,
                            'email' => 'conciergeriemajunga@todotoday.com',
                            'phone' => '01 47 76 09 69',
                            'address' => '"6 place de la Pyramide 92 908 PARIS – LA DEFENSE cedex',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'microsoft' => array(
                            'name' => 'MICROSOFT',
                            'retailStore' => 'MICROSOFT',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
*Votre Conciergerie TO DO TODAY est située au 3è étage du bâtiment Canopée.

Nos horaires d'ouverture:
du lundi au vendredi
8h30-18h30*
EOF
                        ,
                            'email' => 'frconcie@microsoft.com',
                            'phone' => '01 57 75 25 61',
                            'address' => '39 Quai du Président Roosevelt, 92130 Issy-les-Moulineaux',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'reckitt-benckiser' => array(
                            'name' => 'RECKITT BENCKISER',
                            'retailStore' => 'RECKITT BENCKISER',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
*Nos horaires d'ouverture:*
lundis: 8h30 - 10h30
mercredis 10h30 - 14h30 vendredis 15h -17h
EOF
                        ,
                            'email' => 'conciergerietodotoday@rb.com',
                            'phone' => '01 69 93 18 17',
                            'address' => '15, rue Ampère 91300 Massy',
                            'allowRegistration' => true,
                            'allowAdherent' => false,
                        ),
                        'safran-saclay' => array(
                            'name' => 'SAFRAN',
                            'retailStore' => 'SAFRAN SACLAY',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
*Votre Conciergerie TO DO TODAY est située au rez-de-chaussée du bâtiment 3, en face du restaurant d’entreprise.*

*Nos horaires d'ouverture:*
du lundi au vendredi 
 8h - 10h / 10h30 - 14h
mardis 
15h - 16h
EOF
                        ,
                            'email' => 'conciergerie-safran-cmh@todotoday.fr',
                            'phone' => '01 61 31 80 08',
                            'address' => 'Rue des jeunes bois Châteaufort CS 80112 78 772 Magny-Les-Hameaux',
                            'allowRegistration' => true,
                            'allowAdherent' => false,
                        ),
                        'saint-gobain' => array(
                            'name' => 'SAINT GOBIN',
                            'retailStore' => 'SAINT GOBAIN',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
Votre Conciergerie TO DO TODAY est située au rez-de-dalle du bâtiment A
 
Nos horaires d'ouverture :
du lundi au vendredi
9h00 - 14h00  / 17h - 18h30  
mercredis, ouverture à 10h
EOF
                        ,
                            'email' => 'Conciergeriedesmiroirs.Cfrgen@saint-gobain.com',
                            'phone' => '01 47 62 43 21',
                            'address' => '18 Avenue d\'Alsace-92400 Courbevoie',
                            'allowRegistration' => true,
                            'allowAdherent' => false,
                        ),
                        'campussfr' => array(
                            'name' => 'SFR',
                            'retailStore' => 'SFR',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
*Votre Conciergerie TO DO TODAY est située au rez-de-chaussée du secteur H.*

*Nos horaires d'ouverture :*
du lundi au vendredi
9h-19h
EOF
                        ,
                            'email' => 'conciergeriecampussfr@todotoday.com',
                            'phone' => '01 85 06 01 03',
                            'address' => '309 Av du Président Wilson, 93210 SAINT-DENIS',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'vivacity' => array(
                            'name' => 'VIVACITY',
                            'retailStore' => 'VIVA CITY',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
*Votre Conciergerie TO DO TODAY se situe au niveau -1*

*Nos horaires d'ouverture :*
du lundi au vendredi
9h-15h"
EOF
                        ,
                            'email' => 'conciergerie.vivacity@todotoday.com',
                            'phone' => '01 43 07 52 32',
                            'address' => '155, rue de Bercy 75012 Paris',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'webhelp' => array(
                            'name' => 'WEBHELP',
                            'retailStore' => 'WEBHELP',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
*Nos horaires d'ouverture :*

les lundis & vendredis
16h-18h
le mercredi
12h-14h"
EOF
                        ,
                            'email' => 'conciergerie@fr.webhelp.com',
                            'phone' => '01 44 40 17 91',
                            'address' => '155, rue de Bercy 75012 Paris',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'airbustoulouse' => array(
                            'name' => 'AIRBUS GROUP TOULOUSE',
                            'retailStore' => 'AIRBUS GROUP TOULOUSE',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
*Votre Conciergerie TO DO TODAY se situe au rez-de-chaussée de l’immeuble Bérenice*

*Nos horaires d'ouverture :*
du lundi au vendredi
8h - 18h
EOF
                        ,
                            'email' => 'conciergerieto@airbus.com',
                            'phone' => '05 81 31 76 90',
                            'address' => '5 quai Marcel Dassault',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'arevalyon' => array(
                            'name' => 'AREVA LYON',
                            'retailStore' => 'AREVA LYON',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
*Votre Conciergerie TO DO TODAY se situe à droite de l’entrée de la Tour A, au rez-de-chaussée.*

*Nos horaires d'ouverture :*
du lundi au vendredi
8h30 - 14h30
EOF
                        ,
                            'email' => 'Areva.LYON@todotoday.com',
                            'phone' => '04.72.74.88.80',
                            'address' => '10, rue juliette Récamier 69006 Lyon',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'pb' => array(
                            'name' => 'HERMES LYON',
                            'retailStore' => 'HERMES',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
*Votre Conciergerie TO DO TODAY se situe à l’entrée du site à côté du poste de Sécurité.*

*Nos horaires d'ouverture:*
 lundis, mardis et jeudis :
 7h30 - 9h / 12h - 14h /16h30 - 18h
EOF
                        ,
                            'email' => 'conciergerie.pb@todotoday.fr',
                            'phone' => '04 78 51 71 09',
                            'address' => '135, rue Henri Barbusse, 69310 Pierre Benite',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'tourpartdieu' => array(
                            'name' => 'TOUT PART DIEU',
                            'retailStore' => 'TOUR PART-DIEU',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
*Votre Conciergerie TO DO TODAY se situe au rez-de-chaussée sortie Servient.*

*Nos horaires d'ouverture:*
du lundi au vendredi 
8h-10h / 12h-14h /16h-18h
EOF
                        ,
                            'email' => 'conciergerie.tourpartdieu@todotoday.fr',
                            'phone' => '04 78 63 63 00',
                            'address' => '129, rue Servient Lyon',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'nestle' => array(
                            'name' => 'NESTLE',
                            'retailStore' => 'NESTLÉ INTERNATIONAL',
                            'languages' => array('en', 'fr'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
Your Service Centre is located in Bergère at the BR14.

The Self-Service Dry Cleaning rooms are located at :
• Bergère : ground floor BR731
• Bussigny : nearby the Reception
• Plan Dessus : K4, groundfloor R05
• Reller I : nearby the Reception
EOF
                        ,
                            'email' => 'todotoday@nestle.com',
                            'phone' => '021 924 63 00',
                            'address' => 'Avenue Nestlé 55 1800 Vevey',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'axamarly' => array(
                            'name' => 'AXA MARLY LE ROI',
                            'retailStore' => 'AXA MARLY LE ROI',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
*L'espace service TO DO TODAY est situé dans le bâtiment B2, en dessous du
restaurant d’entreprise et à proximité de la médiathèque du
Crifie.*


*Horaires d'ouverture*
du lundi au vendredi
8h30- 14h30 / 15h30-17h30
et le mercredi
8h30-14h30"
EOF
                        ,
                            'email' => 'espaceservice_marlyaxa@todotoday.fr',
                            'phone' => '01 30 82 90 20',
                            'address' => 'Place Victorien Sardou 78160 Marly le Roi',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'eylyon' => array(
                            'name' => 'Ernest & Young LYON',
                            'retailStore' => 'E Y',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
*Votre Conciergerie TO DO TODAY se situe au 20e étage.* 

*Nos horaires d'ouverture:*
du lundi au vendredi
9h - 12h / 14h - 16h30.
EOF
                        ,
                            'email' => 'technique-tdtd@actiane.com',
                            'phone' => '',
                            'address' => '10,bd Marius Vivier Merle 69003 Lyon',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'ifpen' => array(
                            'name' => 'IFPEN',
                            'retailStore' => 'IFPEN',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
*Votre Conciergerie TO DO TODAY est ouverte les lundis, mercredis et vendredis:*
8h - 9h / 12h30 - 13h30 / 16h - 17h
EOF
                        ,
                            'email' => 'conciergerie-lyon@ifpen.fr',
                            'phone' => '04 37 70 30 30',
                            'address' => 'Rond point de l\'échangeur de Solaize, 69360 solaize',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'z44' => array(
                            'name' => 'z44',
                            'retailStore' => 'Z44',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
*L'espace service TO DO TODAY vous accueille du lundi au vendredi.*
8h - 18h
EOF
                        ,
                            'email' => 'z44@todotoday.com',
                            'phone' => '022 770 00 30',
                            'address' => 'Allée Pic-Pic 6',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        '3dswaltham' => array(
                            'name' => 'DASSAULT US',
                            'retailStore' => '3DS WALTHAM',
                            'languages' => array('en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
*La Conciergerie TO DO TODAY is located
under the staircase leading to the cafeteria, on level -1.*

*Openning hours: *
Monday through Friday
from 8:00 a.m. to 1:00 p.m."
EOF
                        ,
                            'email' => 'laconciergerie.3dswaltham@todotoday.com',
                            'phone' => '',
                            'address' => '',
                            'allowRegistration' => false,
                            'allowAdherent' => true,
                        ),
                        'coeurdefense' => array(
                            'name' => 'COEUR DEFENSE',
                            'retailStore' => 'COEUR DEFENSE',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
*Votre Conciergerie TO DO TODAY est située au niveau conférence du bâtiment 2.*

*Nos horaires d'ouverture:*
du lundi au vendredi
8h-20h
EOF
                        ,
                            'email' => 'conciergerie-coeurdefense@todotoday.com',
                            'phone' => '',
                            'address' => '"110 esplanade du Général de Gaulle 92931 LA DEFENSE France',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'urssaf' => array(
                            'name' => 'URSSAF / campus montreuil',
                            'retailStore' => 'URSSAF Montreuil',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
Votre Conciergerie TO DO TODAY est située au premier étage, porte 105.

Nos horaires d'ouverture:
Lundis, mardis, jeudis, vendredis 8h30-10h30 / 12h-14h30 / 16h-17h
EOF
                        ,
                            'email' => 'conciergerieurssaf@todotoday.com',
                            'phone' => '148595684',
                            'address' => '22- 24 rue de Lagny Montreuil',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'urssaf-cergy' => array(
                            'name' => 'URSSAF / Cergy',
                            'retailStore' => 'URSSAF Cergy',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
*Votre Conciergerie TO DO TODAY est située au premier étage, porte 107.*

*Nos horaires d'ouverture:*
lundis 8h30-10h30
jeudis 11h45-14h
EOF
                        ,
                            'email' => 'technique-tdtd@actiane.com',
                            'phone' => '01 30 32 79 98',
                            'address' => '1 rue des Chauffours',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'urssaf-melun' => array(
                            'name' => 'URSSAF Melun',
                            'retailStore' => 'URSSAF Melun',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
Votre Conciergerie TO DO TODAY est située porte 003.

Nos horaires d'ouverture:
lundis 11h45-14h
jeudis 8h-10h30
EOF
                        ,
                            'email' => 'technique-tdtd@actiane.com',
                            'phone' => '01 64 09 44 01',
                            'address' => '6 rue René Cassin',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'urssaf-nanterre' => array(
                            'name' => 'URSSAF / Nanterre',
                            'retailStore' => 'URSSAF Nanterre',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
Votre Conciergerie TO DO TODAY est située au deuxième étage, porte 213.

Nos horaires d'ouverture:
mardis et vendredis 11h45-14h30
EOF
                        ,
                            'email' => 'technique-tdtd@actiane.com',
                            'phone' => '01 41 20 83 27',
                            'address' => '97 Avenue François Arago',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'urssaf-creteil' => array(
                            'name' => 'URSSAF / Créteil',
                            'retailStore' => 'URSSAF Créteil',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
Votre Conciergerie TO DO TODAY est située au troisième étage, porte 300.

Nos horaires d'ouverture:
mardis 8h-10h
vendredis 8h-10h30
EOF
                        ,
                            'email' => 'technique-tdtd@actiane.com',
                            'phone' => '01 43 99 13 79',
                            'address' => '3 rue des Archives',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'urssaf-pn' => array(
                            'name' => 'URSSAF / Paris Nord',
                            'retailStore' => 'URSSAF Paris N',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
Votre Conciergerie TO DO TODAY est située au quatrième étage, porte 407.

Nos horaires d'ouverture:
 lundis 11h45-14h30
jeudis 8h-10h30
EOF
                        ,
                            'email' => 'technique-tdtd@actiane.com',
                            'phone' => '01 40 37 77 21',
                            'address' => '11 rue de Cambrai',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'urssaf-evry' => array(
                            'name' => 'URSSAF / Evry',
                            'retailStore' => 'URSSAF Evry',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
Votre Conciergerie TO DO TODAY est située au rez-de-chaussée, porte 6.

Nos horaires d'ouverture:
mardis 12h-14h
vendredis 8h-10h
EOF
                        ,
                            'email' => 'technique-tdtd@actiane.com',
                            'phone' => '01 69 64 87 55',
                            'address' => '1 Le Clos de la Cathédrale',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'urssaf-ps' => array(
                            'name' => 'URSSAF / Paris sud',
                            'retailStore' => 'URSSAF Paris S',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
Votre Conciergerie TO DO TODAY est située au troisième étage, porte 325.

Nos horaires d'ouverture:
 lundis et jeudis 11h45-14h30
EOF
                        ,
                            'email' => 'technique-tdtd@actiane.com',
                            'phone' => '01 45 84 27 33',
                            'address' => '3 rue Tolbiac',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'urssaf-guyancourt' => array(
                            'name' => 'URSSAF / Guyancourt',
                            'retailStore' => 'URSSAF Guyancourt',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
Votre Conciergerie TO DO TODAY est située aupremier étage, porte 101.

Nos horaires d'ouverture:
lundis 8h-10h30
jeudis 12h-14h30
EOF
                        ,
                            'email' => 'technique-tdtd@actiane.com',
                            'phone' => '01 30 55 44 49',
                            'address' => '13 rue Emile et Charles Pathé',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'lamobiliere' => array(
                            'name' => 'La Mobilière',
                            'retailStore' => 'LA MOBILIÈRE',
                            'languages' => array('fr'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
Votre Conciergerie TO DO TODAY se situe face au restaurant «The Kitchen» 
et à côté de la crèche du Business Park de Terre Bonne.

Nos horaires d'ouverture :
lundis au jeudis
9h30 à 13h30
EOF
                        ,
                            'email' => 'conciergerie.nyon@mobi.ch',
                            'phone' => '022 363 89 92',
                            'address' => '',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'unigestion' => array(
                            'name' => 'UNIGESTION',
                            'retailStore' => 'UNIGESTION SA',
                            'languages' => array('fr'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
Le Conciergerie TO DO TODAY se situe dans un espace dédié face à la grande porte du 2ème étage.
EOF
                        ,
                            'email' => 'unigestion@todotoday.com',
                            'phone' => '022 704 45 00',
                            'address' => '8c Avenue de Champel 1206 Genève',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'barclays' => array(
                            'name' => 'BARCLAYS',
                            'retailStore' => 'BARCLAYS BANK',
                            'languages' => array('fr', 'en'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
Votre Conciergerie TO DO TODAY se situe près de la cafétéria.

Nos horaires d'ouverture:  
lundis et jeudis
10h00 - 14h00.
EOF
                        ,
                            'email' => 'barclays@todotoday.com',
                            'phone' => '022 819 58 70',
                            'address' => '',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'hugsanssouci' => array(
                            'name' => 'HOPITAUX UNIVERSITAIRES DE GENEVE',
                            'retailStore' => 'HÔPITAUX UNIVERSITAIRES DE GENÈVE',
                            'languages' => array('fr'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
Votre Conciergerie TO DO TODAY se situe au niveau S, près des vestiaires Opéra.

Nos horaires d'ouverture:
lundis, mardis, jeudis et vendredis
 11h45 - 15h45.
EOF
                        ,
                            'email' => 'conciergerie.hugsanssouci@hcuge.ch',
                            'phone' => '022 372 65 74',
                            'address' => '',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'loro' => array(
                            'name' => 'LOTERIE ROMANDE',
                            'retailStore' => 'LOTERIE ROMANDE',
                            'languages' => array('fr'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
Votre Conciergerie TO DO TODAY se situe à côté de la réception de la Loterie Romande.

Nos horaires d'ouverture:
 lundis et jeudis
10h - 14h.
EOF
                        ,
                            'email' => 'loro@todotoday.com',
                            'phone' => '021 348 13 08',
                            'address' => '',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                        'totsa' => array(
                            'name' => 'TOTSA OIL TRADING',
                            'retailStore' => 'TOTSA OIL TRADING',
                            'languages' => array('fr'),
                            'information' => static::$information,
                            'schedule' => '',
                            'quickInfo' => <<<EOF
Votre Conciergerie TO DO TODAY se situe au 3ème étage des locaux TOTSA (côté juridique).

Nos horaires d'ouverture:
 mardis et vendredis
 11h30 - 15h30
EOF
                        ,
                            'email' => 'totsa@todotoday.com',
                            'phone' => '022 710 12 56',
                            'address' => '',
                            'allowRegistration' => true,
                            'allowAdherent' => true,
                        ),
                    )
                );
            }
        }

        return static::$agenciesLinkedSlugs;
    }

    /**
     * @param string $slug
     * @param array  $args
     *
     * @return null|Agency
     * @throws \RuntimeException
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     */
    public function createAgency(string $slug, array $args): ?Agency
    {
        // @codingStandardsIgnoreEnd
        $agency = new Agency();
        /**
         * @var AgencyRepository $repo
         */
        $repo = $this->connection->getRepository(AgencyMicrosoft::class);
        /**
         * @var AgencyMicrosoft $agencyMicrosoft
         */
        if ($agencyMicrosoft = $repo->findByStoreName($args['retailStore'])[0]) {
            if (!$agencyMicrosoft->getIdActiane()) {
                $agencyMicrosoft->setIdActiane($slug);
                $this->connection->persist($agencyMicrosoft)->flush();
            }
            $agencyMapping = $this->connection->getMapping(AgencyMicrosoft::class);
            $agencyMapping->fillEntityInEntityDoctrine($agencyMicrosoft, $agency);
            if ('' === $agency->getLanguagesString()) {
                $agency->setLanguages($args['languages']);
            }

            //            $logoPath =
            //                $this->kernel
            //                    ->locateResource(
            //                        '@TodotodayCoreBundle/Resources/public/img/header/' . $slug . '.png'
            //                    );

            //            $imageHeader = clone $this->getMediaBackground();
            //            $imageHeader->setName('logo_header_agency_' . $slug);
            //            $imageHeader->setBinaryContent($logoPath);

            /**
             * @var Media $imageHeader
             */
            $imageHeader = $this->getReference('img-header-' . $slug);

            /**
             * @var Media $imageBackground
             */
            $imageBackground = $this->getReference('img-cabriole');

            $epa = substr($agency->getDataAreaId(), 0, 3);
            $method = 'getPayments' . $epa;
            if (!method_exists($this, $method)) {
                throw new \InvalidArgumentException('Method for getting payments method don\'t exist for ' . $epa);
            }
            $paymentsAvailable = $this->$method();

            $agency
                ->setName($args['name'])
                ->setDefaultLanguage($args['languages'][0])
                ->setAuthorizedPaymentMethods(
                    $paymentsAvailable
                )
                ->setInformation($args['information'])
                ->setSchedule($args['schedule'])
                ->setQuickInfo($args['quickInfo'])
                ->setEmail($args['email'])
                ->setPhone($args['phone'])
                ->setAddress($args['address'])
                ->setEnabledFront(true)
                ->setAllowRegistration($args['allowRegistration'])
                ->setAllowAdherent($args['allowAdherent'])
                ->setHeaderImg($imageHeader)
                ->addModule(AgencyModuleEnum::SOCIAL)
                ->setImageBackground($imageBackground)
                ->setImagePresentation($imageBackground)
                ->setSlug($slug);

            $this->setReference('agency_' . $slug, $agency);

            return $agency;
        }
        echo 'Retail store not found for ' . $slug;

        return null;
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return array(
            LoadContextData::class,
            LoadCategoryData::class,
            LoadMediaData::class,
        );
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     *
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \RuntimeException
     * @throws \Exception
     */
    public function load(ObjectManager $manager): void
    {
        foreach (static::getAgenciesLinkedSlug($this->kernel->getEnvironment()) as $slug => $args) {
            if ($agency = $this->createAgency($slug, $args)) {
                $manager->persist($agency);
                $this->oneDriveManager->createFolderInSepaFolder($agency->getSlug());
            }
        }
        $manager->flush();
    }

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     *
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     */
    public function setContainer(ContainerInterface $container = null): void
    {
        if ($container) {
            $this->connection = $container->get('actiane.api_connector.connection.microsoft');
            $this->paymentMethods = $container->get('actiane.payment.method_manager');
            $this->kernel = $container->get('kernel');
            $this->media = $container->get('sonata.media.manager.media');
            $this->oneDriveManager = $container->get('todotoday.payment.onedrive_manager');
        }
    }

    /**
     * @return array
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function getPaymentsECH(): array
    {
        return array(
            PaymentTypeEnum::getNewInstance(PaymentTypeEnum::POSTFINANCE),
            PaymentTypeEnum::getNewInstance(PaymentTypeEnum::STRIPE),
        );
    }

    /**
     * @return array
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function getPaymentsEPA(): array
    {
        return array(
            PaymentTypeEnum::getNewInstance(PaymentTypeEnum::SEPA),
            PaymentTypeEnum::getNewInstance(PaymentTypeEnum::STRIPE),
        );
    }

    /**
     * @return array
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function getPaymentsEPC(): array
    {
        return $this->getPaymentsEPA();
    }

    /**
     * @return array
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function getPaymentsERA(): array
    {
        return $this->getPaymentsEPA();
    }

    /**
     * @return array
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function getPaymentsEUS(): array
    {
        return array(
            PaymentTypeEnum::getNewInstance(PaymentTypeEnum::PAYPAL),
        );
    }
}
