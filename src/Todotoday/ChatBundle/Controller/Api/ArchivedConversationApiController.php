<?php

declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: alexandre_vinet
 * Date: 10/11/17
 * Time: 16:35
 *
 * @category   TodoToday - WebFront
 *
 * @package    Todotoday\ChatBundle
 *
 * @subpackage Todotoday\ChatBundle\Controller\Api
 *
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */

namespace Todotoday\ChatBundle\Controller\Api;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Todotoday\ChatBundle\Form\ConversationType;

/**
 * Class ArchivedConversationApiController
 *
 * @Rest\NamePrefix("api_chat.archived_conversation.")
 * @Rest\Prefix("archived/conversation")
 */
class ArchivedConversationApiController extends FOSRestController
{
    /**
     * Get single account by id
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Post all message from a chat conversation",
     *     description="Post",
     *     input={
     *           "class"="Todotoday\ChatBundle\Form\ConversationType"
     *     },
     *     output={
     *           "class"="Todotoday\ChatBundle\Entity\Conversation"
     *     },
     *     views={"default","Messages"},
     *     section="Messages"
     * )
     *
     * @param Request $request
     *
     * @return Response
     * @throws \LogicException
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     */
    public function postAction(Request $request): Response
    {
        $data = $form = $this->createForm(ConversationType::class, null, ['csrf_protection' => false]);
        $form->submit($request->request->all());

        if ($form->isValid()) {
            $data = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($data);
            $em->flush();
        }

        $view = $this->view($data);
        $view->getContext()->addGroups(['Default'])->setSerializeNull(true);

        return $this->handleView($view);
    }
}
