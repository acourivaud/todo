<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: alexandre_vinet
 * Date: 10/11/17
 * Time: 17:14
 *
 * @category   TodoToday - WebFront
 *
 * @package    Todotoday\ChatBundle
 *
 * @subpackage Todotoday\ChatBundle\Tests\Controller\Api
 *
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */

namespace Todotoday\ChatBundle\Tests\Controller\Api;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAccountData;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyData;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class ArchivedConversationApiControllerTest
 */
class ArchivedConversationApiControllerTest extends WebTestCase
{
    public function testPostAction()
    {
        /** @var Account $userRoot */
        $userRoot = static::getFRR()->getReference('root');
        /** @var Account $userAdmin */
        $userAdmin = static::getFRR()->getReference('admin');
        /** @var Agency $agency */
        $agency = static::getFRR()->getReference('agency_demo1');
        $this->loginAs($userRoot, 'api');
        $client = $this->makeClient();

        $client->request(
            'POST',
            $this->getUrl('api_chat.archived_conversation.post'),
            [
                'messages' => [
                    [
                        'content' => 'toto',
                        'date' => '2018-01-05T09:41:56.412Z',
                        'author' => $userRoot->getEmail(),
                    ],
                    [
                        'content' => 'toto2',
                        'date' => '2018-01-05T10:41:56.412Z',
                        'author' => $userAdmin->getEmail(),
                    ],
                ],
                'service' => 'toto',
                'accounts' => [$userRoot->getEmail(), $userAdmin->getEmail()],
                'agency' => $agency->getSlug(),

                'startAt' => '2018-01-05T09:40:56.412Z',
                'endAt' => '2018-01-05T09:55:56.412Z',
            ]
        );
        $response = $client->getResponse();
        static::assertNotNull($response);
        $this->isSuccessful($response);
        $content = json_decode($response->getContent());
        static::assertNotEmpty($content);
    }

    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return [LoadAccountData::class, LoadAgencyData::class];
    }
}
