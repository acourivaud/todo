<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 04/01/18
 * Time: 16:52
 *
 * @category   tdtd
 *
 * @package    Todotoday\ChatBundle\Command
 *
 * @subpackage Todotoday\ChatBundle\Command
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\ChatBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Todotoday\AccountBundle\Entity\Partner;

/**
 * Class CreateActianeApiAccountCommand
 */
class CreateActianeApiAccountCommand extends ContainerAwareCommand
{
    /**
     * configure
     *
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function configure(): void
    {
        $this->setName('tdtd:actiane:account:create')
            ->setDescription('Create actiane api account');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $account = (new Partner())
            ->setUsername('actiane')
            ->setEmail('technique-tdtd@actiane.com')
            ->setPlainPassword('2018@acTiaNe!')
            ->setRoles(['ROLE_ARCHIVED_CONVERSATION'])
            ->setEnabled(true)
        ;

        $em = $this->getContainer()->get('doctrine')->getManager();
        $em->persist($account);
        $em->flush();
        $output->writeln('Account Created');
    }
}
