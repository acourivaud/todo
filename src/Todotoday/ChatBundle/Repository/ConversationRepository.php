<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 04/01/18
 * Time: 15:18
 *
 * @category   tdtd
 *
 * @package    Todotoday\ChatBundle\Repository
 *
 * @subpackage Todotoday\ChatBundle\Repository
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\ChatBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\ChatBundle\Entity\Conversation;

/**
 * Class ConversationRepository
 */
class ConversationRepository extends EntityRepository
{
    /**
     * @param Adherent $adherent
     *
     * @return Conversation[]
     */
    public function getConverationByAdherent(Adherent $adherent): array
    {
        return $this->createQueryBuilder('c')
            ->join('c.accounts', 'linkAccounts')
            ->where('linkAccounts =:accounts')
            ->setParameter('accounts', $adherent)
            ->getQuery()
            ->getResult();
    }
}
