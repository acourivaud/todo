<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 04/01/18
 * Time: 15:20
 *
 * @category   tdtd
 *
 * @package    Todotoday\ChatBundle\Repository
 *
 * @subpackage Todotoday\ChatBundle\Repository
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\ChatBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class MessageRepository
 */
class MessageRepository extends EntityRepository
{
}
