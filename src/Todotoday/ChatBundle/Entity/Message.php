<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: alexandre_vinet
 * Date: 10/11/17
 * Time: 13:39
 *
 * @category   TodoToday - WebFront
 *
 * @package    Todotoday\ChatBundle
 *
 * @subpackage Todotoday\ChatBundle\Entity
 *
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */

namespace Todotoday\ChatBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Todotoday\AccountBundle\Entity\Account;

/**
 * Class Message
 *
 * @ORM\Entity(repositoryClass="Todotoday\ChatBundle\Repository\MessageRepository")
 * @ORM\Table(schema="chat")
 * @Serializer\ExclusionPolicy("all")
 */
class Message
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", options={"unsigned"=true})
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @var Conversation
     *
     * @ORM\ManyToOne(targetEntity="Todotoday\ChatBundle\Entity\Conversation", inversedBy="messages")
     */
    private $conversation;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="string")
     * @Serializer\Expose()
     */
    private $content;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     * @Serializer\Expose()
     */
    private $date;

    /**
     * @var Account
     *
     * @ORM\ManyToOne(targetEntity="Todotoday\AccountBundle\Entity\Account")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @Serializer\Expose()
     */
    private $author;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Message
     */
    public function setContent(?string $content): Message
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Message
     */
    public function setDate(?\DateTime $date): Message
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate(): ?\DateTime
    {
        return $this->date;
    }

    /**
     * Set conversation
     *
     * @param Conversation $conversation
     *
     * @return Message
     */
    public function setConversation(Conversation $conversation = null): Message
    {
        $this->conversation = $conversation;

        return $this;
    }

    /**
     * Get conversation
     *
     * @return Conversation
     */
    public function getConversation(): ?Conversation
    {
        return $this->conversation;
    }

    /**
     * Set author
     *
     * @param Account $author
     *
     * @return Message
     */
    public function setAuthor(Account $author = null): Message
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return Account
     */
    public function getAuthor(): ?Account
    {
        return $this->author;
    }
}
