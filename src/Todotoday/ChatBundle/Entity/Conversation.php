<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: alexandre_vinet
 * Date: 10/11/17
 * Time: 13:39
 *
 * @category   TodoToday - WebFront
 *
 * @package    Todotoday\ChatBundle
 *
 * @subpackage Todotoday\ChatBundle\Entity
 *
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */

namespace Todotoday\ChatBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class Conversation
 *
 * @ORM\Entity(repositoryClass="Todotoday\ChatBundle\Repository\ConversationRepository")
 * @ORM\Table(schema="chat")
 * @Serializer\ExclusionPolicy("all")
 */
class Conversation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", options={"unsigned"=true})
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @var Collection|Message[]
     *
     * @ORM\OneToMany(targetEntity="Todotoday\ChatBundle\Entity\Message", mappedBy="conversation", cascade={"PERSIST"})
     * @ORM\OrderBy({"date" = "ASC"})
     * @Serializer\Expose()
     */
    private $messages;

    /**
     * @var Collection|Account[]
     *
     * @ORM\ManyToMany(targetEntity="Todotoday\AccountBundle\Entity\Account")
     * @ORM\JoinTable(
     *     schema="chat",
     *     name="link_account_conversation",
     * )
     * @Serializer\Expose()
     */
    private $accounts;

    /**
     * @var Agency
     *
     * @ORM\ManyToOne(targetEntity="Todotoday\CoreBundle\Entity\Agency")
     */
    private $agency;

    /**
     * @var string
     *
     * @ORM\Column(name="service", type="string")
     * @Serializer\Expose()
     */
    private $service;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_at", type="datetime")
     * @Serializer\Expose()
     */
    private $startAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_at", type="datetime")
     * @Serializer\Expose()
     */
    private $endAt;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->messages = new ArrayCollection();
        $this->accounts = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set service
     *
     * @param string $service
     *
     * @return Conversation
     */
    public function setService(?string $service): Conversation
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return string
     */
    public function getService(): ?string
    {
        return $this->service;
    }

    /**
     * Set startAt
     *
     * @param \DateTime $startAt
     *
     * @return Conversation
     */
    public function setStartAt(?\DateTime $startAt): Conversation
    {
        $this->startAt = $startAt;

        return $this;
    }

    /**
     * Get startAt
     *
     * @return \DateTime
     */
    public function getStartAt(): ?\DateTime
    {
        return $this->startAt;
    }

    /**
     * Set endAt
     *
     * @param \DateTime $endAt
     *
     * @return Conversation
     */
    public function setEndAt(?\DateTime $endAt): Conversation
    {
        $this->endAt = $endAt;

        return $this;
    }

    /**
     * Get endAt
     *
     * @return \DateTime
     */
    public function getEndAt(): ?\DateTime
    {
        return $this->endAt;
    }

    /**
     * Add message
     *
     * @param Message $message
     *
     * @return Conversation
     */
    public function addMessage(Message $message): Conversation
    {
        $this->messages[] = $message;
        $message->setConversation($this);

        return $this;
    }

    /**
     * Remove message
     *
     * @param Message $message
     */
    public function removeMessage(Message $message): void
    {
        $this->messages->removeElement($message);
        $message->setConversation();
    }

    /**
     * Get messages
     *
     * @return Collection|Message[]
     */
    public function getMessages(): Collection
    {
        return $this->messages;
    }

    /**
     * Add account
     *
     * @param Account $account
     *
     * @return Conversation
     */
    public function addAccount(Account $account): Conversation
    {
        $this->accounts[] = $account;

        return $this;
    }

    /**
     * Remove account
     *
     * @param Account $account
     */
    public function removeAccount(Account $account): void
    {
        $this->accounts->removeElement($account);
    }

    /**
     * Get accounts
     *
     * @return Collection|Account[]
     */
    public function getAccounts(): Collection
    {
        return $this->accounts;
    }

    /**
     * Set agency
     *
     * @param Agency $agency
     *
     * @return Conversation
     */
    public function setAgency(Agency $agency = null): Conversation
    {
        $this->agency = $agency;

        return $this;
    }

    /**
     * Get agency
     *
     * @return Agency
     */
    public function getAgency(): ?Agency
    {
        return $this->agency;
    }
}
