<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: alexandre_vinet
 * Date: 10/11/17
 * Time: 16:46
 *
 * @category   TodoToday - WebFront
 *
 * @package    Todotoday\ChatBundle
 *
 * @subpackage Todotoday\ChatBundle\Form
 *
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */

namespace Todotoday\ChatBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\ChatBundle\Entity\Message;

/**
 * Class MessageType
 */
class MessageType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @throws \Symfony\Component\Validator\Exception\ConstraintDefinitionException
     * @throws \Symfony\Component\Validator\Exception\InvalidOptionsException
     * @throws \Symfony\Component\Validator\Exception\MissingOptionsException
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'content',
                TextType::class
            )
            ->add(
                'date',
                DateTimeType::class,
                [
                    'widget' => 'single_text',
                    'date_format' => \DateTime::ISO8601,
                ]
            )
            ->add(
                'author',
                EntityType::class,
                [
                    'class' => Account::class,
                    'choice_value' => 'email',
                ]
            );
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults(
                [
                    'data_class' => Message::class,
                ]
            );
    }

    /**
     * @return string
     */
    public function getBlockPrefix(): string
    {
        return '';
    }
}
