<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: alexandre_vinet
 * Date: 20/11/17
 * Time: 16:21
 *
 * @category   TodoToday - WebFront
 *
 * @package    Todotoday\ChatBundle
 *
 * @subpackage Todotoday\ChatBundle\Form
 *
 * @author     Alexandre Vinet <contact@alexandrevinet.fr>
 */

namespace Todotoday\ChatBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\ChatBundle\Entity\Conversation;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class ConversationType
 */
class ConversationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'messages',
                CollectionType::class,
                [
                    'entry_type' => MessageType::class,
                    'allow_add' => true,
                    'allow_delete' => true,
                ]
            )
            ->add(
                'accounts',
                EntityType::class,
                [
                    'class' => Account::class,
                    'multiple' => true,
                    'choice_value' => 'email',
                ]
            )
            ->add(
                'agency',
                EntityType::class,
                [
                    'class' => Agency::class,
                    'choice_value' => 'slug',
                ]
            )
            ->add('service', TextType::class)
            ->add(
                'startAt',
                DateTimeType::class,
                [
                    'widget' => 'single_text',
                    'date_format' => \DateTime::ISO8601,
                ]
            )
            ->add(
                'endAt',
                DateTimeType::class,
                [
                    'widget' => 'single_text',
                    'date_format' => \DateTime::ISO8601,
                ]
            );
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults(
                [
                    'data_class' => Conversation::class,
                ]
            );
    }

    /**
     * @return string
     */
    public function getBlockPrefix(): string
    {
        return '';
    }
}
