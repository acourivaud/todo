<?php declare(strict_types=1);

namespace Todotoday\ChatBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class TodotodayChatBundle extends Bundle
{
}
