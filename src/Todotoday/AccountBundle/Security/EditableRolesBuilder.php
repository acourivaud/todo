<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 02/02/2017
 * Time: 12:27
 */

namespace Todotoday\AccountBundle\Security;

use Sonata\AdminBundle\Admin\Pool;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * Class EditableRolesBuilder
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle\Security
 * @subpackage Todotoday\AccountBundle\Security
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 * @SuppressWarnings(PHPMD.UnusedLocalVariable)
 */
class EditableRolesBuilder
{
    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @var AuthorizationCheckerInterface
     */
    protected $authorizationChecker;

    /**
     * @var Pool
     */
    protected $pool;

    /**
     * @var array[]
     */
    protected $rolesHierarchy;

    /**
     * @param TokenStorageInterface         $tokenStorage
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param Pool                          $pool
     * @param array[]                       $rolesHierarchy
     *
     * @throws \InvalidArgumentException
     */
    public function __construct(
        TokenStorageInterface $tokenStorage,
        AuthorizationCheckerInterface $authorizationChecker,
        Pool $pool,
        array $rolesHierarchy = array()
    ) {
        if (!$tokenStorage instanceof TokenStorageInterface) {
            throw new \InvalidArgumentException(
                'Argument 1 should be an instance of TokenStorageInterface'
            );
        }
        if (!$authorizationChecker instanceof AuthorizationCheckerInterface
        ) {
            throw new \InvalidArgumentException(
                'Argument 2 should be an instance of AuthorizationCheckerInterface'
            );
        }
        $this->tokenStorage = $tokenStorage;
        $this->authorizationChecker = $authorizationChecker;
        $this->pool = $pool;
        $this->rolesHierarchy = $rolesHierarchy;
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        $roles = array();
        $rolesReadOnly = array();
        if (!$this->tokenStorage->getToken()) {
            return array($roles, $rolesReadOnly);
        }
        $rolesReadOnly = $this->getRolesReadOnly();

        // get roles from the service container
        /** @var string[] $rolesHierarchy */
        foreach ($this->rolesHierarchy as $name => $rolesHierarchy) {
            $roles[$name] = $name;
        }

        return array($roles, $rolesReadOnly);
    }

    /**
     * Do getRolesReadOnly
     *
     * @return array
     */
    protected function getRolesReadOnly()
    {
        $rolesReadOnly = array();

        // get roles from the Admin classes
        foreach ($this->pool->getAdminServiceIds() as $id) {
            try {
                $admin = $this->pool->getInstance($id);
            } catch (\Exception $e) {
                continue;
            }
            $isMaster = $admin->isGranted('MASTER');
            $securityHandler = $admin->getSecurityHandler();
            // TODO get the base role from the admin or security handler
            $baseRole = $securityHandler->getBaseRole($admin);
            if (empty($baseRole)) { // the security handler related to the admin does not provide a valid string
                continue;
            }

            $roles = array_keys($admin->getSecurityInformation());
            foreach ($roles as $role) {
                $role = sprintf($baseRole, $role);
                if ($isMaster) {
                    // if the user has the MASTER permission, allow to grant access the admin roles to other users
                    $roles[$role] = $role;
                } elseif ($this->authorizationChecker->isGranted($role)) {
                    // although the user has no MASTER permission, allow the currently logged in user to view the role
                    $rolesReadOnly[$role] = $role;
                }
            }
        }

        return $rolesReadOnly;
    }
}
