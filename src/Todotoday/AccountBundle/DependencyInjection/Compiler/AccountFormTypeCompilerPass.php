<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 16/01/2017
 * Time: 09:21
 */

namespace Todotoday\AccountBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;

/**
 * Class AccountFormTypeCompilerPass
 *
 * @category   Todo-Todev
 * @package    Todotoday\CoreBundle
 * @subpackage Todotoday\CoreBundle\DependencyInjection\Compiler
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class AccountFormTypeCompilerPass implements CompilerPassInterface
{
    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     *
     * @throws InvalidArgumentException
     * @throws ServiceNotFoundException
     */
    public function process(ContainerBuilder $container)
    {
        $accountManager = $container->getDefinition('todotoday.account.services.account_manager');
        $services = $container->findTaggedServiceIds('account_form_type');

        /** @var array $tagAttributes */
        foreach ($services as $id => $tagAttributes) {
            foreach ($tagAttributes as $attributes) {
                if (!array_key_exists('key', $attributes)) {
                    throw new InvalidArgumentException(
                        'You need to specify attribute key in tag named account_form_type.'
                    );
                }
                $accountManager->addMethodCall(
                    'addFormType',
                    array(
                        $attributes['key'],
                        $container->getDefinition($id)->getClass(),
                        $attributes['context'] ?? 'default',
                    )
                );
            }
        }
    }
}
