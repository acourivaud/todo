<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 26/05/2017
 * Time: 09:03
 */

namespace Todotoday\AccountBundle\Form\Api;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Todotoday\AccountBundle\Entity\Api\Microsoft\ContactPerson;

/**
 * Class AccountContactPersonType
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Form\Api
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class AccountContactPersonType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstName')
            ->add('lastName')
            ->add('middleName')
            ->add('primaryAddressCity')
            ->add('primaryAddressDescription')
            ->add('birthDay')
            ->add('birthMonth')
            ->add('birthYear')
            ->add('canOrder')
            ->add('contactInformationLanguageId')
            ->add('contactPersonName')
            ->add('contactPersonPartyNumber')
            ->add('contactPersonPartyType')
            ->add('contactType')
            ->add('employmentJobFunctionName')
            ->add('employmentJobTitle')
            ->add('employmentProfession')
            ->add('endJobDay')
            ->add('endJobMonth')
            ->add('endJobYear')
            ->add('formattedPrimaryAddress')
            ->add('gender')
            ->add('governmentIdentificationNumber')
            ->add('hobbies')
            ->add('isInactive')
            ->add('isPrimaryPhoneNumberMobile')
            ->add('isVIP')
            ->add('lastEditDateTime')
            ->add('nameValidFrom')
            ->add('nameValidTo')
            ->add('notes')
            ->add('organizationIdNumber')
            ->add('primaryAddressCityInKana')
            ->add('primaryAddressCountryRegionId')
            ->add('primaryAddressCountyId')
            ->add('primaryAddressDistrictName')
            ->add('primaryAddressLocationRoles')
            ->add('primaryAddressStateId')
            ->add('primaryAddressStreet')
            ->add('primaryAddressStreetInKana')
            ->add('primaryAddressTimeZone')
            ->add('primaryTelexPurpose')
            ->add('primaryTelexDescription')
            ->add('primaryPhoneNumberPurpose')
            ->add('primaryPhoneNumberExtension')
            ->add('primaryPhoneNumberDescription')
            ->add('primaryFaxNumberPurpose')
            ->add('primaryFaxNumberExtension')
            ->add('primaryFaxNumberDescription')
            ->add('primaryEmailAddress')
            ->add('primaryEmailAddressPurpose')
            ->add('primaryEmailAddressDescription')
            ->add('primaryAddressZipCode')
            ->add('primaryAddressValidTo')
            ->add('primaryAddressValidFrom')
            ->add('primaryPhoneNumber')
        ;
    }

    /**
     * {@inheritdoc}
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            array(
                'data_class' => ContactPerson::class,
                'id_required' => false,
            )
        );
    }

    /**
     * Do getBlockPrefix
     *
     * @return string
     */
    public function getBlockPrefix(): string
    {
        return '';
    }
}
