<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 26/05/2017
 * Time: 09:51
 */

namespace Todotoday\AccountBundle\Form\Api;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class AccountContactPersonsType
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle\Form
 * @subpackage Todotoday\AccountBundle\Form\Api
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class AccountContactPersonsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add(
            'contactPersons',
            CollectionType::class,
            array(
                'entry_type' => AccountContactPersonType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'allow_extra_fields' => true,
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return '';
    }
}
