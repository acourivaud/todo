<?php declare(strict_types=1);
/**
 * PHP version 7
 */

namespace Todotoday\AccountBundle\Form\Api;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\Exception\AccessException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Validator\Constraints\NotBlank;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\AccountBundle\Enum\AdherentTypeEnum;
use Todotoday\AccountBundle\Form\Transformer\CustomerGroupTransformer;

/**
 * Class AccountType
 *
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Form\Api
 */
class AccountType extends AbstractType
{
    /**
     * @var AuthorizationChecker
     */
    private $authorization;

    /**
     * AccountType constructor.
     *
     * @param AuthorizationChecker $authorization
     */
    public function __construct(AuthorizationChecker $authorization)
    {
        $this->authorization = $authorization;
    }

    /**
     * {@inheritdoc}
     * @throws \Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException
     * @throws \Symfony\Component\Validator\Exception\ConstraintDefinitionException
     * @throws \Symfony\Component\Validator\Exception\InvalidOptionsException
     * @throws \Symfony\Component\Validator\Exception\MissingOptionsException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Form\Exception\InvalidArgumentException
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstName')
            ->add('middleName')
            ->add('lastName')
            ->add(
                'email',
                EmailType::class,
                array('label' => 'form.email', 'translation_domain' => 'FOSUserBundle')
            );
        if ('adherent' === $options['type'] && $this->authorization->isGranted('ROLE_MICROSOFT')
        ) {
            $builder->add(
                'customerAccount',
                TextType::class,
                array(
                    'constraints' => array(
                        new NotBlank(),
                    ),
                )
            )
                ->add(
                    'customerGroup',
                    ChoiceType::class,
                    array(
                        'choices' => AdherentTypeEnum::toArray(),
                        'empty_data' => AdherentTypeEnum::ADH,
                        'constraints' => array(
                            new NotBlank(),
                        ),
                    )
                );

            $builder->get('customerGroup')
                ->addModelTransformer(new CustomerGroupTransformer());
        }
    }

    /**
     * {@inheritdoc}
     * @throws AccessException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            array(
                'data_class' => Account::class,
                'csrf_protection' => false,
                'type' => null,
                'validation_groups' => 'emailWhiteList',
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return '';
    }
}
