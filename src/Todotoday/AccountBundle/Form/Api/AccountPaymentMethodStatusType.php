<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 09/06/2017
 * Time: 15:10
 */

namespace Todotoday\AccountBundle\Form\Api;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\AccountBundle\Enum\PaymentStatusEnum;

/**
 * Class AccountPaymentMethodStatusType
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Form\Api
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class AccountPaymentMethodStatusType extends AbstractType
{
    /**
     * {@inheritdoc]
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add(
            'paymentMethodStatus',
            ChoiceType::class,
            array(
                'choices' => PaymentStatusEnum::getChoices(),
                'choice_value' => function (?PaymentStatusEnum $statusEnum) {
                    if (!$statusEnum) {
                        return $statusEnum;
                    }

                    return $statusEnum->get();
                },
            )
        );
    }

    /**
     * {@inheritdoc]
     *
     * @param OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            array(
                'data_class' => Account::class,
                'validation_groups' => 'emailWhiteList',
            )
        );
    }

    /**
     * {@inheritdoc]
     *
     * @return string
     */
    public function getBlockPrefix(): string
    {
        return '';
    }
}
