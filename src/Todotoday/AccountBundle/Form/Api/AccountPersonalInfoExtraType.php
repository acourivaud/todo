<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 24/05/2017
 * Time: 11:24
 */

namespace Todotoday\AccountBundle\Form\Api;

use Todotoday\AccountBundle\Form\AccountPersonalInfoExtraType as BaseAccountPersonalInfoExtraType;

/**
 * Class AccountPersonalInfoExtraType
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Form\Api
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class AccountPersonalInfoExtraType extends BaseAccountPersonalInfoExtraType
{
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return '';
    }
}
