<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 24/05/2017
 * Time: 11:22
 */

namespace Todotoday\AccountBundle\Form\Api;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Todotoday\AccountBundle\Form\AccountPersonalInfoType as BaseAccountPersonalInfoType;

/**
 * Class AccountPersonalInfoType
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Form\Api
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class AccountPersonalInfoType extends BaseAccountPersonalInfoType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @throws \Doctrine\DBAL\DBALException
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\Validator\Exception\ConstraintDefinitionException
     * @throws \Symfony\Component\Validator\Exception\InvalidOptionsException
     * @throws \Symfony\Component\Validator\Exception\MissingOptionsException
     * @throws \UnexpectedValueException
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $options['is_for_api'] = true;
        parent::buildForm($builder, $options);
        $builder->add('plainPassword');
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return '';
    }
}
