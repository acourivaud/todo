<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 23/03/2017
 * Time: 09:12
 */

namespace Todotoday\AccountBundle\Form;

use Actiane\ToolsBundle\Services\AssetManager;
use ISOCodes\ISO3166_1\Model\ISO3166_1Interface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\OptionsResolver\Exception\AccessException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\AccountBundle\Entity\Api\Microsoft\MandatoryField;
use Todotoday\AccountBundle\Enum\AccountTitleEnum;
use Todotoday\AccountBundle\Enum\PersonGenderEnum;
use Todotoday\AccountBundle\Form\Type\YesNoType;
use Todotoday\AccountBundle\Repository\Api\Microsoft\EnterpriseRepository;
use Todotoday\AccountBundle\Repository\Api\Microsoft\MandatoryFieldRepository;
use Todotoday\CoreBundle\Services\DomainContextManager;
use ISOCodes\ISO3166_1\Adapter\Json;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Actiane\ToolsBundle\Form\USStateType;
use Todotoday\AccountBundle\Constraints\EmployeeDate;
use Symfony\Component\Intl\Intl;

/**
 * Class AccountPersonalInfoType
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Form
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 *
 * @SuppressWarnings(PHPMD)
 */
class AccountPersonalInfoType extends AbstractType implements AccountMandatoryFlowTypeInterface
{
    /**
     * @var MandatoryFieldRepository
     */
    private $mandatoryFieldRepository;

    /**
     * @var EnterpriseRepository
     */
    private $enterpriseRepository;

    /**
     * @var DomainContextManager
     */
    private $domainContext;

    /**
     * @var RequestStack
     */
    private $request;

    /**
     * AccountMandatoryFieldsType constructor.
     *
     * @param MandatoryFieldRepository $mandatoryFieldRepository
     * @param EnterpriseRepository     $enterpriseRepository
     * @param DomainContextManager     $domainContext
     * @param RequestStack             $request
     * @param AssetManager             $assetManager
     */
    public function __construct(
        MandatoryFieldRepository $mandatoryFieldRepository,
        EnterpriseRepository $enterpriseRepository,
        DomainContextManager $domainContext,
        RequestStack $request,
        AssetManager $assetManager
    ) {
        $this->mandatoryFieldRepository = $mandatoryFieldRepository;
        $this->enterpriseRepository = $enterpriseRepository;
        $this->domainContext = $domainContext;
        $this->request = $request;
        $assetManager->addJavascript('build/AccountState.js');
    }

    /**
     * Do getFieldNeeded
     *
     * @return array
     */
    public static function getFieldNeeded(): array
    {
        return array(
            'contactPrincipals' => true,
        );
    }

    /**
     * {@inheritdoc}
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\Validator\Exception\ConstraintDefinitionException
     * @throws \Symfony\Component\Validator\Exception\InvalidOptionsException
     * @throws \Symfony\Component\Validator\Exception\MissingOptionsException
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $adapter = new Json();
        $countries = $adapter->getAll();
        $locale = $this->request->getCurrentRequest()->getLocale();
        $agency = $this->domainContext->getCurrentContext()->getAgency();

        usort(
            $countries,
            function (ISO3166_1Interface $iso1, ISO3166_1Interface $iso2) use ($locale) {
                return $iso1->getName($locale) > $iso2->getName($locale);
            }
        );

        $choices = AccountTitleEnum::getChoices();

        if (is_array($choices) && isset($choices['Miss'])) {
            if ($agency) {
                if ($agency->getDataAreaId() === 'ECA') {
                    unset($choices['Miss']);
                }
            }
        }

        $choiceLanguage = [];

        if ($agency) {
            foreach ($agency->getLanguages() as $language) {
                $choiceLanguage[ucfirst(Intl::getLanguageBundle()->getLanguageName($language))] = $language;
            }
        }

        $builder
            ->add(
                'title',
                ChoiceType::class,
                array(
                    'choices' => $choices,
                    'choice_value' => function (?AccountTitleEnum $accountTitleEnum) {
                        if (!$accountTitleEnum) {
                            return null;
                        }

                        return $accountTitleEnum->get();
                    },
                    'choice_attr' => function ($value, $key, $index) use ($locale, $agency) {
                        if (($locale !== 'fr') && ($key === 'Miss')) {
                            return array('style' => 'display: none;');
                        }

                        return array();
                    },
                    'label' => 'title',
                    'placeholder' => 'form.account.title_default_choice',
                    'translation_domain' => 'todotoday',
                )
            )
            ->add(
                'lastName',
                TextType::class,
                array(
                    'label' => 'lastname',
                    'constraints' => array(new NotBlank()),
                    'attr' => array(
                        'placeholder' => 'lastname',
                    ),
                    'translation_domain' => 'todotoday',
                )
            )
            ->add(
                'firstName',
                TextType::class,
                array(
                    'label' => 'firstname',
                    'constraints' => array(new NotBlank()),
                    'attr' => array(
                        'placeholder' => 'firstname',
                    ),
                    'translation_domain' => 'todotoday',

                )
            )
            ->add(
                'middleName',
                TextType::class,
                array(
                    'label' => 'middleName',
                    'attr' => array(
                        'placeholder' => 'middlename',
                    ),
                    'translation_domain' => 'todotoday',
                    'required' => false,
                )
            )
            ->add(
                'addressCountryRegionId',
                CountryType::class,
                array(
                    'label' => 'country',
                    //                    'choices' => $countryChoices,
                    'choices' => $countries,
                    'choice_value' => function ($iso) {
                        if (!($iso instanceof ISO3166_1Interface)) {
                            return;
                        }

                        /** @var ISO3166_1Interface $iso */

                        return $iso->getAlpha3();
                    },
                    'choice_label' => function (ISO3166_1Interface $iso) use ($locale) {
                        return $iso->getName($locale);
                    },
                    'choice_attr' => function ($iso) {
                        /** @var ISO3166_1Interface $iso */

                        return array(
                            'data-alpha2-value' => $iso->getAlpha2(),
                        );
                    },
                    'translation_domain' => 'todotoday',
                    'preferred_choices' => function ($iso) {
                        /** @var ISO3166_1Interface $iso */

                        return \in_array($iso->getAlpha3(), array('FRA', 'CAN', 'DEU', 'USA', 'CHE'), true);
                    },
                    'placeholder' => 'account.choose_country',
                    'is_vue_template' => $options['is_vue_template'],
                )
            )
            ->add(
                'addressState',
                USStateType::class,
                array(
                    'label' => 'state',
                    'translation_domain' => 'todotoday',
                    'is_vue_template' => $options['is_vue_template'],
                )
            )
            ->add(
                'addressStreet',
                TextType::class,
                array(
                    'label' => 'street',
                    'translation_domain' => 'todotoday',
                    'is_vue_template' => $options['is_vue_template'],
                )
            )
            ->add(
                'addressCity',
                TextType::class,
                array(
                    'label' => 'city',
                    'translation_domain' => 'todotoday',
                    'is_vue_template' => $options['is_vue_template'],
                )
            )
//        $builder->add('primaryAddressStreetInKana');
            ->add(
                'addressZipCode',
                TextType::class,
                array(
                    'label' => 'zipcode',
                    'translation_domain' => 'todotoday',
                    'is_vue_template' => $options['is_vue_template'],
                )
            );

        if (!$options['is_for_api']) {
            $builder->add(
                'phoneDial',
                AccountPhoneDialInfoType::class,
                array(
                    'mapped' => false,
                    'label' => 'form.accout.phone_dial_type',
                    'translation_domain' => 'todotoday',
                    'init_country_with_ip' => $options['init_country_with_ip'],
                )
            );
        } else {
            $builder
                ->add(
                    'phoneExtension',
                    TextType::class,
                    array(
                        'label' => 'signin.phone_extension',
                        'attr' => array(
                            'placeholder' => 'Format : +33',
                        ),
                        'translation_domain' => 'todotoday',
                    )
                )
                ->add(
                    'phone',
                    TextType::class,
                    array(
                        'label' => 'signin.phone',
                        'attr' => array(
                            'placeholder' => 'signin.phone',
                        ),
                        'translation_domain' => 'todotoday',
                    )
                );
        }

        if ($options['is_registration']) {
            $builder->add(
                'language',
                ChoiceType::class,
                [
                    'choices' => $choiceLanguage,
                    'choice_translation_domain' => 'todotoday',
                    'label' => 'signin.language',
                    'translation_domain' => 'todotoday',
                    'data' => $locale,
                ]
            );
        }

        $builder->get('addressCountryRegionId')
            ->addModelTransformer(
                new CallbackTransformer(
                    function ($tagValue) use ($countries) {
                        /** @var ISO3166_1Interface $country */
                        foreach ($countries as $country) {
                            if ($country->getAlpha3() === $tagValue) {
                                return $country;
                            }
                        }
                    },
                    function ($tagsObject) {
                        /** @var ISO3166_1Interface $tagsObject */

                        return $tagsObject->getAlpha3();
                    }
                )
            );

        $this->buildFormEnterprise($builder, $options);

        if ($options['display_optional_fields']) {
            $builder
                ->add(
                    'contactChildren',
                    CollectionVueType::class,
                    array(
                        'entry_type' => AccountContactChildType::class,
                    )
                )
                ->add(
                    'contactAssistants',
                    CollectionVueType::class,
                    array(
                        'entry_type' => AccountContactAssistantType::class,
                    )
                );
        }
        /** @var MandatoryField[] $mandatoryFields */
        if (($agency = $this->domainContext->getCurrentContext()->getAgency())
            && $mandatoryFields = $this->mandatoryFieldRepository->findByAgency($agency)
        ) {
            foreach ($mandatoryFields as $mandatoryField) {
                $method = 'buildForm' . ucfirst($mandatoryField->getFieldName());
                if (method_exists($this, $method)) {
                    $this->$method($builder, $options);
                }
            }
        }
    }

    /**
     * Do buildFormBirthYear
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @return AccountPersonalInfoType
     */
    public function buildFormBirthYear(FormBuilderInterface $builder, array $options): self
    {
        $builder->add('birthYear', IntegerType::class);

        return $this;
    }

    /**
     * Do buildFormCanOrder
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @return AccountPersonalInfoType
     */
    public function buildFormCanOrder(FormBuilderInterface $builder, array $options): self
    {
        $builder->add(
            'canOrder',
            ChoiceType::class,
            array(
                'choices' => array(
                    'Yes' => 'Yes',
                    'No' => 'No',
                ),
            )
        );

        return $this;
    }

    /**
     * Do buildFormPersonMaritalStatus
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @return AccountPersonalInfoType
     */
    public function buildFormPersonMaritalStatus(FormBuilderInterface $builder, array $options): self
    {
        $builder->add(
            'personMaritalStatus',
            ChoiceType::class,
            array(
                'label' => 'form.contact_marital_status',
                'translation_domain' => 'todotoday',
                'choices' => array(
                    'form.contact_marital_none' => 'None',
                    'form.contact_marital_single' => 'Single',
                    'form.contact_marital_married' => 'Married',
                    'form.contact_marital_divorced' => 'Divorced',
                    'form.contact_marital_widowhood' => 'Widowhood',
                    'form.contact_marital_commonlaw' => 'CommonLaw',
                ),
            )
        );

        return $this;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @return AccountPersonalInfoType
     */
    public function buildFormMaritalStatus(FormBuilderInterface $builder, array $options): self
    {
        return $this->buildFormPersonMaritalStatus($builder, $options);
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @return AccountPersonalInfoType
     * @throws \InvalidArgumentException
     * @throws \UnexpectedValueException
     */
    public function buildFormEnterprise(FormBuilderInterface $builder, array $options): self
    {
        if (($agency = $this->domainContext->getCurrentContext()->getAgency())
            && $enterprises = $this->enterpriseRepository->getEnterpriseByAgency($agency)
        ) {
            $choices = [];

            foreach ($enterprises as $enterprise) {
                $choices[$enterprise->getDescription()] = $enterprise->getValue();
            }

            $builder->add(
                'enterprise',
                ChoiceType::class,
                array(
                    'choices' => $choices,
                    'placeholder' => 'form.account.enterprise_default_choice',
                    'label' => 'form.account.enterprise',
                    'translation_domain' => 'todotoday',
                )
            );
        }

        return $this;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @return AccountPersonalInfoType
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function buildFormGender(FormBuilderInterface $builder, array $options): self
    {
        $builder->add(
            'personGender',
            ChoiceType::class,
            array(
                'choices' => PersonGenderEnum::getChoices(),
                'choice_value' => function (?PersonGenderEnum $personGenderEnum) {
                    if (!$personGenderEnum) {
                        return null;
                    }

                    return $personGenderEnum->get();
                },
                'label' => 'sexe',
                'placeholder' => 'form.account.gender_default_choice',
                'translation_domain' => 'todotoday',
            )
        );

        return $this;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @return AccountPersonalInfoType
     */
    public function buildFormChildrenNumber(FormBuilderInterface $builder, array $options): self
    {
        $builder->add(
            'childrenNumber',
            TextType::class,
            array(
                'label' => 'form.account.children_number',
                'translation_domain' => 'todotoday',
            )
        );

        return $this;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @return AccountPersonalInfoType
     */
    public function buildFormPrimaryContactFax(FormBuilderInterface $builder, array $options): self
    {
        $builder->add(
            'primaryContactFax',
            TextType::class,
            array(
                'label' => 'form.account.fax',
                'translation_domain' => 'todotoday',
            )
        );

        return $this;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @return AccountPersonalInfoType
     */
    public function buildFormContactViaApp(FormBuilderInterface $builder, array $options): self
    {
        $builder->add(
            'contactViaApp',
            YesNoType::class,
            array(
                'label' => 'form.account.contact_app',
            )
        );

        return $this;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @return AccountPersonalInfoType
     */
    public function buildFormContactViaMail(FormBuilderInterface $builder, array $options): self
    {
        $builder->add(
            'contactViaMail',
            YesNoType::class,
            array(
                'label' => 'form.account.contact_mail',
            )
        );

        return $this;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @return AccountPersonalInfoType
     */
    public function buildFormContactViaSMS(FormBuilderInterface $builder, array $options): self
    {
        $builder->add(
            'contactViaSms',
            YesNoType::class,
            array(
                'label' => 'form.account.contact_sms',
            )
        );

        return $this;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @return AccountPersonalInfoType
     */
    public function buildFormPersonBirthDay(FormBuilderInterface $builder, array $options): self
    {
        return $this->createBirthDateForm($builder, $options);
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @return AccountPersonalInfoType
     */
    public function buildFormPersonBirthMonth(FormBuilderInterface $builder, array $options): self
    {
        return $this->createBirthDateForm($builder, $options);
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @return AccountPersonalInfoType
     */
    public function buildFormPersonBirthYear(FormBuilderInterface $builder, array $options): self
    {
        return $this->createBirthDateForm($builder, $options);
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @return AccountPersonalInfoType
     */
    public function createBirthDateForm(FormBuilderInterface $builder, array $options): self
    {
        $builder->add(
            'birthDate',
            DateType::class,
            array(
                'label' => 'birth_date',
                'translation_domain' => 'todotoday',
                'years' => range(1900, (int) date('Y')),
            )
        );

        return $this;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @return AccountPersonalInfoType
     */
    public function buildFormPersonAnniversaryDay(FormBuilderInterface $builder, array $options): self
    {
        return $this->createEmployeeBirthDateForm($builder, $options);
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @return AccountPersonalInfoType
     */
    public function buildFormPersonAnniversaryMonth(FormBuilderInterface $builder, array $options): self
    {
        return $this->createEmployeeBirthDateForm($builder, $options);
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @return AccountPersonalInfoType
     */
    public function buildFormPersonAnniversaryYear(FormBuilderInterface $builder, array $options): self
    {
        return $this->createEmployeeBirthDateForm($builder, $options);
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @return AccountPersonalInfoType
     */
    public function createEmployeeBirthDateForm(FormBuilderInterface $builder, array $options): self
    {
        $builder->add(
            'employeeBrithDate',
            DateType::class,
            array(
                'label' => 'signin.society_entry',
                'translation_domain' => 'todotoday',
                'years' => range(1900, (int) date('Y')),
                'constraints' => array(new EmployeeDate()),
            )
        );

        return $this;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @return AccountPersonalInfoType
     * @throws \UnexpectedValueException
     */
    public function buildFormEmploymentJobFunctionName(FormBuilderInterface $builder, array $options): self
    {
        if ($this->domainContext->getCurrentContext()->getAgency()->getDataAreaId() !== 'EUS') {
            $builder->add(
                'contactPrincipal',
                AccountJobFunctionName::class,
                array(
                    'label' => 'account.information_pro',
                    'translation_domain' => 'todotoday',
                )
            );
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     * @throws AccessException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            array(
                'data_class' => Account::class,
                'validation_groups' => 'emailWhiteList',
                'display_optional_fields' => false,
                'label' => 'account.personal_info',
                'translation_domain' => 'todotoday',
                'not_adherent' => false,
                'init_country_with_ip' => false,
                'is_for_api' => false,
                'is_registration' => false,
            )
        );
    }

    /**
     * @param FormView      $view
     * @param FormInterface $form
     * @param array         $options
     */
    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        $view->vars['init_country_with_ip'] = $options['init_country_with_ip'];
    }
}
