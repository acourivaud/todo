<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 23/03/2017
 * Time: 09:12
 */

namespace Todotoday\AccountBundle\Form;

use Actiane\PaymentBundle\Enum\PaymentTypeEnum;
use Actiane\PaymentBundle\Form\StripeType;
use Actiane\ToolsBundle\Services\AssetManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\Exception\AccessException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Valid;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\CoreBundle\Services\DomainContextManager;
use Todotoday\PaymentBundle\Entity\CustomerStripeAccount;
use Todotoday\PaymentBundle\Form\AccountSEPAInfoType;

/**
 * Class AccountTransactionalInfoType
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Form
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class AccountTransactionalInfoType extends AbstractType implements
    DataTransformerInterface,
    AccountMandatoryFlowTypeInterface
{
    /**
     * @var DomainContextManager
     */
    private $domainContextManager;

    /**
     * AccountTransactionalInfoType constructor.
     *
     * @param AssetManager         $assetManager
     * @param DomainContextManager $domainContextManager
     */
    public function __construct(AssetManager $assetManager, DomainContextManager $domainContextManager)
    {
        /**
         * Les scriptes sont ajouté ici pour qu'ils soient load avant le scripte AccountTransactionalInfoType
         * Et que les components puissent être display lors d'un retour sur page.
         */
        $assetManager->addJavascript('https://js.stripe.com/v2/');
        //        $assetManager->addJavascript('/bundles/actianepayment/js/stripe.js');
        $assetManager->addJavascript('build/bundles/stripe.js');
        $assetManager->addJavascript('build/accountSEPAInfoType.js');
        $assetManager->addJavascript('build/AccountTransactionalInfoType.js');
        $this->domainContextManager = $domainContextManager;
    }

    /**
     * Do getFieldNeeded
     *
     * @return array
     */
    public static function getFieldNeeded(): array
    {
        return array(
            'bankAccount' => true,
        );
    }

    /**
     * {@inheritdoc}
     * @throws \Symfony\Component\Form\Exception\InvalidArgumentException
     * @throws \UnexpectedValueException
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'paymentTypeSelected',
                HiddenType::class,
                array(
                    'attr' => array(
                        'v-model' => 'button',
                    ),
                )
            )
            ->get('paymentTypeSelected')->addModelTransformer($this);

        if ($agency = $this->domainContextManager->getCurrentContext()->getAgency()) {
            foreach (array_filter($agency->getAuthorizedPaymentMethods()) as $authorizedPaymentMethod) {
                if ($authorizedPaymentMethod
                    && !in_array(
                        $authorizedPaymentMethod,
                        [
                            PaymentTypeEnum::STRIPE_DIRECT, PaymentTypeEnum::PAYPAL, PaymentTypeEnum::POSTFINANCE,
                            PaymentTypeEnum::CHEQUE, PaymentTypeEnum::VIREMENT,
                        ],
                        true
                    )
                ) {
                    $this->{'buildForm' . ucfirst($authorizedPaymentMethod)}($builder, $options);
                }
            }
        }
    }

    /**
     * Do buildView
     *
     * @param FormView      $view
     * @param FormInterface $form
     * @param array         $options
     *
     * @return void
     * @throws \UnexpectedValueException
     */
    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        if ($agency = $this->domainContextManager->getCurrentContext()->getAgency()) {
            $view->vars['payment_methods'] = $agency->getAuthorizedPaymentMethods();
        }
        $view->vars['payment_method_required'] = $options['payment_method_required'];
    }

    /**
     * {@inheritdoc}
     * @throws AccessException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            array(
                'data_class' => Account::class,
                'validation_groups' => 'emailWhiteList',
                'label' => false,
                'payment_method_required' => true,
                'flow' => array(
                    'enable' => false,
                    'formFlowName' => '',
                    'step' => 0,
                ),
                'card_save_message_success' => 'stripe.card_save_success',
            )
        );
    }

    /**
     * {@inheritdoc}
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     */
    public function reverseTransform($value)
    {
        if (!$value) {
            return null;
        }

        return PaymentTypeEnum::getNewInstance($value);
    }

    /**
     * {@inheritdoc}
     * @param PaymentTypeEnum $value
     */
    public function transform($value)
    {
        if (!$value) {
            return null;
        }

        return $value->get();
    }

    /**
     * Do buildFormSepa
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @return void
     */
    protected function buildFormSepa(FormBuilderInterface $builder, array $options): void
    {
        $builder->add(
            'customerBankAccount',
            AccountSEPAInfoType::class,
            array(
                'as_component' => true,
                'validation_groups' => function (FormInterface $form) {
                    /** @var Account $data */
                    $data = $form->getParent()->getData();
                    if ($data->getPaymentTypeSelected()
                        && $data->getPaymentTypeSelected()->get() === PaymentTypeEnum::SEPA
                    ) {
                        return array('sepa');
                    }
                },

            )
        );
    }

    /**
     * Do buildFormStripe
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @return void
     */
    protected function buildFormStripe(FormBuilderInterface $builder, array $options): void
    {
        $builder->add(
            'customerStripeAccount',
            StripeType::class,
            array(
                'as_component' => true,
                'validation_groups' => function (FormInterface $form) {
                    /** @var CustomerStripeAccount $data */
                    $data = $form->getData();
                    if ($data->getAccount()->getPaymentTypeSelected()
                        && ($data->getAccount()->getPaymentTypeSelected()->get() === PaymentTypeEnum::STRIPE
                            || $data->getAccount()->getPaymentTypeSelected()->get() === PaymentTypeEnum::STRIPE_DIRECT)
                    ) {
                        return array('stripe');
                    }
                },
                'fields_required' => true,
                'flow' => $options['flow'],
                'card_save_message_success' => $options['card_save_message_success'],
            )
        );
    }
}
