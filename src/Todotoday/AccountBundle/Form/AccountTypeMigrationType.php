<?php declare(strict_types=1);
/**
 * PHP version 7
 */

namespace Todotoday\AccountBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Todotoday\AccountBundle\Entity\Account;
use Symfony\Component\Form\CallbackTransformer;
use Todotoday\AccountBundle\Enum\AdherentTypeEnum;

/**
 * Class AccountTypeMigrationType
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 *
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Form
 */
class AccountTypeMigrationType extends AbstractType
{
    /**
     * {@inheritdoc}
     * @throws \Symfony\Component\Form\Exception\InvalidArgumentException
     * @throws \Symfony\Component\Validator\Exception\ConstraintDefinitionException
     * @throws \Symfony\Component\Validator\Exception\InvalidOptionsException
     * @throws \Symfony\Component\Validator\Exception\MissingOptionsException
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if (!$options['co_adh']) {
            $builder
                ->add(
                    'customerGroup',
                    CheckboxType::class,
                    array(
                        'translation_domain' => 'todotoday',
                        'label' => 'account.type_adherent_description',
                    )
                );
        }

        if (!$options['co_adh']) {
            $builder->get('customerGroup')->addModelTransformer(new CallbackTransformer(
                function ($customerGroupasBoolean) {
                    return false;
                },
                function ($customerGroupasString) {
                    return AdherentTypeEnum::getNewInstance(AdherentTypeEnum::ADH);
                }
            ));
        }
    }

    /**
     * {@inheritdoc}
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\Validator\Exception\ConstraintDefinitionException
     * @throws \Symfony\Component\Validator\Exception\InvalidOptionsException
     * @throws \Symfony\Component\Validator\Exception\MissingOptionsException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            array(
                'data_class' => Account::class,
                'validation_groups' => 'emailWhiteList',
                'co_adh' => false,
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): ?string
    {
        return 'todotoday_account_bundle_account';
    }
}
