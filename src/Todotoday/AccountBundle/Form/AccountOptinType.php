<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 14/05/17
 * Time: 16:01
 */

namespace Todotoday\AccountBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\CallbackTransformer;
use Todotoday\AccountBundle\Enum\AdherentOptinEnum;

/**
 * Class AccountOptinType
 * @package Todotoday\AccountBundle\Form
 */
class AccountOptinType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @throws \UnexpectedValueException
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addModelTransformer(new CallbackTransformer(
            function ($optinAsString) {
                return ($optinAsString == AdherentOptinEnum::SUBSCRIBED) ? true : false;
            },
            function ($optinAsBoolean) {
                return ($optinAsBoolean) ? AdherentOptinEnum::SUBSCRIBED : AdherentOptinEnum::UNSUBSCRIBED;
            }
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getParent(): ?string
    {
        return CheckboxType::class;
    }
}
