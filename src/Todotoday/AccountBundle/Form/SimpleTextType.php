<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 13/04/2017
 * Time: 16:08
 */

namespace Todotoday\AccountBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SimpleTextType
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle\Form
 * @subpackage Todotoday\AccountBundle\Form
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class SimpleTextType extends AbstractType
{
    /**
     * @param OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'disabled' => true,
            'required' => false,
            'mapped' => false,
        ]);
    }
}
