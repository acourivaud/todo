<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 22/05/2017
 * Time: 11:32
 */

namespace Todotoday\AccountBundle\Form;

/**
 * Interface AccountMandatoryFlowTypeInterface
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Form
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
interface AccountMandatoryFlowTypeInterface
{
    /**
     * Do getFieldNeeded
     *
     * @return array
     */
    public static function getFieldNeeded(): array;
}
