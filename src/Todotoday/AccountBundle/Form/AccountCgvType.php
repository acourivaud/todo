<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 13/04/2017
 * Time: 16:08
 */

namespace Todotoday\AccountBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Todotoday\AccountBundle\Form\SimpleTextType;
use Symfony\Component\Form\CallbackTransformer;
use Todotoday\AccountBundle\Enum\AdherentOptinEnum;
use Symfony\Component\Validator\Constraints\IsTrue;

/**
 * Class AccountCgvType
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle\Form
 * @subpackage Todotoday\AccountBundle\Form
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class AccountCgvType extends AbstractType implements AccountMandatoryFlowTypeInterface
{
    /**
     * Do getFieldNeeded
     *
     * @return array
     */
    public static function getFieldNeeded(): array
    {
        return array(
            'bankAccount' => true,
        );
    }

    /**
     * Do buildForm
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add(
            'cgv',
            CheckboxType::class,
            array(
                'label' => 'signin.disclaimer_cgv',
                'translation_domain' => 'todotoday',
                'required' => false,
                'constraints' => array(
                    new IsTrue(
                        array(
                            'message' => 'signin.cgv_required',
                        )
                    ),
                ),
            )
        )
        ->add(
            'optinlabel',
            SimpleTextType::class,
            [
                'label' => 'form.account.optin_label',
                'translation_domain' => 'todotoday',
                'label_attr' => [
                    'class' => 'optinlabel',
                ],
            ]
        )
        ->add(
            'optin',
            CheckboxType::class,
            [
                'label' => 'form.account.optin_text',
                'translation_domain' => 'todotoday',
                'required' => false,
            ]
        );
        /*$builder->add(
            'article32',
            CheckboxType::class,
            array(
                'label' => 'signin.disclaimer_law',
                'translation_domain' => 'todotoday',
            )
        );
        $builder->add(
            'allowToUseResponse',
            CheckboxType::class,
            array(
                'label' => 'signin.disclaimer_cnil',
                'translation_domain' => 'todotoday',
            )
        );
        $builder->add(
            'informedCommercialUse',
            CheckboxType::class,
            array(
                'label' => 'signin.disclaimer_cnil_mod',
                'translation_domain' => 'todotoday',
            )
        );*/
//        $builder->add(
//            'refusedCommercialUse',
//            CheckboxType::class,
//            array(
//                'label' => 'signin.disclaimer_commercial',
//                'translation_domain' => 'todotoday',
//            )
//        );

        $builder->get('optin')->addModelTransformer(new CallbackTransformer(
            function ($optinAsString) {
                return ($optinAsString == AdherentOptinEnum::SUBSCRIBED) ? true : false;
            },
            function ($optinAsBoolean) {
                return ($optinAsBoolean) ? AdherentOptinEnum::SUBSCRIBED : AdherentOptinEnum::UNSUBSCRIBED;
            }
        ));
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            array(
                'label' => 'signin.ariane_cgv',
                'translation_domain' => 'todotoday',
            )
        );
    }
}
