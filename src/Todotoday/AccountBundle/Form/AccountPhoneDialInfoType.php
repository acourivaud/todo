<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 23/03/2017
 * Time: 09:12
 */

namespace Todotoday\AccountBundle\Form;

use Actiane\ToolsBundle\Services\AssetManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\Exception\AccessException;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AccountPersonalInfoType
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Form
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 *
 * @SuppressWarnings(PHPMD)
 */
class AccountPhoneDialInfoType extends AbstractType
{
    /**
     * AccountMandatoryFieldsType constructor.
     *
     * @param AssetManager $assetManager
     */
    public function __construct(AssetManager $assetManager)
    {
        $assetManager->addCss('build/AccountPhoneDialInfoType.css');
        $assetManager->addJavascript('build/AccountPhoneDialInfoType.js');
    }

    /**
     * {@inheritdoc}
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\Validator\Exception\ConstraintDefinitionException
     * @throws \Symfony\Component\Validator\Exception\InvalidOptionsException
     * @throws \Symfony\Component\Validator\Exception\MissingOptionsException
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'phoneExtension',
                TextType::class,
                array(
                    'label' => 'signin.phone_extension',
                    'attr' => array(
                        'placeholder' => 'Format : +33',
                        'v-model' => 'phoneExtension',
                    ),
                    'translation_domain' => 'todotoday',
                    'required' => false,
                )
            )
            ->add(
                'phone',
                TextType::class,
                array(
                    'label' => 'signin.phone',
                    'attr' => array(
                        'placeholder' => 'signin.phone',
                        'v-model' => 'phoneNumber',
                    ),
                    'translation_domain' => 'todotoday',
                    'required' => false,
                )
            );
    }

    /**
     * {@inheritdoc}
     * @throws AccessException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            array(
                'inherit_data' => true,
                'init_country_with_ip' => false,
            )
        );
    }

    /**
     * @param FormView      $view
     * @param FormInterface $form
     * @param array         $options
     */
    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        $view->vars['init_country_with_ip'] = $options['init_country_with_ip'];
    }
}
