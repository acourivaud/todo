<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 02/02/2017
 * Time: 12:46
 */

namespace Todotoday\AccountBundle\Form\Transformer;

use Symfony\Component\Form\DataTransformerInterface;
use Todotoday\AccountBundle\Security\EditableRolesBuilder;

/**
 * Class RestoreRolesTransformer
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle\Form\Transformer
 * @subpackage Todotoday\AccountBundle\Form\Transformer
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class RestoreRolesTransformer implements DataTransformerInterface
{
    /**
     * @var array
     */
    protected $originalRoles;

    /**
     * @var EditableRolesBuilder
     */
    protected $rolesBuilder;

    /**
     * @param EditableRolesBuilder $rolesBuilder
     */
    public function __construct(EditableRolesBuilder $rolesBuilder)
    {
        $this->rolesBuilder = $rolesBuilder;
    }

    /**
     * {@inheritdoc}
     * @throws \RuntimeException
     */
    public function reverseTransform($selectedRoles)
    {
        if ($this->originalRoles === null) {
            throw new \RuntimeException('Invalid state, originalRoles array is not set');
        }
        [$availableRoles] = $this->rolesBuilder->getRoles();
        $hiddenRoles = array_diff($this->originalRoles, array_keys($availableRoles));

        return array_merge($selectedRoles, $hiddenRoles);
    }

    /**
     * @param array|null $originalRoles
     *
     * @return RestoreRolesTransformer
     */
    public function setOriginalRoles(array $originalRoles = null): self
    {
        $this->originalRoles = $originalRoles ?: array();

        return $this;
    }

    /**
     * {@inheritdoc}
     * @throws \RuntimeException
     */
    public function transform($value)
    {
        if ($value === null) {
            return $value;
        }
        if ($this->originalRoles === null) {
            throw new \RuntimeException('Invalid state, originalRoles array is not set');
        }

        return $value;
    }
}
