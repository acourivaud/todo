<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 10/05/17
 * Time: 16:12
 */

namespace Todotoday\AccountBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Todotoday\AccountBundle\Entity\Account;

/**
 * Class AccountPersonalInfoExtraType
 *
 * @package Todotoday\AccountBundle\Form
 */
class AccountPersonalInfoExtraType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'contactChildren',
                CollectionVueType::class,
                array(
                    'entry_type' => AccountContactChildType::class,
                    'by_reference' => false,
                    'label' => 'account.contact_children',
                    'translation_domain' => 'todotoday',
                )
            )
            ->add(
                'contactAssistants',
                CollectionVueType::class,
                array(
                    'entry_type' => AccountContactAssistantType::class,
                    'by_reference' => false,
                )
            );
    }

    /**
     * {@inheritdoc}
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            array(
                'data_class' => Account::class,
                'validation_groups' => 'emailWhiteList',
            )
        );
    }
}
