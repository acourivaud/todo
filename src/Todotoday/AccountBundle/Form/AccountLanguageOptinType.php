<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 14/05/17
 * Time: 16:01
 */

namespace Todotoday\AccountBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Intl\Intl;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Todotoday\AccountBundle\Entity\Account;
use Symfony\Component\Form\FormBuilderInterface;
use Todotoday\CoreBundle\Services\DomainContextManager;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\CallbackTransformer;
use Todotoday\AccountBundle\Enum\AdherentOptinEnum;
use Todotoday\AccountBundle\Form\SimpleTextType;

/**
 * Class AccountLanguageOptinType
 * @package Todotoday\AccountBundle\Form
 */
class AccountLanguageOptinType extends AbstractType
{
    /**
     * @var DomainContextManager
     */
    private $domainContext;

    /**
     * @var Session
     */
    private $session;

    /**
     * AccountLanguageOptinType constructor.
     *
     * @param DomainContextManager $domainContext
     * @param Session              $session
     */
    public function __construct(DomainContextManager $domainContext, Session $session)
    {
        $this->domainContext = $domainContext;
        $this->session = $session;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @throws \UnexpectedValueException
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $agency = $this->domainContext->getCurrentContext()->getAgency();

        $choiceLanguage = [];

        foreach ($agency->getLanguages() as $language) {
            $choiceLanguage[ucfirst(Intl::getLanguageBundle()->getLanguageName($language))] = $language;
        }

        $builder->add(
            'language',
            ChoiceType::class,
            [
                'choices' => $choiceLanguage,
                'choice_translation_domain' => 'todotoday',
                'label' => 'signin.language',
                'translation_domain' => 'todotoday',
            ]
        )
        ->add(
            'optinlabel',
            SimpleTextType::class,
            [
                'label' => 'form.account.optin_label',
                'translation_domain' => 'todotoday',
                'label_attr' => [
                    'for' => 'optinlabel',
                ],
            ]
        )
        ->add(
            'optin',
            CheckboxType::class,
            [
                'label' => 'form.account.optin_text',
                'translation_domain' => 'todotoday',
                'required' => false,
            ]
        );

        $builder->get('optin')->addModelTransformer(new CallbackTransformer(
            function ($optinAsString) {
                return ($optinAsString == AdherentOptinEnum::SUBSCRIBED) ? true : false;
            },
            function ($optinAsBoolean) {
                return ($optinAsBoolean) ? AdherentOptinEnum::SUBSCRIBED : AdherentOptinEnum::UNSUBSCRIBED;
            }
        ));

        $builder->addEventListener(FormEvents::POST_SUBMIT, array($this, 'changeLanguage'));
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            array(
                'data_class' => Account::class,
                'validation_groups' => 'emailWhiteList',
            )
        );
    }

    /**
     * Trigger on post submit
     *
     * @param FormEvent $event
     */
    public function changeLanguage(FormEvent $event): void
    {
        $this->session->set('_locale', $event->getForm()->getData()->getLanguage());
        $this->session->migrate();
    }
}
