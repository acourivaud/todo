<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 23/06/2017
 * Time: 17:41
 */

namespace Todotoday\AccountBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Entity\Api\Microsoft\CustField;
use Todotoday\AccountBundle\Form\Type\YesNoType;

/**
 * Class AccountCustFieldsType
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle\Form
 * @subpackage Todotoday\AccountBundle\Form
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class AccountCustFieldsType extends AbstractType implements AccountMandatoryFlowTypeInterface
{
    /**
     * Do buildForm
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var Adherent $account */
        $account = $options['data'];
        if ($custFields = $account->getCustFields()) {
            foreach ($custFields as $custField) {
                $this->{'buildForm' . $custField->getFieldType()}($builder, $options, $custField);
            }
        }
    }

    /**
     * Do buildFormBool
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @param CustField            $custField
     *
     * @return void
     */
    public function buildFormBool(FormBuilderInterface $builder, array $options, CustField $custField): void
    {
        $builder->add(
            $custField->getFieldId(),
            YesNoType::class,
            array(
                'label' => $custField->getName(),
                'translation_domain' => 'todotoday',
            )
        );
    }

    /**
     * Do buildFormInt
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @param CustField            $custField
     *
     * @return void
     */
    public function buildFormInt(FormBuilderInterface $builder, array $options, CustField $custField): void
    {
        $builder->add(
            $custField->getFieldId(),
            IntegerType::class,
            array(
                'label' => $custField->getName(),
                'translation_domain' => 'todotoday',
            )
        );
    }

    /**
     * Do buildFormDate
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @param CustField            $custField
     *
     * @return void
     */
    public function buildFormDate(FormBuilderInterface $builder, array $options, CustField $custField): void
    {
        $builder->add(
            $custField->getFieldId(),
            DateType::class,
            array(
                'label' => $custField->getName(),
                'translation_domain' => 'todotoday',
            )
        );
    }

    /**
     * Do buildFormString
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @param CustField            $custField
     *
     * @return void
     */
    public function buildFormString(FormBuilderInterface $builder, array $options, CustField $custField): void
    {
        $builder->add(
            $custField->getFieldId(),
            TextType::class,
            array(
                'label' => $custField->getName(),
                'translation_domain' => 'todotoday',
            )
        );
    }

    /**
     * Do buildFormReal
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @param CustField            $custField
     *
     * @return void
     */
    public function buildFormReal(FormBuilderInterface $builder, array $options, CustField $custField): void
    {
        $builder->add(
            $custField->getFieldId(),
            NumberType::class,
            array(
                'label' => $custField->getName(),
                'translation_domain' => 'todotoday',
            )
        );
    }

    /**
     * {@inheritdoc}
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            array(
                'data_class' => Adherent::class,
                'validation_groups' => 'emailWhiteList',
            )
        );
    }

    /**
     * Do getFieldNeeded
     *
     * @return array
     */
    public static function getFieldNeeded(): array
    {
        return array('custFields' => true);
    }
}
