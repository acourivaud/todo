<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 23/03/2017
 * Time: 09:09
 */

namespace Todotoday\AccountBundle\Form;

/**
 * Class AccountRegistrationFlow
 *
 * @SuppressWarnings(PHPMD.UnusedLocalVariable)
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Form
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class AccountRegistrationFlow extends AccountMandatoryFlow
{
    /**
     * {@inheritdoc}
     */
    protected function loadStepsConfig(): array
    {
        return array(
            array(
                'label' => 'signin.start_title',
                'form_type' => AccountType::class,
                'form_options' => [
                    'validation_groups' => [
                        'Default',
                        'emailWhiteList',
                    ],
                ],
            ),
            array(
                'label' => 'signin.ariane_cgv',
                'form_type' => AccountCgvType::class,
                'form_options' => [
                    'validation_groups' => ['Default'],
                ],
            ),
        );
    }
}
