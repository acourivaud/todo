<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 23/03/2017
 * Time: 09:09
 */

namespace Todotoday\AccountBundle\Form;

use Craue\FormFlowBundle\Form\FormFlow;
use Craue\FormFlowBundle\Form\FormFlowInterface;
use Todotoday\AccountBundle\Entity\Adherent;

/**
 * Class AccountConfirmationCoadherentFlow
 *
 * @SuppressWarnings(PHPMD.UnusedLocalVariable)
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Form
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class AccountConfirmationCoadherentFlow extends FormFlow
{
    /**
     * AccountConfirmationCoadherentFlow constructor.
     */
    public function __construct()
    {
        $this->allowDynamicStepNavigation = true;
    }

    /**
     * Do getFieldStepNeeded
     *
     * @return array
     * @throws \RuntimeException
     */
    public function getFieldStepNeeded(): array
    {
        $requestStepNumber = $this->getRequestedStepNumber() - 1;
        /** @var AccountMandatoryFlowTypeInterface $formType */
        $formType = $this->loadStepsConfig()[$requestStepNumber++]['form_type'];
        $options = $formType::getFieldNeeded();
        if (isset($this->loadStepsConfig()[$requestStepNumber])) {
            $formType = $this->loadStepsConfig()[$requestStepNumber]['form_type'];
            $options = array_merge(
                $formType::getFieldNeeded(),
                $options
            );
        }

        return array_merge(
            array(
                'contactPrincipals' => true,
                'contactChildren' => false,
                'contactAssistants' => false,
                'bankAccount' => false,
            ),
            $options
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function loadStepsConfig(): array
    {
        return array(
            array(
                'label' => 'signin.ariane_cgv',
                'form_type' => AccountCgvType::class,
            ),
            array(
                'label' => 'signin.password_title',
                'form_type' => AccountPasswordType::class,
            ),
        );
    }
}
