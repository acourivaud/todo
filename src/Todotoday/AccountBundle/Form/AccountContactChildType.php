<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 13/04/2017
 * Time: 11:31
 */

namespace Todotoday\AccountBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Todotoday\AccountBundle\Entity\Api\Microsoft\ContactPerson;

/**
 * Class AccountContactChildType
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Form
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class AccountContactChildType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add(
            'firstName',
            null,
            array(
                'attr' => array(
                    'v-model' => 'first_name',
                ),
                'is_vue_template' => $options['is_vue_template'],
            )
        );
        $builder->add(
            'lastName',
            null,
            array(
                'attr' => array(
                    'v-model' => 'last_name',
                ),
                'is_vue_template' => $options['is_vue_template'],
            )
        );
    }

    /**
     * {@inheritdoc}
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            array(
                'data_class' => ContactPerson::class,
            )
        );
    }
}
