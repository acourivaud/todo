<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 02/02/2017
 * Time: 12:24
 */

namespace Todotoday\AccountBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\Exception\AccessException;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Todotoday\AccountBundle\Form\Transformer\RestoreRolesTransformer;
use Todotoday\AccountBundle\Security\EditableRolesBuilder;

/**
 * Class SecurityRolesType
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle\Form
 * @subpackage Todotoday\AccountBundle\Form
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class SecurityRolesType extends AbstractType
{
    /**
     * @var EditableRolesBuilder
     */
    protected $rolesBuilder;

    /**
     * @param EditableRolesBuilder $rolesBuilder
     */
    public function __construct(EditableRolesBuilder $rolesBuilder)
    {
        $this->rolesBuilder = $rolesBuilder;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $formBuilder, array $options): void
    {
        $transformer = new RestoreRolesTransformer($this->rolesBuilder);
        // GET METHOD
        $formBuilder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($transformer) {
                $transformer->setOriginalRoles($event->getData());
            }
        );
        // POST METHOD
        $formBuilder->addEventListener(
            FormEvents::PRE_SUBMIT,
            function (FormEvent $event) use ($transformer) {
                $transformer->setOriginalRoles($event->getForm()->getData());
            }
        );
        $formBuilder->addModelTransformer($transformer);
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        $attr = $view->vars['attr'];
        if (isset($attr['class']) && empty($attr['class'])) {
            $attr['class'] = 'sonata-medium';
        }
        $view->vars['attr'] = $attr;
        $view->vars['read_only_choices'] = $options['read_only_choices'];
    }

    /**
     * {@inheritdoc}
     * @throws AccessException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        [$roles, $rolesReadOnly] = $this->rolesBuilder->getRoles();
        $resolver->setDefaults(
            array(
                'choices' => function (Options $options, $parentChoices) use ($roles) {
                    return empty($parentChoices) ? $roles : array();
                },
                'read_only_choices' => function (Options $options) use ($rolesReadOnly) {
                    return empty($options['choices']) ? $rolesReadOnly : array();
                },
                'data_class' => null,
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): ?string
    {
        return 'security_roles';
    }

    /**
     * {@inheritdoc}
     */
    public function getParent(): ?string
    {
        return ChoiceType::class;
    }
}
