<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 25/06/17
 * Time: 14:58
 */

namespace Todotoday\AccountBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Todotoday\AccountBundle\Entity\Api\Microsoft\ContactPerson;

/**
 * Class AccountJobTitleName
 * @package Todotoday\AccountBundle\Form
 */
class AccountJobTitleName extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add(
            'employmentJobTitleName',
            ChoiceType::class,
            array(
                'choices' => array(
                    'Cadre' => 'Cadre',
                    'Non cadre' => 'Non cadre',
                ),
                'placeholder' => 'Selectionner votre statut',
                'label' => 'Statut',
            )
        );
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            array(
                'data_class' => ContactPerson::class,
            )
        );
    }
}
