<?php declare(strict_types=1);
/**
 * PHP version 7
 */

namespace Todotoday\AccountBundle\Form;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\AccountBundle\Enum\AdherentTypeEnum;
use Todotoday\AccountBundle\Form\Transformer\CustomerGroupTransformer;

/**
 * Class AccountType
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 *
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Form
 */
class AccountType extends AbstractType
{
    /**
     * {@inheritdoc}
     * @throws \Symfony\Component\Form\Exception\InvalidArgumentException
     * @throws \Symfony\Component\Validator\Exception\ConstraintDefinitionException
     * @throws \Symfony\Component\Validator\Exception\InvalidOptionsException
     * @throws \Symfony\Component\Validator\Exception\MissingOptionsException
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if (!$options['co_adh']) {
            $builder
                ->add(
                    'customerGroup',
                    ChoiceType::class,
                    array(
                        'translation_domain' => 'todotoday',
                        'choices' => array(
                            'account.type_adherent_description' => AdherentTypeEnum::ADH,
                            'account.type_non_adherent_description' => AdherentTypeEnum::NON_ADH,
                        ),
                        'expanded' => true,
                        'required' => true,
                    )
                );
        }
        $builder
            ->add(
                'email',
                EmailType::class,
                array(
                    'required' => true,
                    'label' => 'signin.email_adresse',
                    'translation_domain' => 'todotoday',
                )
            )
            ->add(
                'firstName',
                TextType::class,
                array(
                    'required' => true,
                    'label' => 'firstname',
                    'translation_domain' => 'todotoday',
                    'constraints' => [
                        new NotBlank(),
                    ],
                )
            )
            ->add(
                'lastName',
                TextType::class,
                array(
                    'required' => true,
                    'label' => 'lastname',
                    'translation_domain' => 'todotoday',
                    'constraints' => [
                        new NotBlank(),
                    ],
                )
            );

        if ($options['co_adh']) {
            $builder->add(
                'phone',
                TextType::class,
                [
                    'required' => true,
                    'label' => 'signin.phone',
                    'translation_domain' => 'todotoday',
                    'constraints' => [
                        new NotBlank(),
                    ],
                ]
            )
                ->add(
                    'employer',
                    TextType::class,
                    [
                        'required' => true,
                        'label' => 'account.co_adh_employer',
                        'translation_domain' => 'todotoday',
                        'constraints' => [
                            new NotBlank(),
                        ],
                    ]
                )
                ->add(
                    'relation',
                    ChoiceType::class,
                    [
                        'label' => 'account.co-adherent_relation',
                        'translation_domain' => 'todotoday',
                        'choices' => [
                            'account.co_adh_relation_pro' => 'pro',
                            'account.co_adh_relation_perso' => 'perso',
                        ],
                        'constraints' => [
                            new NotBlank(),
                        ],
                    ]
                )
                ->add(
                    'weddingDate',
                    DateType::class,
                    [
                        'label' => 'account.co_adh_wedding_date',
                        'translation_domain' => 'todotoday',
                        'widget' => 'choice',
                        'years' => range(1900, (new \DateTime('now'))->format('Y')),
                        'required' => false,
                    ]
                );
        }

        if (!$options['co_adh']) {
            $builder->get('customerGroup')->addModelTransformer(new CustomerGroupTransformer());
        }
    }

    /**
     * {@inheritdoc}
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\Validator\Exception\ConstraintDefinitionException
     * @throws \Symfony\Component\Validator\Exception\InvalidOptionsException
     * @throws \Symfony\Component\Validator\Exception\MissingOptionsException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            array(
                'data_class' => Account::class,
                'validation_groups' => 'emailWhiteList',
                'co_adh' => false,
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): ?string
    {
        return 'todotoday_account_bundle_account';
    }
}
