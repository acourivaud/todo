<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 23/03/2017
 * Time: 09:09
 */

namespace Todotoday\AccountBundle\Form;

use Craue\FormFlowBundle\Form\FormFlow;
use Craue\FormFlowBundle\Form\FormFlowInterface;
use Todotoday\AccountBundle\Entity\Adherent;

/**
 * Class AccountMandatoryFieldFlow
 *
 * @SuppressWarnings(PHPMD.UnusedLocalVariable)
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Form
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class AccountMandatoryFlow extends FormFlow
{
    /**
     * AccountMandatoryFlow constructor.
     */
    public function __construct()
    {
        $this->allowDynamicStepNavigation = true;
    }

    /**
     * Do getFieldStepNeeded
     *
     * @return array
     * @throws \RuntimeException
     */
    public function getFieldStepNeeded(): array
    {
        $requestStepNumber = $this->getRequestedStepNumber() - 1;
        /** @var AccountMandatoryFlowTypeInterface $formType */
        $formType = $this->loadStepsConfig()[$requestStepNumber++]['form_type'];
        $options = $formType::getFieldNeeded();
        if (isset($this->loadStepsConfig()[$requestStepNumber])) {
            $formType = $this->loadStepsConfig()[$requestStepNumber]['form_type'];
            $options = array_merge(
                $formType::getFieldNeeded(),
                $options
            );
        }

        return array_merge(
            array(
                'contactPrincipals' => false,
                'contactChildren' => false,
                'contactAssistants' => false,
                'bankAccount' => false,
            ),
            $options
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function loadStepsConfig(): array
    {
        return array(
            array(
                'label' => 'account.personal_info',
                'form_type' => AccountPersonalInfoType::class,
                'form_options' => array(
                    'init_country_with_ip' => true,
                    'is_registration' => true,
                ),
            ),
            array(
                'label' => 'account.cust_field',
                'form_type' => AccountCustFieldsType::class,
                'skip' => function (int $estimatedCurrentStepNumber, FormFlowInterface $flow) {
                    /** @var Adherent $account */
                    $account = $flow->getFormData();

                    return $account->getCustomerAccount() && !$account->getCustFields();
                },
            ),
            array(
                'label' => 'account.paiement_methods',
                'form_type' => AccountTransactionalInfoType::class,
                'form_options' => array(
                    'payment_method_required' => true,
                    'flow' => array(
                        'enable' => true,
                        'formFlowName' => $this->getName(),
                        'step' => 3,
                    ),
                    'card_save_message_success' => 'stripe.inscription.card_save_success',
                ),
            ),
        );
    }
}
