<?php declare(strict_types=1);

namespace Todotoday\AccountBundle\Constraints;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Todotoday\AccountBundle\Entity\Adherent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Translation\Translator;
use DateTimeInterface;

/**
 * @Annotation
 */
class EmployeeDateValidator extends ConstraintValidator
{

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var Translator
     */
    private $translator;

    /**
     * EmployeeDateValidator constructor.
     *
     * @param TokenStorageInterface $tokenStorage
     * @param Translator            $translator
     *
     */
    public function __construct(
        TokenStorageInterface $tokenStorage,
        Translator $translator
    ) {

        $this->tokenStorage = $tokenStorage;
        $this->translator = $translator;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed      $value      The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        if (($adherent = $this->tokenStorage->getToken()->getUser()) instanceof Adherent) {
            if ($value instanceof DateTimeInterface
                && ($adherent->getBirthDate() && $adherent->getBirthDate() instanceof DateTimeInterface)) {
                if ($value->getTimestamp() <= $adherent->getBirthDate()->getTimestamp()) {
                    $this->context->buildViolation(
                        $this->translator->trans($constraint->message, [], 'todotoday')
                    )
                    ->addViolation();
                }
            }
        }
    }
}
