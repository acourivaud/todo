<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 04/04/2017
 * Time: 14:59
 */

namespace Todotoday\AccountBundle\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\EmailValidator;
use Todotoday\AccountBundle\Services\EmailWhitelistManager;
use Todotoday\CoreBundle\Services\DomainContextManager;
use Todotoday\AccountBundle\Entity\Adherent;

/**
 * Class EmailWhitelistValidator
 *
 * @category   Todo-Todev
 * @package    Actiane\ToolsBundle
 * @subpackage Actiane\ToolsBundle\Constraints
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class EmailWhitelistValidator extends EmailValidator
{
    /**
     * @var EmailWhitelistManager
     */
    private $emailWhitelistManager;

    /**
     * EmailWhitelistValidator constructor.
     *
     * @param EmailWhitelistManager $emailWhitelistManager
     */
    public function __construct(EmailWhitelistManager $emailWhitelistManager)
    {
        parent::__construct();
        $this->emailWhitelistManager = $emailWhitelistManager;
    }

    /**
     * {@inheritdoc}
     * @throws \UnexpectedValueException
     */
    public function validate($value, Constraint $constraint)
    {
        parent::validate($value, $constraint);

        $adherent = $this->context->getObject();

        if ($adherent && $adherent instanceof Adherent) {
            if ($adherent->isCoAdherent()) {
                return true;
            }
        }

        if (!$this->emailWhitelistManager->isEmailValidWhitelist($value)) {
            $this->context->buildViolation($constraint->customMessage)
                ->setParameter('{{ value }}', $this->formatValue($value))
                ->setCode(Email::INVALID_FORMAT_ERROR)
                ->addViolation();
        }
    }
}
