<?php declare(strict_types=1);

namespace Todotoday\AccountBundle\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class EmployeeDate extends Constraint
{
    public $message = 'account.error_employee_date';
}
