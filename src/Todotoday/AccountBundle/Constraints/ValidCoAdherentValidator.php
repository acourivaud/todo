<?php declare(strict_types=1);

namespace Todotoday\AccountBundle\Constraints;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Repository\AccountRepository;

/**
 * @Annotation
 */
class ValidCoAdherentValidator extends ConstraintValidator
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * ValidCoAdherentValidator constructor.
     *
     * @param EntityManager $entityManager
     *
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed      $value      The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        /** @var AccountRepository $repo */
        $repo = $this->entityManager->getRepository('TodotodayAccountBundle:Account');
        /** @var Adherent $value */
        $account = $repo->findOneBy(
            array(
                'email' => $value->getEmail(),
            )
        );
        if ($account) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}
