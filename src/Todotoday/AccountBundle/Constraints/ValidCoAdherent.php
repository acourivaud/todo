<?php declare(strict_types=1);

namespace Todotoday\AccountBundle\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ValidCoAdherent extends Constraint
{
    public $message = '';
}
