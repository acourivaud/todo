<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 04/04/2017
 * Time: 14:50
 */

namespace Todotoday\AccountBundle\Constraints;

use Symfony\Component\Validator\Constraints\Email;

/**
 * Class EmailWhitelist
 *
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @category   Todo-Todev
 * @package    Actiane\ToolsBundle
 * @subpackage Actiane\ToolsBundle\Constraints
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class EmailWhitelist extends Email
{
    public $customMessage = 'account.error_email_whitelist';
}
