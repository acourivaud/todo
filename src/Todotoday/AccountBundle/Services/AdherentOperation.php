<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 09/10/17
 * Time: 16:23
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\AccountBundle\Services
 *
 * @subpackage Todotoday\AccountBundle\Services
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\AccountBundle\Services;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Todotoday\AccountBundle\Controller\RegistrationController;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\AccountBundle\Entity\LinkAccountAgency;
use Todotoday\AccountBundle\Exceptions\AdherentMigrateEpaDontMatchException;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class AdherentOperation
 */
class AdherentOperation
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var AccountManager
     */
    private $accountManager;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * AdherentOperation constructor.
     *
     * @param AccountManager         $accountManager
     * @param EntityManagerInterface $entityManager
     * @param LoggerInterface        $logger
     */
    public function __construct(
        AccountManager $accountManager,
        EntityManagerInterface $entityManager,
        LoggerInterface $logger
    ) {
        $this->accountManager = $accountManager;
        $this->entityManager = $entityManager;
        $this->logger = $logger;
    }

    /**
     * @param Agency $agencyFrom
     * @param Agency $agencyTo
     *
     * @return int
     * @throws \Psr\Log\InvalidArgumentException
     * @throws AdherentMigrateEpaDontMatchException
     */
    public function giveAdherentAccessToAnotherAgency(Agency $agencyFrom, Agency $agencyTo): int
    {
        $this->logger->info(sprintf('Initiate migration from %s to %s', $agencyFrom->getSlug(), $agencyTo->getSlug()));
        if ($agencyFrom->getDataAreaId() !== $agencyTo->getDataAreaId()) {
            $this->logger->error(
                sprintf(
                    'Cant migrate from %s to %s - the dataArea ID is not the same',
                    $agencyFrom->getSlug(),
                    $agencyTo->getSlug()
                )
            );
            throw new AdherentMigrateEpaDontMatchException($agencyFrom->getName(), $agencyTo->getName());
        }

        $repo = $this->entityManager->getRepository('TodotodayAccountBundle:LinkAccountAgency');
        $linkAccountToMigrates = $repo->getAllCurrentAdherent($agencyFrom, $agencyTo);
        $count = 0;
        foreach ($linkAccountToMigrates as $linkAccount) {
            /** @var Account $account */
            $account = $linkAccount->getAccount();
            if ($this->isAccountInAgency($account, $agencyTo)) {
                $this->logger->warning(
                    sprintf('%s is already in agency %s', $account->getUsername(), $agencyTo->getSlug())
                );
                continue;
            }

            $this->logger->info(
                sprintf(
                    'Give adherent %s access to %s',
                    $account->getUsername(),
                    $agencyTo->getSlug()
                )
            );
            $roles = RegistrationController::$groupToRole;
            $roleToAdd = null;
            foreach ($roles as $role) {
                if ($account->hasRole($role)) {
                    $roleToAdd = $role;
                    break;
                }
            }

            if (!$roleToAdd) {
                $this->logger->warning(
                    sprintf(
                        'Cant migrate adherent %s, no role has been found',
                        $account->getUsername()
                    )
                );
                continue;
            }

            $this->accountManager->createLinkAccountAgency($account, $agencyTo, [$roleToAdd]);
            $this->entityManager->persist($account);
            ++$count;
            $this->logger->info(
                sprintf('Account %s can now use agency %s', $account->getUsername(), $agencyTo->getSlug())
            );
        }

        $this->logger->info(
            sprintf(
                'Migration from %s to %s succeded. %d members copied',
                $agencyFrom->getSlug(),
                $agencyTo->getSlug(),
                $count
            )
        );
        $this->entityManager->flush();

        return $count;
    }

    /**
     * @param Account $account
     * @param Agency  $agency
     *
     * @return bool
     */
    private function isAccountInAgency(Account $account, Agency $agency): bool
    {
        foreach ($account->getLinkAgencies() as $linkAgency) {
            if ($linkAgency->getAgency() === $agency) {
                return true;
            }
        }

        return false;
    }
}
