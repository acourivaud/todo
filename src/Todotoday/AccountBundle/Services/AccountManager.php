<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 23/01/2017
 * Time: 16:15
 */

namespace Todotoday\AccountBundle\Services;

use Actiane\MailerBundle\Services\ActianeMailer;
use Actiane\ToolsBundle\Enum\LoggerEnum;
use Actiane\ToolsBundle\Traits\LoggerTrait;
use Doctrine\ORM\EntityManagerInterface;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Monolog\Logger;
use Psr\Log\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Todotoday\AccountBundle\Controller\RegistrationController;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Entity\LinkAccountAgency;
use Todotoday\AccountBundle\Enum\AdherentTypeEnum;
use Todotoday\AccountBundle\Repository\AccountRepository;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Services\DomainContextManager;

/**
 * Class AccountManager
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Services
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class AccountManager
{
    use LoggerTrait;

    /**
     * @var TokenGeneratorInterface
     */
    protected $tokenGenerator;

    /**
     * @var string[]
     */
    protected $formTypes;

    /**
     * @var ActianeMailer
     */
    protected $mailer;

    /**
     * @var EngineInterface
     */
    protected $engine;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var Router
     */
    protected $router;

    /**
     * @var string
     */
    private $domain;

    /**
     * @var DomainContextManager
     */
    private $domainContextManager;

    /**
     * @var string
     */
    private $scheme;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var Logger
     */
    private $loggerMigration;

    /**
     * AccountManager constructor.
     *
     * @param TokenGeneratorInterface $tokenGenerator
     * @param ActianeMailer           $mailer
     * @param EngineInterface         $engine
     * @param TranslatorInterface     $translator
     * @param EntityManagerInterface  $entityManager
     * @param DomainContextManager    $domainContextManager
     * @param Router                  $router
     * @param Logger                  $logger
     * @param string                  $domain
     * @param string                  $scheme
     * @param RequestStack            $requestStack
     * @param Logger                  $loggerMigration
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        TokenGeneratorInterface $tokenGenerator,
        ActianeMailer $mailer,
        EngineInterface $engine,
        TranslatorInterface $translator,
        EntityManagerInterface $entityManager,
        DomainContextManager $domainContextManager,
        Router $router,
        Logger $logger,
        string $domain,
        string $scheme,
        RequestStack $requestStack,
        Logger $loggerMigration
    ) {

        $this->tokenGenerator = $tokenGenerator;
        $this->mailer = $mailer;
        $this->engine = $engine;
        $this->translator = $translator;
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->logger = $logger;
        $this->prefix = 'AccountManager';
        $this->domain = $domain;
        $this->domainContextManager = $domainContextManager;
        $this->scheme = $scheme;
        $this->requestStack = $requestStack;
        $this->loggerMigration = $loggerMigration;
    }

    /**
     * Do addFormType
     *
     * @param string $key
     * @param string $formType
     * @param string $context
     *
     * @return AccountManager
     */
    public function addFormType(string $key, string $formType, string $context = 'default'): self
    {
        $this->formTypes[$context][$key] = $formType;

        return $this;
    }

    /**
     * Do createAccount
     *
     * @param string $type
     *
     * @return Account
     * @throws \InvalidArgumentException
     */
    public function createAccount(string $type): Account
    {
        $metaData = $this->entityManager->getClassMetadata(Account::class);
        if (!array_key_exists($type, $metaData->discriminatorMap)) {
            $this->log(
                LoggerEnum::ERROR,
                'Account type (' . $type . ') does not exist',
                \InvalidArgumentException::class
            );
        }

        $class = $metaData->discriminatorMap[$type];

        /** @var Account $account */
        $account = new $class();

        $account->setEnabled(true);

        $this->log(
            LoggerEnum::DEBUG,
            'Create account type : ' . $type
        );

        return $account;
    }

    /**
     * Do createLinkAccountAgency
     *
     * @param Account $account
     * @param Agency  $agency
     * @param array   $roles
     *
     * @return AccountManager
     * @throws InvalidArgumentException
     */
    public function createLinkAccountAgency(Account $account, Agency $agency, array $roles): self
    {
        $linkAccountAgency = new LinkAccountAgency();

        $account->addLinkAgency($linkAccountAgency);
        $agency->addLinkAccount($linkAccountAgency);
        $linkAccountAgency->setRoles($roles);

        $this->log(
            LoggerEnum::DEBUG,
            'Create LinkAccountAgency : Account(' . $account->getId() . ') Agency(' . $agency->getId() . ')'
        );

        return $this;
    }

    /**
     * Do createRegistrationToken
     *
     * @param Account $account
     *
     * @return AccountManager
     * @throws InvalidArgumentException
     */
    public function createRegistrationToken(Account $account): self
    {
        $account->setRegistrationToken($this->tokenGenerator->generateToken());
        $account->setRegistrationTokenRequestedAt(new \DateTime());
        $this->log(
            LoggerEnum::DEBUG,
            '[account:' . $account->getId() . '] Create registration token : ' . $account->getRegistrationToken()
        );

        return $this;
    }

    /**
     * @param Account $account
     *
     * @return AccountManager
     * @throws \Psr\Log\InvalidArgumentException
     */
    public function createConfirmationToken(Account $account): self
    {
        $account->setConfirmationToken($this->tokenGenerator->generateToken());
        $account->setPasswordRequestedAt(new \DateTime());
        $this->log(
            LoggerEnum::DEBUG,
            '[account:' . $account->getId() . '] Create confirmation token : ' . $account->getConfirmationToken()
        );

        return $this;
    }

    /**
     * Do getFormType
     *
     * @param Account|string $account
     * @param string         $context
     *
     * @return string
     * @throws \ReflectionException
     * @throws InvalidArgumentException
     */
    public function getFormType(Account $account, string $context = 'default'): string
    {
        $discriminatorMap = $this->entityManager->getClassMetadata(Account::class)->discriminatorMap;
        $reflect = new \ReflectionClass($account);
        $key = array_search($reflect->getName(), $discriminatorMap, true);

        $formTypeClass = $this->formTypes[$context][$key] ?? $this->formTypes[$context]['default'] ??
            $this->formTypes['default']['default'];

        $this->log(
            LoggerEnum::DEBUG,
            '[account:' . $account->getId() . '] Get form type class : ' . $formTypeClass . ' with key : ' . $key
        );

        return $formTypeClass;
    }

    /**
     * Return true if account belongs to agency, false otherwise
     *
     * @param Account $account
     * @param Agency  $agency
     *
     * @return bool
     */
    public function hasAgency(Account $account, Agency $agency): bool
    {
        return \in_array(
            $agency,
            $this->entityManager->getRepository(Agency::class)->getAgencyByAccount($account),
            true
        );
    }

    /**
     * Do saveAccount
     *
     * @param Account $account
     *
     * @return AccountManager
     */
    public function saveAccount(Account $account): self
    {
        $this->entityManager->persist($account);
        $this->entityManager->flush();

        return $this;
    }

    /**
     * If type match linkableType then we bind account, agency with role
     *
     * @param Account $account
     * @param Agency  $agency
     *
     * @return $this
     * @throws InvalidArgumentException
     */
    public function searchLinkAccount(Account $account, Agency $agency)
    {
        $discriminatorMap = $this->entityManager->getClassMetadata(Account::class)->discriminatorMap;

        if ($account instanceof Adherent) {
            $role = RegistrationController::$groupToRole[$account->getCustomerGroup()->get()];
        } else {
            $class = \get_class($account);
            if (!($key = array_search($class, $discriminatorMap, true))) {
                throw new InvalidArgumentException('discriminator dont match');
            }
            $role = 'ROLE_' . strtoupper($key);
        }

        $account->setLanguage($agency->getDefaultLanguage());
        $roles = array($role);

        /* if ($role === RegistrationController::$groupToRole[AdherentTypeEnum::ADH_CO]) {
            $account->setRoles($roles);
        } */

        $this->createLinkAccountAgency($account, $agency, $roles);

        return $this;
    }

    /**
     * Do sendEmailRegistrationToken
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     *
     * @param Account $account
     * @param Agency  $agency
     * @param bool    $protocol
     *
     * @return AccountManager
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     * @throws InvalidArgumentException
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \RuntimeException
     */
    public function sendEmailRegistrationToken(Account $account, ?Agency $agency = null, bool $protocol = false): self
    {
        $message = '';
        $html = '';
        if ($account instanceof Adherent && $agency) {
            if (!$agency->isAllowAdherent() || !$agency->isAllowRegistration() || !$agency->isEnabledFront()) {
                return $this;
            }

            if ($account->getCustomerGroup()->get() === AdherentTypeEnum::NON_ADH) {
                $message = 'TodotodayAccountBundle:Mail:email_registration_token_nonadh.text.twig';
                $html = 'TodotodayAccountBundle:Mail:email_registration_token_nonadh.html.twig';
            } elseif ($account->getCustomerGroup()->get() === AdherentTypeEnum::ADH) {
                $message = 'TodotodayAccountBundle:Mail:email_registration_token.text.twig';
                $html = 'TodotodayAccountBundle:Mail:email_registration_token.html.twig';
            } elseif ($account->getCustomerGroup()->get() === AdherentTypeEnum::ADH_CO) {
                $message = 'TodotodayAccountBundle:Mail:email_registration_token_coadh.text.twig';
                $html = 'TodotodayAccountBundle:Mail:email_registration_token_coadh.html.twig';
            }
        } else {
            $message = 'TodotodayAccountBundle:Mail:email_registration_token_admin.text.twig';
            $html = 'TodotodayAccountBundle:Mail:email_registration_token_admin.html.twig';
        }

        $subjet =
            $this->translator->trans(
                'subject',
                array(),
                'TodotodayAccounBundleEmailRegistrationToken'
            );

        $mail = $this->mailer
            ->createMail(
                array(
                    $account->getEmail() => $account->getFirstName() . ' ' . $account->getLastName(),
                ),
                $subjet
            );

        if ($account instanceof Adherent && $account->getCustomerGroup()->get() === AdherentTypeEnum::ADH_CO) {
            $routeRegistration = 'todotoday_account_registration_confirmationcoadherentworkflow';
        } else {
            $routeRegistration = 'todotoday_account_registration_confirmation';
        }

        $urlRegistration = $this->router->generate(
            $routeRegistration,
            array(
                'token' => $account->getRegistrationToken(),
            ),
            UrlGeneratorInterface::ABSOLUTE_PATH
        );

        $method = $protocol ? 'https://' : 'http://';
        $baseUrl = $agency ? $agency->getSlug() : 'www';
        $urlRegistration = $method . $baseUrl . '.' . $this->domain . $urlRegistration;

        $options = array(
            'url_registration' => $urlRegistration,
            'agency' => $agency,
        );
        if ($account instanceof Adherent && $account->getCustomerGroup()->get() === AdherentTypeEnum::ADH_CO) {
            $parentAdherent = $account->getParent();
            $options['parentFullName'] = $parentAdherent->getFirstName() . ' ' . $parentAdherent->getLastName();
        }

        $mail->getMessage()->setBody(
            $this->engine->render(
                $message,
                $options
            )
        );
        $mail->getMessage()->addPart(
            $this->engine->render(
                $html,
                $options
            ),
            'text/html'
        );
        $mail->send();
        $this->log(
            LoggerEnum::DEBUG,
            'Account(' . $account->getEmail() . ') Send email with registration token'
        );

        return $this;
    }

    /**
     * Do sendResettingEmailMessage
     *
     * @param Account $user
     *
     * @return AccountManager
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \RuntimeException
     */
    public function sendResettingEmailMessage(Account $user): self
    {
        $agency = null;
        $userLinkedAgency = $user->getLinkAgencies()->first();

        if ($userLinkedAgency) {
            $agency = $user->getLinkAgencies()->first()->getAgency() ??
                $this->domainContextManager->getCurrentContext()->getAgency();
        }

        if ($user instanceof Adherent &&
            ($agency && (!$agency->isAllowAdherent() || !$agency->isEnabledFront()))
        ) {
            return $this;
        }

        $object = $this->translator->trans('reset_password.admin_sujet_mail', [], 'todotoday');
        $subdomain = ($agency) ? $agency->getSlug() : 'www';

        $url = $this->scheme . '://' . $subdomain . '.' . $this->domain;
        $url .= $this->router->generate(
            'fos_user_resetting_reset',
            array('token' => $user->getConfirmationToken())
        );

        $variables = array(
            'user' => $user,
            'confirmationUrl' => $url,
        );

        if ($user->getRegistrationRole() === 'ROLE_COMMUNITY_MANAGER') {
            $variables['messageKey'] = 'reset_password.cm.header_mail';
        } else {
            $variables['messageKey'] = 'reset_password.header_mail';
        }

        $bodyTxt =
            $this->mailer->renderTemplate(
                '@TodotodayAccount/Mail/email_registration_password_resetting.txt.twig',
                $variables
            );
        $bodyHtml =
            $this->mailer->renderTemplate(
                '@TodotodayAccount/Mail/email_registration_password_resetting.html.twig',
                $variables
            );
        $this->mailer->createMail($user->getEmail(), $object);
        $this->mailer->getMessage()->setBody($bodyHtml, 'text/html')->addPart($bodyTxt, 'text/plain');
        $this->mailer->send();

        return $this;
    }

    /**
     * Do sendMigrationResettingEmailMessage
     *
     * @param Account $user
     *
     * @return AccountManager
     * @throws \Twig_Error_Syntax
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Loader
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \RuntimeException
     */
    public function sendMigrationResettingEmailMessage(Account $user): self
    {
        /* @var Agency $agency */
        if (!$agency = $user->getLinkAgencies()->first()->getAgency()) {
            throw new \InvalidArgumentException('No agency found');
        }

        $baseUri = $this->scheme . '://' . $agency->getSlug() . '.' . $this->domain;
        $this->requestStack->push(Request::create($baseUri));

        if ($user instanceof Adherent && (!$agency->isAllowAdherent() || !$agency->isEnabledFront())) {
            return $this;
        }

        if (!\in_array('ROLE_MIGRATION', $user->getRoles(), true)
            || \in_array('ROLE_MIGRATION_SEND', $user->getRoles(), true)
        ) {
            return $this;
        }

        $lang = $agency->getDefaultLanguage();
        if ($lang === 'en-US') {
            $lang = 'en';
        }

        $object = 'Nouveaux services TO DO TODAY';
        if ($lang === 'en') {
            $object = 'New services TO DO TODAY';
        }

        $agencyUrl = $this->scheme . '://' . $agency->getSlug() . '.' . $this->domain;
        $confirmationurl =
            $agencyUrl . $this->router->generate(
                'fos_user_resetting_reset',
                array('token' => $user->getConfirmationToken())
            );

        $variables = array(
            'user' => $user,
            'confirmationUrl' => $confirmationurl,
            'agencyUrl' => $agencyUrl,
        );

        $bodyTxt =
            $this->mailer->renderTemplate(
                '@TodotodayAccount/Mail/email_registration_password_resetting_migration_' . $lang . '.txt.twig',
                $variables
            );
        $bodyHtml =
            $this->mailer->renderTemplate(
                '@TodotodayAccount/Mail/email_registration_password_resetting_migration_' . $lang . '.html.twig',
                $variables
            );
        $this->mailer->createMail($user->getEmail(), $object);
        $this->mailer->getMessage()->setBody($bodyHtml, 'text/html')->addPart($bodyTxt, 'text/plain');
        $this->mailer->send();

        $user->addRole('ROLE_MIGRATION_SEND');

        return $this;
    }

    /**
     * Do sendMigrationResettingEmailMessagesByAgencies
     *
     * @param array|null $agenciesSlugs
     *
     * @return int
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \RuntimeException
     * @throws \Psr\Log\InvalidArgumentException
     */
    public function sendMigrationResettingEmailMessagesByAgencies(?array $agenciesSlugs): int
    {
        /** @var AccountRepository $accountRepo */
        $accountRepo = $this->entityManager->getRepository(Account::class);
        $accounts = $accountRepo->findAccountToResetPasswordByAgenciesSlugs($agenciesSlugs);

        $i = 0;
        foreach ($accounts as $account) {
            $agencyName = $account->getLinkAgencies()->first()->getAgency()->getName();

            try {
                $this->sendMigrationResettingEmailMessage($account);
                $i++;
                $this->loggerMigration->log(
                    LoggerEnum::INFO,
                    $account->getEmail() . ' sent with agency : ' . $agencyName
                );
            } catch (\Exception $e) {
                $this->loggerMigration(
                    LoggerEnum::CRITICAL,
                    'Fail sending mail to ' . $account->getEmail() . ' with agency : ' . $agencyName
                );
            }
        }

        $this->entityManager->flush();

        return $i;
    }

    /**
     * @param string       $renderedTemplate
     * @param array|string $toEmail
     *
     * @return AccountManager
     * @throws \Psr\Log\InvalidArgumentException
     */
    protected function sendEmailMessage(string $renderedTemplate, $toEmail): self
    {
        // Render the email, use the first line as the subject, and the rest as the body
        $renderedLines = explode("\n", trim($renderedTemplate));
        $subject = array_shift($renderedLines);
        $body = implode("\n", $renderedLines);

        $message = $this->mailer->createMail($toEmail, $subject)->getMessage();
        $message
            ->setBody($body);

        $this->mailer->send();

        return $this;
    }
}
