<?php declare(strict_types=1);


namespace Todotoday\AccountBundle\Services;

use Doctrine\ORM\EntityManagerInterface;
use Gedmo\Mapping\Annotation\Uploadable;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CoreBundle\Entity\Agency;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use InvalidArgumentException;

/**
 * Class MailchimpManager
 *
 * @package Todotoday\AccountBundle\Services
 */
class MailchimpManager
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * MailchimpManager constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param TranslatorInterface    $translator
     *
     * @throws \Psr\Log\InvalidArgumentException
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        TranslatorInterface $translator
    ) {

        $this->entityManager = $entityManager;
        $this->translator = $translator;
    }

    /**
     * Move uploaded file
     *
     * @param UploadedFile $file
     *
     * @return string
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     */
    public function manageFile(UploadedFile $file)
    {
        $fileName = $file->getClientOriginalName();

        if (!$file->move('uploads/mailchimp', $fileName)) {
            throw new InvalidArgumentException('No file uploaded');
        }

        return $fileName;
    }

    /**
     * Get Adherent email and optin for Mailchimp import
     *
     * @param Agency $agency
     *
     * @return array
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     */
    public function getAdherentsOptinStatus(Agency $agency): array
    {
        $adherentRepo = $this->entityManager->getRepository(Adherent::class);

        $adherents = $adherentRepo->getAdherentsOptinByAgency($agency);

        return ($adherents) ? $adherents : [];
    }

    /**
     * Extract CSV data and check against site datas
     *
     * @param string $filename
     * @param Agency $agency
     *
     * @return array|null
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     */
    public function getPreviewStats(string $filename, Agency $agency)
    {
        $adherentRepo = $this->entityManager->getRepository(Adherent::class);

        $data = [];

        $unchanged = $this->translator->trans('mailchimp.import.unchanged', [], 'todotoday');
        $changed = $this->translator->trans('mailchimp.import.changed', [], 'todotoday');
        $notExist = $this->translator->trans('mailchimp.import.not_exist', [], 'todotoday');

        $filenameData = explode('_', $filename);
        $data['status'] = $filenameData[0];
        $data['stats'] = [
            $unchanged => 0,
            $changed => 0,
            $notExist => 0,
        ];

        if (($handle = fopen('uploads/mailchimp/' . $filename, "r")) !== false) {
            $count = 1;
            while (($row = fgetcsv($handle, 10000, ",")) !== false) {
                if ($count == 1) {
                    $count++;
                    continue;
                }
                $email = $row[0];
                if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $adherent = $adherentRepo->getAdherentByEmail($agency, $email);

                    if ($adherent instanceof Adherent) {
                        $optin = $adherent->getOptin();

                        if ($optin === $data['status']) {
                            $data['stats'][$unchanged]++;
                        } else {
                            $data['stats'][$changed]++;
                        }
                    } else {
                        $data['stats'][$notExist]++;
                    }
                }
            }
            fclose($handle);
        }

        return $data;
    }

    /**
     * Extract CSV data and proceed to update
     *
     * @param string $filename
     * @param Agency $agency
     *
     * @return array
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     */
    public function managerUpload(string $filename, Agency $agency): array
    {
        $adherentRepo = $this->entityManager->getRepository(Adherent::class);

        $filenameData = explode('_', $filename);
        $status = $filenameData[0];

        $result = false;

        if (($handle = fopen('uploads/mailchimp/' . $filename, "r")) !== false) {
            $count = 1;
            while (($row = fgetcsv($handle, 10000, ",")) !== false) {
                if ($count == 1) {
                    $count++;
                    continue;
                }
                $email = $row[0];
                if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $adherent = $adherentRepo->getAdherentByEmail($agency, $email);

                    if ($adherent instanceof Adherent) {
                        $adherent->setOptin($status);
                        $this->entityManager->flush();
                    }
                }
            }
            fclose($handle);
            $result = true;
            unlink('uploads/mailchimp/' . $filename);
        }

        return ($result) ? [200, $this->translator->trans('mailchimp.import.success', [], 'todotoday')]
            : [424, $this->translator->trans('mailchimp.import.failure', [], 'todotoday')];
    }
}
