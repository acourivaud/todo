<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 09/10/17
 * Time: 16:23
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\AccountBundle\Services
 *
 * @subpackage Todotoday\AccountBundle\Services
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\AccountBundle\Services;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Todotoday\AccountBundle\Controller\RegistrationController;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\AccountBundle\Entity\LinkAccountAgency;
use Todotoday\AccountBundle\Exceptions\AdherentMigrateEpaDontMatchException;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Services\DomainContextManager;

/**
 * Class EmailWhitelistManager
 */
class EmailWhitelistManager
{
    /**
     * @var DomainContextManager
     */
    private $domainContextManager;

    /**
     * EmailWhitelistManager constructor.
     *
     * @param DomainContextManager $domainContextManager
     */
    public function __construct(DomainContextManager $domainContextManager)
    {
        $this->domainContextManager = $domainContextManager;
    }

    /**
     * @param string $email
     *
     * @return bool
     * @throws \UnexpectedValueException
     */
    public function isEmailValidWhitelist(string $email): bool
    {
        if ($this->domainContextManager->getCurrentRequest()
            && ($agency = $this->domainContextManager->getCurrentContext()->getAgency())
            && array_filter($agency->getDomainWhitelist())) {
            foreach ($agency->getDomainWhitelist() as $domain) {
                if (strpos($domain, '*')) {
                    return true;
                }

                if (preg_match('/(@' . str_replace('.', '\.', $domain) . ')$/', $email)) {
                    return true;
                }
            }

            return false;
        }

        return true;
    }
}
