<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 29/03/17
 * Time: 16:33
 */

namespace Todotoday\AccountBundle\Services;

use Actiane\ApiConnectorBundle\Services\Connections\Microsoft\MicrosoftDynamicsConnection;
use Actiane\EntityChangeWatchBundle\Interfaces\InterfaceHelper;
use Actiane\PaymentBundle\Services\PaymentMethodManager;
use Actiane\ToolsBundle\Enum\LoggerEnum;
use Actiane\ToolsBundle\Traits\LoggerTrait;
use Doctrine\Common\Persistence\ObjectManager;
use Monolog\Logger;
use Predis\Client;
use Sonata\MediaBundle\Entity\MediaManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Entity\Api\Microsoft\Account;
use Todotoday\AccountBundle\Entity\Api\Microsoft\ContactPerson;
use Todotoday\AccountBundle\Entity\Api\Microsoft\CustField;
use Todotoday\AccountBundle\Entity\Api\Microsoft\CustomerBankAccount;
use Todotoday\AccountBundle\Entity\Api\MicrosoftCRM\Account as AccountCRM;
use Todotoday\AccountBundle\Entity\Document;
use Todotoday\AccountBundle\Entity\LinkAccountAgency;
use Todotoday\AccountBundle\Enum\AdherentTypeEnum;
use Todotoday\AccountBundle\Enum\PaymentStatusEnum;
use Todotoday\AccountBundle\Exceptions\AdherentAlreayMigratedException;
use Todotoday\AccountBundle\Exceptions\AdherentNotFoundInAxException;
use Todotoday\AccountBundle\Form\AccountType;
use Todotoday\AccountBundle\Constraints\ValidCoAdherent;
use Todotoday\AccountBundle\Repository\AdherentRepository;
use Todotoday\AccountBundle\Repository\Api\Microsoft\AccountRepository;
use Todotoday\AccountBundle\Repository\Api\Microsoft\ContactPersonRepository;
use Todotoday\AccountBundle\Repository\Api\Microsoft\CustFieldRepository;
use Todotoday\AccountBundle\Repository\Api\Microsoft\CustomerBankAccountRepository;
use Todotoday\AccountBundle\Repository\Api\MicrosoftCRM\AccountRepository as AccountCRMRepository;
use Todotoday\AccountBundle\Repository\DocumentRepository;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Repository\Api\Microsoft\AgencyAddressBookRepository;
use Todotoday\CoreBundle\Services\DomainContextManager;
use Todotoday\MediaBundle\Entity\Media;
use Todotoday\PaymentBundle\Event\NoPaymentMethodEvent;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Exception\AclNotFoundException;

/**
 * Class AdherentManager
 *
 * @package Todotoday\AccountBundle\Services
 *
 * @SuppressWarnings(PHPMD)
 */
class AdherentManager implements InterfaceHelper
{
    use LoggerTrait;
    public const LOCK_MULTIPLE_PATCH = 'lock_multiple_patch_';

    /**
     * @var AccountRepository
     */
    protected $accountRepository;

    /**
     * @var DocumentRepository
     */
    protected $documentRepository;

    /**
     * @var MicrosoftDynamicsConnection
     */
    protected $connection;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var AgencyAddressBookRepository
     */
    protected $agencyAddressBookRepository;

    /**
     * @var ObjectManager
     */
    protected $manager;

    /**
     * @var PaymentMethodManager
     */
    protected $paymentMethodManager;

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @var MicrosoftDynamicsConnection
     */
    protected $connectionCRM;

    /**
     * @var DomainContextManager
     */
    protected $domainContextManager;

    /**
     * @var MediaManager
     */
    protected $mediaManager;

    /**
     * @var Logger
     */
    private $loggerMigration;

    /**
     * @var AccountManager
     */
    private $accountManager;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var Client
     */
    private $redis;

    /**
     * AdherentManager constructor.
     *
     * @param AccountRepository           $accountRepository
     * @param DocumentRepository          $documentRepository
     * @param MicrosoftDynamicsConnection $connectionCRM
     * @param MicrosoftDynamicsConnection $connection
     * @param AgencyAddressBookRepository $agencyAddressBookRepository
     * @param ObjectManager               $manager
     * @param Logger                      $logger
     * @param PaymentMethodManager        $paymentMethodManager
     * @param TokenStorageInterface       $tokenStorage
     * @param DomainContextManager        $domainContextManager
     * @param MediaManager                $mediaManager
     * @param Logger                      $loggerMigration
     * @param AccountManager              $accountManager
     * @param EventDispatcherInterface    $dispatcher
     * @param FormFactory                 $formFactory
     * @param TranslatorInterface         $translator
     * @param Client                      $redis
     */
    public function __construct(
        AccountRepository $accountRepository,
        DocumentRepository $documentRepository,
        MicrosoftDynamicsConnection $connectionCRM,
        MicrosoftDynamicsConnection $connection,
        AgencyAddressBookRepository $agencyAddressBookRepository,
        ObjectManager $manager,
        Logger $logger,
        PaymentMethodManager $paymentMethodManager,
        TokenStorageInterface $tokenStorage,
        DomainContextManager $domainContextManager,
        MediaManager $mediaManager,
        Logger $loggerMigration,
        AccountManager $accountManager,
        EventDispatcherInterface $dispatcher,
        FormFactory $formFactory,
        TranslatorInterface $translator,
        Client $redis
    ) {
        $this->accountRepository = $accountRepository;
        $this->documentRepository = $documentRepository;
        $this->connection = $connection;
        $this->logger = $logger;
        $this->agencyAddressBookRepository = $agencyAddressBookRepository;
        $this->manager = $manager;
        $this->paymentMethodManager = $paymentMethodManager;
        $this->tokenStorage = $tokenStorage;
        $this->connectionCRM = $connectionCRM;
        $this->domainContextManager = $domainContextManager;
        $this->mediaManager = $mediaManager;
        $this->loggerMigration = $loggerMigration;
        $this->accountManager = $accountManager;
        $this->dispatcher = $dispatcher;
        $this->formFactory = $formFactory;
        $this->translator = $translator;
        $this->redis = $redis;
    }

    /**
     * Method to be called when user logout from api (app mobile)
     *
     * @param Adherent $adherent
     *
     * @return AdherentManager
     * @throws \InvalidArgumentException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     */
    public function adherentLogOut(Adherent $adherent): self
    {
        $adherent->setOneSignalId(null);
        $this->manager->merge($adherent);
        $this->manager->flush();
        $this->updateMicrosoftOneSignal($adherent);

        return $this;
    }

    /**
     * @param Adherent $account
     * @param Agency   $agency
     * @param string   $lang
     *
     * @return AccountCRM
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \InvalidArgumentException
     */
    public function createAdherentInCrm(Adherent $account, Agency $agency, ?string $lang = null): AccountCRM
    {
        $lang = $account->getLanguage() ?? $lang ?? 'fr';

        $langCRM = array(
            'en' => 100000000,
            'en-US' => 100000000,
            'fr' => 100000001,
            'de' => 100000002,
        );

        /** @var AccountCRMRepository $repo */
        $repo = $this->connectionCRM->getRepository(AccountCRM::class);

        $accountCRM = $repo->findOrCreateByAdherentNumber($account);
        $accountCRM
            ->setName($account->getCustomerAccount())
            ->setApsLangueadhrent($langCRM[$lang] ?? 100000001)
            ->setApsFirstname($account->getFirstName())
            ->setApsLastname($account->getLastName())
            ->setApsMiddlename($account->getMiddleName())
            ->setOwnerIdValueGuid($agency->getCrmTeam())
            ->setAccountnumber($account->getCustomerAccount())
            ->setEmailaddress1($account->getEmail())
            ->setApsCustomergroup(
                AdherentTypeEnum::getApsCustomerGroupForCRM(
                    AdherentTypeEnum::getNewInstance(
                        $account->getCustomerGroupString()
                    )->get()
                )
            )
            ->setApsOrigine(100000001)
            ->setApsNatureducompte(100000001);

        if ($account->isCoAdherent()) {
            $parentAccountCRM = $repo->findAccountIdByDoctrineAccount($account->getParent());
            $accountCRM->setParentaccountidValue($parentAccountCRM->getAccountid());
        }

        $this->connectionCRM->persist($accountCRM);
        $this->connectionCRM->flush();
        if (!$account->getCrmIdAdherent()) {
            $account->setCrmIdAdherent($accountCRM->getAccountid());
        }

        return $accountCRM;
    }

    /**
     * Do fillCustomerAddressBooks
     *
     * @param Account  $account
     * @param Adherent $adherent
     *
     * @return Account
     * @throws \InvalidArgumentException
     */
    public function fillCustomerAddressBooks(Account $account, Adherent $adherent): Account
    {
        $addressBooks = '';
        foreach ($adherent->getLinkAgencies() as $linkAccountAgency) {
            if ($addressBook = $this->agencyAddressBookRepository->findAddressBooksByAgencyRetailChannelId(
                $linkAccountAgency->getAgency()->getRetailChannelId()
            )
            ) {
                if ($addressBooks) {
                    $addressBooks .= ';';
                }
                $addressBooks .= $addressBook[0]->getAddressBookName();
                if (!$account->getWarehouseId()) {
                    $account->setWarehouseId($addressBook[0]->getAddressBookName());
                }
            }
        }

        $account->setAddressBooks($addressBooks);

        return $account;
    }

    /**
     * Do getUser
     *
     * @param null|Adherent $adherent
     *
     * @param array         $options
     *
     * @return \Todotoday\AccountBundle\Entity\Account
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    public function getUser(
        ?Adherent $adherent = null,
        array $options = array()
    ): \Todotoday\AccountBundle\Entity\Account {
        $accountMapping = $this->connection->getMapping(Account::class);
        /** @var Adherent $adherent */
        if (!$adherent && !($adherent = $this->tokenStorage->getToken()->getUser()) instanceof Adherent) {
            return $adherent;
        }

        $this->configureOptions($optionResolver = new OptionsResolver());
        $options = $optionResolver->resolve($options);

        $account = $this->accountRepository->findOrCreateByDoctrineAccount($adherent);

        if ($accountMapping->hasIdentifiersValue($account)) {
            $accountMapping->fillEntityInEntityDoctrine($account, $adherent, true);
            $this->manager->flush();
        }

        foreach ($options as $key => $value) {
            if ($value) {
                $this->{'fillEntity' . ucfirst($key)}($adherent, $account);
            }
        }

        return $adherent;
    }

    /**
     * Get media files of an adherant
     *
     * @return array|\Doctrine\Common\Collections\ArrayCollection|\Todotoday\AccountBundle\Entity\Document
     */
    public function getMedia()
    {
        /** @var Adherent $adherent */
        if (($adherent = $this->tokenStorage->getToken()->getUser()) instanceof Adherent) {
            return $this->documentRepository->getAllDocuments($adherent);
        }

        return [];
    }

    /**
     * Upload a document
     *
     * @param array $formData
     *
     * @return bool
     */
    public function uploadMedia($formData)
    {
        /** @var Adherent $adherent */
        $adherent = $this->tokenStorage->getToken()->getUser();
        $file = $formData['file'];
        $title = $formData['title'];

        $extensions = [
            'png', 'gif', 'jpg', 'jpeg', 'jpg',
        ];

        $provider = 'sonata.media.provider.file';
        if (in_array($file->guessClientExtension(), $extensions)) {
            $provider = 'sonata.media.provider.image';
        }

        $media = new Media();
        $media->setName($file->getClientOriginalName());
        $media->setBinaryContent($file);
        $media->setProviderName($provider);
        $this->mediaManager->save($media, 'default');

        $this->setAcl($media);

        $adherentHasDocument = new Document();
        $adherentHasDocument->setMedia($media);
        $adherentHasDocument->setTitle($title);
        $adherentHasDocument->setUploadedBy($adherent);
        $adherentHasDocument->setAdherant($adherent);

        $this->manager->persist($adherentHasDocument);
        $this->manager->flush();

        return $adherentHasDocument;
    }

    /**
     * Remove a document
     *
     * @param int $id
     *
     * @return bool
     */
    public function removeMedia($id)
    {
        /** @var Adherent $adherent */
        if (!($adherent = $this->tokenStorage->getToken()->getUser()) instanceof Adherent) {
            return false;
        }

        if (!$document = $this->documentRepository->findOneByAdherent($adherent, $id)) {
            return false;
        }

        $this->manager->remove($document);
        $this->manager->flush();

        return true;
    }

    /**
     * Do updatePaymentInformation
     *
     * @param Account  $customer
     * @param Adherent $adherent
     *
     * @return AdherentManager
     * @throws \UnexpectedValueException
     */
    public function updateEntityBankAccount(?Account $customer, Adherent $adherent): self
    {
        if ($agency = $this->domainContextManager->getCurrentContext()->getAgency()) {
            $this->paymentMethodManager->initPaymentInformation(
                $agency->getDataAreaId(),
                $agency->getAuthorizedPaymentMethods()
            );
            $this->paymentMethodManager->updatePaymentInformation($adherent, $agency->getAuthorizedPaymentMethods());
        }

        return $this;
    }

    /**
     * Do updateMicrosoftAccount
     *
     * @param Adherent $adherent
     *
     * @param array    $options
     *
     * @return Account
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \InvalidArgumentException
     */
    public function updateMicrosoftAccount(Adherent $adherent, array $options = array()): Account
    {
        $this->configureOptions($optionResolver = new OptionsResolver());
        $options = $optionResolver->resolve($options);
        $customer = $this->accountRepository->findOrCreateByDoctrineAccount($adherent);
        $accountMapping = $this->connection->getMapping(Account::class);

        if ($adherent->isCoAdherent()) {
            $customerParent =
                $this->accountRepository->findAddressCountryRegionIdByDoctrineAccount($adherent->getParent());
            $adherent->setAddressCountryRegionId($customerParent->getAddressCountryRegionId());
            $customer->setInvoiceAccount($adherent->getParent()->getCustomerAccount());
        }

        $accountMapping->fillEntityDoctrineInEntity($customer, $adherent, true);
        // j'ai enlevé la synchro de l'email car dans l'autre sens si l'email est modifié sur microsoft
        // ça change le username de l'adhérent avec des risques de contrainte d'unicité
        // du coup obligé de reset cet email à la main côté microsoft
        $customer->setPrimaryContactEmail($adherent->getEmail());

        /** @var Agency $agency */
        $agency = $adherent->getLinkAgencies()->first()->getAgency();

        $addressDescription = $adherent->getFullName();

        $customer
            //            ->setCustomerGroupId($adherent->getCustomerGroup()->get())
            //            ->setDataAreaId(strtolower($agency->getDataAreaId()))
            ->setPartyType('Person')
            ->setInFront('Yes')
            ->setReceiptOption('Both')
            ->setDataAreaId($agency->getDataAreaId())
            ->setAddressDescription($addressDescription)
            ->setName($adherent->getFirstName() . ' ' . $adherent->getLastName())
            ->setSalesCurrencyCode(
                $agency->getCurrency()
            );

        $this->fillCustomerAddressBooks($customer, $adherent);

        $this->connection->persist($customer);
        $this->connection->flush();

        if (!empty($options)) {
            foreach ($options as $key => $value) {
                if ($value) {
                    $this->{'updateEntity' . ucfirst($key)}($customer, $adherent);
                    // if the payment method is changed we store redis key 30 seconds to prevent multiple patchs (see
                    // PaymentSubscriber->changePayment() for more details)
                    if ($key === 'bankAccount') {
                        $this->redis->setex(self::LOCK_MULTIPLE_PATCH . $adherent->getUsername(), 30, 'exist');
                    }
                }
            }

            $this->connection->persist($customer);
            $this->connection->flush();
        }

        $accountMapping->fillEntityInEntityDoctrine($customer, $adherent, true);

        if ($options['custFields'] && !$adherent->getCustFields()) {
            $this->fillEntityCustFields($adherent, $customer);
        }

        $language = ($adherent->getLanguage()) ? $adherent->getLanguage()
            : $agency->getDefaultLanguage();

        $adherent->setLanguage($language);
        if (!$adherent->getCrmIdAdherent()) {
            $accountCRM = $this->createAdherentInCrm($adherent, $agency);
            $adherent->setCrmIdAdherent($accountCRM->getAccountid());
        }

        return $customer;
    }

    /**
     * @param Adherent $adherent
     *
     * @return null|AccountCRM
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    public function updateMicrosoftStripeId(Adherent $adherent): ?Account
    {
        /** @var Account $customer */
        if (!$customer = $this->accountRepository->find(
            array(
                'dataAreaId' => $adherent->getDataAreaId(),
                'customerAccount' => $adherent->getCustomerAccount(),
            )
        )
        ) {
            $this->logger->log(
                LoggerEnum::CRITICAL,
                'Can\'t update Customer Stripe Id in AX Id because ' . $adherent->getCustomerAccount()
                . ' was not found'
            );

            return null;
        }

        $customer->setIdStripe($adherent->getIdStripe());
        $this->connection->persist($customer)->flush();
        $this->connection->flush();

        $this->logger->info(
            sprintf(
                'Updating Stripe Id %s in AX for customer %s',
                $adherent->getIdStripe(),
                $adherent->getCustomerAccount()
            )
        );

        return $customer;
    }

    /**
     * @param Adherent $adherent
     *
     * @return Account|null
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    public function updateMicrosoftOneSignal(Adherent $adherent): ?Account
    {
        $customer = $this->updateMicrosoftOneSignalAx($adherent);
        $this->updateMicrosoftOneSignalCRM($adherent);

        return $customer;
    }

    /**
     * Do validatePyamentInformation
     *
     * @param Adherent $adherent
     *
     * @return AdherentManager
     * @throws \UnexpectedValueException
     */
    public function validatePaymentInformation(Adherent $adherent): self
    {
        if ($agency = $this->domainContextManager->getCurrentContext()->getAgency()) {
            $this->paymentMethodManager->validatePaymentInformation($adherent, $agency->getAuthorizedPaymentMethods());
        }

        return $this;
    }

    /**
     * @param Adherent $adherent
     *
     * @param Agency   $agency
     *
     * @return Adherent
     * @throws AdherentAlreayMigratedException
     * @throws AdherentNotFoundInAxException
     * @throws \InvalidArgumentException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     */
    public function migrateAdherent(Adherent $adherent, Agency $agency): Adherent
    {
        /** @var Account $adherentMicrosoft */
        if (!$adherentMicrosoft = $this->accountRepository->find(
            array(
                'dataAreaId' => $adherent->getDataAreaId(),
                'customerAccount' => $adherent->getCustomerAccount(),
            )
        )) {
            $this->loggerMigration->error('Adherent' . $adherent->getCustomerAccount() . ' is not found in AX');
            throw new AdherentNotFoundInAxException($adherent->getCustomerAccount());
        }

        if ($adherentMicrosoft->getInFront() === 'Yes') {
            $this->loggerMigration->error('Adherent' . $adherent->getCustomerAccount() . ' already been migrated');
            throw new AdherentAlreayMigratedException($adherent->getCustomerAccount());
        }

        $role = ($adherent->getCustomerGroupString() === AdherentTypeEnum::NON_ADH) ? 'ROLE_NOT_ADHERENT'
            : 'ROLE_ADHERENT';

        $adherent->addRole($role)
            ->setPaymentMethodStatus(PaymentStatusEnum::getNewInstance(PaymentStatusEnum::VALID))
            ->addRole('ROLE_MIGRATION')
            ->setEnabled(true)
            ->addLinkAgency(
                (new LinkAccountAgency())
                    ->setAgency($agency)
                    ->addRole($role)
            );

        $mapping = $this->connection->getMapping(Account::class);
        if ($mapping->hasIdentifiersValue($adherentMicrosoft)) {
            $mapping->fillEntityInEntityDoctrine($adherentMicrosoft, $adherent);
        }

        $this->accountManager->createConfirmationToken($adherent);
        $this->loggerMigration->info('[Generate confirmation token] ' . $adherent->getFullName());
        $this->createAdherentInCrm($adherent, $agency);
        $this->loggerMigration->info('[Create adherent in CRM] ' . $adherent->getFullName());
        $adherentMicrosoft->setInFront('Yes');
        $this->connection->flush($adherentMicrosoft);

        return $adherent;
    }

    /**
     * @param Adherent $adherent
     */
    public function checkPaymentMethod(Adherent $adherent): void
    {
        if ($adherent->getPaymentMethodStatus()->get() === PaymentStatusEnum::NO_PAYMENT_METHOD) {
            $event = new NoPaymentMethodEvent($adherent);
            $this->dispatcher->dispatch($event::NAME, $event);
        }
    }

    /**
     * @param Request  $request
     * @param Agency   $agency
     * @param Adherent $adherent
     * @param bool     $isAPI
     *
     * @return array
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \RuntimeException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Symfony\Component\Validator\Exception\MissingOptionsException
     * @throws \Symfony\Component\Validator\Exception\InvalidOptionsException
     * @throws \Symfony\Component\Validator\Exception\ConstraintDefinitionException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Form\Exception\UnexpectedTypeException
     * @throws \Symfony\Component\Form\Exception\LogicException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     */
    public function addCoAdherent(Request $request, Agency $agency, Adherent $adherent, bool $isAPI = false)
    {
        /** @var AdherentRepository $adherentRepository */
        $adherentRepository = $this->manager->getRepository('TodotodayAccountBundle:Adherent');
        $coAdherents = $adherentRepository->getAllCoAdherentsByParent($adherent);

        $returnArray = array(
            'coAdherents' => $coAdherents,
            'formSubmitted' => false,
            'success' => false,
            'form' => null,
            'data' => null,
            'coAdherentFull' => true,
        );

        if (count($coAdherents) < Adherent::MAX_CO_ADHERENTS) {
            $returnArray['coAdherentFull'] = false;
            $newCoAdherent = new Adherent();
            $newCoAdherent->setAddressCountryRegionId($adherent->getAddressCountryRegionId());
            $data = $form = $this->formFactory->create(
                AccountType::class,
                $newCoAdherent,
                array(
                    'co_adh' => true,
                    'validation_groups' => ["Default"],
                    'csrf_protection' => !$isAPI,
                    'constraints' => array(
                        new ValidCoAdherent(),
                    ),
                )
            )->add(
                'submit',
                SubmitType::class,
                array(
                    'label' => 'account.action.add_co_adherent',
                    'translation_domain' => 'todotoday',
                )
            );

            if ($isAPI) {
                $form->submit($request->request->all());
            } else {
                $form->handleRequest($request);
            }

            if ($isAPI || $form->isSubmitted()) {
                $returnArray['formSubmitted'] = true;
                $returnArray['data'] = $form->getData();

                if ($form->isValid()) {
                    $newCoAdherent->setEnabled(true);
                    $this->manager->persist($newCoAdherent);
                    $adherent->addChild($newCoAdherent);
                    $newCoAdherent->setCustomerGroup(AdherentTypeEnum::getNewInstance(AdherentTypeEnum::ADH_CO));
                    $this->accountManager->searchLinkAccount(
                        $newCoAdherent,
                        $agency
                    );

                    $this->accountManager
                        ->createRegistrationToken($newCoAdherent)
                        ->sendEmailRegistrationToken($newCoAdherent, $agency, $request->isSecure());
                    $this->manager->flush();

                    $returnArray['success'] = true;
                } else {//if form not valid, we check if the co-adherent is disabled and have the adherent as parent
                    /** @var Adherent $deactivatedCoAdherent */
                    $deactivatedCoAdherent = $adherentRepository->findOneBy(
                        array(
                            'parent' => $adherent,
                            'username' => $data->getData()->getUsername(),
                            'enabled' => false,
                        )
                    );

                    if ($deactivatedCoAdherent) {
                        $deactivatedCoAdherent->setEnabled(true);
                        $this->manager->persist($deactivatedCoAdherent);
                        $this->manager->flush();
                        $returnArray['success'] = true;
                    }
                }
            }

            $returnArray['coAdherents'] = $adherentRepository->getAllCoAdherentsByParent($adherent);

            if (count($returnArray['coAdherents']) >= Adherent::MAX_CO_ADHERENTS) {
                $returnArray['coAdherentFull'] = true;
            }

            $returnArray['form'] = $form;
            $returnArray['data'] = $data;
        } elseif ($isAPI) {
            $returnArray['error'] = $this->translator->trans('account.co_adherent_limit_reached', [], 'todotoday');
        }

        return $returnArray;
    }

    /**
     * Compute the signature of the callable, used to avoid a callable to be called multiple times in a row
     *
     * @param array $callable
     * @param array $parameters
     *
     * @return mixed
     */
    public function computeSignature(array $callable, array $parameters)
    {
        return 'Adherent:' . $callable[1] . ':' . $parameters['entity']->getId();
    }

    /**
     * @param Adherent $adherent
     */
    public function changeCoAdherentsEnabled(Adherent $adherent): void
    {
        /** @var AdherentRepository $adherentRepository */
        $adherentRepository = $this->manager->getRepository('TodotodayAccountBundle:Adherent');
        $coAdherents = $adherentRepository->getAllCoAdherentsByParent($adherent);
        $isEnabled = $adherent->getEnabled();

        // Don' affect co adherent if there is no co adherents or if we enable more than the limit number of coAdherents
        if ((\count($coAdherents) <= 0) || ($isEnabled && \count($coAdherents) > Adherent::MAX_CO_ADHERENTS)) {
            return;
        }

        /** @var Adherent $coAdherent */
        foreach ($coAdherents as $coAdherent) {
            $coAdherent->setEnabled($isEnabled);
        }

        $this->manager->flush();
    }

    /**
     * Do configureOptions
     *
     * @param OptionsResolver $optionsResolver
     *
     * @return AdherentManager
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    protected function configureOptions(OptionsResolver $optionsResolver): self
    {
        $optionsResolver->setDefaults(
            array(
                'contactPrincipals' => true,
                'contactChildren' => true,
                'contactAssistants' => true,
                'bankAccount' => true,
                'custFields' => false,
            )
        );

        return $this;
    }

    /**
     * Do fillEntityBankAccount
     *
     * @param Adherent $adherent
     * @param Account  $account
     *
     * @return AdherentManager
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    protected function fillEntityBankAccount(Adherent $adherent, Account $account): self
    {
        /** @var CustomerBankAccountRepository $customerBankAccountRepo */
        $customerBankAccountRepo = $this->connection->getRepository(CustomerBankAccount::class);

        if ($bankAccounts = $customerBankAccountRepo->findBankAccountsByAccount($account)) {
            $adherent->setCustomerBankAccount($bankAccounts[0]);
        }

        return $this;
    }

    /**
     * Do fillEntityBankAccount
     *
     * @param Adherent $adherent
     * @param Account  $account
     *
     * @return AdherentManager
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    protected function fillEntityCustFields(Adherent $adherent, Account $account): self
    {
        /** @var CustFieldRepository $custFieldRepo */
        $custFieldRepo = $this->connection->getRepository(CustField::class);

        if ($custFields = $custFieldRepo->findByAccount($adherent)) {
            $adherent->setCustFields($custFields);
        }

        return $this;
    }

    /**
     * Do fillEntityContactAssistants
     *
     * @param Adherent $adherent
     * @param Account  $account
     *
     * @return AdherentManager
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    protected function fillEntityContactAssistants(Adherent $adherent, Account $account): self
    {
        /** @var ContactPersonRepository $contactPersonRepo */
        $contactPersonRepo = $this->connection->getRepository(ContactPerson::class);

        if ($contactAssistants =
            $contactPersonRepo->findContactsPersonByAccountAndContactType($account, 'Assistant')
        ) {
            $adherent->setContactAssistants($contactAssistants);
        }

        return $this;
    }

    /**
     * Do fillEntityContactChildren
     *
     * @param Adherent $adherent
     * @param Account  $account
     *
     * @return AdherentManager
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    protected function fillEntityContactChildren(Adherent $adherent, Account $account): self
    {
        /** @var ContactPersonRepository $contactPersonRepo */
        $contactPersonRepo = $this->connection->getRepository(ContactPerson::class);

        if ($contactChildren =
            $contactPersonRepo->findContactsPersonByAccountAndContactType($account, 'Children')
        ) {
            $adherent->setContactChildren($contactChildren);
        }

        return $this;
    }

    /**
     * Do fillEntityContactPrincipals
     *
     * @param Adherent $adherent
     * @param Account  $account
     *
     * @return AdherentManager
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    protected function fillEntityContactPrincipals(Adherent $adherent, Account $account): self
    {
        /** @var ContactPersonRepository $contactPersonRepo */
        $contactPersonRepo = $this->connection->getRepository(ContactPerson::class);

        if ($contactPrincipals =
            $contactPersonRepo->findContactsPersonByAccountAndContactType($account, 'Principal')
        ) {
            $adherent->setContactPrincipal($contactPrincipals[0]);
        }

        return $this;
    }

    /**
     * Do updateEntityCustFields
     *
     * @param Account  $customer
     * @param Adherent $adherent
     *
     * @return AdherentManager
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    protected function updateEntityCustFields(Account $customer, Adherent $adherent): self
    {
        return $this;
    }

    /**
     * Do updateAssistants
     *
     * @param Account  $customer
     * @param Adherent $adherent
     *
     * @return AdherentManager
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    protected function updateEntityContactAssistants(Account $customer, Adherent $adherent): self
    {
        /** @var ContactPersonRepository $contactPersonRepo */
        $contactPersonRepo = $this->connection->getRepository(ContactPerson::class);
        if ($contactAssistants =
            $contactPersonRepo->findContactsPersonByAccountAndContactType($customer, 'Assistant')
        ) {
            foreach ($contactAssistants as $key => $contactAssistant) {
                foreach ($adherent->getContactAssistants() as $child) {
                    if ($contactAssistant->getContactPersonId() === $child->getContactPersonId()) {
                        unset($contactAssistants[$key]);
                        break;
                    }
                }
            }
            foreach ($contactAssistants as $contactAssistant) {
                $this->connection->delete($contactAssistant);
            }
        }
        if ($adherent->getContactAssistants()) {
            foreach ($adherent->getContactAssistants() as $contactAssistant) {
                $contactAssistant->setAssociatedPartyNumber($customer->getPartyNumber())
                    ->setContactType('Assistant')
                    ->setDataAreaId($customer->getDataAreaId());
                $this->connection->persist($contactAssistant);
            }
        }

        return $this;
    }

    /**
     * Do updateChildren
     *
     * @param Account  $customer
     * @param Adherent $adherent
     *
     * @return AdherentManager
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    protected function updateEntityContactChildren(Account $customer, Adherent $adherent): self
    {
        /** @var ContactPersonRepository $contactPersonRepo */
        $contactPersonRepo = $this->connection->getRepository(ContactPerson::class);
        if ($contactChildren =
            $contactPersonRepo->findContactsPersonByAccountAndContactType($customer, 'Children')
        ) {
            foreach ($contactChildren as $key => $contactChild) {
                foreach ($adherent->getContactChildren() as $child) {
                    if ($contactChild->getContactPersonId() === $child->getContactPersonId()) {
                        unset($contactChildren[$key]);
                        break;
                    }
                }
            }
            foreach ($contactChildren as $contactChild) {
                $this->connection->delete($contactChild);
            }
        }
        if ($adherent->getContactChildren()) {
            foreach ($adherent->getContactChildren() as $contactChild) {
                $contactChild->setAssociatedPartyNumber($customer->getPartyNumber())
                    ->setContactType('Children')
                    ->setDataAreaId($customer->getDataAreaId());
                $this->connection->persist($contactChild);
            }
        }

        return $this;
    }

    /**
     * Do updateEntityContactPrincipal
     *
     * @param Account  $customer
     * @param Adherent $adherent
     *
     * @return AdherentManager
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    protected function updateEntityContactPrincipals(Account $customer, Adherent $adherent): self
    {
        if ($contactPrincipal = $adherent->getContactPrincipal()) {
            $contactPrincipal->setLastName($customer->getLastName())
                ->setFirstName($customer->getFirstName())
                ->setAssociatedPartyNumber($customer->getPartyNumber())
                ->setContactType('Principal')
                ->setDataAreaId($customer->getDataAreaId());
            $this->connection->persist($contactPrincipal);
        }

        return $this;
    }

    /**
     * @param Adherent $adherent
     *
     * @return null|Account
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \InvalidArgumentException
     */
    private function updateMicrosoftOneSignalAx(Adherent $adherent): ?Account
    {
        /** @var Account $customer */
        if (!$customer = $this->accountRepository->find(
            array(
                'dataAreaId' => $adherent->getDataAreaId(),
                'customerAccount' => $adherent->getCustomerAccount(),
            )
        )
        ) {
            $this->logger->log(
                LoggerEnum::CRITICAL,
                'Can\'t update Customer OneSignal in AX Id because ' . $adherent->getCustomerAccount()
                . ' was not found'
            );

            return null;
        }
        $customer->setOneSignalId($adherent->getOneSignalId());
        $this->connection->persist($customer)->flush();
        $this->connection->flush();

        return $customer;
    }

    /**
     * @param Adherent $adherent
     *
     * @return null|AccountCRM
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    private function updateMicrosoftOneSignalCRM(Adherent $adherent): ?AccountCRM
    {
        /** @var \Todotoday\AccountBundle\Repository\Api\MicrosoftCRM\AccountRepository $repo */
        $repo = $this->connectionCRM->getRepository(AccountCRM::class);
        if (!$customer = $repo->findByDoctrineAccount($adherent)) {
            $this->logger->log(
                LoggerEnum::CRITICAL,
                'Can\'t update Customer OneSignal Id in CRM because ' . $adherent->getCustomerAccount()
                . ' was not found'
            );

            return null;
        }

        $customer->setApsOnesignalid($adherent->getOneSignalId());
        $this->connectionCRM->persist($customer)->flush();

        return $customer;
    }

    /**
     * @param Media $media
     *
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException
     */
    private function setAcl($media)
    {
        global $kernel;

        $aclProvider = $kernel->getContainer()->get('security.acl.provider');
        $objectIdentity = ObjectIdentity::fromDomainObject($media);
        try {
            $aclProvider->findAcl($objectIdentity);
        } catch (AclNotFoundException $e) {
            $acl = $aclProvider->createAcl($objectIdentity);
            $aclProvider->updateAcl($acl);
        }
    }
}
