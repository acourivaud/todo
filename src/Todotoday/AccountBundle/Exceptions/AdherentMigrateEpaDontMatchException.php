<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 09/10/17
 * Time: 16:25
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\AccountBundle\Exceptions
 *
 * @subpackage Todotoday\AccountBundle\Exceptions
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\AccountBundle\Exceptions;

use Throwable;

/**
 * Class AdherentMigrateEpaDontMatchException
 */
class AdherentMigrateEpaDontMatchException extends \Exception
{
    /**
     * AdherentMigrateEpaDontMatchException constructor.
     *
     * @param string         $agencyNameFrom
     * @param string         $agencyNameTo
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct(string $agencyNameFrom, string $agencyNameTo, $code = 0, Throwable $previous = null)
    {
        $message = sprintf(
            'Agency\'s dataAreaId don\'t match for migrate adhernet from %s to %s',
            $agencyNameFrom,
            $agencyNameTo
        );
        parent::__construct($message, $code, $previous);
    }
}
