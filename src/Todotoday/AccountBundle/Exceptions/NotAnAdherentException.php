<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 29/03/17
 * Time: 10:52
 */

namespace Todotoday\AccountBundle\Exceptions;

use Actiane\ToolsBundle\Exceptions\PublishedMessageExceptionInterface;

class NotAnAdherentException extends \InvalidArgumentException implements PublishedMessageExceptionInterface
{
    /**
     * @return mixed
     */
    public function getMessages()
    {
        parent::getMessage();
    }

    /**
     * @return int
     */
    public function getCustomCode(): int
    {
        return 403;
    }
}
