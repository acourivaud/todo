<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 09/06/17
 * Time: 15:52
 */

namespace Todotoday\AccountBundle\Exceptions;

use Throwable;

/**
 * Class AccountNotFoundException
 * @package Todotoday\AccountBundle\Exceptions
 */
class AccountNotFoundException extends \Exception
{
    /**
     * AccountNotFoundException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = 'Account not found', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
