<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 11/05/17
 * Time: 14:18
 */

namespace Todotoday\AccountBundle\Exceptions;

use Throwable;

/**
 * Class AdherentCRMIdMissingException
 * @package Todotoday\AccountBundle\Exceptions
 */
class AdherentCRMIdMissingException extends \Exception
{
    /**
     * AdherentCRMIdMissing constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = 'Adherent CRM Id is missing', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
