<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 28/07/17
 * Time: 10:06
 */

namespace Todotoday\AccountBundle\Exceptions;

use Actiane\ToolsBundle\Exceptions\PublishedMessageExceptionInterface;
use Throwable;

/**
 * Class AdherentNotFoundInAxException
 * @package Todotoday\AccountBundle\Exceptions
 */
class AdherentNotFoundInAxException extends \Exception implements PublishedMessageExceptionInterface
{
    /**
     * AdherentNotFoundInAxException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = '', $code = 0, Throwable $previous = null)
    {
        parent::__construct('Adherent ' . $message . ' not found on AX', $code, $previous);
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->getMessage();
    }

    /**
     * @return int
     */
    public function getCustomCode(): int
    {
        return 500;
    }
}
