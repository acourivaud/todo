<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 28/07/17
 * Time: 10:09
 */

namespace Todotoday\AccountBundle\Exceptions;

use Actiane\ToolsBundle\Exceptions\PublishedMessageExceptionInterface;
use Throwable;

/**
 * Class AdherentAlreayMigratedException
 * @package Todotoday\AccountBundle\Exceptions
 */
class AdherentAlreayMigratedException extends \Exception implements PublishedMessageExceptionInterface
{
    /**
     * AdherentAlreayMigratedException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = '', $code = 0, Throwable $previous = null)
    {
        parent::__construct('Adherent ' . $message . ' already been migrated', $code, $previous);
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->getMessage();
    }

    /**
     * @return int
     */
    public function getCustomCode(): int
    {
        return 500;
    }
}
