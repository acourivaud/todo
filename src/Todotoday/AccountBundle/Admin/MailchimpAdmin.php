<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 25/06/17
 * Time: 11:41
 */

namespace Todotoday\AccountBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Route\RouteCollection;

/**
 * Class MailchimpAdmin
 *
 * @package Todotoday\AccountBundle\Admin
 */
final class MailchimpAdmin extends AbstractAdmin
{
    protected $baseRoutePattern = 'todotoday/mailchimp';
    protected $baseRouteName = 'admin_todotoday_account_mailchimp';

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('delete')
            ->remove('create')
            ->remove('batch')
            ->remove('show')
            ->remove('export')
            ->remove('acl')
            ->remove('edit')
            ->add('download')
            ->add('check')
            ->add('update');
    }
}
