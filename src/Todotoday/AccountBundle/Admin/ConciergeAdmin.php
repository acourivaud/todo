<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 25/06/17
 * Time: 11:41
 */

namespace Todotoday\AccountBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Form\Type\BooleanType;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Todotoday\AccountBundle\Entity\Concierge;
use Sonata\AdminBundle\Datagrid\ListMapper;

/**
 * Class ConciergeAdmin
 *
 * @package Todotoday\AccountBundle\Admin
 */
class ConciergeAdmin extends AccountAdmin
{
    /**
     * @param FormMapper $formMapper
     *
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     * @throws \LogicException
     */
    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper->add('firstName')
            ->add('lastName')
            ->add('email')
            ->add('enabled')
            ->add(
                'sendMailRegistration',
                BooleanType::class,
                array(
                    'label' => 'Envoyer mail d\'inscription',
                    'transform' => true,
                )
            );
        if (($subject = $this->getSubject()) && $subject instanceof Concierge) {
            $formMapper->add(
                'linkAgencies',
                CollectionType::class,
                array(
                    'by_reference' => false,
                ),
                array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    'admin_code' => 'todotoday_account.admin.link_account_agency',
                )
            );
        }
    }

    /**
     * @param ListMapper $listMapper
     *
     * @throws \RuntimeException
     */
    protected function configureListFields(ListMapper $listMapper): void
    {
        parent::configureListFields($listMapper);
        $listMapper->remove('_action');
        $listMapper->add(
            '_action',
            null,
            array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'reset_password' => array(
                        'template' => 'TodotodayAccountBundle:Admin/Account:reset_password.html.twig',
                    ),
                    'continue_registration' => array(
                        'template' => 'TodotodayAccountBundle:Admin/Account:continue_registration.html.twig',
                    ),
                ),
            )
        );
    }
}
