<?php declare(strict_types=1);

namespace Todotoday\AccountBundle\Admin;

use Actiane\ApiConnectorBundle\Services\Connections\Microsoft\MicrosoftDynamicsConnection;
use Doctrine\ORM\QueryBuilder;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\BooleanType;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Entity\Admin;
use Todotoday\AccountBundle\Entity\Api\Microsoft\Account as AccountMicrosoft;
use Todotoday\AccountBundle\Entity\Concierge;
use Todotoday\AccountBundle\Entity\LinkAccountAgency;
use Todotoday\AccountBundle\Form\SecurityRolesType;
use Todotoday\AccountBundle\Repository\Api\Microsoft\AccountRepository;
use Todotoday\AccountBundle\Services\AccountManager;
use Todotoday\AccountBundle\Services\AdherentManager;
use Todotoday\CoreBundle\Services\DomainContextManager;

/**
 * Class AccountAdmin
 *
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Admin
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 */
class AccountAdmin extends AbstractAdmin
{
    /**
     * @var AccountManager
     */
    protected $accountManager;

    /**
     * @var DomainContextManager
     */
    protected $domainContextManager;

    /**
     * @var AdherentManager
     */
    protected $adherentManager;

    /**
     * Whether or not to persist the filters in the session.
     *
     * @var bool
     */
    protected $persistFilters = true;

    /**
     * @var array
     */
    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'DESC',
        '_sort_by' => 'id',
    );

    /**
     * @var MicrosoftDynamicsConnection
     */
    protected $microsoftDynamicsConnection;

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * AccountAdmin constructor.
     *
     * @param string                      $code
     * @param string                      $class
     * @param string                      $baseControllerName
     * @param AccountManager              $accountManager
     * @param DomainContextManager        $domainContextManager
     * @param AdherentManager             $adherentManager
     * @param MicrosoftDynamicsConnection $microsoftDynamicsConnection
     * @param TokenStorageInterface       $tokenStorage
     */
    public function __construct(
        string $code,
        string $class,
        string $baseControllerName,
        AccountManager $accountManager,
        DomainContextManager $domainContextManager,
        AdherentManager $adherentManager,
        MicrosoftDynamicsConnection $microsoftDynamicsConnection,
        TokenStorageInterface $tokenStorage
    ) {
        parent::__construct($code, $class, $baseControllerName);
        $this->accountManager = $accountManager;
        $this->domainContextManager = $domainContextManager;
        $this->adherentManager = $adherentManager;
        $this->microsoftDynamicsConnection = $microsoftDynamicsConnection;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * {@inheritdoc}
     * @throws \UnexpectedValueException
     */
    public function createQuery($context = 'list')
    {
        /** @var ProxyQueryInterface|QueryBuilder $query */
        $query = parent::createQuery($context);
        if ($agency = $this->domainContextManager->getCurrentContext()->getAgency()) {
            // s'il y a un sous domaine de conciergerie, on va filtrer uniquement sur cette conciergerie
            $query->join($query->getRootAliases()[0] . '.linkAgencies', 'link_account_agency')
                ->andWhere('link_account_agency.agency = :agency')
                ->setParameter('agency', $agency);

            return $query;
        }

        if ($this->isGranted('ROLE_COMMUNITY_MANAGER')) {
            return $query;
        }

        // si on est sur le www
        if ($this->isGranted('ROLE_CONCIERGE')) {
            // filtre pour les concierge ne puisse voir uniquement que les adhérents de leurs conciergeries
            /** @var Account $user */
            $user = $this->tokenStorage->getToken()->getUser();
            $allowedAgencies = [];
            // on filtre les concierges avec une date d'expi null ou > today
            foreach ($user->getLinkAgencies()->filter(
                function (LinkAccountAgency $linkAccountAgency) {
                    return null === $linkAccountAgency->getExpiredAt()
                        || $linkAccountAgency->getExpiredAt() > new
                        \DateTime();
                }
            )->getIterator() as $linkAccountAgency) {
                /** @var LinkAccountAgency $linkAccountAgency */
                $allowedAgencies[] = $linkAccountAgency->getAgency();
            }

            $query
                ->join($query->getRootAliases()[0] . '.linkAgencies', 'link_account_agency')
                ->andWhere('link_account_agency.agency in (:agency)')
                ->setParameter('agency', $allowedAgencies);
        }

        return $query;
    }

    /**
     * Do getFieldAdherent
     *
     * @param ProxyQuery|QueryBuilder $queryBuilder
     * @param string                  $alias
     * @param string                  $field
     * @param array                   $value
     *
     * @return bool
     */
    public function getFieldAdherent(ProxyQuery $queryBuilder, string $alias, string $field, array $value): bool
    {
        if (!$value['value']) {
            return false;
        }
        $queryBuilder->leftJoin(Adherent::class, 'adherent', 'WITH', $alias . '.id = adherent.id')
            ->andWhere('lower(adherent.' . $field . ') LIKE lower(:' . $field . ')')
            ->setParameter($field, '%' . $value['value'] . '%');

        return true;
    }

    /**
     * {@inheritdoc}
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     */
    public function getSubject()
    {
        if ($this->subject === null && $this->request) {
            parent::getSubject();
            $account = $this->subject;
            if ($account instanceof Adherent && $account->getCustomerAccount()) {
                $account = $this->adherentManager->getUser($account);
            }
            $this->subject = $account;
        }

        return $this->subject;
    }

    /**
     * {@inheritdoc}
     * @param Account $object
     *
     * @throws \RuntimeException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \UnexpectedValueException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     */
    public function postPersist($object): void
    {
        if ($object->isSendMailRegistration()) {
            if ($object instanceof Admin) {
                $this->accountManager->sendEmailRegistrationToken($object);
            }
            if ($object->getLinkAgencies()->isEmpty()) {
                if ($domain = $this->domainContextManager->getCurrentContext()->getAgency()) {
                    $this->accountManager->sendEmailRegistrationToken($object, $domain);
                }
            } else {
                $this->accountManager->sendEmailRegistrationToken(
                    $object,
                    $object->getLinkAgencies()->first()->getAgency()
                );
            }
        }
    }

    /**
     * {@inheritdoc}
     *
     * @throws \InvalidArgumentException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function postUpdate($object): self
    {
        if ($object instanceof Adherent) {
            /** @var AccountRepository $accountMicrosoftRepo */
            $accountMicrosoftRepo = $this->microsoftDynamicsConnection->getRepository(AccountMicrosoft::class);
            if ($accountMicrosoft = $accountMicrosoftRepo->findByDoctrineAccount($object)) {
                $accountMicrosoft->setFirstName($object->getFirstName())
                    ->setLastName($object->getLastName())
                    ->setFirstName($object->getFirstName())
                    ->setPrimaryContactEmail($object->getEmail())
                    ->setCustomerGroupId($object->getCustomerGroup()->get());
                $this->microsoftDynamicsConnection->persist($accountMicrosoft)->flush();
            }
        }

        return $this;
    }

    /**
     * @param Account $object
     *
     * {@inheritdoc}
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \RuntimeException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function prePersist($object): void
    {
        if ($agency = $this->domainContextManager->getCurrentContext()->getAgency()) {
            $this->accountManager->searchLinkAccount($object, $agency);
        }
        $this->accountManager->createRegistrationToken($object);
        if ($object instanceof Concierge) {
            $object->addRole('ROLE_CONCIERGE');
            foreach ($object->getLinkAgencies() as $linkAccountAgency) {
                $linkAccountAgency->addRole('ROLE_CONCIERGE');
            }
        }
        if ($object instanceof Adherent) {
            foreach ($object->getLinkAgencies() as $linkAccountAgency) {
                $linkAccountAgency->addRole('ROLE_ADHERENT');
            }
        }
    }

    /**
     * @return array
     */
    public function getExportFields(): array
    {
        $fields = parent::getExportFields();
        $fields[] = 'isRegistrationComplete';
        $fields[] = 'allAgencies';
        $keysToRemove = [
            'title',
            'usernameCanonical',
            'emailCanonical',
            'salt',
            'roles',
            'oneSignalId',
            'enterprise',
            'acronisId',
        ];
        foreach ($keysToRemove as $key) {
            if ($index = array_search($key, $fields, true)) {
                unset($fields[$index]);
            }
        }

        return $fields;
    }

    /**
     * @param DatagridMapper $datagridMapper
     *
     * @throws \RuntimeException
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper->add('id')
            ->add('firstName', 'doctrine_orm_string', ['case_sensitive' => false])
            ->add('lastName', 'doctrine_orm_string', ['case_sensitive' => false])
            ->add('username', 'doctrine_orm_string', ['case_sensitive' => false])
            ->add(
                'customerAccount',
                'doctrine_orm_callback',
                array(
                    'callback' => array($this, 'getFieldAdherent'),
                )
            )
            ->add('linkAgencies.agency')
            ->add('email', 'doctrine_orm_string', ['case_sensitive' => false])
            ->add('enabled')
            ->add('lastLogin', 'doctrine_orm_date')
            ->add('roles', 'doctrine_orm_string', ['case_sensitive' => false]);
    }

    /**
     * @param FormMapper $formMapper
     *
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \UnexpectedValueException
     */
    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper->add('firstName')
            ->add('lastName')
            ->add('email')
            ->add(
                'sendMailRegistration',
                BooleanType::class,
                array(
                    'label' => 'Envoyer mail d\'inscription',
                    'transform' => true,
                )
            )
            ->add('enabled');
        if ($this->isGranted('ROLE_ADMIN')) {
            $formMapper->add(
                'roles',
                SecurityRolesType::class,
                array(
                    'label' => 'form.label_roles',
                    'expanded' => true,
                    'multiple' => true,
                    'required' => false,
                )
            );
        }
    }

    /**
     * @param ListMapper $listMapper
     *
     * @throws \RuntimeException
     */
    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper->add('id')
            ->add('firstName')
            ->add('lastName')
            ->add('username')
            ->add('customerAccount')
            ->add('email')
            ->add(
                'enabled',
                'boolean',
                array(
                    'editable' => true,
                )
            )
            ->add('isPasswordSet', 'boolean')
            ->add('isRegistrationComplete', 'boolean')
            ->add('lastLogin')
            ->add('linkAgencies', 'entity', ['admin_code' => 'todotoday_account.admin.link_account_agency'])
            ->add(
                '_action',
                null,
                array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'reset_password' => array(
                            'template' => 'TodotodayAccountBundle:Admin/Account:reset_password.html.twig',
                        ),
                        'continue_registration' => array(
                            'template' => 'TodotodayAccountBundle:Admin/Account:continue_registration.html.twig',
                        ),
                    ),
                )
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection): void
    {
        $collection->add('reset_password', $this->getRouterIdParameter() . '/reset_password');
        $collection->add('continue_registration', $this->getRouterIdParameter() . '/continue_registration');
    }

    /**
     * @param ShowMapper $showMapper
     *
     * @throws \RuntimeException
     */
    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper->add('id')
            ->add('firstName')
            ->add('lastName')
            ->add('username')
            ->add('email')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('customerAccount')
            ->add('adherent.customerGroup')
            ->add('linkAgencies', 'entity', ['admin_code' => 'todotoday_account.admin.link_account_agency'])
            ->add('enabled')
            ->add('isPasswordSet', 'boolean')
            ->add('isRegistrationComplete', 'boolean')
            ->add('lastLogin')
            ->add(
                'registrationToken',
                null,
                array(
                    'template' => 'TodotodayAccountBundle:Admin/Account:registration_link.html.twig',
                    'label' => 'account.admin.registration_link',
                )
            )
            ->add(
                'confirmationToken',
                null,
                array(
                    'template' => 'TodotodayAccountBundle:Admin/Account:reset_link.html.twig',
                    'label' => 'account.admin.reset_link',
                )
            )
            ->add('passwordRequestedAt')
            ->add('paymentTypeSelected', null, array('label' => 'Moyen de paiement choisi'))
            ->add('paymentMethodStatus', null, array('label' => 'Statut du moyen de paiement'));
    }
}
