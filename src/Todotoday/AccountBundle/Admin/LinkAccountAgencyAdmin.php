<?php declare(strict_types=1);

namespace Todotoday\AccountBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\DatePickerType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Todotoday\AccountBundle\Controller\RegistrationController;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Entity\Concierge;
use Todotoday\AccountBundle\Entity\LinkAccountAgency;
use Todotoday\AccountBundle\Form\SecurityRolesType;

/**
 * Class LinkAccountAgencyAdmin
 *
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Admin
 */
class LinkAccountAgencyAdmin extends AbstractAdmin
{
    /**
     * @param LinkAccountAgency $object
     */
    public function prePersist($object): void
    {
        $account = $object->getAccount();
        if ($account instanceof Adherent) {
            $account->setRoles(array('ROLE_ADHERENT'));
        }

        if ($account instanceof Concierge) {
            $account->setRoles(array('ROLE_CONCIERGE'));
        }
    }

    /**
     * @param DatagridMapper $datagridMapper
     *
     * @throws \RuntimeException
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('id')
            ->add('expiredAt');
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper): void
    {
        $rolesAvailable = array_merge(RegistrationController::$groupToRole, ['CONCIERGE' => 'ROLE_CONCIERGE']);

        $formMapper
            ->add(
                'expiredAt',
                DatePickerType::class,
                array(
                    'required' => false,
                )
            )
            ->add(
                'roles',
                ChoiceType::class,
                array(
                    'label' => 'form.label_roles',
                    'expanded' => true,
                    'multiple' => false,
                    'required' => true,
                    'choices' => $rolesAvailable,
                )
            )
            ->add(
                'agency',
                ModelListType::class,
                array(
                    'btn_add' => false,
                    'btn_delete' => false,
                )
            );

        $formMapper->get('roles')->addModelTransformer(
            new CallbackTransformer(
                function ($rolesAsArray) {
                    return $rolesAsArray[0] ?? null;
                },
                function ($rolesAsString) {
                    return [$rolesAsString];
                }
            )
        );
    }

    /**
     * @param ListMapper $listMapper
     *
     * @throws \RuntimeException
     */
    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('expiredAt')
            ->add('account')
            ->add('agency')
            ->add(
                '_action',
                null,
                array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    ),
                )
            );
    }

    /**
     * @param ShowMapper $showMapper
     *
     * @throws \RuntimeException
     */
    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('id')
            ->add('expiredAt');
    }
}
