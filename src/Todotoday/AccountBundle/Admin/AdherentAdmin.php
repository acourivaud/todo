<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 12/06/17
 * Time: 11:41
 */

namespace Todotoday\AccountBundle\Admin;

use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\CoreBundle\Form\Type\BooleanType;
use Sonata\Form\Validator\ErrorElement;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Entity\Api\Microsoft\Account;
use Todotoday\AccountBundle\Enum\AdherentTypeEnum;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Todotoday\AccountBundle\Form\SecurityRolesType;
use Todotoday\AccountBundle\Repository\Api\Microsoft\AccountRepository;
use Todotoday\AccountBundle\Form\AccountOptinType;

/**
 * Class AdherentAdmin
 *
 * @package Todotoday\AccountBundle\Admin
 */
class AdherentAdmin extends AccountAdmin
{

    /**
     * @param object $object
     */
    public function preRemove($object): void
    {
        try {
            /** @var AccountRepository $accountRepo */
            $accountRepo = $this->microsoftDynamicsConnection->getRepository(Account::class);
            /** @var Adherent $object */
            if ($accountMicrosoft = $accountRepo->findByDoctrineAccount($object)) {
                $accountMicrosoft->setInFront('No');
                $this->microsoftDynamicsConnection->flush();
            }
        } catch (\Exception $e) {
            // prevent exception when update account microsoft fail
        }
    }

    /**
     * {@inheritdoc}
     */
    public function prePersist($object): void
    {
        $object->setDocuments($object->getDocuments());
        parent::prePersist($object);
    }

    /**
     * {@inheritdoc}
     */
    public function preUpdate($object)
    {
        $object->setDocuments($object->getDocuments());

        $user = $this->tokenStorage->getToken()->getUser();
        foreach ($object->getDocuments() as $document) {
            if ($document->getId() === null) {
                $document->setUploadedBy($user);
            }
        }

        parent::preUpdate($object);
    }

    /**
     * @param FormMapper $formMapper
     *
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \UnexpectedValueException
     */
    public function configureFormFields(FormMapper $formMapper): void
    {
//        parent::configureFormFields($formMapper);

        $formMapper
            ->tab('Adherent')
            ->with('Adherent information');

        $formMapper
            ->add('firstName')
            ->add('lastName')
            ->add('email')
            ->add(
                'sendMailRegistration',
                BooleanType::class,
                array(
                    'label' => 'Envoyer mail d\'inscription',
                    'transform' => true,
                )
            )
            ->add(
                'optin',
                AccountOptinType::class,
                [
                    'label' => 'Opt-in marketing',
                    'required' => false,
                ]
            )
            ->add('enabled')
            ->add(
                'customerGroup',
                'choice',
                array(
                    'choices' => AdherentTypeEnum::getChoicesFilterCoAdh(),
                    'choice_value' => function (?AdherentTypeEnum $adherentTypeEnum) {
                        if (!$adherentTypeEnum) {
                            return null;
                        }

                        return $adherentTypeEnum->get();
                    },
                )
            )
            ->add(
                'linkAgencies',
                CollectionType::class,
                array(
                    'by_reference' => false,
                ),
                array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    'admin_code' => 'todotoday_account.admin.link_account_agency',
                )
            );
        if ($this->isGranted('ROLE_ADMIN')) {
            $formMapper->add(
                'roles',
                SecurityRolesType::class,
                array(
                    'label' => 'form.label_roles',
                    'expanded' => true,
                    'multiple' => true,
                    'required' => false,
                )
            );
        }

        $formMapper->end()->end();

        if ($this->isGranted('ROLE_ADMIN')
            || ($this->isGranted('ROLE_CONCIERGE') && !$this->isGranted('ROLE_COMMUNITY_MANAGER'))) {
            $formMapper
                ->tab('Documents')
                    ->with('Adherent documents')
                        ->add(
                            'documents',
                            \Sonata\Form\Type\CollectionType::class,
                            [
                                'label' => 'Documents',
                                'required' => false,
                                'by_reference' => false,
                                'type_options'  => [
                                    'delete' => true,
                                ],
                            ],
                            [
                                'edit' => 'inline',
                                'inline' => 'table',
                                'admin_code' => 'todotoday_account.admin.adherant_has_documents',
                            ]
                        )
                    ->end()
                ->end();
        }
    }

    /**
     * @param ListMapper $listMapper
     *
     * @throws \RuntimeException
     */
    public function configureListFields(ListMapper $listMapper): void
    {
        parent::configureListFields($listMapper);
        $listMapper
            ->add('customerGroup')
            ->add(
                'isOptinSubscribed',
                'boolean',
                [
                    'label' => 'Opt-in marketing',
                ]
            );

        $params = array(
            'actions' => array(
                'show' => array(),
                'edit' => array(),
            ),
        );

        if ($this->isGranted('ROLE_COMMUNITY_MANAGER')) {
            $params['actions']['operation'] = array(
                'template' => 'TodotodayCoreBundle:Action:action_agency_operation.html.twig',
            );
            $params['actions']['reset_password'] = array(
                'template' => 'TodotodayAccountBundle:Admin/Account:reset_password.html.twig',
            );
            $params['actions']['continue_registration'] = array(
                'template' => 'TodotodayAccountBundle:Admin/Account:continue_registration.html.twig',
            );
        }

        if ($this->isGranted('ROLE_ADMIN')
            || ($this->isGranted('ROLE_CONCIERGE') && !$this->isGranted('ROLE_COMMUNITY_MANAGER'))) {
            $params['actions']['reset_password'] = array(
                'template' => 'TodotodayAccountBundle:Admin/Account:reset_password.html.twig',
            );
            $params['actions']['continue_registration'] = array(
                'template' => 'TodotodayAccountBundle:Admin/Account:continue_registration.html.twig',
            );
            $params['actions']['documents'] = array(
                'template' => 'TodotodayAccountBundle:Admin/Account:documents.html.twig',
            );
        }

        $listMapper
        ->remove('_action')
        ->add(
            '_action',
            null,
            $params
        );
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection): void
    {
        parent::configureRoutes($collection);
        $collection->add('operation', $this->getRouterIdParameter() . '/opration');
    }
}
