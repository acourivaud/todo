<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 12/06/17
 * Time: 11:41
 */

namespace Todotoday\AccountBundle\Admin;

use JMS\Serializer\Tests\Fixtures\Input;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Entity\Concierge;
use Todotoday\AccountBundle\Enum\AdherentTypeEnum;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Todotoday\AccountBundle\Repository\Api\Microsoft\AccountRepository;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Todotoday\AccountBundle\Services\AdherentManager;

/**
 * Class AdherentHasMediaAdmin
 *
 * @package Todotoday\AccountBundle\Admin
 */
class DocumentsAdmin extends AbstractAdmin
{

    /**
     * @param FormMapper $form
     *
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     * @throws \LogicException
     */
    protected function configureFormFields(FormMapper $form): void
    {

        /** @var AdherentManager $adherentManager */
        $adherentManager = $this->getConfigurationPool()
                            ->getContainer()
                            ->get('todotoday.account.services.adherent_manager');

        /** @var Adherent $account */
        $account = $adherentManager->getUser();

        $form
            ->add('createdAt', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'disabled' => true,
                'format' => 'dd/MM/yyyy HH:mm',
            ])
            ->add('uploadedBy', TextType::class, [
                'disabled' => true,
                'empty_data' => $account,
            ])
            ->add('title', null, [
                'required' => true,
                'constraints' => array(
                    new NotBlank(),
                ),
            ])
            ->add(
                'media',
                'sonata_type_model_list',
                [
                    'required' => true,
                    'constraints' => array(
                        new NotBlank(),
                    ),
                ],
                [
                    'link_parameters' => [
                        'context' => 'share_files',
                    ],
                ]
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $list)
    {
        $list
            ->add('adherant')
            ->add('media');
    }
}
