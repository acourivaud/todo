<?php declare(strict_types=1);

namespace Todotoday\AccountBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Enum\PaymentStatusEnum;
use Todotoday\AccountBundle\Repository\AdherentRepository;

/**
 * Class TodotodayAdherentPaymentStatusCommand
 * @package Todotoday\AccountBundle\Command
 */
class TodotodayAdherentPaymentStatusCommand extends ContainerAwareCommand
{
    /**
     *
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function configure(): void
    {
        $this
            ->setName('todotoday:adherent:payment:status')
            ->setDescription('Set status payment to given adherent')
            ->addArgument('status', InputArgument::REQUIRED, 'status to switch on')
            ->addArgument('account', InputArgument::REQUIRED, 'array of customer account');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Symfony\Component\Console\Exception\LogicException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \LogicException
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');

        /** @var AdherentRepository $adherentRepo */
        $adherentRepo = $doctrine->getRepository('TodotodayAccountBundle:Adherent');
        $em = $doctrine->getManager();

        $status = PaymentStatusEnum::getNewInstance($input->getArgument('status'));

        $output->writeln('ok');

        $accounts = explode(',', $input->getArgument('account'));
        foreach ((array) $accounts as $account) {
            /** @var Adherent $adherent */
            if ($adherent = $adherentRepo->findOneBy(
                array(
                    'customerAccount' => $account,
                )
            )) {
                $adherent->setPaymentMethodStatus($status);
                $em->flush();
                $output->writeln($adherent->getFullName() . ' updated');
            } else {
                $output->writeln($account . ' not found');
            }
        }
    }
}
