<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 27/01/2017
 * Time: 11:52
 */

namespace Todotoday\AccountBundle\Command;

use Actiane\ToolsBundle\Traits\OutputFormatLoggerTrait;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Exception as SFDException;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class LinkAccountAgencyCommand
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle\Command
 * @subpackage Todotoday\AccountBundle\Command
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class LinkAccountAgencyCommand extends ContainerAwareCommand
{
    use OutputFormatLoggerTrait;

    /**
     * {@inheritdoc}
     * @throws InvalidArgumentException
     */
    protected function configure()
    {
        $this->setName('tdtd:account:link-agency')
            ->setDescription('Account creation')
            ->addArgument(
                'username',
                InputArgument::REQUIRED,
                'The username'
            )
            ->addArgument(
                'agency',
                InputArgument::REQUIRED,
                'The slug agency'
            )
            ->addArgument(
                'roles',
                InputArgument::IS_ARRAY,
                'The roles'
            );
    }

    /**
     * {@inheritdoc}
     * @throws InvalidArgumentException
     * @throws \LogicException
     * @throws SFDException\ServiceCircularReferenceException
     * @throws SFDException\ServiceNotFoundException
     * @throws \InvalidArgumentException
     * @throws \Psr\Log\InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->initFormat($output);
        $accountManager = $this->getContainer()->get('todotoday.account.services.account_manager');
        $accountManager->setOutput($output);

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $account = $em->getRepository(Account::class)->findOneBy(array('username' => $input->getArgument('username')));
        $agency = $em->getRepository(Agency::class)->findOneBy(array('slug' => $input->getArgument('agency')));
        $accountManager->createLinkAccountAgency($account, $agency, $input->getArgument('roles'));
        $em->flush();
    }
}
