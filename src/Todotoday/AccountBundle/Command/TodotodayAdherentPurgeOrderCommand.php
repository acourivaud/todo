<?php declare(strict_types=1);

namespace Todotoday\AccountBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

/**
 * Class TodotodayAdherentPurgeOrderCommand
 * @package Todotoday\AccountBundle\Command
 */
class TodotodayAdherentPurgeOrderCommand extends ContainerAwareCommand
{
    /**
     *
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function configure()
    {
        $this
            ->setName('todotoday:adherent:purge:order')
            ->setDescription('Purge last-week\'s orders for adherent_areva')
            ->addArgument('days', InputArgument::OPTIONAL, 'number of history days to keep', 7);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Symfony\Component\Console\Exception\LogicException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \LogicException
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $connection = $this->getContainer()->get('actiane.api_connector.connection.microsoft');

        $adherent = $this->getContainer()->get('doctrine')->getRepository('TodotodayAccountBundle:Adherent')
            ->findOneBy(
                array(
                    'email' => 'areva@actiane.com',
                )
            );

        $repo = $this->getContainer()->get('todotoday.checkout.repository.api.sales_order_header');
        $ordersToPurge = $repo->getLastDaysOrder($adherent, (int) $input->getArgument('days'));
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('Do you confirm ? (y/N)', false);
        $output->writeln(count($ordersToPurge) . ' orders is about to be purged for ' . $adherent->getUsername());

        if (!$helper->ask($input, $output, $question)) {
            return $output->writeln('tu as refusé');
        }

        foreach ($ordersToPurge as $order) {
            if ($order->getOrderResponsiblePersonnelNumber() === 'Front') {
                $connection->delete($order);
                $output->writeln('Purging order : ' . $order->getSalesOrderNumber());
            }
        }
        $connection->flush();

        return $output->writeln('proceed');
    }
}
