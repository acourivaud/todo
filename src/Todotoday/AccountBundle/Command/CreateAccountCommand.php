<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 27/01/2017
 * Time: 11:52
 */

namespace Todotoday\AccountBundle\Command;

use Actiane\ToolsBundle\Traits\OutputFormatLoggerTrait;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Exception as SFDException;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class CreateAccountCommand
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle\Command
 * @subpackage Todotoday\AccountBundle\Command
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class CreateAccountCommand extends ContainerAwareCommand
{
    use OutputFormatLoggerTrait;

    /**
     * {@inheritdoc}
     * @throws InvalidArgumentException
     */
    protected function configure(): void
    {
        $this->setName('tdtd:account:create')
            ->setDescription('Account creation')
            ->addArgument(
                'email',
                InputArgument::REQUIRED,
                'The email'
            )->addArgument(
                'first_name',
                InputArgument::REQUIRED,
                'The first name'
            )
            ->addArgument(
                'last_name',
                InputArgument::REQUIRED,
                'The last name'
            )
            ->addArgument(
                'type',
                InputArgument::REQUIRED,
                'Account type : adherent|admin|concierge'
            )
            ->addArgument(
                'role',
                InputArgument::REQUIRED,
                'Role to give : admin|community_manager|concierge|adherent'
            )
            ->addArgument(
                'password',
                InputArgument::OPTIONAL,
                'The password'
            )
            ->addOption(
                'agency',
                'a',
                InputOption::VALUE_OPTIONAL,
                'The slug agency'
            );
    }

    /**
     * {@inheritdoc}
     * @throws InvalidArgumentException
     * @throws \LogicException
     * @throws SFDException\ServiceCircularReferenceException
     * @throws SFDException\ServiceNotFoundException
     * @throws \InvalidArgumentException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \RuntimeException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->initFormat($output);
        $accountManager = $this->getContainer()->get('todotoday.account.services.account_manager');
        $accountManager->setOutput($output);

        $accountType = $input->getArgument('type');
        $account = $accountManager->createAccount($accountType);
        $accountManager->createRegistrationToken($account);
        $account
            ->setFirstName($input->getArgument('first_name'))
            ->setLastName($input->getArgument('last_name'))
            ->setEmail($input->getArgument('email'))
            ->setPlainPassword($input->getArgument('password'))
            ->setRoles(array('ROLE_' . strtoupper($input->getArgument('role'))));

        $accountManager->saveAccount($account);

        if ($accountType !== 'adherent' && $accountType !== 'concierge') {
            $accountManager->sendEmailRegistrationToken($account);
        } elseif ($agencySlug = $input->getOption('agency')) {
            $agency = $this->getContainer()->get('doctrine.orm.entity_manager')->getRepository(Agency::class)
                ->findOneBy(array('slug' => $agencySlug));
            if (!$agency) {
                $output->writeln('<error>Agency(' . $agencySlug . ' doesn\'t exist)</error>');
            } else {
                $accountManager->sendEmailRegistrationToken($account, $agency);
            }
        }
    }
}
