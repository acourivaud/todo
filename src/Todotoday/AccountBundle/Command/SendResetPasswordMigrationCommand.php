<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 24/06/2017
 * Time: 12:11
 */

namespace Todotoday\AccountBundle\Command;

use Actiane\ToolsBundle\Traits\OutputFormatLoggerTrait;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SendResetPasswordMigrationCommand
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Command
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class SendResetPasswordMigrationCommand extends ContainerAwareCommand
{
    use OutputFormatLoggerTrait;

    /**
     * {@inheritdoc}
     * @throws \LogicException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \RuntimeException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->initFormat($output);

        $slugs = explode(',', $input->getArgument('agencies'));

        $emailSent = $this->getContainer()->get('todotoday.account.services.account_manager')
            ->sendMigrationResettingEmailMessagesByAgencies($slugs);

//        foreach ($emailSent as $account) {
//            $output->writeln(
//                $account->getEmail() . ' - ' . $account->getLinkAgencies()->first()->getAgency()->getSlug
//                ()
//            );
//        }
        $output->writeln($emailSent . ' email(s) sent');
    }

    /**
     * {@inheritdoc}
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function configure(): void
    {
        $this->setName('todotoday:account:sendmail:migration')
            ->setDescription('Send email reset password to migrate adherents')
            ->addArgument('agencies', InputArgument::REQUIRED, 'Agency slug to send emails (separate agency with ,)');
    }
}
