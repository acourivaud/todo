<?php declare(strict_types=1);

namespace Todotoday\AccountBundle\Command;

use League\Csv\Reader;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class TodotodayConciergeImportCommand
 *
 * @package Todotoday\AccountBundle\Command
 */
class TodotodayConciergeImportCommand extends ContainerAwareCommand
{
    /**
     * Configure Method
     *
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function configure(): void
    {
        $this
            ->setName('todotoday:concierge:import')
            ->setDescription('Import concierge account from csv');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     * @throws \LogicException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\Security\Acl\Exception\AclAlreadyExistsException
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $csv = Reader::createFromPath(__DIR__ . '/../Resources/ImportFile/import_concierge.csv');
        $records = $csv->setOffset(1)->fetchAll();

        $accounts = [];

        foreach ($records as $record) {
            [$nom, $prenom, $email, $agencies] = $record;
            $account = $this->createAccount($nom, $prenom, $email, explode('|', $agencies));
            $em->persist($account);
            $accounts[] = $account;
            $output->writeln('<info>Account ' . $nom . ' ' . $prenom . ' created</info>');
        }
        $em->flush();
        $this->setOwnerAcl($accounts);
    }

    /**
     * @param string $nom
     * @param string $prenom
     * @param string $email
     * @param array  $agencies
     *
     * @return Account
     *
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \LogicException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     */
    private function createAccount(string $nom, string $prenom, string $email, array $agencies): Account
    {
        $manager = $this->getContainer()->get('todotoday.account.services.account_manager');
        $account = $manager->createAccount('concierge');
        $account->setLastName($nom)
            ->setFirstName($prenom)
            ->setEmail($email)
            ->setRoles(['ROLE_CONCIERGE']);

        foreach ($agencies as $retailChannelId) {
            if ($agency = $this->getAgency($retailChannelId)) {
                $manager->createLinkAccountAgency($account, $agency, ['ROLE_CONCIERGE']);
            }
        }
        $manager->createRegistrationToken($account);

        return $account;
    }

    /**
     * @param string $retailChannelId
     *
     * @return null|Agency
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \LogicException
     */
    private function getAgency(string $retailChannelId): ?Agency
    {
        $agencyRepo = $this->getContainer()->get('doctrine')->getRepository('TodotodayCoreBundle:Agency');

        return $agencyRepo->findOneBy(
            [
                'retailChannelId' => $retailChannelId,
            ]
        );
    }

    /**
     * @param array|Account[] $accounts
     *
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \LogicException
     * @throws \Symfony\Component\Security\Acl\Exception\AclAlreadyExistsException
     */
    private function setOwnerAcl(array $accounts): void
    {
        foreach ($accounts as $account) {
            try {
                $aclProvider = $this->getContainer()->get('security.acl.provider');
                $acl = $aclProvider->createAcl(ObjectIdentity::fromDomainObject($account));
                $acl->insertObjectAce(UserSecurityIdentity::fromAccount($account), MaskBuilder::MASK_OWNER);
                $aclProvider->updateAcl($acl);
            } catch (\Exception $e) {
                continue;
            }
        }
    }
}
