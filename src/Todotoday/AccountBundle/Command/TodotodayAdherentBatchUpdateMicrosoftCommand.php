<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 27/01/2017
 * Time: 11:52
 */

namespace Todotoday\AccountBundle\Command;

use Actiane\ToolsBundle\Traits\OutputFormatLoggerTrait;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Exception as SFDException;
use Todotoday\AccountBundle\Entity\Adherent;
use Actiane\PaymentBundle\Enum\PaymentTypeEnum;
use Todotoday\AccountBundle\Enum\PaymentStatusEnum;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Class TodotodayAdherentBatchUpdateMicrosoftCommand
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle\Command
 * @subpackage Todotoday\AccountBundle\Command
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class TodotodayAdherentBatchUpdateMicrosoftCommand extends ContainerAwareCommand
{
    use OutputFormatLoggerTrait;

    /**
     * {@inheritdoc}
     * @throws InvalidArgumentException
     */
    protected function configure()
    {
        $this->setName('tdtd:account:batch-update-microsoft')
            ->setDescription('Update Microsoft infos in batch')
            ->addArgument('offset', InputArgument::OPTIONAL, 'Offset', 0);
    }

    /**
     * {@inheritdoc}
     * @throws InvalidArgumentException
     * @throws \LogicException
     * @throws SFDException\ServiceCircularReferenceException
     * @throws SFDException\ServiceNotFoundException
     * @throws \InvalidArgumentException
     * @throws \Psr\Log\InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->initFormat($output);
        $adherentManager = $this->getContainer()->get('todotoday.account.services.adherent_manager');
        $adherentManager->setOutput($output);
        $offset = $input->getArgument('offset');

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $accounts = $em->getRepository(Adherent::class)->findBy(
            [
                'paymentTypeSelected' => [PaymentTypeEnum::STRIPE],
                'paymentMethodStatus' => [PaymentStatusEnum::VALID],
            ],
            null,
            3000,
            $offset
        );

        foreach ($accounts as $account) {
            if (!!$account->getIdStripe()) {
                if ($adherentManager->updateMicrosoftStripeId($account)) {
                    $output->writeln(
                        '<info>[Stripe ID updated]</info> : '
                        . $account->getCustomerAccount() . ' => '
                        . $account->getIdStripe()
                    );
                }
            }
        }
    }
}
