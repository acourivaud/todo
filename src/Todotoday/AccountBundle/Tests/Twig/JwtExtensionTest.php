<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 20/03/17
 * Time: 10:39
 */

namespace Todotoday\AccountBundle\Tests\Twig;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAccountData;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\AccountBundle\Twig\JwtExtension;

/**
 * Class JwtExtensionTest
 *
 * @package Todotoday\AccountBundle\Tests\Twig
 */
class JwtExtensionTest extends WebTestCase
{
    /**
     * @dataProvider accountProvider
     * @small
     *
     * @param $accountSlug
     */
    public function testGetJwt($accountSlug)
    {
        $requestStack = $this->getContainer()->get('request_stack');

        $twigExtension = $this->getJwtExtension();
        /** @var Account $account */
        $account = self::getFRR()->getReference($accountSlug);
        $accountType = (new \ReflectionClass($account))->getShortName();
        $this->tokenAs($account, 'main');
        $domain = $this->getContainer()->getParameter('domain');
        $requestStack->push(Request::create('http://demo0.' . $domain));
        $token = json_decode($twigExtension->getJwt(), true);
        $this->assertNotNull($token);
        $this->assertEquals($account->getUsername(), $token['user']);
        $this->assertEquals($accountType, $token['user_type']);
        $this->assertNotNull($token['jwt']);
        $this->assertNotNull($token['refresh_token']);
        $this->assertArrayHasKey('locale', $token);
    }

    /**
     * @small
     */
    public function testGetJwtAnonymous()
    {
        $requestStack = $this->getContainer()->get('request_stack');
        $twigExtension = $this->getJwtExtension();

        $domain = $this->getContainer()->getParameter('domain');
        $requestStack->push(Request::create('http://demo0.' . $domain));
        $token = json_decode($twigExtension->getJwt(), true);
        $this->assertNotNull($token);
        $this->assertArrayHasKey('locale', $token);
    }

    /**
     * @return array
     */
    public function accountProvider()
    {
        $provider = array();
        $accounts = array_merge(
            LoadAccountData::getAllAccountsSlugs(),
            array('microsoft', 'noosphere', 'admin', 'root')
        );
        foreach ($accounts as $account) {
            $provider[$account] = array($account);
        }

        return $provider;
    }

    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAccountData::class,
        );
    }

    /**
     * @return JwtExtension
     */
    private function getJwtExtension(): JwtExtension
    {
        $jwtTokenManager = $this->getContainer()->get('lexik_jwt_authentication.jwt_manager');
        $jwtSuccessHanlder = $this->getContainer()->get('lexik_jwt_authentication.handler.authentication_success');
        $tokenStorage = $this->getContainer()->get('security.token_storage');
        $refreshManager = $this->getContainer()->get('gesdinet.jwtrefreshtoken.refresh_token_manager');
        $requestStack = $this->getContainer()->get('request_stack');
        $domainContext = $this->getContainer()->get('todotoday.core.domain_context');
        $securityChecker = $this->getContainer()->get('security.authorization_checker');

        $twigExtension = new JwtExtension(
            $jwtTokenManager,
            $jwtSuccessHanlder,
            $tokenStorage,
            $refreshManager,
            $requestStack,
            $domainContext,
            $securityChecker
        );

        return $twigExtension;
    }
}
