<?php declare(strict_types=1);

/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 06/11/17
 * Time: 13:15
 *
 * @category   Todo-Todev
 *
 * @package    ${NAMESPACE}
 *
 * @subpackage ${NAMESPACE}
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\AccountBundle\Tests\Event;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAccountData;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyData;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class LoginSuccessHandlerTest
 */
class LoginSuccessHandlerTest extends WebTestCase
{
    /**
     * @dataProvider loginAgencyProvider
     * @small
     *
     * @param string $agencySlug
     * @param string $adherentSlug
     * @param int    $expectedStatusCode
     */
    public function testLoginAgency(string $agencySlug, string $adherentSlug, int $expectedStatusCode)
    {
        /** @var Agency $agency1 */
        $agency = self::getFRR()->getReference($agencySlug);
        /** @var Adherent $adherent1 */
        $adherent = self::getFRR()->getReference($adherentSlug);

        $client = $this->makeClient();
        $baseUrl = 'http://' . $agency->getSlug() . '.todotoday.lol';
        $crawler = $client->request('GET', $baseUrl . $this->getUrl('fos_user_security_login'));
        $this->assertStatusCode(200, $client);

        $form = $crawler->selectButton('_submit')->form();
        $form->setValues(['_username' => $adherent->getUsername(), '_password' => $adherent->getUsername()]);
        $client->submit($form);
        // we should get 302 when submitting form
        $this->assertStatusCode(302, $client);
        $client->request('GET', $baseUrl . $this->getUrl('account.profile_index'));
        // we should get 200 if we logged
        $this->assertStatusCode($expectedStatusCode, $client);
    }

    /**
     * @dataProvider loginAdminProvider
     * @small
     *
     * @param string $userSlug
     * @param string $password
     * @param int    $expectedStatusCode
     */
    public function testAccessAdmin(string $userSlug, string $password, int $expectedStatusCode)
    {
        /** @var Account $adherent1 */
        $adherent = self::getFRR()->getReference($userSlug);

        $client = $this->makeClient();
        $baseUrl = 'http://www.todotoday.lol';
        $crawler = $client->request('GET', $baseUrl . $this->getUrl('fos_user_security_login'));
        $this->assertStatusCode(200, $client);

        $form = $crawler->selectButton('_submit')->form();
        $form->setValues(['_username' => $adherent->getUsername(), '_password' => $password]);
        $client->submit($form);
        // we should get 302 when submitting form
        $this->assertStatusCode(302, $client);
        $client->request('GET', $baseUrl . $this->getUrl('sonata_admin_dashboard'));
        // we should get 200 if we logged
        $this->assertStatusCode($expectedStatusCode, $client);
    }

    /**
     * @return array
     */
    public function loginAgencyProvider(): array
    {
        return
            [
                'agency1-adherent1' => ['agency_demo1', 'adherent1', 200],
                'agency1-adherent2' => ['agency_demo1', 'adherent2', 302],
                'agency2-adherent2' => ['agency_demo2', 'adherent2', 200],
                'agency1-adherent12' => ['agency_demo0', 'adherent12', 200],
                'agency2-adherent12' => ['agency_demo1', 'adherent12', 200],
                'agency3-adherent12' => ['agency_demo2', 'adherent12', 302],
            ];
    }

    /**
     * @return array
     */
    public function loginAdminProvider(): array
    {
        return [
            'adherent0' => ['adherent0', 'adherent0', 302],
            'concierge2' => ['concierge2', 'concierge2', 200],
            'root' => ['root', 'tdtd2017!', 200],
        ];
    }

    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return [LoadAgencyData::class, LoadAccountData::class];
    }
}
