<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 30/03/17
 * Time: 11:46
 */

namespace Todotoday\AccountBundle\Tests\Services;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAccountData;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAdherentLinkedMicrosoftData;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Entity\Api\Microsoft\Account;
use Todotoday\AccountBundle\Entity\LinkAccountAgency;
use Todotoday\AccountBundle\Exceptions\AdherentAlreayMigratedException;
use Todotoday\AccountBundle\Exceptions\AdherentNotFoundInAxException;
use Todotoday\AccountBundle\Services\AdherentManager;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class AdherentManagerTest
 * @package Todotoday\AccountBundle\Tests\Services
 */
class AdherentManagerTest extends WebTestCase
{
    /**
     * @var AdherentManager
     */
    private $manager;

    /**
     * @small
     */
    public function testLogoutAdherentWithOneSignalIdNull(): void
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent_linked_arevalta');
        $adherent->setOneSignalId('blabla');
        $em->persist($adherent);
        $em->flush();
        $em->clear();
        $this->manager->adherentLogOut($adherent);
        $result = $em->getRepository('TodotodayAccountBundle:Adherent')
            ->find($adherent->getId());

        $this->assertNull($result->getOneSignalId());
    }

    /**
     * @dataProvider providerOneSignal
     * @small
     *
     * @param null|string $value
     */
    public function testOneSignalUpdateAdherentFoundMicrosoft(?string $value): void
    {
        $adherent = new Adherent();
        $adherent->setOneSignalId($value);
        $repoMicrosoft = $this->getContainer()->get('todotoday.account.repository.api.account');
        /** @var Account[] $accountMicrosoft */
        $accountMicrosoft = $repoMicrosoft->findAll(1);
        $adherent->setDataAreaId($accountMicrosoft[0]->getDataAreaId())
            ->setCustomerAccount($accountMicrosoft[0]->getCustomerAccount());
        /** @var Account $result */
        $result = $this->manager->updateMicrosoftOneSignal($adherent);
        $this->assertNotNull($result);
        $this->assertEquals($result->getOneSignalId(), $value);
    }

    /**
     * @small
     */
    public function testOneSignalUpdateAdherentNotFoundMicrosoft(): void
    {
        $adherent = new Adherent();
        $adherent->setCustomerAccount('toto');
        $result = $this->manager->updateMicrosoftOneSignal($adherent);

        $this->assertNull($result);
    }

    /**
     * @small
     */
    public function testMigrateAdherentNotFoundInAx()
    {
        $this->expectException(AdherentNotFoundInAxException::class);

        /** @var Agency $agency */
        $agency = self::getFRR()->getReference('agency_arevalta');

        $adherent = (new Adherent())
            ->setCustomerAccount('bidon')
            ->setDataAreaId('EPA')
            ->setEmail('toto@toto.fr');

        $manager = $this->getContainer()->get('todotoday.account.services.adherent_manager');
        $manager->migrateAdherent($adherent, $agency);
    }

    /**
     * @small
     */
    public function testMigrateAdherentAlreayExist()
    {
        $this->expectException(AdherentAlreayMigratedException::class);

        /** @var Agency $agency */
        $agency = self::getFRR()->getReference('agency_arevalta');

        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent_linked_arevalta');

        $manager = $this->getContainer()->get('todotoday.account.services.adherent_manager');
        $manager->migrateAdherent($adherent, $agency);
    }

    /**
     * setUp method
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->manager = $this->getContainer()->get('todotoday.account.services.adherent_manager');
    }

    /**
     * @return array
     */
    public function providerOneSignal(): array
    {
        return array(
            'test' => array('test'),
            'setNull' => array(''),
        );
    }

    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAdherentLinkedMicrosoftData::class,
        );
    }
}
