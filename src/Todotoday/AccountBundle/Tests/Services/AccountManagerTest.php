<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 24/01/2017
 * Time: 11:04
 */

namespace Todotoday\AccountBundle\Tests\Services;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit_Framework_MockObject_MockObject;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAccountData;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Entity\Concierge;
use Todotoday\AccountBundle\Services\AccountManager;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyData;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class AccountManagerTest
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Tests\Services
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class AccountManagerTest extends WebTestCase
{
    /**
     * @var \Swift_Message[]
     */
    protected $currentMessages;

    /**
     * @var AccountManager
     */
    protected $accountManager;

    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return string[]
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAccountData::class,
            LoadAgencyData::class,
        );
    }

    /**
     * Do accountProvider
     *
     * @return array[]
     */
    public function accountProvider(): array
    {
        $accountsSlugs = LoadAccountData::getAllAccountsSlugs();
        $newAccountsSlugs = array();

        foreach ($accountsSlugs as $accountSlug) {
            $newAccountsSlugs[] = array(
                $accountSlug,
            );
        }

        return $newAccountsSlugs;
    }

    /**
     * Do accountTypeProvider
     *
     * @return array[]
     */
    public function accountTypeProvider(): array
    {
        $params = array();
        /** @var EntityManagerInterface $em */
        $em = $this->get('doctrine.orm.entity_manager');
        $keys = array_keys($em->getClassMetadata(Account::class)->discriminatorMap);
        foreach ($keys as $key) {
            $params[] = array(
                $key,
            );
        }

        return $params;
    }

    /**
     * Do AgencyProvider
     *
     * @return array[]
     */
    public function agencyAccountProvider(): array
    {
        $accountsSlugs = LoadAccountData::getAllAccountsSlugs();
        $agenciesSlugs = LoadAgencyData::getAgenciesSlug();
        $newAgenciesSlugs = array();

        foreach ($agenciesSlugs as $agencySlug) {
            foreach ($accountsSlugs as $accountSlug) {
                $newAgenciesSlugs[] = array(
                    'agency_' . $agencySlug,
                    $accountSlug,
                );
            }
        }

        return $newAgenciesSlugs;
    }

    /**
     * Do send
     *
     * @param \Swift_Message $message
     *
     * @return int
     */
    public function send(\Swift_Message $message): int
    {
        $this->currentMessages[] = $message;

        return count((array) $message->getTo())
            + count((array) $message->getCc())
            + count((array) $message->getBcc());
    }

    /**
     * Do testCreateAccount
     *
     * @small
     * @dataProvider accountTypeProvider
     *
     * @param string $type
     *
     * @return void
     */
    public function testCreateAccount(string $type): void
    {
        $account = $this->accountManager->createAccount($type);

        static::assertNotNull($account);
        static::assertInstanceOf(Account::class, $account);
    }

    /**
     * Do testCreateAccountFail
     *
     * @small
     *
     * @return void
     */
    public function testCreateAccountFail(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        $this->accountManager->createAccount('fail');
    }

    /**
     * Do testCreateRegistrationToken
     *
     * @small
     * @dataProvider accountProvider
     *
     * @param string $accountRefId
     *
     * @return void
     */
    public function testCreateRegistrationToken(string $accountRefId): void
    {
        /** @var Account $account */
        $account = static::getFRR()->getReference($accountRefId);

        $this->accountManager->createRegistrationToken($account);

        static::assertNotNull($account->getRegistrationToken());
        static::assertAttributeLessThanOrEqual(new \DateTime(), 'registrationTokenRequestedAt', $account);
    }

    /**
     * Do testGetFormType
     *
     * @small
     *
     * @return void
     */
    public function testGetFormType(): void
    {
        static::assertNotNull($this->accountManager->getFormType(new Adherent()));
        static::assertNotNull($this->accountManager->getFormType(new Concierge()));
    }

    /**
     * Do testSendEmailRegistrationToken
     *
     * @small
     * @dataProvider agencyAccountProvider
     * @depends      testCreateRegistrationToken
     *
     * @param string $agencyRefId
     * @param string $accountRefId
     *
     * @return void
     */
    public function testSendEmailRegistrationToken(string $agencyRefId, string $accountRefId): void
    {
        /** @var Agency $agency */
        $agency = static::getFRR()->getReference($agencyRefId);

        /** @var Account $account */
        $account = static::getFRR()->getReference($accountRefId);
        static::assertNotNull($account->getRegistrationToken());
        static::assertNotNull($account->getRegistrationTokenRequestedAt());

        $requestStack = $this->getContainer()->get('request_stack');
        $requestStack->push(Request::create('http://' . $agency->getSlug() . '.todotoday.local'));

        $this->accountManager->sendEmailRegistrationToken($account, $agency);

        $spool = $this->get('swiftmailer.spool');
        /** @var PHPUnit_Framework_MockObject_MockObject|\Swift_Transport $transport */
        $transport = $this->createMock(\Swift_Transport::class);
        $transport->method('send')->willReturnCallback(array($this, 'send'));
        $spool->flushQueue($transport);

        // Check that an email was sent
        static::assertCount(1, $this->currentMessages);

        $message = $this->currentMessages[0];

        $urlRegistration = $this->getUrl(
            'todotoday_account_registration_confirmation',
            array(
                'token' => $account->getRegistrationToken(),
            ),
            UrlGeneratorInterface::ABSOLUTE_PATH
        );

        $domain = $this->getContainer()->getParameter('domain');

        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off'
            || (!empty($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] === 443))
            ? 'https://'
            : 'http://';

        $urlRegistration = $protocol . $agency->getSlug() . '.' . $domain . $urlRegistration;

        // Asserting email data
        static::assertInstanceOf(\Swift_Message::class, $message);
        static::assertNotNull($message->getSubject());
        //        static::assertContains($agency->getName(), $message->getSubject());
        static::assertEquals('no-reply@conciergerie-todotoday.com', key($message->getFrom()));
        static::assertEquals($account->getEmail(), key($message->getTo()));
        static::assertNotNull($message->getBody());
        static::assertContains($urlRegistration, $message->getBody());
        static::assertEquals('multipart/alternative', $message->getContentType());
        $hasHtmlContentType = false;
        foreach ($message->getChildren() as $child) {
            static::assertNotNull($child->getBody());
            static::assertNotNull($child->getContentType());
            static::assertContains($urlRegistration, $child->getBody());
            if ($child->getContentType() === 'text/html') {
                $hasHtmlContentType = true;
            }
        }

        static::assertNotFalse($hasHtmlContentType);
    }

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->accountManager = $this->get('todotoday.account.services.account_manager');
    }
}
