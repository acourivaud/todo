<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: riwan
 * Date: 28/11/17
 * Time: 09:51
 */

namespace Todotoday\AccountBundle\Tests\Services;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyData;
use Todotoday\CoreBundle\Entity\Agency;

class EmailWhitelistManagerTest extends WebTestCase
{
    /**
     * @small
     */
    public function testIsEmailValidWhitelistSuccessWithNoRequestStack(): void
    {
        $whitelistManager = $this->getContainer()->get('todotoday.account.services.email_whitelist_manager');
        $result = $whitelistManager->isEmailValidWhitelist('test@test.com');
        $this->assertTrue($result);
    }

    /**
     * @dataProvider getDataProvider
     * @small
     *
     * @param string $emailToTest
     * @param bool   $isSuccess
     */
    public function testIsEmailValidWhitelistSuccess(string $emailToTest, bool $isSuccess): void
    {
        /** @var Agency $agency */
        $agency = self::getFRR()->getReference('agency_demo0');
        $domain = $this->getContainer()->getParameter('domain');
        $requestStack = $this->getContainer()->get('request_stack');
        $requestStack->push(Request::create('http://' . $agency->getSlug() . '.' . $domain));

        $whitelistManager = $this->getContainer()->get('todotoday.account.services.email_whitelist_manager');
        $result = $whitelistManager->isEmailValidWhitelist($emailToTest);
        $this->assertEquals($isSuccess, $result);
    }

    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAgencyData::class,
        );
    }

    /**
     * @return array
     */
    public function getDataProvider(): array
    {
        return array(
            'success' => array(
                'test@actiane.com',
                true,
            ),
            'failed' => array(
                'test@invalid.com',
                false,
            ),
            'not_email' => array(
                'jenesuispasunemail',
                false,
            ),
        );
    }
}
