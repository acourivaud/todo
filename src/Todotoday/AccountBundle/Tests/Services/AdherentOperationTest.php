<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 09/10/17
 * Time: 17:30
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\AccountBundle\Tests\Services
 *
 * @subpackage Todotoday\AccountBundle\Tests\Services
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\AccountBundle\Tests\Services;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAccountData;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Exceptions\AdherentMigrateEpaDontMatchException;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyData;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class AdherentOperationTest
 */
class AdherentOperationTest extends WebTestCase
{
    /**
     * @small
     */
    public function testAdherentOperationSuccess()
    {
        /** @var Agency $agencyFrom */
        $agencyFrom = self::getFRR()->getReference('agency_demo0');
        /** @var Agency $agencyTo */
        $agencyTo = self::getFRR()->getReference('agency_demo1');

        $manager = $this->getContainer()->get('todotoday.account.services.adherent_operation');
        $manager->giveAdherentAccessToAnotherAgency($agencyFrom, $agencyTo);

        /** @var Adherent $adh */
        $adh = self::getFRR()->getReference('adherent0');
        $this->assertCount(2, $adh->getLinkAgencies());
    }

    /**
     * @small
     */
    public function testAdherentOperationFail()
    {
        $this->expectException(AdherentMigrateEpaDontMatchException::class);
        /** @var Agency $agencyFrom */
        $agencyFrom = self::getFRR()->getReference('agency_demo0');
        /** @var Agency $agencyTo */
        $agencyTo = self::getFRR()->getReference('agency_demo1');
        $agencyTo->setDataAreaId('ECH');

        $manager = $this->getContainer()->get('todotoday.account.services.adherent_operation');
        $manager->giveAdherentAccessToAnotherAgency($agencyFrom, $agencyTo);
    }

    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return [
            LoadAgencyData::class,
            LoadAccountData::class,
        ];
    }
}
