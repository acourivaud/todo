<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 13/02/17
 * Time: 11:27
 */

namespace Todotoday\AccountBundle\Tests\ControllerAdmin;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyData;

/**
 * Class AccountAdminControllerTest
 * @package Todotoday\AccountBundle\Tests\ControllerAdmin
 */
class AccountAdminControllerTest extends WebTestCase
{
    /**
     * Do testHomepage
     *
     * Test if homepage for www.%domain%/login works
     *
     * @dataProvider agencyProvider
     *
     * @small
     *
     * @param string $agenySlug
     *
     * @return void
     */
    public function testHomepage(string $agenySlug)
    {
        $url = 'http://' . $agenySlug . '.todotoday.loczadzadzdal';
        $url .= $this->getUrl('fos_user_security_login');
        $client = $this->makeClient();
        $client->request('GET', $url);
        $this->assertStatusCode('200', $client);
    }

    /**
     * Do AgencyProvider
     *
     * @return array[]
     */
    public function agencyProvider(): array
    {
        $agencysSlugs = LoadAgencyData::getAgenciesSlug();
        $newAgencysSlugs = array();

        foreach ($agencysSlugs as $agencySlug) {
            $newAgencysSlugs[$agencySlug] = array(
                $agencySlug,
            );
        }
//        $newAgencysSlugs['www'] = array('www');

        return $newAgencysSlugs;
    }

    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(LoadAgencyData::class);
    }
}
