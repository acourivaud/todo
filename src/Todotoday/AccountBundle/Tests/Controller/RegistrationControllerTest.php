<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 04/04/2017
 * Time: 16:30
 */

namespace Todotoday\AccountBundle\Tests\Controller;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyData;

/**
 * Class RegistrationControllerTest
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Tests\Controller
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class RegistrationControllerTest extends WebTestCase
{
    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAgencyData::class,
        );
    }

    /**
     * Do agencyProvider
     *
     * @return array
     */
    public function agencyProvider(): array
    {
        $agencies = LoadAgencyData::getAgenciesSlug();
        $agencyList = array();

        foreach ($agencies as $agency) {
            $agencyList[] = array($agency => $agency);
        }

        return $agencyList;
    }

    /**
     * Do testNewAction
     *
     * @dataProvider agencyProvider
     *
     * @param string $agencySlug
     *
     * @return void
     */
    public function testNewAction(string $agencySlug)
    {
        $context = $this->getContainer()->get('router')->getContext();
        $context->setHost($agencySlug . '.youpi.tata');

        $client = static::createClient();

        $crawler = $client->request(
            'GET',
            $this->getUrl(
                'todotoday_account_registration_new',
                array(),
                UrlGeneratorInterface::ABSOLUTE_URL
            )
        );

        $this->assertStatusCode(200, $client);

        // Try again with with the fields filled out.
        $form = $crawler->selectButton('Je m\'inscris')->form();
        $form->setValues(
            [
                'todotoday_account_bundle_account[email]' => $agencySlug . '@actiane.com',
                'todotoday_account_bundle_account[firstName]' => 'Toto',
                'todotoday_account_bundle_account[lastName]' => 'Tata',
                'todotoday_account_bundle_account[customerGroup]' => 'ADH',
            ]
        );
        $client->submit($form);
        $this->assertStatusCode(302, $client);
    }
}
