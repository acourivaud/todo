<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 23/06/2017
 * Time: 16:06
 */

namespace Todotoday\AccountBundle\Tests\Controller\Profile;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAdherentLinkedMicrosoftData;
use Todotoday\AccountBundle\Entity\Adherent;

/**
 * Class ProfileControllerTest
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Tests\Controller\Profile
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class ProfileControllerTest extends WebTestCase
{
    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAdherentLinkedMicrosoftData::class,
        );
    }

    /**
     * Do getRoutes
     *
     * @return string[]
     */
    public static function getRoutes(): array
    {
        return [
            'account.profile_extra',
            'account.profile_index',
            'account.profile_payment',
            'account.profile_option',
        ];
    }

    /**
     * Do accountProvider
     *
     * @return array
     */
    public function accountProvider(): array
    {
        $ids = array();

        foreach (LoadAdherentLinkedMicrosoftData::getAdherentsSlug() as $agency => $value) {
            foreach (static::getRoutes() as $route) {
                $ids[$route . ' # ' . $value] = [
                    $value,
                    'http://' . $agency . '.tdtd.local' . $this->getUrl($route),
                ];
            }
        }

        return $ids;
    }

    /**
     * @dataProvider accountProvider
     * Do testExtraAction
     *
     * @param string $adhRef
     *
     * @param string $url
     *
     * @return void
     */
    public function testGetAction(string $adhRef, string $url): void
    {
        /** @var Adherent $adh */
        $adh = static::getFRR()->getReference($adhRef);

        $this->loginAs($adh, 'main');

        $client = $this->makeClient();

        $client->request('GET', $url);

        static::assertSame(200, $client->getResponse()->getStatusCode());
    }
}
