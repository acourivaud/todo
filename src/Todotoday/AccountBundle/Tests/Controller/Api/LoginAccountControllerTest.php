<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 31/01/2017
 * Time: 11:29
 */

namespace Todotoday\AccountBundle\Tests\Controller\Api;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Client;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAccountData;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAdherentLinkedMicrosoftData;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyLinkedMicrosoft;

/**
 * Class AccountControllerTest
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Tests\Controller\Api
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class LoginAccountControllerTest extends WebTestCase
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $baseUrl;

    /**
     * {@inheritdoc}
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAccountData::class,
            LoadAdherentLinkedMicrosoftData::class,
        );
    }

    /**
     * @return array[]
     */
    public function accountProvider(): array
    {
        $account = [];
        $accountSlugs = LoadAccountData::getAllAccountsSlugs();

        foreach ($accountSlugs as $slug) {
            $account[$slug] = array($slug, $slug);
        }
//
//        foreach (LoadAgencyLinkedMicrosoft::getAgenciesLinkedSlug() as $slug => $name) {
//            $login = $slug . '@actiane.com';
//            $account[$login] = array($login, 'test');
//        }

        $account['admin'] = array('admin', 'tdtd2017!');
        $account['root'] = array('root', 'tdtd2017!');
        $account['noosphere'] = array('noosphere', 'noosphere');
        $account['microsoft'] = array('microsoft', '=#eJsK8$spyjAbVS');

        return $account;
    }

    /**
     * SetUp Method
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->client = static::createClient();
        $this->baseUrl = 'http://api.' . $this->getContainer()->getParameter('domain');
    }

    /**
     * Do test login failed
     *
     * @small
     * @return void
     */
    public function testLoginFailed(): void
    {
        $client = static::createClient();

        $data = array(
            '_username' => 'fail',
            '_password' => 'fail',
        );

        $url = $this->baseUrl . $this->getUrl('fos_user_security_check');
        $client->request('POST', $url, $data);
        $response = json_decode($client->getResponse()->getContent());
        $this->assertStatusCode(401, $client);
        static::assertEquals('Bad credentials', $response->message);
    }

    /**
     * Do testGetAction
     *
     * @dataProvider accountProvider
     *
     * @small
     *
     * @param string $slugAccount
     *
     * @param string $pwd
     *
     * @return void
     */
    public function testLoginSuccess(string $slugAccount, string $pwd): void
    {
        $data = array(
            '_username' => $slugAccount,
            '_password' => $pwd,
        );

        $urlCheck = $this->baseUrl . $this->getUrl('fos_user_security_check');
        $this->client->request('POST', $urlCheck, $data);
        $this->assertStatusCode(200, $this->client);
        $responseData = json_decode($this->client->getResponse()->getContent());
        static::assertObjectHasAttribute('token', $responseData);
        static::assertObjectHasAttribute('refresh_token', $responseData);

        $dataRefresh = array(
            'refresh_token' => $responseData->{'refresh_token'},
        );

        $urlRefresh = $this->getUrl('gesdinet_jwt_refresh_token');
        $this->client->request('POST', $urlRefresh, $dataRefresh);
        $this->assertStatusCode(200, $this->client);
        static::assertObjectHasAttribute('token', $responseData);
        static::assertObjectHasAttribute('refresh_token', $responseData);
    }
}
