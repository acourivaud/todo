<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 08/06/2017
 * Time: 18:43
 */

namespace Todotoday\AccountBundle\Tests\Controller\Api;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAdherentLinkedMicrosoftData;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Entity\Api\Microsoft\ContactPerson;
use Todotoday\AccountBundle\Repository\Api\Microsoft\ContactPersonRepository;

/**
 * Class AdherentContactPersonApiControllerTest
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Tests\Controller\Api
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class AdherentContactPersonApiControllerTest extends WebTestCase
{
    /**
     * @var ContactPerson
     */
    protected static $contactPerson;

    /**
     * @var ContactPerson[]
     */
    protected static $contactPersons;

    /**
     * Do getContactPersonId
     *
     * @param Adherent $adherent
     *
     * @return ContactPerson
     */
    public function getContactPerson(Adherent $adherent): ContactPerson
    {
        if (!static::$contactPerson) {
            /** @var ContactPersonRepository $contactPersonRepo */
            $contactPersonRepo = $this->get('todotoday.account.repository.api.contact_person');

            /** @var ContactPerson[] $contactPersons */
            $contactPersons = $contactPersonRepo->findContactsPersonByAccount(
                $adherent,
                1
            );

            static::$contactPerson = $contactPersons[0];
        }

        return static::$contactPerson;
    }

    /**
     * Do contactPersonIdProvider
     *
     * @param Adherent $adherent
     *
     * @return array|ContactPerson[]
     */
    public function getContactPersons(Adherent $adherent): array
    {
        if (!static::$contactPersons) {
            /** @var ContactPersonRepository $contactPersonRepo */
            $contactPersonRepo = $this->get('todotoday.account.repository.api.contact_person');

            static::$contactPersons = $contactPersonRepo->findContactsPersonByAccount(
                $adherent,
                5
            );
        }

        return static::$contactPersons;
    }

    /**
     * Do testGetAllAction
     *
     * @return string
     */
    public function testPostAction(): string
    {
        /** @var Adherent $adherent */
        $adherent = static::getFRR()->getReference('adherent_linked_arevalta');
        $this->loginAs($adherent, 'api');

        $client = $this->makeClient();

        $client->request(
            'POST',
            $this->getUrl('todotoday.account.api.adherent.info.post'),
            array('firstName' => 'test', 'lastName' => 'toto', 'middleName' => 'mid')
        );

        $response = $client->getResponse();
        static::assertSame(200, $response->getStatusCode());
        $content = json_decode($response->getContent(), true);
        static::assertArrayHasKey('contact_person_id', $content);

        return $content['contact_person_id'];
    }

    /**
     * Do testGetAction
     */
    public function testGetAction(): void
    {
        /** @var Adherent $adherent */
        $adherent = static::getFRR()->getReference('adherent_linked_arevalta');
        $this->loginAs($adherent, 'api');

        $client = $this->makeClient();

        $client->request(
            'GET',
            $this->getUrl(
                'todotoday.account.api.adherent.info.get',
                array('id' => $this->getContactPerson($adherent)->getContactPersonId())
            )
        );

        $response = $client->getResponse();
        static::assertSame(200, $response->getStatusCode());
        $content = json_decode($response->getContent(), true);
        static::assertArrayHasKey('contact_person_id', $content);
    }

    /**
     * Do testGetAction
     */
    public function testPatchAction(): void
    {
        /** @var Adherent $adherent */
        $adherent = static::getFRR()->getReference('adherent_linked_arevalta');
        $this->loginAs($adherent, 'api');

        $client = $this->makeClient();

        $client->request(
            'PATCH',
            $this->getUrl(
                'todotoday.account.api.adherent.info.patch',
                array('id' => $this->getContactPerson($adherent)->getContactPersonId())
            ),
            array('middleName' => 'test_patch')
        );

        $response = $client->getResponse();
        static::assertSame(200, $response->getStatusCode());
        $content = json_decode($response->getContent(), true);
        static::assertArrayHasKey('contact_person_id', $content);
        static::assertNotNull($content['first_name']);
        static::assertNotNull($content['last_name']);
        static::assertNotNull($content['middle_name']);
    }

    /**
     * @group ignore
     * @small
     * Do testGetAction
     */
    public function testPutAction(): void
    {
        /** @var Adherent $adherent */
        $adherent = static::getFRR()->getReference('adherent_linked_arevalta');
        $this->loginAs($adherent, 'api');

        $client = $this->makeClient();

        $client->request(
            'PUT',
            $this->getUrl(
                'todotoday.account.api.adherent.info.put',
                array('id' => $this->getContactPerson($adherent)->getContactPersonId())
            ),
            array('lastName' => 'testa_put', 'firstName' => 'testi_put')
        );

        $response = $client->getResponse();
        static::assertSame(200, $response->getStatusCode());
        $content = json_decode($response->getContent(), true);
        static::assertArrayHasKey('contact_person_id', $content);
        static::assertNotNull($content['first_name']);
        static::assertNotNull($content['last_name']);
        static::assertNull($content['middle_name']);
    }

    /**
     * @group ignore
     * @small
     * Do testGetAction
     */
    public function testDeleteAction(): void
    {
        /** @var Adherent $adherent */
        $adherent = static::getFRR()->getReference('adherent_linked_arevalta');
        $this->loginAs($adherent, 'api');

        $client = $this->makeClient();

        $client->request(
            'DELETE',
            $this->getUrl(
                'todotoday.account.api.adherent.info.delete',
                array('id' => $this->getContactPerson($adherent)->getContactPersonId())
            )
        );

        $response = $client->getResponse();
        static::assertSame(204, $response->getStatusCode());
    }

    /**
     * Do testGetAllAction
     *
     * @return void
     */
    public function testPostAllAction(): void
    {
        /** @var Adherent $adherent */
        $adherent = static::getFRR()->getReference('adherent_linked_arevalta');
        $this->loginAs($adherent, 'api');

        $client = $this->makeClient();

        $client->request(
            'POST',
            $this->getUrl('todotoday.account.api.adherent.info.post_all'),
            array(
                'contactPersons' => array(
                    array('firstName' => 'test1', 'lastName' => 'toto1', 'middleName' => 'mid1'),
                    array('firstName' => 'test2', 'lastName' => 'toto2', 'middleName' => 'mid2'),
                ),
            )
        );

        $response = $client->getResponse();
        static::assertSame(200, $response->getStatusCode());
        /** @var array[] $content */
        $content = json_decode($response->getContent(), true);
        static::assertCount(1, $content);
        static::assertArrayHasKey('contactPersons', $content);
        $contactPersons = $content['contactPersons'];
        static::assertCount(2, $contactPersons);
        static::assertArrayHasKey('contact_person_id', $contactPersons[0]);
        static::assertContains('test1', $contactPersons[0]['first_name']);
        static::assertContains('toto1', $contactPersons[0]['last_name']);
        static::assertContains('mid1', $contactPersons[0]['middle_name']);
        static::assertArrayHasKey('contact_person_id', $contactPersons[1]);
        static::assertContains('test2', $contactPersons[1]['first_name']);
        static::assertContains('toto2', $contactPersons[1]['last_name']);
        static::assertContains('mid2', $contactPersons[1]['middle_name']);
    }

    /**
     * Do testGetAllAction
     *
     * @return void
     */
    public function testPatchAllAction(): void
    {
        /** @var Adherent $adherent */
        $adherent = static::getFRR()->getReference('adherent_linked_arevalta');
        $this->loginAs($adherent, 'api');

        $client = $this->makeClient();

        $localContactPersons = $this->getContactPersons($adherent);
        $nbLocalContactPersons = count($localContactPersons);
        $contactPersonsPatch = array();
        $ids = array();
        foreach ($localContactPersons as $contactPerson) {
            $contactPersonsPatch[] = array(
                'middleName' => 'mid_patch' . $contactPerson->getContactPersonId(),
            );
            $ids[] = $contactPerson->getContactPersonId();
        }

        $client->request(
            'PATCH',
            $this->getUrl('todotoday.account.api.adherent.info.patch_all'),
            array(
                'contactPersons' => $contactPersonsPatch,
                'ids' => $ids,
            )
        );

        $response = $client->getResponse();
        static::assertSame(200, $response->getStatusCode());
        /** @var array[] $content */
        $content = json_decode($response->getContent(), true);
        static::assertCount(1, $content);
        static::assertArrayHasKey('contactPersons', $content);
        $contactPersons = $content['contactPersons'];
        static::assertCount($nbLocalContactPersons, $contactPersons);
        foreach ($localContactPersons as $key => $contactPerson) {
            static::assertArrayHasKey('contact_person_id', $contactPersons[$key]);
            static::assertNotNull($contactPersons[$key]['first_name']);
            static::assertNotNull($contactPersons[$key]['last_name']);
            static::assertContains(
                'mid_patch' . $contactPerson->getContactPersonId(),
                $contactPersons[$key]['middle_name']
            );
        }
    }

    /**
     * @group ignore
     * @small
     * Do testGetAllAction
     *
     * @return void
     */
    public function testDeleteAllAction(): void
    {
        /** @var Adherent $adherent */
        $adherent = static::getFRR()->getReference('adherent_linked_arevalta');
        $this->loginAs($adherent, 'api');

        $client = $this->makeClient();

        $localContactPersons = $this->getContactPersons($adherent);
        $ids = array();
        foreach ($localContactPersons as $contactPerson) {
            $ids[] = $contactPerson->getContactPersonId();
        }

        $client->request(
            'DELETE',
            $this->getUrl('todotoday.account.api.adherent.info.delete_all'),
            array(
                'ids' => $ids,
            )
        );

        $response = $client->getResponse();
        static::assertSame(204, $response->getStatusCode());
    }

    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAdherentLinkedMicrosoftData::class,
        );
    }
}
