<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 09/06/2017
 * Time: 16:47
 */

namespace Todotoday\AccountBundle\Tests\Controller\Api;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAccountData;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAdherentLinkedMicrosoftData;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Entity\Partner;
use Todotoday\AccountBundle\Enum\PaymentStatusEnum;

/**
 * Class AdherentLinkedApiControllerTest
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Tests\Controller\Api
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class AdherentLinkedApiControllerTest extends WebTestCase
{
    /**
     * Do testPutPaymentMethodStatusAction
     *
     * @return void
     */
    public function testPutPaymentMethodStatusAction(): void
    {
        /** @var Adherent $adherent */
        $adherent = static::getFRR()->getReference('adherent_linked_arevalta');
        /** @var Partner $micro */
        $micro = static::getFRR()->getReference('microsoft');
        $this->loginAs($micro, 'api');

        $client = $this->makeClient();

        $client->request(
            'PUT',
            $this->getUrl(
                'todotoday.account.api.account.put_payment_method_status',
                array('customerAccount' => $adherent->getCustomerAccount())
            ),
            array('paymentMethodStatus' => PaymentStatusEnum::PAYMENT_INCIDENT)
        );

        $response = $client->getResponse();

        static::assertSame(200, $response->getStatusCode());
    }

    /**
     * {@inheritdoc}
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAdherentLinkedMicrosoftData::class,
            LoadAccountData::class,
        );
    }
}
