<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 15/02/17
 * Time: 15:56
 */

namespace Todotoday\AccountBundle\Tests\Controller\Api;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Symfony\Component\Security\Core\User\UserInterface;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAccountData;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Enum\PaymentStatusEnum;

/**
 * Class AccountApiControllerTest
 * @package Todotoday\AccountBundle\Tests\Controller\Api
 */
class AccountApiControllerTest extends WebTestCase
{
    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(LoadAccountData::class);
    }

    /**
     * method accountProvider
     *
     * @return array
     */
    public function allAccountProvider(): array
    {
        $accountSlugs = LoadAccountData::getAllAccountsSlugs();

        $data = [];
        foreach ($accountSlugs as $slug) {
            $data[$slug] = array($slug);
        }
        $data['admin'] = array('admin');
        $data['noosphere'] = array('noosphere');
        $data['microsoft'] = array('microsoft');

        return $data;
    }

    /**
     * Provider returning onlye adherent and concierge account
     *
     * @return array
     */
    public function notPartnerProvider(): array
    {
        $accountSlugs = LoadAccountData::getAllAccountsSlugs();
        $data = [];
        foreach ($accountSlugs as $slug) {
            $data[$slug] = array($slug);
        }

        return $data;
    }

    /**
     * @return array
     */
    public function getAllAcountExceptMicrosoft(): array
    {
        $provider = [];
        foreach (LoadAccountData::getAllAccountsSlugs() as $slug) {
            if ($slug !== 'microsoft') {
                $provider[$slug] = array($slug);
            }
        }

        return $provider;
    }

    /**
     * @return array
     */
    public function partnerCreateAdherent(): array
    {
        return array(
            'microsoft' => array('microsoft'),
        );
    }

    /**
     * partnerProvider
     *
     * @return array
     */
    public function partnerProvider(): array
    {
        return array(
            'noosphere' => array('noosphere'),
            'microsoft' => array('microsoft'),
        );
    }

    /**
     * @dataProvider partnerCreateAdherent
     * @small
     *
     * @param string $slug
     */
    public function testCreateAdherent(string $slug): void
    {
        $iteration = 1;
        $type = 'adherent';
        $form = array(
            'firstName' => 'test',
            'lastName' => 'test',
            'email' => 'test' . $iteration . $slug . '@actiane.com',
        );
        if ($type === 'adherent' && $slug === 'microsoft') {
            $form['customerAccount'] = 'toto';
        }
        $url = $this->getUrl('todotoday.account.api.account.post', array('type' => $type));
        /** @var UserInterface $account */
        $account = static::getFRR()->getReference($slug);
        $this->loginAs($account, 'api');
        $client = $this->makeClient();
        $client->request('POST', $url, $form, array(), array('HTTP_AGENCY' => 'demo0'));
        $response = json_decode($client->getResponse()->getContent());
        $this->assertStatusCode(200, $client);
        static::assertEquals($type, $response->discr);
    }

    /**
     * @dataProvider notPartnerProvider
     * @small
     *
     * @param string $slug
     */
    public function testCreateAdherentDenied(string $slug): void
    {
        $this->loginAs(static::getFRR()->getReference($slug), 'api');
        $typeAccount = array('concierge', 'adherent');
        foreach ($typeAccount as $type) {
            $url = $this->getUrl('todotoday.account.api.account.post', array('type' => $type));
            $client = $this->makeClient();
            $client->request('POST', $url);
            $this->assertStatusCode(403, $client);
        }
    }

    /**
     * Test create adherent with no agency in header
     *
     * @small
     */
    public function testCreateAdherentWithNoAgency(): void
    {
        $typeAccount = array('concierge', 'adherent');
        /** @var UserInterface $account */
        $account = static::getFRR()->getReference('microsoft');
        foreach ($typeAccount as $type) {
            $this->loginAs($account, 'api');
            $client = $this->makeClient();
            $url = $this->getUrl('todotoday.account.api.account.post', array('type' => $type));
            $client->request('POST', $url);
            $this->assertStatusCode(403, $client);
        }
    }

    /**
     * Testing create adherent account wihtout custommer account for microsoft
     *
     * @small
     */
    public function testCreateWihoutCustommerForMicrosoftAccount(): void
    {
        $this->loginAs(static::getFRR()->getReference('microsoft'), 'api');
        $url = $this->getUrl('todotoday.account.api.account.post', array('type' => 'adherent'));
        $form = array(
            'username' => 'test',
            'firstName' => 'test',
            'lastName' => 'test',
            'email' => 'toto@test.fr',
        );
        $client = $this->makeClient();
        $client->request('POST', $url, $form, array(), array('HTTP_AGENCY' => 'demo0'));
        $reponse = json_decode($client->getResponse()->getContent());
        $this->assertStatusCode(400, $client);
        static::assertEquals('Validation Failed', $reponse->message);
    }

    /**
     * Test account creation when type is wrong
     *
     * @small
     */
    public function testCreateWrongType(): void
    {
        $this->loginAs(static::getFRR()->getReference('microsoft'), 'api');
        $client = $this->makeClient();
        $url = $this->getUrl('todotoday.account.api.account.post', array('type' => 'blabla'));
        $client->request('POST', $url);
        $this->assertStatusCode(500, $client);
    }

    /**
     * All account can acces to /account/me
     *
     * @dataProvider allAccountProvider
     * @small
     *
     * @param string $slug
     */
    public function testGetAccountMe(string $slug): void
    {
        $url = $this->getUrl('todotoday.account.api.account.get_me');
        $this->loginAs(static::getFRR()->getReference($slug), 'api');
        $client = $this->makeClient();
        $client->request('GET', $url);
        $this->assertStatusCode(200, $client);
    }

    /**
     * @small
     */
    public function testUpdatePaymentStatusWithInvalidStatus(): void
    {
        /** @var Adherent $account */
        $account = self::getFRR()->getReference('adherent0');
        $url = $this->getUrl(
            'todotoday.account.api.account.put_payment_method_status',
            array(
                'customerAccount' =>
                    $account->getCustomerAccount(),
            )
        );
        $this->loginAs(static::getFRR()->getReference('microsoft'), 'api');
        $client = $this->makeClient();
        $data = array(
            'payment_method_status' => 'blkabla',
        );
        $client->request('PUT', $url, $data);
        $this->assertStatusCode(400, $client);
    }

    /**
     * @small
     */
    public function testUpdatePaymentStatusWithInvalidCustomerAccount(): void
    {
        $url = $this->getUrl(
            'todotoday.account.api.account.put_payment_method_status',
            array(
                'customerAccount' =>
                    'personne',
            )
        );
        $this->loginAs(static::getFRR()->getReference('microsoft'), 'api');
        $client = $this->makeClient();
        $data = array(
            'payment_method_status' => 'blkabla',
        );
        $client->request('PUT', $url, $data);
        $this->assertStatusCode(404, $client);
    }

    /**
     * @small
     */
    public function testUpdatePaymentStatusSuccess(): void
    {
        /** @var Adherent $account */
        $account = self::getFRR()->getReference('adherent0');
        $url = $this->getUrl(
            'todotoday.account.api.account.put_payment_method_status',
            array(
                'customerAccount' =>
                    $account->getCustomerAccount(),
            )
        );
        $this->loginAs(static::getFRR()->getReference('microsoft'), 'api');
        $client = $this->makeClient();
        $data = array(
            'paymentMethodStatus' => PaymentStatusEnum::VALID,
        );
        $client->request('PUT', $url, $data);
        $this->assertStatusCode(200, $client);
    }

    /**
     * @dataProvider getAllAcountExceptMicrosoft
     *
     * @small
     *
     * @param string $slug
     */
    public function testUpdatePaymentStatusAccessDenied(string $slug): void
    {
        $url = $this->getUrl(
            'todotoday.account.api.account.put_payment_method_status',
            array('customerAccount' => 'osef')
        );
        $this->loginAs(static::getFRR()->getReference($slug), 'api');
        $client = $this->makeClient();
        $client->request('PUT', $url);
        $this->assertStatusCode(403, $client);
    }
}
