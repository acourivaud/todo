<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 29/03/17
 * Time: 11:23
 */

namespace Todotoday\AccountBundle\Tests\Controller\Api;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Actiane\UnitFonctionalTestBundle\Traits\AccessDeniedForNotAdherentTestableTrait;
use Symfony\Component\Security\Core\User\UserInterface;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAccountData;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Entity\Partner;
use Todotoday\AccountBundle\Exceptions\AdherentNotFoundInAxException;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyLinkedMicrosoft;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class AdherentApiControllerTest
 *
 * @package Todotoday\AccountBundle\Tests\Controller\Api
 */
class AdherentApiControllerTest extends WebTestCase
{
    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAccountData::class,
            LoadAgencyLinkedMicrosoft::class,
        );
    }

    /**
     * @return array
     */
    public function adherentProvider(): array
    {
        $provider = array();
        foreach (LoadAccountData::getAdherentsSlugs() as $adherent) {
            $provider[$adherent] = array($adherent);
        }

        return $provider;
    }

    /**
     * @return array
     */
    public function notAndAdherentProvider(): array
    {
        $provider = array();

        $accounts = array_merge(
            LoadAccountData::getConciergesSlugs(),
            array('root', 'admin', 'noosphere', 'microsoft')
        );
        foreach ($accounts as $account) {
            $provider[$account] = array($account);
        }

        return $provider;
    }

    /**
     * @dataProvider adherentProvider
     * @small
     *
     * @param string $adherent
     *
     * @throws \Doctrine\Common\DataFixtures\OutOfBoundsException
     */
    public function testLogoutFromAdherent(string $adherent): void
    {
        /** @var Adherent $adherent */
        $adherent = static::getFRR()->getReference($adherent);
        $this->loginAs($adherent, 'api');
        $client = $this->makeClient();
        $url = $this->getUrl('todotoday.account.api.adherent.get_log_out');
        $client->request('GET', $url);
        $this->assertStatusCode(204, $client);
        $adherentAfter = $this->getDoctrine()->getRepository(Adherent::class)->find(
            $adherent->getId()
        );
        static::assertNull($adherentAfter->getOneSignalId());
    }

    /**
     * @small
     */
    public function testLogoutFromNotAnAdherent(): void
    {
        $client = $this->makeClient();
        $url = $this->getUrl('todotoday.account.api.adherent.get_log_out');
        $client->request('GET', $url);
        $this->assertStatusCode(401, $client);
    }

    /**
     * @dataProvider notAndAdherentProvider
     * @small
     *
     * @param string $account
     *
     * @throws \Doctrine\Common\DataFixtures\OutOfBoundsException
     */
    public function testNotAnAdherentUpdateOneSignal(string $account): void
    {
        /** @var UserInterface $account */
        $account = static::getFRR()->getReference($account);
        $this->loginAs($account, 'api');
        $client = $this->makeClient();
        $url = $this->getUrl('todotoday.account.api.adherent.patch_one_signal');
        $client->request('PATCH', $url);
        $response = json_decode($client->getResponse()->getContent());
        $this->assertStatusCode(403, $client);
        static::assertEquals(
            'Only an adherent can perform this operation',
            $response->error->message
        );
    }

    /**
     * @small
     */
    public function testNotLoggedInUpdateOneSignal(): void
    {
        $client = $this->makeClient();
        $url = $this->getUrl('todotoday.account.api.adherent.patch_one_signal');
        $client->request('PATCH', $url);
        $this->assertStatusCode(401, $client);
    }

    /**
     * Do testPostMigrationAction
     *
     * @return void
     * @throws \Doctrine\Common\DataFixtures\OutOfBoundsException
     */
    public function testPostMigrationActionFail(): void
    {
        /** @var Partner $microsoft */
        $microsoft = static::getFRR()->getReference('microsoft');
        /** @var Agency $areva */
        $areva = static::getFRR()->getReference('agency_arevalta');
        $this->loginAs($microsoft, 'api');
        $client = $this->makeClient();
        $url = $this->getUrl('todotoday.account.api.adherent.post_migration');
        $client->request(
            'POST',
            $url,
            array(
                'customerAccount' => '00001',
                'email' => 'test_migration@actiane.com',
                'dataAreaId' => 'epa',
            ),
            array(),
            array('HTTP_AGENCY' => $areva->getSlug())
        );
        $this->assertStatusCode(500, $client);
    }

    /**
     * @dataProvider adherentProvider
     * @small
     *
     * @param string $adherent
     *
     * @throws \Doctrine\Common\DataFixtures\OutOfBoundsException
     */
    public function testUpdateOneSignal(string $adherent): void
    {
        /** @var Adherent $adherent */
        $adherent = static::getFRR()->getReference($adherent);
        $adherent->setOneSignalId('toto');
        $data = array('oneSignalId' => 'toto' . $adherent->getUsername());
        $this->loginAs($adherent, 'api');
        $client = $this->makeClient();
        $url = $this->getUrl('todotoday.account.api.adherent.patch_one_signal');
        $client->request('PATCH', $url, [], [], [], json_encode($data));
        $response = json_decode($client->getResponse()->getContent(), true);
        $this->assertStatusCode(200, $client);
        static::assertEquals($adherent->getEmail(), $response['email']);
        static::assertEquals($adherent->getOneSignalId(), $response['one_signal_id']);
    }

    /**
     * @dataProvider getAccount
     *
     * @param string $account
     * @param int    $statusExpected
     *
     * @throws \Doctrine\Common\DataFixtures\OutOfBoundsException
     */
    public function testSearchAxAccessDeniedForAdherent(?string $account, int $statusExpected): void
    {
        if ($account) {
            /** @var Account $adh */
            $adh = static::getFRR()->getReference($account);
            $this->loginAs($adh, 'api');
        }
        $client = $this->makeClient();
        $url = $this->getUrl('todotoday.account.api.adherent.get_search_ax_adherent_by_account_number');

        $client->request('GET', $url, ['account-number' => 'EPA_ON_SEN_FOU']);
        $this->assertStatusCode($statusExpected, $client);
    }

    /**
     * @small
     * @throws \Doctrine\Common\DataFixtures\OutOfBoundsException
     */
    public function testSearchAxFailWithNoParam()
    {
        $adh = static::getFRR()->getReference('root');
        $this->loginAs($adh, 'api');
        $client = $this->makeClient();
        $url = $this->getUrl('todotoday.account.api.adherent.get_search_ax_adherent_by_account_number');
        $client->request('GET', $url);
        $this->assertStatusCode(400, $client);
    }

    /**
     * @return array
     */
    public function getAccount(): array
    {
        return [
            'root' => ['root', 204],
            'adherent0' => ['adherent0', 403],
            'anonymous' => [null, 401],
        ];
    }
}
