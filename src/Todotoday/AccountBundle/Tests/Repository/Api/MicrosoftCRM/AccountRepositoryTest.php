<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 20/04/17
 * Time: 22:21
 */

namespace Todotoday\AccountBundle\Tests\Repository\Api\MicrosoftCRM;

use Actiane\ApiConnectorBundle\Tests\Repository\Api\AbstractRepositoryTestCase;

class AccountRepositoryTest extends AbstractRepositoryTestCase
{
    /**
     * Do getRepositoryId
     *
     * @return string
     */
    protected function getRepositoryId(): string
    {
        return 'todotoday.account.repository.api.account_crm';
    }
}
