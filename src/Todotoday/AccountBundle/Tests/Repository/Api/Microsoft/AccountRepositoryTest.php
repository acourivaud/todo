<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 20/02/2017
 * Time: 11:29
 */

namespace Todotoday\AccountBundle\Tests\Repository\Api\Microsoft;

use Liip\FunctionalTestBundle\Test\WebTestCase;
use Todotoday\AccountBundle\Entity\Api\Microsoft\Account;
use Todotoday\CoreBundle\Entity\Api\Microsoft\Agency;
use Todotoday\CoreBundle\Entity\Api\Microsoft\AgencyAddressBook;

/**
 * Class AccountRepositoryTest
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Tests\Repository\Api\Microsoft
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class AccountRepositoryTest extends WebTestCase
{
    /**
     * Do testFindAll
     *
     * @small
     *
     * @return Account[]
     */
    public function testAFindAll(): array
    {
        $repo = $this->getContainer()->get('todotoday.account.repository.api.account');

        /** @var Account[] $accounts */
        $accounts = $repo->findAll(5);

        static::assertNotEmpty($accounts);

        return $accounts;
    }

    /**
     * @depends testAFindAll
     * @small
     */
    public function testFind(): void
    {
        $accounts = $this->testAFindAll();
        $repo = $this->getContainer()->get('todotoday.account.repository.api.account');

        $findArray = array(
            'customerAccount' => $accounts[0]->getCustomerAccount(),
            'dataAreaId' => $accounts[0]->getDataAreaId(),
        );

        /** @var Account $account */
        $account = $repo->find(
            $findArray
        );

        static::assertNotNull($account);
        foreach ($findArray as $key => $value) {
            static::assertEquals($value, $account->{'get' . ucfirst($key)}());
        }
    }

    /**
     * Do testPersistAndFlush
     *
     * @group ignore
     * @return Account
     */
    public function testPersistAndFlush(): Account
    {
        $apiConnection = $this->getContainer()->get('actiane.api_connector.connection.microsoft');

        $repo = $this->getContainer()->get('todotoday.core.repository.api.agency');

        /** @var Agency[] $agencies */
        $agencies = $repo->findAll();

        /** @var AgencyAddressBook[] $addressBooks */
        $addressBooks = $this->getContainer()->get('todotoday.core.repository.api.agency_address_book')
            ->findAddressBooksByAgency($agencies[0]);
        $date = date('YmdHis');

        $account = (new Account())
            ->setPrimaryContactEmail($date . 'test@actiane.com')
            ->setFirstName($date . 'testi')
            ->setLastName($date . 'testta')
            ->setAddressBooks($addressBooks[0]->getAddressBookName())
            ->setWarehouseId($addressBooks[0]->getAddressBookName())
            ->setCustomerGroupId('ADH')
            ->setLanguageId('fr')
            ->setDataAreaId('epa')
            ->setPartyType('Person')
            ->setInFront('Yes')
            ->setName('adherent2')
            ->setAddressCountryRegionId('FRA')
            ->setSalesCurrencyCode('EUR')
            ->setAddressDescription('Une description');

        $apiConnection->persist($account);
        $apiConnection->flush();

        static::assertNotNull($account->getCustomerAccount());

        return $account;
    }

    /**
     * Do testUpdateAccount
     *
     * @depends testPersistAndFlush
     *
     * @param Account $account
     *
     * @return Account
     */
    public function testUpdateAccount(Account $account): Account
    {
        $apiConnection = $this->getContainer()->get('actiane.api_connector.connection.microsoft');

        $account->setApiConnection($apiConnection);
        $account
            ->setPrimaryContactPhone('0606060606')
            ->setAddressCountryRegionId('US');

        $apiConnection->flush();

        static::assertNotNull($account->getCustomerAccount());

        return $account;
    }
}
