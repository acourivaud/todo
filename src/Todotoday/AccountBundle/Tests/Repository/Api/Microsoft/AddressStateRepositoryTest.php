<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 17/06/17
 * Time: 16:34
 */

namespace Todotoday\AccountBundle\Tests\Repository\Api\Microsoft;

use Actiane\ApiConnectorBundle\Tests\Repository\Api\AbstractRepositoryTestCase;
use Actiane\ToolsBundle\Enum\USStateEnum;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use Todotoday\AccountBundle\Entity\Api\Microsoft\AddressState;
use Todotoday\AccountBundle\Repository\Api\Microsoft\AddressStateRepository;

/**
 * Class AddressStateRepositoryTest
 * @package Todotoday\AccountBundle\Tests\Repository\Api\Microsoft
 */
class AddressStateRepositoryTest extends AbstractRepositoryTestCase
{
    /**
     * @dataProvider statesProvider
     * @small
     *
     * @param string $name
     * @param string $state
     */
    public function testGetAllStates(string $name, string $state): void
    {
        /** @var AddressStateRepository $repo */
        $repo = $this->getContainer()->get($this->getRepositoryId());
        /** @var AddressState $state */
        $addressState = $repo->find(
            array(
                'state' => $state,
                'countryRegionId' => 'USA',
            )
        );

        static::assertSame($addressState->getName(), $name);
    }

    /**
     * @return array
     */
    public function statesProvider(): array
    {
        $provider = array();
        foreach (USStateEnum::toArray() as $state => $name) {
            $provider[$state] = array($name, $state);
        }

        return $provider;
    }

    /**
     * Do getRepositoryId
     *
     * @return string
     */
    protected function getRepositoryId(): string
    {
        return 'todotoday.account.repository.api.address_state';
    }
}
