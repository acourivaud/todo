<?php declare(strict_types=1);

/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 01/03/2017
 * Time: 12:40
 */

namespace Todotoday\AccountBundle\Tests\Repository\Api\Microsoft;

use Actiane\ApiConnectorBundle\Tests\Repository\Api\AbstractRepositoryTestCase;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAdherentLinkedMicrosoftData;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Entity\Api\Microsoft\Account;
use Todotoday\AccountBundle\Entity\Api\Microsoft\ContactPerson;
use Todotoday\AccountBundle\Repository\Api\Microsoft\ContactPersonRepository;

/**
 * Class ContactPersonRepositoryTest
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Tests\Repository\Api\Microsoft
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class ContactPersonRepositoryTest extends AbstractRepositoryTestCase
{
    /**
     * {@inheritdoc}
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAdherentLinkedMicrosoftData::class,
        );
    }

    /**
     * Do contactTypeProvider
     *
     * @return array
     */
    public function contactTypeProvider(): array
    {
        return array(
            array(
                'Principal',
            ),
            array(
                'Children',
            ),
            array(
                'Assistant',
            ),
        );
    }

    /**
     * Get Account
     *
     * @return Account
     */
    public function getAccount(): Account
    {
        /** @var Adherent $adherent */
        $adherent = static::getFRR()->getReference('adherent_linked_arevalta');

        return $this->get('todotoday.account.repository.api.account')->findOrCreateByDoctrineAccount($adherent);
    }

    /**
     * Do testCreateContactPerson
     *
     * @dataProvider contactTypeProvider
     *
     * @param string $contactType
     *
     * @return void
     */
    public function testCreateContactPerson(string $contactType): void
    {
        $connection = $this->getContainer()->get('actiane.api_connector.connection.microsoft');
        $contactPerson = new ContactPerson();
        $account = $this->getAccount();

        /**
         * Il faut soit le first name soit le last name au minimum lors de la création d'un contact pour que
         * microsoft hydrate un champ name qui est en required.
         */
        $contactPerson
            ->setLastName('toto')
            ->setFirstName('titi')
            ->setAssociatedPartyNumber($account->getPartyNumber())
            ->setContactType($contactType)
        ;

        $connection->persist($contactPerson);
        $connection->flush();

        $this->testHasIdentifier($contactPerson);
    }

    /**
     * Do testFindContactsPersonByAccountAndContactType
     *
     * @dataProvider contactTypeProvider
     * @depends      testCreateContactPerson
     *
     * @param string $contactType
     *
     * @return void
     */
    public function testFindContactsPersonByAccountAndContactType(string $contactType): void
    {
        $account = $this->getAccount();

        /** @var ContactPersonRepository $contactPersonRepo */
        $contactPersonRepo = $this->get('todotoday.account.repository.api.contact_person');

        $contactPersons = $contactPersonRepo->findContactsPersonByAccountAndContactType($account, $contactType);

        static::assertNotEmpty($contactPersons);
    }

    /**
     * Do getRepositoryId
     *
     * @return string
     */
    protected function getRepositoryId(): string
    {
        return 'todotoday.account.repository.api.contact_person';
    }
}
