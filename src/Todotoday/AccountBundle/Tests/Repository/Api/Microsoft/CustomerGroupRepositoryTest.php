<?php declare(strict_types = 1);

/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 01/03/2017
 * Time: 12:40
 */

namespace Todotoday\AccountBundle\Tests\Repository\Api\Microsoft;

use Actiane\ApiConnectorBundle\Tests\Repository\Api\AbstractRepositoryTestCase;

/**
 * Class CustomerGroupRepositoryTest
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Tests\Repository\Api\Microsoft
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class CustomerGroupRepositoryTest extends AbstractRepositoryTestCase
{
    /**
     * Do getRepositoryId
     *
     * @return string
     */
    protected function getRepositoryId(): string
    {
        return 'todotoday.account.repository.api.customer_group';
    }
}
