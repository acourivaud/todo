<?php declare(strict_types=1);

/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 01/03/2017
 * Time: 12:40
 */

namespace Todotoday\AccountBundle\Tests\Repository\Api\Microsoft;

use Actiane\ApiConnectorBundle\Tests\Repository\Api\AbstractRepositoryTestCase;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAdherentLinkedMicrosoftData;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Repository\Api\Microsoft\CustFieldRepository;

/**
 * Class CustFieldRepositoryTest
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Tests\Repository\Api\Microsoft
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class CustFieldRepositoryTest extends AbstractRepositoryTestCase
{
    /**
     * Do accountProvider
     *
     * @return array[]
     */
    public function accountProvider(): array
    {
        return [['adherent_linked_carrefour-massy']];
//        $ids = array();
//
//        foreach (LoadAdherentLinkedMicrosoftData::getAdherentsSlug() as $value) {
//            $ids[] = [$value];
//        }
//
//        return $ids;
    }

    /**
     * {@inheritdoc}
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAdherentLinkedMicrosoftData::class,
        );
    }

    /**
     * Do testFindByAccount
     *
     * @dataProvider accountProvider
     *
     * @param string $adhId
     *
     * @return void
     */
    public function testFindByAccount(string $adhId): void
    {
        /** @var Adherent $account */
        $account = static::getFRR()->getReference($adhId);

        /** @var CustFieldRepository $custFieldRepo */
        $custFieldRepo = $this->get($this->getRepositoryId());

        $custFields = $custFieldRepo->findByAccount($account);
        static::assertNotEmpty($custFields);
    }

    /**
     * Do getRepositoryId
     *
     * @return string
     */
    protected function getRepositoryId(): string
    {
        return 'todotoday.account.repository.api.cust_field';
    }
}
