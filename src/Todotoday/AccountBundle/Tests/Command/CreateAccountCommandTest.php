<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 27/01/2017
 * Time: 17:35
 */

namespace Todotoday\AccountBundle\Tests\Command;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Doctrine\ORM\EntityManagerInterface;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyData;

/**
 * Class CreateAccountCommandTest
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Tests\Command
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class CreateAccountCommandTest extends WebTestCase
{
    /**
     * {@inheritdoc}
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAgencyData::class,
        );
    }

    /**
     * Do accountTypeProvider
     *
     * @return array[]
     */
    public function accountTypeProvider(): array
    {
        $params = array();
        /** @var EntityManagerInterface $em */
        $em = $this->get('doctrine.orm.entity_manager');
        $keys = array_keys($em->getClassMetadata(Account::class)->discriminatorMap);
        foreach ($keys as $key) {
            $params[] = array(
                $key,
            );
        }

        return $params;
    }

    /**
     * Do testExecute
     *
     * @dataProvider accountTypeProvider
     * @small
     *
     * @param string $type
     *
     * @return void
     */
    public function testExecute(string $type): void
    {
        $result = $this->runCommand(
            'tdtd:account:create',
            array(
                'first_name' => 'test' . $type,
                'last_name' => 'test' . $type,
                'email' => 'test' . $type,
                'password' => 'test' . $type,
                'type' => $type,
                'role' => 'blabla',
            )
        );

        static::assertContains('Create account type : ' . $type, $result);
    }

    /**
     * Do testExecute
     *
     * @small
     *
     * @return void
     */
    public function testExecuteFail(): void
    {
        $result = $this->runCommand(
            'tdtd:account:create',
            array(
                'first_name' => 'test',
                'last_name' => 'test',
                'email' => 'test',
                'password' => 'test',
                'type' => 'test',
                'role' => 'blabla',
            )
        );

        static::assertContains('Account type (test) does not exist', $result);
    }
}
