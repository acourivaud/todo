<?php declare(strict_types=1);

namespace Todotoday\AccountBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\AccountBundle\Entity\Adherent;

/**
 * Class AccountAdminController
 *
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Controller\Admin
 */
class AccountAdminController extends CRUDController
{
    /**
     * Do resetPasswordAction
     *
     * @param Account $account
     *
     * @return Response
     * @throws \LogicException
     * @throws \Symfony\Component\DependencyInjection\Exception\InvalidArgumentException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \RuntimeException
     * @throws \Psr\Log\InvalidArgumentException
     */
    public function resetPasswordAction(Account $account): Response
    {
        $ttl = $this->container->getParameter('fos_user.resetting.retry_ttl');

        if (!$account) {
            throw new \InvalidArgumentException('No account found');
        }
        if (!$account->isPasswordRequestNonExpired($ttl)) {
            if (null === $account->getConfirmationToken()) {
                /** @var $tokenGenerator TokenGeneratorInterface */
                $tokenGenerator = $this->get('fos_user.util.token_generator');
                $account->setConfirmationToken($tokenGenerator->generateToken());
            }

            $this->get('todotoday.account.services.account_manager')->sendResettingEmailMessage($account);
            $account->setPasswordRequestedAt(new \DateTime());
            $this->get('fos_user.user_manager')->updateUser($account);
            $this->addFlash('success', 'Password reset sent.');
        } else {
            $this->addFlash('danger', 'Password reset already sent. Please check your mail box.');
        }

        return new RedirectResponse(
            $this->admin->generateUrl('list', array('filter' => $this->admin->getFilterParameters()))
        );
    }

    /**
     * @param Adherent $adherent
     *
     * @return Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \InvalidArgumentException
     */
    public function continueRegistrationAction(Adherent $adherent): Response
    {
        if (!$token = $adherent->getRegistrationToken()) {
            throw $this->createNotFoundException('Token not found');
        }

        return new RedirectResponse(
            $this->generateUrl(
                'todotoday_account_registration_confirmation',
                array('token' => $token)
            )
        );
    }
}
