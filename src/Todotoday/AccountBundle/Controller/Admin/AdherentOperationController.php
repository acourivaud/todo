<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 07/11/17
 * Time: 17:54
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\AccountBundle\Controller\Admin
 *
 * @subpackage Todotoday\AccountBundle\Controller\Admin
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\AccountBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Enum\PaymentStatusEnum;

/**
 * @Route("admin/todotoday/account/adherent/{id}/operation")
 * Class AdherentOperationController
 */
class AdherentOperationController extends Controller
{
    /**
     * @Route(name="adherent.admin.operation", path="")
     * @Security("has_role('ROLE_COMMUNITY_MANAGER')")
     *
     * @param int $id
     *
     * @return Response
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function indexAdherentOperation(int $id): Response
    {
        $adherent = $this->getAdherent($id);

        return $this->render(
            '@TodotodayAccount/Admin/Adherent/adherent_operation.html.twig',
            [
                'adherent' => $adherent,
                'paymentStatus' => PaymentStatusEnum::getUpdatedStatusChoice(),

            ]
        );
    }

    /**
     * @Route(name="adherent.admin.operation.update_payment_status", path="/payment-status-update/{status}")
     * @Security("has_role('ROLE_COMMUNITY_MANAGER')")
     *
     * @param int    $id
     * @param string $status
     *
     * @return Response
     * @throws \InvalidArgumentException
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     *
     */
    public function setPayementStatusMethodAction(int $id, string $status): Response
    {
        $adherent = $this->getAdherent($id);
        $translator = $this->get('translator');
        $em = $this->getDoctrine()->getManager();

        if (PaymentStatusEnum::VALID === $status && null === $adherent->getPaymentTypeSelected()) {
            $this->addFlash(
                'error',
                $translator->trans('adherent_payement_status_invalid', [], 'messages')
            );
        } else {
            $adherent->setPaymentMethodStatus(PaymentStatusEnum::getNewInstance($status));
            $em->flush();

            $this->addFlash(
                'success',
                $translator->trans('adherent_payment_status_change_success', [], 'messages')
            );
        }

        return $this->redirectToRoute('adherent.admin.operation', ['id' => $id]);
    }

    /**
     * @param int $id
     *
     * @return Adherent
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    private function getAdherent(int $id): Adherent
    {
        if (!$adherent = $this->get('doctrine')->getRepository('TodotodayAccountBundle:Adherent')->find($id)) {
            throw $this->createNotFoundException();
        }

        return $adherent;
    }
}
