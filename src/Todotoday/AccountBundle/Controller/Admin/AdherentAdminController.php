<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 12/06/17
 * Time: 11:44
 */

namespace Todotoday\AccountBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class AdherentAdminController
 *
 * @package Todotoday\AccountBundle\Controller\Admin
 */
class AdherentAdminController extends AccountAdminController
{
    /**
     * @param int $id
     *
     * @return Response
     * @throws \InvalidArgumentException
     */
    public function operationAction(int $id): Response
    {
        return new RedirectResponse(
            $this->generateUrl('adherent.admin.operation', ['id' => $id])
        );
    }

    public function documentsAction(int $id)
    {
        $object = $this->admin->getSubject();

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id: %s', $id));
        }

        $this->admin->setSubject($object);

        return $this->render($this->admin->getTemplate('show'), array(
            'action' => 'show',
            'object' => $object,
            'elements' => $this->admin->getShow(),
        ), null);
    }
}
