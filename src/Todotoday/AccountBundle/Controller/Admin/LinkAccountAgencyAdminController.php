<?php declare(strict_types=1);

namespace Todotoday\AccountBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController;

/**
 * Class LinkAccountAgencyAdminController
 *
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Controller\Admin
 */
class LinkAccountAgencyAdminController extends CRUDController
{
}
