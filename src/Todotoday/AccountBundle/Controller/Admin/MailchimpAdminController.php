<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 25/06/17
 * Time: 11:22
 */

namespace Todotoday\AccountBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Repository\AgencyRepository;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Repository\AdherentRepository;
use Todotoday\AccountBundle\Enum\AdherentOptinEnum;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use DateTime;

/**
 * Class MailchimpAdminController
 * @package Todotoday\AccountBundle\Controller\Admin
 */
class MailchimpAdminController extends CRUDController
{
    /**
     *
     * @return Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \LogicException
     */
    public function listAction(): Response
    {
        if (!$this->isGranted('VIEW', $this)) {
            throw $this->createAccessDeniedException('Fail');
        }

        if (!$agency = $this->get('todotoday.core.domain_context')->getCurrentContext()->getAgency()) {
            throw $this->createNotFoundException();
        }

        $optinStats = [];

        $em = $this->getDoctrine()->getManager();
        /** @var AdherentRepository $adherentRepo */
        $adherentRepo = $em->getRepository(Adherent::class);
        $optinTypes = AdherentOptinEnum::toArray();
        if (count($optinTypes) > 0) {
            foreach ($optinTypes as $optinType) {
                $optinStat = [];
                $optinStat['label'] = 'mailchimp.export.label.' . $optinType;
                $optinStat['count'] = $adherentRepo->getCountAdherentsOptinByAgency($agency, $optinType);

                $optinStats[] = $optinStat;
            }
        }

        $optinStat = [];
        $optinStat['label'] = 'mailchimp.export.label.null';
        $optinStat['count'] = $adherentRepo->getCountAdherentsOptinNullByAgency($agency);

        $optinStats[] = $optinStat;

        return $this->renderWithExtraParams('TodotodayAccountBundle:Admin/Mailchimp:list.html.twig', [
            'agency' => $agency,
            'optinStats' => $optinStats,
        ]);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \LogicException
     */
    public function checkAction(Request $request): JsonResponse
    {
        if (!$request->isMethod('post')) {
            throw $this->createNotFoundException();
        }

        if (!$request->files->get('file') ||
                !$request->get('agency')) {
            throw $this->createNotFoundException();
        }

        $em = $this->getDoctrine()->getManager();

        /** @var AgencyRepository $adherentRepo */
        $agencyRepo = $em->getRepository(Agency::class);
        $agency = $agencyRepo->findOneBy(array('slug' => $request->get('agency')));

        if (!$agency) {
            throw $this->createNotFoundException();
        }

        $mailchimpManager = $this->get('todotoday.account.services.mailchimp_manager');

        $file = $request->files->get('file');
        $fileName = $mailchimpManager->manageFile($file);

        $result = $mailchimpManager->getPreviewStats($fileName, $agency);

        $response = new JsonResponse($result, 200);

        return $response;
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \LogicException
     */
    public function updateAction(Request $request): JsonResponse
    {
        if (!$request->isMethod('post')) {
            throw $this->createNotFoundException();
        }

        $fileName = $request->get('filename');
        $agencySlug = $request->get('agency');

        if (!$fileName || !$agencySlug) {
            throw $this->createNotFoundException();
        }

        $em = $this->getDoctrine()->getManager();
        /** @var AgencyRepository $adherentRepo */
        $agencyRepo = $em->getRepository(Agency::class);
        $agency = $agencyRepo->findOneBy(array('slug' => $agencySlug));

        if (!$agency) {
            throw $this->createNotFoundException();
        }

        $mailchimpManager = $this->get('todotoday.account.services.mailchimp_manager');
        $status = $mailchimpManager->managerUpload($fileName, $agency);

        $response = new JsonResponse($status, 200);

        return $response;
    }

    /**
     * @param Request $request
     *
     * @return Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \LogicException
     */
    public function downloadAction(Request $request): Response
    {
        $agencySlug = $request->get('agency');

        $em = $this->getDoctrine()->getManager();
        /** @var AgencyRepository $adherentRepo */
        $agencyRepo = $em->getRepository(Agency::class);
        $agency = $agencyRepo->findOneBy(array('slug' => $agencySlug));

        if (!$agency) {
            throw $this->createNotFoundException();
        }

        $mailchimpManager = $this->get('todotoday.account.services.mailchimp_manager');
        $adherents = $mailchimpManager->getAdherentsOptinStatus($agency);

        $filename = 'export_optin_' . $agencySlug . '_' . date('YmdHis') . '.csv';

        $content = $this->strPutCSV($adherents);
        $response = new Response($content);
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Disposition', sprintf('attachment; filename="%s"', $filename));

        return $response;
    }

    /**
     * Array to string ready for CSV file
     *
     * @param array $data
     *
     * @return string
     */
    public function strPutCSV($data)
    {
        $fh = fopen('php://temp', 'rw');

        if (current($data)) {
            fputcsv($fh, array_keys(current($data)));

            foreach ($data as $row) {
                fputcsv($fh, $row);
            }
        }

        rewind($fh);
        $csv = stream_get_contents($fh);
        fclose($fh);

        return $csv;
    }
}
