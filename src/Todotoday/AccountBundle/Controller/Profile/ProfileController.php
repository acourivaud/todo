<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 12/04/17
 * Time: 22:09
 */

namespace Todotoday\AccountBundle\Controller\Profile;

use Actiane\ToolsBundle\Traits\LoggerTrait;
use Stripe\Error\Base;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Entity\Partner;
use Todotoday\AccountBundle\Enum\PaymentStatusEnum;
use Todotoday\AccountBundle\Exceptions\AccountNotFoundException;
use Todotoday\AccountBundle\Form\AccountCustFieldsType;
use Todotoday\AccountBundle\Form\AccountLanguageOptinType;
use Todotoday\AccountBundle\Form\AccountPersonalInfoType;
use Todotoday\AccountBundle\Form\AccountTransactionalInfoType;
use Todotoday\AccountBundle\Form\AccountTypeMigrationType;
use Todotoday\AccountBundle\Repository\AdherentRepository;
use Todotoday\AccountBundle\Repository\Api\Microsoft\CustFieldRepository;
use Todotoday\AccountBundle\Services\AdherentManager;
use Todotoday\ChatBundle\Entity\Conversation;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Enum\AgencyModuleEnum;
use Todotoday\CoreBundle\Exceptions\AgencyModuleDisabledException;
use Todotoday\AccountBundle\Enum\AdherentTypeEnum;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Todotoday\AccountBundle\Entity\LinkAccountAgency;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @Route("/profile")
 * Class ProfileController
 *
 * @package Todotoday\AccountBundle\Controller
 */
class ProfileController extends Controller
{
    use LoggerTrait;

    /**
     * @param Request $request
     *
     * @return Response
     * @throws \Symfony\Component\Form\Exception\UnexpectedTypeException
     * @throws \Symfony\Component\Form\Exception\LogicException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Gedmo\Exception\InvalidArgumentException
     * @throws \InvalidArgumentException
     * @throws \LogicException
     * @Route("/extra", name="account.profile_extra")
     */
    public function extraAction(Request $request): Response
    {
        /** @var Adherent $account */
        $account = $this->getUser();

        /** @var CustFieldRepository $custFieldRepo */
        $custFieldRepo = $this->get('todotoday.account.repository.api.cust_field');

        $custFields = $custFieldRepo->findByAccount($account);

        $formView = null;
        if ($custFields) {
            $account->setCustFields($custFields);

            $form = $this->createForm(AccountCustFieldsType::class, $account, ['validation_groups' => 'Default'])
                ->add('submit', SubmitType::class, array('label' => 'save', 'translation_domain' => 'todotoday'));

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $this->get('actiane.api_connector.connection.microsoft')->flush();
            }

            $formView = $form->createView();
        }

        return $this->render(
            '@TodotodayAccount/Profile/profile_extra.html.twig',
            array(
                'lockerCode' => $account->getLockerCode(),
                'accountNumber' => $account->getCustomerAccount(),
                'form' => $formView,
            )
        );
    }

    /**
     * @param Request $request
     *
     * @return Response
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Symfony\Component\Form\Exception\UnexpectedTypeException
     * @throws \Symfony\Component\Form\Exception\LogicException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Gedmo\Exception\InvalidArgumentException
     * @throws \InvalidArgumentException
     * @throws \LogicException
     * @Route("/", name="account.profile_index")
     */
    public function indexAction(Request $request): Response
    {
        /** @var AdherentManager $adherentManager */
        $adherentManager = $this->get('todotoday.account.services.adherent_manager');

        /** @var Adherent $account */
        $account = $adherentManager->getUser(
            null,
            array(
                'contactPrincipals' => true,
                'contactChildren' => false,
                'contactAssistants' => false,
                'bankAccount' => false,
            )
        );

        $form = $this->createForm(
            AccountPersonalInfoType::class,
            $account,
            array(
                'action' => $this->generateUrl('account.profile_index'),
                'method' => 'POST',
                'display_optional_fields' => false,
                'validation_groups' => ['Default'],
            )
        )->add(
            'submit',
            SubmitType::class,
            array(
                'label' => 'save',
                'translation_domain' => 'todotoday',
            )
        );

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $translator = $this->get('translator');
            if ($form->isValid()) {
                $this->addFlash(
                    'success',
                    $translator->trans('edit_success', [], 'todotoday')
                );
                $em = $this->getDoctrine()->getManager();
                $adherentManager->updateMicrosoftAccount(
                    $account,
                    array(
                        'contactPrincipals' => true,
                        'contactChildren' => false,
                        'contactAssistants' => false,
                        'bankAccount' => false,
                        'custFields' => false,
                    )
                );
                $em->persist($account);

                return $this->redirectToRoute('account.profile_index');
            }

            $this->addFlash(
                'error',
                $translator->trans('edit_error', [], 'todotoday')
            );
        }

        return $this->render(
            ':account:profile_index.html.twig',
            array(
                'form' => $form->createView(),
                'lockerCode' => $account->getLockerCode(),
                'accountNumber' => $account->getCustomerAccount(),
            )
        );
    }

    /**
     * @Route("/option", name="account.profile_option")
     *
     * @param Request $request
     *
     * @return Response
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Symfony\Component\Form\Exception\UnexpectedTypeException
     * @throws \Symfony\Component\Form\Exception\LogicException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    public function optionAction(Request $request): Response
    {
        /** @var AdherentManager $adherentManager */
        $adherentManager = $this->get('todotoday.account.services.adherent_manager');

        /** @var Adherent $account */
        $account = $adherentManager->getUser(
            null,
            array(
                'contactPrincipals' => true,
                'contactChildren' => false,
                'contactAssistants' => false,
                'bankAccount' => false,
            )
        );

        $form = $this->createForm(
            AccountLanguageOptinType::class,
            $account,
            array(
                'action' => $this->generateUrl('account.profile_option'),
                'method' => 'POST',
                'validation_groups' => ['Default'],
            )
        )
        ->add(
            'submit',
            SubmitType::class,
            array(
                'label' => 'save',
                'translation_domain' => 'todotoday',
            )
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $translator = $this->get('translator');
            $this->addFlash(
                'success',
                $translator->trans('edit_preference_success', [], 'todotoday')
            );
            $em = $this->getDoctrine()->getManager();
            $adherentManager->updateMicrosoftAccount(
                $account,
                array(
                    'contactPrincipals' => true,
                    'contactChildren' => false,
                    'contactAssistants' => false,
                    'bankAccount' => false,
                    'custFields' => false,
                )
            );
            $em->persist($account);
            $em->flush();

            return $this->redirectToRoute('account.profile_option');
        }

        return $this->render(
            ':account:profile_index.html.twig',
            array(
                'form' => $form->createView(),
                'lockerCode' => $account->getLockerCode(),
                'accountNumber' => $account->getCustomerAccount(),
            )
        );
    }

    /**
     * @Route("/payment", name="account.profile_payment")
     *
     * @param Request $request
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return Response
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\Form\Exception\UnexpectedTypeException
     * @throws \Symfony\Component\Form\Exception\LogicException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    public function paymentAction(Request $request): Response
    {
        /** @var AdherentManager $adherentManager */
        $adherentManager = $this->get('todotoday.account.services.adherent_manager');

        /** @var Adherent $account */
        $account = $adherentManager->getUser(
            null,
            array(
                'contactPrincipals' => false,
                'contactChildren' => false,
                'contactAssistants' => false,
                'bankAccount' => true,
            )
        );

        if (($paymentMethodSelected = $account->getPaymentTypeSelected()) && $paymentMethodSelected->get()
            && ($account->getPaymentMethodStatus()->get() === PaymentStatusEnum::VALID
                || $account->getPaymentMethodStatus()->get() === PaymentStatusEnum::SEPA_WAITING_FOR_SIGNATURE
            )) {
            $iban = ($bankAccount = $account->getBankAccount()) ? $bankAccount->getIban() : '';

            return $this->render(
                '@TodotodayAccount/Profile/payment_show.html.twig',
                array(
                    'defaultPayment' => $paymentMethodSelected->get(),
                    'status' => $account->getPaymentMethodStatusString(),
                    'bankAccount' => $iban,
                    'lockerCode' => $account->getLockerCode(),
                    'accountNumber' => $account->getCustomerAccount(),
                )
            );
        }
        $form = $this->createForm(AccountTransactionalInfoType::class, $account, ['validation_groups' => 'Default'])
            ->add('save', SubmitType::class, array('translation_domain' => 'todotoday', 'label' => 'save'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $adherentManager->updateEntityBankAccount(null, $account);
                $adherentManager->validatePaymentInformation($account);
                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute('account.profile_payment');
            } catch (Base $exceptionStripe) {
                $message = method_exists($exceptionStripe, 'getStripeCode')
                    ? 'stripe_' . $exceptionStripe->getStripeCode()
                    : 'error.title';
                $this->addFlash('stripe_error', $message);
            }
        }

        return $this->render(
            '@TodotodayAccount/Profile/payment_edit.html.twig',
            array(
                'form' => $form->createView(),
                'lockerCode' => $account->getLockerCode(),
                'accountNumber' => $account->getCustomerAccount(),
            )
        );
    }

    /**
     * @Route("/co_adherent", name="account.profile_co_adherent")
     *
     * @param Request $request
     *
     * @return Response
     * @throws \Symfony\Component\Validator\Exception\MissingOptionsException
     * @throws \Symfony\Component\Validator\Exception\InvalidOptionsException
     * @throws \Symfony\Component\Validator\Exception\ConstraintDefinitionException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \RuntimeException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\Form\Exception\UnexpectedTypeException
     * @throws \Symfony\Component\Form\Exception\LogicException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     * @throws AccessDeniedException
     */
    public function coAdherentAction(Request $request): Response
    {
        /** @var Agency $agency */
        $agency = $this->getAgency();
        /** @var Adherent $adherent */
        $adherent = $this->getUser();

        if (!$agency->hasModule(AgencyModuleEnum::COADH)) {
            return $this->redirectToRoute('account.profile_index');
        }

        if (!$this->isGranted('ROLE_MANAGE_CO_ADHERENT')) {
            throw $this->createAccessDeniedException();
        }

        $adherentManager = $this->get('todotoday.account.services.adherent_manager');
        $values = $adherentManager->addCoAdherent($request, $agency, $adherent);

        $coAdherents = $values['coAdherents'];
        /** @var Form $form */
        $form = $values['form'];

        if ($values['formSubmitted']) {
            if ($values['success']) {
                $this->addFlash(
                    'co_adherent_success_add',
                    'account.co_adh_success_add'
                );
            } else {
                $this->addFlash(
                    'co_adherent_error_add',
                    'account.co_adh_error_add'
                );
            }

            return $this->redirectToRoute('account.profile_co_adherent');
        }

        return $this->render(
            '@TodotodayAccount/Profile/co_adherent.html.twig',
            array(
                'coAdherents' => $coAdherents,
                'form' => $form ? $form->createView() : null,
                'lockerCode' => $adherent->getLockerCode(),
                'accountNumber' => $adherent->getCustomerAccount(),
            )
        );
    }

    /**
     * @Route("/co_adherent/resend_email/{id}", name="account.profile_co_adherent.resend_email", requirements={"id":
     *     "\d+"})
     *
     * @param int $id
     *
     * @return Response
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \RuntimeException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \LogicException
     * @throws \Todotoday\AccountBundle\Exceptions\AccountNotFoundException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyModuleDisabledException
     * @throws AgencyModuleDisabledException
     * @throws AccountNotFoundException
     */
    public function resendMailToCoAdherent(int $id): Response
    {
        /** @var Agency $agency */
        $agency = $this->getAgency();

        if (!$agency->hasModule(AgencyModuleEnum::COADH)) {
            throw new AgencyModuleDisabledException();
        }

        $em = $this->getDoctrine()->getManager();
        /** @var AdherentRepository $adherentRepo */
        $adherentRepo = $em->getRepository(Adherent::class);
        $coAdherent = $adherentRepo->getCoAdherentByIdAndParent($id, $this->getUser());

        if (!$coAdherent) {
            throw new AccountNotFoundException();
        }

        $accountManager = $this->get('todotoday.account.services.account_manager');
        $accountManager->createRegistrationToken($coAdherent)
            ->sendEmailRegistrationToken($coAdherent, $agency);
        $em->flush();

        $this->addFlash(
            'co_adherent_success_add',
            'account.co_adherent.resend_email.success'
        );

        return $this->redirectToRoute('account.profile_co_adherent');
    }

    /**
     * @Route("/co_adherent/{id}/disable", name="account.profile_co_adherent_disable", requirements={"id":
     *     "\d+"})
     *
     * @param Request $request
     * @param int     $id
     *
     * @return Response
     * @throws \Todotoday\AccountBundle\Exceptions\AccountNotFoundException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyModuleDisabledException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \RuntimeException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\Form\Exception\UnexpectedTypeException
     * @throws \Symfony\Component\Form\Exception\LogicException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     * @throws AccountNotFoundException
     * @throws AgencyModuleDisabledException
     */
    public function coAdherentDisableAction(Request $request, int $id): Response
    {
        /** @var Agency $agency */
        $agency = $this->getAgency();

        if (!$agency->hasModule(AgencyModuleEnum::COADH)) {
            throw new AgencyModuleDisabledException();
        }

        $em = $this->getDoctrine()->getManager();
        /** @var AdherentRepository $repo */
        $repo = $em->getRepository(Adherent::class);

        /** @var Adherent $coAdherent */
        $coAdherent = $repo->getCoAdherentByIdAndParent($id, $this->getUser());

        if (!$coAdherent) {
            throw new AccountNotFoundException();
        }

        $coAdherent->setEnabled(false);
        $em->persist($coAdherent);
        $em->flush();

        return $this->redirectToRoute('account.profile_co_adherent');
    }

    /**
     * @Route(name="account.profile_archived_conversation", path="/archived-conversation")
     *
     * @return Response
     * @throws \LogicException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws AgencyModuleDisabledException
     */
    public function archivedConversationAction(): Response
    {
        $agency = $this->getAgency();

        if (!$agency->hasModule('chat')) {
            throw new AgencyModuleDisabledException();
        }

        /** @var Adherent $adherent */
        $adherent = $this->getUser();

        $conversations = $this->getDoctrine()->getRepository(Conversation::class)
            ->getConverationByAdherent($adherent);

        return $this->render(
            '@TodotodayAccount/Profile/profile_archived_conversation.html.twig',
            [
                'accountNumber' => $adherent->getCustomerAccount(),
                'lockerCode' => $adherent->getLockerCode(),
                'conversations' => $conversations,
            ]
        );
    }

    /**
     * @Route("/type-migration", name="account.profile_type_migration")
     *
     * @param Request $request
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return Response
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\Form\Exception\UnexpectedTypeException
     * @throws \Symfony\Component\Form\Exception\LogicException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    public function typeMigrationAction(Request $request): Response
    {
        /** @var AdherentManager $adherentManager */
        $adherentManager = $this->get('todotoday.account.services.adherent_manager');

        /** @var Adherent $account */
        $account = $adherentManager->getUser(
            null,
            array(
                'contactPrincipals' => true,
                'contactChildren' => false,
                'contactAssistants' => false,
                'bankAccount' => false,
            )
        );

        $form = $this->createForm(
            AccountTypeMigrationType::class,
            $account,
            array(
                'action' => $this->generateUrl('account.profile_type_migration'),
                'method' => 'POST',
                'validation_groups' => ['Default'],
            )
        )->add(
            'submit',
            SubmitType::class,
            array(
                'label' => 'save',
                'translation_domain' => 'todotoday',
            )
        );

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $linkAgencies = $account->getLinkAgencies();
                foreach ($linkAgencies as $linkAgency) {
                    $linkAgency->setRoles(['ROLE_ADHERENT']);
                }

                $account->setRoles(array());
                $account->setCustomerGroup(AdherentTypeEnum::getNewInstance(AdherentTypeEnum::ADH));

                $userManager = $this->get('fos_user.user_manager');
                $userManager->updateUser($account);

                $em = $this->getDoctrine()->getManager();
                $em->persist($account);
                $em->flush();

                $adherentManager->updateMicrosoftAccount($account);

                $this->get('security.token_storage')->getToken()->setAuthenticated(false);

                return $this->redirectToRoute('todotoday_account_registration_mandatoryworkflow');
            }

            $this->addFlash('error', 'profile_type_error');
        }

        return $this->render(
            '@TodotodayAccount/Profile/profile_type_migration.html.twig',
            array(
                'form' => $form->createView(),
                'lockerCode' => $account->getLockerCode(),
                'accountNumber' => $account->getCustomerAccount(),
            )
        );
    }

    /**
     * Do getAgency
     *
     * @return Agency
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \UnexpectedValueException
     * @throws NotFoundHttpException
     */
    protected function getAgency(): Agency
    {
        if (!$agency = $this->get('todotoday.core.domain_context')->getCurrentContext()->getAgency()) {
            throw $this->createNotFoundException();
        }

        return $agency;
    }
}
