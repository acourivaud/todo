<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 24/04/2017
 * Time: 11:40
 */

namespace Todotoday\AccountBundle\Controller;

use FOS\UserBundle\Controller\SecurityController as BaseSecurityController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class SecurityController
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Controller
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class SecurityController extends BaseSecurityController
{
    /**
     * {@inheritdoc}
     * @throws \LogicException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \UnexpectedValueException
     */
    public function loginAction(Request $request)
    {
        $domain = $this->get('todotoday.core.domain_context');
        if ($domain->getCurrentContextKey() !== 'www' && !$domain->getCurrentContext()->getAgency()) {
            throw $this->createNotFoundException();
        }

        if ($this->isGranted('IS_AUTHENTICATED_FULLY')
            && !$this->isGranted(
                array('ROLE_ADHERENT')
            )
            && !$this->isGranted(
                array('ROLE_NOT_ADHERENT')
            )
            && !$this->isGranted(
                array('ROLE_CO_ADHERENT')
            )
        ) {
            return $this->redirectToRoute('todotoday_account_registration_mandatoryworkflow');
        }

        return parent::loginAction($request);
    }
}
