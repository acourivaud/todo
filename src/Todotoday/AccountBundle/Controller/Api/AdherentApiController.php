<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 27/03/17
 * Time: 17:07
 */

namespace Todotoday\AccountBundle\Controller\Api;

use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Request\ParamFetcher;
use Monolog\Logger;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Entity\Api\Microsoft\Account as AccountMicrosoft;
use Todotoday\AccountBundle\Entity\Api\Microsoft\ContactPerson;
use Todotoday\AccountBundle\Event\RegistrationSuccessEvent;
use Todotoday\AccountBundle\Exceptions\NotAnAdherentException;
use Todotoday\AccountBundle\Form\Api\AccountMigrationType;
use Todotoday\AccountBundle\Form\Api\AccountPersonalInfoType;
use Todotoday\AccountBundle\Form\Api\AdherentType;
use Todotoday\AccountBundle\Services\AdherentManager;
use Todotoday\CoreBundle\Controller\Api\AbstractApiController;

/**
 * Class AdherentApiController
 *
 * @FosRest\Prefix(value="adherent")
 * @FosRest\NamePrefix("todotoday.account.api.adherent.")
 */
class AdherentApiController extends AbstractApiController
{
    /**
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Resource description to put new entity",
     *     description="API Rest description to put new entity",
     *     views={"Adherent"},
     *     section="Adherent"
     * )
     *
     * @FosRest\Route("/logout")
     *
     * @return Response
     * @throws \Exception
     */
    public function getLogOutAction(): Response
    {
        /** @var Adherent $adherent */
        if (!($adherent = $this->getUser()) instanceof Adherent) {
            throw new NotAnAdherentException('Only an adherent can perform this operation');
        }

        $this->get('todotoday.account.services.adherent_manager')->adherentLogOut($adherent);
        $view = $this->view(null);

        return $this->handleView($view);
    }

    /**
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Resource description to patch entity",
     *     description="Operation on adherent",
     *     input={
     *           "class"="Todotoday\AccountBundle\Form\Api\AdherentType"
     *     },
     *     output={
     *           "class"="Todotoday\AccountBundle\Entity\Adherent",
     *           "groups"={"Default","account"}
     *     },
     *     views={"Adherent"},
     *     section="Adherent"
     * )
     *
     * @FosRest\Route("/onesignal")
     * @param Request $request
     *
     * @return Response
     * @throws \Exception
     */
    public function patchOneSignalAction(Request $request): Response
    {
        /** @var Adherent $adherent */
        if (!($adherent = $this->getUser()) instanceof Adherent) {
            throw new NotAnAdherentException('Only an adherent can perform this operation');
        }

        $form = $this->createForm(
            $this->getFormType(),
            $adherent,
            array(
                'method' => 'PATCH',
                'validation_groups' => ['Default'],
            )
        );

        $data = json_decode($request->getContent(), true);
        $form->submit($data, true);
        $data = $form;

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->merge($adherent);
            $em->flush();
            $this->get('todotoday.account.services.adherent_manager')->updateMicrosoftOneSignal($adherent);
            $data = $adherent;
        }

        $view = $this->view($data);
        $view->getContext()->addGroups(array_merge($this->getDefaultGroups(), array('onesignal')));
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * Do migrationAction
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Resource description to patch entity",
     *     description="Operation on adherent",
     *     input={
     *           "class"="Todotoday\AccountBundle\Form\Api\AccountMigrationType"
     *     },
     *     output={
     *           "class"="Todotoday\AccountBundle\Entity\Adherent",
     *           "groups"={"Default"}
     *     },
     *     views={"Adherent"},
     *     section="Adherent"
     * )
     *
     * @FOSRest\Route("/migration", options={"expose"=true})
     *
     * @param Request $request
     *
     * @return Response
     * @throws \Todotoday\AccountBundle\Exceptions\AdherentNotFoundInAxException
     * @throws \Todotoday\AccountBundle\Exceptions\AdherentAlreayMigratedException
     * @throws \Symfony\Component\Security\Acl\Exception\AclAlreadyExistsException
     * @throws \Symfony\Component\Security\Acl\Exception\InvalidDomainObjectException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \LogicException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \InvalidArgumentException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \UnexpectedValueException
     */
    public function postMigrationAction(Request $request): Response
    {
        if (!$this->isGranted('CREATE', $this)) {
            throw $this->createAccessDeniedException();
        }

        if (!$agency = $this->get('todotoday.core.domain_context')->getCurrentContext()->getAgency()) {
            throw $this->createNotFoundException('No agency was found');
        }

        /** @var Logger $logger */
        $logger = $this->get('monolog.logger.migration');
        $logger->info('[Migration start]');

        $form = $this->createForm(
            AccountMigrationType::class,
            null,
            array(
                'csrf_protection' => false,
            )
        );
        $data = $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $logger->info('Submit form valid');

                $adherentManager = $this->get('todotoday.account.services.adherent_manager');

                $adherent = $adherentManager->migrateAdherent(
                    $data->getData(),
                    $agency
                );

                $em = $this->getDoctrine()->getManager();
                $em->persist($adherent);

                $adherentManager->checkPaymentMethod($adherent);

                $dispatcher = $this->get('event_dispatcher');
                $event = new RegistrationSuccessEvent($adherent);
                $dispatcher->dispatch(RegistrationSuccessEvent::USER_CREATED, $event);

                $em->flush();

                $data = $adherent;
                $logger->info('[Success] Adherent ' . $adherent->getFullName() . ' created !');
            } else {
                $logger->critical('[Migration Fail] ' . $data->getData());
            }
        }
        $view = $this->view($data);
        $view->getContext()->addGroups(array_merge($this->getDefaultGroups()));

        return $this->handleView($view);
    }

    /**
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Resource description to get entity",
     *     description="Operation on contactPerson",
     *     output={
     *           "class"="Todotoday\AccountBundle\Entity\Adherent",
     *           "groups"={"adherent"}
     *     },
     *     views={"Adherent"},
     *     section="Adherent"
     * )
     *
     * @FOSRest\QueryParam(
     *     name="fields",
     *     requirements="((contactPrincipals|contactChildren|contactAssistants),?)*",
     *     default="",
     *     description="Fields you need"
     * )
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return Response
     * @throws \LogicException
     * @throws \Todotoday\AccountBundle\Exceptions\NotAnAdherentException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    public function getAction(ParamFetcher $paramFetcher): Response
    {
        /** @var Adherent $adherent */
        if (!($adherent = $this->getUser()) instanceof Adherent) {
            throw new NotAnAdherentException('Only an adherent can perform this operation');
        }

        /** @var AdherentManager $adherentManager */
        $adherentManager = $this->get('todotoday.account.services.adherent_manager');

        $fields = explode(',', $paramFetcher->get('fields'));

        /** @var Adherent $account */
        $account = $adherentManager->getUser(
            $adherent,
            array(
                'contactPrincipals' => \in_array('contactPrincipals', $fields, true),
                'contactChildren' => \in_array('contactChildren', $fields, true),
                'contactAssistants' => \in_array('contactAssistants', $fields, true),
                'bankAccount' => false,
            )
        );

        $view = $this->view($account);
        $view->getContext()->addGroups(array('Default', 'personal', 'extra', 'contact_person'))->setSerializeNull(true);

        return $this->handleView($view);
    }

//    public function

    /**
     * @param Request $request
     * @param bool    $clearMissing
     *
     * @return Response
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \LogicException
     * @throws \Todotoday\AccountBundle\Exceptions\NotAnAdherentException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     */
    public function update(Request $request, bool $clearMissing = true): Response
    {
        /** @var Adherent $adherent */
        if (!($adherent = $this->getUser()) instanceof Adherent) {
            throw new NotAnAdherentException('Only an adherent can perform this operation');
        }

        $em = $this->getDoctrine()->getManager();

        $connection = $this->get('actiane.api_connector.connection.microsoft');
        $mapping = $connection->getMapping(AccountMicrosoft::class);
        $adherentMicrosoft = $this->get('todotoday.account.repository.api.account')->findByDoctrineAccount($adherent);
        $mapping->fillEntityInEntityDoctrine($adherentMicrosoft, $adherent);

        $requestData = $request->request->all();

        $data = $form = $this->createForm(
            AccountPersonalInfoType::class,
            $adherent,
            array(
                'csrf_protection' => false,
                'validation_groups' => ['Default'],
            )
        );

        $form->submit($requestData, $clearMissing);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $adherent;
            $mapping->fillEntityDoctrineInEntity($adherentMicrosoft, $adherent, true);
            $adherentMicrosoft->setPrimaryContactEmail($adherent->getEmail());
            $connection->flush();
            $em->flush();
        }

        $view = $this->view($data);
        $view->getContext()->addGroups(['Default'])->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Get principal contact or create one",
     *     description="Operation on contactPerson",
     *     output={
     *           "class"="Todotoday\AccountBundle\Entity\ContactPerson"
     *     },
     *     views={"Adherent"},
     *     section="Adherent"
     * )
     *
     * @FosRest\Route("/contact_principal")
     *
     * @return Response
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     *
     * @throws \Todotoday\AccountBundle\Exceptions\NotAnAdherentException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     **/
    public function getOrCreateContactPrincipalAction(): Response
    {
        /** @var Adherent $adherent */
        if (!($adherent = $this->getUser()) instanceof Adherent) {
            throw new NotAnAdherentException('Only an adherent can perform this operation');
        }

        $connection = $this->get('actiane.api_connector.connection.microsoft');
//
//        /** @var ContactPersonRepository $repo */
//        $repo = $connection->getRepository(ContactPerson::class);

        $adherentManager = $this->get('todotoday.account.services.adherent_manager');
        $adherent = $adherentManager->getUser(
            $adherent,
            array(
                'contactPrincipals' => true,
                'contactChildren' => false,
                'contactAssistants' => false,
                'bankAccount' => false,
                'custFields' => false,
            )
        );

        if (!$contact = $adherent->getContactPrincipal()) {
            $contact = new ContactPerson();
            $contact->setDataAreaId($adherent->getDataAreaId())
                ->setAssociatedPartyNumber($adherent->getPartyNumber())
                ->setFirstName($adherent->getFirstName())
                ->setLastName($adherent->getLastName())
                ->setContactType('Principal');

            $connection->persist($contact)->flush();
        }

        $view = $this->view($contact);

        return $this->handleView($view);
    }

    /**
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Search adherent on AX",
     *     description="Search adherent on AX",
     *     output={
     *           "class"="Todotoday\AccountBundle\Entity\Api\Microsoft\Account",
     *     },
     *     views={"Adherent"},
     *     section="Adherent"
     * )
     *
     * @FosRest\Route("/search-account-ax", options={"expose"=true})
     *
     * @FOSRest\QueryParam(name="account-number")
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return Response
     * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \InvalidArgumentException
     */
    public function getSearchAxAdherentByAccountNumberAction(ParamFetcher $paramFetcher): Response
    {
        if (!$this->isGranted('CREATE', $this)) {
            throw $this->createAccessDeniedException();
        }

        $accountNumber = $paramFetcher->get('account-number');
        if (!$accountNumber) {
            throw new BadRequestHttpException('AccountNumber is mandatory');
        }

        $microsoftRepo = $this->get('todotoday.account.repository.api.account');
        $adherents = $microsoftRepo->find(
            [
                'customerAccount' => $accountNumber,
                'dataAreaId' => substr($accountNumber, 0, 3),
            ]
        );

        $view = $this->view($adherents);
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Resource description to patch entity",
     *     description="Operation on contactPerson",
     *     input={
     *           "class"="Todotoday\AccountBundle\Form\Api\AccountPersonalInfoType"
     *     },
     *     output={
     *           "class"="Todotoday\AccountBundle\Entity\Adherent",
     *           "groups"={"adherent"}
     *     },
     *     views={"Adherent"},
     *     section="Adherent"
     * )
     *
     * @param Request $request
     *
     * @return Response
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Todotoday\AccountBundle\Exceptions\NotAnAdherentException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    public function patchAction(Request $request): Response
    {
        return $this->update($request, false);
    }

    /**
     * Get the formType for the current entity class link to the controller
     *
     * @return string
     */
    protected function getFormType(): string
    {
        return AdherentType::class;
    }

    /**
     * Get the serivce name from custum repository's entity
     *
     * @return string
     */
    protected function getServiceRepository(): string
    {
        return 'todotoday_account.repository.adherent';
    }
}
