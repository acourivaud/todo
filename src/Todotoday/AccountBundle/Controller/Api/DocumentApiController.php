<?php declare(strict_types=1);
/**
 * PHP version 7
 */

namespace Todotoday\AccountBundle\Controller\Api;

use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Request\ParamFetcher;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Response;
use Todotoday\CoreBundle\Controller\Api\AbstractApiController;
use Todotoday\CoreBundle\Entity\Agency;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use InvalidArgumentException;

/**
 * Class DocumentApiController
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Controller\Api
 * @FOSRest\NamePrefix("todotoday.account.api.document.")
 * @FOSRest\Prefix(value="/document")
 */
class DocumentApiController extends AbstractApiController
{

    /**
     * Post a document
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Operation on documents",
     *     description="Post a new document",
     *     views={"Default","Document"},
     *     section="document"
     * )
     *
     * @FosRest\Route("/add")
     * @FosRest\QueryParam(
     *     name="fields",
     *     requirements="((Default),?)*",
     *     default="",
     *     description="Fields you need"
     * )
     *
     * @param Request $request
     *
     * @return Response
     * @throws \Symfony\Component\Security\Acl\Exception\AclNotFoundException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \Todotoday\SocialBundle\Exceptions\SocialForbiddenException
     * @throws \Todotoday\SocialBundle\Exceptions\SocialBadRequestException
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Symfony\Component\Finder\Exception\AccessDeniedException
     * @throws \Symfony\Component\Security\Acl\Exception\InvalidDomainObjectException
     * @throws \Symfony\Component\Security\Acl\Exception\AclAlreadyExistsException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \UnexpectedValueException
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \LogicException
     */
    public function postDocumentAction(Request $request): Response
    {
        if (!$this->isGranted('ROLE_ADHERENT')) {
            throw $this->createAccessDeniedException();
        }

        /** @var \Todotoday\AccountBundle\Services\AdherentManager $adherentManager */
        $adherentManager = $this->get('todotoday.account.services.adherent_manager');

        $formData = [
            'title' => $request->get('title'),
            'file' => $request->files->get('file'),
        ];

        if ($formData['file'] instanceof UploadedFile) {
            $document = $adherentManager->uploadMedia($formData);
        }

        $view = $this->view($document);
        $view->getContext()->addGroups(array_merge($this->getDefaultGroups(), array('adherent')));
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * @FosRest\Route("/sharedfiles", options={"expose"=true})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getSharedFilesAction(): Response
    {
        /** @var \Todotoday\AccountBundle\Services\AdherentManager $adherentManager */
        $adherentManager = $this->get('todotoday.account.services.adherent_manager');

        $sharedFiles = $adherentManager->getMedia();

        $view = $this->view($sharedFiles);
        $view->getContext()->addGroups(array_merge($this->getDefaultGroups(), array('adherent')));
        $view->getContext()->setSerializeNull(true)->enableMaxDepth();

        return $this->handleView($view);
    }

    /**
     * @FosRest\Route("/files", options={"expose"=true})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getFilesAction(Request $request): Response
    {
        $files = $this->get('todotoday_cms.repository.file')->findAllByLocale($request->getLocale());

        $view = $this->view($files);
        $view->getContext()->addGroups(array_merge($this->getDefaultGroups(), array('adherent')));
        $view->getContext()->setSerializeNull(true)->enableMaxDepth();

        return $this->handleView($view);
    }

    /**
     * @FosRest\Route("/cgv", options={"expose"=true})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getCgvAction(Request $request): Response
    {
        if (!$agency = $this->get('todotoday.core.domain_context')->getCurrentContext()->getAgency()) {
            throw $this->createNotFoundException();
        }

        $cgv = $agency->getCgv();

        $view = $this->view($cgv);
        $view->getContext()->addGroups(array_merge($this->getDefaultGroups(), array('adherent')));
        $view->getContext()->setSerializeNull(true)->enableMaxDepth();

        return $this->handleView($view);
    }

/**
     * Get the formType for the current entity class link to the controller
     *
     * @return string
     */
    protected function getFormType(): string
    {
        return '';
    }

    /**
     * Get the serivce name from custum repository's entity
     *
     * @return string
     */
    protected function getServiceRepository(): string
    {
        return 'todotoday_account.repository.document';
    }

    /**
     * @return Agency
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \UnexpectedValueException
     * @throws InvalidArgumentException
     */
    private function getAgency(): ?Agency
    {
        if (!$agency = $this->get('todotoday.core.domain_context.api')->getAgency()) {
            throw new InvalidArgumentException('Agency is missing');
        }

        return $agency;
    }
}
