<?php declare(strict_types=1);

/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 30/06/2017
 * Time: 03:49
 */

namespace Todotoday\AccountBundle\Controller\Api;

use Actiane\ToolsBundle\Traits\LoggerTrait;
use FOS\RestBundle\Controller\Annotations\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Exceptions\AccountNotFoundException;
use Todotoday\AccountBundle\Form\AccountType;
use Todotoday\AccountBundle\Repository\AdherentRepository;
use Todotoday\CoreBundle\Controller\Api\AbstractApiController;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Enum\AgencyModuleEnum;
use Todotoday\CoreBundle\Exceptions\AgencyMissingException;
use Todotoday\CoreBundle\Exceptions\AgencyModuleDisabledException;
use Todotoday\AccountBundle\Controller\RegistrationController;
use Todotoday\AccountBundle\Enum\AdherentTypeEnum;

/**
 * @FosRest\Prefix(value="adherent/coadherent")
 * @FosRest\NamePrefix("todotoday.account.api.adherent.coadherent.")
 * Class AdherentCoAdherentApiController
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Controller\Api
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class AdherentCoAdherentApiController extends AbstractApiController
{
    use LoggerTrait;

    /**
     * Get all coAdherent for the current user
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Get all coAdherent for the current user
     *     co-adherent.", description="Get all coAdherent for the current user
     *     co-adherent", output={
     *           "class"="array<Todotoday\AccountBundle\Entity\Account>",
     *     },
     *     views={"Adherent"},
     *     section="CoAdherent"
     * )
     *
     * @Route("")
     *
     * @return Response
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \UnexpectedValueException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingException
     * @throws \InvalidArgumentException
     * @throws \LogicException
     * @throws AgencyModuleDisabledException
     */
    public function getAllAction(): Response
    {
        /** @var Agency $agency */
        $agency = $this->getAgency();

        if (!$agency->hasModule(AgencyModuleEnum::COADH)) {
            throw new AgencyModuleDisabledException();
        }

        $adherentRepo = $this->getDoctrine()
            ->getManager()
            ->getRepository(Adherent::class);
        $coAdherents = $adherentRepo->findBy(
            array(
                'parent' => $this->getUser(),
                'enabled' => true,
            )
        );

        $view = $this->view($coAdherents);
        $groups = $this->getDefaultGroups();
        $view->getContext()->addGroups($groups)->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * Get single coAdherent account of current user by id
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Get single coAdherent account of current user by id",
     *     description="Get single coAdherent account of current user by id",
     *     output={
     *           "class"="Todotoday\AccountBundle\Entity\Account",
     *           "groups"={"Default", "account"}
     *     },
     *     views={"Adherent"},
     *     section="CoAdherent"
     * )
     *
     * @param string $id
     *
     * @return Response
     * @throws \LogicException
     */
    public function getAction(string $id): Response
    {
        $coAdherent = $this->getRepository()->findOneBy(array('parent' => $this->getUser(), 'customerAccount' => $id));

        $view = $this->view($coAdherent);
        $groups = $this->getDefaultGroups();
        $view->getContext()->addGroups($groups)->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * Post co-adherent
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Post co-adherent",
     *     description="Post co-adherent",
     *     output={
     *           "class"="array<Todotoday\AccountBundle\Entity\Account>",
     *           "groups"={"Default", "account"}
     *     },
     *     views={"Adherent"},
     *     section="CoAdherent"
     * )
     *
     * @Route("/new")
     *
     * @param Request $request
     *
     * @return Response
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \Symfony\Component\Validator\Exception\MissingOptionsException
     * @throws \Symfony\Component\Validator\Exception\InvalidOptionsException
     * @throws \Symfony\Component\Validator\Exception\ConstraintDefinitionException
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\Form\Exception\UnexpectedTypeException
     * @throws \Symfony\Component\Form\Exception\LogicException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \RuntimeException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     * @throws AgencyModuleDisabledException
     */
    public function postNewAction(Request $request): Response
    {
        /** @var Agency $agency */
        $agency = $this->getAgency();
        /** @var Adherent $adherent */
        $adherent = $this->getUser();

        if (!$agency->hasModule(AgencyModuleEnum::COADH)) {
            throw new AgencyModuleDisabledException();
        }

        $adherentManager = $this->get('todotoday.account.services.adherent_manager');
        $values = $adherentManager->addCoAdherent($request, $agency, $adherent, true);

        $errors = array();

        if (isset($values['error'])) {
            $errors[] = $values['error'];
        }

        /** @var Form $form */
        $form = $values['form'];

        if ($form && !$values['success']) {
            foreach ($form->getErrors(true, true) as $error) {
                $errors[] = $error->getOrigin()->getName() . ' : ' . $error->getMessage();
            }
        }

        if (isset($values['success'])) {
            $roles = array(RegistrationController::$groupToRole[AdherentTypeEnum::ADH_CO]);
            $adherent->setRoles($roles);
        }

        $view = $this->view(
            array(
                'coAdherents' => $values['coAdherents'],
                'coAdherentFull' => $values['coAdherentFull'],
                'errors' => $errors,
            ),
            (count($errors) > 0) ? 500 : null
        );

        $groups = $this->getDefaultGroups();
        $view->getContext()->addGroups($groups);
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * Disable coAdherent account of current user
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Disable coAdherent account of current user",
     *     description="Disable coAdherent account of current user",
     *     output={
     *           "class"="array<Todotoday\AccountBundle\Entity\Account>",
     *           "groups"={"Default", "account"}
     *     },
     *     views={"Adherent"},
     *     section="CoAdherent"
     * )
     *
     * @Route("/{id}/disable")
     *
     * @param string $id
     *
     * @return Response
     * @throws \InvalidArgumentException
     * @throws \LogicException
     * @throws AccountNotFoundException
     */
    public function putDisableAction(string $id): Response
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(Adherent::class);

        /** @var Adherent $coAdherent */
        $coAdherent = $repo->findOneBy(
            array(
                'id' => $id,
                'parent' => $this->getUser(),
                'enabled' => true,
            )
        );
        if (!$coAdherent) {
            throw new AccountNotFoundException();
        }

        $coAdherent->setEnabled(false);
        $em->persist($coAdherent);
        $em->flush();

        $remainingCoAdherents = $repo->findOneBy(
            array(
                'parent' => $this->getUser(),
                'enabled' => true,
            )
        );

        $view = $this->view($remainingCoAdherents);

        return $this->handleView($view);
    }

    /**
     * Get the formType for the current entity class link to the controller
     *
     * @return string
     */
    protected function getFormType(): string
    {
        // TODO: Implement getFormType() method.
    }

    /**
     * Get the serivce name from custum repository's entity
     *
     * @return string
     */
    protected function getServiceRepository(): string
    {
        return 'todotoday_account.repository.adherent';
    }

    /**
     * Do getRepository
     *
     * @return object|AdherentRepository
     */
    protected function getRepository(): AdherentRepository
    {
        return $this->get($this->getServiceRepository());
    }

    /**
     * @return Agency
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingException
     * @throws \UnexpectedValueException
     * @throws \InvalidArgumentException
     */
    private function getAgency(): Agency
    {
        if (!$agency = $this->get('todotoday.core.domain_context.api')->getAgency()) {
            throw new AgencyMissingException('Agency is missing');
        }

        return $agency;
    }
}
