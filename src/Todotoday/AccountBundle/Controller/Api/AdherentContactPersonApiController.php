<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 27/03/17
 * Time: 17:07
 */

namespace Todotoday\AccountBundle\Controller\Api;

use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Request\ParamFetcher;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Entity\Api\Microsoft\ContactPerson;
use Todotoday\AccountBundle\Exceptions\NotAnAdherentException;
use Todotoday\AccountBundle\Form\AccountPersonalInfoType;
use Todotoday\AccountBundle\Form\Api\AccountContactPersonsType;
use Todotoday\AccountBundle\Form\Api\AccountContactPersonType;
use Todotoday\AccountBundle\Form\Api\AccountPersonalInfoExtraType;
use Todotoday\CoreBundle\Controller\Api\AbstractApiController;

/**
 * Class AdherentContactPersonApiController
 *
 * @FosRest\Prefix(value="adherent/contact_person")
 * @FosRest\NamePrefix("todotoday.account.api.adherent.info.")
 */
class AdherentContactPersonApiController extends AbstractApiController
{
    /**
     * @var bool[]
     */
    public static $optionsExtra = array(
        'contactPrincipals' => false,
        'contactChildren' => true,
        'contactAssistants' => true,
        'bankAccount' => false,
    );

    /**
     * @var bool[]
     */
    public static $optionsPersonal = array(
        'contactPrincipals' => true,
        'contactChildren' => false,
        'contactAssistants' => false,
        'bankAccount' => false,
    );

    /**
     * Do getExtraAction
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Resource description to get entity",
     *     description="API Rest description to get entity",
     *     views={"Adherent"},
     *     section="Adherent-ContactPerson",
     *     output={
     *           "class"="Todotoday\AccountBundle\Entity\Adherent",
     *           "groups"={"extra", "contact_person"}
     *     }
     * )
     *
     * @return Response
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     * @throws \Todotoday\AccountBundle\Exceptions\NotAnAdherentException
     */
    public function getExtraAction(): Response
    {
        return $this->getContact(array('extra', 'contact_person'), static::$optionsExtra);
    }

    /**
     * Do getExtraAction
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Resource description to get entity",
     *     description="API Rest description to get entity",
     *     views={"Adherent"},
     *     section="Adherent-ContactPerson",
     *     output={
     *           "class"="Todotoday\AccountBundle\Entity\Adherent",
     *           "groups"={"personal", "contact_person"}
     *     }
     * )
     *
     * @return Response
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     * @throws \Todotoday\AccountBundle\Exceptions\NotAnAdherentException
     */
    public function getPersonal(): Response
    {
        //TODO n'est plus utilisé
        return $this->getContact(array('personal', 'contact_person'), static::$optionsPersonal);
    }

    /**
     * Delete specified ContactPerson linked to the current account
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Resource description to patch entityp",
     *     description="Delete specified ContactPerson linked to the current account",
     *     views={"Adherent"},
     *     section="Adherent-ContactPerson"
     * )
     *
     * @FOSRest\RequestParam(map=true, name="ids", strict=true, description="List of ids")
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return void
     * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
     * @throws \InvalidArgumentException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     */
    public function deleteAllAction(ParamFetcher $paramFetcher): void
    {
        $contactPersonsIds = $paramFetcher->get('ids');
        $connection = $this->get('actiane.api_connector.connection.microsoft');

        /** @var ContactPerson[] $contactPersons */
        $contactPersons = $this->get('todotoday.account.repository.api.contact_person')
            ->findContactsPersonByAccount($this->getUser());

        foreach ($contactPersons as $contactPerson) {
            if (!$contactPersonsIds || in_array($contactPerson->getContactPersonId(), $contactPersonsIds, true)) {
                $connection->delete($contactPerson);
            }
        }

        $connection->flush();
    }

    /**
     * Delete specified ContactPerson linked to the current account
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Resource description to patch entityp",
     *     description="Delete specified ContactPerson linked to the current account",
     *     views={"Adherent"},
     *     section="Adherent-ContactPerson"
     * )
     *
     * @param string $id
     *
     * @return void
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \InvalidArgumentException
     *
     */
    public function deleteAction(string $id): void
    {
        $connection = $this->get('actiane.api_connector.connection.microsoft');

        $contactPerson = $this->get('todotoday.account.repository.api.contact_person')
            ->findContactsPersonByAccountAndId($this->getUser(), $id);

        if (!$contactPerson) {
            throw $this->createNotFoundException();
        }

        $connection->delete($contactPerson);

        $connection->flush();
    }

    /**
     * Do getAllAction
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Resource description to get entity",
     *     description="API Rest description to get entity",
     *     views={"Adherent"},
     *     section="Adherent-ContactPerson",
     *     output={
     *           "class"="array<Todotoday\AccountBundle\Entity\Api\ContactPerson>",
     *           "groups"={"contact_person"}
     *     }
     * )
     *
     * @return Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \InvalidArgumentException
     * @throws \LogicException
     */
    public function getAllAction(): Response
    {
        $contactPersons = $this->get('todotoday.account.repository.api.contact_person')
            ->findContactsPersonByAccount($this->getUser());

        $view = $this->view($contactPersons);
        $view->getContext()->addGroups(array('contact_person'));
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * Do getAction
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Resource description to get entity",
     *     description="API Rest description to get entity",
     *     views={"Adherent"},
     *     section="Adherent-ContactPerson",
     *     output={
     *           "class"="Todotoday\AccountBundle\Entity\Api\ContactPerson",
     *           "groups"={"contact_person"}
     *     }
     * )
     *
     * @param string $id
     *
     * @return Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \InvalidArgumentException
     * @throws \LogicException
     */
    public function getAction(string $id): Response
    {
        $contactPerson = $this->get('todotoday.account.repository.api.contact_person')
            ->findContactsPersonByAccountAndId($this->getUser(), $id);

//        if (!$contactPerson) {
//            throw $this->createNotFoundException();
//        }

        $view = $this->view($contactPerson);
        $view->getContext()->addGroups(array('contact_person'));
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * Do patchAllAction
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Resource description to patch entity",
     *     description="Operation on contactPerson",
     *     input={
     *           "class"="Todotoday\AccountBundle\Form\Api\AccountContactPersonsType"
     *     },
     *     output={
     *           "class"="array<Todotoday\AccountBundle\Entity\Api\Microsoft\ContactPerson>",
     *           "groups"={"contact_person"}
     *     },
     *     views={"Adherent"},
     *     section="Adherent-ContactPerson"
     * )
     *
     * @FOSRest\RequestParam(map=true, name="ids", strict=true, description="List of ids")
     *
     * @param Request      $request
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return Response
     * @throws \LogicException
     * @throws \Todotoday\AccountBundle\Exceptions\NotAnAdherentException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \InvalidArgumentException
     */
    public function patchAllAction(Request $request, ParamFetcher $paramFetcher): Response
    {
        return $this->updateAll($request, $paramFetcher, false);
    }

    /**
     * Do patchAction
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Resource description to patch entity",
     *     description="Operation on contactPerson",
     *     input={
     *           "class"="Todotoday\AccountBundle\Form\Api\AccountContactPersonType"
     *     },
     *     output={
     *           "class"="Todotoday\AccountBundle\Entity\Api\Microsoft\ContactPerson",
     *           "groups"={"contact_person"}
     *     },
     *     views={"Adherent"},
     *     section="Adherent-ContactPerson"
     * )
     *
     * @param Request $request
     * @param string  $id
     *
     * @return Response
     * @throws \Todotoday\AccountBundle\Exceptions\NotAnAdherentException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    public function patchAction(Request $request, string $id): Response
    {
        return $this->update($request, $id, false);
    }

    /**
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Resource description to patch entity",
     *     description="Operation on contactPerson",
     *     input={
     *           "class"="Todotoday\AccountBundle\Form\Api\AccountPersonalInfoExtraType"
     *     },
     *     output={
     *           "class"="Todotoday\AccountBundle\Entity\Adherent",
     *           "groups"={"extra", "contact_person"}
     *     },
     *     views={"Adherent"},
     *     section="Adherent-ContactPerson"
     * )
     *
     * @param Request $request
     *
     * @return Response
     * @throws \Exception
     */
    public function patchExtraAction(
        Request $request
    ): Response {
        return $this->updateContact(
            $request,
            AccountPersonalInfoExtraType::class,
            static::$optionsExtra,
            'extra',
            false
        );
    }

    /**
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Resource description to patch entity",
     *     description="Operation on contactPerson",
     *     input={
     *           "class"="Todotoday\AccountBundle\Form\Api\AccountPersonalInfoType"
     *     },
     *     output={
     *           "class"="Todotoday\AccountBundle\Entity\Adherent",
     *           "groups"={"personal"}
     *     },
     *     views={"Adherent"},
     *     section="Adherent-ContactPerson"
     * )
     *
     * @param Request $request
     *
     * @return Response
     * @throws \Exception
     */
    public function patchPersonal(
        Request $request
    ): Response {
        return $this->updateContact(
            $request,
            AccountPersonalInfoType::class,
            static::$optionsPersonal,
            'personal',
            false
        );
    }

    /**
     * Do postAllAction
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Resource description to patch entity",
     *     description="Operation on contactPerson",
     *     input={
     *           "class"="Todotoday\AccountBundle\Form\Api\AccountContactPersonsType"
     *     },
     *     output={
     *           "class"="array<Todotoday\AccountBundle\Entity\Api\Microsoft\ContactPerson>",
     *           "groups"={"contact_person"}
     *     },
     *     views={"Adherent"},
     *     section="Adherent-ContactPerson"
     * )
     *
     * @param Request $request
     *
     * @FOSRest\Route("/all")
     *
     * @return Response
     * @throws \LogicException
     * @throws \Todotoday\AccountBundle\Exceptions\NotAnAdherentException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \InvalidArgumentException
     */
    public function postAllAction(Request $request): Response
    {
        /** @var Adherent $adherent */
        if (!($adherent = $this->getUser()) instanceof Adherent) {
            throw new NotAnAdherentException('Only an adherent can perform this operation');
        }

        $data = $form = $this->createForm(
            AccountContactPersonsType::class,
            null,
            array(
                'csrf_protection' => false,
            )
        );

        $form->submit($request->request->all());

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var array[] $data */
            $data = $form->getData();
            $connection = $this->get('actiane.api_connector.connection.microsoft');
            /** @var ContactPerson $contactPerson */
            foreach ($data['contactPersons'] as $contactPerson) {
                $contactPerson
                    ->setAssociatedPartyNumber($adherent->getPartyNumber())
                    ->setDataAreaId($adherent->getDataAreaId());
                $connection->persist($contactPerson);
            }
            $connection->flush();
        }

        $view = $this->view($data);
        $view->getContext()->setGroups(array('contact_person'));
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * Do postAction
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Resource description to patch entity",
     *     description="Operation on contactPerson",
     *     input={
     *           "class"="Todotoday\AccountBundle\Form\Api\AccountContactPersonType"
     *     },
     *     output={
     *           "class"="Todotoday\AccountBundle\Entity\Api\Microsoft\ContactPerson",
     *           "groups"={"contact_person"}
     *     },
     *     views={"Adherent"},
     *     section="Adherent-ContactPerson"
     * )
     *
     * @param Request $request
     *
     * @return Response
     * @throws \LogicException
     * @throws \Todotoday\AccountBundle\Exceptions\NotAnAdherentException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \InvalidArgumentException
     */
    public function postAction(Request $request): Response
    {
        /** @var Adherent $adherent */
        if (!($adherent = $this->getUser()) instanceof Adherent) {
            throw new NotAnAdherentException('Only an adherent can perform this operation');
        }

        $data = $form = $this->createForm(
            AccountContactPersonType::class,
            null,
            array(
                'csrf_protection' => false,
            )
        );

        $form->submit($request->request->all());

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var ContactPerson $data */
            $data = $form->getData();
            $connection = $this->get('actiane.api_connector.connection.microsoft');
            $data
                ->setAssociatedPartyNumber($adherent->getPartyNumber())
                ->setDataAreaId($adherent->getDataAreaId());
            $connection->persist($data);
            $connection->flush();
        }

        $view = $this->view($data);
        $view->getContext()->setGroups(array('contact_person'));
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * Do putAllAction
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Resource description to patch entity",
     *     description="Operation on contactPerson",
     *     input={
     *           "class"="Todotoday\AccountBundle\Form\Api\AccountContactPersonsType"
     *     },
     *     output={
     *           "class"="array<Todotoday\AccountBundle\Entity\Api\Microsoft\ContactPerson>",
     *           "groups"={"contact_person"}
     *     },
     *     views={"Adherent"},
     *     section="Adherent-ContactPerson"
     * )
     *
     * @FOSRest\RequestParam(map=true, name="ids", requirements="\w+", strict=true, description="List of ids")
     *
     * @param Request      $request
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return Response
     * @throws \LogicException
     * @throws \Todotoday\AccountBundle\Exceptions\NotAnAdherentException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \InvalidArgumentException
     */
    public function putAllAction(Request $request, ParamFetcher $paramFetcher): Response
    {
        return $this->updateAll($request, $paramFetcher);
    }

    /**
     * Do postAction
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Resource description to patch entity",
     *     description="Operation on contactPerson",
     *     input={
     *           "class"="Todotoday\AccountBundle\Form\Api\AccountContactPersonType"
     *     },
     *     output={
     *           "class"="Todotoday\AccountBundle\Entity\Api\Microsoft\ContactPerson",
     *           "groups"={"contact_person"}
     *     },
     *     views={"Adherent"},
     *     section="Adherent-ContactPerson"
     * )
     *
     * @param Request $request
     * @param string  $id
     *
     * @return Response
     * @throws \Todotoday\AccountBundle\Exceptions\NotAnAdherentException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     *
     */
    public function putAction(Request $request, string $id): Response
    {
        return $this->update($request, $id);
    }

    /**
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Resource description to patch entity",
     *     description="Operation on contactPerson",
     *     input={
     *           "class"="Todotoday\AccountBundle\Form\Api\AccountPersonalInfoExtraType"
     *     },
     *     output={
     *           "class"="Todotoday\AccountBundle\Entity\Adherent",
     *           "groups"={"extra", "contact_person"}
     *     },
     *     views={"Adherent"},
     *     section="Adherent-ContactPerson"
     * )
     *
     * @param Request $request
     *
     * @return Response
     * @throws \Exception
     */
    public function putExtraAction(
        Request $request
    ): Response {
        return $this->updateContact($request, AccountPersonalInfoExtraType::class, static::$optionsExtra, 'extra');
    }

    /**
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Resource description to patch entity",
     *     description="Operation on contactPerson",
     *     input={
     *           "class"="Todotoday\AccountBundle\Form\Api\AccountPersonalInfoType"
     *     },
     *     output={
     *           "class"="Todotoday\AccountBundle\Entity\Adherent",
     *           "groups"={"personal"}
     *     },
     *     views={"Adherent"},
     *     section="Adherent-ContactPerson"
     * )
     *
     * @param Request $request
     *
     * @return Response
     * @throws \Exception
     */
    public function putPersonal(
        Request $request
    ): Response {
        return $this->updateContact($request, AccountPersonalInfoType::class, static::$optionsPersonal, 'personal');
    }

    /**
     * Do getContact
     *
     * @param array $groups
     *
     * @param array $options
     *
     * @return Response
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     * @throws \Todotoday\AccountBundle\Exceptions\NotAnAdherentException
     */
    protected function getContact(
        array $groups,
        array $options
    ): Response {
        $adherentManager = $this->get('todotoday.account.services.adherent_manager');
        /** @var Adherent $adherent */
        if (!($adherent = $adherentManager->getUser(null, $options)) instanceof Adherent) {
            throw new NotAnAdherentException('Only an adherent can perform this operation');
        }

        $view = $this->view($adherent);
        $view->getContext()->addGroups($groups);
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * Get the formType for the current entity class link to the controller
     *
     * @return string
     */
    protected function getFormType(): string
    {
        return AccountPersonalInfoType::class;
    }

    /**
     * Get the serivce name from custum repository's entity
     *
     * @return string
     */
    protected function getServiceRepository(): string
    {
        return 'todotoday_account.repository.adherent';
    }

    /**
     * Do update
     *
     * @param Request $request
     * @param string  $id
     * @param bool    $clearMissing
     *
     * @return Response
     * @throws \LogicException
     * @throws \Todotoday\AccountBundle\Exceptions\NotAnAdherentException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \InvalidArgumentException
     */
    protected function update(Request $request, string $id, bool $clearMissing = true): Response
    {
        /** @var Adherent $adherent */
        if (!($adherent = $this->getUser()) instanceof Adherent) {
            throw new NotAnAdherentException('Only an adherent can perform this operation');
        }

        $contactPersonRepo = $this->get('todotoday.account.repository.api.contact_person');
        $requestData = $request->request->all();

        $contactPersons = $contactPersonRepo->findContactsPersonByAccountAndId($adherent, $id);

        $data = $form = $this->createForm(
            AccountContactPersonType::class,
            $contactPersons,
            array('csrf_protection' => false)
        );

        $form->submit($requestData, $clearMissing);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var ContactPerson $data */
            $data = $form->getData();
            $connection = $this->get('actiane.api_connector.connection.microsoft');
            $connection->flush();
        }

        $view = $this->view($data);
        $view->getContext()->setGroups(array('contact_person'));
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * Do update
     *
     * @param Request      $request
     * @param ParamFetcher $paramFetcher
     * @param bool         $clearMissing
     *
     * @return Response
     * @throws \LogicException
     * @throws \Todotoday\AccountBundle\Exceptions\NotAnAdherentException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \InvalidArgumentException
     */
    protected function updateAll(Request $request, ParamFetcher $paramFetcher, bool $clearMissing = true): Response
    {
        /** @var Adherent $adherent */
        if (!($adherent = $this->getUser()) instanceof Adherent) {
            throw new NotAnAdherentException('Only an adherent can perform this operation');
        }

        $contactPersonRepo = $this->get('todotoday.account.repository.api.contact_person');
        $requestData = $request->request->all();

        if ($contactPersonsIds = $paramFetcher->get('ids')) {
            unset($requestData['ids']);
            $contactPersons = $contactPersonRepo->findContactsPersonByAccountAndIds($adherent, $contactPersonsIds);
        } else {
            $contactPersons = $contactPersonRepo->findContactsPersonByAccount($adherent);
        }
        $contactPersonsIds = array();
        /** @var ContactPerson[] $contactPersons */
        foreach ($contactPersons as $contactPerson) {
            $contactPersonsIds[$contactPerson->getContactPersonId()] = $contactPerson;
        }

        $data = $form = $this->createForm(
            AccountContactPersonsType::class,
            array('contactPersons' => $contactPersons),
            array('csrf_protection' => false)
        );

        $form->submit($requestData, $clearMissing);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var array[] $data */
            $data = $form->getData();
            $connection = $this->get('actiane.api_connector.connection.microsoft');
            /** @var ContactPerson $contactPerson */
            foreach ($data['contactPersons'] as $contactPerson) {
                $contactPerson
                    ->setAssociatedPartyNumber($adherent->getPartyNumber())
                    ->setDataAreaId($adherent->getDataAreaId());
                $connection->persist($contactPerson);
                if ($id = $contactPerson->getContactPersonId()) {
                    unset($contactPersonsIds[$id]);
                }
            }
            foreach ($contactPersonsIds as $contactPerson) {
                $connection->delete($contactPerson);
            }
            $connection->flush();
        }

        $view = $this->view($data);
        $view->getContext()->setGroups(array('contact_person'));
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * Do putContact
     *
     * @param Request $request
     *
     * @param string  $formType
     * @param array   $options
     * @param string  $group
     *
     * @param bool    $clearMissing
     *
     * @return Response
     * @throws \Todotoday\AccountBundle\Exceptions\NotAnAdherentException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \LogicException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \InvalidArgumentException
     */
    protected function updateContact(
        Request $request,
        string $formType,
        array $options,
        string $group,
        bool $clearMissing = true
    ): Response {
        $adherentManager = $this->get('todotoday.account.services.adherent_manager');
        /** @var Adherent $adherent */
        if (!($adherent = $adherentManager->getUser(null, $options)) instanceof Adherent) {
            throw new NotAnAdherentException('Only an adherent can perform this operation');
        }

        $options = array(
            'csrf_protection' => false,
        );

        if ($formType === AccountPersonalInfoType::class) {
            $options['is_for_api'] = true;
        }

        $data = $form = $this->createForm(
            $formType,
            $adherent,
            $options
        );

        $form->submit($request->request->all(), $clearMissing);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->merge($adherent);
            $em->flush();
            $adherentManager->updateMicrosoftAccount($adherent, $options);
            $data = $adherent;
        }

        $view = $this->view($data);
        $view->getContext()->setGroups(array($group, 'contact_person'));
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }
}
