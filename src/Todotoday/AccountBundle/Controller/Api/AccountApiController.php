<?php

declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 27/01/2017
 * Time: 22:47
 */

namespace Todotoday\AccountBundle\Controller\Api;

use Actiane\AlertBundle\Enum\AlertStatusEnum;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Model\DomainObjectInterface;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Enum\PaymentStatusEnum;
use Todotoday\AccountBundle\Event\RegistrationSuccessEvent;
use Todotoday\AccountBundle\Exceptions\AccountNotFoundException;
use Todotoday\AccountBundle\Form\Api\AccountPaymentMethodStatusType;
use Todotoday\AccountBundle\Repository\AdherentRepository;
use Todotoday\CoreBundle\Annotation\Api\ForceDomainVote;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class AccountController
 *
 * @FOSRest\Prefix(value="account")
 * @FOSRest\NamePrefix(value="todotoday.account.api.account.")
 *
 * @SuppressWarnings(PHPMD)
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Controller\Api
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class AccountApiController extends FOSRestController implements DomainObjectInterface
{
    /**
     * Get single account by id
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Operations on accounts.",
     *     description="Get single account by id",
     *     output={
     *           "class"="Todotoday\AccountBundle\Entity\Account",
     *           "groups"={"Default", "account"}
     *     },
     *     views={"default","account"},
     *     section="Account"
     * )
     *
     * @FOSRest\QueryParam(
     *     name="fields",
     *     requirements="((account),?)*",
     *     default="",
     *     description="Fields you need"
     * )
     *
     * @FosRest\Route("/{id}", requirements={"id" = "\d+"})
     *
     * @param ParamFetcher $paramFetcher
     * @param int          $id
     *
     * @return Response
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \LogicException
     */
    public function getAction(ParamFetcher $paramFetcher, int $id): Response
    {
        if (!$this->isGranted('VIEW', $this)) {
            throw $this->createAccessDeniedException('Fail');
        }

        $accounts = $this->getDoctrine()->getManager()->getRepository(Account::class)->find($id);
        $view = $this->view($accounts);
        $groups = ['Default'];
        if ($fields = explode(',', $paramFetcher->get('fields'))) {
            $groups = array_merge($groups, $fields);
        }
        $view->getContext()->addGroups($groups);
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * Get all accounts with limit 10 by default
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Operations on accounts.",
     *     description="Get all accounts with limit 10 by default",
     *     output={
     *           "class"="array<Todotoday\AccountBundle\Entity\Account> as accounts",
     *           "groups"={"Default", "account","agency"}
     *     },
     *     views={"default","account"},
     *     section="Account"
     * )
     *
     * @FOSRest\Route("")
     *
     * @FOSRest\QueryParam(
     *     name="fields",
     *     requirements="((account|agency),?)*",
     *     default="",
     *     description="Fields you need"
     * )
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return Response
     * @throws \Symfony\Component\Security\Acl\Exception\InvalidDomainObjectException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\Security\Acl\Exception\AclNotFoundException
     * @throws \Symfony\Component\Security\Acl\Exception\NoAceFoundException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    public function getAllAction(ParamFetcher $paramFetcher): Response
    {
        if (!$this->isGranted('VIEW', $this)) {
            throw $this->createAccessDeniedException('Fail');
        }

        $accounts = $this->getDoctrine()->getManager()->getRepository(Account::class)->findAll();
        $view = $this->view($accounts);
        $groups = ['Default'];
        if ($fields = explode(',', $paramFetcher->get('fields'))) {
            $groups = array_merge($groups, $fields);
        }
        $view->getContext()->addGroups($groups);
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * Get account information from current connection
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Operations on accounts.",
     *     description="Get current account information",
     *     output={
     *           "class"="Todotoday\AccountBundle\Entity\Account",
     *           "groups"={"Default", "account"}
     *     },
     *     views={"default","account"},
     *     section="Account"
     * )
     * @ForceDomainVote(Voter::ACCESS_GRANTED)
     *
     * @FosRest\Route("/me", options={"expose"=true})
     *
     * @param Request $request
     *
     * @return Response
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \LogicException
     */
    public function getMeAction(Request $request): Response
    {
        /** @var Account $user */
        $user = $this->getUser();
        $account = $this->getDoctrine()->getRepository(Account::class)->findOneBy(
            array(
                'username' => $user->getUsername(),
            )
        );

        $locale = $request->getLocale();

        $fgssManager = $this->get('todotoday.fgs.manager');
        $account->setFgsHierarchy(
            $fgssManager->getFGSSubjectHierarchyTranslate($locale)
        );

        $orderManager = $this->get('todotoday.order.manager');
        $account->setOrderSatisfactionSubjects(
            $orderManager->getOrderSatisfactionSubjectsTranslate($locale)
        );

        $groups = ['Default', 'news', 'account', 'carts', 'payment', 'fgs'];
        $view = $this->view($account);
        $view->getContext()->addGroups($groups);
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * Returns a unique identifier for this domain object.
     *
     * @return string
     */
    public function getObjectIdentifier(): string
    {
        return 'class';
    }

    /**
     * Post form to create new account
     * If all ok, it return the new account if not it return form with the errors
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Operations on accounts.",
     *     description="Post form to create new account",
     *     requirements={
     *      {
     *          "name"="type",
     *          "dataType"="string",
     *          "requirements"="(adherent|concierge)",
     *          "description"="Create adherent's account or concierge's account (adherent|concierge)"
     *      }
     *     },
     *     input={
     *           "class"="Todotoday\AccountBundle\Form\Api\AccountType"
     *     },
     *     output={
     *           "class"="Todotoday\AccountBundle\Entity\Account",
     *           "groups"={"Default", "account"}
     *     },
     *     views={"default","account"},
     *     section="Account"
     * )
     *
     * @FOSRest\Route("/{type}", requirements={"type" = "\w+"})
     *
     * @FOSRest\QueryParam(
     *     name="fields",
     *     requirements="((account),?)*",
     *     default="",
     *     description="Fields you need"
     * )
     *
     * @param ParamFetcher $paramFetcher
     * @param Request      $request
     * @param string       $type
     *
     * @return Response
     * @throws \Symfony\Component\Security\Acl\Exception\InvalidDomainObjectException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\Security\Acl\Exception\AclAlreadyExistsException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \ReflectionException
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \RuntimeException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    public function postAction(ParamFetcher $paramFetcher, Request $request, string $type): Response
    {
        if (!$this->isGranted('CREATE', $this)) {
            throw $this->createAccessDeniedException();
        }

        $sendMail = true;
        if ($request->headers->get('SendMail') && $request->headers->get('SendMail') == 'false') {
            $sendMail = false;
        }

        /** @var Agency $agency */
        if (($type === 'adherent' || $type === 'concierge')
            && !($agency = $this->get('todotoday.core.domain_context.api')->getAgency())
        ) {
            throw $this->createAccessDeniedException('You must specified agency in header to create new ' . $type);
        }

        $accountManager = $this->get('todotoday.account.services.account_manager');
        $account = $accountManager->createAccount($type);

        $options['type'] = $type;

        $formType = $accountManager->getFormType($account, 'api');
        $form = $this->createForm(
            $formType,
            $account,
            $options
        );

        $form->handleRequest($request);
        $data = $form;

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $accountManager->searchLinkAccount($account, $agency);
            $em->persist($account);
            $accountManager
                ->createRegistrationToken($account);
            if ($sendMail) {
                $accountManager->sendEmailRegistrationToken($account, $agency, $request->isSecure());
            }

            $dispatcher = $this->get('event_dispatcher');
            $event = new RegistrationSuccessEvent($account);
            $dispatcher->dispatch(RegistrationSuccessEvent::USER_CREATED, $event);

            $em->flush();

            $data = $account;
        }

        $view = $this->view($data);
        $groups = ['Default'];
        if ($fields = explode(',', $paramFetcher->get('fields'))) {
            $groups = array_merge($groups, $fields);
        }
        $view->getContext()->addGroups($groups);
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * Post request to send email for reset password
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Operations on accounts.",
     *     description="Post request to send email for reset password",
     *     requirements={
     *      {
     *          "name"="id",
     *          "dataType"="int",
     *          "requirements"="Account",
     *          "description"="Account id"
     *      }
     *     },
     *     views={"default","account"},
     *     section="Account"
     * )
     *
     * @FOSRest\Route("/reset/{id}/password", requirements={"id" = "\d"})
     * @param Account $account
     *
     * @return void
     * @throws \LogicException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \RuntimeException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Symfony\Component\DependencyInjection\Exception\InvalidArgumentException
     * @throws \InvalidArgumentException
     */
    public function postResetPasswordAction(Account $account): void
    {
        $this->resetPassword($account);
    }

    /**
     * Post request to send email for reset password
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Operations on accounts.",
     *     description="Post request to send email for reset password",
     *     requirements={
     *      {
     *          "name"="email",
     *          "dataType"="string",
     *          "requirements"="Account",
     *          "description"="Account id"
     *      }
     *     },
     *     views={"default","account"},
     *     section="Account"
     * )
     *
     * @FOSRest\Route("/reset/{email}/password/email")
     * @param string $email
     *
     * @return void
     * @throws \LogicException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \RuntimeException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Symfony\Component\DependencyInjection\Exception\InvalidArgumentException
     * @throws AccountNotFoundException
     * @throws \InvalidArgumentException
     */
    public function postResetPasswordEmailAction(string $email): void
    {
        /** @var Account $account */
        if (!$account = $this->get('fos_user.user_manager')->findUserByUsernameOrEmail($email)) {
            throw new AccountNotFoundException();
        }

        $this->resetPassword($account);
    }

    /**
     * Do updatePaymentMethodStatusAction
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Resource description to patch entity",
     *     description="Operation on adherent",
     *     input={
     *           "class"="Todotoday\AccountBundle\Form\Api\AccountPaymentMethodStatusType"
     *     },
     *     views={"account"},
     *     section="Account"
     * )
     *
     * @FOSRest\Route("/{customerAccount}/payment_method_status")
     *
     * @param Request $request
     * @param string  $customerAccount
     *
     * @return Response
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     */
    public function putPaymentMethodStatusAction(Request $request, string $customerAccount): Response
    {
        if (!$this->isGranted('ROLE_MICROSOFT')) {
            throw $this->createAccessDeniedException();
        }
        $em = $this->getDoctrine()->getManager();
        /** @var AdherentRepository $repo */
        $repo = $em->getRepository(Adherent::class);

        /** @var Adherent $account */
        if (!$account = $repo->findOneBy(array('customerAccount' => $customerAccount))) {
            throw $this->createNotFoundException('Customer account not registered yet');
        }

        $data = $form = $this->createForm(
            AccountPaymentMethodStatusType::class,
            $account,
            array(
                'csrf_protection' => false,
                'method' => 'PUT',
            )
        );

        $form->submit($request->request->all());

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($account);
            $em->flush();
            $data = $account;
        }

        $view = $this->view($data);
        $view->getContext()->addGroups(array('Default', 'payment'));
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * Do resetPassword
     *
     * @param Account $account
     *
     * @return void
     * @throws \LogicException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \RuntimeException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Symfony\Component\DependencyInjection\Exception\InvalidArgumentException
     * @throws \InvalidArgumentException
     */
    public function resetPassword(Account $account): void
    {
        if (!$this->isGranted('ROLE_TODOTODAY_RESET_PWD')) {
            throw $this->createAccessDeniedException();
        }
        /** @var Agency $agency */
        $agency = $account->getLinkAgencies()->first()->getAgency();
        $baseUri = $this->getParameter('scheme') . '://' . $agency->getSlug() . '.' . $this->getParameter('domain');
        $this->get('request_stack')->push(Request::create($baseUri));

        $ttl = $this->container->getParameter('fos_user.resetting.retry_ttl');

        if ($account->isPasswordRequestNonExpired($ttl)) {
            throw new AccessDeniedException('Password reset already sent. Please check your mail box.');
        }
        if (null === $account->getConfirmationToken()) {
            /** @var $tokenGenerator TokenGeneratorInterface */
            $tokenGenerator = $this->get('fos_user.util.token_generator');
            $account->setConfirmationToken($tokenGenerator->generateToken());
        }

        if ($account instanceof Adherent && (!$agency->isAllowAdherent() || !$agency->isAllowRegistration())) {
            return;
        }
        $this->get('todotoday.account.services.account_manager')->sendResettingEmailMessage($account);
        $account->setPasswordRequestedAt(new \DateTime());
        $this->get('fos_user.user_manager')->updateUser($account);
    }

    /**
     * Get accounts suggestions with limit 10 by default filtered by search parameter on full name
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Operations on accounts.",
     *     description="Get accounts suggestions with limit 10 by default filtered by search parameter on full name",
     *     output={
     *           "class"="array<Todotoday\AccountBundle\Entity\Account> as accounts",
     *           "groups"={"Default", "account","agency"}
     *     },
     *     views={"default","account"},
     *     section="Account"
     * )
     *
     * @FOSRest\Route("/suggestions/{search}")
     *
     * @FOSRest\QueryParam(
     *     name="fields",
     *     requirements="((account|agency),?)*",
     *     default="",
     *     description="Fields you need"
     * )
     *
     * @param ParamFetcher $paramFetcher
     * @param string       $search
     *
     * @return Response|null
     * @throws \Symfony\Component\Security\Acl\Exception\InvalidDomainObjectException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\Security\Acl\Exception\AclNotFoundException
     * @throws \Symfony\Component\Security\Acl\Exception\NoAceFoundException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    public function getSuggestionAction(ParamFetcher $paramFetcher, string $search): Response
    {
        /** @var Agency $agency */
        if (!($agency = $this->get('todotoday.core.domain_context.api')->getAgency())
        ) {
            throw $this->createAccessDeniedException('You must specified agency in header');
        }

        if (!$search) {
            return null;
        }

        $search = urldecode($search);

        $accounts = $this->getDoctrine()
                        ->getManager()
                        ->getRepository(Account::class)
                        ->findSuggestions($search, $agency, 10);
        $view = $this->view($accounts);
        $groups = ['Default'];
        if ($fields = explode(',', $paramFetcher->get('fields'))) {
            $groups = array_merge($groups, $fields);
        }
        $view->getContext()->addGroups($groups);
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }
}
