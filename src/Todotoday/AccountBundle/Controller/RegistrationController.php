<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 12/01/2017
 * Time: 11:44
 */

namespace Todotoday\AccountBundle\Controller;

use Actiane\PaymentBundle\Enum\PaymentTypeEnum;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\FOSUserEvents;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as SFConf;
use Stripe\Error\Base;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Enum\AdherentTypeEnum;
use Todotoday\AccountBundle\Enum\PaymentStatusEnum;
use Todotoday\AccountBundle\Event\RegistrationSuccessEvent;
use Todotoday\AccountBundle\Form\AccountPasswordType;
use Todotoday\AccountBundle\Form\AccountPersonalInfoType;
use Todotoday\AccountBundle\Form\AccountCgvType;
use Todotoday\AccountBundle\Repository\AccountRepository;
use Todotoday\AccountBundle\Services\AdherentManager;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\PaymentBundle\Event\ChangeAccountRolesEvent;
use Exception;

/**
 * Class RegistrationController
 *
 * @SuppressWarnings(PHPMD)
 * @SFConf\Route("/registration")
 * @SFConf\Route("/enregistrement")
 *
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Controller
 */
class RegistrationController extends Controller
{
    public static $groupToRole = array(
        AdherentTypeEnum::ADH => 'ROLE_ADHERENT',
        AdherentTypeEnum::NON_ADH => 'ROLE_NOT_ADHERENT',
        AdherentTypeEnum::ADH_CO => 'ROLE_CO_ADHERENT',
    );

    protected static $groupReRouting = array(
        AdherentTypeEnum::NON_ADH => 'todotoday_account_registration_mandatorynotadherentworkflow',
    );

    protected static $groupReRoutingNonAdh = array(
        AdherentTypeEnum::ADH => 'todotoday_account_registration_mandatoryworkflow',
    );

    /**
     * Do checkEmail
     * @SFConf\Route("/check-email", defaults={"token"=null})
     * @SFConf\Template()
     *
     * @return array|Response
     */
    public function checkEmailAction()
    {
        return array();
    }

    /**
     * @SFConf\Route("/confirmation/{token}")
     * @SFConf\Template()
     *
     * @param Request     $request
     * @param null|string $token
     *
     * @return array|Response
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Symfony\Component\Form\Exception\UnexpectedTypeException
     * @throws \Symfony\Component\Form\Exception\LogicException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    public function confirmationAction(Request $request, string $token)
    {
        $entityManager = $this->getDoctrine()->getManager();
        /** @var AccountRepository $repo */
        $repo = $entityManager->getRepository(Account::class);
        /** @var Account $account */
        $account = $repo->findOneBy(array('registrationToken' => $token));

        if (!$account) {
            throw $this->createNotFoundException('This token can\'t be found or is expired');
        }

        if ($account instanceof Adherent && $account->getCustomerGroup() == AdherentTypeEnum::ADH_CO) {
            return $this->redirectToRoute('todotoday_account_registration_confirmationcoadherentworkflow', [
                'token' => $token,
            ]);
        }

        $form = $this->createForm(AccountPasswordType::class, $account)
            ->add(
                'submit',
                SubmitType::class,
                array(
                    'label' => 'next',
                    'translation_domain' => 'todotoday',
                )
            );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userManager = $this->get('fos_user.user_manager');
            $userManager->updateUser($account);
            $dispatcher = $this->get('event_dispatcher');
            $account->setRegistrationToken(null)->setEnabled(true);
            $event = new FilterUserResponseEvent($account, $request, $this->redirectToRoute('index_cms_concierge'));
            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_CONFIRMED, $event);
            $event = new ChangeAccountRolesEvent($account);
            $dispatcher->dispatch($event::NAME, $event);

            return $this->redirectToRoute('todotoday_account_registration_mandatoryworkflow');
        }

        return array(
            'form' => $form->createView(),
        );
    }

    /**
     * @SFConf\Route("/confirmation-coadherent-workflow/{token}")
     * @SFConf\Template()
     *
     * @param Request     $request
     * @param null|string $token
     *
     * @return array|Response
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Symfony\Component\Form\Exception\UnexpectedTypeException
     * @throws \Symfony\Component\Form\Exception\LogicException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    public function confirmationCoadherentWorkflowAction(Request $request, string $token)
    {
        $entityManager = $this->getDoctrine()->getManager();
        /** @var AccountRepository $repo */
        $repo = $entityManager->getRepository(Account::class);
        /** @var Account $account */
        $account = $repo->findOneBy(array('registrationToken' => $token));

        if (!$account) {
            throw $this->createNotFoundException('This token can\'t be found or is expired');
        }

        if ($account instanceof Adherent && $account->getCustomerGroup() != AdherentTypeEnum::ADH_CO) {
            return $this->redirectToRoute('todotoday_account_registration_confirmation', [
                'token' => $token,
            ]);
        }

        $flow = $this->get('todotoday.account.form.account_confirmation_coadherent_flow');

        /** @var AdherentManager $adherentManager */
        $adherentManager = $this->get('todotoday.account.services.adherent_manager');

        $agency = $this->get('todotoday.core.domain_context')->getCurrentContext()->getAgency();

        $flow->bind($account);

        $form = $flow->createForm();
        if ($flow->isValid($form)) {
            $flow->saveCurrentStepData($form);
            try {
                if ($flow->nextStep()) {
                    $form = $flow->createForm();
                } else {
                    $userManager = $this->get('fos_user.user_manager');
                    $userManager->updateUser($account);
                    $dispatcher = $this->get('event_dispatcher');
                    $account->setRegistrationToken(null)->setEnabled(true);
                    $event = new FilterUserResponseEvent(
                        $account,
                        $request,
                        $this->redirectToRoute('index_cms_concierge')
                    );
                    $dispatcher->dispatch(FOSUserEvents::REGISTRATION_CONFIRMED, $event);
                    $event = new ChangeAccountRolesEvent($account);
                    $dispatcher->dispatch($event::NAME, $event);

                    $roles = array(self::$groupToRole[AdherentTypeEnum::ADH_CO]);
                    $account->setRoles($roles);

                    $adherentManager->updateMicrosoftAccount($account);
                    $account->setPaymentTypeSelected(null);//to prevent AccountSubscriber->trackPaymentTypeSelected
                    $entityManager->persist($account);
                    $entityManager->flush();

                    //todo: twig où on ne peut pas aller sur le /profile + changer texte de succès
                    return $this->redirectToRoute('todotoday_account_registration_mandatoryworkflowconfirmation');
                }
            } catch (\Exception $e) {
                $flow->invalidateStepData($flow->getCurrentStepNumber());
            }
        }

        return array(
            'form' => $form->createView(),
            'flow' => $flow,
            'cgv' => $agency->getCgv() ? $agency->getCgv()->getContent() : '',
            'showAlert' => false,
        );
    }

    /**
     * Do mandatoryNotAdherentWorkflowAction
     *
     * @SFConf\Route("/mandatory-not-adherent-workflow")
     * @SFConf\Template()
     *
     * @param Request $request
     *
     * @return array|Response
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Symfony\Component\Form\Exception\UnexpectedTypeException
     * @throws \Symfony\Component\Form\Exception\LogicException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \InvalidArgumentException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \LogicException
     */
    public function mandatoryNotAdherentWorkflowAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $am = $this->get('todotoday.account.services.adherent_manager');

        $options = array(
            'contactPrincipals' => true,
            'contactChildren' => false,
            'contactAssistants' => false,
            'bankAccount' => false,
        );
        /** @var Adherent $account */
        if (!($account = $am->getUser(null, $options)) instanceof Adherent) {
            return $this->redirectToRoute('index_cms_concierge');
        }

        foreach (static::$groupReRoutingNonAdh as $group => $route) {
            if ($account->getCustomerGroup()->get() === $group) {
                return $this->redirectToRoute($route);
            }
        }

        $form = $this
            ->createForm(
                AccountPersonalInfoType::class,
                $account,
                array(
                    'not_adherent' => true,
                    'init_country_with_ip' => true,
                    'is_registration' => true,
                )
            )
            ->add(
                'submit',
                SubmitType::class,
                array(
                    'translation_domain' => 'todotoday',
                    'label' => 'valid',
                )
            );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var Agency $agency */
            $agency = $account->getLinkAgencies()->first()->getAgency();

            if ($agency->getDataAreaId() === 'EUS') {
                $account->setPaymentTypeSelected(PaymentTypeEnum::getNewInstance(PaymentTypeEnum::PAYPAL));
            } elseif ($agency->getDataAreaId() === 'ECH') {
                $account->setPaymentTypeSelected(PaymentTypeEnum::getNewInstance(PaymentTypeEnum::POSTFINANCE));
            } else {
                $account->setPaymentTypeSelected(PaymentTypeEnum::getNewInstance(PaymentTypeEnum::STRIPE_DIRECT));
            }

            $am = $this->get('todotoday.account.services.adherent_manager');
            $account->setPaymentMethodStatus(PaymentStatusEnum::getNewInstance(PaymentStatusEnum::VALID));
            $am->updateMicrosoftAccount($account, $options);
            $em->flush();

            $language = $account->getLanguage();
            $request->setLocale($language);
            $request->getSession()->set('_locale', $language);

            return $this->redirectToRoute('todotoday_account_registration_mandatoryworkflowconfirmation');
        }

        return array(
            'form' => $form,
            'showAlert' => false,
        );
    }

    /**
     * @SFConf\Route("/mandatory-workflow")
     * @SFConf\Template()
     *
     * @param Request $request
     *
     * @return array|Response
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \LogicException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \InvalidArgumentException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \RuntimeException
     * @throws \UnexpectedValueException
     */
    public function mandatoryWorkflowAction(Request $request)
    {
        if (!$this->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        if (!$this->getUser() instanceof Adherent
            || $this->isGranted('ROLE_ADHERENT')
            || $this->isGranted('ROLE_NOT_ADHERENT')
        ) {
            return $this->redirectToRoute('index_cms_concierge');
        }
        /** @var Adherent $account */
        $account = $this->getUser();

        foreach (static::$groupReRouting as $group => $route) {
            if ($account->getCustomerGroup()->get() === $group) {
                return $this->redirectToRoute($route);
            }
        }

        /** @var AdherentManager $adherentManager */
        $adherentManager = $this->get('todotoday.account.services.adherent_manager');

        $flow = $this->get('todotoday.account.form.account_mandatory_flow');

        /** @var Adherent $account */
        $account = $adherentManager->getUser(null, $flow->getFieldStepNeeded());
        $agency = $this->get('todotoday.core.domain_context')->getCurrentContext()->getAgency();

        $flow->bind($account);

        $form = $flow->createForm();
        if ($flow->isValid($form)) {
            $flow->saveCurrentStepData($form);
            $em = $this->getDoctrine()->getManager();
            try {
                $adherentManager->updateMicrosoftAccount($account, $flow->getFieldStepNeeded());
                $em->persist($account);
                $em->flush();

                if ($flow->nextStep()) {
                    $form = $flow->createForm();
                } else {
                    $adherentManager->validatePaymentInformation($account);
                    $adherentManager->updateMicrosoftAccount($account, $flow->getFieldStepNeeded());
                    $flow->reset();

                    $language = $account->getLanguage();
                    $request->setLocale($language);
                    $request->getSession()->set('_locale', $language);

                    return $this->redirectToRoute('todotoday_account_registration_mandatoryworkflowconfirmation');
                }
            } catch (Base $exceptionStripe) {
                $flow->invalidateStepData($flow->getCurrentStepNumber());
                $message = method_exists($exceptionStripe, 'getStripeCode')
                    ? 'stripe_' . $exceptionStripe->getStripeCode()
                    : 'error.title';
                $this->addFlash('stripe_error', $message);
            }
        }

        return array(
            'form' => $form->createView(),
            'flow' => $flow,
            'cgv' => $agency->getCgv() ? $agency->getCgv()->getContent() : '',
            'showAlert' => false,
        );
    }

    /**
     * @SFConf\Route("/mandatory-workflow/confirmation")
     * @SFConf\Template()
     *
     * @return array
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \LogicException
     */
    public function mandatoryWorkflowConfirmationAction(): array
    {
        /** @var Adherent $account */
        if (($account = $this->getUser()) instanceof Adherent) {
            $tokenStorage = $this->get('security.token_storage');
            /** @var UsernamePasswordToken $token */
            $token = $tokenStorage->getToken();
            $account->addRole(static::$groupToRole[strtoupper($account->getCustomerGroup()->get())]);
            $this->get('fos_user.user_manager')->updateUser($account);
            $token->setUser($account);
            $newToken = new UsernamePasswordToken(
                $account,
                null,
                $token->getProviderKey(),
                $account->getRoles()
            );
            $dispatcher = $this->get('event_dispatcher');
            $event = new RegistrationSuccessEvent($account);
            $dispatcher->dispatch(RegistrationSuccessEvent::USER_REGISTRATION_SUCCESS, $event);
            $tokenStorage->setToken($newToken);
        }

        return array();
    }

    /**
     * @SFConf\Route("/")
     * @SFConf\Template()
     *
     * @param Request $request
     *
     * @return array|Response
     * @throws \ReflectionException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \RuntimeException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Symfony\Component\Form\Exception\UnexpectedTypeException
     * @throws \Symfony\Component\Form\Exception\LogicException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    public function newAction(Request $request)
    {
        if ($this->get('session')->has('samlUserdata')) {
            return $this->redirectToRoute('todotoday_authsso_authsso_registration');
        }

        if (!$agency = $this->get('todotoday.core.domain_context')->getCurrentContext()->getAgency()) {
            throw $this->createNotFoundException();
        }

        if (!$agency->isAllowAdherent() || !$agency->isAllowRegistration()) {
            throw $this->createNotFoundException();
        }

        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute('index_cms_concierge');
        }
        $accountManager = $this->get('todotoday.account.services.account_manager');
        $account = new Adherent();

        $flow = $this->get('todotoday.account.form.account_registration_flow');
        $flow->bind($account);

        $form = $flow->createForm();
        if ($flow->isValid($form)) {
            $flow->saveCurrentStepData($form);
            try {
                if ($flow->nextStep()) {
                    $form = $flow->createForm();
                } else {
                    $accountManager->searchLinkAccount(
                        $account,
                        $agency
                    );
                    $accountManager
                        ->createRegistrationToken($account)
                        ->sendEmailRegistrationToken($account, $agency, $request->isSecure());
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($account);
                    $dispatcher = $this->get('event_dispatcher');
                    $event = new RegistrationSuccessEvent($account);
                    $dispatcher->dispatch(RegistrationSuccessEvent::USER_CREATED, $event);
                    $em->flush();

                    return $this->redirectToRoute('todotoday_account_registration_checkemail');
                }
            } catch (\Exception $e) {
                $flow->invalidateStepData($flow->getCurrentStepNumber());
            }
        }

        return array(
            'form' => $form->createView(),
            'flow' => $flow,
            'cgv' => $agency->getCgv() ? $agency->getCgv()->getContent() : '',
            'showAlert' => false,
        );
    }
}
