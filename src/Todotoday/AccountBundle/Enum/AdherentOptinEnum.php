<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 05/04/2017
 * Time: 11:22
 */

namespace Todotoday\AccountBundle\Enum;

use Actiane\ToolsBundle\Enum\AbstractEnum;

/**
 * Class AdherentOptinEnum
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Enum
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class AdherentOptinEnum extends AbstractEnum
{
    const SUBSCRIBED = 'subscribed';
    const UNSUBSCRIBED = 'unsubscribed';
    const CLEANED = 'cleaned';
    const PENDING = 'pending';
    const ARCHIVED = 'archived';

    /**
     * @return array
     */
    public static function getFormChoices(): array
    {
        return [
            'profile.optin_' . self::UNSUBSCRIBED => self::UNSUBSCRIBED,
            'profile.optin_' . self::SUBSCRIBED => self::SUBSCRIBED,
        ];
    }
}
