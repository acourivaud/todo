<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 08/06/17
 * Time: 11:06
 */

namespace Todotoday\AccountBundle\Enum;

use Actiane\ToolsBundle\Enum\AbstractEnum;

/**
 * Class PersonGenderEnum
 * @package Todotoday\AccountBundle\Enum
 */
class PersonGenderEnum extends AbstractEnum
{
    const MALE = 'Male';
    const FEMALE = 'Female';
    const NON_SPECIFIC = 'NonSpecific';
    const UNKNOWN = 'Unknown';
//    const __DEFAULT = self::NON_SPECIFIC;

    /**
     * @return array
     */
    public static function getFormChoices(): array
    {
        return array(
            'form.account.gender.choose' => self::UNKNOWN,
            'form.account.gender.male' => self::MALE,
            'form.account.gender.female' => self::FEMALE,
            'form.account.gender.non-specific' => self::NON_SPECIFIC,
        );
    }
}
