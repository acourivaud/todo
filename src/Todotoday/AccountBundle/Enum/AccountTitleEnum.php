<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 05/04/2017
 * Time: 11:22
 */

namespace Todotoday\AccountBundle\Enum;

use Actiane\ToolsBundle\Enum\AbstractEnum;

/**
 * Class AccountTitleEnum
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Enum
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class AccountTitleEnum extends AbstractEnum
{
    const M = 'Mr.';
    const MLLE = 'Miss';
    const MME = 'Ms.';

    /**
     * @return array
     */
    public static function getFormChoices(): array
    {
        return array(
            'm' => self::M,
            'mme' => self::MME,
        );
    }
}
