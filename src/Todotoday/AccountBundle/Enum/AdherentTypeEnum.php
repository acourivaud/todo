<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 01/06/17
 * Time: 15:32
 */

namespace Todotoday\AccountBundle\Enum;

use Actiane\ToolsBundle\Enum\AbstractEnum;

/**
 * Class AdherentTypeEnum
 * @package Todotoday\AccountBundle\Enum
 */
class AdherentTypeEnum extends AbstractEnum
{
    const ADH = 'ADH';
    const NON_ADH = 'NON_ADH';
    const ADH_CO = 'ADH_CO';

    /**
     * Set customerGroup integer for CRM
     *
     * @param string $type
     *
     * @return int|null
     */
    public static function getApsCustomerGroupForCRM(string $type): ?int
    {
        if ($type === self::ADH) {
            return 100000007;
        }

        if ($type === self::NON_ADH) {
            return 100000006;
        }

        if ($type === self::ADH_CO) {
            return 100000009;
        }

        return null;
    }

    /**
     * Filter co-adherent
     *
     * @return array
     */
    public static function getChoicesFilterCoAdh(): array
    {
        $choices = array();
        foreach (static::toArray() as $value) {
            if ($value !== self::ADH_CO) {
                $choices[$value] = static::getNewInstance($value);
            }
        }

        return $choices;
    }
}
