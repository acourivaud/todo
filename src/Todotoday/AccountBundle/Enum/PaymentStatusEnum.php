<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 09/06/2017
 * Time: 15:28
 */

namespace Todotoday\AccountBundle\Enum;

use Actiane\ToolsBundle\Enum\AbstractEnum;

/**
 * Class PaymentStatusEnum
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Enum
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class PaymentStatusEnum extends AbstractEnum
{
    public const __DEFAULT = self::NO_PAYMENT_METHOD;
    public const NO_PAYMENT_METHOD = 'no_payment_method';
    public const VALID = 'valid';
    public const SEPA_WAITING_FOR_SIGNATURE = 'sepa_waiting_for_signature';
    public const CREDIT_CARD_EXPIRED = 'credit_card_expired';
    public const PAYMENT_INCIDENT = 'payment_incident';

    /**
     * @return array
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     */
    public static function getUpdatedStatusChoice(): array
    {
        $arr = self::getChoices();
        unset($arr[self::NO_PAYMENT_METHOD], $arr[self::SEPA_WAITING_FOR_SIGNATURE]);

        return $arr;
    }
}
