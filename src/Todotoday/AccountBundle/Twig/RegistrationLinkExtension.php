<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 03/08/17
 * Time: 17:01
 */

namespace Todotoday\AccountBundle\Twig;

use Symfony\Component\Routing\Router;

/**
 * Class RegistrationLinkExtension
 *
 * @package Todotoday\AccountBundle\Twig
 */
class RegistrationLinkExtension extends \Twig_Extension
{
    public const ROUTES = array(
        'reset' => 'fos_user_resetting_reset',
        'registration' => 'todotoday_account_registration_confirmation',
    );

    /**
     * @var Router
     */
    private $router;

    /**
     * @var string
     */
    private $domain;

    /**
     * @var string
     */
    private $scheme;

    /**
     * RegistrationLinkExtension constructor.
     *
     * @param Router $router
     * @param string $domain
     * @param string $scheme
     */
    public function __construct(Router $router, string $domain, string $scheme)
    {
        $this->router = $router;
        $this->domain = $domain;
        $this->scheme = $scheme;
    }

    /**
     * @return array
     */
    public function getFilters(): array
    {
        return array(
            new \Twig_SimpleFilter(
                'activation_link',
                array($this, 'getActivationLink')
            ),
        );
    }

    /**
     *
     * @param string      $token
     * @param string|null $agencySlug
     * @param string|null $type
     *
     * @return null|string
     *
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @internal param string $slug
     */
    public function getActivationLink(?string $token = null, ?string $agencySlug, ?string $type = 'reset'): ?string
    {
        if (!$token || !array_key_exists($type, self::ROUTES)) {
            return null;
        }

        $agencySlug = $agencySlug ?: 'www';

        $route = $this->router->generate(self::ROUTES[$type], array('token' => $token));

        return $this->scheme . '://' . $agencySlug . '.' . $this->domain . $route;
    }
}
