<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 16/03/17
 * Time: 09:45
 */

namespace Todotoday\AccountBundle\Twig;

use Doctrine\ORM\EntityManager;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Repository\AdherentRepository;

/**
 * Class AccountTwigExtension
 *
 * @package Todotoday\AccountBundle\Twig
 */
class AccountTwigExtension extends \Twig_Extension
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * JwtExtension constructor.
     *
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return array
     */
    public function getFunctions(): array
    {
        return array(
            new \Twig_SimpleFunction('getFullNameWithCustomerAccount', array($this, 'getFullNameWithCustomerAccount')),
        );
    }

    /**
     * @param null|string $customerAccountNumber
     *
     * @return null|string
     */
    public function getFullNameWithCustomerAccount(?string $customerAccountNumber): string
    {
        /** @var AdherentRepository $adherentRepository */
        $adherentRepository = $this->entityManager->getRepository(Adherent::class);
        /** @var Adherent $adherent */
        $adherent = $adherentRepository->findOneBy(array('customerAccount' => $customerAccountNumber));

        if ($adherent) {
            return $adherent->getFirstName() . ' ' . $adherent->getLastName();
        }

        return '';
    }
}
