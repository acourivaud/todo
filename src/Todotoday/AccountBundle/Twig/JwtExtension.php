<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 16/03/17
 * Time: 09:45
 */

namespace Todotoday\AccountBundle\Twig;

use Gesdinet\JWTRefreshTokenBundle\Model\RefreshTokenManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Http\Authentication\AuthenticationSuccessHandler;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CoreBundle\Enum\AgencyModuleEnum;
use Todotoday\CoreBundle\Services\DomainContextManager;

/**
 * Class JwtExtension
 *
 * @package Todotoday\AccountBundle\Twig
 */
class JwtExtension extends \Twig_Extension
{
    /**
     * @var JWTTokenManagerInterface
     */
    private $jwtTokenManager;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var RefreshTokenManagerInterface
     */
    private $refreshTokenManager;

    /**
     * @var AuthenticationSuccessHandler
     */
    private $jwtSuccessHandler;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var DomainContextManager
     */
    private $domainContext;

    /**
     * @var AuthorizationCheckerInterface
     */
    private $checker;

    /**
     * JwtExtension constructor.
     *
     * @param JWTTokenManagerInterface      $jwtTokenManager
     * @param AuthenticationSuccessHandler  $jwtSuccessHandler
     * @param TokenStorageInterface         $tokenStorage
     * @param RefreshTokenManagerInterface  $refreshTokenManager
     * @param RequestStack                  $requestStack
     * @param DomainContextManager          $domainContext
     * @param AuthorizationCheckerInterface $checker
     */
    public function __construct(
        JWTTokenManagerInterface $jwtTokenManager,
        AuthenticationSuccessHandler $jwtSuccessHandler,
        TokenStorageInterface $tokenStorage,
        RefreshTokenManagerInterface $refreshTokenManager,
        RequestStack $requestStack,
        DomainContextManager $domainContext,
        AuthorizationCheckerInterface $checker
    ) {
        $this->jwtTokenManager = $jwtTokenManager;
        $this->jwtSuccessHandler = $jwtSuccessHandler;
        $this->tokenStorage = $tokenStorage;
        $this->refreshTokenManager = $refreshTokenManager;
        $this->requestStack = $requestStack;
        $this->domainContext = $domainContext;
        $this->checker = $checker;
    }

    /**
     * @return array
     */
    public function getFunctions(): array
    {
        return array(
            new \Twig_SimpleFunction('getJwt', array($this, 'getJwt')),
        );
    }

    /**
     * @param bool $onlyJwt
     *
     * @return null|string
     * @throws \ReflectionException
     * @throws \UnexpectedValueException
     */
    public function getJwt(bool $onlyJwt = false): ?string
    {
        if (($token = $this->tokenStorage->getToken()) && ($user = $token->getUser()) && !is_string($user)) {
            $agency = $this->domainContext->getCurrentContext()->getAgency();
            $currency = $agency ? $agency->getCurrency() : '';
            $fgsModuleEnable = $agency ? $agency->hasModule(AgencyModuleEnum::FGS) : false;

            if ($onlyJwt) {
                return $this->createJwt($user);
            }

            return json_encode(
                array(
                    'user' => $user->getUsername(),
                    'user_type' => (new \ReflectionClass($user))->getShortName(),
                    'is_co_adherent' => ($user instanceof Adherent) && $user->isCoAdherent(),
                    'payment_method_status' => $user->getPaymentMethodStatusString(),
                    'payment_type_selected' => $user->getPaymentTypeSelectedString(),
                    'jwt' => $this->createJwt($user),
                    'refresh_token' => $this->refreshTokenManager->getLastFromUsername($user->getUsername())
                        ->getRefreshToken(),
                    'locale' => $this->requestStack->getCurrentRequest()->getLocale(),
                    'currency' => $currency,
                    'grantedAddCart' => $this->checker->isGranted('ROLE_TODOTODAY_CART_API_CARTPRODUCT_CREATE'),
                    'fgs_module_enable' => $fgsModuleEnable,
                )
            );
        } elseif ($agency = $this->domainContext->getCurrentContext()->getAgency()) {
            return json_encode(
                array(
                    'locale' => $agency->getDefaultLanguage(),
                    'currency' => $agency->getCurrency(),
                )
            );
        } else {
            return null;
        }
    }

    /**
     * @param UserInterface $user
     *
     * @return string
     */
    private function createJwt(UserInterface $user): string
    {
        $token = $this->jwtTokenManager->create($user);
        if (!$this->refreshTokenManager->getLastFromUsername($user->getUsername())) {
            $this->jwtSuccessHandler->handleAuthenticationSuccess($user);
        }

        return $token;
    }
}
