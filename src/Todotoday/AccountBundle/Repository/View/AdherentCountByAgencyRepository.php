<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 08/10/17
 * Time: 18:44
 */

namespace Todotoday\AccountBundle\Repository\View;

use Doctrine\ORM\EntityRepository;

/**
 * Class AdherentCountByAgencyRepository
 * @package Todotoday\AccountBundle\Repository\View
 */
class AdherentCountByAgencyRepository extends EntityRepository
{
}
