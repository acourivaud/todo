<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 10/01/18
 * Time: 11:51
 *
 * @category   tdtd
 *
 * @package    Todotoday\AccountBundle\Repository
 *
 * @subpackage Todotoday\AccountBundle\Repository
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\AccountBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Todotoday\AccountBundle\Entity\Account;

/**
 * Class AbstractAccountRepository
 */
abstract class AbstractAccountRepository extends EntityRepository
{
    /**
     * @param array $criteria
     *
     * @return array|Account[]
     */
    public function findByUniqueEmail(array $criteria): array
    {
        return $this->_em->getRepository(Account::class)->findBy($criteria);
    }
}
