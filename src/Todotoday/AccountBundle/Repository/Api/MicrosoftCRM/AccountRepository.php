<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 20/04/17
 * Time: 22:19
 */

namespace Todotoday\AccountBundle\Repository\Api\MicrosoftCRM;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiRepository;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Entity\Api\MicrosoftCRM\Account as AccountCRM;

/**
 * Class AccountRepository
 * @package Todotoday\AccountBundle\Repository\Api\MicrosoftCRM
 */
class AccountRepository extends AbstractApiRepository
{
    /**
     * Do findByDoctrineAccount
     *
     * @param Adherent $adherent
     *
     * @return AccountCRM
     * @throws \InvalidArgumentException
     */
    public function findByDoctrineAccount(Adherent $adherent): ?AccountCRM
    {
        $filter = 'accountnumber eq \'' . $adherent->getCustomerAccount() . '\'';
        $result = $this
            ->createQueryBuilder()
            ->addCustomQuery(
                '$filter',
                $filter
            )
            ->getRequest()
            ->execute();
        if (!$result) {
            return null;
        }

        return $result[0];
    }

    /**
     * @param Adherent $account
     *
     * @return AccountCRM
     * @throws \InvalidArgumentException
     */
    public function findOrCreateByAdherentNumber(Adherent $account): AccountCRM
    {
        $filter = 'accountnumber eq \'' . $account->getCustomerAccount() . '\'';
        $result = $this
            ->createQueryBuilder()
            ->addCustomQuery(
                '$filter',
                $filter
            )
            ->getRequest()
            ->execute();
        if (!$result) {
            return new AccountCRM();
        }

        return $result[0];
    }

    /**
     * Do findAccountIdByDoctrineAccount
     *
     * @param Adherent $adherent
     *
     * @return AccountCRM
     * @throws \InvalidArgumentException
     */
    public function findAccountIdByDoctrineAccount(Adherent $adherent): ?AccountCRM
    {
        $select = 'accountid';
        $filter = 'accountnumber eq \'' . $adherent->getCustomerAccount() . '\'';
        $result = $this
            ->createQueryBuilder()
            ->addCustomQuery(
                '$select',
                $select
            )
            ->addCustomQuery(
                '$filter',
                $filter
            )
            ->getRequest()
            ->execute();

        if (!$result) {
            return null;
        }

        return $result[0];
    }
}
