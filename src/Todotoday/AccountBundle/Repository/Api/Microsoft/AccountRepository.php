<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 16/02/2017
 * Time: 16:11
 */

namespace Todotoday\AccountBundle\Repository\Api\Microsoft;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiRepository;
use Actiane\ApiConnectorBundle\Lib\Request;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Entity\Api\Microsoft\Account;

/**
 * Class AccountRepository
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Repository\Api\Microsoft
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class AccountRepository extends AbstractApiRepository
{
    /**
     * Do findByDoctrineAccount
     *
     * @param Adherent $adherent
     *
     * @return null|Account
     * @throws \InvalidArgumentException
     */
    public function findByDoctrineAccount(Adherent $adherent): ?Account
    {
        $account = null;
        if ($adherent->getCustomerAccount()) {
            $dataAreaId = $adherent->getDataAreaId() ?? substr($adherent->getCustomerAccount(), 0, 3);
            if ($dataAreaId) {
                /** @var Account $account */
                $account = $this->find(
                    array(
                        'customerAccount' => $adherent->getCustomerAccount(),
                        'dataAreaId' => $dataAreaId,
                    )
                );
            }
        }

        return $account;
    }

    /**
     * Do findAddressCountryRegionIdByDoctrineAccount
     *
     * @param Adherent $adherent
     *
     * @return null|Account
     * @throws \InvalidArgumentException
     */
    public function findAddressCountryRegionIdByDoctrineAccount(Adherent $adherent): ?Account
    {
        $account = null;

        if ($adherent->getCustomerAccount() && $adherent->getDataAreaId()) {
            $select = 'AddressCountryRegionId';
            $filter = 'CustomerAccount eq \'' . $adherent->getCustomerAccount() . '\'';
            $filter .= ' and dataAreaId eq \'' . $adherent->getDataAreaId() . '\'';

            /** @var Account $account */
            $account = $this->createQueryBuilder()
                           ->addCustomQuery(
                               '$select',
                               $select
                           )
                           ->addCustomQuery(
                               '$filter',
                               $filter
                           )
                           ->getRequest()
                           ->execute()[0];
        }

        return $account;
    }

    /**
     * Do findByDoctrineAccount
     *
     * @param Adherent $adherent
     *
     * @return Account
     * @throws \InvalidArgumentException
     */
    public function findOrCreateByDoctrineAccount(Adherent $adherent): Account
    {
        if (!$account = $this->findByDoctrineAccount($adherent)) {
            $account = new Account();
        }

        return $account;
    }

    /**
     * @param string $email
     *
     * @return Account
     * @throws \InvalidArgumentException
     */
    public function findOrCreateByEmailAddress(string $email): Account
    {
        $result = $this->getRequestFindByEmail($email)->execute();
        if (!$result) {
            return new Account();
        }

        return $result[0];
    }

    /**
     * @param string $email
     *
     * @return Account[]
     * @throws \InvalidArgumentException
     */
    public function findByEmail(string $email): ?array
    {
        return $this->getRequestFindByEmail($email)->execute();
    }

    /**
     * @param string $epa
     *
     * @return Account[]|null
     * @throws \InvalidArgumentException
     */
    public function findByEpa(string $epa): ?array
    {
        return $this->getRequestFindByEpa($epa)->execute();
    }

    /**
     * @param string $epa
     *
     * @return Account
     * @throws \InvalidArgumentException
     */
    public function findOrCreateByEpa(string $epa): Account
    {
        $result = $this->getRequestFindByEpa($epa)->execute();
        if (!$result) {
            return new Account();
        }

        return $result[0];
    }

    /**
     * @param string $email
     *
     * @return Request
     * @throws \InvalidArgumentException
     */
    private function getRequestFindByEmail(string $email): Request
    {
        $filter = 'PrimaryContactEmail eq \'' . $email . '\'';

        return $this
            ->createQueryBuilder()
            ->addCustomQuery(
                '$filter',
                $filter
            )
            ->getRequest();
    }

    /**
     * @param string $epa
     *
     * @return Request
     * @throws \InvalidArgumentException
     */
    private function getRequestFindByEpa(string $epa): Request
    {
        $filter = 'CustomerAccount eq \'' . $epa . '\'';

        return $this
            ->createQueryBuilder()
            ->addCustomQuery(
                '$filter',
                $filter
            )
            ->getRequest();
    }
}
