<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 16/02/2017
 * Time: 16:11
 */

namespace Todotoday\AccountBundle\Repository\Api\Microsoft;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiRepository;
use Todotoday\AccountBundle\Entity\Api\Microsoft\VehicleModel;

/**
 * Class VehiclModelRepository
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Repository\Api\Microsoft
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class VehicleModelRepository extends AbstractApiRepository
{
    /**
     * Do findVehicleModelByAccount
     *
     * @return VehicleModel
     */
    public function findVehicleModelByAccount(): VehicleModel
    {
        /** @var VehicleModel $vehicleModel */
        $vehicleModel = $this->find(
            array(
                '',
            )
        );

        return $vehicleModel;
    }
}
