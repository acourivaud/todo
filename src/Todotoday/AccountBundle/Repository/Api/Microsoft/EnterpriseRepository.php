<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 09/06/17
 * Time: 15:19
 */

namespace Todotoday\AccountBundle\Repository\Api\Microsoft;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiRepository;
use Todotoday\AccountBundle\Entity\Api\Microsoft\Enterprise;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class EnterpriseRepository
 * @package Todotoday\AccountBundle\Repository\Api\Microsoft
 */
class EnterpriseRepository extends AbstractApiRepository
{
    /**
     * @param Agency $agency
     *
     * @return Enterprise[]|null
     * @throws \InvalidArgumentException
     */
    public function getEnterpriseByAgency(Agency $agency): ?array
    {
        $filter = 'IdActiane eq \'' . $agency->getSlug() . '\'';

        return $this
            ->createQueryBuilder()
            ->addCustomQuery(
                '$filter',
                $filter
            )
            ->addCustomQuery(
                '$orderby',
                'Description'
            )
            ->getRequest()
            ->execute();
    }
}
