<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 17/06/17
 * Time: 16:32
 */

namespace Todotoday\AccountBundle\Repository\Api\Microsoft;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiConnection;
use Actiane\ApiConnectorBundle\Abstracts\AbstractApiRepository;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Entity\Api\Microsoft\CustField;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class CustFieldRepository
 * @package Todotoday\AccountBundle\Repository\Api\Microsoft
 */
class CustFieldRepository extends AbstractApiRepository
{
    /**
     * @var string
     */
    private $local;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * {@inheritdoc}
     * @param string $local
     */
    public function __construct(
        $entityClass,
        AbstractApiConnection $apiConnection,
        string $local,
        RequestStack $requestStack
    ) {

        parent::__construct($entityClass, $apiConnection);
        $this->local = $local;
        $this->requestStack = $requestStack;
    }

    /**
     * Do findByAccount
     *
     * @param Adherent $adherent
     *
     * @return array|CustField[]
     * @throws \InvalidArgumentException
     */
    public function findByAccount(Adherent $adherent): ?array
    {
        $request = $this->requestStack->getCurrentRequest();
        $local = $request->getLocale();

        $filter = 'CustAccount eq \'' . $adherent->getCustomerAccount() . '\'
         and LanguageId eq \'' . $local . '\'';

        $result = $this->createQueryBuilder()
            ->addCustomQuery('$filter', $filter)
            ->getRequest()
            ->execute()
            ;

        if (!$result) {
            $filter = 'CustAccount eq \'' . $adherent->getCustomerAccount() . '\'
            and LanguageId eq \'' . 'fr' . '\'';

            $result = $this->createQueryBuilder()
                ->addCustomQuery('$filter', $filter)
                ->getRequest()
                ->execute()
                ;
        }

        return $result;
    }
}
