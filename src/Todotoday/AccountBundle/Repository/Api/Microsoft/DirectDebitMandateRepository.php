<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 04/04/17
 * Time: 16:05
 */

namespace Todotoday\AccountBundle\Repository\Api\Microsoft;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiRepository;

/**
 * Class DirectDebitMandateRepository
 * @package Todotoday\AccountBundle\Repository\Api\Microsoft
 */
class DirectDebitMandateRepository extends AbstractApiRepository
{
}
