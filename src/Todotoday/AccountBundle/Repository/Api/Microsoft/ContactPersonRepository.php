<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 16/02/2017
 * Time: 16:11
 */

namespace Todotoday\AccountBundle\Repository\Api\Microsoft;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiRepository;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Entity\Api\Microsoft\Account;
use Todotoday\AccountBundle\Entity\Api\Microsoft\ContactPerson;

/**
 * Class ContactPersonRepository
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Repository\Api\Microsoft
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class ContactPersonRepository extends AbstractApiRepository
{
    /**
     * Do findContactsPersonByAccountAndContactType
     *
     * @param Adherent $account
     *
     * @param int|null $limit
     * @param int|null $offset
     *
     * @return array|null|ContactPerson[]
     * @throws \InvalidArgumentException
     */
    public function findContactsPersonByAccount(Adherent $account, int $limit = null, int $offset = null): ?array
    {
        $filter = 'AssociatedPartyNumber eq \'' . $account->getPartyNumber() . '\'';

        return $this
            ->createQueryBuilder()
            ->addCustomQuery('$filter', trim($filter))
            ->setLimit($limit)
            ->setOffset($offset)
            ->getRequest()
            ->execute();
    }

    /**
     * Do findContactsPersonByAccountAndContactType
     *
     * @param Account $account
     * @param string  $type
     *
     * @return ContactPerson[]
     * @throws \InvalidArgumentException
     */
    public function findContactsPersonByAccountAndContactType(Account $account, string $type): ?array
    {
        if ($account->getPartyNumber()) {
            $filter = 'AssociatedPartyNumber eq \'' . $account->getPartyNumber() . '\'
        and ContactType eq Microsoft.Dynamics.DataEntities.ContactType\'' . $type . '\'';

            return $this
                ->createQueryBuilder()
                ->addCustomQuery('$filter', trim($filter))
                ->getRequest()
                ->execute();
        }

        return null;
    }

    /**
     * Do findContactsPersonByAccountAndContactType
     *
     * @param Adherent $account
     *
     * @param string   $id
     *
     * @return object|null|ContactPerson
     * @throws \InvalidArgumentException
     */
    public function findContactsPersonByAccountAndId(Adherent $account, string $id): ?ContactPerson
    {
        $filter = 'AssociatedPartyNumber eq \'' . $account->getPartyNumber() . '\'';
        $filter .= ' and ContactPersonId eq \'' . $id . '\'';

        return $this
                   ->createQueryBuilder()
                   ->addCustomQuery('$filter', trim($filter))
                   ->getRequest()
                   ->execute()[0];
    }

    /**
     * Do findContactsPersonByAccountAndContactType
     *
     * @param Adherent $account
     *
     * @param array    $ids
     *
     * @return array|null|ContactPerson[]
     * @throws \InvalidArgumentException
     */
    public function findContactsPersonByAccountAndIds(Adherent $account, array $ids): ?array
    {
        $filter = 'AssociatedPartyNumber eq \'' . $account->getPartyNumber() . '\'';
        $filterIds = null;

        foreach ($ids as $id) {
            if ($filterIds) {
                $filterIds .= ' or ';
            }
            $filterIds .= 'ContactPersonId eq \'' . $id . '\'';
        }

        $filter .= ' and (' . $filterIds . ')';

        return $this
            ->createQueryBuilder()
            ->addCustomQuery('$filter', trim($filter))
            ->getRequest()
            ->execute();
    }
}
