<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 16/02/2017
 * Time: 16:11
 */

namespace Todotoday\AccountBundle\Repository\Api\Microsoft;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiRepository;
use Todotoday\AccountBundle\Entity\Api\Microsoft\MandatoryField;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class MandatoryFieldRepository
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Repository\Api\Microsoft
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class MandatoryFieldRepository extends AbstractApiRepository
{
    /**
     * @param Agency $agency
     *
     * @return MandatoryField[]|null
     * @throws \InvalidArgumentException
     */
    public function findByAgency(Agency $agency): ?array
    {
        $filter = 'RetailChannelId eq \'' . $agency->getRetailChannelId() . '\'';

        return $this
            ->createQueryBuilder()
            ->addCustomQuery(
                '$filter',
                $filter
            )
            ->getRequest()
            ->execute();
    }
}
