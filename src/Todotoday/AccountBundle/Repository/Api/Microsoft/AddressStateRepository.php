<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 17/06/17
 * Time: 16:32
 */

namespace Todotoday\AccountBundle\Repository\Api\Microsoft;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiRepository;

/**
 * Class AddressStateRepository
 * @package Todotoday\AccountBundle\Repository\Api\Microsoft
 */
class AddressStateRepository extends AbstractApiRepository
{
}
