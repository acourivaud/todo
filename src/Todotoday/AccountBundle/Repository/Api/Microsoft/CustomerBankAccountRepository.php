<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 04/04/17
 * Time: 16:05
 */

namespace Todotoday\AccountBundle\Repository\Api\Microsoft;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiRepository;
use Todotoday\AccountBundle\Entity\Api\Microsoft\Account;
use Todotoday\AccountBundle\Entity\Api\Microsoft\CustomerBankAccount;

/**
 * Class CustomerBankAccountRepository
 *
 * @package Todotoday\AccountBundle\Repository\Api\Microsoft
 */
class CustomerBankAccountRepository extends AbstractApiRepository
{
    /**
     * Do findBanqueAccountByAccount
     *
     * @param Account $account
     *
     * @return CustomerBankAccount[]
     * @throws \InvalidArgumentException
     */
    public function findBankAccountsByAccount(Account $account): ?array
    {
        $filter = 'CustomerAccountNumber eq \'' . $account->getCustomerAccount() . '\'
        and dataAreaId eq \'' . $account->getDataAreaId() . '\'';

        return $this->createQueryBuilder()
            ->addCustomQuery('$filter', $filter)
            ->getRequest()
            ->execute()
            ;
    }
}
