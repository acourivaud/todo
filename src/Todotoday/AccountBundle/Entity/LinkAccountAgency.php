<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 25/01/2017
 * Time: 10:17
 */

namespace Todotoday\AccountBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class LinkAccountAgency
 *
 * @ORM\Entity(repositoryClass="Todotoday\AccountBundle\Repository\LinkAccountAgencyRepository")
 * @ORM\Table(name="link_account_Agency", schema="account",
 *      uniqueConstraints={
 *     @ORM\UniqueConstraint(name="unique_link_account_agency",columns={"account_id", "agency_id"})
 * })
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Entity
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 * @Serializer\ExclusionPolicy("all")
 */
class LinkAccountAgency
{
    /**
     * @var integer
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer", options={"unsigned"=true})
     */
    protected $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expired_at", type="datetime", nullable=true)
     * @Serializer\Expose()
     */
    protected $expiredAt;

    /**
     * @var array
     *
     * @ORM\Column(name="roles", type="simple_array")
     * @Serializer\Expose()
     */
    protected $roles;

    /**
     * @var Account
     *
     * @ORM\ManyToOne(targetEntity="Account", inversedBy="linkAgencies")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $account;

    /**
     * @var Agency
     *
     * @ORM\ManyToOne(targetEntity="Todotoday\CoreBundle\Entity\Agency", inversedBy="linkAccounts")
     * @Serializer\Expose()
     * @Serializer\Groups("account")
     */
    protected $agency;

    /**
     * LinkAccountAgency constructor.
     */
    public function __construct()
    {
        $this->roles = array();
    }

    /**
     * Do __toString
     *
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->getAgency();
    }

    /**
     * Add role
     *
     * @param string $role
     *
     * @return LinkAccountAgency
     */
    public function addRole(string $role): self
    {
        if (!in_array($role, $this->roles, true)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    /**
     * Get account
     *
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set account
     *
     * @param Account $account
     *
     * @return LinkAccountAgency
     */
    public function setAccount(Account $account = null): self
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get agency
     *
     * @return Agency
     */
    public function getAgency()
    {
        return $this->agency;
    }

    /**
     * Set agency
     *
     * @param Agency $agency
     *
     * @return LinkAccountAgency
     */
    public function setAgency(Agency $agency = null): self
    {
        $this->agency = $agency;

        return $this;
    }

    /**
     * Get expiredAt
     *
     * @return \DateTime
     */
    public function getExpiredAt()
    {
        return $this->expiredAt;
    }

    /**
     * Set expiredAt
     *
     * @param \DateTime $expiredAt
     *
     * @return LinkAccountAgency
     */
    public function setExpiredAt(\DateTime $expiredAt = null): self
    {
        $this->expiredAt = $expiredAt;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get roles
     *
     * @return array
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Set roles
     *
     * @param array $roles
     *
     * @return LinkAccountAgency
     */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * Do removeRole
     *
     * @param string $role
     *
     * @return LinkAccountAgency
     */
    public function removeRole(string $role): self
    {
        if ($key = array_search($role, $this->roles, true)) {
            unset($this->roles[$key]);
        }

        return $this;
    }
}
