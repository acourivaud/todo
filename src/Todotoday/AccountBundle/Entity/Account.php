<?php declare(strict_types=1);
/**
 * PHP version 7
 */

namespace Todotoday\AccountBundle\Entity;

use Actiane\PaymentBundle\Enum\PaymentTypeEnum;
use Actiane\ToolsBundle\Enum\AbstractEnum;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\GroupableInterface;
use FOS\UserBundle\Model\GroupInterface;
use FOS\UserBundle\Model\UserInterface;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Todotoday\AccountBundle\Constraints\EmailWhitelist;
use Todotoday\AccountBundle\Entity\Api\Microsoft\ContactPerson;
use Todotoday\AccountBundle\Entity\Api\Microsoft\CustomerBankAccount;
use Todotoday\AccountBundle\Entity\Api\Microsoft\VehicleModel;
use Todotoday\AccountBundle\Enum\AccountTitleEnum;
use Todotoday\AccountBundle\Enum\PaymentStatusEnum;
use Todotoday\AccountBundle\Enum\PersonGenderEnum;
use Todotoday\CMSBundle\Entity\Post;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Entity\Corporate;
use Todotoday\PaymentBundle\Entity\CustomerStripeAccount;
use Todotoday\SocialBundle\Entity\SocialComment;
use Todotoday\SocialBundle\Entity\SocialGroup;

/**
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 *
 * @ORM\Entity(repositoryClass="Todotoday\AccountBundle\Repository\AccountRepository")
 * @ORM\Table(
 *     name="account",
 *     schema="account"
 * )
 *
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({
 *     "adherent" = "Todotoday\AccountBundle\Entity\Adherent",
 *     "concierge" = "Todotoday\AccountBundle\Entity\Concierge",
 *     "admin" = "Todotoday\AccountBundle\Entity\Admin",
 *     "partner" = "Todotoday\AccountBundle\Entity\Partner"
 * })
 * @Serializer\ExclusionPolicy("all")
 * @Serializer\AccessorOrder("custom", custom =
 *      {
 *     "firstName",
 *     "lastName",
 *     "username",
 *     "customerAccount",
 *     "language",
 *     "email",
 *     "enabled",
 *     "lastLogin",
 *     "oneSignalId",
 *     "lastUsedAgencySlug",
 *     "linkAgencies",
 *     "cartProducts",
 * })
 * @UniqueEntity(fields={"emailCanonical"},
 *      errorPath="email",
 *      groups={"emailWhiteList"},
 *      message="registration.email.already_used",
 *      repositoryMethod="findByUniqueEmail"
 * )
 */
abstract class Account implements UserInterface, GroupableInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @Serializer\Expose()
     * @Serializer\Groups({"Default", "Social"})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var AccountTitleEnum
     *
     * @ORM\Column(name="title", type="AccountTitleEnum", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"Default"})
     */
    protected $title;

    /**
     * @var PersonGenderEnum
     *
     * @ORM\Column(name="person_gender", type="PersonGenderEnum", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"extra", "Default"})
     */
    protected $personGender;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=128, nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"Default", "Social"})
     */
    protected $firstName;

    /**
     * @var string
     * @Serializer\Expose()
     * @Serializer\Groups({"Default", "Social"})
     * @ORM\Column(name="middle_name", type="string", length=128, nullable=true)
     */
    protected $middleName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=128, nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"Default", "Social"})
     */
    protected $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="registration_token", type="string", length=128, nullable=true, unique=true)
     */
    protected $registrationToken;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="registration_token_requested_at", type="datetime", nullable=true)
     */
    protected $registrationTokenRequestedAt;

    /**
     * @var Collection|LinkAccountAgency[]
     *
     * @ORM\OneToMany(targetEntity="Todotoday\AccountBundle\Entity\LinkAccountAgency", mappedBy="account",
     *     cascade={"persist"})
     * @Serializer\Expose()
     * @Serializer\Groups({"account"})
     */
    protected $linkAgencies;

    /**
     * @ORM\OneToMany(targetEntity="Todotoday\SocialBundle\Entity\LinkAdherentSocialGroup", mappedBy="adherent")
     */
    protected $linkSocialGroups;


    /**
     * @ORM\OneToMany(targetEntity="Todotoday\SocialBundle\Entity\SocialGroup", mappedBy="createdBy")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $socialGroupsAuthor;

    /**
     * @var Corporate
     *
     * @ORM\ManyToOne(targetEntity="Todotoday\CoreBundle\Entity\Corporate", inversedBy="accounts")
     */
    protected $corporate;

    /**
     * @ORM\OneToMany(targetEntity="Todotoday\SocialBundle\Entity\SocialComment", mappedBy="author")
     */
    protected $comments;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=128, unique=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"account"})
     */
    protected $username;

    /**
     * @var string
     *
     * @ORM\Column(name="username_canonical", type="string", length=128, unique=true)
     */
    protected $usernameCanonical;

    /**
     * @var string
     *
     * @EmailWhitelist(customMessage="account.error_email_whitelist",checkMX=true, checkHost=true,
     *     groups={"emailWhiteList"})
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"Default"})
     */
    protected $email;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"Default"})
     */
    protected $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="phone_extension", type="string", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"Default"})
     */
    protected $phoneExtension;

    /**
     * @var string
     *
     * @ORM\Column(name="email_canonical", type="string", length=255, unique=true)
     */
    protected $emailCanonical;

    /**
     * @var bool
     *
     * @ORM\Column(name="enabled", type="boolean")
     * @Serializer\Expose()
     * @Serializer\Groups({"account"})
     */
    protected $enabled;

    /**
     * The salt to use for hashing.
     *
     * @var string
     *
     * @ORM\Column(name="salt", type="string", length=255, nullable=true)
     */
    protected $salt;

    /**
     * Encrypted password. Must be persisted.
     *
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=true)
     */
    protected $password;

    /**
     * Plain password. Used for model validation. Must not be persisted.
     *
     * @var string
     * @Assert\Regex(
     *  pattern="/((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{10,})/",
     *  message="error.password_validation",
     *     groups={"Default","emailWhiteList"}
     * )
     */
    protected $plainPassword;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_login", type="datetime", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"account"})
     */
    protected $lastLogin;

    /**
     * Random string sent to the user email address in order to verify it.
     *
     * @var string
     *
     * @ORM\Column(name="confirmation_token", type="string", length=255, nullable=true, unique=true)
     */
    protected $confirmationToken;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="password_requested_at", type="datetime", nullable=true)
     */
    protected $passwordRequestedAt;

    /**
     * @var Collection
     */
    protected $groups;

    /**
     * @var array
     *
     * @ORM\Column(name="roles", type="array")
     */
    protected $roles;

    /**
     * @var Collection|Post[]
     *
     * @ORM\OneToMany(targetEntity="Todotoday\CMSBundle\Entity\Post", mappedBy="author")
     */
    protected $posts;

    /**
     * @var Agency
     *
     * @ORM\ManyToOne(targetEntity="Todotoday\CoreBundle\Entity\Agency")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $lastAgencyUsed;

    /**
     * @var CustomerBankAccount
     */
    protected $bankAccount;

    /**
     * @var CustomerStripeAccount
     *
     * @ORM\OneToOne(
     *     targetEntity="Todotoday\PaymentBundle\Entity\CustomerStripeAccount",
     *     mappedBy="account",
     *     cascade={"persist"}
     *     )
     */
    protected $customerStripeAccount;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", nullable=true)
     * @Serializer\Expose()
     */
    protected $language;

    /**
     * @var PaymentTypeEnum
     *
     * @ORM\Column(name="payment_type_selected", type="PaymentTypeEnum", nullable=true)
     * @Serializer\Groups({"payment"})
     * @Serializer\Expose()
     */
    protected $paymentTypeSelected;

    /**
     * @var PaymentStatusEnum
     *
     * @ORM\Column(name="payment_method_status", type="PaymentStatusEnum", nullable=false)
     * @Serializer\Groups({"payment"})
     * @Serializer\Expose()
     */
    protected $paymentMethodStatus;

    /**
     * @var ContactPerson
     *
     * @Serializer\Expose()
     * @Serializer\Groups({"personal"})
     * @Serializer\Type("Todotoday\AccountBundle\Entity\Api\Microsoft\ContactPerson")
     */
    protected $contactPrincipal;

    /**
     * @var ContactPerson[]
     *
     * @Serializer\Expose()
     * @Serializer\Groups({"extra"})
     * @Serializer\Type("array<Todotoday\AccountBundle\Entity\Api\Microsoft\ContactPerson>")
     */
    protected $contactChildren;

    /**
     * @var ContactPerson[]
     *
     * @Serializer\Expose()
     * @Serializer\Groups({"extra"})
     * @Serializer\Type("array<Todotoday\AccountBundle\Entity\Api\Microsoft\ContactPerson>")
     */
    protected $contactAssistants;

    /**
     * @var string
     * @Serializer\Expose()
     * @Serializer\Groups({"extra"})
     * @Serializer\Type("string")
     */
    protected $addressCountryRegionId;

    /**
     * @var string
     * @Serializer\Expose()
     * @Serializer\Groups({"extra"})
     * @Serializer\Type("string")
     */
    protected $addressState;

    /**
     * @var string
     * @Serializer\Expose()
     * @Serializer\Groups({"extra"})
     * @Serializer\Type("string")
     */
    protected $addressStreet;

    /**
     * @var string
     * @Serializer\Expose()
     * @Serializer\Groups({"extra"})
     * @Serializer\Type("string")
     */
    protected $addressCity;

    /**
     * @var string
     * @Serializer\Expose()
     * @Serializer\Groups({"extra"})
     * @Serializer\Type("string")
     */
    protected $addressZipCode;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updatedAt;

    /**
     * @var CustomerBankAccount
     */
    protected $customerBankAccount;

    /**
     * @var bool
     */
    protected $cgv;

    /**
     * @var bool
     */
    protected $article32;

    /**
     * @var bool
     */
    protected $allowToUseResponse;

    /**
     * @var bool
     */
    protected $informedCommercialUse;

    /**
     * @var bool
     */
    protected $refusedCommercialUse;

    /**
     * @var \DateTime
     */
    protected $birthDate;

    /**
     * @var \DateTime
     */
    protected $employeeBrithDate;

    /**
     * @var VehicleModel
     */
    protected $vehicleModel;

    /**
     * @var string
     */
    protected $childrenNumber;

    /**
     * @var string
     */
    protected $contactViaApp;

    /**
     * @var string
     */
    protected $contactViaMail;

    /**
     * @var string
     */
    protected $contactViaSms;

    /**
     * @var string
     */
    protected $primaryContactFax;

    /**
     * @var array
     * @Serializer\Expose()
     * @Serializer\Groups({"fgs"})
     * @Serializer\Type("array")
     */
    protected $fgsHierarchy;

    /**
     * @var boolean
     */
    protected $sendMailRegistration;

    /**
     * @var array
     * @Serializer\Expose()
     * @Serializer\Groups({"fgs"})
     * @Serializer\Type("array")
     */
    protected $orderSatisfactionSubjects;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(
     *     targetEntity="Document",
     *     mappedBy="uploadedBy",
     *     cascade={"persist", "remove"},
     *     orphanRemoval=true
     * )
     * @Serializer\Expose()
     * @Serializer\Groups({"Default"})
     */
    protected $myDocuments;

    /**
     * User constructor.
     *
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     */
    public function __construct()
    {
        $this->enabled = false;
        $this->roles = array();
        $this->linkAgencies = new ArrayCollection();
        $this->socialGroupsAuthor = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->linkSocialGroups = new ArrayCollection();

        //        $this->title = AccountTitleEnum::getNewInstance();
        $this->personGender = PersonGenderEnum::getNewInstance();
        $this->paymentMethodStatus = PaymentStatusEnum::getNewInstance();
        $this->contactAssistants = array();
        $this->contactChildren = array();
        $this->language = 'fr';
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getUsername();
    }

    /**
     * @param SocialComment $comment
     *
     * @return Account
     */
    public function addComment(SocialComment $comment): self
    {
        $this->comments[] = $comment;
        $comment->setAuthor($this);

        return $this;
    }

    /**
     * Add ContactAssistant
     *
     * @param ContactPerson $contactPerson
     *
     * @return Account
     */
    public function addContactAssistant(ContactPerson $contactPerson): self
    {
        if (!$this->contactAssistants || !in_array($contactPerson, $this->contactAssistants, true)) {
            $this->contactAssistants[] = $contactPerson;
        }

        return $this;
    }

    /**
     * Add ContactChildren
     *
     * @param ContactPerson $contactPerson
     *
     * @return Account
     */
    public function addContactChild(ContactPerson $contactPerson): self
    {
        if (!$this->contactChildren || !in_array($contactPerson, $this->contactChildren, true)) {
            $this->contactChildren[] = $contactPerson;
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function addGroup(GroupInterface $group): self
    {
        if (!$this->getGroups()->contains($group)) {
            $this->getGroups()->add($group);
        }

        return $this;
    }

    /**
     * Add linkAgency
     *
     * @param LinkAccountAgency $linkAgency
     *
     * @return Account
     */
    public function addLinkAgency(LinkAccountAgency $linkAgency): self
    {
        $this->linkAgencies[] = $linkAgency;
        $linkAgency->setAccount($this);

        return $this;
    }

    /**
     * Add post
     *
     * @param Post $post
     *
     * @return Account
     */
    public function addPost(Post $post): self
    {
        $this->posts->add($post);
        $post->setAuthor($this);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function addRole($role): self
    {
        $role = strtoupper($role);
        if ($role === static::ROLE_DEFAULT) {
            return $this;
        }
        if (!in_array($role, $this->roles, true)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    /**
     * @param SocialGroup $socialGroup
     *
     * @return Account
     */
    public function addSocialGroupsAuthor(SocialGroup $socialGroup): self
    {
        $this->socialGroupsAuthor[] = $socialGroup;
        $socialGroup->setCreatedBy($this);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function eraseCredentials(): self
    {
        $this->plainPassword = null;

        return $this;
    }

    /**
     * Get bankAccount
     *
     * @return CustomerBankAccount
     */
    public function getBankAccount(): ?CustomerBankAccount
    {
        return $this->bankAccount;
    }

    /**
     * Set bankAccount
     *
     * @param CustomerBankAccount $bankAccount
     *
     * @return Account
     */
    public function setBankAccount(CustomerBankAccount $bankAccount): self
    {
        $this->bankAccount = $bankAccount;

        return $this;
    }

    /**
     * Get birthDate
     *
     * @return \DateTime
     */
    public function getBirthDate(): ?\DateTime
    {
        return $this->birthDate;
    }

    /**
     * Set birthDate
     *
     * @param \DateTime $birthDate
     *
     * @return Account
     */
    public function setBirthDate(?\DateTime $birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * Get birthDateDay
     *
     * @return int
     */
    public function getBirthDateDay(): ?int
    {
        if ($this->birthDate) {
            return (int) $this->birthDate->format('d');
        }

        return 0;
    }

    /**
     * Get EmployeebirthDateDay
     *
     * @return int
     */
    public function getEmployeeBirthDateDay(): ?int
    {
        if ($this->employeeBrithDate) {
            return (int) $this->employeeBrithDate->format('d');
        }

        return 0;
    }

    /**
     * Get birthDateMonth
     *
     * @return string
     */
    public function getBirthDateMonth(): ?string
    {
        if ($this->birthDate) {
            return $this->birthDate->format('F');
        }

        return 'None';
    }

    /**
     * Get EmployeeBirthDateMonth
     *
     * @return string
     */
    public function getEmployeeBirthDateMonth(): ?string
    {
        if ($this->employeeBrithDate) {
            return $this->employeeBrithDate->format('F');
        }

        return 'None';
    }

    /**
     * Get birthDateYear
     *
     * @return int
     */
    public function getBirthDateYear(): ?int
    {
        if ($this->birthDate) {
            return (int) $this->birthDate->format('Y');
        }

        return 0;
    }

    /**
     * Get EmployeeBirthDateYear
     *
     * @return int
     */
    public function getEmployeeBirthDateYear(): ?int
    {
        if ($this->employeeBrithDate) {
            return (int) $this->employeeBrithDate->format('Y');
        }

        return 0;
    }

    /**
     * @return Collection|SocialComment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    /**
     * {@inheritdoc}
     */
    public function getConfirmationToken(): ?string
    {
        return $this->confirmationToken;
    }

    /**
     * {@inheritdoc}
     */
    public function setConfirmationToken($confirmationToken): self
    {
        $this->confirmationToken = $confirmationToken;

        return $this;
    }

    /**
     * Get contactAssistants
     *
     * @return ContactPerson[]
     */
    public function getContactAssistants(): ?array
    {
        return $this->contactAssistants;
    }

    /**
     * Set contactAssistants
     *
     * @param ContactPerson[] $contactAssistants
     *
     * @return Account
     */
    public function setContactAssistants(array $contactAssistants): self
    {
        $this->contactAssistants = $contactAssistants;

        return $this;
    }

    /**
     * Get contactChildren
     *
     * @return ContactPerson[]
     */
    public function getContactChildren(): ?array
    {
        return $this->contactChildren;
    }

    /**
     * Set contactChildren
     *
     * @param ContactPerson[] $contactChildren
     *
     * @return Account
     */
    public function setContactChildren(array $contactChildren): self
    {
        $this->contactChildren = $contactChildren;

        return $this;
    }

    /**
     * Get contactPrincipal
     *
     * @return ContactPerson
     */
    public function getContactPrincipal(): ?ContactPerson
    {
        return $this->contactPrincipal;
    }

    /**
     * Set contactPersonal
     *
     * @param ContactPerson $contactPrincipal
     *
     * @return Account
     */
    public function setContactPrincipal(?ContactPerson $contactPrincipal): self
    {
        $this->contactPrincipal = $contactPrincipal;

        return $this;
    }

    /**
     * Get corporate
     *
     * @return Corporate
     */
    public function getCorporate(): ?Corporate
    {
        return $this->corporate;
    }

    /**
     * Set corporate
     *
     * @param Corporate $corporate
     *
     * @return Account
     */
    public function setCorporate(?Corporate $corporate = null): self
    {
        $this->corporate = $corporate;

        return $this;
    }

    /**
     * Get customerBankAccount
     *
     * @return CustomerBankAccount
     */
    public function getCustomerBankAccount(): ?CustomerBankAccount
    {
        return $this->customerBankAccount;
    }

    /**
     * Set customerBankAccount
     *
     * @param CustomerBankAccount $customerBankAccount
     *
     * @return Account
     */
    public function setCustomerBankAccount(?CustomerBankAccount $customerBankAccount): self
    {
        $this->customerBankAccount = $customerBankAccount;

        return $this;
    }

    /**
     * Get customerStripeAccount
     *
     * @return CustomerStripeAccount
     */
    public function getCustomerStripeAccount(): ?CustomerStripeAccount
    {
        return $this->customerStripeAccount;
    }

    /**
     * Set customerStripeAccount
     *
     * @param CustomerStripeAccount $customerStripeAccount
     *
     * @return Account
     */
    public function setCustomerStripeAccount(?CustomerStripeAccount $customerStripeAccount): self
    {
        if ($customerStripeAccount) {
            $customerStripeAccount->setAccount($this);
        } elseif ($this->customerStripeAccount) {
            $customerStripeAccount->setAccount(null);
        }
        $this->customerStripeAccount = $customerStripeAccount;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * {@inheritdoc}
     */
    public function setEmail($email): self
    {
        $this->email = $email;
        $this->username = $email;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getEmailCanonical(): ?string
    {
        return $this->emailCanonical;
    }

    /**
     * {@inheritdoc}
     */
    public function setEmailCanonical($emailCanonical): self
    {
        $this->emailCanonical = $emailCanonical;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return Account
     */
    public function setFirstName(?string $firstName = null): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getGroupNames(): ?array
    {
        $names = array();
        foreach ($this->getGroups() as $group) {
            $names[] = $group->getName();
        }

        return $names;
    }

    /**
     * {@inheritdoc}
     */
    public function getGroups()
    {
        return $this->groups ?: $this->groups = new ArrayCollection();
    }

    /**
     * {@inheritdoc}
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Do getIdStripe
     *
     * @return null|string
     */
    public function getIdStripe(): ?string
    {
        if ($this->customerStripeAccount) {
            return $this->customerStripeAccount->getStripeCustomerId();
        }

        return null;
    }

    /**
     * @return string
     */
    public function getLanguage(): ?string
    {
        return $this->language;
    }

    /**
     * @param string $language
     *
     * @return Account
     */
    public function setLanguage(?string $language): Account
    {
        if ($language === 'en-US') {
            $language = 'en';
        }
        $this->language = $language;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getLanguageMicrosoft(): ?string
    {
        if ($this->language === 'en') {
            return 'en-US';
        }

        return $this->language;
    }

    /**
     * Get lastAgencyUsed
     *
     * @return Agency
     */
    public function getLastAgencyUsed(): ?Agency
    {
        return $this->lastAgencyUsed;
    }

    /**
     * Set lastAgencyUsed
     *
     * @param Agency $lastAgencyUsed
     *
     * @return Account
     */
    public function setLastAgencyUsed(Agency $lastAgencyUsed): self
    {
        $this->lastAgencyUsed = $lastAgencyUsed;

        return $this;
    }

    /**
     * Gets the last login time.
     *
     * @return \DateTime
     */
    public function getLastLogin(): ?\DateTime
    {
        return $this->lastLogin;
    }

    /**
     * {@inheritdoc}
     */
    public function setLastLogin(?\DateTime $time = null): self
    {
        $this->lastLogin = $time;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @Serializer\VirtualProperty()
     * @Serializer\Type("string")
     * @Serializer\Expose()
     * @Serializer\Groups({"Social", "Default"})
     *
     * @return null|string
     */
    public function getFullName(): ?string
    {
        return str_replace('  ', ' ', $this->firstName . ' ' . $this->middleName . ' ' . $this->lastName);
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return Account
     */
    public function setLastName(?string $lastName = null): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return null|string
     * @Serializer\VirtualProperty()
     */
    public function getLastUsedAgencySlug(): ?string
    {
        if ($this->getLastAgencyUsed()) {
            return $this->getLastAgencyUsed()->getSlug();
        }

        return null;
    }

    /**
     * Get linkAgencies
     *
     * @return Collection|LinkAccountAgency[]
     */
    public function getLinkAgencies(): Collection
    {
        return $this->linkAgencies;
    }

    /**
     * @return string|null
     */
    public function getMiddleName(): ?string
    {
        return $this->middleName;
    }

    /**
     * @param mixed $middleName
     *
     * @return Account
     */
    public function setMiddleName(?string $middleName): self
    {
        $this->middleName = $middleName;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * {@inheritdoc}
     */
    public function setPassword($password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Gets the timestamp that the user requested a password reset.
     *
     * @return null|\DateTime
     */
    public function getPasswordRequestedAt(): ?\DateTime
    {
        return $this->passwordRequestedAt;
    }

    /**
     * {@inheritdoc}
     */
    public function setPasswordRequestedAt(\DateTime $date = null): self
    {
        $this->passwordRequestedAt = $date;

        return $this;
    }

    /**
     * Get paymentMethodStatus
     *
     * @return PaymentStatusEnum
     */
    public function getPaymentMethodStatus(): ?PaymentStatusEnum
    {
        return $this->paymentMethodStatus;
    }

    /**
     * Set paymentMethodStatus
     *
     * @param AbstractEnum|PaymentStatusEnum $paymentMethodStatus
     *
     * @return Account
     */
    public function setPaymentMethodStatus(?PaymentStatusEnum $paymentMethodStatus): self
    {
        if (!$paymentMethodStatus || !$this->paymentMethodStatus
            || $paymentMethodStatus->get() !== $this->paymentMethodStatus->get()) {
            $this->paymentMethodStatus = $paymentMethodStatus;
        }

        return $this;
    }

    /**
     * @return null|string
     */
    public function getPaymentMethodStatusString(): ?string
    {
        if (!$this->paymentMethodStatus) {
            return null;
        }

        return $this->paymentMethodStatus->get();
    }

    /**
     * Get paymentTypeSelected
     *
     * @return PaymentTypeEnum
     */
    public function getPaymentTypeSelected(): ?PaymentTypeEnum
    {
        return $this->paymentTypeSelected;
    }

    /**
     * Set paymentTypeSelected
     *
     * @param AbstractEnum|PaymentTypeEnum $paymentTypeSelected
     *
     * @return Account
     */
    public function setPaymentTypeSelected(?PaymentTypeEnum $paymentTypeSelected): self
    {
        if (!$paymentTypeSelected || !$this->paymentTypeSelected
            || $paymentTypeSelected->get() !== $this->paymentTypeSelected->get()) {
            $this->paymentTypeSelected = $paymentTypeSelected;
        }

        return $this;
    }

    /**
     * Get paymentTypeSelected
     *
     * @return string
     */
    public function getPaymentTypeSelectedString(): ?string
    {
        if ($this->paymentTypeSelected) {
            return $this->paymentTypeSelected->get();
        }

        return null;
    }

    /**
     * @return PersonGenderEnum|null
     */
    public function getPersonGender(): ?PersonGenderEnum
    {
        if (!$this->personGender) {
            return null;
        }

        return $this->personGender;
    }

    /**
     * @param AbstractEnum|PersonGenderEnum $personGender
     *
     * @return Account
     */
    public function setPersonGender(?PersonGenderEnum $personGender): self
    {
        if (!$this->personGender
            || ($personGender && $personGender->get() !== $this->personGender->get())) {
            $this->personGender = $personGender;
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getPersonGenderString(): ?string
    {
        if (!$this->personGender) {
            return null;
        }

        return $this->personGender->get();
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Account
     */
    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phoneExtension
     *
     * @return string
     */
    public function getPhoneExtension(): ?string
    {
        return $this->phoneExtension;
    }

    /**
     * Set phoneExtension
     *
     * @param string $phoneExtension
     *
     * @return Account
     */
    public function setPhoneExtension(?string $phoneExtension): self
    {
        $this->phoneExtension = $phoneExtension;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    /**
     * {@inheritdoc}
     */
    public function setPlainPassword($password): self
    {
        $this->plainPassword = $password;

        return $this;
    }

    /**
     * Get posts
     *
     * @return Collection|Post[]
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    /**
     * Set posts
     *
     * @param Collection|Post[] $posts
     *
     * @return Account
     */
    public function setPosts($posts): self
    {
        $this->posts = $posts;

        return $this;
    }

    /**
     * Do getRegistrationRole
     *
     * @return string
     */
    public function getRegistrationRole(): string
    {
        return $this->roles[0] ?? '';
    }

    /**
     * Get registrationToken
     *
     * @return string
     */
    public function getRegistrationToken(): ?string
    {
        return $this->registrationToken;
    }

    /**
     * Set registrationToken
     *
     * @param string|null $registrationToken
     *
     * @return Account
     */
    public function setRegistrationToken(?string $registrationToken): self
    {
        $this->registrationToken = $registrationToken;

        return $this;
    }

    /**
     * Get registrationTokenRequestedAt
     *
     * @return \DateTime
     */
    public function getRegistrationTokenRequestedAt(): ?\DateTime
    {
        return $this->registrationTokenRequestedAt;
    }

    /**
     * Set registrationTokenRequestedAt
     *
     * @param \DateTime $registrationTokenRequestedAt
     *
     * @return Account
     */
    public function setRegistrationTokenRequestedAt(?\DateTime $registrationTokenRequestedAt): self
    {
        $this->registrationTokenRequestedAt = $registrationTokenRequestedAt;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getRoles(): ?array
    {
        $roles = $this->roles;
        foreach ($this->getGroups() as $group) {
            $roles = array_merge($roles, $group->getRoles());
        }
        // we need to make sure to have at least one role
        $roles[] = static::ROLE_DEFAULT;

        return array_unique($roles);
    }

    /**
     * {@inheritdoc}
     */
    public function setRoles(array $roles): self
    {
        $this->roles = array();
        foreach ($roles as $role) {
            $this->addRole($role);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getSalt(): ?string
    {
        return $this->salt;
    }

    /**
     * {@inheritdoc}
     */
    public function setSalt($salt): self
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * @return Collection|null|SocialGroup[]
     */
    public function getSocialGroupsAuthor(): Collection
    {
        return $this->socialGroupsAuthor;
    }

    /**
     * Get title|null
     *
     * @return AccountTitleEnum
     */
    public function getTitle(): ?AccountTitleEnum
    {
        if (!$this->title) {
            return null;
        }

        return $this->title;
    }

    /**
     * Set title
     *
     * @param AccountTitleEnum $title
     *
     * @return Account
     */
    public function setTitle(?AccountTitleEnum $title): self
    {
        if (!$this->title || ($title && $title->get() !== $this->title->get())) {
            $this->title = $title;
        }

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitleString(): ?string
    {
        if (!$this->title) {
            return null;
        }

        return $this->title->get();
    }

    /**
     * {@inheritdoc}
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * {@inheritdoc}
     */
    public function setUsername($username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getUsernameCanonical(): ?string
    {
        return $this->usernameCanonical;
    }

    /**
     * {@inheritdoc}
     */
    public function setUsernameCanonical($usernameCanonical): self
    {
        $this->usernameCanonical = $usernameCanonical;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     *
     * @return Account
     */
    public function setCreatedAt(\DateTime $createdAt): Account
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     *
     * @return Account
     */
    public function setUpdatedAt(\DateTime $updatedAt): Account
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get vehicleModel
     *
     * @return VehicleModel
     */
    public function getVehicleModel(): ?VehicleModel
    {
        return $this->vehicleModel;
    }

    /**
     * Set vehicleModel
     *
     * @param VehicleModel $vehicleModel
     *
     * @return Account
     */
    public function setVehicleModel(?VehicleModel $vehicleModel): self
    {
        $this->vehicleModel = $vehicleModel;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function hasGroup($name): bool
    {
        return in_array($name, $this->getGroupNames(), true);
    }

    /**
     * {@inheritdoc}
     */
    public function hasRole($role): bool
    {
        return in_array(strtoupper($role), $this->getRoles(), true);
    }

    /**
     * {@inheritdoc}
     */
    public function isAccountNonExpired(): bool
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isAccountNonLocked(): bool
    {
        return true;
    }

    /**
     * Get allowToUseResponse
     *
     * @return bool
     */
    public function isAllowToUseResponse(): ?bool
    {
        return $this->allowToUseResponse;
    }

    /**
     * Set allowToUseResponse
     *
     * @param bool $allowToUseResponse
     *
     * @return Account
     */
    public function setAllowToUseResponse(bool $allowToUseResponse): Account
    {
        $this->allowToUseResponse = $allowToUseResponse;

        return $this;
    }

    /**
     * Get article32
     *
     * @return bool
     */
    public function isArticle32(): ?bool
    {
        return $this->article32;
    }

    /**
     * Set article32
     *
     * @param bool $article32
     *
     * @return Account
     */
    public function setArticle32(bool $article32): Account
    {
        $this->article32 = $article32;

        return $this;
    }

    /**
     * Get cgv
     *
     * @return bool
     */
    public function isCgv(): ?bool
    {
        return $this->cgv;
    }

    /**
     * Set cgv
     *
     * @param bool $cgv
     *
     * @return Account
     */
    public function setCgv(bool $cgv): Account
    {
        $this->cgv = $cgv;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function isCredentialsNonExpired(): bool
    {
        return true;
    }

    /**
     * Do isEnabled
     *
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * {@inheritdoc}
     */
    public function setEnabled($boolean): self
    {
        $this->enabled = (bool) $boolean;

        return $this;
    }

    /**
     * Get informedCommercialUse
     *
     * @return bool
     */
    public function isInformedCommercialUse(): ?bool
    {
        return $this->informedCommercialUse;
    }

    /**
     * Set informedCommercialUse
     *
     * @param bool $informedCommercialUse
     *
     * @return Account
     */
    public function setInformedCommercialUse(bool $informedCommercialUse): Account
    {
        $this->informedCommercialUse = $informedCommercialUse;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function isPasswordRequestNonExpired($ttl): bool
    {
        return $this->getPasswordRequestedAt() instanceof \DateTime
            && $this->getPasswordRequestedAt()->getTimestamp() + $ttl > time();
    }

    /**
     * Get refusedCommercialUse
     *
     * @return bool
     */
    public function isRefusedCommercialUse(): ?bool
    {
        return $this->refusedCommercialUse;
    }

    /**
     * Set refusedCommercialUse
     *
     * @param bool $refusedCommercialUse
     *
     * @return Account
     */
    public function setRefusedCommercialUse(bool $refusedCommercialUse): Account
    {
        $this->refusedCommercialUse = $refusedCommercialUse;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function isSuperAdmin(): bool
    {
        return $this->hasRole(static::ROLE_SUPER_ADMIN);
    }

    /**
     * @param SocialComment $comment
     *
     * @return Account
     */
    public function removeComment(SocialComment $comment): self
    {
        $this->comments->removeElement($comment);
        $comment->setAuthor(null);

        return $this;
    }

    /**
     * Remove ContactAssistant
     *
     * @param ContactPerson $contactPerson
     *
     * @return Account
     */
    public function removeContactAssistant(ContactPerson $contactPerson): self
    {
        if (false !== $key = array_search($contactPerson, $this->contactAssistants, true)) {
            unset($this->contactAssistants[$key]);
        }

        return $this;
    }

    /**
     * Remove ContactChild
     *
     * @param ContactPerson $contactPerson
     *
     * @return Account
     */
    public function removeContactChild(ContactPerson $contactPerson): self
    {
        if (false !== $key = array_search($contactPerson, $this->contactChildren, true)) {
            unset($this->contactChildren[$key]);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function removeGroup(GroupInterface $group): self
    {
        if ($this->getGroups()->contains($group)) {
            $this->getGroups()->removeElement($group);
        }

        return $this;
    }

    /**
     * Remove linkAgency
     *
     * @param LinkAccountAgency $linkAgency
     *
     * @return Account
     */
    public function removeLinkAgency(LinkAccountAgency $linkAgency): self
    {
        $this->linkAgencies->removeElement($linkAgency);
        $linkAgency->setAccount(null);

        return $this;
    }

    /**
     * Remove post
     *
     * @param Post $post
     *
     * @return Account
     */
    public function removePost(Post $post): self
    {
        $this->posts->remove($post);
        $post->setAuthor(null);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function removeRole($role)
    {
        if (false !== $key = array_search(strtoupper($role), $this->roles, true)) {
            unset($this->roles[$key]);
            $this->roles = array_values($this->roles);
        }

        return $this;
    }

    /**
     * @param SocialGroup $socialGroup
     *
     * @return Account
     */
    public function removeSocialGroupsAuthor(SocialGroup $socialGroup): self
    {
        $this->socialGroupsAuthor->removeElement($socialGroup);
        $socialGroup->setCreatedBy(null);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function serialize(): ?string
    {
        return serialize(
            array(
                $this->password,
                $this->salt,
                $this->usernameCanonical,
                $this->username,
                $this->enabled,
                $this->id,
                $this->email,
                $this->emailCanonical,
            )
        );
    }

    /**
     * Set birthDateDay
     *
     * @param int $day
     *
     * @return Account
     */
    public function setBirthDateDay(?int $day): self
    {
        if (0 !== $day) {
            if (!$this->birthDate) {
                $this->birthDate = new \DateTime();
            }
            $this->birthDate->setDate(
                (int) $this->birthDate->format('Y'),
                (int) $this->birthDate->format('m'),
                $day
            );
        }

        return $this;
    }

    /**
     * Set EmployeeBirthDateDay
     *
     * @param int $day
     *
     * @return Account
     */
    public function setEmployeeBirthDateDay(?int $day): self
    {
        if (0 !== $day) {
            if (!$this->employeeBrithDate) {
                $this->employeeBrithDate = new \DateTime();
            }
            $this->employeeBrithDate->setDate(
                (int) $this->employeeBrithDate->format('Y'),
                (int) $this->employeeBrithDate->format('m'),
                $day
            );
        }

        return $this;
    }

    /**
     * Set birthDateMonth
     *
     * @param string $month
     *
     * @return Account
     */
    public function setBirthDateMonth(?string $month): self
    {
        if ($month !== 'None') {
            if (!$this->birthDate) {
                $this->birthDate = new \DateTime();
            }
            $this->birthDate->setDate(
                (int) $this->birthDate->format('Y'),
                (int) date('m', strtotime($month)),
                (int) $this->birthDate->format('d')
            );
        }

        return $this;
    }

    /**
     * Set EmployeeBirthDateMonth
     *
     * @param string $month
     *
     * @return Account
     */
    public function setEmployeeBirthDateMonth(?string $month): self
    {
        if ($month !== 'None') {
            if (!$this->employeeBrithDate) {
                $this->employeeBrithDate = new \DateTime();
            }
            $this->employeeBrithDate->setDate(
                (int) $this->employeeBrithDate->format('Y'),
                (int) date('m', strtotime($month)),
                (int) $this->employeeBrithDate->format('d')
            );
        }

        return $this;
    }

    /**
     * Set birthDateYear
     *
     * @param int $year
     *
     * @return Account
     */
    public function setBirthDateYear(?int $year): self
    {
        if (0 !== $year) {
            if (!$this->birthDate) {
                $this->birthDate = new \DateTime();
            }
            $this->birthDate->setDate(
                $year,
                (int) $this->birthDate->format('m'),
                (int) $this->birthDate->format('d')
            );
        }

        return $this;
    }

    /**
     * Set EmployeebirthDateYear
     *
     * @param int $year
     *
     * @return Account
     */
    public function setEmployeeBirthDateYear(?int $year): self
    {
        if (0 !== $year) {
            if (!$this->employeeBrithDate) {
                $this->employeeBrithDate = new \DateTime();
            }
            $this->employeeBrithDate->setDate(
                $year,
                (int) $this->employeeBrithDate->format('m'),
                (int) $this->employeeBrithDate->format('d')
            );
        }

        return $this;
    }

    /**
     * Do setIdStripe
     *
     * @param string $idStripe
     *
     * @return Account
     */
    public function setIdStripe(?string $idStripe): self
    {
        if ($idStripe) {
            if (!$this->customerStripeAccount) {
                $this->customerStripeAccount = new CustomerStripeAccount();
            }
            $this->customerStripeAccount->setStripeCustomerId($idStripe);
        }

        return $this;
    }

    /**
     * @param null|string $paymentMethodStatus
     *
     * @return Account
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function setPaymentMethodStatusString(?string $paymentMethodStatus): self
    {
        return $this->setPaymentMethodStatus(PaymentStatusEnum::getNewInstance($paymentMethodStatus));
    }

    /**
     * Set paymentTypeSelected
     *
     * @param string $paymentTypeSelected
     *
     * @return Account
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function setPaymentTypeSelectedString(?string $paymentTypeSelected): self
    {
        $value = $paymentTypeSelected ? strtolower($paymentTypeSelected) : null;

        return $this->setPaymentTypeSelected(PaymentTypeEnum::getNewInstance($value));
    }

    /**
     * @param null|string $personGender
     *
     * @return Account
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function setPersonGenderString(?string $personGender): self
    {
        return $this->setPersonGender(PersonGenderEnum::getNewInstance($personGender));
    }

    /**
     * Do setRegistrationRole
     *
     * @param string $role
     *
     * @return Account
     */
    public function setRegistrationRole(string $role): self
    {
        $this->roles = [$role];

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setSuperAdmin($boolean): self
    {
        if (true === $boolean) {
            return $this->addRole(static::ROLE_SUPER_ADMIN);
        }

        return $this->removeRole(static::ROLE_SUPER_ADMIN);
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Account
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function setTitleString(?string $title): self
    {
        $this->title = AccountTitleEnum::getNewInstance($title);

        return $this;
    }

    /**
     * @return string
     */
    public function getAddressCountryRegionId(): ?string
    {
        return $this->addressCountryRegionId;
    }

    /**
     * @param string $addressCountryRegionId
     *
     * @return Account
     */
    public function setAddressCountryRegionId(?string $addressCountryRegionId): Account
    {
        $this->addressCountryRegionId = $addressCountryRegionId;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddressState(): ?string
    {
        return $this->addressState;
    }

    /**
     * @param string $addressState
     *
     * @return Account
     */
    public function setAddressState(?string $addressState): Account
    {
        $this->addressState = $addressState;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddressStreet(): ?string
    {
        return $this->addressStreet;
    }

    /**
     * @param string $addressStreet
     *
     * @return Account
     */
    public function setAddressStreet(?string $addressStreet): Account
    {
        $this->addressStreet = $addressStreet;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddressCity(): ?string
    {
        return $this->addressCity;
    }

    /**
     * @param string $addressCity
     *
     * @return Account
     */
    public function setAddressCity(?string $addressCity): Account
    {
        $this->addressCity = $addressCity;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddressZipCode(): ?string
    {
        return $this->addressZipCode;
    }

    /**
     * @param string $addressZipCode
     *
     * @return Account
     */
    public function setAddressZipCode(?string $addressZipCode): Account
    {
        $this->addressZipCode = $addressZipCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getChildrenNumber(): ?string
    {
        return $this->childrenNumber;
    }

    /**
     * @param string $childrenNumber
     *
     * @return Account
     */
    public function setChildrenNumber(?string $childrenNumber): Account
    {
        $this->childrenNumber = $childrenNumber;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getContactViaApp(): ?string
    {
        return $this->contactViaApp;
    }

    /**
     * @param string|null $contactViaApp
     *
     * @return Account
     */
    public function setContactViaApp(?string $contactViaApp): Account
    {
        $this->contactViaApp = $contactViaApp;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getContactViaMail(): ?string
    {
        return $this->contactViaMail;
    }

    /**
     * @param string|null $contactViaMail
     *
     * @return Account
     */
    public function setContactViaMail(?string $contactViaMail): Account
    {
        $this->contactViaMail = $contactViaMail;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getContactViaSms(): ?string
    {
        return $this->contactViaSms;
    }

    /**
     * @param string|null $contactViaSms
     *
     * @return Account
     */
    public function setContactViaSms(?string $contactViaSms): Account
    {
        $this->contactViaSms = $contactViaSms;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPrimaryContactFax(): ?string
    {
        return $this->primaryContactFax;
    }

    /**
     * @param string|null $primaryContactFax
     *
     * @return Account
     */
    public function setPrimaryContactFax(?string $primaryContactFax): Account
    {
        $this->primaryContactFax = $primaryContactFax;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEmployeeBrithDate(): ?\DateTime
    {
        return $this->employeeBrithDate;
    }

    /**
     * @return bool
     */
    public function isPasswordSet(): bool
    {
        return null !== $this->password && '' !== $this->password;
    }

    /**
     * @return bool
     */
    public function isRegistrationComplete(): bool
    {
        return $this->hasRole('ROLE_ADHERENT')
            || $this->hasRole('ROLE_NOT_ADHERENT')
            || $this->hasRole('ROLE_CO_ADHERENT-ADHERENT')
            || $this->hasRole('ROLE_COMMUNITY_MANAGER')
            || $this->hasRole('ROLE_CUSTOMER_SERVICE')
            || $this->hasRole('ROLE_ADMIN');
    }

    /**
     * @return array
     */
    public function getFgsHierarchy(): ?array
    {
        return $this->fgsHierarchy;
    }

    /**
     * @param array $fgsHierarchy
     */
    public function setFgsHierarchy(array $fgsHierarchy)
    {
        $this->fgsHierarchy = $fgsHierarchy;
    }

    /**
     * @return array
     */
    public function getOrderSatisfactionSubjects(): ?array
    {
        return $this->orderSatisfactionSubjects;
    }

    /**
     * @param array $orderSatisfactionSubjects
     */
    public function setOrderSatisfactionSubjects(array $orderSatisfactionSubjects)
    {
        $this->orderSatisfactionSubjects = $orderSatisfactionSubjects;
    }

    /**
     * @param \DateTime $employeeBrithDate
     *
     * @return Account
     */
    public function setEmployeeBrithDate(?\DateTime $employeeBrithDate): Account
    {
        $this->employeeBrithDate = $employeeBrithDate;

        return $this;
    }

    /**
     * @return bool
     */
    public function isSendMailRegistration(): ?bool
    {
        return $this->sendMailRegistration;
    }

    /**
     * @param bool $sendMailRegistration
     *
     * @return Account
     */
    public function setSendMailRegistration(bool $sendMailRegistration): Account
    {
        $this->sendMailRegistration = $sendMailRegistration;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getAllAgencies(): ?string
    {
        $agencies = [];
        foreach ($this->getLinkAgencies() as $linkAccountAgency) {
            $agencies[] = $linkAccountAgency->getAgency()->getSlug();
        }

        return implode(';', $agencies);
    }

    /**
     * {@inheritdoc}
     */
    public function unserialize($serialized): void
    {
        $data = unserialize($serialized);
        if (13 === count($data)) {
            // Unserializing a User object from 1.3.x
            unset($data[4], $data[5], $data[6], $data[9], $data[10]);
            $data = array_values($data);
        } elseif (11 === count($data)) {
            // Unserializing a User from a dev version somewhere between 2.0-alpha3 and 2.0-beta1
            unset($data[4], $data[7], $data[8]);
            $data = array_values($data);
        }
        list(
            $this->password, $this->salt, $this->usernameCanonical, $this->username, $this->enabled, $this->id,
            $this->email, $this->emailCanonical
            ) = $data;
    }
}
