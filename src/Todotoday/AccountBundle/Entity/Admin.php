<?php declare(strict_types = 1);

namespace Todotoday\AccountBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Users
 *
 * @ORM\Table(name="admin", schema="account")
 * @ORM\Entity(repositoryClass="Todotoday\AccountBundle\Repository\AdminRepository")
 */
class Admin extends Account
{
}
