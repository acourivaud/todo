<?php declare(strict_types=1);


namespace Todotoday\AccountBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Todotoday\MediaBundle\Entity\Media;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Documents
 *
 * @ORM\Table(name="adherent_documents", schema="media")
 * @ORM\Entity(repositoryClass="Todotoday\AccountBundle\Repository\DocumentRepository")
 * @Serializer\ExclusionPolicy("all")
 */

class Document
{
    /**
     * @var integer
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer", options={"unsigned"=true})
     * @Serializer\Expose()
     */
    protected $id;

    /**
     * @var
     *
     * @ORM\Column(type="string", name="title", nullable=true)
     * @Serializer\Expose()
     */
    protected $title;

    /**
     * @var Adherent
     *
     * @ORM\ManyToOne(targetEntity="Todotoday\AccountBundle\Entity\Adherent", inversedBy="documents")
     * @Serializer\Expose()
     * @Serializer\Groups("adherent")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $adherent;

    /**
     * @var \Todotoday\AccountBundle\Entity\Account
     *
     * @ORM\ManyToOne(targetEntity="\Todotoday\AccountBundle\Entity\Account", inversedBy="myDocuments")
     * @Serializer\Expose()
     * @Serializer\Groups("adherent")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $uploadedBy;

    /**
     * @var \Todotoday\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="\Todotoday\MediaBundle\Entity\Media")
     * @ORM\JoinColumn(name="media_id", referencedColumnName="id")
     * @Serializer\Expose()
     */
    protected $media;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=true)
     * @Serializer\Expose()
     */
    protected $createdAt;

    protected $uploadedByAdherent;


    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getUploadedByMe(): bool
    {
        return $this->adherent->getId() === $this->uploadedBy->getId();
    }

    /**
     * {@inheritdoc}
     */
    public function getAdherant()
    {
        return $this->adherent;
    }

    /**
     * @param \Todotoday\AccountBundle\Entity\Adherent $adherent
     *
     * @return $this
     */
    public function setAdherant(Adherent $adherent = null): self
    {
        $this->adherent = $adherent;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getUploadedBy()
    {
        return $this->uploadedBy;
    }

    /**
     * @param \Todotoday\AccountBundle\Entity\Account $uploadedBy
     *
     * @return $this
     */
    public function setUploadedBy(Account $uploadedBy = null): self
    {
        $this->uploadedBy = $uploadedBy;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setMedia(\Sonata\MediaBundle\Model\MediaInterface $media = null)
    {
        $this->media = $media;
    }

    /**
     * {@inheritdoc}
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     *
     * @return \Todotoday\AccountBundle\Entity\Document
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     *
     * @return Document
     */
    public function setCreatedAt(\DateTime $createdAt): Document
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
