<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 31/01/2017
 * Time: 17:09
 */

namespace Todotoday\AccountBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Partner
 *
 * @ORM\Entity()
 * @ORM\Table(name="partner", schema="account")
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Entity
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class Partner extends Account
{
}
