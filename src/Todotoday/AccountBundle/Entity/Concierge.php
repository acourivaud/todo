<?php declare(strict_types = 1);

namespace Todotoday\AccountBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Todotoday\MediaBundle\Entity\Media;
use JMS\Serializer\Annotation as Serializer;

/**
 * Concierge
 *
 * @ORM\Table(name="concierge", schema="account")
 * @ORM\Entity(repositoryClass="Todotoday\AccountBundle\Repository\ConciergeRepository")
 */
class Concierge extends Account
{

}
