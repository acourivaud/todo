<?php declare(strict_types=1);

namespace Todotoday\AccountBundle\Entity;

use Actiane\AlertBundle\Entity\Alert;
use Actiane\PaymentBundle\Enum\PaymentTypeEnum;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Exception\BadMethodCallException;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Todotoday\AccountBundle\Entity\Api\Microsoft\CustField;
use Todotoday\AccountBundle\Enum\AdherentTypeEnum;
use Todotoday\AccountBundle\Enum\PaymentStatusEnum;
use Todotoday\CartBundle\Entity\CartProduct;
use Todotoday\CartBundle\Entity\Wishlist;
use Todotoday\MediaBundle\Entity\Media;
use Todotoday\SocialBundle\Entity\LinkAdherentSocialGroup;

/**
 * Adherent
 *
 * @ORM\Table(name="adherent", schema="account")
 * @ORM\Entity(repositoryClass="Todotoday\AccountBundle\Repository\AdherentRepository")
 * @UniqueEntity(fields={"customerAccount"}, groups={"emailWhiteList"})
 * @Serializer\ExclusionPolicy("all")
 * @Serializer\AccessorOrder("custom", custom={"customerAccount","dataAreaId","oneSignalId","discr","title","phone"})
 */
class Adherent extends Account
{
    public const MAX_CO_ADHERENTS = 1;

    /**
     * @var string
     *
     * @ORM\Column(name="customer_account", type="string", length=15, unique=true, nullable=true)
     * @Serializer\Expose()
     */
    protected $customerAccount;

    /**
     * @var string
     *
     * @ORM\Column(name="party_number", type="string", unique=true, nullable=true)
     * @Serializer\Expose()
     */
    protected $partyNumber;

    /**
     * @ORM\Column(name="data_area_id", type="string", nullable=true)
     * @Serializer\Expose()
     */
    protected $dataAreaId;

    /**
     * @var Api\Microsoft\Account
     */
    protected $customer;

    /**
     * @var string
     */
    protected $personMaritalStatus;

    /**
     * @var string
     */
    protected $canOrder;

    /**
     * @var int
     */
    protected $birthYear;

    /**
     * @ORM\OneToMany(targetEntity="Todotoday\SocialBundle\Entity\LinkAdherentSocialGroup", mappedBy="adherent")
     */
    protected $linkSocialGroups;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"account","onesignal"})
     */
    protected $oneSignalId;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="crm_id_adherent", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"account"})
     */
    protected $crmIdAdherent;

    /**
     * @var AdherentTypeEnum
     *
     * @ORM\Column(name="customer_group", type="AdherentTypeEnum", nullable=false)
     * @Serializer\Expose()
     */
    protected $customerGroup;

    /**
     * @var Collection|Alert[]
     *
     * @ORM\OneToMany(targetEntity="Actiane\AlertBundle\Entity\Alert", mappedBy="account")
     */
    protected $alerts;

    /**
     * @var cartProduct
     *
     * @ORM\OneToMany(targetEntity="Todotoday\CartBundle\Entity\CartProduct", mappedBy="adherent")
     * @Serializer\Expose()
     * @Serializer\Groups("carts")
     */
    private $cartProducts;

    /**
     * @var Wishlist
     *
     * @ORM\OneToMany(targetEntity="Todotoday\CartBundle\Entity\Wishlist", mappedBy="adherent")
     */
    private $wishlists;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="locker_code", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"extra","account"})
     */
    private $lockerCode;

    /**
     * @ORM\Column(type="string", name="enterprise", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"extra"})
     */
    private $enterprise;

    /**
     * @var CustField[]
     */
    protected $custFields;

    /**
     * @var Adherent
     *
     * @ORM\ManyToOne(targetEntity="Todotoday\AccountBundle\Entity\Adherent", inversedBy="childrens")
     */
    protected $parent;

    /**
     * @var Collection|Adherent[]
     *
     * @ORM\OneToMany(targetEntity="Todotoday\AccountBundle\Entity\Adherent", mappedBy="parent", cascade={"persist"})
     */
    protected $childrens;

    /**
     * @var string
     * @ORM\Column(name="acronis_id", type="string", nullable=true)
     */
    protected $acronisId;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $employer;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $relation;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $weddingDate;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"Default"})
     */
    protected $optin;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(
     *     targetEntity="Document",
     *     mappedBy="adherent",
     *     cascade={"persist", "remove"},
     *     orphanRemoval=true
     * )
     * @Serializer\Expose()
     * @Serializer\Groups({"Default"})
     */
    protected $documents;

    /**
     * Do __isset
     *
     * @param string $name
     *
     * @return bool
     */
    public function __isset($name)
    {
        return isset($this->custFields[$name]);
    }

    /**
     * Do __set
     *
     * @param string $name
     * @param mixed  $value
     *
     * @return Adherent
     * @throws \Gedmo\Exception\BadMethodCallException
     */
    public function __set($name, $value)
    {
        if (isset($this->custFields[$name])) {
            $custField = $this->custFields[$name];
            $custField->{'set' . $custField->getFieldType() . 'Value'}($value);

            return $this;
        }

        throw new BadMethodCallException('Method ' . $name . ' not exists');
    }

    /**
     * Do __get
     *
     * @param string $name
     *
     * @return mixed
     * @throws \Gedmo\Exception\BadMethodCallException
     */
    public function __get($name)
    {
        if (isset($this->custFields[$name])) {
            $custField = $this->custFields[$name];

            return $custField->{'get' . $custField->getFieldType() . 'Value'}();
        }

        throw new BadMethodCallException('Method ' . $name . ' not exists');
    }

    /**
     * Adherent constructor.
     *
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     */
    public function __construct()
    {
        parent::__construct();
        $this->cartProducts = new ArrayCollection();
        $this->wishlists = new ArrayCollection();
        $this->linkSocialGroups = new ArrayCollection();
        $this->alerts = new ArrayCollection();
        $this->documents = new ArrayCollection();
    }

    /**
     * Add cartProduct
     *
     * @param CartProduct $cartProduct
     *
     * @return Adherent
     */
    public function addCartProduct(CartProduct $cartProduct): self
    {
        $this->cartProducts[] = $cartProduct;
        $cartProduct->setAdherent($this);

        return $this;
    }

    /**
     * Add socialGroup
     *
     * @param LinkAdherentSocialGroup $socialGroup
     *
     * @return Adherent
     */
    public function addLinkSocialGroup(LinkAdherentSocialGroup $socialGroup): self
    {
        $this->linkSocialGroups[] = $socialGroup;
        $socialGroup->setAdherent($this);

        return $this;
    }

    /**
     * Add wishlist
     *
     * @param Wishlist $wishlist
     *
     * @return Adherent
     *
     */
    public function addWishlist(Wishlist $wishlist): self
    {
        $this->wishlists[] = $wishlist;
        $wishlist->setAdherent($this);

        return $this;
    }

    /**
     * Get birthYear
     *
     * @return int
     */
    public function getBirthYear(): ?int
    {
        return $this->birthYear;
    }

    /**
     * Set birthYear
     *
     * @param int $birthYear
     *
     * @return Adherent
     */
    public function setBirthYear(?int $birthYear): self
    {
        $this->birthYear = $birthYear;

        return $this;
    }

    /**
     * Get canOrder
     *
     * @return string
     */
    public function getCanOrder(): ?string
    {
        return $this->canOrder;
    }

    /**
     * Set canOrder
     *
     * @param string $canOrder
     *
     * @return Adherent
     */
    public function setCanOrder(?string $canOrder): self
    {
        $this->canOrder = $canOrder;

        return $this;
    }

    /**
     * Get cartProducts
     *
     * @return Collection|CartProduct[]
     */
    public function getCartProducts(): Collection
    {
        return $this->cartProducts;
    }

    /**
     * Get childrens
     *
     * @return Collection|Adherent[]
     */
    public function getChildrens(): Collection
    {
        return $this->childrens;
    }

    /**
     * @return string
     */
    public function getCrmIdAdherent(): ?string
    {
        return $this->crmIdAdherent;
    }

    /**
     * @param string $crmIdAdherent
     *
     * @return Adherent
     */
    public function setCrmIdAdherent(?string $crmIdAdherent): self
    {
        $this->crmIdAdherent = $crmIdAdherent;

        return $this;
    }

    /**
     * Get custFields
     *
     * @return CustField[]
     */
    public function getCustFields(): ?array
    {
        return $this->custFields;
    }

    /**
     * @return Api\Microsoft\Account
     */
    public function getCustomer(): Api\Microsoft\Account
    {
        return $this->customer;
    }

    /**
     * @param Api\Microsoft\Account $customer
     *
     * @return Adherent
     */
    public function setCustomer(Api\Microsoft\Account $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerAccount(): ?string
    {
        return $this->customerAccount;
    }

    /**
     * @param string $customerAccount
     *
     * @return Adherent
     */
    public function setCustomerAccount(string $customerAccount): self
    {
        $this->customerAccount = $customerAccount;

        return $this;
    }

    /**
     * @return AdherentTypeEnum|null
     */
    public function getCustomerGroup(): ?AdherentTypeEnum
    {
        return $this->customerGroup;
    }

    /**
     * @param AdherentTypeEnum $customerGroup
     *
     * @return Adherent
     */
    public function setCustomerGroup(?AdherentTypeEnum $customerGroup): self
    {
        $this->customerGroup = $customerGroup;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getCustomerGroupString(): ?string
    {
        if (!$this->getCustomerGroup()) {
            return null;
        }

        return $this->customerGroup->get();
    }

    /**
     * @return mixed
     */
    public function getDataAreaId(): ?string
    {
        return $this->dataAreaId;
    }

    /**
     * @param mixed $dataAreaId
     *
     * @return Adherent
     */
    public function setDataAreaId(string $dataAreaId): self
    {
        $this->dataAreaId = strtolower($dataAreaId);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEnterprise(): ?string
    {
        return $this->enterprise;
    }

    /**
     * @param string|null $enterprise
     *
     * @return Adherent
     */
    public function setEnterprise(?string $enterprise): self
    {
        $this->enterprise = $enterprise;

        return $this;
    }

    /**
     * @return Collection|LinkAdherentSocialGroup[]
     */
    public function getLinkSocialGroups(): Collection
    {
        return $this->linkSocialGroups;
    }

    /**
     * @return string|null
     */
    public function getLockerCode(): ?string
    {
        return $this->lockerCode;
    }

    /**
     * @param string $lockerCode
     *
     * @return Adherent
     */
    public function setLockerCode(?string $lockerCode): self
    {
        $this->lockerCode = $lockerCode;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOneSignalId(): ?string
    {
        return $this->oneSignalId;
    }

    /**
     * @param mixed $oneSignalId
     *
     * @return Adherent
     */
    public function setOneSignalId(?string $oneSignalId): self
    {
        $this->oneSignalId = $oneSignalId;

        return $this;
    }

    /**
     * @return null|Adherent
     */
    public function getParent(): ?self
    {
        return $this->parent;
    }

    /**
     * Get partyNumber
     *
     * @return string
     */
    public function getPartyNumber(): ?string
    {
        return $this->partyNumber;
    }

    /**
     * Set partyNumber
     *
     * @param string $partyNumber
     *
     * @return Adherent
     */
    public function setPartyNumber(?string $partyNumber): self
    {
        $this->partyNumber = $partyNumber;

        return $this;
    }

    /**
     * Get personMaritalStatus
     *
     * @return string
     */
    public function getPersonMaritalStatus(): ?string
    {
        return $this->personMaritalStatus;
    }

    /**
     * Set personMaritalStatus
     *
     * @param string $personMaritalStatus
     *
     * @return Adherent
     */
    public function setPersonMaritalStatus(?string $personMaritalStatus): self
    {
        $this->personMaritalStatus = $personMaritalStatus;

        return $this;
    }

    /**
     * Get wishlists
     *
     * @return Collection|Wishlist[}
     */
    public function getWishlists(): Collection
    {
        return $this->wishlists;
    }

    /**
     * Remove cartProduct
     *
     * @param CartProduct $cartProduct
     *
     * @return Adherent
     */
    public function removeCartProduct(CartProduct $cartProduct): self
    {
        $this->cartProducts->removeElement($cartProduct);
        $cartProduct->setAdherent();

        return $this;
    }

    /**
     * Remove socialGroup
     *
     * @param LinkAdherentSocialGroup $socialGroup
     *
     * @return Adherent
     */
    public function removeLinkSocialGroup(LinkAdherentSocialGroup $socialGroup): self
    {
        $this->linkSocialGroups->removeElement($socialGroup);
        $socialGroup->setAdherent();

        return $this;
    }

    /**
     * Remove wishlist
     *
     * @param \Todotoday\CartBundle\Entity\Wishlist $wishlist
     *
     * @return Adherent
     */
    public function removeWishlist(Wishlist $wishlist): self
    {
        $this->wishlists->removeElement($wishlist);
        $wishlist->setAdherent();

        return $this;
    }

    /**
     * Set custFields
     *
     * @param CustField[] $custFields
     *
     * @return Adherent
     */
    public function setCustFields(array $custFields): self
    {
        $this->custFields = array();

        foreach ($custFields as $custField) {
            $this->custFields[$custField->getFieldId()] = $custField;
        }

        return $this;
    }

    /**
     * Set childrens
     *
     * @param Collection|Adherent[] $childrens
     *
     * @return Adherent
     */
    public function setChildren($childrens): self
    {
        $this->childrens = $childrens;

        return $this;
    }

    /**
     * @param null|string $customerGroup
     *
     * @return Adherent
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function setCustomerGroupString(?string $customerGroup): self
    {
        $this->customerGroup = AdherentTypeEnum::getNewInstance($customerGroup);

        return $this;
    }

    /**
     * Set parent
     *
     * @param Adherent $parent
     *
     * @return Adherent
     */
    public function setParent(?Adherent $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Add alert
     *
     * @param Alert $alert
     *
     * @return Adherent
     */
    public function addAlert(Alert $alert): self
    {
        $this->alerts[] = $alert;
        $alert->setAccount($this);

        return $this;
    }

    /**
     * Remove alert
     *
     * @param Alert $alert
     *
     * @return Adherent
     */
    public function removeAlert(Alert $alert): self
    {
        $this->alerts->removeElement($alert);
        $alert->setAccount(null);

        return $this;
    }

    /**
     * Get alerts
     *
     * @return Collection|Alert[]
     */
    public function getAlerts(): Collection
    {
        return $this->alerts;
    }

    /**
     * Add child
     *
     * @param Adherent $child
     *
     * @return Adherent
     */
    public function addChild(Adherent $child): self
    {
        $this->childrens[] = $child;
        $child->setParent($this);

        return $this;
    }

    /**
     * Remove child
     *
     * @param Adherent $child
     *
     * @return Adherent
     */
    public function removeChild(Adherent $child): self
    {
        $this->childrens->removeElement($child);
        $child->setParent(null);

        return $this;
    }

    /**
     * @return bool
     */
    public function isCoAdherent(): bool
    {
        return $this->hasRole('ROLE_CO_ADHERENT') && $this->getParent();
    }

    /**
     * Get paymentMethodStatus
     *
     * @return PaymentStatusEnum
     */
    public function getPaymentMethodStatus(): ?PaymentStatusEnum
    {
        if ($this->isCoAdherent()) {
            $parentPaymentMethodStatus = $this
                ->getParent()
                ->getPaymentMethodStatus();

            return $parentPaymentMethodStatus;
        }

        return $this->paymentMethodStatus;
    }

    /**
     * @return null|string
     */
    public function getPaymentMethodStatusString(): ?string
    {
        $paymentMethodStatus = $this->getPaymentMethodStatus();
        if (!$paymentMethodStatus) {
            return null;
        }

        return $paymentMethodStatus->get();
    }

    /**
     * Get paymentTypeSelected
     *
     * @return PaymentTypeEnum
     */
    public function getPaymentTypeSelected(): ?PaymentTypeEnum
    {
        if ($this->isCoAdherent()) {
            $parentPaymentTypeSelected = $this
                ->getParent()
                ->getPaymentTypeSelected();

            return $parentPaymentTypeSelected;
        }

        return $this->paymentTypeSelected;
    }

    /**
     * Get paymentTypeSelected
     *
     * @return string
     */
    public function getPaymentTypeSelectedString(): ?string
    {
        $paymentTypeSelected = $this->getPaymentTypeSelected();
        if ($paymentTypeSelected) {
            return $paymentTypeSelected->get();
        }

        return null;
    }

    /**
     * @return string
     */
    public function getAcronisId(): ?string
    {
        return $this->acronisId;
    }

    /**
     * @param string $acronisId
     *
     * @return Adherent
     */
    public function setAcronisId(?string $acronisId): Adherent
    {
        $this->acronisId = $acronisId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmployer(): ?string
    {
        return $this->employer;
    }

    /**
     * @param string $employer
     *
     * @return Adherent
     */
    public function setEmployer(string $employer): Adherent
    {
        $this->employer = $employer;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getRelation(): ?string
    {
        return $this->relation;
    }

    /**
     * @param string $relation
     *
     * @return Adherent
     */
    public function setRelation(string $relation): Adherent
    {
        $this->relation = $relation;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getWeddingDate(): ?\DateTime
    {
        return $this->weddingDate;
    }

    /**
     * @param \DateTime|null $weddingDate
     *
     * @return Adherent
     */
    public function setWeddingDate(?\DateTime $weddingDate): Adherent
    {
        $this->weddingDate = $weddingDate;

        return $this;
    }

    /**
     * Remove document
     *
     * @param Document $documents
     */
    public function removeDocument(Document $documents)
    {
        $this->documents->removeElement($documents);
    }

    /**
     * {@inheritdoc}
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * {@inheritdoc}
     */
    public function setDocuments($documents)
    {
        $this->documents = $documents;
    }


    /**
     * {@inheritdoc}
     */
    public function addDocument(Document $document)
    {
        $document->setAdherant($this);
        $this->documents[] = $document;
    }

    /**
     * @return string
     */
    public function getOptin(): ?string
    {
        return $this->optin;
    }

    /**
     * @param string $optin
     */
    public function setOptin(string $optin)
    {
        $this->optin = $optin;
    }

    /**
     * @return bool
     */
    public function isOptinSubscribed(): bool
    {
        return null !== $this->optin && 'subscribed' === $this->optin;
    }
}
