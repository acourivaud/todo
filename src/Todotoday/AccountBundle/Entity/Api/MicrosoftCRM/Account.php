<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 20/04/17
 * Time: 22:15
 */

namespace Todotoday\AccountBundle\Entity\Api\MicrosoftCRM;

use Actiane\ApiConnectorBundle\Lib\EntityProxyTrait;
use Actiane\ApiConnectorBundle\Lib\Mapping as APIConnector;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class Account
 *
 * @SuppressWarnings(PHPMD)
 * @APIConnector\Entity(path="accounts", repositoryId="todotoday.account.repository.api.account_crm")
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 */
class Account
{
    use EntityProxyTrait;

    /**
     * @var int
     *
     * @APIConnector\Column(name="address2_addresstypecode", type="int")
     * @Serializer\Expose()
     */
    protected $address2Addresstypecode;

    /**
     * @var bool
     *
     * @APIConnector\Column(name="merged", type="bool")
     * @Serializer\Expose()
     */
    protected $merged;

    /**
     * @var string
     *
     * @APIConnector\Column(name="accountnumber", type="string")
     * @Serializer\Expose()
     */
    protected $accountnumber;

    /**
     * @var int
     *
     * @APIConnector\Column(name="statecode", type="int")
     * @Serializer\Expose()
     */
    protected $statecode;

    /**
     * @var float
     *
     * @APIConnector\Column(name="exchangerate", type="float")
     * @Serializer\Expose()
     */
    protected $exchangerate;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_parentaccountid_value", type="string", guid="parentaccountid")
     * @Serializer\Expose()
     */
    protected $parentaccountidValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="name", type="string")
     * @Serializer\Expose()
     */
    protected $name;

    /**
     * @var int
     *
     * @APIConnector\Column(name="opendeals", type="int")
     * @Serializer\Expose()
     */
    protected $opendeals;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_langueadhrent", type="int")
     * @Serializer\Expose()
     */
    protected $apsLangueadhrent;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_owninguser_value", type="string", guid="systemuserid")
     * @Serializer\Expose()
     */
    protected $owninguserValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_primarycontactid_value", type="string", guid="contactid")
     * @Serializer\Expose()
     */
    protected $primarycontactidValue;

    /**
     * @var int
     *
     * @APIConnector\Column(name="importsequencenumber", type="int")
     * @Serializer\Expose()
     */
    protected $importsequencenumber;

    /**
     * @var int
     *
     * @APIConnector\Column(name="openrevenue_state", type="int")
     * @Serializer\Expose()
     */
    protected $openrevenueState;

    /**
     * @var bool
     *
     * @APIConnector\Column(name="donotpostalmail", type="bool")
     * @Serializer\Expose()
     */
    protected $donotpostalmail;

    /**
     * @var bool
     *
     * @APIConnector\Column(name="marketingonly", type="bool")
     * @Serializer\Expose()
     */
    protected $marketingonly;

    /**
     * @var bool
     *
     * @APIConnector\Column(name="donotphone", type="bool")
     * @Serializer\Expose()
     */
    protected $donotphone;

    /**
     * @var int
     *
     * @APIConnector\Column(name="preferredcontactmethodcode", type="int")
     * @Serializer\Expose()
     */
    protected $preferredcontactmethodcode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_ownerid_value", type="string", guid="ownerid")
     * @Serializer\Expose()
     */
    protected $owneridValue;

    /**
     * @var float
     *
     * @APIConnector\Column(name="aps_cadeladherent_base", type="float")
     * @Serializer\Expose()
     */
    protected $apsCadeladherentBase;

    /**
     * @var int
     *
     * @APIConnector\Column(name="customersizecode", type="int")
     * @Serializer\Expose()
     */
    protected $customersizecode;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="openrevenue_date", type="datetimez")
     * @Serializer\Expose()
     */
    protected $openrevenueDate;

    /**
     * @var float
     *
     * @APIConnector\Column(name="openrevenue_base", type="float")
     * @Serializer\Expose()
     */
    protected $openrevenueBase;

    /**
     * @var bool
     *
     * @APIConnector\Column(name="aps_statutvip", type="bool")
     * @Serializer\Expose()
     */
    protected $apsStatutvip;

    /**
     * @var int
     *
     * @APIConnector\Column(name="businesstypecode", type="int")
     * @Serializer\Expose()
     */
    protected $businesstypecode;

    /**
     * @var bool
     *
     * @APIConnector\Column(name="donotemail", type="bool")
     * @Serializer\Expose()
     */
    protected $donotemail;

    /**
     * @var int
     *
     * @APIConnector\Column(name="address2_shippingmethodcode", type="int")
     * @Serializer\Expose()
     */
    protected $address2Shippingmethodcode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="address1_addressid", type="string", guid="yapa")
     * @Serializer\Expose()
     */
    protected $address1Addressid;

    /**
     * @var int
     *
     * @APIConnector\Column(name="address2_freighttermscode", type="int")
     * @Serializer\Expose()
     */
    protected $address2Freighttermscode;

    /**
     * @var int
     *
     * @APIConnector\Column(name="statuscode", type="int")
     * @Serializer\Expose()
     */
    protected $statuscode;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="createdon", type="datetimez")
     * @Serializer\Expose()
     */
    protected $createdon;

    /**
     * @var int
     *
     * @APIConnector\Column(name="opendeals_state", type="int")
     * @Serializer\Expose()
     */
    protected $opendealsState;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="aps_cadeladherent_date", type="datetimez")
     * @Serializer\Expose()
     */
    protected $apsCadeladherentDate;

    /**
     * @var int
     *
     * @APIConnector\Column(name="versionnumber", type="int")
     * @Serializer\Expose()
     */
    protected $versionnumber;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_natureducompte", type="int")
     * @Serializer\Expose()
     */
    protected $apsNatureducompte;

    /**
     * @var float
     *
     * @APIConnector\Column(name="openrevenue", type="float")
     * @Serializer\Expose()
     */
    protected $openrevenue;

    /**
     * @var bool
     *
     * @APIConnector\Column(name="donotsendmm", type="bool")
     * @Serializer\Expose()
     */
    protected $donotsendmm;

    /**
     * @var bool
     *
     * @APIConnector\Column(name="donotfax", type="bool")
     * @Serializer\Expose()
     */
    protected $donotfax;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_customergroup", type="int")
     * @Serializer\Expose()
     */
    protected $apsCustomergroup;

    /**
     * @var bool
     *
     * @APIConnector\Column(name="donotbulkpostalmail", type="bool")
     * @Serializer\Expose()
     */
    protected $donotbulkpostalmail;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="modifiedon", type="datetimez")
     * @Serializer\Expose()
     */
    protected $modifiedon;

    /**
     * @var bool
     *
     * @APIConnector\Column(name="creditonhold", type="bool")
     * @Serializer\Expose()
     */
    protected $creditonhold;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_owningbusinessunit_value", type="string", guid="businessunitid")
     * @Serializer\Expose()
     */
    protected $owningbusinessunitValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_transactioncurrencyid_value", type="string", guid="transactioncurrencyid")
     * @Serializer\Expose()
     */
    protected $transactioncurrencyidValue;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @APIConnector\Column(name="accountid", type="string", guid="id")
     * @Serializer\Expose()
     */
    protected $accountid;

    /**
     * @var bool
     *
     * @APIConnector\Column(name="donotbulkemail", type="bool")
     * @Serializer\Expose()
     */
    protected $donotbulkemail;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_modifiedby_value", type="string", guid="systemuserid")
     * @Serializer\Expose()
     */
    protected $modifiedbyValue;

    /**
     * @var bool
     *
     * @APIConnector\Column(name="followemail", type="bool")
     * @Serializer\Expose()
     */
    protected $followemail;

    /**
     * @var int
     *
     * @APIConnector\Column(name="shippingmethodcode", type="int")
     * @Serializer\Expose()
     */
    protected $shippingmethodcode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_createdby_value", type="string", guid="systemuserid")
     * @Serializer\Expose()
     */
    protected $createdbyValue;

    /**
     * @var int
     *
     * @APIConnector\Column(name="territorycode", type="int")
     * @Serializer\Expose()
     */
    protected $territorycode;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_title", type="int")
     * @Serializer\Expose()
     */
    protected $apsTitle;

    /**
     * @var bool
     *
     * @APIConnector\Column(name="participatesinworkflow", type="bool")
     * @Serializer\Expose()
     */
    protected $participatesinworkflow;

    /**
     * @var int
     *
     * @APIConnector\Column(name="accountclassificationcode", type="int")
     * @Serializer\Expose()
     */
    protected $accountclassificationcode;

    /**
     * @var int
     *
     * @APIConnector\Column(name="accountratingcode", type="int")
     * @Serializer\Expose()
     */
    protected $accountratingcode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="address2_addressid", type="string", guid="yapa")
     * @Serializer\Expose()
     */
    protected $address2Addressid;

    /**
     * @var float
     *
     * @APIConnector\Column(name="aps_cadeladherent", type="float")
     * @Serializer\Expose()
     */
    protected $apsCadeladherent;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_origine", type="int")
     * @Serializer\Expose()
     */
    protected $apsOrigine;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_cadeladherent_state", type="int")
     * @Serializer\Expose()
     */
    protected $apsCadeladherentState;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="opendeals_date", type="datetimez")
     * @Serializer\Expose()
     */
    protected $opendealsDate;

    /**
     * @var string
     * @APIConnector\Column(name="emailaddress1", type="string")
     * @Serializer\Expose()
     */
    protected $emailaddress1;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_onesignalid", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsOnesignalid;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_firstname", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsFirstname;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_lastname", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsLastname;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_middlename", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsMiddlename;

    /**
     * @return int
     */
    public function getAddress2Addresstypecode(): ?int
    {
        return $this->address2Addresstypecode;
    }

    /**
     * @param int $address2Addresstypecode
     *
     * @return Account
     */
    public function setAddress2Addresstypecode(?int $address2Addresstypecode): Account
    {
        return $this->setTrigger('address2Addresstypecode', $address2Addresstypecode);
    }

    /**
     * @return bool
     */
    public function isMerged(): ?bool
    {
        return $this->merged;
    }

    /**
     * @param bool $merged
     *
     * @return Account
     */
    public function setMerged(?bool $merged): Account
    {
        return $this->setTrigger('merged', $merged);
    }

    /**
     * @return string
     */
    public function getAccountnumber(): ?string
    {
        return $this->accountnumber;
    }

    /**
     * @param string $accountnumber
     *
     * @return Account
     */
    public function setAccountnumber(?string $accountnumber): Account
    {
        return $this->setTrigger('accountnumber', $accountnumber);
    }

    /**
     * @return int
     */
    public function getStatecode(): ?int
    {
        return $this->statecode;
    }

    /**
     * @param int $statecode
     *
     * @return Account
     */
    public function setStatecode(?int $statecode): Account
    {
        return $this->setTrigger('statecode', $statecode);
    }

    /**
     * @return float
     */
    public function getExchangerate(): ?float
    {
        return $this->exchangerate;
    }

    /**
     * @param float $exchangerate
     *
     * @return Account
     */
    public function setExchangerate(?float $exchangerate): Account
    {
        return $this->setTrigger('exchangerate', $exchangerate);
    }

    /**
     * @return string
     */
    public function getParentaccountidValue()
    {
        return $this->parentaccountidValue;
    }

    /**
     * @param string $parentaccountidValue
     *
     * @return Account
     */
    public function setParentaccountidValue(?string $parentaccountidValue): Account
    {
        return $this->setTriggerGuid(
            'parentaccountidValue',
            array(
                'target' => 'accounts',
                'value' => $parentaccountidValue,
            )
        );
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Account
     */
    public function setName(?string $name): Account
    {
        return $this->setTrigger('name', $name);
    }

    /**
     * @return int
     */
    public function getOpendeals(): ?int
    {
        return $this->opendeals;
    }

    /**
     * @param int $opendeals
     *
     * @return Account
     */
    public function setOpendeals(?int $opendeals): Account
    {
        return $this->setTrigger('opendeals', $opendeals);
    }

    /**
     * @return int
     */
    public function getApsLangueadhrent(): ?int
    {
        return $this->apsLangueadhrent;
    }

    /**
     * @param int $apsLangueadhrent
     *
     * @return Account
     */
    public function setApsLangueadhrent(?int $apsLangueadhrent): Account
    {
        return $this->setTrigger('apsLangueadhrent', $apsLangueadhrent);
    }

    /**
     * @return string
     */
    public function getOwninguserValue(): ?string
    {
        return $this->owninguserValue;
    }

    /**
     * @param string $owninguserValue
     *
     * @return Account
     */
    public function setOwninguserValue(?string $owninguserValue): Account
    {
        return $this->setTrigger('owninguserValue', $owninguserValue);
    }

    /**
     * @return string
     */
    public function getPrimarycontactidValue(): ?string
    {
        return $this->primarycontactidValue;
    }

    /**
     * @param string $primarycontactidValue
     *
     * @return Account
     */
    public function setPrimarycontactidValue(?string $primarycontactidValue): Account
    {
        return $this->setTrigger('primarycontactidValue', $primarycontactidValue);
    }

    /**
     * @return int
     */
    public function getImportsequencenumber(): ?int
    {
        return $this->importsequencenumber;
    }

    /**
     * @param int $importsequencenumber
     *
     * @return Account
     */
    public function setImportsequencenumber(?int $importsequencenumber): Account
    {
        return $this->setTrigger('importsequencenumber', $importsequencenumber);
    }

    /**
     * @return int
     */
    public function getOpenrevenueState(): ?int
    {
        return $this->openrevenueState;
    }

    /**
     * @param int $openrevenueState
     *
     * @return Account
     */
    public function setOpenrevenueState(?int $openrevenueState): Account
    {
        return $this->setTrigger('openrevenueState', $openrevenueState);
    }

    /**
     * @return bool
     */
    public function isDonotpostalmail(): ?bool
    {
        return $this->donotpostalmail;
    }

    /**
     * @param bool $donotpostalmail
     *
     * @return Account
     */
    public function setDonotpostalmail(?bool $donotpostalmail): Account
    {
        return $this->setTrigger('donotpostalmail', $donotpostalmail);
    }

    /**
     * @return bool
     */
    public function isMarketingonly(): ?bool
    {
        return $this->marketingonly;
    }

    /**
     * @param bool $marketingonly
     *
     * @return Account
     */
    public function setMarketingonly(?bool $marketingonly): Account
    {
        return $this->setTrigger('marketingonly', $marketingonly);
    }

    /**
     * @return bool
     */
    public function isDonotphone(): ?bool
    {
        return $this->donotphone;
    }

    /**
     * @param bool $donotphone
     *
     * @return Account
     */
    public function setDonotphone(?bool $donotphone): Account
    {
        return $this->setTrigger('donotphone', $donotphone);
    }

    /**
     * @return int
     */
    public function getPreferredcontactmethodcode(): ?int
    {
        return $this->preferredcontactmethodcode;
    }

    /**
     * @param int $preferredcontactmethodcode
     *
     * @return Account
     */
    public function setPreferredcontactmethodcode(?int $preferredcontactmethodcode): Account
    {
        return $this->setTrigger('preferredcontactmethodcode', $preferredcontactmethodcode);
    }

    /**
     * @return string|array
     */
    public function getOwneridValue()
    {
        return $this->owneridValue;
    }

    /**
     * @param string $owneridValue
     *
     * @return Account
     */
    public function setOwneridValue(?string $owneridValue): Account
    {
        return $this->setTrigger('owneridValue', $owneridValue);
    }

    /**
     * @param null|string $owneridValue
     *
     * @return Account
     */
    public function setOwnerIdValueGuid(?string $owneridValue): Account
    {
        return $this->setTriggerGuid(
            'owneridValue',
            array(
                'target' => 'teams',
                'value' => $owneridValue,
            )
        );
    }

    /**
     * @return float
     */
    public function getApsCadeladherentBase(): ?float
    {
        return $this->apsCadeladherentBase;
    }

    /**
     * @param float $apsCadeladherentBase
     *
     * @return Account
     */
    public function setApsCadeladherentBase(?float $apsCadeladherentBase): Account
    {
        return $this->setTrigger('apsCadeladherentBase', $apsCadeladherentBase);
    }

    /**
     * @return int
     */
    public function getCustomersizecode(): ?int
    {
        return $this->customersizecode;
    }

    /**
     * @param int $customersizecode
     *
     * @return Account
     */
    public function setCustomersizecode(?int $customersizecode): Account
    {
        return $this->setTrigger('customersizecode', $customersizecode);
    }

    /**
     * @return \DateTime
     */
    public function getOpenrevenueDate(): ?\DateTime
    {
        return $this->openrevenueDate;
    }

    /**
     * @param \DateTime $openrevenueDate
     *
     * @return Account
     */
    public function setOpenrevenueDate(?\DateTime $openrevenueDate): Account
    {
        return $this->setTrigger('openrevenueDate', $openrevenueDate);
    }

    /**
     * @return float
     */
    public function getOpenrevenueBase(): ?float
    {
        return $this->openrevenueBase;
    }

    /**
     * @param float $openrevenueBase
     *
     * @return Account
     */
    public function setOpenrevenueBase(?float $openrevenueBase): Account
    {
        return $this->setTrigger('openrevenueBase', $openrevenueBase);
    }

    /**
     * @return bool
     */
    public function isApsStatutvip(): ?bool
    {
        return $this->apsStatutvip;
    }

    /**
     * @param bool $apsStatutvip
     *
     * @return Account
     */
    public function setApsStatutvip(?bool $apsStatutvip): Account
    {
        return $this->setTrigger('apsStatutvip', $apsStatutvip);
    }

    /**
     * @return int
     */
    public function getBusinesstypecode(): ?int
    {
        return $this->businesstypecode;
    }

    /**
     * @param int $businesstypecode
     *
     * @return Account
     */
    public function setBusinesstypecode(?int $businesstypecode): Account
    {
        return $this->setTrigger('businesstypecode', $businesstypecode);
    }

    /**
     * @return bool
     */
    public function isDonotemail(): ?bool
    {
        return $this->donotemail;
    }

    /**
     * @param bool $donotemail
     *
     * @return Account
     */
    public function setDonotemail(?bool $donotemail): Account
    {
        return $this->setTrigger('donotemail', $donotemail);
    }

    /**
     * @return int
     */
    public function getAddress2Shippingmethodcode(): ?int
    {
        return $this->address2Shippingmethodcode;
    }

    /**
     * @param int $address2Shippingmethodcode
     *
     * @return Account
     */
    public function setAddress2Shippingmethodcode(?int $address2Shippingmethodcode): Account
    {
        return $this->setTrigger('address2Shippingmethodcode', $address2Shippingmethodcode);
    }

    /**
     * @return string
     */
    public function getAddress1Addressid(): ?string
    {
        return $this->address1Addressid;
    }

    /**
     * @param string $address1Addressid
     *
     * @return Account
     */
    public function setAddress1Addressid(?string $address1Addressid): Account
    {
        return $this->setTrigger('address1Addressid', $address1Addressid);
    }

    /**
     * @return int
     */
    public function getAddress2Freighttermscode(): ?int
    {
        return $this->address2Freighttermscode;
    }

    /**
     * @param int $address2Freighttermscode
     *
     * @return Account
     */
    public function setAddress2Freighttermscode(?int $address2Freighttermscode): Account
    {
        return $this->setTrigger('address2Freighttermscode', $address2Freighttermscode);
    }

    /**
     * @return int
     */
    public function getStatuscode(): ?int
    {
        return $this->statuscode;
    }

    /**
     * @param int $statuscode
     *
     * @return Account
     */
    public function setStatuscode(?int $statuscode): Account
    {
        return $this->setTrigger('statuscode', $statuscode);
    }

    /**
     * @return \DateTime
     */
    public function getCreatedon(): ?\DateTime
    {
        return $this->createdon;
    }

    /**
     * @param \DateTime $createdon
     *
     * @return Account
     */
    public function setCreatedon(?\DateTime $createdon): Account
    {
        return $this->setTrigger('createdon', $createdon);
    }

    /**
     * @return int
     */
    public function getOpendealsState(): ?int
    {
        return $this->opendealsState;
    }

    /**
     * @param int $opendealsState
     *
     * @return Account
     */
    public function setOpendealsState(?int $opendealsState): Account
    {
        return $this->setTrigger('opendealsState', $opendealsState);
    }

    /**
     * @return \DateTime
     */
    public function getApsCadeladherentDate(): ?\DateTime
    {
        return $this->apsCadeladherentDate;
    }

    /**
     * @param \DateTime $apsCadeladherentDate
     *
     * @return Account
     */
    public function setApsCadeladherentDate(?\DateTime $apsCadeladherentDate): Account
    {
        return $this->setTrigger('apsCadeladherentDate', $apsCadeladherentDate);
    }

    /**
     * @return int
     */
    public function getVersionnumber(): ?int
    {
        return $this->versionnumber;
    }

    /**
     * @param int $versionnumber
     *
     * @return Account
     */
    public function setVersionnumber(?int $versionnumber): Account
    {
        return $this->setTrigger('versionnumber', $versionnumber);
    }

    /**
     * @return int
     */
    public function getApsNatureducompte(): ?int
    {
        return $this->apsNatureducompte;
    }

    /**
     * @param int $apsNatureducompte
     *
     * @return Account
     */
    public function setApsNatureducompte(?int $apsNatureducompte): Account
    {
        return $this->setTrigger('apsNatureducompte', $apsNatureducompte);
    }

    /**
     * @return float
     */
    public function getOpenrevenue(): ?float
    {
        return $this->openrevenue;
    }

    /**
     * @param float $openrevenue
     *
     * @return Account
     */
    public function setOpenrevenue(?float $openrevenue): Account
    {
        return $this->setTrigger('openrevenue', $openrevenue);
    }

    /**
     * @return bool
     */
    public function isDonotsendmm(): ?bool
    {
        return $this->donotsendmm;
    }

    /**
     * @param bool $donotsendmm
     *
     * @return Account
     */
    public function setDonotsendmm(?bool $donotsendmm): Account
    {
        return $this->setTrigger('donotsendmm', $donotsendmm);
    }

    /**
     * @return bool
     */
    public function isDonotfax(): ?bool
    {
        return $this->donotfax;
    }

    /**
     * @param bool $donotfax
     *
     * @return Account
     */
    public function setDonotfax(?bool $donotfax): Account
    {
        return $this->setTrigger('donotfax', $donotfax);
    }

    /**
     * @return int
     */
    public function getApsCustomergroup(): ?int
    {
        return $this->apsCustomergroup;
    }

    /**
     * @param int $apsCustomergroup
     *
     * @return Account
     */
    public function setApsCustomergroup(?int $apsCustomergroup): Account
    {
        return $this->setTrigger('apsCustomergroup', $apsCustomergroup);
    }

    /**
     * @return bool
     */
    public function isDonotbulkpostalmail(): ?bool
    {
        return $this->donotbulkpostalmail;
    }

    /**
     * @param bool $donotbulkpostalmail
     *
     * @return Account
     */
    public function setDonotbulkpostalmail(?bool $donotbulkpostalmail): Account
    {
        return $this->setTrigger('donotbulkpostalmail', $donotbulkpostalmail);
    }

    /**
     * @return \DateTime
     */
    public function getModifiedon(): ?\DateTime
    {
        return $this->modifiedon;
    }

    /**
     * @param \DateTime $modifiedon
     *
     * @return Account
     */
    public function setModifiedon(?\DateTime $modifiedon): Account
    {
        return $this->setTrigger('modifiedon', $modifiedon);
    }

    /**
     * @return bool
     */
    public function isCreditonhold(): ?bool
    {
        return $this->creditonhold;
    }

    /**
     * @param bool $creditonhold
     *
     * @return Account
     */
    public function setCreditonhold(?bool $creditonhold): Account
    {
        return $this->setTrigger('creditonhold', $creditonhold);
    }

    /**
     * @return string
     */
    public function getOwningbusinessunitValue(): ?string
    {
        return $this->owningbusinessunitValue;
    }

    /**
     * @param string $owningbusinessunitValue
     *
     * @return Account
     */
    public function setOwningbusinessunitValue(?string $owningbusinessunitValue): Account
    {
        return $this->setTrigger('owningbusinessunitValue', $owningbusinessunitValue);
    }

    /**
     * @return string
     */
    public function getTransactioncurrencyidValue(): ?string
    {
        return $this->transactioncurrencyidValue;
    }

    /**
     * @param string $transactioncurrencyidValue
     *
     * @return Account
     */
    public function setTransactioncurrencyidValue(?string $transactioncurrencyidValue): Account
    {
        return $this->setTrigger('transactioncurrencyidValue', $transactioncurrencyidValue);
    }

    /**
     * @return string
     */
    public function getAccountid(): ?string
    {
        return $this->accountid;
    }

    /**
     * @param string $accountid
     *
     * @return Account
     */
    public function setAccountid(?string $accountid): Account
    {
        return $this->setTrigger('accountid', $accountid);
    }

    /**
     * @return bool
     */
    public function isDonotbulkemail(): ?bool
    {
        return $this->donotbulkemail;
    }

    /**
     * @param bool $donotbulkemail
     *
     * @return Account
     */
    public function setDonotbulkemail(?bool $donotbulkemail): Account
    {
        return $this->setTrigger('donotbulkemail', $donotbulkemail);
    }

    /**
     * @return string
     */
    public function getModifiedbyValue(): ?string
    {
        return $this->modifiedbyValue;
    }

    /**
     * @param string $modifiedbyValue
     *
     * @return Account
     */
    public function setModifiedbyValue(?string $modifiedbyValue): Account
    {
        return $this->setTrigger('modifiedbyValue', $modifiedbyValue);
    }

    /**
     * @return bool
     */
    public function isFollowemail(): ?bool
    {
        return $this->followemail;
    }

    /**
     * @param bool $followemail
     *
     * @return Account
     */
    public function setFollowemail(?bool $followemail): Account
    {
        return $this->setTrigger('followemail', $followemail);
    }

    /**
     * @return int
     */
    public function getShippingmethodcode(): ?int
    {
        return $this->shippingmethodcode;
    }

    /**
     * @param int $shippingmethodcode
     *
     * @return Account
     */
    public function setShippingmethodcode(?int $shippingmethodcode): Account
    {
        return $this->setTrigger('shippingmethodcode', $shippingmethodcode);
    }

    /**
     * @return string
     */
    public function getCreatedbyValue(): ?string
    {
        return $this->createdbyValue;
    }

    /**
     * @param string $createdbyValue
     *
     * @return Account
     */
    public function setCreatedbyValue(?string $createdbyValue): Account
    {
        return $this->setTrigger('createdbyValue', $createdbyValue);
    }

    /**
     * @return int
     */
    public function getTerritorycode(): ?int
    {
        return $this->territorycode;
    }

    /**
     * @param int $territorycode
     *
     * @return Account
     */
    public function setTerritorycode(?int $territorycode): Account
    {
        return $this->setTrigger('territorycode', $territorycode);
    }

    /**
     * @return int
     */
    public function getApsTitle(): ?int
    {
        return $this->apsTitle;
    }

    /**
     * @param int $apsTitle
     *
     * @return Account
     */
    public function setApsTitle(?int $apsTitle): Account
    {
        return $this->setTrigger('apsTitle', $apsTitle);
    }

    /**
     * @return bool
     */
    public function isParticipatesinworkflow(): ?bool
    {
        return $this->participatesinworkflow;
    }

    /**
     * @param bool $participatesinworkflow
     *
     * @return Account
     */
    public function setParticipatesinworkflow(?bool $participatesinworkflow): Account
    {
        return $this->setTrigger('participatesinworkflow', $participatesinworkflow);
    }

    /**
     * @return int
     */
    public function getAccountclassificationcode(): ?int
    {
        return $this->accountclassificationcode;
    }

    /**
     * @param int $accountclassificationcode
     *
     * @return Account
     */
    public function setAccountclassificationcode(?int $accountclassificationcode): Account
    {
        return $this->setTrigger('accountclassificationcode', $accountclassificationcode);
    }

    /**
     * @return int
     */
    public function getAccountratingcode(): ?int
    {
        return $this->accountratingcode;
    }

    /**
     * @param int $accountratingcode
     *
     * @return Account
     */
    public function setAccountratingcode(?int $accountratingcode): Account
    {
        return $this->setTrigger('accountratingcode', $accountratingcode);
    }

    /**
     * @return string
     */
    public function getAddress2Addressid(): ?string
    {
        return $this->address2Addressid;
    }

    /**
     * @param string $address2Addressid
     *
     * @return Account
     */
    public function setAddress2Addressid(?string $address2Addressid): Account
    {
        return $this->setTrigger('address2Addressid', $address2Addressid);
    }

    /**
     * @return float
     */
    public function getApsCadeladherent(): ?float
    {
        return $this->apsCadeladherent;
    }

    /**
     * @param float $apsCadeladherent
     *
     * @return Account
     */
    public function setApsCadeladherent(?float $apsCadeladherent): Account
    {
        return $this->setTrigger('apsCadeladherent', $apsCadeladherent);
    }

    /**
     * @return int
     */
    public function getApsOrigine(): ?int
    {
        return $this->apsOrigine;
    }

    /**
     * @param int $apsOrigine
     *
     * @return Account
     */
    public function setApsOrigine(?int $apsOrigine): Account
    {
        return $this->setTrigger('apsOrigine', $apsOrigine);
    }

    /**
     * @return int
     */
    public function getApsCadeladherentState(): ?int
    {
        return $this->apsCadeladherentState;
    }

    /**
     * @param int $apsCadeladherentState
     *
     * @return Account
     */
    public function setApsCadeladherentState(?int $apsCadeladherentState): Account
    {
        return $this->setTrigger('apsCadeladherentState', $apsCadeladherentState);
    }

    /**
     * @return \DateTime
     */
    public function getOpendealsDate(): ?\DateTime
    {
        return $this->opendealsDate;
    }

    /**
     * @param \DateTime $opendealsDate
     *
     * @return Account
     */
    public function setOpendealsDate(?\DateTime $opendealsDate): Account
    {
        return $this->setTrigger('opendealsDate', $opendealsDate);
    }

    /**
     * @return string
     */
    public function getEmailaddress1(): ?string
    {
        return $this->emailaddress1;
    }

    /**
     * @param string $emailaddress1
     *
     * @return Account
     */
    public function setEmailaddress1(?string $emailaddress1): Account
    {
        return $this->setTrigger('emailaddress1', $emailaddress1);
    }

    /**
     * @return string|null
     */
    public function getApsOnesignalid(): ?string
    {
        return $this->apsOnesignalid;
    }

    /**
     * @param string $apsOnesignalid
     *
     * @return Account
     */
    public function setApsOnesignalid(?string $apsOnesignalid): Account
    {
        return $this->setTrigger('apsOnesignalid', $apsOnesignalid);
    }

    /**
     * @return string
     */
    public function getApsFirstname(): ?string
    {
        return $this->apsFirstname;
    }

    /**
     * @param string $apsFirstname
     *
     * @return Account
     */
    public function setApsFirstname(?string $apsFirstname): Account
    {
        return $this->setTrigger('apsFirstname', $apsFirstname);
    }

    /**
     * @return string
     */
    public function getApsLastname(): ?string
    {
        return $this->apsLastname;
    }

    /**
     * @param string $apsLastname
     *
     * @return Account
     */
    public function setApsLastname(?string $apsLastname): Account
    {
        return $this->setTrigger('apsLastname', $apsLastname);
    }

    /**
     * @return string
     */
    public function getApsMiddlename(): ?string
    {
        return $this->apsMiddlename;
    }

    /**
     * @param string $apsMiddlename
     *
     * @return Account
     */
    public function setApsMiddlename(?string $apsMiddlename): Account
    {
        return $this->setTrigger('apsMiddlename', $apsMiddlename);
    }
}
