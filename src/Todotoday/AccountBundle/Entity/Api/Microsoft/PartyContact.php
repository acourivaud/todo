<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 13/03/2017
 * Time: 13:40
 */

namespace Todotoday\AccountBundle\Entity\Api\Microsoft;

use Actiane\ApiConnectorBundle\Lib\EntityProxyTrait;
use Actiane\ApiConnectorBundle\Lib\Mapping as APIConnector;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class PartyContact
 *
 * @APIConnector\Entity(path="PartyContacts", repositoryId="todotoday.account.repository.api.party_contact")
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Entity\Api\Microsoft
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class PartyContact
{
    use EntityProxyTrait;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @Serializer\Expose()
     * @APIConnector\Column(name="Locator", type="string")
     */
    protected $locator;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @Serializer\Expose()
     * @APIConnector\Column(name="LocationId", type="string")
     */
    protected $locationId;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @Serializer\Expose()
     * @APIConnector\Column(name="PartyNumber", type="string")
     */
    protected $partyNumber;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="Purpose", type="string")
     */
    protected $purpose;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @Serializer\Expose()
     * @APIConnector\Column(name="Description", type="string")
     */
    protected $description;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="CountryRegionCode", type="string")
     */
    protected $countryRegionCode;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="IsPrivate", type="string")
     */
    protected $isPrivate;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="IsMobilePhone", type="string")
     */
    protected $isMobilePhone;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="IsPrimary", type="string")
     */
    protected $isPrimary;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="LocatorExtension", type="string")
     */
    protected $locatorExtension;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @Serializer\Expose()
     * @APIConnector\Column(
     *     name="Type", type="string", mType="Microsoft.Dynamics.DataEntities.LogisticsElectronicAddressMethodType"
     *     )
     */
    protected $type;

    /**
     * Get countryRegionCode
     *
     * @return string
     */
    public function getCountryRegionCode(): ?string
    {
        return $this->countryRegionCode;
    }

    /**
     * Set countryRegionCode
     *
     * @param string $countryRegionCode
     *
     * @return PartyContact
     */
    public function setCountryRegionCode(?string $countryRegionCode): PartyContact
    {
        return $this->setTrigger('countryRegionCode', $countryRegionCode);
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return PartyContact
     */
    public function setDescription(?string $description): PartyContact
    {
        return $this->setTrigger('description', $description);
    }

    /**
     * Get isMobilePhone
     *
     * @return string
     */
    public function getIsMobilePhone(): ?string
    {
        return $this->isMobilePhone;
    }

    /**
     * Set isMobilePhone
     *
     * @param string $isMobilePhone
     *
     * @return PartyContact
     */
    public function setIsMobilePhone(?string $isMobilePhone): PartyContact
    {
        return $this->setTrigger('isMobilePhone', $isMobilePhone);
    }

    /**
     * Get isPrimary
     *
     * @return string
     */
    public function getIsPrimary(): ?string
    {
        return $this->isPrimary;
    }

    /**
     * Set isPrimary
     *
     * @param string $isPrimary
     *
     * @return PartyContact
     */
    public function setIsPrimary(?string $isPrimary): PartyContact
    {
        return $this->setTrigger('isPrimary', $isPrimary);
    }

    /**
     * Get isPrivate
     *
     * @return string
     */
    public function getIsPrivate(): ?string
    {
        return $this->isPrivate;
    }

    /**
     * Set isPrivate
     *
     * @param string $isPrivate
     *
     * @return PartyContact
     */
    public function setIsPrivate(?string $isPrivate): PartyContact
    {
        return $this->setTrigger('isPrivate', $isPrivate);
    }

    /**
     * Get locationId
     *
     * @return string
     */
    public function getLocationId(): ?string
    {
        return $this->locationId;
    }

    /**
     * Set locationId
     *
     * @param string $locationId
     *
     * @return PartyContact
     */
    public function setLocationId(?string $locationId): PartyContact
    {
        return $this->setTrigger('locationId', $locationId);
    }

    /**
     * Get locator
     *
     * @return string
     */
    public function getLocator(): ?string
    {
        return $this->locator;
    }

    /**
     * Set locator
     *
     * @param string $locator
     *
     * @return PartyContact
     */
    public function setLocator(?string $locator): PartyContact
    {
        return $this->setTrigger('locator', $locator);
    }

    /**
     * Get locatorExtension
     *
     * @return string
     */
    public function getLocatorExtension(): ?string
    {
        return $this->locatorExtension;
    }

    /**
     * Set locatorExtension
     *
     * @param string $locatorExtension
     *
     * @return PartyContact
     */
    public function setLocatorExtension(?string $locatorExtension): PartyContact
    {
        return $this->setTrigger('locatorExtension', $locatorExtension);
    }

    /**
     * Get partyNumber
     *
     * @return string
     */
    public function getPartyNumber(): ?string
    {
        return $this->partyNumber;
    }

    /**
     * Set partyNumber
     *
     * @param string $partyNumber
     *
     * @return PartyContact
     */
    public function setPartyNumber(?string $partyNumber): PartyContact
    {
        return $this->setTrigger('partyNumber', $partyNumber);
    }

    /**
     * Get purpose
     *
     * @return string
     */
    public function getPurpose(): ?string
    {
        return $this->purpose;
    }

    /**
     * Set purpose
     *
     * @param string $purpose
     *
     * @return PartyContact
     */
    public function setPurpose(?string $purpose): PartyContact
    {
        return $this->setTrigger('purpose', $purpose);
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return PartyContact
     */
    public function setType(?string $type): PartyContact
    {
        return $this->setTrigger('type', $type);
    }
}
