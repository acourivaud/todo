<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 21/03/2017
 * Time: 16:31
 */

namespace Todotoday\AccountBundle\Entity\Api\Microsoft;

use Actiane\ApiConnectorBundle\Lib\EntityProxyTrait;
use Actiane\ApiConnectorBundle\Lib\Mapping as APIConnector;

/**
 * Class MandatoryField
 *
 * @APIConnector\Entity(path="MandatoryFields", repositoryId="todotoday.account.repository.api.mandatory_field")
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Entity\Api\Microsoft
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class MandatoryField
{
    use EntityProxyTrait;

    /**
     * @var string
     *
     * @APIConnector\Column(name="IsMandatory", type="string", mType="Microsoft.Dynamics.DataEntities.NoYes")
     */
    protected $isMandatory;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @APIConnector\Column(name="RetailChannelId", type="string")
     */
    protected $retailChannelId;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @APIConnector\Column(name="dataAreaId", type="string")
     */
    protected $dataAreaId;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @APIConnector\Column(
     *     name="InterfaceName", type="string", mType="Microsoft.Dynamics.DataEntities.TDTDInterfaceEnum"
     * )
     */
    protected $interfaceName;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @APIConnector\Column(name="FieldName", type="string")
     */
    protected $fieldName;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @APIConnector\Column(name="TypeValue", type="string")
     */
    protected $typeValue;

    /**
     * Get dataAreaId
     *
     * @return string
     */
    public function getDataAreaId(): ?string
    {
        return $this->dataAreaId;
    }

    /**
     * Set dataAreaId
     *
     * @param string $dataAreaId
     *
     * @return MandatoryField
     */
    public function setDataAreaId(?string $dataAreaId): self
    {
        return $this->setTrigger('dataAreaId', $dataAreaId);
    }

    /**
     * Get fieldName
     *
     * @return string
     */
    public function getFieldName(): ?string
    {
        return $this->fieldName;
    }

    /**
     * Set fieldName
     *
     * @param string $fieldName
     *
     * @return MandatoryField
     */
    public function setFieldName(?string $fieldName): self
    {
        return $this->setTrigger('fieldName', $fieldName);
    }

    /**
     * Get interfaceName
     *
     * @return string
     */
    public function getInterfaceName(): ?string
    {
        return $this->interfaceName;
    }

    /**
     * Set interfaceName
     *
     * @param string $interfaceName
     *
     * @return MandatoryField
     */
    public function setInterfaceName(?string $interfaceName): self
    {
        return $this->setTrigger('interfaceName', $interfaceName);
    }

    /**
     * Get isMandatory
     *
     * @return string
     */
    public function getIsMandatory(): ?string
    {
        return $this->isMandatory;
    }

    /**
     * Set isMandatory
     *
     * @param string $isMandatory
     *
     * @return MandatoryField
     */
    public function setIsMandatory(?string $isMandatory): self
    {
        return $this->setTrigger('isMandatory', $isMandatory);
    }

    /**
     * Get retailChannelId
     *
     * @return string
     */
    public function getRetailChannelId(): ?string
    {
        return $this->retailChannelId;
    }

    /**
     * Set retailChannelId
     *
     * @param string $retailChannelId
     *
     * @return MandatoryField
     */
    public function setRetailChannelId(?string $retailChannelId): self
    {
        return $this->setTrigger('retailChannelId', $retailChannelId);
    }

    /**
     * Get typeValue
     *
     * @return string
     */
    public function getTypeValue(): ?string
    {
        return $this->typeValue;
    }

    /**
     * Set typeValue
     *
     * @param string $typeValue
     *
     * @return MandatoryField
     */
    public function setTypeValue(?string $typeValue): self
    {
        return $this->setTrigger('typeValue', $typeValue);
    }
}
