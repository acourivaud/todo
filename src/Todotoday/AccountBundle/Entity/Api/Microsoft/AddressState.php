<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 17/06/17
 * Time: 16:29
 */

namespace Todotoday\AccountBundle\Entity\Api\Microsoft;

use Actiane\ApiConnectorBundle\Lib\EntityProxyTrait;
use Actiane\ApiConnectorBundle\Lib\Mapping as APIConnector;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class AddressState
 * @package Todotoday\AccountBundle\Entity\Api\Microsoft
 * @APIConnector\Entity(path="AddressStates", repositoryId="todotoday.account.repository.api.address_state")
 */
class AddressState
{
    use EntityProxyTrait;

    /**
     * @var string
     *
     * @APIConnector\Column(name="State", type="string")
     * @Serializer\Expose()
     * @APIConnector\Id()
     * @Serializer\Type("string")
     */
    protected $state;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CountryRegionId", type="string")
     * @Serializer\Expose()
     * @APIConnector\Id()
     * @Serializer\Type("string")
     */
    protected $countryRegionId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="Name", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $name;

    /**
     * @var string
     *
     * @APIConnector\Column(name="TimeZone", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $timeZone;

    /**
     * @var string
     *
     * @APIConnector\Column(name="BrazilStateCode", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $brazilStateCode;

    /**
     * @return string
     */
    public function getState(): ?string
    {
        return $this->state;
    }

    /**
     * @param string $state
     *
     * @return AddressState
     */
    public function setState(?string $state): AddressState
    {
        return $this->setTrigger('state', $state);
    }

    /**
     * @return string
     */
    public function getCountryRegionId(): ?string
    {
        return $this->countryRegionId;
    }

    /**
     * @param string $countryRegionId
     *
     * @return AddressState
     */
    public function setCountryRegionId(?string $countryRegionId): AddressState
    {
        return $this->setTrigger('countryRegionId', $countryRegionId);
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return AddressState
     */
    public function setName(?string $name): AddressState
    {
        return $this->setTrigger('name', $name);
    }

    /**
     * @return string
     */
    public function getTimeZone(): ?string
    {
        return $this->timeZone;
    }

    /**
     * @param string $timeZone
     *
     * @return AddressState
     */
    public function setTimeZone(?string $timeZone): AddressState
    {
        return $this->setTrigger('timeZone', $timeZone);
    }

    /**
     * @return string
     */
    public function getBrazilStateCode(): ?string
    {
        return $this->brazilStateCode;
    }

    /**
     * @param string $brazilStateCode
     *
     * @return AddressState
     */
    public function setBrazilStateCode(?string $brazilStateCode): AddressState
    {
        return $this->setTrigger('brazilStateCode', $brazilStateCode);
    }
}
