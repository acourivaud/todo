<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 21/04/2017
 * Time: 08:41
 */

namespace Todotoday\AccountBundle\Entity\Api\Microsoft;

use Actiane\ApiConnectorBundle\Lib\EntityProxyTrait;
use Actiane\ApiConnectorBundle\Lib\Mapping as APIConnector;

/**
 * Class VehicleModel
 *
 * @APIConnector\Entity(path="VehicleModels", repositoryId="")
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle\Entity\Api\Microsoft
 * @subpackage Todotoday\AccountBundle\Entity\Api\Microsoft
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class VehicleModel
{
    use EntityProxyTrait;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ImageFormat", type="string")
     */
    protected $imageFormat;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ModelSpecification", type="string")
     */
    protected $modelSpecification;

    /**
     * @var int
     *
     * @APIConnector\Column(name="ModelYear", type="int", required=true)
     */
    protected $modelYear;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @APIConnector\Column(name="Model", type="string", required=true)
     */
    protected $model;

    /**
     * @var int
     *
     * @APIConnector\Column(name="VehicleModelId", type="int", required=true)
     */
    protected $vehicleModelId;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @APIConnector\Column(name="VehicleMake_Make", type="string", required=true)
     */
    protected $vehicleMakeMake;

    /**
     * @var mixed
     * @TODO: attend un binary, donc aucune idée de quoi faire pour ça.
     *
     * @APIConnector\Column(name="VehicleImage", type="string")
     */
    protected $vehicleImage;

    /**
     * @var int
     *
     * @APIConnector\Column(name="VehicleCount", type="int")
     */
    protected $vehicleCount;

    /**
     * Get imageFormat
     *
     * @return string
     */
    public function getImageFormat(): string
    {
        return $this->imageFormat;
    }

    /**
     * Set imageFormat
     *
     * @param string $imageFormat
     *
     * @return VehicleModel
     */
    public function setImageFormat(string $imageFormat): VehicleModel
    {
        $this->imageFormat = $imageFormat;

        return $this;
    }

    /**
     * Get model
     *
     * @return string
     */
    public function getModel(): string
    {
        return $this->model;
    }

    /**
     * Set model
     *
     * @param string $model
     *
     * @return VehicleModel
     */
    public function setModel(string $model): VehicleModel
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Get modelSpecification
     *
     * @return string
     */
    public function getModelSpecification(): string
    {
        return $this->modelSpecification;
    }

    /**
     * Set modelSpecification
     *
     * @param string $modelSpecification
     *
     * @return VehicleModel
     */
    public function setModelSpecification(string $modelSpecification): VehicleModel
    {
        $this->modelSpecification = $modelSpecification;

        return $this;
    }

    /**
     * Get modelYear
     *
     * @return int
     */
    public function getModelYear(): int
    {
        return $this->modelYear;
    }

    /**
     * Set modelYear
     *
     * @param int $modelYear
     *
     * @return VehicleModel
     */
    public function setModelYear(int $modelYear): VehicleModel
    {
        $this->modelYear = $modelYear;

        return $this;
    }

    /**
     * Get vehicleCount
     *
     * @return int
     */
    public function getVehicleCount(): int
    {
        return $this->vehicleCount;
    }

    /**
     * Set vehicleCount
     *
     * @param int $vehicleCount
     *
     * @return VehicleModel
     */
    public function setVehicleCount(int $vehicleCount): VehicleModel
    {
        $this->vehicleCount = $vehicleCount;

        return $this;
    }

    /**
     * Get vehicleImage
     *
     * @return mixed
     */
    public function getVehicleImage()
    {
        return $this->vehicleImage;
    }

    /**
     * Set vehicleImage
     *
     * @param mixed $vehicleImage
     *
     * @return VehicleModel
     */
    public function setVehicleImage($vehicleImage): VehicleModel
    {
        $this->vehicleImage = $vehicleImage;

        return $this;
    }

    /**
     * Get vehicleMakeMake
     *
     * @return string
     */
    public function getVehicleMakeMake(): string
    {
        return $this->vehicleMakeMake;
    }

    /**
     * Set vehicleMakeMake
     *
     * @param string $vehicleMakeMake
     *
     * @return VehicleModel
     */
    public function setVehicleMakeMake(string $vehicleMakeMake): VehicleModel
    {
        $this->vehicleMakeMake = $vehicleMakeMake;

        return $this;
    }

    /**
     * Get vehicleModelId
     *
     * @return int
     */
    public function getVehicleModelId(): int
    {
        return $this->vehicleModelId;
    }

    /**
     * Set vehicleModelId
     *
     * @param int $vehicleModelId
     *
     * @return VehicleModel
     */
    public function setVehicleModelId(int $vehicleModelId): VehicleModel
    {
        $this->vehicleModelId = $vehicleModelId;

        return $this;
    }
}
