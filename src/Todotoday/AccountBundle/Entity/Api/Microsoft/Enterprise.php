<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 09/06/17
 * Time: 15:15
 */

namespace Todotoday\AccountBundle\Entity\Api\Microsoft;

use Actiane\ApiConnectorBundle\Lib\EntityProxyTrait;
use Actiane\ApiConnectorBundle\Lib\Mapping as APIConnector;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class Enterprise
 * @package Todotoday\AccountBundle\Entity\Api\Microsoft
 * @APIConnector\Entity(path="Entreprises", repositoryId="todotoday.account.repository.api.enterprise")
 *
 */
class Enterprise
{
    use EntityProxyTrait;

    /**
     * @var string
     *
     * @APIConnector\Column(name="Value", type="string")
     * @Serializer\Expose()
     * @APIConnector\Id()
     * @Serializer\Type("string")
     */
    protected $value;

    /**
     * @var string
     *
     * @APIConnector\Column(name="IdActiane", type="string")
     * @Serializer\Expose()
     * @APIConnector\Id()
     * @Serializer\Type("string")
     */
    protected $idActiane;

    /**
     * @var string
     *
     * @APIConnector\Column(name="dataAreaId", type="string")
     * @Serializer\Expose()
     * @APIConnector\Id()
     * @Serializer\Type("string")
     */
    protected $dataAreaId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="Description", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $description;

    /**
     * @return string
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string $value
     *
     * @return Enterprise
     */
    public function setValue(?string $value): Enterprise
    {
        return $this->setTrigger('value', $value);
    }

    /**
     * @return string
     */
    public function getIdActiane(): ?string
    {
        return $this->idActiane;
    }

    /**
     * @param string $idActiane
     *
     * @return Enterprise
     */
    public function setIdActiane(?string $idActiane): Enterprise
    {
        return $this->setTrigger('idActiane', $idActiane);
    }

    /**
     * @return string
     */
    public function getDataAreaId(): ?string
    {
        return $this->dataAreaId;
    }

    /**
     * @param string $dataAreaId
     *
     * @return Enterprise
     */
    public function setDataAreaId(?string $dataAreaId): Enterprise
    {
        return $this->setTrigger('dataAreaId', $dataAreaId);
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return Enterprise
     */
    public function setDescription(?string $description): Enterprise
    {
        return $this->setTrigger('description', $description);
    }
}
