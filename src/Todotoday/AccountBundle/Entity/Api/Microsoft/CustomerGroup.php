<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 27/02/2017
 * Time: 12:14
 */

namespace Todotoday\AccountBundle\Entity\Api\Microsoft;

use Actiane\ApiConnectorBundle\Lib\EntityProxyTrait;
use Actiane\ApiConnectorBundle\Lib\Mapping as APIConnector;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class CustomerGroup
 *
 * @APIConnector\Entity(path="CustomerGroups", repositoryId="todotoday.account.repository.api.customer_group")
 * @Serializer\ExclusionPolicy("all")
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Entity\Api\Microsoft
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class CustomerGroup
{
    use EntityProxyTrait;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="Description", type="string")
     */
    protected $description;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Id()
     * @APIConnector\Column(name="CustomerGroupId", type="string")
     */
    protected $customerGroupId;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="DefaultDimensionDisplayValue", type="string")
     */
    protected $defaultDimensionDisplayValue;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Id()
     * @APIConnector\Column(name="dataAreaId", type="string")
     */
    protected $dataAreaId;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="PaymentTermId", type="string")
     */
    protected $paymentTermId;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="IsSalesTaxIncludedInPrice", type="string")
     */
    protected $isSalesTaxIncludedInPrice;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="WriteOffReason", type="string")
     */
    protected $writeOffReason;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="TaxGroupId", type="string")
     */
    protected $taxGroupId;

    /**
     * Get customerGroupId
     *
     * @return string
     */
    public function getCustomerGroupId(): ?string
    {
        return $this->customerGroupId;
    }

    /**
     * Set customerGroupId
     *
     * @param string $customerGroupId
     *
     * @return CustomerGroup
     */
    public function setCustomerGroupId(?string $customerGroupId = null): CustomerGroup
    {
        return $this->setTrigger('customerGroupId', $customerGroupId);
    }

    /**
     * Get dataAreaId
     *
     * @return string
     */
    public function getDataAreaId(): ?string
    {
        return $this->dataAreaId;
    }

    /**
     * Set dataAreaId
     *
     * @param string $dataAreaId
     *
     * @return CustomerGroup
     */
    public function setDataAreaId(?string $dataAreaId = null): CustomerGroup
    {
        return $this->setTrigger('dataAreaId', $dataAreaId);
    }

    /**
     * Get defaultDimensionDisplayValue
     *
     * @return string
     */
    public function getDefaultDimensionDisplayValue(): ?string
    {
        return $this->defaultDimensionDisplayValue;
    }

    /**
     * Set defaultDimensionDisplayValue
     *
     * @param string $defaultDimensionDisplayValue
     *
     * @return CustomerGroup
     */
    public function setDefaultDimensionDisplayValue(?string $defaultDimensionDisplayValue = null): CustomerGroup
    {
        return $this->setTrigger('defaultDimensionDisplayValue', $defaultDimensionDisplayValue);
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return CustomerGroup
     */
    public function setDescription(?string $description = null): CustomerGroup
    {
        return $this->setTrigger('description', $description);
    }

    /**
     * Get isSalesTaxIncludedInPrice
     *
     * @return string
     */
    public function getIsSalesTaxIncludedInPrice(): ?string
    {
        return $this->isSalesTaxIncludedInPrice;
    }

    /**
     * Set isSalesTaxIncludedInPrice
     *
     * @param string $isSalesTaxIncludedInPrice
     *
     * @return CustomerGroup
     */
    public function setIsSalesTaxIncludedInPrice(?string $isSalesTaxIncludedInPrice = null): CustomerGroup
    {
        return $this->setTrigger('isSalesTaxIncludedInPrice', $isSalesTaxIncludedInPrice);
    }

    /**
     * Get paymentTermId
     *
     * @return string
     */
    public function getPaymentTermId(): ?string
    {
        return $this->paymentTermId;
    }

    /**
     * Set paymentTermId
     *
     * @param string $paymentTermId
     *
     * @return CustomerGroup
     */
    public function setPaymentTermId(?string $paymentTermId = null): CustomerGroup
    {
        return $this->setTrigger('paymentTermId', $paymentTermId);
    }

    /**
     * Get taxGroupId
     *
     * @return string
     */
    public function getTaxGroupId(): ?string
    {
        return $this->taxGroupId;
    }

    /**
     * Set taxGroupId
     *
     * @param string $taxGroupId
     *
     * @return CustomerGroup
     */
    public function setTaxGroupId(?string $taxGroupId = null): CustomerGroup
    {
        return $this->setTrigger('taxGroupId', $taxGroupId);
    }

    /**
     * Get writeOffReason
     *
     * @return string
     */
    public function getWriteOffReason(): ?string
    {
        return $this->writeOffReason;
    }

    /**
     * Set writeOffReason
     *
     * @param string $writeOffReason
     *
     * @return CustomerGroup
     */
    public function setWriteOffReason(?string $writeOffReason = null): CustomerGroup
    {
        return $this->setTrigger('writeOffReason', $writeOffReason);
    }
}
