<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 04/04/17
 * Time: 17:10
 */

namespace Todotoday\AccountBundle\Entity\Api\Microsoft;

use Actiane\ApiConnectorBundle\Lib\EntityProxyTrait;
use Actiane\ApiConnectorBundle\Lib\Mapping as APIConnector;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class DirectDebitMandate
 *
 * @SuppressWarnings(PHPMD)
 * @APIConnector\Entity("DirectDebitMandates", repositoryId="todotoday.account.repository.api.direct_debit_mandate")
 * @package Todotoday\AccountBundle\Entity\Api\Microsoft
 * @Serializer\ExclusionPolicy("all")
 */
class DirectDebitMandate
{
    use EntityProxyTrait;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CustomerAddress", type="string")
     * @Serializer\Expose()
     */
    protected $customerAddress;

    /**
     * @var int
     *
     * @APIConnector\Column(name="DaysForRecurringPrenotification", type="int")
     * @Serializer\Expose()
     */
    protected $daysForRecurringPrenotification;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CustomerAccount", type="string", required=true)
     * @Serializer\Expose()
     */
    protected $customerAccount;

    /**
     * @var string
     *
     * @APIConnector\Column(name="Status", type="string", mType="Microsoft.Dynamics.DataEntities.CustMandateStatus")
     * @Serializer\Expose()
     */
    protected $status;

    /**
     * @var string
     * @APIConnector\Id()
     * @APIConnector\Column(name="MandateId", type="string")
     * @Serializer\Expose()
     */
    protected $mandateId;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="ExpirationDate", type="datetimez")
     * @Serializer\Expose()
     */
    protected $expirationDate;

    /**
     * @var \Datetime
     *
     * @APIConnector\Column(name="CancellationDate", type="datetimez")
     * @Serializer\Expose()
     */
    protected $cancellationDate;

    /**
     * @var int
     *
     * @APIConnector\Column(name="DaysForFirstPrenotification", type="int")
     * @Serializer\Expose()
     */
    protected $daysForFirstPrenotification;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CustomerBankAccount", type="string", required=true)
     * @Serializer\Expose()
     */
    protected $customerBankAccount;

    /**
     * @var \Datetime
     *
     * @APIConnector\Column(name="SignatureDate", type="datetimez")
     * @Serializer\Expose()
     */
    protected $signatureDate;

    /**
     * @var string
     *
     * @APIConnector\Column(name="MandateScheme", type="string",
     *                                            mType="Microsoft.Dynamics.DataEntities.CustMandateScheme")
     * @Serializer\Expose()
     */
    protected $mandateScheme;

    /**
     * @var int
     *
     * @APIConnector\Column(name="DaysForFirstBankSubmission", type="int")
     * @Serializer\Expose()
     */
    protected $daysForFirstBankSubmission;

    /**
     * @var int
     *
     * @APIConnector\Column(name="UsageCount", type="int")
     * @Serializer\Expose()
     */
    protected $usageCount;

    /**
     * @var int
     *
     * @APIConnector\Column(name="ExpectedNumberOfPayments", type="int")
     * @Serializer\Expose()
     */
    protected $expectedNumberOfPayments;

    /**
     * @var string
     *
     * @APIConnector\Column(name="MandatePaymentType", type="string",
     *                                                 mType="Microsoft.Dynamics.DataEntities.CustMandatePaymentType")
     * @Serializer\Expose()
     */
    protected $mandatePaymentType;

    /**
     * @var string
     * @APIConnector\Id()
     * @APIConnector\Column(name="dataAreaId", type="string")
     * @Serializer\Expose()
     */
    protected $dataAreaId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CreditorBankAccount", type="string", required=true)
     * @Serializer\Expose()
     */
    protected $creditorBankAccount;

    /**
     * @var string
     *
     * @APIConnector\Column(name="SignatureLocation", type="string")
     * @Serializer\Expose()
     */
    protected $signatureLocation;

    /**
     * @var int
     *
     * @APIConnector\Column(name="DaysForRecurringBankSubmission", type="int")
     * @Serializer\Expose()
     */
    protected $daysForRecurringBankSubmission;

    /**
     * @var string
     *
     * @APIConnector\Column(name="IdAcronis", type="string")
     * @Serializer\Expose()
     */
    protected $idAcronis;

    /**
     * @return \Datetime
     */
    public function getCancellationDate(): ?\Datetime
    {
        return $this->cancellationDate;
    }

    /**
     * @param \Datetime $cancellationDate
     *
     * @return DirectDebitMandate
     */
    public function setCancellationDate(\Datetime $cancellationDate): DirectDebitMandate
    {
        return $this->setTrigger('cancellationDate', $cancellationDate);
    }

    /**
     * @return string
     */
    public function getCreditorBankAccount(): ?string
    {
        return $this->creditorBankAccount;
    }

    /**
     * @param string $creditorBankAccount
     *
     * @return DirectDebitMandate
     */
    public function setCreditorBankAccount(string $creditorBankAccount): DirectDebitMandate
    {
        return $this->setTrigger('creditorBankAccount', $creditorBankAccount);
    }

    /**
     * @return string
     */
    public function getCustomerAccount(): ?string
    {
        return $this->customerAccount;
    }

    /**
     * @param string $customerAccount
     *
     * @return DirectDebitMandate
     */
    public function setCustomerAccount(string $customerAccount): DirectDebitMandate
    {
        return $this->setTrigger('customerAccount', $customerAccount);
    }

    /**
     * @return string
     */
    public function getCustomerAddress(): ?string
    {
        return $this->customerAddress;
    }

    /**
     * @param string $customerAddress
     *
     * @return DirectDebitMandate
     */
    public function setCustomerAddress(string $customerAddress): DirectDebitMandate
    {
        return $this->setTrigger('customerAddress', $customerAddress);
    }

    /**
     * @return string
     */
    public function getCustomerBankAccount(): ?string
    {
        return $this->customerBankAccount;
    }

    /**
     * @param string $customerBankAccount
     *
     * @return DirectDebitMandate
     */
    public function setCustomerBankAccount(string $customerBankAccount): DirectDebitMandate
    {
        return $this->setTrigger('customerBankAccount', $customerBankAccount);
    }

    /**
     * @return string
     */
    public function getDataAreaId(): ?string
    {
        return $this->dataAreaId;
    }

    /**
     * @param string $dataAreaId
     *
     * @return DirectDebitMandate
     */
    public function setDataAreaId(string $dataAreaId): DirectDebitMandate
    {
        return $this->setTrigger('dataAreaId', $dataAreaId);
    }

    /**
     * @return int
     */
    public function getDaysForFirstBankSubmission(): ?int
    {
        return $this->daysForFirstBankSubmission;
    }

    /**
     * @param int $daysForFirstBankSubmission
     *
     * @return DirectDebitMandate
     */
    public function setDaysForFirstBankSubmission(int $daysForFirstBankSubmission): DirectDebitMandate
    {
        return $this->setTrigger('daysForFirstBankSubmission', $daysForFirstBankSubmission);
    }

    /**
     * @return int
     */
    public function getDaysForFirstPrenotification(): ?int
    {
        return $this->daysForFirstPrenotification;
    }

    /**
     * @param int $daysForFirstPrenotification
     *
     * @return DirectDebitMandate
     */
    public function setDaysForFirstPrenotification(int $daysForFirstPrenotification): DirectDebitMandate
    {
        return $this->setTrigger('daysForFirstPrenotification', $daysForFirstPrenotification);
    }

    /**
     * @return int
     */
    public function getDaysForRecurringBankSubmission(): ?int
    {
        return $this->daysForRecurringBankSubmission;
    }

    /**
     * @param int $daysForRecurringBankSubmission
     *
     * @return DirectDebitMandate
     */
    public function setDaysForRecurringBankSubmission(int $daysForRecurringBankSubmission): DirectDebitMandate
    {
        return $this->setTrigger('daysForRecurringBankSubmission', $daysForRecurringBankSubmission);
    }

    /**
     * @return int
     */
    public function getDaysForRecurringPrenotification(): ?int
    {
        return $this->daysForRecurringPrenotification;
    }

    /**
     * @param int $daysForRecurringPrenotification
     *
     * @return DirectDebitMandate
     */
    public function setDaysForRecurringPrenotification(int $daysForRecurringPrenotification): DirectDebitMandate
    {
        return $this->setTrigger('daysForRecurringPrenotification', $daysForRecurringPrenotification);
    }

    /**
     * @return int
     */
    public function getExpectedNumberOfPayments(): ?int
    {
        return $this->expectedNumberOfPayments;
    }

    /**
     * @param int $expectedNumberOfPayments
     *
     * @return DirectDebitMandate
     */
    public function setExpectedNumberOfPayments(int $expectedNumberOfPayments): DirectDebitMandate
    {
        return $this->setTrigger('expectedNumberOfPayments', $expectedNumberOfPayments);
    }

    /**
     * @return \DateTime
     */
    public function getExpirationDate(): ?\DateTime
    {
        return $this->expirationDate;
    }

    /**
     * @param \DateTime $expirationDate
     *
     * @return DirectDebitMandate
     */
    public function setExpirationDate(\DateTime $expirationDate): DirectDebitMandate
    {
        return $this->setTrigger('expirationDate', $expirationDate);
    }

    /**
     * Get idAcronis
     *
     * @return string
     */
    public function getIdAcronis(): ?string
    {
        return $this->idAcronis;
    }

    /**
     * Set idAcronis
     *
     * @param string $idAcronis
     *
     * @return DirectDebitMandate
     */
    public function setIdAcronis(string $idAcronis): self
    {
        return $this->setTrigger('idAcronis', $idAcronis);
    }

    /**
     * @return string
     */
    public function getMandateId(): ?string
    {
        return $this->mandateId;
    }

    /**
     * @param string $mandateId
     *
     * @return DirectDebitMandate
     */
    public function setMandateId(string $mandateId): DirectDebitMandate
    {
        return $this->setTrigger('mandateId', $mandateId);
    }

    /**
     * @return string
     */
    public function getMandatePaymentType(): ?string
    {
        return $this->mandatePaymentType;
    }

    /**
     * @param string $mandatePaymentType
     *
     * @return DirectDebitMandate
     */
    public function setMandatePaymentType(string $mandatePaymentType): DirectDebitMandate
    {
        return $this->setTrigger('mandatePaymentType', $mandatePaymentType);
    }

    /**
     * @return string
     */
    public function getMandateScheme(): ?string
    {
        return $this->mandateScheme;
    }

    /**
     * @param string $mandateScheme
     *
     * @return DirectDebitMandate
     */
    public function setMandateScheme(string $mandateScheme): DirectDebitMandate
    {
        return $this->setTrigger('mandateScheme', $mandateScheme);
    }

    /**
     * @return \Datetime
     */
    public function getSignatureDate(): ?\Datetime
    {
        return $this->signatureDate;
    }

    /**
     * @param \Datetime $signatureDate
     *
     * @return DirectDebitMandate
     */
    public function setSignatureDate(\Datetime $signatureDate): DirectDebitMandate
    {
        return $this->setTrigger('signatureDate', $signatureDate);
    }

    /**
     * @return string
     */
    public function getSignatureLocation(): ?string
    {
        return $this->signatureLocation;
    }

    /**
     * @param string $signatureLocation
     *
     * @return DirectDebitMandate
     */
    public function setSignatureLocation(string $signatureLocation): DirectDebitMandate
    {
        return $this->setTrigger('signatureLocation', $signatureLocation);
    }

    /**
     * @return string
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return DirectDebitMandate
     */
    public function setStatus(string $status): DirectDebitMandate
    {
        return $this->setTrigger('status', $status);
    }

    /**
     * @return int
     */
    public function getUsageCount(): ?int
    {
        return $this->usageCount;
    }

    /**
     * @param int $usageCount
     *
     * @return DirectDebitMandate
     */
    public function setUsageCount(int $usageCount): DirectDebitMandate
    {
        return $this->setTrigger('usageCount', $usageCount);
    }
}
