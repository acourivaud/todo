<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 23/06/2017
 * Time: 12:13
 */

namespace Todotoday\AccountBundle\Entity\Api\Microsoft;

use Actiane\ApiConnectorBundle\Lib\EntityProxyTrait;
use Actiane\ApiConnectorBundle\Lib\Mapping as APIConnector;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class CustField
 *
 * @SuppressWarnings(PHPMD)
 * @APIConnector\Entity(path="CustFields", repositoryId="todotoday.account.repository.api.cust_field")
 * @Serializer\ExclusionPolicy("all")
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Entity\Api\Microsoft
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class CustField
{
    use EntityProxyTrait;

    /**
     * @var string
     *
     * @APIConnector\Column(name="Name", type="string")
     */
    protected $name;

    /**
     * @var string
     *
     * @APIConnector\Column(
     *     name="FieldType",
     *     type="string",
     *     mType="Microsoft.Dynamics.DataEntities.TDTD_AdditionalFieldType"
     * )
     */
    protected $fieldType;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @APIConnector\Column(name="dataAreaId", type="string")
     */
    protected $dataAreaId;

    /**
     * @var int
     *
     * @APIConnector\Column(name="IntValue", type="int")
     */
    protected $intValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="BoolValue", type="string", mType="Microsoft.Dynamics.DataEntities.NoYes")
     */
    protected $boolValue;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="DateValue", type="datetimez")
     */
    protected $dateValue;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @APIConnector\Column(name="LanguageId", type="string")
     */
    protected $languageId;

    /**
     * @var int
     *
     * @APIConnector\Id()
     * @APIConnector\Column(name="FieldId", type="int")
     */
    protected $fieldId;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @APIConnector\Column(name="CustAccount", type="string")
     */
    protected $custAccount;

    /**
     * @var float
     *
     * @APIConnector\Column(name="RealValue", type="float")
     */
    protected $realValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="StringValue", type="string")
     */
    protected $stringValue;

    /**
     * Get name
     *
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CustField
     */
    public function setName(?string $name): CustField
    {
        return $this->setTrigger('name', $name);
    }

    /**
     * Get fieldType
     *
     * @return string
     */
    public function getFieldType(): ?string
    {
        return $this->fieldType;
    }

    /**
     * Set fieldType
     *
     * @param string $fieldType
     *
     * @return CustField
     */
    public function setFieldType(?string $fieldType): CustField
    {
        return $this->setTrigger('fieldType', $fieldType);
    }

    /**
     * Get dataAreaId
     *
     * @return string
     */
    public function getDataAreaId(): ?string
    {
        return $this->dataAreaId;
    }

    /**
     * Set dataAreaId
     *
     * @param string $dataAreaId
     *
     * @return CustField
     */
    public function setDataAreaId(?string $dataAreaId): CustField
    {
        return $this->setTrigger('dataAreaId', $dataAreaId);
    }

    /**
     * Get intValue
     *
     * @return int
     */
    public function getIntValue(): ?int
    {
        return $this->intValue;
    }

    /**
     * Set intValue
     *
     * @param int $intValue
     *
     * @return CustField
     */
    public function setIntValue(?int $intValue): CustField
    {
        return $this->setTrigger('intValue', $intValue);
    }

    /**
     * Get boolValue
     *
     * @return string
     */
    public function getBoolValue(): ?string
    {
        return $this->boolValue;
    }

    /**
     * Set boolValue
     *
     * @param string $boolValue
     *
     * @return CustField
     */
    public function setBoolValue(?string $boolValue): CustField
    {
        return $this->setTrigger('boolValue', $boolValue);
    }

    /**
     * Get dateValue
     *
     * @return \DateTime
     */
    public function getDateValue(): ?\DateTime
    {
        return $this->dateValue;
    }

    /**
     * Set dateValue
     *
     * @param \DateTime $dateValue
     *
     * @return CustField
     */
    public function setDateValue(?\DateTime $dateValue): CustField
    {
        return $this->setTrigger('dateValue', $dateValue);
    }

    /**
     * Get languageId
     *
     * @return string
     */
    public function getLanguageId(): ?string
    {
        return $this->languageId;
    }

    /**
     * Set languageId
     *
     * @param string $languageId
     *
     * @return CustField
     */
    public function setLanguageId(?string $languageId): CustField
    {
        return $this->setTrigger('languageId', $languageId);
    }

    /**
     * Get fieldId
     *
     * @return int
     */
    public function getFieldId(): ?int
    {
        return $this->fieldId;
    }

    /**
     * Set fieldId
     *
     * @param int $fieldId
     *
     * @return CustField
     */
    public function setFieldId(?int $fieldId): CustField
    {
        return $this->setTrigger('fieldId', $fieldId);
    }

    /**
     * Get custAccount
     *
     * @return string
     */
    public function getCustAccount(): ?string
    {
        return $this->custAccount;
    }

    /**
     * Set custAccount
     *
     * @param string $custAccount
     *
     * @return CustField
     */
    public function setCustAccount(?string $custAccount): CustField
    {
        return $this->setTrigger('custAccount', $custAccount);
    }

    /**
     * Get realValue
     *
     * @return float
     */
    public function getRealValue(): ?float
    {
        return $this->realValue;
    }

    /**
     * Set realValue
     *
     * @param float $realValue
     *
     * @return CustField
     */
    public function setRealValue(?float $realValue): CustField
    {
        return $this->setTrigger('realValue', $realValue);
    }

    /**
     * Get stringValue
     *
     * @return string
     */
    public function getStringValue(): ?string
    {
        return $this->stringValue;
    }

    /**
     * Set stringValue
     *
     * @param string $stringValue
     *
     * @return CustField
     */
    public function setStringValue(?string $stringValue): CustField
    {
        return $this->setTrigger('stringValue', $stringValue);
    }
}
