<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 14/03/2017
 * Time: 11:04
 */

namespace Todotoday\AccountBundle\Entity\Api\Microsoft;

use Actiane\ApiConnectorBundle\Lib\EntityProxyTrait;
use Actiane\ApiConnectorBundle\Lib\Mapping as APIConnector;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class ContactPerson
 *
 * @SuppressWarnings(PHPMD)
 * @APIConnector\Entity(path="TDTDContactPersons", repositoryId="todotoday.account.repository.api.contact_person")
 * @Serializer\ExclusionPolicy("all")
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Entity\Api\Microsoft
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class ContactPerson
{
    use EntityProxyTrait;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="IsPrimaryPhoneNumberMobile", type="string",
     *                                                         mType="Microsoft.Dynamics.DataEntities.NoYes")
     * @Serializer\Groups({"contact_person"})
     */
    protected $isPrimaryPhoneNumberMobile;

    /**
     * @var int
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="BirthDay", type="int")
     * @Serializer\Groups({"contact_person"})
     */
    protected $birthDay;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="ContactInformationLanguageId", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $contactInformationLanguageId;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="LastName", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $lastName;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="PrimaryTelex", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $primaryTelex;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="PrimaryFaxNumberPurpose", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $primaryFaxNumberPurpose;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="ContactPersonPartyType", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $contactPersonPartyType;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="PrimaryEmailAddressPurpose", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $primaryEmailAddressPurpose;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="Hobbies", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $hobbies;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="PrimaryEmailAddress", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $primaryEmailAddress;

    /**
     * @var \DateTime
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="LastEditDateTime", type="datetimez")
     * @Serializer\Groups({"contact_person"})
     */
    protected $lastEditDateTime;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="PrimaryEmailAddressDescription", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $primaryEmailAddressDescription;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="FormattedPrimaryAddress", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $formattedPrimaryAddress;

    /**
     * @var \DateTime
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="PrimaryAddressValidFrom", type="datetimez")
     * @Serializer\Groups({"contact_person"})
     */
    protected $primaryAddressValidFrom;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="EmploymentProfession", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $employmentProfession;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="PrimaryFaxNumberExtension", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $primaryFaxNumberExtension;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="PrimaryPhoneNumberDescription", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $primaryPhoneNumberDescription;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="PrimaryFaxNumberDescription", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $primaryFaxNumberDescription;

    /**
     * @var \DateTime
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="NameValidTo", type="datetimez")
     * @Serializer\Groups({"contact_person"})
     */
    protected $nameValidTo;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="AssociatedPartyNumber", type="string", required=true)
     * @Serializer\Groups({"contact_person"})
     */
    protected $associatedPartyNumber;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="PrimaryAddressStateId", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $primaryAddressStateId;

    /**
     * @var int
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="EndJobDay", type="int")
     * @Serializer\Groups({"contact_person"})
     */
    protected $endJobDay;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="EndJobMonth", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $endJobMonth;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="OrganizationIdentificationNumber", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $organizationIdNumber;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="CanOrder", type="string", mType="Microsoft.Dynamics.DataEntities.NoYes")
     * @Serializer\Groups({"contact_person"})
     */
    protected $canOrder;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="PrimaryAddressCountryRegionId", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $primaryAddressCountryRegionId;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="PrimaryPhoneNumberPurpose", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $primaryPhoneNumberPurpose;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="PrimaryTelexPurpose", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $primaryTelexPurpose;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="PrimaryAddressCountyId", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $primaryAddressCountyId;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="EmploymentJobTitle", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $employmentJobTitle;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="FirstName", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $firstName;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="Gender", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $gender;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="PrimaryPhoneNumberExtension", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $primaryPhoneNumberExtension;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="PrimaryAddressLocationRoles", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $primaryAddressLocationRoles;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="Notes", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $notes;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="ContactPersonName", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $contactPersonName;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="PrimaryAddressDistrictName", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $primaryAddressDistrictName;

    /**
     * @var int
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="EndJobYear", type="int")
     * @Serializer\Groups({"contact_person"})
     */
    protected $endJobYear;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(
     *     name="ContactType",
     *     type="string",
     *     mType="Microsoft.Dynamics.DataEntities.ContactPersonType"
     * )
     * @Serializer\Groups({"contact_person"})
     */
    protected $contactType;

    /**
     * @var \DateTime
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="PrimaryAddressValidTo", type="datetimez")
     * @Serializer\Groups({"contact_person"})
     */
    protected $primaryAddressValidTo;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="PrimaryTelexDescription", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $primaryTelexDescription;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="PrimaryAddressStreet", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $primaryAddressStreet;

    /**
     * @var int
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="BirthYear", type="int")
     * @Serializer\Groups({"contact_person"})
     */
    protected $birthYear;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="ContactPersonPartyNumber", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $contactPersonPartyNumber;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="PrimaryFaxNumber", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $primaryFaxNumber;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="GovernmentIdentificationNumber", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $governmentIdentificationNumber;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="PrimaryAddressDescription", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $primaryAddressDescription;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="PrimaryAddressCity", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $primaryAddressCity;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="PrimaryAddressStreetInKana", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $primaryAddressStreetInKana;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="PrimaryAddressZipCode", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $primaryAddressZipCode;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="PrimaryPhoneNumber", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $primaryPhoneNumber;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="PrimaryAddressTimeZone", type="string",
     *                                                     mType="Microsoft.Dynamics.DataEntities.Timezone")
     * @Serializer\Groups({"contact_person"})
     */
    protected $primaryAddressTimeZone;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="PrimaryAddressCityInKana", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $primaryAddressCityInKana;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="MiddleName", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $middleName;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="EmploymentJobFunctionName", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $employmentJobFunctionName;


    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Id()
     * @APIConnector\Column(name="dataAreaId", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $dataAreaId;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="IsVIP", type="string", mType="Microsoft.Dynamics.DataEntities.NoYes")
     * @Serializer\Groups({"contact_person"})
     */
    protected $isVIP;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Id()
     * @APIConnector\Column(name="ContactPersonId", type="string")
     * @Serializer\Groups({"contact_person"})
     */
    protected $contactPersonId;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="IsInactive", type="string", mType="Microsoft.Dynamics.DataEntities.NoYes")
     * @Serializer\Groups({"contact_person"})
     */
    protected $isInactive;

    /**
     * @var \DateTime
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="NameValidFrom", type="datetimez")
     * @Serializer\Groups({"contact_person"})
     */
    protected $nameValidFrom;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="BirthMonth", type="string", mType="Microsoft.Dynamics.DataEntities.MonthsOfYear")
     * @Serializer\Groups({"contact_person"})
     */
    protected $birthMonth;

    /**
     * Get associatedPartyNumber
     *
     * @return string
     */
    public function getAssociatedPartyNumber(): ?string
    {
        return $this->associatedPartyNumber;
    }

    /**
     * Set associatedPartyNumber
     *
     * @param string $associatedPartyNumber
     *
     * @return ContactPerson
     */
    public function setAssociatedPartyNumber(?string $associatedPartyNumber): ContactPerson
    {
        return $this->setTrigger('associatedPartyNumber', $associatedPartyNumber);
    }

    /**
     * Get birthDay
     *
     * @return int
     */
    public function getBirthDay(): ?int
    {
        return $this->birthDay;
    }

    /**
     * Set birthDay
     *
     * @param int $birthDay
     *
     * @return ContactPerson
     */
    public function setBirthDay(?int $birthDay): ContactPerson
    {
        return $this->setTrigger('birthDay', $birthDay);
    }

    /**
     * Get birthMonth
     *
     * @return string
     */
    public function getBirthMonth(): ?string
    {
        return $this->birthMonth;
    }

    /**
     * Set birthMonth
     *
     * @param string $birthMonth
     *
     * @return ContactPerson
     */
    public function setBirthMonth(?string $birthMonth): ContactPerson
    {
        return $this->setTrigger('birthMonth', $birthMonth);
    }

    /**
     * Get birthYear
     *
     * @return int
     */
    public function getBirthYear(): ?int
    {
        return $this->birthYear;
    }

    /**
     * Set birthYear
     *
     * @param int $birthYear
     *
     * @return ContactPerson
     */
    public function setBirthYear(?int $birthYear): ContactPerson
    {
        return $this->setTrigger('birthYear', $birthYear);
    }

    /**
     * Get canOrder
     *
     * @return string
     */
    public function getCanOrder(): ?string
    {
        return $this->canOrder;
    }

    /**
     * Set canOrder
     *
     * @param string $canOrder
     *
     * @return ContactPerson
     */
    public function setCanOrder(?string $canOrder): ContactPerson
    {
        return $this->setTrigger('canOrder', $canOrder);
    }

    /**
     * Get contactInformationLanguageId
     *
     * @return string
     */
    public function getContactInformationLanguageId(): ?string
    {
        return $this->contactInformationLanguageId;
    }

    /**
     * Set contactInformationLanguageId
     *
     * @param string $contactInformationLanguageId
     *
     * @return ContactPerson
     */
    public function setContactInformationLanguageId(?string $contactInformationLanguageId): ContactPerson
    {
        return $this->setTrigger('contactInformationLanguageId', $contactInformationLanguageId);
    }

    /**
     * Get contactPersonId
     *
     * @return string
     */
    public function getContactPersonId(): ?string
    {
        return $this->contactPersonId;
    }

    /**
     * Set contactPersonId
     *
     * @param string $contactPersonId
     *
     * @return ContactPerson
     */
    public function setContactPersonId(?string $contactPersonId): ContactPerson
    {
        return $this->setTrigger('contactPersonId', $contactPersonId);
    }

    /**
     * Get contactPersonName
     *
     * @return string
     */
    public function getContactPersonName(): ?string
    {
        return $this->contactPersonName;
    }

    /**
     * Set contactPersonName
     *
     * @param string $contactPersonName
     *
     * @return ContactPerson
     */
    public function setContactPersonName(?string $contactPersonName): ContactPerson
    {
        return $this->setTrigger('contactPersonName', $contactPersonName);
    }

    /**
     * Get contactPersonPartyNumber
     *
     * @return string
     */
    public function getContactPersonPartyNumber(): ?string
    {
        return $this->contactPersonPartyNumber;
    }

    /**
     * Set contactPersonPartyNumber
     *
     * @param string $contactPersonPartyNumber
     *
     * @return ContactPerson
     */
    public function setContactPersonPartyNumber(?string $contactPersonPartyNumber): ContactPerson
    {
        return $this->setTrigger('contactPersonPartyNumber', $contactPersonPartyNumber);
    }

    /**
     * Get contactPersonPartyType
     *
     * @return string
     */
    public function getContactPersonPartyType(): ?string
    {
        return $this->contactPersonPartyType;
    }

    /**
     * Set contactPersonPartyType
     *
     * @param string $contactPersonPartyType
     *
     * @return ContactPerson
     */
    public function setContactPersonPartyType(?string $contactPersonPartyType): ContactPerson
    {
        return $this->setTrigger('contactPersonPartyType', $contactPersonPartyType);
    }

    /**
     * Get contactType
     *
     * @return string
     */
    public function getContactType(): ?string
    {
        return $this->contactType;
    }

    /**
     * Set contactType
     *
     * @param string $contactType
     *
     * @return ContactPerson
     */
    public function setContactType(?string $contactType): ContactPerson
    {
        return $this->setTrigger('contactType', $contactType);
    }

    /**
     * Get dataAreaId
     *
     * @return string
     */
    public function getDataAreaId(): ?string
    {
        return $this->dataAreaId;
    }

    /**
     * Set dataAreaId
     *
     * @param string $dataAreaId
     *
     * @return ContactPerson
     */
    public function setDataAreaId(?string $dataAreaId): ContactPerson
    {
        return $this->setTrigger('dataAreaId', $dataAreaId);
    }

    /**
     * Get employmentJobFunctionName
     *
     * @return string
     */
    public function getEmploymentJobFunctionName(): ?string
    {
        return $this->employmentJobFunctionName;
    }

    /**
     * Set employmentJobFunctionName
     *
     * @param string $employmentJobFunctionName
     *
     * @return ContactPerson
     */
    public function setEmploymentJobFunctionName(?string $employmentJobFunctionName): ContactPerson
    {
        return $this->setTrigger('employmentJobFunctionName', $employmentJobFunctionName);
    }

    /**
     * Get employmentJobTitle
     *
     * @return string
     */
    public function getEmploymentJobTitle(): ?string
    {
        return $this->employmentJobTitle;
    }

    /**
     * Set employmentJobTitle
     *
     * @param string $employmentJobTitle
     *
     * @return ContactPerson
     */
    public function setEmploymentJobTitle(?string $employmentJobTitle): ContactPerson
    {
        return $this->setTrigger('employmentJobTitle', $employmentJobTitle);
    }

    /**
     * Get employmentProfession
     *
     * @return string
     */
    public function getEmploymentProfession(): ?string
    {
        return $this->employmentProfession;
    }

    /**
     * Set employmentProfession
     *
     * @param string $employmentProfession
     *
     * @return ContactPerson
     */
    public function setEmploymentProfession(?string $employmentProfession): ContactPerson
    {
        return $this->setTrigger('employmentProfession', $employmentProfession);
    }

    /**
     * Get endJobDay
     *
     * @return int|null
     */
    public function getEndJobDay(): ?int
    {
        return $this->endJobDay;
    }

    /**
     * Set endJobDay
     *
     * @param null|int $endJobDay
     *
     * @return ContactPerson
     */
    public function setEndJobDay(?int $endJobDay): ContactPerson
    {
        return $this->setTrigger('endJobDay', $endJobDay);
    }

    /**
     * Get endJobMonth
     *
     * @return string
     */
    public function getEndJobMonth(): ?string
    {
        return $this->endJobMonth;
    }

    /**
     * Set endJobMonth
     *
     * @param string $endJobMonth
     *
     * @return ContactPerson
     */
    public function setEndJobMonth(?string $endJobMonth): ContactPerson
    {
        return $this->setTrigger('endJobMonth', $endJobMonth);
    }

    /**
     * Get endJobYear
     *
     * @return int|null
     */
    public function getEndJobYear(): ?int
    {
        return $this->endJobYear;
    }

    /**
     * Set endJobYear
     *
     * @param int|null $endJobYear
     *
     * @return ContactPerson
     */
    public function setEndJobYear(?int $endJobYear): ContactPerson
    {
        return $this->setTrigger('endJobYear', $endJobYear);
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return ContactPerson
     */
    public function setFirstName(?string $firstName): ContactPerson
    {
        return $this->setTrigger('firstName', $firstName);
    }

    /**
     * Get formattedPrimaryAddress
     *
     * @return string
     */
    public function getFormattedPrimaryAddress(): ?string
    {
        return $this->formattedPrimaryAddress;
    }

    /**
     * Set formattedPrimaryAddress
     *
     * @param string $formattedPrimaryAddress
     *
     * @return ContactPerson
     */
    public function setFormattedPrimaryAddress(?string $formattedPrimaryAddress): ContactPerson
    {
        return $this->setTrigger('formattedPrimaryAddress', $formattedPrimaryAddress);
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender(): ?string
    {
        return $this->gender;
    }

    /**
     * Set gender
     *
     * @param string $gender
     *
     * @return ContactPerson
     */
    public function setGender(?string $gender): ContactPerson
    {
        return $this->setTrigger('gender', $gender);
    }

    /**
     * Get governmentIdentificationNumber
     *
     * @return string
     */
    public function getGovernmentIdentificationNumber(): ?string
    {
        return $this->governmentIdentificationNumber;
    }

    /**
     * Set governmentIdentificationNumber
     *
     * @param string $governmentIdentificationNumber
     *
     * @return ContactPerson
     */
    public function setGovernmentIdentificationNumber(?string $governmentIdentificationNumber): ContactPerson
    {
        return $this->setTrigger('governmentIdentificationNumber', $governmentIdentificationNumber);
    }

    /**
     * Get hobbies
     *
     * @return string
     */
    public function getHobbies(): ?string
    {
        return $this->hobbies;
    }

    /**
     * Set hobbies
     *
     * @param string $hobbies
     *
     * @return ContactPerson
     */
    public function setHobbies(?string $hobbies): ContactPerson
    {
        return $this->setTrigger('hobbies', $hobbies);
    }

    /**
     * Get isInactive
     *
     * @return string
     */
    public function getIsInactive(): ?string
    {
        return $this->isInactive;
    }

    /**
     * Set isInactive
     *
     * @param string $isInactive
     *
     * @return ContactPerson
     */
    public function setIsInactive(?string $isInactive): ContactPerson
    {
        return $this->setTrigger('isInactive', $isInactive);
    }

    /**
     * Get isPrimaryPhoneNumberMobile
     *
     * @return string
     */
    public function getIsPrimaryPhoneNumberMobile(): ?string
    {
        return $this->isPrimaryPhoneNumberMobile;
    }

    /**
     * Set isPrimaryPhoneNumberMobile
     *
     * @param string $isPrimaryPhoneNumberMobile
     *
     * @return ContactPerson
     */
    public function setIsPrimaryPhoneNumberMobile(?string $isPrimaryPhoneNumberMobile): ContactPerson
    {
        return $this->setTrigger('isPrimaryPhoneNumberMobile', $isPrimaryPhoneNumberMobile);
    }

    /**
     * Get isVIP
     *
     * @return string
     */
    public function getIsVIP(): ?string
    {
        return $this->isVIP;
    }

    /**
     * Set isVIP
     *
     * @param string $isVIP
     *
     * @return ContactPerson
     */
    public function setIsVIP(?string $isVIP): ContactPerson
    {
        return $this->setTrigger('isVIP', $isVIP);
    }

    /**
     * Get lastEditDateTime
     *
     * @return \DateTime
     */
    public function getLastEditDateTime(): ?\DateTime
    {
        return $this->lastEditDateTime;
    }

    /**
     * Set lastEditDateTime
     *
     * @param \DateTime $lastEditDateTime
     *
     * @return ContactPerson
     */
    public function setLastEditDateTime(?\DateTime $lastEditDateTime): ContactPerson
    {
        return $this->setTrigger('lastEditDateTime', $lastEditDateTime);
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return ContactPerson
     */
    public function setLastName(?string $lastName): ContactPerson
    {
        return $this->setTrigger('lastName', $lastName);
    }

    /**
     * Get middleName
     *
     * @return string
     */
    public function getMiddleName(): ?string
    {
        return $this->middleName;
    }

    /**
     * Set middleName
     *
     * @param string $middleName
     *
     * @return ContactPerson
     */
    public function setMiddleName(?string $middleName): ContactPerson
    {
        return $this->setTrigger('middleName', $middleName);
    }

    /**
     * Get nameValidFrom
     *
     * @return \DateTime
     */
    public function getNameValidFrom(): ?\DateTime
    {
        return $this->nameValidFrom;
    }

    /**
     * Set nameValidFrom
     *
     * @param \DateTime $nameValidFrom
     *
     * @return ContactPerson
     */
    public function setNameValidFrom(?\DateTime $nameValidFrom): ContactPerson
    {
        return $this->setTrigger('nameValidFrom', $nameValidFrom);
    }

    /**
     * Get nameValidTo
     *
     * @return \DateTime
     */
    public function getNameValidTo(): ?\DateTime
    {
        return $this->nameValidTo;
    }

    /**
     * Set nameValidTo
     *
     * @param \DateTime $nameValidTo
     *
     * @return ContactPerson
     */
    public function setNameValidTo(?\DateTime $nameValidTo): ContactPerson
    {
        return $this->setTrigger('nameValidTo', $nameValidTo);
    }

    /**
     * Get notes
     *
     * @return string
     */
    public function getNotes(): ?string
    {
        return $this->notes;
    }

    /**
     * Set notes
     *
     * @param string $notes
     *
     * @return ContactPerson
     */
    public function setNotes(?string $notes): ContactPerson
    {
        return $this->setTrigger('notes', $notes);
    }

    /**
     * Get organizationIdNumber
     *
     * @return string
     */
    public function getOrganizationIdNumber(): ?string
    {
        return $this->organizationIdNumber;
    }

    /**
     * Set organizationIdNumber
     *
     * @param string $organizationIdNumber
     *
     * @return ContactPerson
     */
    public function setOrganizationIdNumber(?string $organizationIdNumber): ContactPerson
    {
        return $this->setTrigger('organizationIdNumber', $organizationIdNumber);
    }

    /**
     * Get primaryAddressCity
     *
     * @return string
     */
    public function getPrimaryAddressCity(): ?string
    {
        return $this->primaryAddressCity;
    }

    /**
     * Set primaryAddressCity
     *
     * @param string $primaryAddressCity
     *
     * @return ContactPerson
     */
    public function setPrimaryAddressCity(?string $primaryAddressCity): ContactPerson
    {
        return $this->setTrigger('primaryAddressCity', $primaryAddressCity);
    }

    /**
     * Get primaryAddressCityInKana
     *
     * @return string
     */
    public function getPrimaryAddressCityInKana(): ?string
    {
        return $this->primaryAddressCityInKana;
    }

    /**
     * Set primaryAddressCityInKana
     *
     * @param string $primaryAddressCityInKana
     *
     * @return ContactPerson
     */
    public function setPrimaryAddressCityInKana(?string $primaryAddressCityInKana): ContactPerson
    {
        return $this->setTrigger('primaryAddressCityInKana', $primaryAddressCityInKana);
    }

    /**
     * Get primaryAddressCountryRegionId
     *
     * @return string
     */
    public function getPrimaryAddressCountryRegionId(): ?string
    {
        return $this->primaryAddressCountryRegionId;
    }

    /**
     * Set primaryAddressCountryRegionId
     *
     * @param string $primaryAddressCountryRegionId
     *
     * @return ContactPerson
     */
    public function setPrimaryAddressCountryRegionId(?string $primaryAddressCountryRegionId): ContactPerson
    {
        return $this->setTrigger('primaryAddressCountryRegionId', $primaryAddressCountryRegionId);
    }

    /**
     * Get primaryAddressCountyId
     *
     * @return string
     */
    public function getPrimaryAddressCountyId(): ?string
    {
        return $this->primaryAddressCountyId;
    }

    /**
     * Set primaryAddressCountyId
     *
     * @param string $primaryAddressCountyId
     *
     * @return ContactPerson
     */
    public function setPrimaryAddressCountyId(?string $primaryAddressCountyId): ContactPerson
    {
        return $this->setTrigger('primaryAddressCountyId', $primaryAddressCountyId);
    }

    /**
     * Get primaryAddressDescription
     *
     * @return string
     */
    public function getPrimaryAddressDescription(): ?string
    {
        return $this->primaryAddressDescription;
    }

    /**
     * Set primaryAddressDescription
     *
     * @param string $primaryAddressDescription
     *
     * @return ContactPerson
     */
    public function setPrimaryAddressDescription(?string $primaryAddressDescription): ContactPerson
    {
        return $this->setTrigger('primaryAddressDescription', $primaryAddressDescription);
    }

    /**
     * Get primaryAddressDistrictName
     *
     * @return string
     */
    public function getPrimaryAddressDistrictName(): ?string
    {
        return $this->primaryAddressDistrictName;
    }

    /**
     * Set primaryAddressDistrictName
     *
     * @param string $primaryAddressDistrictName
     *
     * @return ContactPerson
     */
    public function setPrimaryAddressDistrictName(?string $primaryAddressDistrictName): ContactPerson
    {
        return $this->setTrigger('primaryAddressDistrictName', $primaryAddressDistrictName);
    }

    /**
     * Get primaryAddressLocationRoles
     *
     * @return string
     */
    public function getPrimaryAddressLocationRoles(): ?string
    {
        return $this->primaryAddressLocationRoles;
    }

    /**
     * Set primaryAddressLocationRoles
     *
     * @param string $primaryAddressLocationRoles
     *
     * @return ContactPerson
     */
    public function setPrimaryAddressLocationRoles(?string $primaryAddressLocationRoles): ContactPerson
    {
        return $this->setTrigger('primaryAddressLocationRoles', $primaryAddressLocationRoles);
    }

    /**
     * Get primaryAddressStateId
     *
     * @return string
     */
    public function getPrimaryAddressStateId(): ?string
    {
        return $this->primaryAddressStateId;
    }

    /**
     * Set primaryAddressStateId
     *
     * @param string $primaryAddressStateId
     *
     * @return ContactPerson
     */
    public function setPrimaryAddressStateId(?string $primaryAddressStateId): ContactPerson
    {
        return $this->setTrigger('primaryAddressStateId', $primaryAddressStateId);
    }

    /**
     * Get primaryAddressStreet
     *
     * @return string
     */
    public function getPrimaryAddressStreet(): ?string
    {
        return $this->primaryAddressStreet;
    }

    /**
     * Set primaryAddressStreet
     *
     * @param string $primaryAddressStreet
     *
     * @return ContactPerson
     */
    public function setPrimaryAddressStreet(?string $primaryAddressStreet): ContactPerson
    {
        return $this->setTrigger('primaryAddressStreet', $primaryAddressStreet);
    }

    /**
     * Get primaryAddressStreetInKana
     *
     * @return string
     */
    public function getPrimaryAddressStreetInKana(): ?string
    {
        return $this->primaryAddressStreetInKana;
    }

    /**
     * Set primaryAddressStreetInKana
     *
     * @param string $primaryAddressStreetInKana
     *
     * @return ContactPerson
     */
    public function setPrimaryAddressStreetInKana(?string $primaryAddressStreetInKana): ContactPerson
    {
        return $this->setTrigger('primaryAddressStreetInKana', $primaryAddressStreetInKana);
    }

    /**
     * Get primaryAddressTimeZone
     *
     * @return string
     */
    public function getPrimaryAddressTimeZone(): ?string
    {
        return $this->primaryAddressTimeZone;
    }

    /**
     * Set primaryAddressTimeZone
     *
     * @param string $primaryAddressTimeZone
     *
     * @return ContactPerson
     */
    public function setPrimaryAddressTimeZone(?string $primaryAddressTimeZone): ContactPerson
    {
        return $this->setTrigger('primaryAddressTimeZone', $primaryAddressTimeZone);
    }

    /**
     * Get primaryAddressValidFrom
     *
     * @return \DateTime
     */
    public function getPrimaryAddressValidFrom(): ?\DateTime
    {
        return $this->primaryAddressValidFrom;
    }

    /**
     * Set primaryAddressValidFrom
     *
     * @param \DateTime $primaryAddressValidFrom
     *
     * @return ContactPerson
     */
    public function setPrimaryAddressValidFrom(?\DateTime $primaryAddressValidFrom): ContactPerson
    {
        return $this->setTrigger('primaryAddressValidFrom', $primaryAddressValidFrom);
    }

    /**
     * Get primaryAddressValidTo
     *
     * @return \DateTime
     */
    public function getPrimaryAddressValidTo(): ?\DateTime
    {
        return $this->primaryAddressValidTo;
    }

    /**
     * Set primaryAddressValidTo
     *
     * @param \DateTime $primaryAddressValidTo
     *
     * @return ContactPerson
     */
    public function setPrimaryAddressValidTo(?\DateTime $primaryAddressValidTo): ContactPerson
    {
        return $this->setTrigger('primaryAddressValidTo', $primaryAddressValidTo);
    }

    /**
     * Get primaryAddressZipCode
     *
     * @return string
     */
    public function getPrimaryAddressZipCode(): ?string
    {
        return $this->primaryAddressZipCode;
    }

    /**
     * Set primaryAddressZipCode
     *
     * @param string $primaryAddressZipCode
     *
     * @return ContactPerson
     */
    public function setPrimaryAddressZipCode(?string $primaryAddressZipCode): ContactPerson
    {
        return $this->setTrigger('primaryAddressZipCode', $primaryAddressZipCode);
    }

    /**
     * Get primaryEmailAddress
     *
     * @return string
     */
    public function getPrimaryEmailAddress(): ?string
    {
        return $this->primaryEmailAddress;
    }

    /**
     * Set primaryEmailAddress
     *
     * @param string $primaryEmailAddress
     *
     * @return ContactPerson
     */
    public function setPrimaryEmailAddress(?string $primaryEmailAddress): ContactPerson
    {
        return $this->setTrigger('primaryEmailAddress', $primaryEmailAddress);
    }

    /**
     * Get primaryEmailAddressDescription
     *
     * @return string
     */
    public function getPrimaryEmailAddressDescription(): ?string
    {
        return $this->primaryEmailAddressDescription;
    }

    /**
     * Set primaryEmailAddressDescription
     *
     * @param string $primaryEmailAddressDescription
     *
     * @return ContactPerson
     */
    public function setPrimaryEmailAddressDescription(?string $primaryEmailAddressDescription): ContactPerson
    {
        return $this->setTrigger('primaryEmailAddressDescription', $primaryEmailAddressDescription);
    }

    /**
     * Get primaryEmailAddressPurpose
     *
     * @return string
     */
    public function getPrimaryEmailAddressPurpose(): ?string
    {
        return $this->primaryEmailAddressPurpose;
    }

    /**
     * Set primaryEmailAddressPurpose
     *
     * @param string $primaryEmailAddressPurpose
     *
     * @return ContactPerson
     */
    public function setPrimaryEmailAddressPurpose(?string $primaryEmailAddressPurpose): ContactPerson
    {
        return $this->setTrigger('primaryEmailAddressPurpose', $primaryEmailAddressPurpose);
    }

    /**
     * Get primaryFaxNumber
     *
     * @return string
     */
    public function getPrimaryFaxNumber(): ?string
    {
        return $this->primaryFaxNumber;
    }

    /**
     * Set primaryFaxNumber
     *
     * @param string $primaryFaxNumber
     *
     * @return ContactPerson
     */
    public function setPrimaryFaxNumber(?string $primaryFaxNumber): ContactPerson
    {
        return $this->setTrigger('primaryFaxNumber', $primaryFaxNumber);
    }

    /**
     * Get primaryFaxNumberDescription
     *
     * @return string
     */
    public function getPrimaryFaxNumberDescription(): ?string
    {
        return $this->primaryFaxNumberDescription;
    }

    /**
     * Set primaryFaxNumberDescription
     *
     * @param string $primaryFaxNumberDescription
     *
     * @return ContactPerson
     */
    public function setPrimaryFaxNumberDescription(?string $primaryFaxNumberDescription): ContactPerson
    {
        return $this->setTrigger('primaryFaxNumberDescription', $primaryFaxNumberDescription);
    }

    /**
     * Get primaryFaxNumberExtension
     *
     * @return string
     */
    public function getPrimaryFaxNumberExtension(): ?string
    {
        return $this->primaryFaxNumberExtension;
    }

    /**
     * Set primaryFaxNumberExtension
     *
     * @param string $primaryFaxNumberExtension
     *
     * @return ContactPerson
     */
    public function setPrimaryFaxNumberExtension(?string $primaryFaxNumberExtension): ContactPerson
    {
        return $this->setTrigger('primaryFaxNumberExtension', $primaryFaxNumberExtension);
    }

    /**
     * Get primaryFaxNumberPurpose
     *
     * @return string
     */
    public function getPrimaryFaxNumberPurpose(): ?string
    {
        return $this->primaryFaxNumberPurpose;
    }

    /**
     * Set primaryFaxNumberPurpose
     *
     * @param string $primaryFaxNumberPurpose
     *
     * @return ContactPerson
     */
    public function setPrimaryFaxNumberPurpose(?string $primaryFaxNumberPurpose): ContactPerson
    {
        return $this->setTrigger('primaryFaxNumberPurpose', $primaryFaxNumberPurpose);
    }

    /**
     * Get primaryPhoneNumber
     *
     * @return string
     */
    public function getPrimaryPhoneNumber(): ?string
    {
        return $this->primaryPhoneNumber;
    }

    /**
     * Set primaryPhoneNumber
     *
     * @param string $primaryPhoneNumber
     *
     * @return ContactPerson
     */
    public function setPrimaryPhoneNumber(?string $primaryPhoneNumber): ContactPerson
    {
        return $this->setTrigger('primaryPhoneNumber', $primaryPhoneNumber);
    }

    /**
     * Get primaryPhoneNumberDescription
     *
     * @return string
     */
    public function getPrimaryPhoneNumberDescription(): ?string
    {
        return $this->primaryPhoneNumberDescription;
    }

    /**
     * Set primaryPhoneNumberDescription
     *
     * @param string $primaryPhoneNumberDescription
     *
     * @return ContactPerson
     */
    public function setPrimaryPhoneNumberDescription(?string $primaryPhoneNumberDescription): ContactPerson
    {
        return $this->setTrigger('primaryPhoneNumberDescription', $primaryPhoneNumberDescription);
    }

    /**
     * Get primaryPhoneNumberExtension
     *
     * @return string
     */
    public function getPrimaryPhoneNumberExtension(): ?string
    {
        return $this->primaryPhoneNumberExtension;
    }

    /**
     * Set primaryPhoneNumberExtension
     *
     * @param string $primaryPhoneNumberExtension
     *
     * @return ContactPerson
     */
    public function setPrimaryPhoneNumberExtension(?string $primaryPhoneNumberExtension): ContactPerson
    {
        return $this->setTrigger('primaryPhoneNumberExtension', $primaryPhoneNumberExtension);
    }

    /**
     * Get primaryPhoneNumberPurpose
     *
     * @return string
     */
    public function getPrimaryPhoneNumberPurpose(): ?string
    {
        return $this->primaryPhoneNumberPurpose;
    }

    /**
     * Set primaryPhoneNumberPurpose
     *
     * @param string $primaryPhoneNumberPurpose
     *
     * @return ContactPerson
     */
    public function setPrimaryPhoneNumberPurpose(?string $primaryPhoneNumberPurpose): ContactPerson
    {
        return $this->setTrigger('primaryPhoneNumberPurpose', $primaryPhoneNumberPurpose);
    }

    /**
     * Get primaryTelex
     *
     * @return string
     */
    public function getPrimaryTelex(): ?string
    {
        return $this->primaryTelex;
    }

    /**
     * Set primaryTelex
     *
     * @param string $primaryTelex
     *
     * @return ContactPerson
     */
    public function setPrimaryTelex(?string $primaryTelex): ContactPerson
    {
        return $this->setTrigger('primaryTelex', $primaryTelex);
    }

    /**
     * Get primaryTelexDescription
     *
     * @return string
     */
    public function getPrimaryTelexDescription(): ?string
    {
        return $this->primaryTelexDescription;
    }

    /**
     * Set primaryTelexDescription
     *
     * @param string $primaryTelexDescription
     *
     * @return ContactPerson
     */
    public function setPrimaryTelexDescription(?string $primaryTelexDescription): ContactPerson
    {
        return $this->setTrigger('primaryTelexDescription', $primaryTelexDescription);
    }

    /**
     * Get primaryTelexPurpose
     *
     * @return string
     */
    public function getPrimaryTelexPurpose(): ?string
    {
        return $this->primaryTelexPurpose;
    }

    /**
     * Set primaryTelexPurpose
     *
     * @param string $primaryTelexPurpose
     *
     * @return ContactPerson
     */
    public function setPrimaryTelexPurpose(?string $primaryTelexPurpose): ContactPerson
    {
        return $this->setTrigger('primaryTelexPurpose', $primaryTelexPurpose);
    }
}
