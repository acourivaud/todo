<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 04/04/17
 * Time: 14:37
 */

namespace Todotoday\AccountBundle\Entity\Api\Microsoft;

use Actiane\ApiConnectorBundle\Lib\EntityProxyTrait;
use Actiane\ApiConnectorBundle\Lib\Mapping as APIConnector;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class CustomerBankAccounts
 * @SuppressWarnings(PHPMD)
 * @APIConnector\Entity(path="CustomerBankAccounts", repositoryId="todotoday.account.repository.api.customer_bank_account")
 * @package Todotoday\AccountBundle\Entity\Api\Microsoft
 * @Serializer\ExclusionPolicy("all")
 */
class CustomerBankAccount
{
    use EntityProxyTrait;

    /**
     * @var string
     * @APIConnector\Id()
     * @APIConnector\Column(name="BankAccountId", type="string")
     * @Serializer\Expose()
     */
    protected $bankAccountID;

    /**
     * @var string
     *
     * @APIConnector\Column(name="RoutingNumberType", type="string",
     *                                                mType="Microsoft.Dynamics.DataEntities.BankCodeType")
     * @Serializer\Expose()
     */
    protected $routingNumberType;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="AddressValidFrom", type="datetimez")
     * @Serializer\Expose()
     */
    protected $addressValidFrom;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ContactEmailAddress", type="string")
     * @Serializer\Expose()
     */
    protected $contactEmailAddress;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ContactTelexNumber", type="string")
     * @Serializer\Expose()
     */
    protected $contactTelexNumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="SWIFTCode", type="string")
     * @Serializer\Expose()
     * @Assert\Bic()
     */
    protected $swiftCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ContactInternetAddress", type="string")
     * @Serializer\Expose()
     */
    protected $contactInternetAddress;

    /**
     * @var string
     *
     * @APIConnector\Column(name="AddressCity", type="string")
     * @Serializer\Expose()
     */
    protected $addressCity;

    /**
     * @var string
     *
     * @APIConnector\Column(name="AddressState", type="string")
     * @Serializer\Expose()
     */
    protected $addressState;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ESR", type="string")
     * @Serializer\Expose()
     */
    protected $esr;

    /**
     * @var string
     *
     * @APIConnector\Column(name="FullAddress", type="string")
     * @Serializer\Expose()
     */
    protected $fullAddress;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ContactPersonName", type="string")
     * @Serializer\Expose()
     */
    protected $contactPersonName;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ContactPager", type="string")
     * @Serializer\Expose()
     */
    protected $contactPager;

    /**
     * @var string
     *
     * @APIConnector\Column(name="AddressValidTo", type="string")
     * @Serializer\Expose()
     */
    protected $addressValidTo;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="RateOfExchangeReference", type="datetimez")
     * @Serializer\Expose()
     */
    protected $rateOfExchangeReference;

    /**
     * @var string
     *
     * @APIConnector\Column(name="PaymentTextCode", type="string")
     * @Serializer\Expose()
     */
    protected $paymentTextCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="BankAccountType", type="string",
     *                                              mType="Microsoft.Dynamics.DataEntities.BankAccountType")
     * @Serializer\Expose()
     */
    protected $bankAccountType;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ContactPhoneNumber", type="string")
     * @Serializer\Expose()
     */
    protected $contactPhoneNumber;

    /**
     * @var float
     *
     * @APIConnector\Column(name="AddressLatitude", type="float")
     * @Serializer\Expose()
     */
    protected $addressLatitude;

    /**
     * @var string
     *
     * @APIConnector\Column(name="BankSpecificSymbol", type="string")
     * @Serializer\Expose()
     */
    protected $bankSpecificSymbol;

    /**
     * @var string
     *
     * @APIConnector\Column(name="MessageToBank", type="string")
     * @Serializer\Expose()
     */
    protected $messageToBank;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CurrencyCode", type="string")
     * @Serializer\Expose()
     */
    protected $currencyCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="BankContractAccount", type="string")
     * @Serializer\Expose()
     */
    protected $bankContractAccount;

    /**
     * @var string
     *
     * @APIConnector\Column(name="AddressDescription", type="string")
     * @Serializer\Expose()
     */
    protected $addressDescription;

    /**
     * @var string
     *
     * @APIConnector\Column(name="AddressLocationId", type="string")
     * @Serializer\Expose()
     */
    protected $addressLocationID;

    /**
     * @var string
     *
     * @APIConnector\Column(name="BankName", type="string")
     * @Serializer\Expose()
     */
    protected $bankName;

    /**
     * @var string
     *
     * @APIConnector\Column(name="AddressDistrictName", type="string")
     * @Serializer\Expose()
     */
    protected $addressDistrictName;

    /**
     * @var string
     * @APIConnector\Id()
     * @APIConnector\Column(name="CustomerAccountNumber", type="string")
     * @Serializer\Expose()
     */
    protected $customerAccountNumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ContactEmailAddressForSendingSMS", type="string")
     * @Serializer\Expose()
     */
    protected $contactEmailAddressForSendingSMS;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ContactMobilePhoneNumber", type="string")
     * @Serializer\Expose()
     */
    protected $contactMobilePhoneNumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ContactPhoneNumberExtension", type="string")
     * @Serializer\Expose()
     */
    protected $contactPhoneNumberExtension;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ContactFaxNumber", type="string")
     * @Serializer\Expose()
     */
    protected $contactFaxNumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="AddressStreet", type="string")
     * @Serializer\Expose()
     */
    protected $addressStreet;

    /**
     * @var string
     *
     * @APIConnector\Column(name="AddressCounty", type="string")
     * @Serializer\Expose()
     */
    protected $addressCounty;

    /**
     * @var float
     *
     * @APIConnector\Column(name="CrossRate", type="float")
     * @Serializer\Expose()
     */
    protected $crossRate;

    /**
     * @var string
     *
     * @APIConnector\Column(name="IBAN", type="string")
     * @Serializer\Expose()
     * @Assert\Iban(
     *     message="This is not a valid International Bank Account Number (IBAN)."
     * )
     */
    protected $iban;

    /**
     * @var string
     *
     * @APIConnector\Column(name="BankGroupId", type="string")
     * @Serializer\Expose()
     */
    protected $bankGroupID;

    /**
     * @var string
     *
     * @APIConnector\Column(name="AddressTimeZone", type="string", mType="Microsoft.Dynamics.DataEntities.Timezone")
     * @Serializer\Expose()
     */
    protected $addressTimeZone;

    /**
     * @var float
     *
     * @APIConnector\Column(name="AddressLongitude", type="float")
     * @Serializer\Expose()
     */
    protected $addressLongitude;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @APIConnector\Column(name="dataAreaId", type="string")
     * @Serializer\Expose()
     */
    protected $dataAreaId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="RoutingNumber", type="string")
     * @Serializer\Expose()
     */
    protected $routingNumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="AddressZipCode", type="string")
     * @Serializer\Expose()
     */
    protected $addressZipCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="BankConstantSymbol", type="string")
     * @Serializer\Expose()
     */
    protected $bankConstantSymbol;

    /**
     * @var string
     *
     * @APIConnector\Column(name="AddressCountryRegionId", type="string")
     * @Serializer\Expose()
     */
    protected $addressCountryRegionID;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ControlInternalNumber", type="string")
     * @Serializer\Expose()
     */
    protected $controlInternalNumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="BankAccountNumber", type="string")
     * @Serializer\Expose()
     */
    protected $bankAccountNumber;

    /**
     * @return string
     */
    public function getAddressCity(): ?string
    {
        return $this->addressCity;
    }

    /**
     * @param string $addressCity
     *
     * @return CustomerBankAccount
     */
    public function setAddressCity(string $addressCity): CustomerBankAccount
    {
        return $this->setTrigger('addressCity', $addressCity);
    }

    /**
     * @return string
     */
    public function getAddressCountryRegionID(): ?string
    {
        return $this->addressCountryRegionID;
    }

    /**
     * @param string $addressCountryRegionID
     *
     * @return CustomerBankAccount
     */
    public function setAddressCountryRegionID(string $addressCountryRegionID): CustomerBankAccount
    {
        return $this->setTrigger('addressCountryRegionID', $addressCountryRegionID);
    }

    /**
     * @return string
     */
    public function getAddressCounty(): ?string
    {
        return $this->addressCounty;
    }

    /**
     * @param string $addressCounty
     *
     * @return CustomerBankAccount
     */
    public function setAddressCounty(string $addressCounty): CustomerBankAccount
    {
        return $this->setTrigger('addressCounty', $addressCounty);
    }

    /**
     * @return string
     */
    public function getAddressDescription(): ?string
    {
        return $this->addressDescription;
    }

    /**
     * @param string $addressDescription
     *
     * @return CustomerBankAccount
     */
    public function setAddressDescription(string $addressDescription): CustomerBankAccount
    {
        return $this->setTrigger('addressDescription', $addressDescription);
    }

    /**
     * @return string
     */
    public function getAddressDistrictName(): ?string
    {
        return $this->addressDistrictName;
    }

    /**
     * @param string $addressDistrictName
     *
     * @return CustomerBankAccount
     */
    public function setAddressDistrictName(string $addressDistrictName): CustomerBankAccount
    {
        return $this->setTrigger('addressDistrictName', $addressDistrictName);
    }

    /**
     * @return float
     */
    public function getAddressLatitude(): ?float
    {
        return $this->addressLatitude;
    }

    /**
     * @param float $addressLatitude
     *
     * @return CustomerBankAccount
     */
    public function setAddressLatitude(float $addressLatitude): CustomerBankAccount
    {
        return $this->setTrigger('addressLatitude', $addressLatitude);
    }

    /**
     * @return string
     */
    public function getAddressLocationID(): ?string
    {
        return $this->addressLocationID;
    }

    /**
     * @param string $addressLocationID
     *
     * @return CustomerBankAccount
     */
    public function setAddressLocationID(string $addressLocationID): CustomerBankAccount
    {
        return $this->setTrigger('addressLocationID', $addressLocationID);
    }

    /**
     * @return float
     */
    public function getAddressLongitude(): ?float
    {
        return $this->addressLongitude;
    }

    /**
     * @param float $addressLongitude
     *
     * @return CustomerBankAccount
     */
    public function setAddressLongitude(float $addressLongitude): CustomerBankAccount
    {
        return $this->setTrigger('addressLongitude', $addressLongitude);
    }

    /**
     * @return string
     */
    public function getAddressState(): ?string
    {
        return $this->addressState;
    }

    /**
     * @param string $addressState
     *
     * @return CustomerBankAccount
     */
    public function setAddressState(string $addressState): CustomerBankAccount
    {
        return $this->setTrigger('addressState', $addressState);
    }

    /**
     * @return string
     */
    public function getAddressStreet(): ?string
    {
        return $this->addressStreet;
    }

    /**
     * @param string $addressStreet
     *
     * @return CustomerBankAccount
     */
    public function setAddressStreet(string $addressStreet): CustomerBankAccount
    {
        return $this->setTrigger('addressStreet', $addressStreet);
    }

    /**
     * @return string
     */
    public function getAddressTimeZone(): ?string
    {
        return $this->addressTimeZone;
    }

    /**
     * @param string $addressTimeZone
     *
     * @return CustomerBankAccount
     */
    public function setAddressTimeZone(string $addressTimeZone): CustomerBankAccount
    {
        return $this->setTrigger('addressTimeZone', $addressTimeZone);
    }

    /**
     * @return \DateTime
     */
    public function getAddressValidFrom(): ?\DateTime
    {
        return $this->addressValidFrom;
    }

    /**
     * @param \DateTime $addressValidFrom
     *
     * @return CustomerBankAccount
     */
    public function setAddressValidFrom(\DateTime $addressValidFrom): CustomerBankAccount
    {
        return $this->setTrigger('addressValidFrom', $addressValidFrom);
    }

    /**
     * @return string
     */
    public function getAddressValidTo(): ?string
    {
        return $this->addressValidTo;
    }

    /**
     * @param string $addressValidTo
     *
     * @return CustomerBankAccount
     */
    public function setAddressValidTo(string $addressValidTo): CustomerBankAccount
    {
        return $this->setTrigger('addressValidTo', $addressValidTo);
    }

    /**
     * @return string
     */
    public function getAddressZipCode(): ?string
    {
        return $this->addressZipCode;
    }

    /**
     * @param string $addressZipCode
     *
     * @return CustomerBankAccount
     */
    public function setAddressZipCode(string $addressZipCode): CustomerBankAccount
    {
        return $this->setTrigger('addressZipCode', $addressZipCode);
    }

    /**
     * @return string
     */
    public function getBankAccountID(): ?string
    {
        return $this->bankAccountID;
    }

    /**
     * @param string $bankAccountID
     *
     * @return CustomerBankAccount
     */
    public function setBankAccountID(string $bankAccountID): CustomerBankAccount
    {
        return $this->setTrigger('bankAccountID', $bankAccountID);
    }

    /**
     * @return string
     */
    public function getBankAccountNumber(): ?string
    {
        return $this->bankAccountNumber;
    }

    /**
     * @param string $bankAccountNumber
     *
     * @return CustomerBankAccount
     */
    public function setBankAccountNumber(string $bankAccountNumber): CustomerBankAccount
    {
        return $this->setTrigger('bankAccountNumber', $bankAccountNumber);
    }

    /**
     * @return string
     */
    public function getBankAccountType(): ?string
    {
        return $this->bankAccountType;
    }

    /**
     * @param string $bankAccountType
     *
     * @return CustomerBankAccount
     */
    public function setBankAccountType(string $bankAccountType): CustomerBankAccount
    {
        return $this->setTrigger('bankAccountType', $bankAccountType);
    }

    /**
     * @return string
     */
    public function getBankConstantSymbol(): ?string
    {
        return $this->bankConstantSymbol;
    }

    /**
     * @param string $bankConstantSymbol
     *
     * @return CustomerBankAccount
     */
    public function setBankConstantSymbol(string $bankConstantSymbol): CustomerBankAccount
    {
        return $this->setTrigger('bankConstantSymbol', $bankConstantSymbol);
    }

    /**
     * @return string
     */
    public function getBankContractAccount(): ?string
    {
        return $this->bankContractAccount;
    }

    /**
     * @param string $bankContractAccount
     *
     * @return CustomerBankAccount
     */
    public function setBankContractAccount(string $bankContractAccount): CustomerBankAccount
    {
        return $this->setTrigger('bankContractAccount', $bankContractAccount);
    }

    /**
     * @return string
     */
    public function getBankGroupID(): ?string
    {
        return $this->bankGroupID;
    }

    /**
     * @param string $bankGroupID
     *
     * @return CustomerBankAccount
     */
    public function setBankGroupID(string $bankGroupID): CustomerBankAccount
    {
        return $this->setTrigger('bankGroupID', $bankGroupID);
    }

    /**
     * @return string|null
     */
    public function getBankName(): ?string
    {
        return $this->bankName;
    }

    /**
     * @param string|null $bankName
     *
     * @return CustomerBankAccount
     */
    public function setBankName(?string $bankName): CustomerBankAccount
    {
        return $this->setTrigger('bankName', $bankName);
    }

    /**
     * @return string
     */
    public function getBankSpecificSymbol(): ?string
    {
        return $this->bankSpecificSymbol;
    }

    /**
     * @param string $bankSpecificSymbol
     *
     * @return CustomerBankAccount
     */
    public function setBankSpecificSymbol(string $bankSpecificSymbol): CustomerBankAccount
    {
        return $this->setTrigger('bankSpecificSymbol', $bankSpecificSymbol);
    }

    /**
     * @return string
     */
    public function getContactEmailAddress(): ?string
    {
        return $this->contactEmailAddress;
    }

    /**
     * @param string $contactEmailAddress
     *
     * @return CustomerBankAccount
     */
    public function setContactEmailAddress(string $contactEmailAddress): CustomerBankAccount
    {
        return $this->setTrigger('contactEmailAddress', $contactEmailAddress);
    }

    /**
     * @return string
     */
    public function getContactEmailAddressForSendingSMS(): ?string
    {
        return $this->contactEmailAddressForSendingSMS;
    }

    /**
     * @param string $contactEmailAddressForSendingSMS
     *
     * @return CustomerBankAccount
     */
    public function setContactEmailAddressForSendingSMS(string $contactEmailAddressForSendingSMS): CustomerBankAccount
    {
        return $this->setTrigger('contactEmailAddressForSendingSMS', $contactEmailAddressForSendingSMS);
    }

    /**
     * @return string
     */
    public function getContactFaxNumber(): ?string
    {
        return $this->contactFaxNumber;
    }

    /**
     * @param string $contactFaxNumber
     *
     * @return CustomerBankAccount
     */
    public function setContactFaxNumber(string $contactFaxNumber): CustomerBankAccount
    {
        return $this->setTrigger('contactFaxNumber', $contactFaxNumber);
    }

    /**
     * @return string
     */
    public function getContactInternetAddress(): ?string
    {
        return $this->contactInternetAddress;
    }

    /**
     * @param string $contactInternetAddress
     *
     * @return CustomerBankAccount
     */
    public function setContactInternetAddress(string $contactInternetAddress): CustomerBankAccount
    {
        return $this->setTrigger('contactInternetAddress', $contactInternetAddress);
    }

    /**
     * @return string
     */
    public function getContactMobilePhoneNumber(): ?string
    {
        return $this->contactMobilePhoneNumber;
    }

    /**
     * @param string $contactMobilePhoneNumber
     *
     * @return CustomerBankAccount
     */
    public function setContactMobilePhoneNumber(string $contactMobilePhoneNumber): CustomerBankAccount
    {
        return $this->setTrigger('contactMobilePhoneNumber', $contactMobilePhoneNumber);
    }

    /**
     * @return string
     */
    public function getContactPager(): ?string
    {
        return $this->contactPager;
    }

    /**
     * @param string $contactPager
     *
     * @return CustomerBankAccount
     */
    public function setContactPager(string $contactPager): CustomerBankAccount
    {
        return $this->setTrigger('contactPager', $contactPager);
    }

    /**
     * @return string
     */
    public function getContactPersonName(): ?string
    {
        return $this->contactPersonName;
    }

    /**
     * @param string $contactPersonName
     *
     * @return CustomerBankAccount
     */
    public function setContactPersonName(string $contactPersonName): CustomerBankAccount
    {
        return $this->setTrigger('contactPersonName', $contactPersonName);
    }

    /**
     * @return string
     */
    public function getContactPhoneNumber(): ?string
    {
        return $this->contactPhoneNumber;
    }

    /**
     * @param string $contactPhoneNumber
     *
     * @return CustomerBankAccount
     */
    public function setContactPhoneNumber(string $contactPhoneNumber): CustomerBankAccount
    {
        return $this->setTrigger('contactPhoneNumber', $contactPhoneNumber);
    }

    /**
     * @return string
     */
    public function getContactPhoneNumberExtension(): ?string
    {
        return $this->contactPhoneNumberExtension;
    }

    /**
     * @param string $contactPhoneNumberExtension
     *
     * @return CustomerBankAccount
     */
    public function setContactPhoneNumberExtension(string $contactPhoneNumberExtension): CustomerBankAccount
    {
        return $this->setTrigger('contactPhoneNumberExtension', $contactPhoneNumberExtension);
    }

    /**
     * @return string
     */
    public function getContactTelexNumber(): ?string
    {
        return $this->contactTelexNumber;
    }

    /**
     * @param string $contactTelexNumber
     *
     * @return CustomerBankAccount
     */
    public function setContactTelexNumber(string $contactTelexNumber): CustomerBankAccount
    {
        return $this->setTrigger('contactTelexNumber', $contactTelexNumber);
    }

    /**
     * @return string
     */
    public function getControlInternalNumber(): ?string
    {
        return $this->controlInternalNumber;
    }

    /**
     * @param string $controlInternalNumber
     *
     * @return CustomerBankAccount
     */
    public function setControlInternalNumber(string $controlInternalNumber): CustomerBankAccount
    {
        return $this->setTrigger('controlInternalNumber', $controlInternalNumber);
    }

    /**
     * @return float
     */
    public function getCrossRate(): ?float
    {
        return $this->crossRate;
    }

    /**
     * @param float $crossRate
     *
     * @return CustomerBankAccount
     */
    public function setCrossRate(float $crossRate): CustomerBankAccount
    {
        return $this->setTrigger('crossRate', $crossRate);
    }

    /**
     * @return string
     */
    public function getCurrencyCode(): ?string
    {
        return $this->currencyCode;
    }

    /**
     * @param string $currencyCode
     *
     * @return CustomerBankAccount
     */
    public function setCurrencyCode(string $currencyCode): CustomerBankAccount
    {
        return $this->setTrigger('currencyCode', $currencyCode);
    }

    /**
     * @return string
     */
    public function getCustomerAccountNumber(): ?string
    {
        return $this->customerAccountNumber;
    }

    /**
     * @param string $customerAccountNumber
     *
     * @return CustomerBankAccount
     */
    public function setCustomerAccountNumber(string $customerAccountNumber): CustomerBankAccount
    {
        return $this->setTrigger('customerAccountNumber', $customerAccountNumber);
    }

    /**
     * @return string
     */
    public function getDataAreaId(): ?string
    {
        return $this->dataAreaId;
    }

    /**
     * @param string $dataAreaId
     *
     * @return CustomerBankAccount
     */
    public function setDataAreaId(string $dataAreaId): CustomerBankAccount
    {
        return $this->setTrigger('dataAreaId', $dataAreaId);
    }

    /**
     * @return string
     */
    public function getEsr(): ?string
    {
        return $this->esr;
    }

    /**
     * @param string $esr
     *
     * @return CustomerBankAccount
     */
    public function setEsr(string $esr): CustomerBankAccount
    {
        return $this->setTrigger('esr', $esr);
    }

    /**
     * @return string
     */
    public function getFullAddress(): ?string
    {
        return $this->fullAddress;
    }

    /**
     * @param string $fullAddress
     *
     * @return CustomerBankAccount
     */
    public function setFullAddress(string $fullAddress): CustomerBankAccount
    {
        return $this->setTrigger('fullAddress', $fullAddress);
    }

    /**
     * @return string|null
     */
    public function getIban(): ?string
    {
        return $this->iban;
    }

    /**
     * @param string|null $iban
     *
     * @return CustomerBankAccount
     */
    public function setIban(?string $iban): CustomerBankAccount
    {
        return $this->setTrigger('iban', $iban);
    }

    /**
     * @return string
     */
    public function getMessageToBank(): ?string
    {
        return $this->messageToBank;
    }

    /**
     * @param string $messageToBank
     *
     * @return CustomerBankAccount
     */
    public function setMessageToBank(string $messageToBank): CustomerBankAccount
    {
        return $this->setTrigger('messageToBank', $messageToBank);
    }

    /**
     * @return string
     */
    public function getPaymentTextCode(): ?string
    {
        return $this->paymentTextCode;
    }

    /**
     * @param string $paymentTextCode
     *
     * @return CustomerBankAccount
     */
    public function setPaymentTextCode(string $paymentTextCode): CustomerBankAccount
    {
        return $this->setTrigger('paymentTextCode', $paymentTextCode);
    }

    /**
     * @return \DateTime
     */
    public function getRateOfExchangeReference(): ?\DateTime
    {
        return $this->rateOfExchangeReference;
    }

    /**
     * @param \DateTime $rateOfExchangeReference
     *
     * @return CustomerBankAccount
     */
    public function setRateOfExchangeReference(\DateTime $rateOfExchangeReference): CustomerBankAccount
    {
        return $this->setTrigger('rateOfExchangeReference', $rateOfExchangeReference);
    }

    /**
     * @return string
     */
    public function getRoutingNumber(): ?string
    {
        return $this->routingNumber;
    }

    /**
     * @param string $routingNumber
     *
     * @return CustomerBankAccount
     */
    public function setRoutingNumber(string $routingNumber): CustomerBankAccount
    {
        return $this->setTrigger('routingNumber', $routingNumber);
    }

    /**
     * @return string
     */
    public function getRoutingNumberType(): ?string
    {
        return $this->routingNumberType;
    }

    /**
     * @param string $routingNumberType
     *
     * @return CustomerBankAccount
     */
    public function setRoutingNumberType(string $routingNumberType): CustomerBankAccount
    {
        return $this->setTrigger('routingNumberType', $routingNumberType);
    }

    /**
     * Get stripeCardId
     *
     * @return string
     */
    public function getStripeCardId(): ?string
    {
        return $this->stripeCardId;
    }

    /**
     * Set stripeCardId
     *
     * @param string $stripeCardId
     *
     * @return CustomerBankAccount
     */
    public function setStripeCardId(string $stripeCardId): self
    {
        $this->stripeCardId = $stripeCardId;

        return $this;
    }

    /**
     * Get stripeCustomerId
     *
     * @return string
     */
    public function getStripeCustomerId(): ?string
    {
        return $this->stripeCustomerId;
    }

    /**
     * Set stripeCustomerId
     *
     * @param string $stripeCustomerId
     *
     * @return CustomerBankAccount
     */
    public function setStripeCustomerId(string $stripeCustomerId): self
    {
        $this->stripeCustomerId = $stripeCustomerId;

        return $this;
    }

    /**
     * @return string
     */
    public function getSwiftCode(): ?string
    {
        return $this->swiftCode;
    }

    /**
     * @param string|null $swiftCode
     *
     * @return CustomerBankAccount
     */
    public function setSwiftCode(?string $swiftCode): CustomerBankAccount
    {
        return $this->setTrigger('swiftCode', $swiftCode);
    }
}
