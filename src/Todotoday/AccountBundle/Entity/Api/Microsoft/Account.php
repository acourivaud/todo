<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 16/02/2017
 * Time: 15:06
 */

namespace Todotoday\AccountBundle\Entity\Api\Microsoft;

use Actiane\ApiConnectorBundle\Lib\EntityProxyTrait;
use Actiane\ApiConnectorBundle\Lib\Mapping as APIConnector;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class Account
 *
 * @SuppressWarnings(PHPMD)
 * @APIConnector\Entity(path="TDTDCustomers", repositoryId="todotoday.account.repository.api.account")
 * @Serializer\ExclusionPolicy("all")
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Entity\Api\Microsoft
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class Account
{
    use EntityProxyTrait;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CustomerGroupId", type="string", doctrineAttribute="customerGroupString")
     * @Serializer\Expose()
     * @Serializer\Expose()
     */
    protected $customerGroupId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="PrimaryContactPhonePurpose", type="string")
     * @Serializer\Expose()
     */
    protected $primaryContactPhonePurpose;

    /**
     * @var string
     *
     * @APIConnector\Column(name="PersonChildrenNames", type="string")
     * @Serializer\Expose()
     */
    protected $personChildrenNames;

    /**
     * @var string
     *
     * @APIConnector\Column(name="AddressTimeZone", type="string")
     * @Serializer\Expose()
     */
    protected $addressTimeZone;

    /**
     * @var string
     *
     * @APIConnector\Column(name="AddressDescription", type="string")
     * @Serializer\Expose()
     */
    protected $addressDescription;

    /**
     * @var string
     *
     * @APIConnector\Column(name="AddressLocationRoles", type="string")
     * @Serializer\Expose()
     */
    protected $addressLocationRoles;

    /**
     * @var string
     *
     * @APIConnector\Column(name="PrimaryContactFaxExtension", type="string")
     * @Serializer\Expose()
     */
    protected $primaryContactFaxExtension;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @APIConnector\Column(name="CustomerAccount", type="string", doctrineAttribute="customerAccount")
     * @Serializer\Expose()
     */
    protected $customerAccount;

    /**
     * @var string
     *
     * @APIConnector\Column(name="PrimaryContactPhoneExtension", type="string", doctrineAttribute="phoneExtension")
     * @Serializer\Expose()
     */
    protected $primaryContactPhoneExtension;

    /**
     * @var string
     *
     * @APIConnector\Column(name="WeddingMonth", type="string")
     * @Serializer\Expose()
     */
    protected $weddingMonth;

    /**
     * @var string
     *
     * @APIConnector\Column(name="PrimaryContactPhoneIsMobile", type="string")
     * @Serializer\Expose()
     */
    protected $primaryContactPhoneIsMobile;

    /**
     * @var string
     *
     * @APIConnector\Column(name="PrimaryContactFaxPurpose", type="string")
     * @Serializer\Expose()
     */
    protected $primaryContactFaxPurpose;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="AddressValidFrom", type="datetimez")
     * @Serializer\Expose()
     */
    protected $addressValidFrom;

    /**
     * @var string
     *
     * @APIConnector\Column(name="PrimaryContactTelexDescription", type="string")
     * @Serializer\Expose()
     */
    protected $primaryContactTelexDescription;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ReceiptOption", type="string")
     * @Serializer\Expose()
     */
    protected $receiptOption;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DefaultDimensionDisplayValue", type="string")
     * @Serializer\Expose()
     */
    protected $defaultDimensionDisplayValue;

    /**
     * @var int
     *
     * @APIConnector\Column(name="WeddingYear", type="int")
     * @Serializer\Expose()
     */
    protected $weddingYear;

    /**
     * @var string
     *
     * @APIConnector\Column(name="PrimaryContactPhoneDescription", type="string")
     * @Serializer\Expose()
     */
    protected $primaryContactPhoneDescription;

    /**
     * @var string
     *
     * @APIConnector\Column(name="PersonalTitle", type="string", doctrineAttribute="titleString")
     * @Serializer\Expose()
     */
    protected $personalTitle;

    /**
     * @var string
     *
     * @APIConnector\Column(name="VIN", type="string")
     * @Serializer\Expose()
     */
    protected $vin;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ReceiptCalendar", type="string")
     * @Serializer\Expose()
     */
    protected $receiptCalendar;

    /**
     * @var string
     *
     * @APIConnector\Column(name="PrimaryContactEmailDescription", type="string")
     * @Serializer\Expose()
     */
    protected $primaryContactEmailDescription;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ParkingSpot", type="string")
     * @Serializer\Expose()
     */
    protected $parkingSpot;

    /**
     * @var string
     *
     * @APIConnector\Column(name="PersonBirthMonth", type="string", doctrineAttribute="birthDateMonth")
     * @Serializer\Expose()
     */
    protected $personBirthMonth;

    /**
     * @var string
     *
     * @APIConnector\Column(name="PrimaryContactEmailPurpose", type="string")
     * @Serializer\Expose()
     */
    protected $primaryContactEmailPurpose;

    /**
     * @var string
     *
     * @APIConnector\Column(name="PrimaryContactTelex", type="string")
     * @Serializer\Expose()
     */
    protected $primaryContactTelex;

    /**
     * @var string
     *
     * @APIConnector\Column(name="Assureur", type="string")
     * @Serializer\Expose()
     */
    protected $insurer;

    /**
     * @var string
     *
     * @APIConnector\Column(name="OnHoldStatus", type="string")
     * @Serializer\Expose()
     */
    protected $onHoldStatus;

    /**
     * @var int
     *
     * @APIConnector\Column(name="PersonAnniversaryYear", type="int", doctrineAttribute="EmployeeBirthDateYear")
     * @Serializer\Expose()
     */
    protected $personAnniversaryYear;

    /**
     * @var string
     *
     * @APIConnector\Column(name="PartyNumber", type="string", doctrineAttribute="partyNumber")
     * @Serializer\Expose()
     */
    protected $partyNumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="PrimaryContactPhone", type="string", doctrineAttribute="phone")
     * @Serializer\Expose()
     */
    protected $primaryContactPhone;

    /**
     * @var string
     *
     * @APIConnector\Column(name="AddressState", type="string", doctrineAttribute="addressState")
     * @Serializer\Expose()
     */
    protected $addressState;

    /**
     * @var string
     *
     * @APIConnector\Column(name="PartyType", type="string")
     * @Serializer\Expose()
     */
    protected $partyType;

    /**
     * @var string
     *
     * @APIConnector\Column(name="AddressCity", type="string", doctrineAttribute="addressCity")
     * @Serializer\Expose()
     */
    protected $addressCity;

    /**
     * @var int
     *
     * @APIConnector\Column(name="VehicleModel", type="int")
     * @Serializer\Expose()
     */
    protected $vehicleModel;

    /**
     * @var string
     *
     * @APIConnector\Column(name="AddressBooks", type="string")
     * @Serializer\Expose()
     */
    protected $addressBooks;

    /**
     * @var string
     *
     * @APIConnector\Column(name="AddressZipCode", type="string", doctrineAttribute="addressZipCode")
     * @Serializer\Expose()
     */
    protected $addressZipCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="PrimaryContactEmail", type="string")
     * @Serializer\Expose()
     */
    protected $primaryContactEmail;

    /**
     * @var string
     *
     * @APIConnector\Column(name="Name", type="string")
     * @Serializer\Expose()
     */
    protected $name;

    /**
     * @var string
     *
     * @APIConnector\Column(name="SalesCurrencyCode", type="string")
     * @Serializer\Expose()
     */
    protected $salesCurrencyCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="SalesMemo", type="string")
     * @Serializer\Expose()
     */
    protected $salesMemo;

    /**
     * @var string
     *
     * @APIConnector\Column(name="WarehouseId", type="string")
     * @Serializer\Expose()
     */
    protected $warehouseId;

    /**
     * @var int
     *
     * @APIConnector\Column(name="PersonBirthYear", type="int", doctrineAttribute="birthDateYear")
     * @Serializer\Expose()
     */
    protected $personBirthYear;

    /**
     * @var string
     *
     * @APIConnector\Column(name="PrimaryContactTelexPurpose", type="string")
     * @Serializer\Expose()
     */
    protected $primaryContactTelexPurpose;

    /**
     * @var string
     *
     * @APIConnector\Column(name="AddressStreet", type="string", doctrineAttribute="addressStreet")
     * @Serializer\Expose()
     */
    protected $addressStreet;

    /**
     * @var int
     *
     * @APIConnector\Column(name="WeddingDay", type="int")
     * @Serializer\Expose()
     */
    protected $weddingDay;

    /**
     * @var string
     *
     * @APIConnector\Column(name="PersonAnniversaryMonth", type="string", doctrineAttribute="EmployeeBirthDateMonth")
     * @Serializer\Expose()
     */
    protected $personAnniversaryMonth;

    /**
     * @var string
     *
     * @APIConnector\Column(name="FullPrimaryAddress", type="string")
     * @Serializer\Expose()
     */
    protected $fullPrimaryAddress;

    /**
     * @var string
     *
     * @APIConnector\Column(name="PersonMaritalStatus", type="string", doctrineAttribute="personMaritalStatus")
     * @Serializer\Expose()
     */
    protected $personMaritalStatus;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ReceiptEmail", type="string")
     * @Serializer\Expose()
     */
    protected $receiptEmail;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CodeCasier", type="string", doctrineAttribute="lockerCode")
     * @Serializer\Expose()
     */
    protected $lockerCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="AddressCountryRegionId", type="string", doctrineAttribute="addressCountryRegionId")
     * @Serializer\Expose()
     */
    protected $addressCountryRegionId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="AddressDistrictName", type="string")
     * @Serializer\Expose()
     */
    protected $addressDistrictName;

    /**
     * @var string
     *
     * @APIConnector\Column(name="Blocked", type="string")
     * @Serializer\Expose()
     */
    protected $blocked;

    /**
     * @var string
     *
     * @APIConnector\Column(name="LanguageId", type="string", doctrineAttribute="language")
     * @Serializer\Expose()
     */
    protected $languageId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="MiddleName", type="string", doctrineAttribute="middleName")
     * @Serializer\Expose()
     */
    protected $middleName;

    /**
     * @var int
     *
     * @APIConnector\Column(name="PersonBirthDay", type="int", doctrineAttribute="birthDateDay")
     * @Serializer\Expose()
     */
    protected $personBirthDay;

    /**
     * @var string
     *
     * @APIConnector\Column(name="PrimaryContactFax", type="string", doctrineAttribute="primaryContactFax")
     * @Serializer\Expose()
     */
    protected $primaryContactFax;

    /**
     * @var int
     *
     * @APIConnector\Column(name="PersonAnniversaryDay", type="int", doctrineAttribute="EmployeeBirthDateDay")
     * @Serializer\Expose()
     */
    protected $personAnniversaryDay;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @APIConnector\Column(name="dataAreaId", type="string", doctrineAttribute="dataAreaId")
     * @Serializer\Expose()
     */
    protected $dataAreaId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="TaxExemptNumber", type="string")
     * @Serializer\Expose()
     */
    protected $taxExemptNumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="PersonGender",
     *     type="string", mType="Microsoft.Dynamics.DataEntities.Gender",
     *     doctrineAttribute="personGenderString")
     * @Serializer\Expose()
     */
    protected $personGender;

    /**
     * @var string
     *
     * @APIConnector\Column(name="NumAdherent", type="string")
     * @Serializer\Expose()
     */
    protected $numAdherent;

    /**
     * @var string
     *
     * @APIConnector\Column(name="PrimaryContactFaxDescription", type="string")
     * @Serializer\Expose()
     */
    protected $primaryContactFaxDescription;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ContactPersonId", type="string")
     * @Serializer\Expose()
     */
    protected $contactPersonId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="AddressCounty", type="string")
     * @Serializer\Expose()
     */
    protected $addressCounty;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="AdhesionDate", type="datetimez")
     * @Serializer\Expose()
     */
    protected $adhesionDate;

    /**
     * @var string
     *
     * @APIConnector\Column(name="VehicleColor", type="string")
     * @Serializer\Expose()
     */
    protected $vehicleColor;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="AddressValidTo", type="datetimez")
     * @Serializer\Expose()
     */
    protected $addressValidTo;

    /**
     * @var string
     *
     * @APIConnector\Column(name="FirstName", type="string", doctrineAttribute="firstName")
     * @Serializer\Expose()
     */
    protected $firstName;

    /**
     * @var string
     *
     * @APIConnector\Column(name="LastName", type="string", doctrineAttribute="lastName")
     * @Serializer\Expose()
     */
    protected $lastName;

    /**
     * @var string
     *
     * @APIConnector\Column(name="OneSignalId", type="string")
     * @Serializer\Expose()
     */
    protected $oneSignalId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CGV_Version", type="string")
     * @Serializer\Expose()
     */
    protected $cgvVersion;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="CGV_SignatureDate", type="datetimez")
     * @Serializer\Expose()
     */
    protected $cgvSignatureDate;

    /**
     * @var string
     *
     * @APIConnector\Column(name="InFront", type="string", mType="Microsoft.Dynamics.DataEntities.NoYes")
     * @Serializer\Expose()
     */
    protected $inFront;

    /**
     * @var string
     *
     * @APIConnector\Column(name="IdStripe", type="string", doctrineAttribute="idStripe")
     * @Serializer\Expose()
     */
    protected $idStripe;

    /**
     * @var string
     *
     * @APIConnector\Column(name="PaymMethod", type="string", doctrineAttribute="paymentTypeSelectedString")
     * @Serializer\Expose()
     */
    protected $paymentMethod;

    /**
     * @var string
     *
     * @APIConnector\Column(name="Enterprise", type="string", doctrineAttribute="enterprise")
     * @Serializer\Expose()
     */
    protected $enterprise;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ChildrenNumber", type="string", doctrineAttribute="childrenNumber")
     * @Serializer\Expose()
     */
    protected $childrenNumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ContactViaApp", type="string", mType="Microsoft.Dynamics.DataEntities.NoYes",
     *     doctrineAttribute="contactViaApp")
     * @Serializer\Expose()
     */
    protected $contactViaApp;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ContactViaMail", type="string", mType="Microsoft.Dynamics.DataEntities.NoYes",
     *     doctrineAttribute="contactViaMail")
     * @Serializer\Expose()
     */
    protected $contactViaMail;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ContactViaSMS", type="string", mType="Microsoft.Dynamics.DataEntities.NoYes",
     *     doctrineAttribute="contactViaSms")
     * @Serializer\Expose()
     */
    protected $contactViaSms;

    /**
     * @var string
     *
     * @APIConnector\Column(name="InvoiceAccount", type="string")
     * @Serializer\Expose()
     */
    protected $invoiceAccount;

    /**
     * Get addressBooks
     *
     * @return string
     */
    public function getAddressBooks(): ?string
    {
        return $this->addressBooks;
    }

    /**
     * Set addressBooks
     *
     * @param string $addressBooks
     *
     * @return Account
     */
    public function setAddressBooks(?string $addressBooks = null): Account
    {
        return $this->setTrigger('addressBooks', $addressBooks);
    }

    /**
     * Get addressCity
     *
     * @return string
     */
    public function getAddressCity(): ?string
    {
        return $this->addressCity;
    }

    /**
     * Set addressCity
     *
     * @param string $addressCity
     *
     * @return Account
     */
    public function setAddressCity(?string $addressCity = null): Account
    {
        return $this->setTrigger('addressCity', $addressCity);
    }

    /**
     * Get addressCountryRegionId
     *
     * @return string
     */
    public function getAddressCountryRegionId(): ?string
    {
        return $this->addressCountryRegionId;
    }

    /**
     * Set addressCountryRegionId
     *
     * @param string $addressCountryRegionId
     *
     * @return Account
     */
    public function setAddressCountryRegionId(?string $addressCountryRegionId = null): Account
    {
        return $this->setTrigger('addressCountryRegionId', $addressCountryRegionId);
    }

    /**
     * Get addressCounty
     *
     * @return string
     */
    public function getAddressCounty(): ?string
    {
        return $this->addressCounty;
    }

    /**
     * Set addressCounty
     *
     * @param string $addressCounty
     *
     * @return Account
     */
    public function setAddressCounty(?string $addressCounty = null): Account
    {
        return $this->setTrigger('addressCounty', $addressCounty);
    }

    /**
     * Get addressDescription
     *
     * @return string
     */
    public function getAddressDescription(): ?string
    {
        return $this->addressDescription;
    }

    /**
     * Set addressDescription
     *
     * @param string $addressDescription
     *
     * @return Account
     */
    public function setAddressDescription(?string $addressDescription = null): Account
    {
        return $this->setTrigger('addressDescription', $addressDescription);
    }

    /**
     * Get addressDistrictName
     *
     * @return string
     */
    public function getAddressDistrictName(): ?string
    {
        return $this->addressDistrictName;
    }

    /**
     * Set addressDistrictName
     *
     * @param string $addressDistrictName
     *
     * @return Account
     */
    public function setAddressDistrictName(?string $addressDistrictName = null): Account
    {
        return $this->setTrigger('addressDistrictName', $addressDistrictName);
    }

    /**
     * Get addressLocationRoles
     *
     * @return string
     */
    public function getAddressLocationRoles(): ?string
    {
        return $this->addressLocationRoles;
    }

    /**
     * Set addressLocationRoles
     *
     * @param string $addressLocationRoles
     *
     * @return Account
     */
    public function setAddressLocationRoles(?string $addressLocationRoles = null): Account
    {
        return $this->setTrigger('addressLocationRoles', $addressLocationRoles);
    }

    /**
     * Get addressState
     *
     * @return string
     */
    public function getAddressState(): ?string
    {
        if ($this->addressCountryRegionId !== "USA") {
            return null;
        }

        return $this->addressState;
    }

    /**
     * Set addressState
     *
     * @param string $addressState
     *
     * @return Account
     */
    public function setAddressState(?string $addressState = null): Account
    {
        return $this->setTrigger('addressState', $addressState);
    }

    /**
     * Get addressStreet
     *
     * @return string
     */
    public function getAddressStreet(): ?string
    {
        return $this->addressStreet;
    }

    /**
     * Set addressStreet
     *
     * @param string $addressStreet
     *
     * @return Account
     */
    public function setAddressStreet(?string $addressStreet = null): Account
    {
        return $this->setTrigger('addressStreet', $addressStreet);
    }

    /**
     * Get addressTimeZone
     *
     * @return string
     */
    public function getAddressTimeZone(): ?string
    {
        return $this->addressTimeZone;
    }

    /**
     * Set addressTimeZone
     *
     * @param string $addressTimeZone
     *
     * @return Account
     */
    public function setAddressTimeZone(?string $addressTimeZone = null): Account
    {
        return $this->setTrigger('addressTimeZone', $addressTimeZone);
    }

    /**
     * Get addressValidFrom
     *
     * @return \DateTime
     */
    public function getAddressValidFrom(): ?\DateTime
    {
        return $this->addressValidFrom;
    }

    /**
     * Set addressValidFrom
     *
     * @param \DateTime $addressValidFrom
     *
     * @return Account
     */
    public function setAddressValidFrom(\DateTime $addressValidFrom): Account
    {
        return $this->setTrigger('addressValidFrom', $addressValidFrom);
    }

    /**
     * Get addressValidTo
     *
     * @return \DateTime
     */
    public function getAddressValidTo(): ?\DateTime
    {
        return $this->addressValidTo;
    }

    /**
     * Set addressValidTo
     *
     * @param \DateTime $addressValidTo
     *
     * @return Account
     */
    public function setAddressValidTo(\DateTime $addressValidTo): Account
    {
        return $this->setTrigger('addressValidTo', $addressValidTo);
    }

    /**
     * Get addressZipCode
     *
     * @return string
     */
    public function getAddressZipCode(): ?string
    {
        return $this->addressZipCode;
    }

    /**
     * Set addressZipCode
     *
     * @param string $addressZipCode
     *
     * @return Account
     */
    public function setAddressZipCode(?string $addressZipCode = null): Account
    {
        return $this->setTrigger('addressZipCode', $addressZipCode);
    }

    /**
     * Get adhesionDate
     *
     * @return \DateTime
     */
    public function getAdhesionDate(): ?\DateTime
    {
        return $this->adhesionDate;
    }

    /**
     * Set adhesionDate
     *
     * @param \DateTime $adhesionDate
     *
     * @return Account
     */
    public function setAdhesionDate(\DateTime $adhesionDate): Account
    {
        return $this->setTrigger('adhesionDate', $adhesionDate);
    }

    /**
     * Get blocked
     *
     * @return string
     */
    public function getBlocked(): ?string
    {
        return $this->blocked;
    }

    /**
     * Set blocked
     *
     * @param string $blocked
     *
     * @return Account
     */
    public function setBlocked(?string $blocked = null): Account
    {
        return $this->setTrigger('blocked', $blocked);
    }

    /**
     * @return \DateTime|null
     */
    public function getCgvSignatureDate(): ?\DateTime
    {
        return $this->cgvSignatureDate;
    }

    /**
     * @param \DateTime $cgvSignatureDate
     *
     * @return Account
     */
    public function setCgvSignatureDate(\DateTime $cgvSignatureDate): self
    {
        return $this->setTrigger('cgvSignatureDate', $cgvSignatureDate);
    }

    /**
     * @return string
     */
    public function getCgvVersion(): ?string
    {
        return $this->cgvVersion;
    }

    /**
     * @param string $cgvVersion
     *
     * @return Account
     */
    public function setCgvVersion(string $cgvVersion): self
    {
        return $this->setTrigger('cgvVersion', $cgvVersion);
    }

    /**
     * Get contactPersonId
     *
     * @return string
     */
    public function getContactPersonId(): ?string
    {
        return $this->contactPersonId;
    }

    /**
     * Set contactPersonId
     *
     * @param string $contactPersonId
     *
     * @return Account
     */
    public function setContactPersonId(?string $contactPersonId = null): Account
    {
        return $this->setTrigger('contactPersonId', $contactPersonId);
    }

    /**
     * Get customerAccount
     *
     * @return string
     */
    public function getCustomerAccount(): ?string
    {
        return $this->customerAccount;
    }

    /**
     * Set customerAccount
     *
     * @param string $customerAccount
     *
     * @return Account
     */
    public function setCustomerAccount(?string $customerAccount = null): Account
    {
        return $this->setTrigger('customerAccount', $customerAccount);
    }

    /**
     * Get customerGroupId
     *
     * @return string
     */
    public function getCustomerGroupId(): ?string
    {
        return $this->customerGroupId;
    }

    /**
     * Set customerGroupId
     *
     * @param string $customerGroupId
     *
     * @return Account
     */
    public function setCustomerGroupId(?string $customerGroupId = null): Account
    {
        return $this->setTrigger('customerGroupId', $customerGroupId);
    }

    /**
     * Get dataAreaId
     *
     * @return string
     */
    public function getDataAreaId(): ?string
    {
        return $this->dataAreaId;
    }

    /**
     * Set dataAreaId
     *
     * @param string $dataAreaId
     *
     * @return Account
     */
    public function setDataAreaId(?string $dataAreaId = null): Account
    {
        if ($dataAreaId) {
            $dataAreaId = strtolower($dataAreaId);
        }

        return $this->setTrigger('dataAreaId', $dataAreaId);
    }

    /**
     * Get defaultDimensionDisplayValue
     *
     * @return string
     */
    public function getDefaultDimensionDisplayValue(): ?string
    {
        return $this->defaultDimensionDisplayValue;
    }

    /**
     * Set defaultDimensionDisplayValue
     *
     * @param string $defaultDimensionDisplayValue
     *
     * @return Account
     */
    public function setDefaultDimensionDisplayValue(?string $defaultDimensionDisplayValue = null): Account
    {
        return $this->setTrigger('defaultDimensionDisplayValue', $defaultDimensionDisplayValue);
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return Account
     */
    public function setFirstName(?string $firstName = null): Account
    {
        $this->setTrigger('firstName', $firstName);

        return $this->refreshName();
    }

    /**
     * Get fullPrimaryAddress
     *
     * @return string
     */
    public function getFullPrimaryAddress(): ?string
    {
        return $this->fullPrimaryAddress;
    }

    /**
     * Set fullPrimaryAddress
     *
     * @param string $fullPrimaryAddress
     *
     * @return Account
     */
    public function setFullPrimaryAddress(?string $fullPrimaryAddress = null): Account
    {
        return $this->setTrigger('fullPrimaryAddress', $fullPrimaryAddress);
    }

    /**
     * Get idStripe
     *
     * @return string
     */
    public function getIdStripe(): ?string
    {
        return $this->idStripe;
    }

    /**
     * Set idStripe
     *
     * @param string $idStripe
     *
     * @return Account
     */
    public function setIdStripe(?string $idStripe): self
    {
        return $this->setTrigger('idStripe', $idStripe);
    }

    /**
     * @return string
     */
    public function getInFront(): ?string
    {
        return $this->inFront;
    }

    /**
     * @param string $inFront
     *
     * @return Account
     */
    public function setInFront(string $inFront): self
    {
        return $this->setTrigger('inFront', $inFront);
    }

    /**
     * Get insurer
     *
     * @return string
     */
    public function getInsurer(): ?string
    {
        return $this->insurer;
    }

    /**
     * Set insurer
     *
     * @param string $insurer
     *
     * @return Account
     */
    public function setInsurer(?string $insurer = null): Account
    {
        return $this->setTrigger('insurer', $insurer);
    }

    /**
     * Get languageId
     *
     * @return string
     */
    public function getLanguageId(): ?string
    {
        return $this->languageId;
    }

    /**
     * Set languageId
     *
     * @param string $languageId
     *
     * @return Account
     */
    public function setLanguageId(?string $languageId = null): Account
    {
        if ($languageId === 'en') {
            $languageId = 'en-US';
        }

        return $this->setTrigger('languageId', $languageId);
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return Account
     */
    public function setLastName(?string $lastName = null): Account
    {
        $this->setTrigger('lastName', $lastName);

        return $this->refreshName();
    }

    /**
     * Get lockerCode
     *
     * @return string
     */
    public function getLockerCode(): ?string
    {
        return $this->lockerCode;
    }

    /**
     * Set lockerCode
     *
     * @param string $lockerCode
     *
     * @return Account
     */
    public function setLockerCode(?string $lockerCode = null): Account
    {
        return $this->setTrigger('lockerCode', $lockerCode);
    }

    /**
     * Get middleName
     *
     * @return string
     */
    public function getMiddleName(): ?string
    {
        return $this->middleName;
    }

    /**
     * Set middleName
     *
     * @param string $middleName
     *
     * @return Account
     */
    public function setMiddleName(?string $middleName = null): Account
    {
        $this->setTrigger('middleName', $middleName);

        return $this->refreshName();
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Account
     */
    public function setName(?string $name = null): Account
    {
        return $this->setTrigger('name', $name);
    }

    /**
     * Get numAdherent
     *
     * @return string
     */
    public function getNumAdherent(): ?string
    {
        return $this->numAdherent;
    }

    /**
     * Set numAdherent
     *
     * @param string $numAdherent
     *
     * @return Account
     */
    public function setNumAdherent(?string $numAdherent = null): Account
    {
        return $this->setTrigger('numAdherent', $numAdherent);
    }

    /**
     * Get onHoldStatus
     *
     * @return string
     */
    public function getOnHoldStatus(): ?string
    {
        return $this->onHoldStatus;
    }

    /**
     * Set onHoldStatus
     *
     * @param string $onHoldStatus
     *
     * @return Account
     */
    public function setOnHoldStatus(?string $onHoldStatus = null): Account
    {
        return $this->setTrigger('onHoldStatus', $onHoldStatus);
    }

    /**
     * @return string
     */
    public function getOneSignalId(): ?string
    {
        return $this->oneSignalId;
    }

    /**
     * @param string $oneSignalId
     *
     * @return Account
     */
    public function setOneSignalId(?string $oneSignalId): self
    {
        return $this->setTrigger('oneSignalId', $oneSignalId);
    }

    /**
     * Get parkingSpot
     *
     * @return string
     */
    public function getParkingSpot(): ?string
    {
        return $this->parkingSpot;
    }

    /**
     * Set parkingSpot
     *
     * @param string $parkingSpot
     *
     * @return Account
     */
    public function setParkingSpot(?string $parkingSpot = null): Account
    {
        return $this->setTrigger('parkingSpot', $parkingSpot);
    }

    /**
     * Get partyNumber
     *
     * @return string
     */
    public function getPartyNumber(): ?string
    {
        return $this->partyNumber;
    }

    /**
     * Set partyNumber
     *
     * @param string $partyNumber
     *
     * @return Account
     */
    public function setPartyNumber(?string $partyNumber = null): Account
    {
        return $this->setTrigger('partyNumber', $partyNumber);
    }

    /**
     * Get partyType
     *
     * @return string
     */
    public function getPartyType(): ?string
    {
        return $this->partyType;
    }

    /**
     * Set partyType
     *
     * @param string $partyType
     *
     * @return Account
     */
    public function setPartyType(?string $partyType = null): Account
    {
        return $this->setTrigger('partyType', $partyType);
    }

    /**
     * Get paymentMethod
     *
     * @return string
     */
    public function getPaymentMethod(): ?string
    {
        return $this->paymentMethod;
    }

    /**
     * Set paymentMethod
     *
     * @param string $paymentMethod
     *
     * @return Account
     */
    public function setPaymentMethod(?string $paymentMethod): self
    {
        return $this->setTrigger('paymentMethod', $paymentMethod);
    }

    /**
     * Get personAnniversaryDay
     *
     * @return int
     */
    public function getPersonAnniversaryDay(): ?int
    {
        return $this->personAnniversaryDay;
    }

    /**
     * Set personAnniversaryDay
     *
     * @param int $personAnniversaryDay
     *
     * @return Account
     */
    public function setPersonAnniversaryDay(?int $personAnniversaryDay): Account
    {
        return $this->setTrigger('personAnniversaryDay', $personAnniversaryDay);
    }

    /**
     * Get personAnniversaryMonth
     *
     * @return string
     */
    public function getPersonAnniversaryMonth(): ?string
    {
        return $this->personAnniversaryMonth;
    }

    /**
     * Set personAnniversaryMonth
     *
     * @param string $personAnniversaryMonth
     *
     * @return Account
     */
    public function setPersonAnniversaryMonth(?string $personAnniversaryMonth = null): Account
    {
        return $this->setTrigger('personAnniversaryMonth', $personAnniversaryMonth);
    }

    /**
     * Get personAnniversaryYear
     *
     * @return int
     */
    public function getPersonAnniversaryYear(): ?int
    {
        return $this->personAnniversaryYear;
    }

    /**
     * Set personAnniversaryYear
     *
     * @param int $personAnniversaryYear
     *
     * @return Account
     */
    public function setPersonAnniversaryYear(?int $personAnniversaryYear): Account
    {
        return $this->setTrigger('personAnniversaryYear', $personAnniversaryYear);
    }

    /**
     * Get personBirthDay
     *
     * @return int
     */
    public function getPersonBirthDay(): ?int
    {
        return $this->personBirthDay;
    }

    /**
     * Set personBirthDay
     *
     * @param int $personBirthDay
     *
     * @return Account
     */
    public function setPersonBirthDay(?int $personBirthDay): Account
    {
        return $this->setTrigger('personBirthDay', $personBirthDay);
    }

    /**
     * Get personBirthMonth
     *
     * @return string
     */
    public function getPersonBirthMonth(): ?string
    {
        return $this->personBirthMonth;
    }

    /**
     * Set personBirthMonth
     *
     * @param string $personBirthMonth
     *
     * @return Account
     */
    public function setPersonBirthMonth(?string $personBirthMonth = null): Account
    {
        return $this->setTrigger('personBirthMonth', $personBirthMonth);
    }

    /**
     * Get personBirthYear
     *
     * @return int
     */
    public function getPersonBirthYear(): ?int
    {
        return $this->personBirthYear;
    }

    /**
     * Set personBirthYear
     *
     * @param int $personBirthYear
     *
     * @return Account
     */
    public function setPersonBirthYear(?int $personBirthYear): Account
    {
        return $this->setTrigger('personBirthYear', $personBirthYear);
    }

    /**
     * Get personChildrenNames
     *
     * @return string
     */
    public function getPersonChildrenNames(): ?string
    {
        return $this->personChildrenNames;
    }

    /**
     * Set personChildrenNames
     *
     * @param string $personChildrenNames
     *
     * @return Account
     */
    public function setPersonChildrenNames(?string $personChildrenNames = null): Account
    {
        return $this->setTrigger('personChildrenNames', $personChildrenNames);
    }

    /**
     * Get personGender
     *
     * @return string
     */
    public function getPersonGender(): ?string
    {
        return $this->personGender;
    }

    /**
     * Set personGender
     *
     * @param string $personGender
     *
     * @return Account
     */
    public function setPersonGender(?string $personGender = null): Account
    {
        return $this->setTrigger('personGender', $personGender);
    }

    /**
     * Get personMaritalStatus
     *
     * @return string
     */
    public function getPersonMaritalStatus(): ?string
    {
        return $this->personMaritalStatus;
    }

    /**
     * Set personMaritalStatus
     *
     * @param string $personMaritalStatus
     *
     * @return Account
     */
    public function setPersonMaritalStatus(?string $personMaritalStatus = null): Account
    {
        return $this->setTrigger('personMaritalStatus', $personMaritalStatus);
    }

    /**
     * Get personalTitle
     *
     * @return string
     */
    public function getPersonalTitle(): ?string
    {
        return $this->personalTitle;
    }

    /**
     * Set personalTitle
     *
     * @param string $personalTitle
     *
     * @return Account
     */
    public function setPersonalTitle(?string $personalTitle = null): Account
    {
        return $this->setTrigger('personalTitle', $personalTitle);
    }

    /**
     * Get primaryContactEmail
     *
     * @return string
     */
    public function getPrimaryContactEmail(): ?string
    {
        return $this->primaryContactEmail;
    }

    /**
     * Set primaryContactEmail
     *
     * @param string $primaryContactEmail
     *
     * @return Account
     */
    public function setPrimaryContactEmail(?string $primaryContactEmail = null): Account
    {
        return $this->setTrigger('primaryContactEmail', $primaryContactEmail);
    }

    /**
     * Get primaryContactEmailDescription
     *
     * @return string
     */
    public function getPrimaryContactEmailDescription(): ?string
    {
        return $this->primaryContactEmailDescription;
    }

    /**
     * Set primaryContactEmailDescription
     *
     * @param string $primaryContactEmailDescription
     *
     * @return Account
     */
    public function setPrimaryContactEmailDescription(?string $primaryContactEmailDescription = null): Account
    {
        return $this->setTrigger('primaryContactEmailDescription', $primaryContactEmailDescription);
    }

    /**
     * Get primaryContactEmailPurpose
     *
     * @return string
     */
    public function getPrimaryContactEmailPurpose(): ?string
    {
        return $this->primaryContactEmailPurpose;
    }

    /**
     * Set primaryContactEmailPurpose
     *
     * @param string $primaryContactEmailPurpose
     *
     * @return Account
     */
    public function setPrimaryContactEmailPurpose(?string $primaryContactEmailPurpose = null): Account
    {
        return $this->setTrigger('primaryContactEmailPurpose', $primaryContactEmailPurpose);
    }

    /**
     * Get primaryContactFax
     *
     * @return string
     */
    public function getPrimaryContactFax(): ?string
    {
        return $this->primaryContactFax;
    }

    /**
     * Set primaryContactFax
     *
     * @param string $primaryContactFax
     *
     * @return Account
     */
    public function setPrimaryContactFax(?string $primaryContactFax = null): Account
    {
        return $this->setTrigger('primaryContactFax', $primaryContactFax);
    }

    /**
     * Get primaryContactFaxDescription
     *
     * @return string
     */
    public function getPrimaryContactFaxDescription(): ?string
    {
        return $this->primaryContactFaxDescription;
    }

    /**
     * Set primaryContactFaxDescription
     *
     * @param string $primaryContactFaxDescription
     *
     * @return Account
     */
    public function setPrimaryContactFaxDescription(?string $primaryContactFaxDescription = null): Account
    {
        return $this->setTrigger('primaryContactFaxDescription', $primaryContactFaxDescription);
    }

    /**
     * Get primaryContactFaxExtension
     *
     * @return string
     */
    public function getPrimaryContactFaxExtension(): ?string
    {
        return $this->primaryContactFaxExtension;
    }

    /**
     * Set primaryContactFaxExtension
     *
     * @param string $primaryContactFaxExtension
     *
     * @return Account
     */
    public function setPrimaryContactFaxExtension(?string $primaryContactFaxExtension = null): Account
    {
        return $this->setTrigger('primaryContactFaxExtension', $primaryContactFaxExtension);
    }

    /**
     * Get primaryContactFaxPurpose
     *
     * @return string
     */
    public function getPrimaryContactFaxPurpose(): ?string
    {
        return $this->primaryContactFaxPurpose;
    }

    /**
     * Set primaryContactFaxPurpose
     *
     * @param string $primaryContactFaxPurpose
     *
     * @return Account
     */
    public function setPrimaryContactFaxPurpose(?string $primaryContactFaxPurpose = null): Account
    {
        return $this->setTrigger('primaryContactFaxPurpose', $primaryContactFaxPurpose);
    }

    /**
     * Get primaryContactPhone
     *
     * @return string
     */
    public function getPrimaryContactPhone(): ?string
    {
        return $this->primaryContactPhone;
    }

    /**
     * Set primaryContactPhone
     *
     * @param string $primaryContactPhone
     *
     * @return Account
     */
    public function setPrimaryContactPhone(?string $primaryContactPhone = null): Account
    {
        return $this->setTrigger('primaryContactPhone', $primaryContactPhone);
    }

    /**
     * Get primaryContactPhoneDescription
     *
     * @return string
     */
    public function getPrimaryContactPhoneDescription(): ?string
    {
        return $this->primaryContactPhoneDescription;
    }

    /**
     * Set primaryContactPhoneDescription
     *
     * @param string $primaryContactPhoneDescription
     *
     * @return Account
     */
    public function setPrimaryContactPhoneDescription(?string $primaryContactPhoneDescription = null): Account
    {
        return $this->setTrigger('primaryContactPhoneDescription', $primaryContactPhoneDescription);
    }

    /**
     * Get primaryContactPhoneExtension
     *
     * @return string
     */
    public function getPrimaryContactPhoneExtension(): ?string
    {
        return $this->primaryContactPhoneExtension;
    }

    /**
     * Set primaryContactPhoneExtension
     *
     * @param string $primaryContactPhoneExtension
     *
     * @return Account
     */
    public function setPrimaryContactPhoneExtension(?string $primaryContactPhoneExtension = null): Account
    {
        return $this->setTrigger('primaryContactPhoneExtension', $primaryContactPhoneExtension);
    }

    /**
     * Get primaryContactPhoneIsMobile
     *
     * @return string
     */
    public function getPrimaryContactPhoneIsMobile(): ?string
    {
        return $this->primaryContactPhoneIsMobile;
    }

    /**
     * Set primaryContactPhoneIsMobile
     *
     * @param string $primaryContactPhoneIsMobile
     *
     * @return Account
     */
    public function setPrimaryContactPhoneIsMobile(?string $primaryContactPhoneIsMobile = null): Account
    {
        return $this->setTrigger('primaryContactPhoneIsMobile', $primaryContactPhoneIsMobile);
    }

    /**
     * Get primaryContactPhonePurpose
     *
     * @return string
     */
    public function getPrimaryContactPhonePurpose(): ?string
    {
        return $this->primaryContactPhonePurpose;
    }

    /**
     * Set primaryContactPhonePurpose
     *
     * @param string $primaryContactPhonePurpose
     *
     * @return Account
     */
    public function setPrimaryContactPhonePurpose(?string $primaryContactPhonePurpose = null): Account
    {
        return $this->setTrigger('primaryContactPhonePurpose', $primaryContactPhonePurpose);
    }

    /**
     * Get primaryContactTelex
     *
     * @return string
     */
    public function getPrimaryContactTelex(): ?string
    {
        return $this->primaryContactTelex;
    }

    /**
     * Set primaryContactTelex
     *
     * @param string $primaryContactTelex
     *
     * @return Account
     */
    public function setPrimaryContactTelex(?string $primaryContactTelex = null): Account
    {
        return $this->setTrigger('primaryContactTelex', $primaryContactTelex);
    }

    /**
     * Get primaryContactTelexDescription
     *
     * @return string
     */
    public function getPrimaryContactTelexDescription(): ?string
    {
        return $this->primaryContactTelexDescription;
    }

    /**
     * Set primaryContactTelexDescription
     *
     * @param string $primaryContactTelexDescription
     *
     * @return Account
     */
    public function setPrimaryContactTelexDescription(?string $primaryContactTelexDescription = null): Account
    {
        return $this->setTrigger('primaryContactTelexDescription', $primaryContactTelexDescription);
    }

    /**
     * Get primaryContactTelexPurpose
     *
     * @return string
     */
    public function getPrimaryContactTelexPurpose(): ?string
    {
        return $this->primaryContactTelexPurpose;
    }

    /**
     * Set primaryContactTelexPurpose
     *
     * @param string $primaryContactTelexPurpose
     *
     * @return Account
     */
    public function setPrimaryContactTelexPurpose(?string $primaryContactTelexPurpose = null): Account
    {
        return $this->setTrigger('primaryContactTelexPurpose', $primaryContactTelexPurpose);
    }

    /**
     * Get receiptCalendar
     *
     * @return string
     */
    public function getReceiptCalendar(): ?string
    {
        return $this->receiptCalendar;
    }

    /**
     * Set receiptCalendar
     *
     * @param string $receiptCalendar
     *
     * @return Account
     */
    public function setReceiptCalendar(?string $receiptCalendar = null): Account
    {
        return $this->setTrigger('receiptCalendar', $receiptCalendar);
    }

    /**
     * Get receiptEmail
     *
     * @return string
     */
    public function getReceiptEmail(): ?string
    {
        return $this->receiptEmail;
    }

    /**
     * Set receiptEmail
     *
     * @param string $receiptEmail
     *
     * @return Account
     */
    public function setReceiptEmail(?string $receiptEmail = null): Account
    {
        return $this->setTrigger('receiptEmail', $receiptEmail);
    }

    /**
     * Get receiptOption
     *
     * @return string
     */
    public function getReceiptOption(): ?string
    {
        return $this->receiptOption;
    }

    /**
     * Set receiptOption
     *
     * @param string $receiptOption
     *
     * @return Account
     */
    public function setReceiptOption(?string $receiptOption = null): Account
    {
        return $this->setTrigger('receiptOption', $receiptOption);
    }

    /**
     * Get salesCurrencyCode
     *
     * @return string
     */
    public function getSalesCurrencyCode(): ?string
    {
        return $this->salesCurrencyCode;
    }

    /**
     * Set salesCurrencyCode
     *
     * @param string $salesCurrencyCode
     *
     * @return Account
     */
    public function setSalesCurrencyCode(?string $salesCurrencyCode = null): Account
    {
        return $this->setTrigger('salesCurrencyCode', $salesCurrencyCode);
    }

    /**
     * Get salesMemo
     *
     * @return string
     */
    public function getSalesMemo(): ?string
    {
        return $this->salesMemo;
    }

    /**
     * Set salesMemo
     *
     * @param string $salesMemo
     *
     * @return Account
     */
    public function setSalesMemo(?string $salesMemo = null): Account
    {
        return $this->setTrigger('salesMemo', $salesMemo);
    }

    /**
     * Get taxExemptNumber
     *
     * @return string
     */
    public function getTaxExemptNumber(): ?string
    {
        return $this->taxExemptNumber;
    }

    /**
     * Set taxExemptNumber
     *
     * @param string $taxExemptNumber
     *
     * @return Account
     */
    public function setTaxExemptNumber(?string $taxExemptNumber = null): Account
    {
        return $this->setTrigger('taxExemptNumber', $taxExemptNumber);
    }

    /**
     * Get vehicleColor
     *
     * @return string
     */
    public function getVehicleColor(): ?string
    {
        return $this->vehicleColor;
    }

    /**
     * Set vehicleColor
     *
     * @param string $vehicleColor
     *
     * @return Account
     */
    public function setVehicleColor(?string $vehicleColor = null): Account
    {
        return $this->setTrigger('vehicleColor', $vehicleColor);
    }

    /**
     * Get vehicleModel
     *
     * @return int
     */
    public function getVehicleModel(): ?int
    {
        return $this->vehicleModel;
    }

    /**
     * Set vehicleModel
     *
     * @param int $vehicleModel
     *
     * @return Account
     */
    public function setVehicleModel(?int $vehicleModel): Account
    {
        return $this->setTrigger('vehicleModel', $vehicleModel);
    }

    /**
     * Get vin
     *
     * @return string
     */
    public function getVin(): ?string
    {
        return $this->vin;
    }

    /**
     * Set vin
     *
     * @param string $vin
     *
     * @return Account
     */
    public function setVin(?string $vin = null): Account
    {
        return $this->setTrigger('vin', $vin);
    }

    /**
     * Get warehouseId
     *
     * @return string
     */
    public function getWarehouseId(): ?string
    {
        return $this->warehouseId;
    }

    /**
     * Set warehouseId
     *
     * @param string $warehouseId
     *
     * @return Account
     */
    public function setWarehouseId(?string $warehouseId = null): Account
    {
        return $this->setTrigger('warehouseId', $warehouseId);
    }

    /**
     * Get weddingDay
     *
     * @return int
     */
    public function getWeddingDay(): ?int
    {
        return $this->weddingDay;
    }

    /**
     * Set weddingDay
     *
     * @param int $weddingDay
     *
     * @return Account
     */
    public function setWeddingDay(?int $weddingDay): Account
    {
        return $this->setTrigger('weddingDay', $weddingDay);
    }

    /**
     * Get weddingMonth
     *
     * @return string
     */
    public function getWeddingMonth(): ?string
    {
        return $this->weddingMonth;
    }

    /**
     * Set weddingMonth
     *
     * @param string $weddingMonth
     *
     * @return Account
     */
    public function setWeddingMonth(?string $weddingMonth = null): Account
    {
        return $this->setTrigger('weddingMonth', $weddingMonth);
    }

    /**
     * Get weddingYear
     *
     * @return int
     */
    public function getWeddingYear(): ?int
    {
        return $this->weddingYear;
    }

    /**
     * Set weddingYear
     *
     * @param int $weddingYear
     *
     * @return Account
     */
    public function setWeddingYear(?int $weddingYear): Account
    {
        return $this->setTrigger('weddingYear', $weddingYear);
    }

    /**
     * Do refreshName
     *
     * @return Account
     */
    public function refreshName(): self
    {
        $this->name = trim(str_replace('  ', ' ', $this->firstName . ' ' . $this->middleName . ' ' . $this->lastName));

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEnterprise(): ?string
    {
        return $this->enterprise;
    }

    /**
     * @param string $enterprise
     *
     * @return Account
     */
    public function setEnterprise(?string $enterprise): Account
    {
        return $this->setTrigger('enterprise', $enterprise);
    }

    /**
     * @return string|null
     */
    public function getChildrenNumber(): ?string
    {
        return $this->childrenNumber;
    }

    /**
     * @param string|null $childrenNumber
     *
     * @return Account
     */
    public function setChildrenNumber(?string $childrenNumber): Account
    {
        return $this->setTrigger('childrenNumber', $childrenNumber);
    }

    /**
     * @return string
     */
    public function getContactViaApp(): ?string
    {
        return $this->contactViaApp;
    }

    /**
     * @param string $contactViaApp
     *
     * @return Account
     */
    public function setContactViaApp(?string $contactViaApp): Account
    {
        return $this->setTrigger('contactViaApp', $contactViaApp);
    }

    /**
     * @return string
     */
    public function getContactViaMail(): ?string
    {
        return $this->contactViaMail;
    }

    /**
     * @param string $contactViaMail
     *
     * @return Account
     */
    public function setContactViaMail(?string $contactViaMail): Account
    {
        return $this->setTrigger('contactViaMail', $contactViaMail);
    }

    /**
     * @return string
     */
    public function getContactViaSms(): ?string
    {
        return $this->contactViaSms;
    }

    /**
     * @param string $contactViaSms
     *
     * @return Account
     */
    public function setContactViaSms(?string $contactViaSms): Account
    {
        return $this->setTrigger('contactViaSms', $contactViaSms);
    }

    /**
     * @return string
     */
    public function getInvoiceAccount(): ?string
    {
        return $this->invoiceAccount;
    }

    /**
     * @param string|null $invoiceAccount
     *
     * @return Account
     */
    public function setInvoiceAccount(?string $invoiceAccount): Account
    {
        return $this->setTrigger('invoiceAccount', $invoiceAccount);
    }
}
