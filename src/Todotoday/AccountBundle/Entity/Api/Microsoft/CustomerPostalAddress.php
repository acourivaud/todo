<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 08/03/2017
 * Time: 20:52
 */

namespace Todotoday\AccountBundle\Entity\Api\Microsoft;

use Actiane\ApiConnectorBundle\Lib\EntityProxyTrait;
use Actiane\ApiConnectorBundle\Lib\Mapping as APIConnector;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class CustomerPostalAddress
 *
 * @SuppressWarnings(PHPMD)
 * @APIConnector\Entity(
 *     path="CustomerPostalAddresses",
 *     repositoryId="todotoday.account.repository.api.customer_postal_address"
 * )
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Entity\Api\Microsoft
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class CustomerPostalAddress
{
    use EntityProxyTrait;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="IsPrimary", type="string")
     */
    protected $isPrimary;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="IsRoleDelivery", type="string")
     */
    protected $isRoleDelivery;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="IsPrivate", type="string")
     */
    protected $isPrivate;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="AddressCity", type="string")
     */
    protected $addressCity;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="AddressState", type="string")
     */
    protected $addressState;

    /**
     * @var \DateTime
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="Expiration", type="datetime")
     */
    protected $expiration;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="AddressPostBox", type="string")
     */
    protected $addressPostBox;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="AddressCityInKana", type="string")
     */
    protected $addressCityInKana;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="IsRoleInvoice", type="string")
     */
    protected $isRoleInvoice;

    /**
     * @var \DateTime
     *
     * @APIConnector\Id()
     * @Serializer\Expose()
     * @APIConnector\Column(name="Effective", type="datetime")
     */
    protected $effective;

    /**
     * @var int
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="AddressLatitude", type="int")
     */
    protected $addressLatitude;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="IsRoleHome", type="string")
     */
    protected $isRoleHome;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="AddressLocationRoles", type="string")
     */
    protected $addressLocationRoles;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="AddressDescription", type="string")
     */
    protected $addressDescription;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="IsPrimaryTaxRegistration", type="string")
     */
    protected $isPrimaryTaxRegistration;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @Serializer\Expose()
     * @APIConnector\Column(name="AddressLocationId", type="string")
     */
    protected $addressLocationId;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="AddressDistrictName", type="string")
     */
    protected $addressDistrictName;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="AddressBuilding", type="string")
     */
    protected $addressBuilding;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @Serializer\Expose()
     * @APIConnector\Column(name="CustomerAccountNumber", type="string")
     */
    protected $customerAccountNumber;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="DunsNumber", type="string")
     */
    protected $dunsNumber;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="AttentionToAddressLine", type="string")
     */
    protected $attentionToAddressLine;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="AddressStreet", type="string")
     */
    protected $addressStreet;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="IsLocationOwner", type="string")
     */
    protected $isLocationOwner;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="AddressStreetInKana", type="string")
     */
    protected $addressStreetInKana;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="IsPrivatePostalAddress", type="string")
     */
    protected $isPrivatePostalAddress;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="AddressApartment", type="string")
     */
    protected $addressApartment;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="AddressCountyId", type="string")
     */
    protected $addressCountyId;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="FormattedAddress", type="string")
     */
    protected $formattedAddress;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @Serializer\Expose()
     * @APIConnector\Column(name="CustomerLegalEntityId", type="string")
     */
    protected $customerLegalEntityId;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="AddressTimeZone", type="string")
     */
    protected $addressTimeZone;

    /**
     * @var int
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="AddressLongitude", type="int")
     */
    protected $addressLongitude;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @Serializer\Expose()
     * @APIConnector\Column(name="dataAreaId", type="string")
     */
    protected $dataAreaId;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="AddressZipCode", type="string")
     */
    protected $addressZipCode;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="AddressCountryRegionId", type="string")
     */
    protected $addressCountryRegionId;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="AddressStreetNumber", type="string")
     */
    protected $addressStreetNumber;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="BuildingCompliment", type="string")
     */
    protected $buildingCompliment;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="IsPostalAddress", type="string")
     */
    protected $isPostalAddress;

    /**
     * Get addressApartment
     *
     * @return string
     */
    public function getAddressApartment(): ?string
    {
        return $this->addressApartment;
    }

    /**
     * Set addressApartment
     *
     * @param string $addressApartment
     *
     * @return CustomerPostalAddress
     */
    public function setAddressApartment(?string $addressApartment): CustomerPostalAddress
    {
        return $this->setTrigger('addressApartment', $addressApartment);
    }

    /**
     * Get addressBuilding
     *
     * @return string
     */
    public function getAddressBuilding(): ?string
    {
        return $this->addressBuilding;
    }

    /**
     * Set addressBuilding
     *
     * @param string $addressBuilding
     *
     * @return CustomerPostalAddress
     */
    public function setAddressBuilding(?string $addressBuilding): CustomerPostalAddress
    {
        return $this->setTrigger('addressBuilding', $addressBuilding);
    }

    /**
     * Get addressCity
     *
     * @return string
     */
    public function getAddressCity(): ?string
    {
        return $this->addressCity;
    }

    /**
     * Set addressCity
     *
     * @param string $addressCity
     *
     * @return CustomerPostalAddress
     */
    public function setAddressCity(?string $addressCity): CustomerPostalAddress
    {
        return $this->setTrigger('addressCity', $addressCity);
    }

    /**
     * Get addressCityInKana
     *
     * @return string
     */
    public function getAddressCityInKana(): ?string
    {
        return $this->addressCityInKana;
    }

    /**
     * Set addressCityInKana
     *
     * @param string $addressCityInKana
     *
     * @return CustomerPostalAddress
     */
    public function setAddressCityInKana(?string $addressCityInKana): CustomerPostalAddress
    {
        return $this->setTrigger('addressCityInKana', $addressCityInKana);
    }

    /**
     * Get addressCountryRegionId
     *
     * @return string
     */
    public function getAddressCountryRegionId(): ?string
    {
        return $this->addressCountryRegionId;
    }

    /**
     * Set addressCountryRegionId
     *
     * @param string $addressCountryRegionId
     *
     * @return CustomerPostalAddress
     */
    public function setAddressCountryRegionId(?string $addressCountryRegionId): CustomerPostalAddress
    {
        return $this->setTrigger('addressCountryRegionId', $addressCountryRegionId);
    }

    /**
     * Get addressCountyId
     *
     * @return string
     */
    public function getAddressCountyId(): ?string
    {
        return $this->addressCountyId;
    }

    /**
     * Set addressCountyId
     *
     * @param string $addressCountyId
     *
     * @return CustomerPostalAddress
     */
    public function setAddressCountyId(?string $addressCountyId): CustomerPostalAddress
    {
        return $this->setTrigger('addressCountyId', $addressCountyId);
    }

    /**
     * Get addressDescription
     *
     * @return string
     */
    public function getAddressDescription(): ?string
    {
        return $this->addressDescription;
    }

    /**
     * Set addressDescription
     *
     * @param string $addressDescription
     *
     * @return CustomerPostalAddress
     */
    public function setAddressDescription(?string $addressDescription): CustomerPostalAddress
    {
        return $this->setTrigger('addressDescription', $addressDescription);
    }

    /**
     * Get addressDistrictName
     *
     * @return string
     */
    public function getAddressDistrictName(): ?string
    {
        return $this->addressDistrictName;
    }

    /**
     * Set addressDistrictName
     *
     * @param string $addressDistrictName
     *
     * @return CustomerPostalAddress
     */
    public function setAddressDistrictName(?string $addressDistrictName): CustomerPostalAddress
    {
        return $this->setTrigger('addressDistrictName', $addressDistrictName);
    }

    /**
     * Get addressLatitude
     *
     * @return int
     */
    public function getAddressLatitude(): ?int
    {
        return $this->addressLatitude;
    }

    /**
     * Set addressLatitude
     *
     * @param int $addressLatitude
     *
     * @return CustomerPostalAddress
     */
    public function setAddressLatitude(?int $addressLatitude): CustomerPostalAddress
    {
        return $this->setTrigger('addressLatitude', $addressLatitude);
    }

    /**
     * Get addressLocationId
     *
     * @return string
     */
    public function getAddressLocationId(): ?string
    {
        return $this->addressLocationId;
    }

    /**
     * Set addressLocationId
     *
     * @param string $addressLocationId
     *
     * @return CustomerPostalAddress
     */
    public function setAddressLocationId(?string $addressLocationId): CustomerPostalAddress
    {
        return $this->setTrigger('addressLocationId', $addressLocationId);
    }

    /**
     * Get addressLocationRoles
     *
     * @return string
     */
    public function getAddressLocationRoles(): ?string
    {
        return $this->addressLocationRoles;
    }

    /**
     * Set addressLocationRoles
     *
     * @param string $addressLocationRoles
     *
     * @return CustomerPostalAddress
     */
    public function setAddressLocationRoles(?string $addressLocationRoles): CustomerPostalAddress
    {
        return $this->setTrigger('addressLocationRoles', $addressLocationRoles);
    }

    /**
     * Get addressLongitude
     *
     * @return int
     */
    public function getAddressLongitude(): ?int
    {
        return $this->addressLongitude;
    }

    /**
     * Set addressLongitude
     *
     * @param int $addressLongitude
     *
     * @return CustomerPostalAddress
     */
    public function setAddressLongitude(?int $addressLongitude): CustomerPostalAddress
    {
        return $this->setTrigger('addressLongitude', $addressLongitude);
    }

    /**
     * Get addressPostBox
     *
     * @return string
     */
    public function getAddressPostBox(): ?string
    {
        return $this->addressPostBox;
    }

    /**
     * Set addressPostBox
     *
     * @param string $addressPostBox
     *
     * @return CustomerPostalAddress
     */
    public function setAddressPostBox(?string $addressPostBox): CustomerPostalAddress
    {
        return $this->setTrigger('addressPostBox', $addressPostBox);
    }

    /**
     * Get addressState
     *
     * @return string
     */
    public function getAddressState(): ?string
    {
        return $this->addressState;
    }

    /**
     * Set addressState
     *
     * @param string $addressState
     *
     * @return CustomerPostalAddress
     */
    public function setAddressState(?string $addressState): CustomerPostalAddress
    {
        return $this->setTrigger('addressState', $addressState);
    }

    /**
     * Get addressStreet
     *
     * @return string
     */
    public function getAddressStreet(): ?string
    {
        return $this->addressStreet;
    }

    /**
     * Set addressStreet
     *
     * @param string $addressStreet
     *
     * @return CustomerPostalAddress
     */
    public function setAddressStreet(?string $addressStreet): CustomerPostalAddress
    {
        return $this->setTrigger('addressStreet', $addressStreet);
    }

    /**
     * Get addressStreetInKana
     *
     * @return string
     */
    public function getAddressStreetInKana(): ?string
    {
        return $this->addressStreetInKana;
    }

    /**
     * Set addressStreetInKana
     *
     * @param string $addressStreetInKana
     *
     * @return CustomerPostalAddress
     */
    public function setAddressStreetInKana(?string $addressStreetInKana): CustomerPostalAddress
    {
        return $this->setTrigger('addressStreetInKana', $addressStreetInKana);
    }

    /**
     * Get addressStreetNumber
     *
     * @return string
     */
    public function getAddressStreetNumber(): ?string
    {
        return $this->addressStreetNumber;
    }

    /**
     * Set addressStreetNumber
     *
     * @param string $addressStreetNumber
     *
     * @return CustomerPostalAddress
     */
    public function setAddressStreetNumber(?string $addressStreetNumber): CustomerPostalAddress
    {
        return $this->setTrigger('addressStreetNumber', $addressStreetNumber);
    }

    /**
     * Get addressTimeZone
     *
     * @return string
     */
    public function getAddressTimeZone(): ?string
    {
        return $this->addressTimeZone;
    }

    /**
     * Set addressTimeZone
     *
     * @param string $addressTimeZone
     *
     * @return CustomerPostalAddress
     */
    public function setAddressTimeZone(?string $addressTimeZone): CustomerPostalAddress
    {
        return $this->setTrigger('addressTimeZone', $addressTimeZone);
    }

    /**
     * Get addressZipCode
     *
     * @return string
     */
    public function getAddressZipCode(): ?string
    {
        return $this->addressZipCode;
    }

    /**
     * Set addressZipCode
     *
     * @param string $addressZipCode
     *
     * @return CustomerPostalAddress
     */
    public function setAddressZipCode(?string $addressZipCode): CustomerPostalAddress
    {
        return $this->setTrigger('addressZipCode', $addressZipCode);
    }

    /**
     * Get attentionToAddressLine
     *
     * @return string
     */
    public function getAttentionToAddressLine(): ?string
    {
        return $this->attentionToAddressLine;
    }

    /**
     * Set attentionToAddressLine
     *
     * @param string $attentionToAddressLine
     *
     * @return CustomerPostalAddress
     */
    public function setAttentionToAddressLine(?string $attentionToAddressLine): CustomerPostalAddress
    {
        return $this->setTrigger('attentionToAddressLine', $attentionToAddressLine);
    }

    /**
     * Get buildingCompliment
     *
     * @return string
     */
    public function getBuildingCompliment(): ?string
    {
        return $this->buildingCompliment;
    }

    /**
     * Set buildingCompliment
     *
     * @param string $buildingCompliment
     *
     * @return CustomerPostalAddress
     */
    public function setBuildingCompliment(?string $buildingCompliment): CustomerPostalAddress
    {
        return $this->setTrigger('buildingCompliment', $buildingCompliment);
    }

    /**
     * Get customerAccountNumber
     *
     * @return string
     */
    public function getCustomerAccountNumber(): ?string
    {
        return $this->customerAccountNumber;
    }

    /**
     * Set customerAccountNumber
     *
     * @param string $customerAccountNumber
     *
     * @return CustomerPostalAddress
     */
    public function setCustomerAccountNumber(?string $customerAccountNumber): CustomerPostalAddress
    {
        return $this->setTrigger('customerAccountNumber', $customerAccountNumber);
    }

    /**
     * Get customerLegalEntityId
     *
     * @return string
     */
    public function getCustomerLegalEntityId(): ?string
    {
        return $this->customerLegalEntityId;
    }

    /**
     * Set customerLegalEntityId
     *
     * @param string $customerLegalEntityId
     *
     * @return CustomerPostalAddress
     */
    public function setCustomerLegalEntityId(?string $customerLegalEntityId): CustomerPostalAddress
    {
        return $this->setTrigger('customerLegalEntityId', $customerLegalEntityId);
    }

    /**
     * Get dataAreaId
     *
     * @return string
     */
    public function getDataAreaId(): ?string
    {
        return $this->dataAreaId;
    }

    /**
     * Set dataAreaId
     *
     * @param string $dataAreaId
     *
     * @return CustomerPostalAddress
     */
    public function setDataAreaId(?string $dataAreaId): CustomerPostalAddress
    {
        return $this->setTrigger('dataAreaId', $dataAreaId);
    }

    /**
     * Get dunsNumber
     *
     * @return string
     */
    public function getDunsNumber(): ?string
    {
        return $this->dunsNumber;
    }

    /**
     * Set dunsNumber
     *
     * @param string $dunsNumber
     *
     * @return CustomerPostalAddress
     */
    public function setDunsNumber(?string $dunsNumber): CustomerPostalAddress
    {
        return $this->setTrigger('dunsNumber', $dunsNumber);
    }

    /**
     * Get effective
     *
     * @return \DateTime
     */
    public function getEffective(): ?\DateTime
    {
        return $this->effective;
    }

    /**
     * Set effective
     *
     * @param \DateTime $effective
     *
     * @return CustomerPostalAddress
     */
    public function setEffective(?\DateTime $effective): CustomerPostalAddress
    {
        return $this->setTrigger('effective', $effective);
    }

    /**
     * Get expiration
     *
     * @return \DateTime
     */
    public function getExpiration(): ?\DateTime
    {
        return $this->expiration;
    }

    /**
     * Set expiration
     *
     * @param \DateTime $expiration
     *
     * @return CustomerPostalAddress
     */
    public function setExpiration(?\DateTime $expiration): CustomerPostalAddress
    {
        return $this->setTrigger('expiration', $expiration);
    }

    /**
     * Get formattedAddress
     *
     * @return string
     */
    public function getFormattedAddress(): ?string
    {
        return $this->formattedAddress;
    }

    /**
     * Set formattedAddress
     *
     * @param string $formattedAddress
     *
     * @return CustomerPostalAddress
     */
    public function setFormattedAddress(?string $formattedAddress): CustomerPostalAddress
    {
        return $this->setTrigger('formattedAddress', $formattedAddress);
    }

    /**
     * Get isLocationOwner
     *
     * @return string
     */
    public function getIsLocationOwner(): ?string
    {
        return $this->isLocationOwner;
    }

    /**
     * Set isLocationOwner
     *
     * @param string $isLocationOwner
     *
     * @return CustomerPostalAddress
     */
    public function setIsLocationOwner(?string $isLocationOwner): CustomerPostalAddress
    {
        return $this->setTrigger('isLocationOwner', $isLocationOwner);
    }

    /**
     * Get isPostalAddress
     *
     * @return string
     */
    public function getIsPostalAddress(): ?string
    {
        return $this->isPostalAddress;
    }

    /**
     * Set isPostalAddress
     *
     * @param string $isPostalAddress
     *
     * @return CustomerPostalAddress
     */
    public function setIsPostalAddress(?string $isPostalAddress): CustomerPostalAddress
    {
        return $this->setTrigger('isPostalAddress', $isPostalAddress);
    }

    /**
     * Get isPrimary
     *
     * @return string
     */
    public function getIsPrimary(): ?string
    {
        return $this->isPrimary;
    }

    /**
     * Set isPrimary
     *
     * @param string $isPrimary
     *
     * @return CustomerPostalAddress
     */
    public function setIsPrimary(?string $isPrimary): CustomerPostalAddress
    {
        return $this->setTrigger('isPrimary', $isPrimary);
    }

    /**
     * Get isPrimaryTaxRegistration
     *
     * @return string
     */
    public function getIsPrimaryTaxRegistration(): ?string
    {
        return $this->isPrimaryTaxRegistration;
    }

    /**
     * Set isPrimaryTaxRegistration
     *
     * @param string $isPrimaryTaxRegistration
     *
     * @return CustomerPostalAddress
     */
    public function setIsPrimaryTaxRegistration(?string $isPrimaryTaxRegistration): CustomerPostalAddress
    {
        return $this->setTrigger('isPrimaryTaxRegistration', $isPrimaryTaxRegistration);
    }

    /**
     * Get isPrivate
     *
     * @return string
     */
    public function getIsPrivate(): ?string
    {
        return $this->isPrivate;
    }

    /**
     * Set isPrivate
     *
     * @param string $isPrivate
     *
     * @return CustomerPostalAddress
     */
    public function setIsPrivate(?string $isPrivate): CustomerPostalAddress
    {
        return $this->setTrigger('isPrivate', $isPrivate);
    }

    /**
     * Get isPrivatePostalAddress
     *
     * @return string
     */
    public function getIsPrivatePostalAddress(): ?string
    {
        return $this->isPrivatePostalAddress;
    }

    /**
     * Set isPrivatePostalAddress
     *
     * @param string $isPrivatePostalAddress
     *
     * @return CustomerPostalAddress
     */
    public function setIsPrivatePostalAddress(?string $isPrivatePostalAddress): CustomerPostalAddress
    {
        return $this->setTrigger('isPrivatePostalAddress', $isPrivatePostalAddress);
    }

    /**
     * Get isRoleDelivery
     *
     * @return string
     */
    public function getIsRoleDelivery(): ?string
    {
        return $this->isRoleDelivery;
    }

    /**
     * Set isRoleDelivery
     *
     * @param string $isRoleDelivery
     *
     * @return CustomerPostalAddress
     */
    public function setIsRoleDelivery(?string $isRoleDelivery): CustomerPostalAddress
    {
        return $this->setTrigger('isRoleDelivery', $isRoleDelivery);
    }

    /**
     * Get isRoleHome
     *
     * @return string
     */
    public function getIsRoleHome(): ?string
    {
        return $this->isRoleHome;
    }

    /**
     * Set isRoleHome
     *
     * @param string $isRoleHome
     *
     * @return CustomerPostalAddress
     */
    public function setIsRoleHome(?string $isRoleHome): CustomerPostalAddress
    {
        return $this->setTrigger('isRoleHome', $isRoleHome);
    }

    /**
     * Get isRoleInvoice
     *
     * @return string
     */
    public function getIsRoleInvoice(): ?string
    {
        return $this->isRoleInvoice;
    }

    /**
     * Set isRoleInvoice
     *
     * @param string $isRoleInvoice
     *
     * @return CustomerPostalAddress
     */
    public function setIsRoleInvoice(?string $isRoleInvoice): CustomerPostalAddress
    {
        return $this->setTrigger('isRoleInvoice', $isRoleInvoice);
    }
}
