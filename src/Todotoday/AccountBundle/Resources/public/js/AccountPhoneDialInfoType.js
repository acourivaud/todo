import Vue from "vue";
import VuePhoneInput from './../../../../../../vue-src/components/base/vue-phone-input'

if(document.getElementById('accountPhoneDialInfoType')) {
  /* eslint-disable no-new */
  new Vue({
    el: '#accountPhoneDialInfoType',
    delimiters: ['#{', '}'],
    components: {
      'vue-phone-input': VuePhoneInput
    },
    data() {
      return {
        phoneNumber: null,
        phoneExtension: null
      }
    },
    created() {
      let phoneInitValue = document.getElementById('phone-init-value').getAttribute('value')
      if(phoneInitValue) {
        this.phoneNumber = phoneInitValue
      }

      let phoneExtensionInitValue = document.getElementById('phone-extension-init-value').getAttribute('value')
      if(phoneExtensionInitValue) {
        this.phoneExtension = phoneExtensionInitValue
      }
    },
    methods: {
      setValues(values) {
        this.phoneNumber = values.phoneNumber
        this.phoneExtension = values.dialPhoneExtension
      }
    }
  });
}
