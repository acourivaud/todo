Vue.component('transaction-button', {
    data: loadTransactionalInfoWidgetData,
    delimiters: ['${', '}'],
    methods: {
        switchPayment: function (button) {
            this.button = button;
        }
    },
    template: '#account_transactional_info_widget-template'
});

let transactionalComponent = new Vue({
    el: '#account_transactional_info_widget'
});