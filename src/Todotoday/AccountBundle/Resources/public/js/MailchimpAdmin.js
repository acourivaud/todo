(function($){
    $(document).ready(function (){
        var mcMenuLink = $('a[href="/admin/todotoday/mailchimp/list"]');
        if (mcMenuLink.length) {
            var host = window.location.host.split('.');
            if (host[0] === 'www') {
                mcMenuLink.hide();
            }
        }
    });
})(jQuery);