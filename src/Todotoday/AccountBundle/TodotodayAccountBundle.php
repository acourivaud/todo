<?php declare(strict_types = 1);
/**
 * PHP version 7
 */

namespace Todotoday\AccountBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Todotoday\AccountBundle\DependencyInjection\Compiler\AccountFormTypeCompilerPass;

/**
 * Class TodotodayAccountBundle
 *
 * @package    Todotoday\AccountBundle
 */
class TodotodayAccountBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function build(ContainerBuilder $container): void
    {
        parent::build($container);

        $container->addCompilerPass(new AccountFormTypeCompilerPass());
    }

    /**
     * {@inheritdoc}
     */
    public function getParent(): string
    {
        return 'FOSUserBundle';
    }
}
