<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 08/06/17
 * Time: 21:39
 */

namespace Todotoday\AccountBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Todotoday\AccountBundle\Entity\Account;

/**
 * Class RegistrationSuccessEvent
 *
 * @package Todotoday\AccountBundle\Event
 */
class RegistrationSuccessEvent extends Event
{
    public const USER_REGISTRATION_SUCCESS = 'registration.success';
    public const USER_CREATED = ' registration.user.created';

    /**
     * @var Account
     */
    protected $account;

    /**
     * RegistrationSuccessEvent constructor.
     *
     * @param Account $account
     */
    public function __construct(Account $account)
    {
        $this->account = $account;
    }

    /**
     * @return Account
     */
    public function getAccount(): Account
    {
        return $this->account;
    }
}
