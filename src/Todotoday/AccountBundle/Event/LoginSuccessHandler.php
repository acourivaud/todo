<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 11/06/17
 * Time: 17:26
 */

namespace Todotoday\AccountBundle\Event;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Services\AdherentManager;
use Todotoday\CoreBundle\Services\DomainContextManager;

/**
 * Class LoginSuccessHandler
 *
 * @package Todotoday\AccountBundle\Event
 */
class LoginSuccessHandler
{
    /**
     * @var DomainContextManager
     */
    private $domainContext;

    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var AdherentManager
     */
    private $adherentManager;

    /**
     * LoginSuccessHandler constructor.
     *
     * @param DomainContextManager   $domainContext
     * @param TokenStorage           $tokenStorage
     * @param EntityManagerInterface $entityManager
     * @param AdherentManager        $adherentManager
     */
    public function __construct(
        DomainContextManager $domainContext,
        TokenStorage $tokenStorage,
        EntityManagerInterface $entityManager,
        AdherentManager $adherentManager
    ) {
        $this->domainContext = $domainContext;
        $this->tokenStorage = $tokenStorage;
        $this->entityManager = $entityManager;
        $this->adherentManager = $adherentManager;
    }

    /**
     *
     * @param InteractiveLoginEvent $event
     *
     * @throws \UnexpectedValueException
     */
    public function onSecurityInteractivelogin(InteractiveLoginEvent $event): void
    {
        $user = $event->getAuthenticationToken()->getUser();
        $agencyRepo = $this->entityManager->getRepository('TodotodayCoreBundle:Agency');
        $agency = $this->domainContext->getCurrentContext()->getAgency();

        if ($user instanceof Adherent) {
            // if adherent, agency must be set
            // or auth on api
            // or agency must allow adherent
            // and curent agency must match link adherent agency
            if ((!$agency && 'api' !== $this->domainContext->getCurrentContextKey())
                || ($agency
                    && (!$agency->isAllowAdherent()
                        || count(
                            array_filter(
                                $agencyRepo->getAgencyByAccount($user),
                                function ($agencyUser) use ($agency) {
                                    return
                                        $agencyUser->getId() === $agency->getId();
                                }
                            )
                        ) < 1
                    )
                )) {
                // on ne set pas le token, la session n'est pas créé
                $this->tokenStorage->setToken();
            }
            // on valide la session et on vérifie les moyens de paiement de l'adhérent
            $this->adherentManager->checkPaymentMethod($user);
        }
    }
}
