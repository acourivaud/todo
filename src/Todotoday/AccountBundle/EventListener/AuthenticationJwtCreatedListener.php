<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 01/09/17
 * Time: 13:26
 */

namespace Todotoday\AccountBundle\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Enum\AgencyModuleEnum;

/**
 * Class AuthenticationJwtCreatedListener
 *
 * @package Todotoday\AccountBundle\EventListener
 */
class AuthenticationJwtCreatedListener
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * @var Session
     */
    private $session;

    /**
     * AuthenticationJwtCreatedListener constructor.
     *
     * @param EntityManagerInterface        $manager
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param Session                       $session
     */
    public function __construct(
        EntityManagerInterface $manager,
        AuthorizationCheckerInterface $authorizationChecker,
        Session $session
    ) {
        $this->manager = $manager;
        $this->authorizationChecker = $authorizationChecker;
        $this->session = $session;
    }

    /**
     * @param JWTCreatedEvent $event
     *
     * @throws \ReflectionException
     */
    public function onJwtCreated(JWTCreatedEvent $event): void
    {
        $payload = $event->getData();
        $user = $this->manager->getRepository(Account::class)
            ->findOneBy(
                [
                    'username' => $payload['username'],
                ]
            );

        $agencies = [];

        if ($linkAccountsAgencies = $this->manager->getRepository('TodotodayAccountBundle:LinkAccountAgency')
            ->getCurrentAgencies($user)) {
            foreach ($linkAccountsAgencies as $linkAccount) {
                $agencies[] = $linkAccount->getAgency()->getSlug();
            }
        }

        /** @var Agency[] $activeChatAgencies */
        $activeChatAgencies = array_filter(
            $this->manager->getRepository('TodotodayCoreBundle:Agency')
                ->findAll(),
            function ($agency) {
                /** @var Agency $agency */

                return $agency->hasModule(AgencyModuleEnum::CHAT);
            }
        );
        $activeAgencySlug = [];
        foreach ($activeChatAgencies as $agency) {
            $activeAgencySlug[] = $agency->getSlug();
        }

        $payload = $event->getData();
        $payload['fullname'] = $user->getFullName();
        $payload['firstname'] = $user->getFirstName();
        $payload['lastname'] = $user->getLastName();
        $payload['service'] = $this->getService($user);
        $payload['agencies'] = $agencies;
        $payload['activeChatAgencies'] = $activeAgencySlug;
        $payload['language'] = $this->session->get('_locale');
        $payload['grantedChat'] = $this->getService($user) === 'admin'
            ? false
            : $this->authorizationChecker->isGranted(
                'ROLE_TODOTODAY_CHAT'
            );
        $event->setData($payload);
    }

    /**
     * @param Account $account
     *
     * @return null|string
     * @throws \ReflectionException
     */
    private function getService(Account $account): ?string
    {
        $reflect = new \ReflectionClass($account);
        $type = strtolower($reflect->getShortName());

        if ('admin' === $type  && $account->hasRole('ROLE_CUSTOMER_SERVICE')) {
            return 'customer_service';
        }

        return $type;
    }
}
