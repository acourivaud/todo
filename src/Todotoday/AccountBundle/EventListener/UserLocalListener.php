<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 25/04/2017
 * Time: 08:59
 */

namespace Todotoday\AccountBundle\EventListener;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\CoreBundle\Services\DomainContextManager;

/**
 * Class UserLocalListener
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\EventListener
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class UserLocalListener
{
    /**
     * @var Session
     */
    private $session;

    /**
     * @var DomainContextManager
     */
    private $domainContextManager;

    /**
     * UserLocalListener constructor.
     *
     * @param Session              $session
     * @param DomainContextManager $domainContextManager
     */
    public function __construct(Session $session, DomainContextManager $domainContextManager)
    {
        $this->session = $session;
        $this->domainContextManager = $domainContextManager;
    }

    /**
     * @param InteractiveLoginEvent $event
     *
     * @throws \UnexpectedValueException
     */
    public function onInteractiveLogin(InteractiveLoginEvent $event): void
    {
        /** @var Account $user */
        if (!$user = $event->getAuthenticationToken()->getUser()) {
            return;
        }
        if (!$agency = $this->domainContextManager->getCurrentContext()->getAgency()) {
            return;
        }

        if (in_array($user->getLanguage(), $agency->getLanguages(), true)) {
            $this->session->set('_locale', $user->getLanguage());
        } else {
            $this->session->set('_locale', $agency->getDefaultLanguage());
        }
    }
}
