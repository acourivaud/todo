<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 08/06/17
 * Time: 21:45
 */

namespace Todotoday\AccountBundle\EventListener;

use Actiane\MailerBundle\Services\ActianeMailer;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Acl\Dbal\MutableAclProvider;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Exception\AclAlreadyExistsException;
use Symfony\Component\Security\Acl\Model\MutableAclInterface;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\Translation\Translator;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\AccountBundle\Event\RegistrationSuccessEvent;
use Todotoday\SocialBundle\Services\SocialGroupManager;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Services\UrlGenerator;
use Psr\Log\InvalidArgumentException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class RegistrationSubscriber
 *
 * @package Todotoday\AccountBundle\EventListener
 */
class RegistrationSubscriber implements EventSubscriberInterface
{
    /**
     * @var Router
     */
    protected $router;

    /**
     * @var ActianeMailer
     */
    private $mailer;

    /**
     * @var Translator
     */
    private $translator;

    /**
     * @var MutableAclInterface
     */
    private $acl;

    /**
     * @var SocialGroupManager
     */
    private $groupManager;

    /**
     * @var UrlGenerator
     */
    private $urlGenerator;

    /**
     * RegistrationSubscriber constructor.
     *
     * @param ActianeMailer      $mailer
     * @param Translator         $translator
     * @param MutableAclProvider $acl
     * @param SocialGroupManager $groupManager
     * @param UrlGenerator       $urlGenerator
     * @param Router             $router
     */
    public function __construct(
        ActianeMailer $mailer,
        Translator $translator,
        MutableAclProvider $acl,
        SocialGroupManager $groupManager,
        UrlGenerator $urlGenerator,
        Router $router
    ) {
        $this->mailer = $mailer;
        $this->translator = $translator;
        $this->acl = $acl;
        $this->groupManager = $groupManager;
        $this->urlGenerator = $urlGenerator;
        $this->router = $router;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return array(
            RegistrationSuccessEvent::USER_REGISTRATION_SUCCESS => 'onRegistrationSuccess',
            RegistrationSuccessEvent::USER_CREATED => 'onCreateAccount',
        );
    }

    /**
     * @param RegistrationSuccessEvent $event
     *
     * @throws \Symfony\Component\Security\Acl\Exception\InvalidDomainObjectException
     * @throws \Symfony\Component\Security\Acl\Exception\AclAlreadyExistsException
     */
    public function onCreateAccount(RegistrationSuccessEvent $event)
    {
        $account = $event->getAccount();
        $this->updateAcl($account);
    }

    /**
     * @param RegistrationSuccessEvent $event
     *
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Symfony\Component\Security\Acl\Exception\InvalidDomainObjectException
     * @throws \Symfony\Component\Security\Acl\Exception\AclAlreadyExistsException
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     * @throws \Twig_Error_Syntax
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Loader
     * @throws \Psr\Log\InvalidArgumentException
     */
    public function onRegistrationSuccess(RegistrationSuccessEvent $event): void
    {
        /* @var \Todotoday\AccountBundle\Entity\Adherent $account */
        $account = $event->getAccount();
        $lockerCode = $account->getLockerCode();
        $this->sendRegistrationMail($account, $lockerCode);
        $this->groupManager->processPendingInvitations($account);
    }

    /**
     * @param Account $account
     *
     * @throws \Symfony\Component\Security\Acl\Exception\AclAlreadyExistsException
     * @throws \Symfony\Component\Security\Acl\Exception\InvalidDomainObjectException
     */
    private function updateAcl(Account $account): void
    {
        try {
            $acl = $this->acl->createAcl(ObjectIdentity::fromDomainObject($account));
            $acl->insertObjectAce(UserSecurityIdentity::fromAccount($account), MaskBuilder::MASK_OWNER);
            $this->acl->updateAcl($acl);
        } catch (\Exception $e) {
            return;
        }
    }

    /**
     * @param Account $account
     * @param string  $lockerCode
     *
     * @throws \Twig_Error_Syntax
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Loader
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     * @throws \Symfony\Component\Security\Acl\Exception\InvalidDomainObjectException
     * @throws \Symfony\Component\Security\Acl\Exception\AclAlreadyExistsException
     * @throws \Psr\Log\InvalidArgumentException
     */
    private function sendRegistrationMail(Account $account, ?string $lockerCode = null): void
    {
        /* @var Agency $agency */
        if (!$agency = $account->getLinkAgencies()->first()->getAgency()) {
            throw new InvalidArgumentException('No agency found');
        }

        $agencyUrl = $this->urlGenerator->generateAbsoluteUrl($agency->getSlug());
        $downloadsUrl = $this->router->generate(
            'cms_todotoday_index_cgv',
            array(),
            UrlGeneratorInterface::ABSOLUTE_URL
        );

        $vars = [
            'lockerCode' => $lockerCode,
            'messageKey' => 'mail.validation_adh',
            'fullname' => $account->getFullName(),
            'agencyUrl' => $agencyUrl,
            'downloadsUrl' => $downloadsUrl,
        ];

        $objectKey = "";

        if ($account->getRegistrationRole() === 'ROLE_NOT_ADHERENT') {
            $vars['messageKey'] = 'mail.validation_nonadh';
            $objectKey = "_nonadh";
        }

        if ($account->getRegistrationRole() === 'ROLE_CO_ADHERENT') {
            $vars['messageKey'] = 'mail.validation_coadh';
            $objectKey = "_coadh";
        }

        $body = $this->mailer->renderTemplate(
            '@TodotodayAccount/Mail/email_registration_success.text.twig',
            $vars
        );
        $bodyHtml = $this->mailer->renderTemplate(
            '@TodotodayAccount/Mail/email_registration_success.html.twig',
            $vars
        );

        $object = $this->translator->trans('mail_object_subscriber' . $objectKey, [], 'todotoday');

        $this->mailer->createMail($account->getEmail(), $object);
        $this->mailer->getMessage()->setBody($bodyHtml, 'text/html')->addPart($body, 'text/plain');

        $this->updateAcl($account);

        $this->mailer->send();
    }
}
