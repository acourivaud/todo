<?php declare(strict_types=1);

namespace Todotoday\AccountBundle\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use FOS\UserBundle\Event\GetResponseNullableUserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CoreBundle\Services\DomainContextManager;

/**
 * Listener responsible to change the redirection at the end of the password resetting
 */
class PasswordResettingListener implements EventSubscriberInterface
{
    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var DomainContextManager
     */
    private $domainContext;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var int
     */
    private $ttlRequestPassword;

    /**
     * PasswordResettingListener constructor.
     *
     * @param UrlGeneratorInterface  $router
     * @param Session                $session
     * @param DomainContextManager   $domainContext
     * @param EntityManagerInterface $entityManager
     * @param int                    $ttlRequestPassword
     */
    public function __construct(
        UrlGeneratorInterface $router,
        Session $session,
        DomainContextManager $domainContext,
        EntityManagerInterface $entityManager,
        int $ttlRequestPassword
    ) {
        $this->router = $router;
        $this->session = $session;
        $this->domainContext = $domainContext;
        $this->entityManager = $entityManager;
        $this->ttlRequestPassword = $ttlRequestPassword;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            FOSUserEvents::RESETTING_RESET_SUCCESS => 'onPasswordResettingSuccess',
            FOSUserEvents::RESETTING_SEND_EMAIL_INITIALIZE => 'requestPasswordInitialize',
            FOSUserEvents::CHANGE_PASSWORD_SUCCESS => 'onPasswordChangeSuccess',
        ];
    }

    /**
     * @param FormEvent $event
     *
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     */
    public function onPasswordChangeSuccess(FormEvent $event): void
    {
        $url = $this->router->generate('account.profile_extra');
        $event->setResponse(new RedirectResponse($url));
    }

    /**
     * @param FormEvent $event
     *
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     */
    public function onPasswordResettingSuccess(FormEvent $event): void
    {
        $url = $this->router->generate('index_cms_concierge');
        $event->setResponse(new RedirectResponse($url));
    }

    /**
     * @param GetResponseNullableUserEvent $event
     *
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \UnexpectedValueException
     */
    public function requestPasswordInitialize(GetResponseNullableUserEvent $event): void
    {
        $agencyRepo = $this->entityManager->getRepository('TodotodayCoreBundle:Agency');
        $url = $this->router->generate('fos_user_resetting_request');

        if (!($user = $event->getUser())
            || (($user instanceof Adherent)
                && (!($agency = $this->domainContext->getCurrentContext()->getAgency())
                    || !$agency->isAllowAdherent()
                    || \count(
                        array_filter(
                            $agencyRepo->getAgencyByAccount($user),
                            function ($agencyUser) use ($agency) {
                                return
                                    $agencyUser->getId() === $agency->getId();
                            }
                        )
                    ) < 1
                ))
        ) {
            $this->session->getFlashBag()->add('error', 'account.unknown.user');
            $event->setResponse(new RedirectResponse($url));

            return;
        }

        if ($user && $user->isPasswordRequestNonExpired($this->ttlRequestPassword)) {
            $this->session->getFlashBag()->add('error', 'login.reset_password.error_ttl_request');
            $event->setResponse(new RedirectResponse($url));

            return;
        }
    }
}
