<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 10/04/17
 * Time: 13:55
 */

namespace Todotoday\AccountBundle\EventListener;

use Doctrine\Common\Persistence\ObjectManager;
use FOS\UserBundle\Model\UserInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;

/**
 * Class AuthenticationSuccessListener
 * @package Todotoday\AccountBundle\EventListener
 */
class AuthenticationSuccessListener
{
    /**
     * @var ObjectManager
     */
    private $manager;

    /**
     * AuthenticationSuccessListener constructor.
     *
     * @param ObjectManager $manager
     */
    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param AuthenticationSuccessEvent $event
     */
    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event)
    {
        $user = $event->getUser();

        if (!$user instanceof UserInterface) {
            return;
        }

        $data = $event->getData();
        $refreshToken = $data['refresh_token'];
        $result = $this->manager->getRepository('GesdinetJWTRefreshTokenBundle:RefreshToken')->findOneBy(
            array('refreshToken' => $refreshToken)
        );
        $datetime = $result->getValid();
        $data['refresh_token_expire'] = $datetime->format('c');
        $event->setData($data);
    }
}
