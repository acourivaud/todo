<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 03/06/17
 * Time: 09:08
 */

namespace Todotoday\AccountBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Todotoday\AccountBundle\Entity\Concierge;
use Todotoday\AccountBundle\Entity\LinkAccountAgency;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyLinkedMicrosoft;

/**
 * Class LoadMEPConciergeAccount
 * @package Todotoday\AccountBundle\DataFixtures\ORM
 */
class LoadMEPConciergeAccount extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return array(
            LoadAgencyLinkedMicrosoft::class,
        );
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     *
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     */
    public function load(ObjectManager $manager): void
    {
        foreach (LoadAgencyLinkedMicrosoft::getAgenciesLinkedSlug() as $agencySlug => $agencyArgs) {
            $this->createConciergeAccount($manager, $agencySlug, $agencyArgs);
        }
        $manager->flush();
    }

    /**
     * @param ObjectManager $manager
     * @param string        $agencySlug
     * @param array         $agencyArgs
     *
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     */
    public function createConciergeAccount(ObjectManager $manager, string $agencySlug, array $agencyArgs): void
    {
        $usernameSlug = 'concierge_' . $agencySlug;

        $conciergeAccount = new Concierge();
        $conciergeAccount->setFirstName('Concierge')
            ->setLastName($agencyArgs['name'])
            ->setRoles(array('ROLE_CONCIERGE'))
            ->setEnabled(true)
            ->setEmail($usernameSlug . '@actiane.com')
            ->setUsername($usernameSlug)
            ->setPlainPassword('Concierge3000');

        $manager->persist($conciergeAccount);

        $linkAgency = new LinkAccountAgency();
        $linkAgency->setRoles(array('ROLE_CONCIERGE'))
            ->setAgency($this->getReference('agency_' . $agencySlug))
            ->setAccount($conciergeAccount);

        $manager->persist($linkAgency);
    }
}
