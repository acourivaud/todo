<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 10/04/17
 * Time: 23:08
 */

namespace Todotoday\AccountBundle\DataFixtures\ORM;

use Actiane\ApiConnectorBundle\Services\Connections\Microsoft\MicrosoftDynamicsConnection;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Generator;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Entity\Api\Microsoft\Account;
use Todotoday\AccountBundle\Entity\LinkAccountAgency;
use Todotoday\AccountBundle\Enum\PaymentStatusEnum;
use Todotoday\AccountBundle\Repository\Api\Microsoft\AccountRepository;
use Todotoday\AccountBundle\Services\AdherentManager;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyLinkedMicrosoft;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * @SuppressWarnings(PHPMD.UnusedLocalVariable)
 * Class LoadAdherentLinkedMicrosoftData
 * @package Todotoday\AccountBundle\DataFixtures\ORM
 */
class LoadAdherentLinkedMicrosoftData extends AbstractFixture implements
    DependentFixtureInterface,
    ContainerAwareInterface
{
    /**
     * @var array
     */
    private static $adherentsSlug;

    /**
     * @var AdherentManager
     */
    private $adherentManager;

    /**
     * @var Generator
     */
    private $faker;

    /**
     * @var MicrosoftDynamicsConnection
     */
    private $connection;

    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * Used for unit testing
     *
     * @return array
     */
    public static function getAdherentsSlug(): array
    {
        if (!static::$adherentsSlug) {
            static::$adherentsSlug = array();
        }

        foreach (LoadAgencyLinkedMicrosoft::getAgenciesLinkedSlug() as $slug => $name) {
            static::$adherentsSlug[$slug] = 'adherent_linked_' . $slug;
        }

        return static::$adherentsSlug;
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return array(
            LoadAgencyLinkedMicrosoft::class,
        );
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     *
     * @throws \InvalidArgumentException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     */
    public function load(ObjectManager $manager): void
    {
//        if ($this->kernel->getEnvironment() === 'prod') {
//            return;
//        }
        foreach (static::getAdherentsSlug() as $slug => $value) {
            $adherentUnitTesting = $this->createAdherent($slug, $slug . '_test@actiane.com');
            $manager->persist($adherentUnitTesting);
            $this->setReference($value, $adherentUnitTesting);
        }

        $realAdherent1 = $this->createAdherent('arevalta', 'arevalta1@actiane.com');
        $realAdherent2 = $this->createAdherent('arevalta', 'arevalta2@actiane.com');
        $manager->persist($realAdherent1);
        $this->setReference('adherent_linked_real_1_arevalta', $realAdherent1);
        $manager->persist($realAdherent2);
        $this->setReference('adherent_linked_real_2_arevalta', $realAdherent2);

//        if ($this->kernel->getEnvironment() !== 'test') {
//            $fabrice = $this->createAdherent('carrefour-massy', null, 'EPA0530001741');
//            $julie = $this->createAdherent('coeurdefense', null, 'EPA3130002190');
//
//            $manager->persist($fabrice);
//            $manager->persist($julie);
//        }

        $manager->flush();
    }

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     *
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     */
    public function setContainer(ContainerInterface $container = null): void
    {
        if ($container) {
            $this->connection = $container->get('actiane.api_connector.connection.microsoft');
            $this->adherentManager = $container->get('todotoday.account.services.adherent_manager');
            $this->faker = $container->get('davidbadura_faker.faker');
            $this->kernel = $container->get('kernel');
        }
    }

    /**
     * @param string      $agencySlug
     * @param string|null $email
     * @param string|null $customerAccount
     *
     * @return Adherent
     * @throws \InvalidArgumentException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     */
    private function createAdherent(string $agencySlug, ?string $email, ?string $customerAccount = null): Adherent
    {
        /** @var Agency $agency */
        $agency = $this->getReference('agency_' . $agencySlug);
        $adherent = new Adherent();
        $linkAgency = new LinkAccountAgency();
        $linkAgency
            ->setAgency($agency)
            ->setAccount($adherent)
            ->setRoles(array('ROLE_ADHERENT'));
        $adherent->addLinkAgency($linkAgency);

        /** @var AccountRepository $repo */
        $repo = $this->connection->getRepository(Account::class);

        if ($email) {
            $accountMicrosoft = $repo->findOrCreateByEmailAddress($email);
            if (!$accountMicrosoft->getCustomerAccount()) {
                $accountMicrosoft
                    ->setPrimaryContactEmail($email)
                    ->setFirstName($this->faker->firstName)
                    ->setLastName($this->faker->lastName)
                    ->setName($accountMicrosoft->getFirstName() . ' ' . $accountMicrosoft->getLastName())
                    ->setCustomerGroupId('ADH')
                    ->setDataAreaId($agency->getDataAreaId())
                    ->setPaymentMethod('sepa')
                    ->setPartyType('PERSON')
                    ->setInFront('Yes')
                    ->setAddressCountryRegionId('FRA')
                    ->setSalesCurrencyCode($agency->getCurrency());

                $this->adherentManager->fillCustomerAddressBooks($accountMicrosoft, $adherent);
                $this->connection->persist($accountMicrosoft);
                $this->connection->flush();
            }
        } else {
            $accountMicrosoft = $repo->find(
                array(
                    'dataAreaId' => $agency->getDataAreaId(),
                    'customerAccount' => $customerAccount,
                )
            );
        }

        $mapping = $this->connection->getMapping(Account::class);
        $mapping->fillEntityInEntityDoctrine($accountMicrosoft, $adherent, true);
        $adherent
            ->setPaymentTypeSelectedString('sepa')
            ->setEnabled(true)
            ->setRoles(array('ROLE_ADHERENT'))
            ->setUsername($accountMicrosoft->getPrimaryContactEmail())
            ->setEmail($accountMicrosoft->getPrimaryContactEmail())
            ->setPlainPassword('test')
            ->setPaymentMethodStatus(PaymentStatusEnum::getNewInstance(PaymentStatusEnum::VALID));

        $accountCRM = $this->adherentManager->createAdherentInCrm($adherent, $agency);
        $adherent->setCrmIdAdherent($accountCRM->getAccountid());

        return $adherent;
    }
}
