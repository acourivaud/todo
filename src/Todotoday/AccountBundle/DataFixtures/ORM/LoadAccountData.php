<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 10/01/2017
 * Time: 18:26
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\DataFixtures\ORM
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */

namespace Todotoday\AccountBundle\DataFixtures\ORM;

use Actiane\PaymentBundle\Enum\PaymentTypeEnum;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Generator;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Exception\AclAlreadyExistsException;
use Symfony\Component\Security\Acl\Exception\InvalidDomainObjectException;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Entity\Admin;
use Todotoday\AccountBundle\Entity\Concierge;
use Todotoday\AccountBundle\Entity\LinkAccountAgency;
use Todotoday\AccountBundle\Entity\Partner;
use Todotoday\AccountBundle\Enum\AccountTitleEnum;
use Todotoday\AccountBundle\Enum\AdherentTypeEnum;
use Todotoday\AccountBundle\Enum\PaymentStatusEnum;
use Todotoday\AccountBundle\Enum\PersonGenderEnum;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyData;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadCorporateData;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Entity\Corporate;

/**
 * Class LoadAccountData
 *
 *
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\DataFixtures\ORM
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class LoadAccountData extends AbstractFixture implements DependentFixtureInterface, ContainerAwareInterface
{
    /**
     * @var string[]
     */
    protected static $adherentsSlugs;

    /**
     * @var string[]
     */
    protected static $adherentsMultiAgenciesSlugs;

    /**
     * @var string[]
     */
    protected static $usersSlugs;

    /**
     * @var string[]
     */
    protected static $partnerSlugs;

    /**
     * @var ObjectManager
     */
    protected $manager;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var int
     */
    private $iAccount = 1;

    /**
     * @var Generator
     */
    private $faker;

    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * Return all the multi agencies adherent
     *
     * @return string[]
     */
    public static function getAdherentsMultiAgenciesSlugs(): array
    {
        if (!static::$adherentsMultiAgenciesSlugs) {
            static::$adherentsMultiAgenciesSlugs = array();
        }

        return static::$adherentsMultiAgenciesSlugs;
    }

    /**
     * Do getAdherentsSlugs
     *
     * @return string[]
     */
    public static function getAdherentsSlugs(): array
    {
        if (!static::$adherentsSlugs) {
            static::$adherentsSlugs = array();

            for ($iteration = 0; $iteration < 3; $iteration++) {
                static::$adherentsSlugs[] = 'adherent' . $iteration;
            }
        }

        return static::$adherentsSlugs;
    }

    /**
     * Do getAllAccountsSlugs
     *
     * @return string[]
     */
    public static function getAllAccountsSlugs(): array
    {
        return array_merge(
            static::getAdherentsSlugs(),
            static::getConciergesSlugs(),
            static::getAdherentsMultiAgenciesSlugs()
        );
    }

    /**
     * Do getConciergesSlugs
     *
     * @return string[]
     */
    public static function getConciergesSlugs(): array
    {
        if (!static::$usersSlugs) {
            static::$usersSlugs = array();

            for ($iteration = 0; $iteration < 3; $iteration++) {
                static::$usersSlugs[] = 'concierge' . $iteration;
            }
        }

        return static::$usersSlugs;
    }

    /**
     * Return partners slugs
     *
     * @return array
     */
    public static function getPartnersSlugs(): array
    {
        if (!static::$partnerSlugs) {
            static::$partnerSlugs = array('noosphere', 'microsoft');
        }

        return static::$partnerSlugs;
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return array(
            LoadAgencyData::class,
            LoadCorporateData::class,
        );
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     *
     * @throws \Exception
     * @throws AclAlreadyExistsException
     * @throws ServiceCircularReferenceException
     * @throws ServiceNotFoundException
     * @throws InvalidDomainObjectException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     */
    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;
        /** @var Admin $root */
        $manager->persist(
            $root = (new Admin())
                ->setEmail('root@todotoday.com')
                ->setUsername('root')
                ->setPlainPassword('tdtd2017!')
                ->setTitle(AccountTitleEnum::getNewInstance(AccountTitleEnum::M))
                ->setPersonGender(PersonGenderEnum::getNewInstance(PersonGenderEnum::MALE))
                ->setFirstName($this->faker->firstNameMale())
                ->setLastName($this->faker->lastName())
                ->setEnabled(true)
                ->setRoles(
                    array(
                        'ROLE_SUPER_ADMIN',
                    )
                )
        );
        /** @var Admin $admin */
        $manager->persist(
            $admin = (new Admin())
                ->setEmail('admin@todotoday.com')
                ->setTitle(AccountTitleEnum::getNewInstance(AccountTitleEnum::M))
                ->setPersonGender(PersonGenderEnum::getNewInstance(PersonGenderEnum::MALE))
                ->setFirstName($this->faker->firstNameMale)
                ->setLastName($this->faker->lastName)
                ->setUsername('admin')
                ->setPlainPassword('tdtd2017!')
                ->setEnabled(true)
                ->setRoles(
                    array(
                        'ROLE_ADMIN',
                    )
                )
        );
        /** @var Partner $noosphere */
        $manager->persist(
            $noosphere = (new Partner())
                ->setEmail('noosphere@todotoday.com')
                ->setTitle(AccountTitleEnum::getNewInstance(AccountTitleEnum::M))
                ->setPersonGender(PersonGenderEnum::getNewInstance(PersonGenderEnum::MALE))
                ->setFirstName($this->faker->firstNameMale)
                ->setUsername('noosphere')
                ->setPlainPassword('noosphere')
                ->setEnabled(true)
                ->setRoles(
                    array(
                        'ROLE_PARTNER',
                        'ROLE_TODOTODAY_CORE_API_AGENCY_VIEW',
                        'ROLE_TODOTODAY_CORE_API_CORPORATE_VIEW',
                        'ROLE_TODOTODAY_RESET_PWD',
                    )
                )
        );
        /** @var Partner $microsoft */
        $manager->persist(
            $microsoft = (new Partner())
                ->setEmail('microsoft@todotoday.com')
                ->setTitle(AccountTitleEnum::getNewInstance(AccountTitleEnum::M))
                ->setPersonGender(PersonGenderEnum::getNewInstance(PersonGenderEnum::MALE))
                ->setFirstName($this->faker->firstNameMale)
                ->setUsername('microsoft')
                ->setPlainPassword('=#eJsK8$spyjAbVS')
                ->setEnabled(true)
                ->setRoles(
                    array(
                        'ROLE_PARTNER',
                        'ROLE_TODOTODAY_ACCOUNT_API_ADHERENT_CREATE',
                        'ROLE_TODOTODAY_ACCOUNT_API_ACCOUNT_OPERATOR',
                        'ROLE_TODOTODAY_CORE_API_AGENCY_OPERATOR',
                        'ROLE_TODOTODAY_CORE_API_CORPORATE_OPERATOR',
                        'ROLE_TODOTODAY_CART_API_CARTPRODUCT_VIEW',
                        'ROLE_MICROSOFT',
                    )
                )
        );

        $manager->persist(
            $cm = (new Admin())
                ->setEmail('cm@todotoday.com')
                ->setTitle(AccountTitleEnum::getNewInstance(AccountTitleEnum::M))
                ->setPersonGender(PersonGenderEnum::getNewInstance(PersonGenderEnum::MALE))
                ->setFirstName($this->faker->firstNameMale)
                ->setUsername('cm')
                ->setPlainPassword('cm')
                ->setEnabled(true)
                ->setRoles(
                    array(
                        'ROLE_COMMUNITY_MANAGER',
                    )
                )
        );

        $manager->flush();
        $this
            ->setOwnerAcl($admin)
            ->setOwnerAcl($root)
            ->setOwnerAcl($noosphere)
            ->setOwnerAcl($microsoft);
        $this->setReference('admin', $admin);
        $this->setReference('root', $root);
        $this->setReference('noosphere', $noosphere);
        $this->setReference('microsoft', $microsoft);
        $this->setReference('cm', $cm);

        if ($this->kernel->getEnvironment() === 'prod') {
            return;
        }

        $this->createAccounts(
            Concierge::class,
            array(
                'ROLE_CONCIERGE',
            ),
            static::getConciergesSlugs()
        );
        $this->createAccounts(
            Adherent::class,
            array(
                'ROLE_ADHERENT',
            ),
            static::getAdherentsSlugs()
        );
        $this->createAdherentMultiAgencies(
            'adherent12',
            array('demo0', 'demo1')
        );
    }

    /**
     * {@inheritdoc}
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     */
    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
        if ($container) {
            $this->faker = $container->get('davidbadura_faker.faker');
            $this->kernel = $container->get('kernel');
        }
    }

    /**
     * Do createAccounts
     *
     * @param string $class
     * @param array  $roles
     * @param array  $accountsSlugs
     *
     * @return void
     * @throws \Exception
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     * @throws InvalidDomainObjectException
     * @throws AclAlreadyExistsException
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     */
    protected function createAccounts(string $class, array $roles, array $accountsSlugs): void
    {
        $agenciesSlugs = LoadAgencyData::getAgenciesSlug();
        $corporatesSlugs = LoadCorporateData::getCorporatesSlug();
        /** @var Account $account */
        $accountBase = new $class();
        foreach ($accountsSlugs as $key => $accountSlug) {
            /** @var Corporate $corporate */
            $corporate = $this->getReference($corporatesSlugs[$key]);
            /** @var Agency $agency */
            $agency = $this->getReference('agency_' . $agenciesSlugs[$key]);
            /** @var Account|Adherent $account */
            $account = clone $accountBase;
            if ($account instanceof Adherent) {
                $account->setCustomerAccount('EPA-00000' . $this->iAccount)
                    ->setCustomerGroup(AdherentTypeEnum::getNewInstance(AdherentTypeEnum::ADH));
                $this->iAccount++;
            }
            $account
                ->setCorporate($corporate)
                ->setEmail($accountSlug . '@todotoday.com')
                ->setTitle(AccountTitleEnum::getNewInstance(AccountTitleEnum::M))
                ->setPersonGender(PersonGenderEnum::getNewInstance(PersonGenderEnum::MALE))
                ->setPaymentTypeSelectedString(PaymentTypeEnum::STRIPE)
                ->setPaymentMethodStatusString(PaymentStatusEnum::VALID)
                ->setUsername($accountSlug)
                ->setPlainPassword($accountSlug)
                ->setFirstName($this->faker->firstNameMale())
                ->setLastName($this->faker->lastName())
                ->setEnabled(true)
                ->setRoles($roles);
            $linkAccountAgency = (new LinkAccountAgency())
                ->setRoles($roles)
                ->setAgency($agency)
                ->setAccount($account);
            $this->manager->persist($linkAccountAgency);
            $this->manager->persist($account);
            $this->manager->flush();
            $this->setOwnerAcl($account);
            $this->setReference($accountSlug, $account);
        }
    }

    /**
     * @param string $slug
     * @param array  $agencies
     *
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Exception
     */
    protected function createAdherentMultiAgencies(string $slug, array $agencies): void
    {
        $role = 'ROLE_ADHERENT';
        $corporatesSlugs = LoadCorporateData::getCorporatesSlug();
        $adherent = new Adherent();
        /** @var Corporate $corporate */
        $corporate = $this->getReference($corporatesSlugs[0]);
        $linkAgencies = [];
        foreach ($agencies as $agency) {
            $linkAgencies[] = (new LinkAccountAgency())
                ->setAgency($this->getReference('agency_' . $agency))
                ->setRoles(array($role))
                ->setAccount($adherent);
        }
        $adherent
            ->setCustomerAccount('EPA0-00000' . $this->iAccount)
            ->setCustomerGroup(AdherentTypeEnum::getNewInstance(AdherentTypeEnum::ADH))
            ->setLastName($this->faker->lastName)
            ->setFirstName($this->faker->firstNameFemale)
            ->setCorporate($corporate)
            ->setEmail($slug . '@todotoday.com')
            ->setTitle(AccountTitleEnum::getNewInstance(AccountTitleEnum::MME))
            ->setPersonGender(PersonGenderEnum::getNewInstance(PersonGenderEnum::FEMALE))
            ->setUsername($slug)
            ->setPlainPassword($slug)
            ->setEnabled(true)
            ->setRoles(array($role));
        foreach ($linkAgencies as $linkAgency) {
            $this->manager->persist($linkAgency);
        }
        $this->manager->persist($adherent);
        $this->setReference($slug, $adherent);
        $this->manager->flush();
        $this->setOwnerAcl($adherent);
        $this->setReference($slug, $adherent);
        static::$adherentsMultiAgenciesSlugs[] = $slug;
        $this->iAccount++;
    }

    /**
     * Do setOwnerAcl
     *
     * @param Account $account
     *
     * @return LoadAccountData
     * @throws \Exception
     */
    protected function setOwnerAcl(Account $account): self
    {
        $aclProvider = $this->container->get('security.acl.provider');
        $acl = $aclProvider->createAcl(ObjectIdentity::fromDomainObject($account));
        $acl->insertObjectAce(UserSecurityIdentity::fromAccount($account), MaskBuilder::MASK_OWNER);
        $aclProvider->updateAcl($acl);

        return $this;
    }
}
