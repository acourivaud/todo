<?php declare(strict_types=1);

namespace Todotoday\SmartBannerAppBundle\Services;

use Symfony\Component\Security\Core\User\UserInterface;
use Gesdinet\JWTRefreshTokenBundle\Model\RefreshTokenManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Http\Authentication\AuthenticationSuccessHandler;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\DependencyInjection\Container;
use Todotoday\AccountBundle\Entity\Account;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

/**
 * Class DynamicLinkManager
 *
 * @package Todotoday\SmartBannerAppBundle\Services
 */
class DynamicLinkManager
{

    /**
     * @var JWTTokenManagerInterface
     */
    private $jwtTokenManager;

    /**
     * @var RefreshTokenManagerInterface
     */
    private $refreshTokenManager;

    /**
     * @var AuthenticationSuccessHandler
     */
    private $jwtSuccessHandler;

    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var Container
     */
    private $container;

    /**
     * DynamicLinkManager constructor.
     *
     * @param JWTTokenManagerInterface     $jwtTokenManager
     * @param AuthenticationSuccessHandler $jwtSuccessHandler
     * @param RefreshTokenManagerInterface $refreshTokenManager
     * @param TokenStorage                 $tokenStorage
     * @param Router                       $router
     * @param Container                    $container
     */
    public function __construct(
        JWTTokenManagerInterface $jwtTokenManager,
        AuthenticationSuccessHandler $jwtSuccessHandler,
        RefreshTokenManagerInterface $refreshTokenManager,
        TokenStorage $tokenStorage,
        Router $router,
        Container $container
    ) {
        $this->jwtTokenManager = $jwtTokenManager;
        $this->jwtSuccessHandler = $jwtSuccessHandler;
        $this->refreshTokenManager = $refreshTokenManager;
        $this->tokenStorage = $tokenStorage;
        $this->router = $router;
        $this->container = $container;
    }

    /**
     * @param UserInterface $user
     *
     * @return string
     */
    public function createJwt(UserInterface $user): string
    {
        $token = $this->jwtTokenManager->create($user);
        if (!$this->refreshTokenManager->getLastFromUsername($user->getUsername())) {
            $this->jwtSuccessHandler->handleAuthenticationSuccess($user);
        }

        return $token;
    }

    /**
     * @param UserInterface $user
     *
     * @return null|string
     */
    public function getRefreshToken(UserInterface $user)
    {
        if ($refreshToken = $this->refreshTokenManager->getLastFromUsername($user->getUsername())) {
            return $refreshToken;
        }

        return null;
    }

    /**
     *
     * @return null|string
     */
    public function getLink()
    {
        $home = $this->router->generate(
            'index_cms_concierge',
            [],
            UrlGeneratorInterface::ABSOLUTE_URL
        );

        //get jwt and refresh_token if user connected
        $user = $this->tokenStorage->getToken()->getUser();
        if ($user instanceof Account) {
            $jwt = $this->createJwt($user);
            $refreshToken = $this->getRefreshToken($user);
            if ($jwt) {
                $home .= '?jwt%3D' . $jwt;
                $home .= '%26refresh_token%3D' . $refreshToken;
            }
        }

        $args = '?link=' . $home;
        $args .= '&apn=' . $this->container->getParameter('dynamic_link_apn');
        $args .= '&amv=' . $this->container->getParameter('dynamic_link_amv');
        $args .= '&isi=' . $this->container->getParameter('dynamic_link_isi');
        $args .= '&ibi=' . $this->container->getParameter('dynamic_link_ibi');

        $url = $this->container->getParameter('dynamic_link_base_url') . $args;

        return $url;
    }
}
