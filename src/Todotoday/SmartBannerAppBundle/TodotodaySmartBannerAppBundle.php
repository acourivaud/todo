<?php declare(strict_types = 1);

namespace Todotoday\SmartBannerAppBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class TodotodaySmartBannerAppBundle extends Bundle
{
}
