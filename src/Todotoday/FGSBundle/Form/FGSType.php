<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 23/03/2017
 * Time: 09:12
 */

namespace Todotoday\FGSBundle\Form;

use Actiane\ToolsBundle\Services\AssetManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;
use Todotoday\CheckoutBundle\Entity\Api\Microsoft\SalesOrderLine;
use Todotoday\FGSBundle\Enum\FGSSubjectEnum;

/**
 * Class FGSType
 *
 * @category   Todo-Todev
 * @package    Todotoday\FGSBundle
 * @subpackage Todotoday\FGSBundle\Form\Api
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class FGSType extends AbstractType
{
    /**
     * @var AssetManager
     */
    private $assetManager;

    /**
     * FGSType constructor.
     *
     * @param AssetManager $assetManager
     */
    public function __construct(AssetManager $assetManager)
    {
        $this->assetManager = $assetManager;
        $this->assetManager->addJavascript('/dist/js/filters/base/translation.js');
        $this->assetManager->addJavascript('/dist/js/components/base/ACTLoader.js');
        $this->assetManager->addJavascript('/dist/js/bundles/FGSType.js');
    }

    /**
     * {@inheritdoc}
     * @throws \Symfony\Component\Validator\Exception\ConstraintDefinitionException
     * @throws \Symfony\Component\Validator\Exception\InvalidOptionsException
     * @throws \Symfony\Component\Validator\Exception\MissingOptionsException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \Symfony\Component\Form\Exception\LogicException
     * @throws \Symfony\Component\Form\Exception\UnexpectedTypeException
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        // obligé de mettre 0 car le formulaire ne se valide pas avec l'api
        // quand la valeur est null
        $orderLines = array(
            'fgs.all_order_lines' => 0,
        );
        /** @var SalesOrderLine $line */
        foreach ($options['orderLines'] as $line) {
            $label = $line->getLineDescription() . ' (' .
                twig_localized_currency_filter($line->getSalesPrice(), $options['currency'])
                . ')';
            $orderLines[$label] = $line->getInventoryLotId();
        }

        $builder->add(
            'inventoryLotId',
            ChoiceType::class,
            array(
                'choices' => $orderLines,
                'multiple' => false,
                'expanded' => true,
                'label' => 'fgs.form_select_order_line',
                'translation_domain' => 'todotoday',
            )
        );

        $builder->add(
            'listSubjectsHierarchy',
            HiddenType::class,
            array(
                'attr' => array(
                    'hierarchy' => json_encode(FGSSubjectEnum::getAllHierarchy()),
                ),
            )
        );

        $builder->add(
            'subject',
            ChoiceType::class,
            array(
                'choices' => FGSSubjectEnum::getLevelOneFormChoices(),
                'choice_value' => function ($value) {
                    return $value;
                },
                'multiple' => false,
                'expanded' => false,
                'label' => 'fgs.motif',
                'translation_domain' => 'todotoday',
                'constraints' => array(
                    new NotNull(),
                ),
                'attr' => array(
                    'v-model' => 'subjectLevelOne',
                ),
            )
        );

        $builder->add(
            'subjectLevel2',
            HiddenType::class,
            array(
                'label' => 'fgs.motif',
                'translation_domain' => 'todotoday',
                'attr' => array(
                    'v-model' => 'subjectLevelTwo',
                ),
            )
        );

        $builder->add(
            'observation',
            TextareaType::class,
            array(
                'label' => 'fgs.comment',
                'translation_domain' => 'todotoday',
                'attr' => array(
                    'placeholder' => 'fgs.comment',
                ),
                'required' => true,
            )
        );
    }

    /**
     * {@inheritdoc}
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            array(
                'orderLines' => array(),
                'currency' => null,
            )
        );
    }
}
