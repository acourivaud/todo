<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 02/08/17
 * Time: 09:58
 */

namespace Todotoday\FGSBundle\Tests\Controller\Api;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Actiane\UnitFonctionalTestBundle\Traits\AccessDeniedForNotAdherentTestableTrait;
use Actiane\UnitFonctionalTestBundle\Traits\AccessGrantedForAdherentMicrosoftTestableTrait;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAccountData;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAdherentLinkedMicrosoftData;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyLinkedMicrosoft;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class FGSApiControllerTest
 * @package Todotoday\FGSBundle\Tests\Controller\Api
 */
class FGSApiControllerTest extends WebTestCase
{
    use AccessDeniedForNotAdherentTestableTrait, AccessGrantedForAdherentMicrosoftTestableTrait;

    /**
     * @small
     * @group ignore
     */
    public function testCreateFgs(): void
    {
        // Ne peux pas fonctionner tant que la synchro CRM ==> ax est broken sur uat
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent_linked_arevalta');
        /** @var Agency $agency */
        $agency = $adherent->getLinkAgencies()->first()->getAgency();

        $this->loginAs($adherent, 'api');
        $client = $this->makeClient();
        $url = $this->getUrl('todotoday.fgs.api.fgs.post');
        $client->request(
            'POST',
            $url,
            array(
                'subjet' => '00001',
                'observation' => 'test via api',
                'orderId' => 'CDE01983884',
                'inventoryLotId' => 'EPA-018440',
            ),
            array(),
            array('HTTP_AGENCY' => $agency->getSlug())
        );
        $this->assertStatusCode(200, $client);
    }

    /**
     * @small
     * @dataProvider notAdherentProvider
     *
     * @param string $slug
     */
    public function testAccessDeniedForNotAdherent(string $slug): void
    {
        $url = $this->getUrl('todotoday.fgs.api.fgs.get_all');
        $this->accessDeniedForNotAdherent($slug, 'api', $url);
    }

    /**
     * @small
     * @dataProvider adherentMicrosoftProvider
     *
     * @param string $agency
     * @param string $adherent
     */
    public function testGetAllSuccess(string $agency, string $adherent): void
    {
        $url = $this->getUrl('todotoday.fgs.api.fgs.get_all');
        $this->succesRoute($adherent, 'api', $url, $agency);
    }

    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return [
            LoadAccountData::class,
            LoadAgencyLinkedMicrosoft::class,
            LoadAdherentLinkedMicrosoftData::class,
        ];
    }
}
