<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 01/08/17
 * Time: 17:44
 */

namespace Todotoday\FGSBundle\Tests\Repository\Api\MicrosoftCRM;

use Actiane\ApiConnectorBundle\Tests\Repository\Api\AbstractRepositoryTestCase;

/**
 * Class LigneDeCommandeCRMRepositoryTest
 * @package Todotoday\FGSBundle\Tests\Repository\Api\MicrosoftCRM
 */
class LigneDeCommandeCRMRepositoryTest extends AbstractRepositoryTestCase
{
    /**
     * Do getRepositoryId
     *
     * @return string
     */
    protected function getRepositoryId(): string
    {
        return 'todotoday.fgs.repository.api.ligne_commande';
    }
}
