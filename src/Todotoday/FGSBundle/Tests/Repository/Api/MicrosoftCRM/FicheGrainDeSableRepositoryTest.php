<?php declare(strict_types=1);

namespace Todotoday\FGSBundle\Tests\Repository\Api\MicrosoftCRM;

use Actiane\ApiConnectorBundle\Tests\Repository\Api\AbstractRepositoryTestCase;

/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 01/08/17
 * Time: 16:36
 */
class FicheGrainDeSableRepositoryTest extends AbstractRepositoryTestCase
{
    /**
     * Do getRepositoryId
     *
     * @return string
     */
    protected function getRepositoryId(): string
    {
        return 'todotoday.fgs.repository.api.fgs';
    }
}
