<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 01/08/17
 * Time: 17:10
 */

namespace Todotoday\FGSBundle\Tests\Repository\Api\MicrosoftCRM;

use Actiane\ApiConnectorBundle\Tests\Repository\Api\AbstractRepositoryTestCase;

/**
 * Class SalesOrderCRMRepositoryTest
 * @package Todotoday\FGSBundle\Tests\Repository\Api\MicrosoftCRM
 */
class SalesOrderCRMRepositoryTest extends AbstractRepositoryTestCase
{
    /**
     * Do getRepositoryId
     *
     * @return string
     */
    protected function getRepositoryId(): string
    {
        return 'todotoday.fgs.repository.api.salesorder';
    }
}
