<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 02/08/17
 * Time: 08:53
 */

namespace Todotoday\FGSBundle\Tests\Services;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAdherentLinkedMicrosoftData;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Exceptions\AdherentCRMIdMissingException;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyLinkedMicrosoft;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\FGSBundle\Entity\Api\MicrosoftCRM\FicheGrainDeSable;
use Todotoday\FGSBundle\Entity\Api\MicrosoftCRM\LigneDeCommandeCRM;
use Todotoday\FGSBundle\Entity\Api\MicrosoftCRM\SalesOrderCRM;
use Todotoday\FGSBundle\Exceptions\LigneDeCommandeNotFoundException;
use Todotoday\FGSBundle\Exceptions\SalesOrderNotFoundException;
use Todotoday\FGSBundle\Repository\Api\MicrosoftCRM\LigneDeCommandeCRMRepository;
use Todotoday\FGSBundle\Repository\Api\MicrosoftCRM\SalesOrderCRMRepository;

/**
 * Class FgsManagerTest
 * @package Todotoday\FGSBundle\Tests\Services
 */
class FgsManagerTest extends WebTestCase
{
    /**
     * @small
     */
    public function testCreateFgsSuccess()
    {
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent_linked_arevalta');
        /** @var Agency $agency */

//        /** @var SalesOrderCRMRepository $repo */
//        $repo = $this->getContainer()->get('actiane.api_connector.connection.crm')
//            ->getRepository(SalesOrderCRM::class);
//        /** @var SalesOrderCRM $salesOrderCrm */
//        $salesOrderCrm = $repo->findAll(1)[0];

//        $refAx = $salesOrderCrm->getName();
        $refAx = 'CDE01983884';

        $agency = $adherent->getLinkAgencies()->first()->getAgency();
        $manager = $this->getContainer()->get('todotoday.fgs.manager');
        $fgs = $manager->createFgs($agency, $adherent, 100000000, 100000001, 'test', $refAx, 'EPA-018440');

        self::assertInstanceOf(FicheGrainDeSable::class, $fgs);
    }

//    /**
//     * @small
//     */
//    public function testCreateFgsFailInvalidCommand()
//    {
//        $this->expectException(SalesOrderNotFoundException::class);
//        /** @var Adherent $adherent */
//        $adherent = self::getFRR()->getReference('adherent_linked_arevalta');
//        /** @var Agency $agency */
//        $agency = $adherent->getLinkAgencies()->first()->getAgency();
//        $manager = $this->getContainer()->get('todotoday.fgs.manager');
//        $manager->createFgs($agency, $adherent, 100000000, 'test', 'inconnu');
//    }

//    /**
//     * @small
//     */
//    public function testCreateFgsFailInvalidLigneCommande()
//    {
//        $this->expectException(LigneDeCommandeNotFoundException::class);
//        /** @var Adherent $adherent */
//        $adherent = self::getFRR()->getReference('adherent_linked_arevalta');
//        /** @var Agency $agency */
//
//        /** @var SalesOrderCRMRepository $repo */
//        $repo = $this->getContainer()->get('actiane.api_connector.connection.crm')
//            ->getRepository(SalesOrderCRM::class);
//        /** @var SalesOrderCRM $exemple */
//        $exemple = $repo->findAll(1)[0];
//
//        $agency = $adherent->getLinkAgencies()->first()->getAgency();
//        $manager = $this->getContainer()->get('todotoday.fgs.manager');
//        $manager->createFgs($agency, $adherent, 100000000, 'test', $exemple->getName(), 'inconnu');
//    }

    /**
     * @small
     */
    public function testGetAllFgsByAdherent(): void
    {
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent_linked_arevalta');
        $manager = $this->getContainer()->get('todotoday.fgs.manager');

        $result = $manager->getAllFgsByAdherent($adherent, null);

        self::assertNotNull($result);
    }

    /**
     * @small
     */
    public function testGetAllFgsByAdherentWithNoIdCrm(): void
    {
        $this->expectException(AdherentCRMIdMissingException::class);
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent_linked_arevalta');
        $manager = $this->getContainer()->get('todotoday.fgs.manager');
        $adherent->setCrmIdAdherent(null);
        $manager->getAllFgsByAdherent($adherent, null);
    }

    /**
     * @small
     */
    public function testFindOrderCrm(): void
    {
        /** @var SalesOrderCRMRepository $repo */
        $repo = $this->getContainer()->get('actiane.api_connector.connection.crm')
            ->getRepository(SalesOrderCRM::class);

        /** @var SalesOrderCRM $exemple */
        $exemple = $repo->findAll(1)[0];

        $result = $repo->findOrderCrmByRefAx($exemple->getName());

        static::assertInstanceOf(SalesOrderCRM::class, $result);
    }

    /**
     * @small
     */
    public function testFindLigneCommandCrm(): void
    {
        /** @var LigneDeCommandeCRMRepository $repo */
        $repo = $this->getContainer()->get('actiane.api_connector.connection.crm')
            ->getRepository(LigneDeCommandeCRM::class);

        /** @var LigneDeCommandeCRM $exemple */
        $exemple = $repo->findAll(1)[0];

        $result = $repo->findLigneByInventoryLot($exemple->getApsIdcommandline());

        static::assertInstanceOf(LigneDeCommandeCRM::class, $result);
    }

    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return [
            LoadAgencyLinkedMicrosoft::class,
            LoadAdherentLinkedMicrosoftData::class,
        ];
    }
}
