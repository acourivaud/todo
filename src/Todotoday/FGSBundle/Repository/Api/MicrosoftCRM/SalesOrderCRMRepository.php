<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 01/08/17
 * Time: 17:09
 */

namespace Todotoday\FGSBundle\Repository\Api\MicrosoftCRM;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiRepository;
use Todotoday\FGSBundle\Entity\Api\MicrosoftCRM\SalesOrderCRM;

/**
 * Class SalesOrderCRMRepository
 * @package Todotoday\FGSBundle\Repository\Api\MicrosoftCRM
 */
class SalesOrderCRMRepository extends AbstractApiRepository
{
    /**
     * @param string $refOrderAx
     *
     * @return null|SalesOrderCRM
     * @throws \InvalidArgumentException
     */
    public function findOrderCrmByRefAx(string $refOrderAx): ?SalesOrderCRM
    {
        $filter = 'name eq \'' . $refOrderAx . '\'';

        return $this->createQueryBuilder()
                   ->addCustomQuery(
                       '$filter',
                       $filter
                   )
                   ->getRequest()
                   ->execute()[0];
    }
}
