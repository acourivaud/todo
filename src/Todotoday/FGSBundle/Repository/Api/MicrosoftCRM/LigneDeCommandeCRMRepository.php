<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 01/08/17
 * Time: 17:43
 */

namespace Todotoday\FGSBundle\Repository\Api\MicrosoftCRM;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiRepository;
use Todotoday\FGSBundle\Entity\Api\MicrosoftCRM\LigneDeCommandeCRM;

/**
 * Class LigneDeCommandeCRMRepository
 * @package Todotoday\FGSBundle\Repository\Api\MicrosoftCRM
 */
class LigneDeCommandeCRMRepository extends AbstractApiRepository
{
    /**
     * @param string $inventoryLotId
     *
     * @return null|LigneDeCommandeCRM
     * @throws \InvalidArgumentException
     */
    public function findLigneByInventoryLot(string $inventoryLotId): ?LigneDeCommandeCRM
    {
        $filter = 'aps_idcommandline eq \'' . $inventoryLotId . '\'';

        return $this->createQueryBuilder()
                   ->addCustomQuery(
                       '$filter',
                       $filter
                   )
                   ->getRequest()
                   ->execute()[0];
    }
}
