<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 01/08/17
 * Time: 16:32
 */

namespace Todotoday\FGSBundle\Repository\Api\MicrosoftCRM;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiRepository;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\FGSBundle\Entity\Api\MicrosoftCRM\FicheGrainDeSable;

/**
 * Class FicheGraindDeSableRepository
 * @package Todotoday\FGSBundle\Repository\Api\MicrosoftCRM
 */
class FicheGraindDeSableRepository extends AbstractApiRepository
{
    /**
     * @param Adherent $adherent
     *
     * @return FicheGrainDeSable[]|null
     * @throws \InvalidArgumentException
     */
    public function getAllByAdherent(Adherent $adherent): ?array
    {
        $filter = '_aps_adherent_value eq ' . $adherent->getCrmIdAdherent();

        return $this->createQueryBuilder()
            ->addCustomQuery(
                '$filter',
                $filter
            )
            ->addCustomQuery(
                '$orderby',
                'createdon desc'
            )
            ->getRequest()
            ->execute();
    }
}
