<?php declare(strict_types=1);

namespace Todotoday\FGSBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class TodotodayFGSBundle
 * @package Todotoday\FGSBundle
 */
class TodotodayFGSBundle extends Bundle
{
}
