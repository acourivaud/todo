<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 06/03/17
 * Time: 11:56
 */

namespace Todotoday\FGSBundle\Controller\Api;

use FOS\RestBundle\Controller\Annotations as FosRest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Todotoday\CheckoutBundle\Entity\Api\Microsoft\SalesOrderHeader;
use Todotoday\CoreBundle\Controller\Api\AbstractApiController;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Enum\AgencyModuleEnum;
use Todotoday\CoreBundle\Exceptions\AgencyMissingException;
use Todotoday\CoreBundle\Exceptions\AgencyModuleDisabledException;
use Todotoday\FGSBundle\Enum\FGSSubjectEnum;
use Todotoday\FGSBundle\Form\FGSType;
use Todotoday\FGSBundle\Services\FgsManager;

/**
 * Class FGSApiController
 *
 * @package Todotoday\FGSBundle\Controller\Api
 * @FosRest\NamePrefix("todotoday.fgs.api.fgs.")
 * @FosRest\Prefix(value="/fgs")
 */
class FGSApiController extends AbstractApiController
{
    /**
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Retrieve FGS for current user",
     *     description="Retrieve FGS for current user",
     *     output={
     *           "class"="array<Todotoday\FGSBundle\Entity\Api\MicrosoftCRM\FicheGrainDeSable>",
     *           "groups"={"Default"}
     *     },
     *     views={"default","FGS"},
     *     section="FGS"
     * )
     *
     * @FosRest\Route("", options={"expose"=true})
     *
     * @param Request $request
     *
     * @return Response
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyModuleDisabledException
     * @throws \Todotoday\AccountBundle\Exceptions\AdherentCRMIdMissingException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingException
     * @throws \LogicException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \UnexpectedValueException
     * @throws \InvalidArgumentException
     * @throws AgencyModuleDisabledException
     */
    public function getAllAction(Request $request): Response
    {
        $agency = $this->getAgency();
        if (!$agency->hasModule(AgencyModuleEnum::FGS)) {
            throw new AgencyModuleDisabledException();
        }

        if (!$this->isGranted('VIEW', $this)) {
            throw $this->createAccessDeniedException();
        }

        $adherent = $this->getUser();
        $locale = $request->getLocale();

        /** @var FgsManager $manager */
        $manager = $this->get($this->getServiceRepository());
        $result = $manager->getAllFgsByAdherent($adherent, $locale);
        $view = $this->view($result);

        return $this->handleView($view);
    }

    /**
     * Create a FGS
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Create a FGS",
     *     description="Create a FGS",
     *     output={
     *           "class"="Todotoday\FGSBundle\Entity\Api\MicrosoftCRM\FicheGrainDeSable",
     *           "groups"={"Default"}
     *     },
     *     views={"default","FGS"},
     *     section="FGS",
     *     input="Todotoday\FGSBundle\Form\Api\FGSType"
     * )
     *
     * @FosRest\Route("", options={"expose"=true})
     *
     * @param Request $request
     *
     * @return Response
     * @throws \Todotoday\CheckoutBundle\Exception\OrderNotFoundException
     * @throws \Todotoday\CheckoutBundle\Exception\OrderNotBelongsToAdherentException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \Todotoday\FGSBundle\Exceptions\SalesOrderNotFoundException
     * @throws \Todotoday\FGSBundle\Exceptions\LigneDeCommandeNotFoundException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingCrmTeamMissingException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingCrmServiceClientMissingException
     *
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Todotoday\AccountBundle\Exceptions\AdherentCRMIdMissingException
     * @throws \LogicException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \UnexpectedValueException
     */
    public function postAction(Request $request): Response
    {
        $agency = $this->getAgency();
        if (!$agency->hasModule(AgencyModuleEnum::FGS)) {
            throw new AgencyModuleDisabledException();
        }

        if (!$this->isGranted('CREATE', $this)) {
            throw $this->createAccessDeniedException();
        }

        $adherent = $this->getUser();
        /** @var FgsManager $manager */
        $agency = $this->getAgency();

        $manager = $this->get($this->getServiceRepository());
        $orderManager = $this->get('todotoday.order.manager');

        $orderId = $request->request->get('orderId');
        $order = $orderManager->getOrder($adherent, $orderId);

        $data = $form = $this->createForm(
            $this->getFormType(),
            null,
            array(
                'orderLines' => $orderManager->getOrderLines($order),
                'csrf_protection' => false,
                'allow_extra_fields' => true,
                'currency' => $order ? $order->getCurrencyCode() : null,
            )
        );

        $form->submit($request->request->all());

        if ($form->isValid()) {
            $data = $form->getData();

            $subject1 = (int) $data['subject'];
            $subject2 = $data['subjectLevel2'] ? (int) $data['subjectLevel2'] : null;

            $manager->createFgs(
                $agency,
                $adherent,
                $subject1,
                $subject2,
                $data['observation'],
                $orderId,
                0 === $data['inventoryLotId'] ? null : $data['inventoryLotId']
            );
        }

        $view = $this->view($data);

        return $this->handleView($view);
    }

    /**
     * Get the formType for the current entity class link to the controller
     *
     * @return string
     */
    protected function getFormType(): string
    {
        return FGSType::class;
    }

    /**
     * Get the serivce name from custum repository's entity
     *
     * @return string
     */
    protected function getServiceRepository(): string
    {
        return 'todotoday.fgs.manager';
    }

    /**
     * @return null|Agency
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \UnexpectedValueException
     * @throws AgencyMissingException
     */
    private function getAgency(): ?Agency
    {
        if (!$agency = $this->get('todotoday.core.domain_context.api')->getAgency()) {
            throw new AgencyMissingException('Agency is missing');
        }

        return $agency;
    }
}
