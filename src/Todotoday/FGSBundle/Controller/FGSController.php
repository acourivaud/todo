<?php declare(strict_types=1);

namespace Todotoday\FGSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CheckoutBundle\Entity\Api\Microsoft\SalesOrderHeader;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Enum\AgencyModuleEnum;
use Todotoday\CoreBundle\Exceptions\AgencyModuleDisabledException;
use Todotoday\FGSBundle\Form\FGSType;

/**
 * Class FGSController
 *
 * @package Todotoday\FGSBundle\Controller
 * @Route("/fgs")
 */
class FGSController extends Controller
{
    /**
     * @Route("/", name="fgs_index", options={"expose"=true})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyModuleDisabledException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     * @throws AgencyModuleDisabledException
     */
    public function indexAction(Request $request): Response
    {
        $agency = $this->getAgency();
        if (!$agency->hasModule(AgencyModuleEnum::FGS)) {
            throw new AgencyModuleDisabledException();
        }

        if (!$this->isGranted('ROLE_FGS')) {
            throw $this->createAccessDeniedException();
        }

        $user = $this->getUser();
        $locale = $request->getLocale();

        $fgsManager = $this->get('todotoday.fgs.manager');
        $allFGS = $fgsManager->getAllFgsByAdherent($user, $locale);

        return $this->render(
            'fgs/fgs.html.twig',
            array(
                'all_fgs' => $allFGS,
            )
        );
    }

    /**
     * @Route("/select", name="fgs_select_order", options={"expose"=true})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     * @throws AgencyModuleDisabledException
     */
    public function selectAction(): Response
    {
        $agency = $this->getAgency();
        if (!$agency->hasModule(AgencyModuleEnum::FGS)) {
            throw new AgencyModuleDisabledException();
        }

        if (!$this->isGranted('ROLE_FGS')) {
            throw $this->createAccessDeniedException();
        }

        return $this->render(
            'fgs/fgs_select_order.html.twig'
        );
    }

    /**
     * @Route("/new/{orderId}", name="fgs_new", options={"expose"=true})
     *
     * @param string  $orderId
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Todotoday\CheckoutBundle\Exception\OrderNotFoundException
     * @throws \Todotoday\CheckoutBundle\Exception\OrderNotBelongsToAdherentException
     * @throws \Symfony\Component\Form\Exception\UnexpectedTypeException
     * @throws \Symfony\Component\Form\Exception\LogicException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \Todotoday\FGSBundle\Exceptions\SalesOrderNotFoundException
     * @throws \Todotoday\FGSBundle\Exceptions\LigneDeCommandeNotFoundException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingCrmTeamMissingException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingCrmServiceClientMissingException
     * @throws \Todotoday\AccountBundle\Exceptions\AdherentCRMIdMissingException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     * @throws AgencyModuleDisabledException
     */
    public function newAction(string $orderId = null, Request $request): Response
    {
        $agency = $this->getAgency();
        if (!$agency->hasModule(AgencyModuleEnum::FGS)) {
            throw new AgencyModuleDisabledException();
        }

        if (!$this->isGranted('ROLE_FGS')) {
            throw $this->createAccessDeniedException();
        }

        /** @var Adherent $adherent */
        $adherent = $this->getUser();

        $orderManager = $this->get('todotoday.order.manager');

        /** @var SalesOrderHeader $order */
        $order = $orderManager->getOrder($adherent, $orderId);
        $orderManager->addLineAndPictos($order);

        $form = $this->createForm(
            FGSType::class,
            null,
            array(
                'orderLines' => $order->getOrderLines(),
                'currency' => $order->getCurrencyCode(),
            )
        );
        $form->add('submit', SubmitType::class, array('label' => 'fgs.send_fgs', 'translation_domain' => 'todotoday'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $fgsManager = $this->get('todotoday.fgs.manager');

            $subject1 = (int) $data['subject'];
            $subject2 = $data['subjectLevel2'] ? (int) $data['subjectLevel2'] : null;

            $fgsManager->createFgs(
                $this->getAgency(),
                $adherent,
                $subject1,
                $subject2,
                $data['observation'],
                $orderId,
                0 === $data['inventoryLotId'] ? null : $data['inventoryLotId']
            );

            return $this->redirectToRoute('fgs_index');
        }

        return $this->render(
            'fgs/fgs_new.html.twig',
            array(
                'order' => $order,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * Do getAgency
     *
     * @return Agency
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \UnexpectedValueException
     * @throws NotFoundHttpException
     */
    protected function getAgency(): Agency
    {
        if (!$agency = $this->get('todotoday.core.domain_context')->getCurrentContext()->getAgency()) {
            throw $this->createNotFoundException();
        }

        return $agency;
    }
}
