<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 08/06/17
 * Time: 11:06
 */

namespace Todotoday\FGSBundle\Enum;

use Actiane\ToolsBundle\Enum\AbstractEnum;

/**
 * Class PersonGenderEnum
 * @package Todotoday\AccountBundle\Enum
 */
class FGSSubjectEnum extends AbstractEnum
{
    //Level 1
    public const PRESTATION = 100000000;
    public const FACTURE = 100000001;
    public const LIVRAISON = 100000002;
    public const TECHNIQUE = 100000003;
    public const SERVICE_CLIENT = 100000004;
    public const OTHER = 100000005;
    //Level 2
    public const PRESTATION_NOT_CORRESPONDING = 100000000;
    public const PRESTATION_ARTICLE_LOST = 100000001;
    public const PRESTATION_RDV_CANCELED = 100000002;
    public const PRESTATION_RDV_TIME = 100000003;
    public const PRESTATION_OTHER = 100000008;
    public const LIVRAISON_PROTECTION = 100000004;
    public const LIVRAISON_LATE = 100000005;
    public const LIVRAISON_OTHER = 100000009;
    public const TECHNIQUE_CONNECT = 100000006;
    public const TECHNIQUE_ORDER = 100000007;
    public const TECHNIQUE_OTHER = 1000000010;

    /**
     * @return array
     */
    public static function getLevelOneFormChoices(): array
    {
        $allHierarchy = static::getAllHierarchy();
        $levelOneChoices = array(
            'fgs.select_motif' => null,
        );

        foreach ($allHierarchy as $mainSubject) {
            $levelOneChoices[$mainSubject['text']] = $mainSubject['value'];
        }

        return $levelOneChoices;
    }

    /**
     * @param int $subjectLevelOne
     *
     * @return array
     */
    public static function getChilds(int $subjectLevelOne): array
    {
        $allHierarchy = static::getAllHierarchy();

        foreach ($allHierarchy as $mainSubject) {
            if ($mainSubject['value'] === $subjectLevelOne) {
                return $mainSubject['childs'];
            }
        }

        return array();
    }

    /**
     * @return array
     */
    public static function getAllHierarchy(): array
    {
        return array(
            array(
                'text' => 'fgs.sujet_prestation',
                'value' => self::PRESTATION,
                'childs' => array(
                    static::getSubjectRow('fgs.sujet_prestation_not_corresponding', self::PRESTATION_NOT_CORRESPONDING),
                    static::getSubjectRow('fgs.sujet_prestation_article_lost', self::PRESTATION_ARTICLE_LOST),
                    static::getSubjectRow('fgs.sujet_prestation_rdv_canceled', self::PRESTATION_RDV_CANCELED),
                    static::getSubjectRow('fgs.sujet_prestation_rdv_schedule', self::PRESTATION_RDV_TIME),
                    static::getSubjectRow('fgs.sujet_prestation_other', self::PRESTATION_OTHER),
                ),
            ),
            array(
                'text' => 'fgs.sujet_facture',
                'value' => self::FACTURE,
                'childs' => array(),
            ),
            array(
                'text' => 'fgs.sujet_livraison',
                'value' => self::LIVRAISON,
                'childs' => array(
                    static::getSubjectRow('fgs.sujet_livraison_protection', self::LIVRAISON_PROTECTION),
                    static::getSubjectRow('fgs.sujet_livraison_late', self::LIVRAISON_LATE),
                    static::getSubjectRow('fgs.sujet_livraison_other', self::LIVRAISON_OTHER),
                ),
            ),
            array(
                'text' => 'fgs.sujet_technique',
                'value' => self::TECHNIQUE,
                'childs' => array(
                    static::getSubjectRow('fgs.sujet_technique_connect', self::TECHNIQUE_CONNECT),
                    static::getSubjectRow('fgs.sujet_technique_order', self::TECHNIQUE_ORDER),
                    static::getSubjectRow('fgs.sujet_technique_other', self::TECHNIQUE_OTHER),
                ),
            ),
            array(
                'text' => 'fgs.sujet_service_client',
                'value' => self::SERVICE_CLIENT,
                'childs' => array(),
            ),
            array(
                'text' => 'fgs.sujet_autre',
                'value' => self::OTHER,
                'childs' => array(),
            ),
        );
    }

    /**
     * @param string $text
     * @param int    $value
     *
     * @return array
     */
    private static function getSubjectRow(string $text, int $value): array
    {
        return array(
            'text' => $text,
            'value' => $value,
        );
    }
}
