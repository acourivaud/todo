<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 02/08/17
 * Time: 08:51
 */

namespace Todotoday\FGSBundle\Services;

use Actiane\ApiConnectorBundle\Services\Connections\Microsoft\MicrosoftDynamicsConnection;
use Symfony\Component\Translation\TranslatorInterface;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Exceptions\AdherentCRMIdMissingException;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Exceptions\AgencyMissingCrmServiceClientMissingException;
use Todotoday\CoreBundle\Exceptions\AgencyMissingCrmTeamMissingException;
use Todotoday\FGSBundle\Entity\Api\MicrosoftCRM\FicheGrainDeSable;
use Todotoday\FGSBundle\Entity\Api\MicrosoftCRM\LigneDeCommandeCRM;
use Todotoday\FGSBundle\Entity\Api\MicrosoftCRM\SalesOrderCRM;
use Todotoday\FGSBundle\Enum\FGSSubjectEnum;
use Todotoday\FGSBundle\Exceptions\LigneDeCommandeNotFoundException;
use Todotoday\FGSBundle\Exceptions\SalesOrderNotFoundException;
use Todotoday\FGSBundle\Repository\Api\MicrosoftCRM\FicheGraindDeSableRepository;
use Todotoday\FGSBundle\Repository\Api\MicrosoftCRM\LigneDeCommandeCRMRepository;
use Todotoday\FGSBundle\Repository\Api\MicrosoftCRM\SalesOrderCRMRepository;

/**
 * Class FgsManager
 *
 * @package Todotoday\FGSBundle\Services
 */
class FgsManager
{
    /**
     * @var MicrosoftDynamicsConnection
     */
    private $connection;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * FgsManager constructor.
     *
     * @param MicrosoftDynamicsConnection $connection
     * @param TranslatorInterface         $translator
     */
    public function __construct(MicrosoftDynamicsConnection $connection, TranslatorInterface $translator)
    {
        $this->connection = $connection;
        $this->translator = $translator;
    }

    /**
     * @param Adherent    $adherent
     * @param null|string $locale
     *
     * @return FicheGrainDeSable[]|null
     * @throws AdherentCRMIdMissingException
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     */
    public function getAllFgsByAdherent(Adherent $adherent, ?string $locale): ?array
    {
        if (!$adherent->getCrmIdAdherent()) {
            throw new AdherentCRMIdMissingException();
        }

        /** @var FicheGraindDeSableRepository $repo */
        $repo = $this->connection->getRepository(FicheGrainDeSable::class);
        $allFGS = $repo->getAllByAdherent($adherent);
        $this->setAllStatusCodeString($allFGS, $locale);

        return $allFGS;
    }

    /**
     * @param Agency      $agency
     * @param Adherent    $adherent
     * @param int         $sujet1
     * @param int|null    $sujet2
     * @param string      $observation
     * @param null|string $orderRefAx
     * @param null|string $inventoryLotId
     *
     * @return FicheGrainDeSable
     * @throws AdherentCRMIdMissingException
     * @throws AgencyMissingCrmServiceClientMissingException
     * @throws AgencyMissingCrmTeamMissingException
     * @internal param int $sujet
     */
    public function createFgs(
        Agency $agency,
        Adherent $adherent,
        int $sujet1,
        ?int $sujet2,
        string $observation,
        ?string $orderRefAx = null,
        ?string $inventoryLotId = null
    ): FicheGrainDeSable {
        if (!$adherent->getCrmIdAdherent()) {
            throw new AdherentCRMIdMissingException();
        }

        if (!$agency->getCrmTeam()) {
            throw new AgencyMissingCrmTeamMissingException();
        }

        if (!$agency->getCrmServiceClient()) {
            throw new AgencyMissingCrmServiceClientMissingException();
        }

        $fgs = (new FicheGrainDeSable())
            ->setOwnerIdValueGuid($agency->getCrmServiceClient(), 'teams')
            ->setApsOrigine(100000001)
            ->setApsDescriptiondemande($observation)
            ->setApsSujetmarketingfgs($sujet1)
            ->setApsSujetmarketing2($sujet2)
            ->setApsAdhrentValueGuid($adherent->getCrmIdAdherent())
            ->setApsCommandeid($orderRefAx)
            ->setApsLignedecommandeassociee($inventoryLotId);

        // si microsoft nous demande d'utiliser les champs recherche du CRM (guid) avec identifiant interne
        // pour cela leur synchro AX ==> CRM doit refonctionner

//        if ($orderRefAx) {
//            $salesOrder = $this->findSalesOrderByRefOrderAx($orderRefAx);
//            $fgs->setApsCommandeAdherentValueGuid($salesOrder->getSalesorderid());
//        }
//
//        if ($invetoryLotId) {
//            $commandLine = $this->findLigneCommandeByInventoryLot($invetoryLotId);
//            $fgs->setApsLignedecommandeValueGuid($commandLine->getApsLignedecommandeid());
//        }

        $this->connection->persist($fgs)->flush();

        return $fgs;
    }

    /**
     * @param string $orderRefAx
     *
     * @return SalesOrderCRM
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     * @throws SalesOrderNotFoundException
     */
    public function findSalesOrderByRefOrderAx(string $orderRefAx): SalesOrderCRM
    {
        /** @var SalesOrderCRMRepository $repo */
        $repo = $this->connection->getRepository(SalesOrderCRM::class);
        $result = $repo->findOrderCrmByRefAx($orderRefAx);
        if (!$result) {
            throw new SalesOrderNotFoundException($orderRefAx);
        }

        return $result;
    }

    /**
     * @param string $inventoryLotId
     *
     * @return LigneDeCommandeCRM
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     * @throws LigneDeCommandeNotFoundException
     */
    public function findLigneCommandeByInventoryLot(string $inventoryLotId): LigneDeCommandeCRM
    {
        /** @var LigneDeCommandeCRMRepository $repo */
        $repo = $this->connection->getRepository(LigneDeCommandeCRM::class);
        $result = $repo->findLigneByInventoryLot($inventoryLotId);
        if (!$result) {
            throw new LigneDeCommandeNotFoundException($inventoryLotId);
        }

        return $result;
    }

    /**
     * @param null|string $locale
     *
     * @return array
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     */
    public function getFGSSubjectHierarchyTranslate(?string $locale): array
    {
        $allHierarchy = FGSSubjectEnum::getAllHierarchy();

        foreach ($allHierarchy as $key => $mainSubject) {
            $allHierarchy[$key]['text'] = $this->translator->trans($mainSubject['text'], [], 'todotoday', $locale);

            foreach ($mainSubject['childs'] as $keyChild => $childSubject) {
                $allHierarchy[$key]['childs'][$keyChild]['text'] = $this->translator->trans(
                    $childSubject['text'],
                    [],
                    'todotoday',
                    $locale
                );
            }
        }

        return $allHierarchy;
    }

    /**
     * @param array|null  $allFGS
     * @param null|string $locale
     *
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     */
    public function setAllStatusCodeString(?array $allFGS, ?string $locale): void
    {
        if (!$allFGS) {
            return;
        }

        $status = array(
            1 => 'fgs.status.waiting_concierge_validation',
            1000000 => 'fgs.status.in_progress',
            1000001 => 'fgs.status.waiting',
            2 => 'fgs.status.closed',
        );

        /** @var FicheGrainDeSable $fgs */
        foreach ($allFGS as $fgs) {
            $fgs->setStatuscodeString(
                $this->translator->trans(
                    $status[$fgs->getStatuscode()],
                    [],
                    'todotoday',
                    $locale
                )
            );
        }
    }
}
