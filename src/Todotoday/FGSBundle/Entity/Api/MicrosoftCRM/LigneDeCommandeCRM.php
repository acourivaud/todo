<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 01/08/17
 * Time: 17:32
 */

namespace Todotoday\FGSBundle\Entity\Api\MicrosoftCRM;

use Actiane\ApiConnectorBundle\Lib\EntityProxyTrait;
use Actiane\ApiConnectorBundle\Lib\Mapping as APIConnector;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class LigneDeCommandeCRM
 * @package Todotoday\FGSBundle\Entity\Api\MicrosoftCRM
 * @APIConnector\Entity(
 *     path="aps_lignedecommandes",
 *     repositoryId="todotoday.fgs.repository.api.ligne_commande"
 * )
 */
class LigneDeCommandeCRM
{
    use EntityProxyTrait;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_lignedecommandeid", type="string", guid="id")
     * @APIConnector\Id()
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsLignedecommandeid;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_idcommandline", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsIdcommandline;

    /**
     * @var int
     *
     * @APIConnector\Column(name="statuscode", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $statuscode;

    /**
     * @var bool
     *
     * @APIConnector\Column(name="aps_receptionhorsdelai", type="bool")
     * @Serializer\Expose()
     * @Serializer\Type("bool")
     */
    protected $apsReceptionhorsdelai;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_description", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsDescription;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="createdon", type="datetimez")
     * @Serializer\Expose()
     * @Serializer\Type("DateTime")
     */
    protected $createdon;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_produit", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsProduit;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_quantity", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $apsQuantity;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_satisfactionlvl", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $apsSatisfactionlvl;

    /**
     * @var float
     *
     * @APIConnector\Column(name="aps_totalfinal", type="float")
     * @Serializer\Expose()
     * @Serializer\Type("float")
     */
    protected $apsTotalfinal;

    /**
     * @var float
     *
     * @APIConnector\Column(name="aps_prixunitaire", type="float")
     * @Serializer\Expose()
     * @Serializer\Type("float")
     */
    protected $apsPrixunitaire;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_satisfaction", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $apsSatisfaction;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_service", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $apsService;

    /**
     * @return string
     */
    public function getApsLignedecommandeid(): ?string
    {
        return $this->apsLignedecommandeid;
    }

    /**
     * @param string $apsLignedecommandeid
     *
     * @return LigneDeCommandeCRM
     */
    public function setApsLignedecommandeid(?string $apsLignedecommandeid): LigneDeCommandeCRM
    {
        return $this->setTrigger('apsLignedecommandeid', $apsLignedecommandeid);
    }

    /**
     * @return string
     */
    public function getApsIdcommandline(): ?string
    {
        return $this->apsIdcommandline;
    }

    /**
     * @param string $apsIdcommandline
     *
     * @return LigneDeCommandeCRM
     */
    public function setApsIdcommandline(?string $apsIdcommandline): LigneDeCommandeCRM
    {
        return $this->setTrigger('apsIdcommandline', $apsIdcommandline);
    }

    /**
     * @return int
     */
    public function getStatuscode(): ?int
    {
        return $this->statuscode;
    }

    /**
     * @param int $statuscode
     *
     * @return LigneDeCommandeCRM
     */
    public function setStatuscode(?int $statuscode): LigneDeCommandeCRM
    {
        return $this->setTrigger('statuscode', $statuscode);
    }

    /**
     * @return bool
     */
    public function isApsReceptionhorsdelai(): ?bool
    {
        return $this->apsReceptionhorsdelai;
    }

    /**
     * @param bool $apsReceptionhorsdelai
     *
     * @return LigneDeCommandeCRM
     */
    public function setApsReceptionhorsdelai(?bool $apsReceptionhorsdelai): LigneDeCommandeCRM
    {
        return $this->setTrigger('apsReceptionhorsdelai', $apsReceptionhorsdelai);
    }

    /**
     * @return string
     */
    public function getApsDescription(): ?string
    {
        return $this->apsDescription;
    }

    /**
     * @param string $apsDescription
     *
     * @return LigneDeCommandeCRM
     */
    public function setApsDescription(?string $apsDescription): LigneDeCommandeCRM
    {
        return $this->setTrigger('apsDescription', $apsDescription);
    }

    /**
     * @return \DateTime
     */
    public function getCreatedon(): ?\DateTime
    {
        return $this->createdon;
    }

    /**
     * @param \DateTime $createdon
     *
     * @return LigneDeCommandeCRM
     */
    public function setCreatedon(?\DateTime $createdon): LigneDeCommandeCRM
    {
        return $this->setTrigger('createdon', $createdon);
    }

    /**
     * @return string
     */
    public function getApsProduit(): ?string
    {
        return $this->apsProduit;
    }

    /**
     * @param string $apsProduit
     *
     * @return LigneDeCommandeCRM
     */
    public function setApsProduit(?string $apsProduit): LigneDeCommandeCRM
    {
        return $this->setTrigger('apsProduit', $apsProduit);
    }

    /**
     * @return int
     */
    public function getApsQuantity(): ?int
    {
        return $this->apsQuantity;
    }

    /**
     * @param int $apsQuantity
     *
     * @return LigneDeCommandeCRM
     */
    public function setApsQuantity(?int $apsQuantity): LigneDeCommandeCRM
    {
        return $this->setTrigger('apsQuantity', $apsQuantity);
    }

    /**
     * @return int
     */
    public function getApsSatisfactionlvl(): ?int
    {
        return $this->apsSatisfactionlvl;
    }

    /**
     * @param int $apsSatisfactionlvl
     *
     * @return LigneDeCommandeCRM
     */
    public function setApsSatisfactionlvl(?int $apsSatisfactionlvl): LigneDeCommandeCRM
    {
        return $this->setTrigger('apsSatisfactionlvl', $apsSatisfactionlvl);
    }

    /**
     * @return float
     */
    public function getApsTotalfinal(): ?float
    {
        return $this->apsTotalfinal;
    }

    /**
     * @param float $apsTotalfinal
     *
     * @return LigneDeCommandeCRM
     */
    public function setApsTotalfinal(?float $apsTotalfinal): LigneDeCommandeCRM
    {
        return $this->setTrigger('apsTotalfinal', $apsTotalfinal);
    }

    /**
     * @return float
     */
    public function getApsPrixunitaire(): ?float
    {
        return $this->apsPrixunitaire;
    }

    /**
     * @param float $apsPrixunitaire
     *
     * @return LigneDeCommandeCRM
     */
    public function setApsPrixunitaire(?float $apsPrixunitaire): LigneDeCommandeCRM
    {
        return $this->setTrigger('apsPrixunitaire', $apsPrixunitaire);
    }

    /**
     * @return int
     */
    public function getApsSatisfaction(): ?int
    {
        return $this->apsSatisfaction;
    }

    /**
     * @param int $apsSatisfaction
     *
     * @return LigneDeCommandeCRM
     */
    public function setApsSatisfaction(?int $apsSatisfaction): LigneDeCommandeCRM
    {
        return $this->setTrigger('apsSatisfaction', $apsSatisfaction);
    }

    /**
     * @return int
     */
    public function getApsService(): ?int
    {
        return $this->apsService;
    }

    /**
     * @param int $apsService
     *
     * @return LigneDeCommandeCRM
     */
    public function setApsService(?int $apsService): LigneDeCommandeCRM
    {
        return $this->setTrigger('apsService', $apsService);
    }
}
