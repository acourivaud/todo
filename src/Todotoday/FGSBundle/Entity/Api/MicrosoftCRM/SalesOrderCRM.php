<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 01/08/17
 * Time: 17:08
 */

namespace Todotoday\FGSBundle\Entity\Api\MicrosoftCRM;

use Actiane\ApiConnectorBundle\Lib\EntityProxyTrait;
use Actiane\ApiConnectorBundle\Lib\Mapping as APIConnector;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class SalesOrderCRM
 * @package Todotoday\FGSBundle\Entity\Api\MicrosoftCRM
 * @Serializer\ExclusionPolicy("all")
 * @APIConnector\Entity(
 *     path="salesorders",
 *     repositoryId="todotoday.fgs.repository.api.salesorder"
 * )
 */
class SalesOrderCRM
{
    use EntityProxyTrait;

    /**
     * @var int
     *
     * @APIConnector\Column(name="statecode", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $statecode;

    /**
     * @var int
     *
     * @APIConnector\Column(name="statuscode", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $statuscode;

    /**
     * @var int
     *
     * @APIConnector\Column(name="totalamount", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $totalamount;

    /**
     * @var string
     *
     * @APIConnector\Column(name="salesorderid", type="string", guid="id")
     * @Serializer\Expose()
     * @APIConnector\Id()
     * @Serializer\Type("string")
     */
    protected $salesorderid;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="createdon", type="datetimez")
     * @Serializer\Expose()
     * @Serializer\Type("DateTime")
     */
    protected $createdon;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ordernumber", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $ordernumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="name", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $name;

    /**
     * @return int
     */
    public function getStatecode(): ?int
    {
        return $this->statecode;
    }

    /**
     * @param int $statecode
     *
     * @return SalesOrderCRM
     */
    public function setStatecode(?int $statecode): SalesOrderCRM
    {
        return $this->setTrigger('statecode', $statecode);
    }

    /**
     * @return int
     */
    public function getStatuscode(): ?int
    {
        return $this->statuscode;
    }

    /**
     * @param int $statuscode
     *
     * @return SalesOrderCRM
     */
    public function setStatuscode(?int $statuscode): SalesOrderCRM
    {
        return $this->setTrigger('statuscode', $statuscode);
    }

    /**
     * @return int
     */
    public function getTotalamount(): ?int
    {
        return $this->totalamount;
    }

    /**
     * @param int $totalamount
     *
     * @return SalesOrderCRM
     */
    public function setTotalamount(?int $totalamount): SalesOrderCRM
    {
        return $this->setTrigger('totalamount', $totalamount);
    }

    /**
     * @return string
     */
    public function getSalesorderid(): ?string
    {
        return $this->salesorderid;
    }

    /**
     * @param string $salesorderid
     *
     * @return SalesOrderCRM
     */
    public function setSalesorderid(?string $salesorderid): SalesOrderCRM
    {
        return $this->setTrigger('salesorderid', $salesorderid);
    }

    /**
     * @return \DateTime
     */
    public function getCreatedon(): ?\DateTime
    {
        return $this->createdon;
    }

    /**
     * @param \DateTime $createdon
     *
     * @return SalesOrderCRM
     */
    public function setCreatedon(?\DateTime $createdon): SalesOrderCRM
    {
        return $this->setTrigger('createdon', $createdon);
    }

    /**
     * @return string
     */
    public function getOrdernumber(): ?string
    {
        return $this->ordernumber;
    }

    /**
     * @param string $ordernumber
     *
     * @return SalesOrderCRM
     */
    public function setOrdernumber(?string $ordernumber): SalesOrderCRM
    {
        return $this->setTrigger('ordernumber', $ordernumber);
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return SalesOrderCRM
     */
    public function setName(?string $name): SalesOrderCRM
    {
        return $this->setTrigger('name', $name);
    }
}
