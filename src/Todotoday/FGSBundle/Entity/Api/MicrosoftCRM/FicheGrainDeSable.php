<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 01/08/17
 * Time: 16:30
 */

namespace Todotoday\FGSBundle\Entity\Api\MicrosoftCRM;

use Actiane\ApiConnectorBundle\Lib\EntityProxyTrait;
use Actiane\ApiConnectorBundle\Lib\Mapping as APIConnector;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class FicheGrainDeSable
 * @SuppressWarnings(PHPMD)
 *
 * @package Todotoday\FGSBundle\Entity\Api\MicrosoftCRM
 * @Serializer\ExclusionPolicy("all")
 * @APIConnector\Entity(
 *      path="aps_fichegraindesables",
 *      repositoryId="todotoday.fgs.repository.api.fgs"
 * )
 */
class FicheGrainDeSable
{
    use EntityProxyTrait;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_commentaireconcierge", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsCommentaireconcierge;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_descriptiondemande", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsDescriptiondemande;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_sujetmarketingfgs", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $apsSujetmarketingfgs;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_sujetmarketing2", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $apsSujetmarketing2;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_transactioncurrencyid_value", type="string", guid="transactioncurrencyid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $transactioncurrencyidValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_owningbusinessunit_value", type="string", guid="businessunitid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $owningbusinessunitValue;

    /**
     * @var int
     *
     * @APIConnector\Column(name="statuscode", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $statuscode;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="aps_datedequalification", type="datetimez")
     * @Serializer\Expose()
     * @Serializer\Type("DateTime")
     */
    protected $apsDatedequalification;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="createdon", type="datetimez")
     * @Serializer\Expose()
     * @Serializer\Type("DateTime")
     */
    protected $createdon;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_dayofweek", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $apsDayofweek;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_sujet", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $apsSujet;

    /**
     * @var bool
     *
     * @APIConnector\Column(name="aps_copiebooleenmois", type="bool")
     * @Serializer\Expose()
     * @Serializer\Type("boolean")
     */
    protected $apsCopiebooleenmois;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_poidsdelafiche", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $apsPoidsdelafiche;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_ownerid_value", type="string", guid="ownerid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $owneridValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_name", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsName;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="modifiedon", type="datetimez")
     * @Serializer\Expose()
     * @Serializer\Type("DateTime")
     */
    protected $modifiedon;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="aps_datedeclotureprevue", type="datetimez")
     * @Serializer\Expose()
     * @Serializer\Type("DateTime")
     */
    protected $apsDatedeclotureprevue;

    /**
     * @var int
     *
     * @APIConnector\Column(name="statecode", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $statecode;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_diffrencemoisactuelmoisdecreation", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $apsDiffrencemoisactuelmoisdecreation;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_dodgydiv", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $apsDodgydiv;

    /**
     * @var int
     *
     * @APIConnector\Column(name="versionnumber", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $versionnumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_aps_fournisseur_value", type="string", guid="contactid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsFournisseurValue;

    /**
     * @var int
     *
     * @APIConnector\Column(name="exchangerate", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $exchangerate;

    /**
     * @var int
     *
     * @APIConnector\Column(name="timezoneruleversionnumber", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $timezoneruleversionnumber;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="aps_datereference", type="datetimez")
     * @Serializer\Expose()
     * @Serializer\Type("DateTime")
     */
    protected $apsDatereference;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_aps_fournisseurfgs_value", type="string", guid="aps_fournisseurid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsFournisseurfgsValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_modifiedby_value", type="string", guid="systemuserid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $modifiedbyValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_testid", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsTestid;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_aps_adherent_value", type="string", guid="aps_adherent")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsAdherentValue;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_montantrembourser_base", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $apsMontantrembourserBase;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_aps_lignedecommande_value", type="string", guid="aps_Lignedecommande")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $apsLignedecommandeValue;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_dotwnumber", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $apsDotwnumber;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_montantrembourser", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $apsMontantrembourser;

    /**
     * @var bool
     *
     * @APIConnector\Column(name="aps_blocagedate", type="bool")
     * @Serializer\Expose()
     * @Serializer\Type("boolean")
     */
    protected $apsBlocagedate;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="aps_copiedateprvue", type="datetimez")
     * @Serializer\Expose()
     * @Serializer\Type("DateTime")
     */
    protected $apsCopiedateprvue;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_origine", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $apsOrigine;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_service", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $apsService;

    /**
     * @var bool
     *
     * @APIConnector\Column(name="aps_creecemoici", type="bool")
     * @Serializer\Expose()
     * @Serializer\Type("boolean")
     */
    protected $apsCreecemoici;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_createdby_value", type="string", guid="systemuserid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $createdbyValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_idfgs", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsIdfgs;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="aps_datedujour", type="datetimez")
     * @Serializer\Expose()
     * @Serializer\Type("DateTime")
     */
    protected $apsDatedujour;

    /**
     * @var bool
     *
     * @APIConnector\Column(name="aps_resolutionhorsdelai", type="bool")
     * @Serializer\Expose()
     * @Serializer\Type("boolean")
     */
    protected $apsResolutionhorsdelai;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_fichegraindesableid", type="string", guid="id")
     * @APIConnector\Id()
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsFichegraindesableid;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_type", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $apsType;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_owninguser_value", type="string", guid="systemuserid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $owninguserValue;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_responsableincident", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $apsResponsableincident;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_priorite", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $apsPriorite;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_serviceldc", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $apsServiceldc;

    /**
     * @var float
     *
     * @APIConnector\Column(name="aps_montantfournisseurttc_base", type="float")
     * @Serializer\Expose()
     * @Serializer\Type("float")
     */
    protected $apsMontantfournisseurttcBase;

    /**
     * @var int
     *
     * @APIConnector\Column(name="importsequencenumber", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $importsequencenumber;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="aps_datedecloture", type="datetimez")
     * @Serializer\Expose()
     * @Serializer\Type("DateTime")
     */
    protected $apsDatedecloture;

    /**
     * @var int
     *
     * @APIConnector\Column(name="utcconversiontimezonecode", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $utcconversiontimezonecode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_ndavoir", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsNdavoir;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_traitementdemandedeux", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $apsTraitementdemandedeux;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_actionralise", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $apsActionralise;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_createdonbehalfby_value", type="string", guid="systemuserid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $createdonbehalfbyValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_modifiedonbehalfby_value", type="string", guid="systemuserid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $modifiedonbehalfbyValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_aps_conciergerie_value", type="string", guid="teamid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsConciergerieValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_commentaires", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsCommentaires;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_titre", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsTitre;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_aps_commandeadherent_value", type="string", guid="aps_commandeadherent")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsCommandeadherentValue;

    /**
     * @var float
     *
     * @APIConnector\Column(name="aps_montantavoirttc_base", type="float")
     * @Serializer\Expose()
     * @Serializer\Type("float")
     */
    protected $apsMontantavoirttcBase;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_aps_contact_value", type="string", guid="contactid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsContactValue;

    /**
     * @var float
     *
     * @APIConnector\Column(name="aps_montantfournisseurttc", type="float")
     * @Serializer\Expose()
     * @Serializer\Type("float")
     */
    protected $apsMontantfournisseurttc;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="overriddencreatedon", type="datetimez")
     * @Serializer\Expose()
     * @Serializer\Type("DateTime")
     */
    protected $overriddencreatedon;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_motif", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsMotif;

    /**
     * @var float
     *
     * @APIConnector\Column(name="aps_montantavoirttc", type="float")
     * @Serializer\Expose()
     * @Serializer\Type("float")
     */
    protected $apsMontantavoirttc;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_motifreclamation", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $apsMotifreclamation;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_owningteam_value", type="string", guid="teamid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $owningteamValue;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_traitementdemande", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $apsTraitementdemande;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_commandeid", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsCommandeid;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_lignedecommandeassociee", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsLignedecommandeassociee;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $statuscodeString;

    /**
     * @return string
     */
    public function getApsCommentaireconcierge(): ?string
    {
        return $this->apsCommentaireconcierge;
    }

    /**
     * @param string $apsCommentaireconcierge
     *
     * @return FicheGrainDeSable
     */
    public function setApsCommentaireconcierge(?string $apsCommentaireconcierge): FicheGrainDeSable
    {
        return $this->setTrigger('apsCommentaireconcierge', $apsCommentaireconcierge);
    }

    /**
     * @return string
     */
    public function getApsDescriptiondemande(): ?string
    {
        return $this->apsDescriptiondemande;
    }

    /**
     * @param string $apsDescriptiondemande
     *
     * @return FicheGrainDeSable
     */
    public function setApsDescriptiondemande(?string $apsDescriptiondemande): FicheGrainDeSable
    {
        return $this->setTrigger('apsDescriptiondemande', $apsDescriptiondemande);
    }

    /**
     * @return int
     */
    public function getApsSujetmarketingfgs(): ?int
    {
        return $this->apsSujetmarketingfgs;
    }

    /**
     * @param int $apsSujetmarketingfgs
     *
     * @return FicheGrainDeSable
     */
    public function setApsSujetmarketingfgs(?int $apsSujetmarketingfgs): FicheGrainDeSable
    {
        return $this->setTrigger('apsSujetmarketingfgs', $apsSujetmarketingfgs);
    }

    /**
     * @return string
     */
    public function getTransactioncurrencyidValue(): ?string
    {
        return $this->transactioncurrencyidValue;
    }

    /**
     * @param string $transactioncurrencyidValue
     *
     * @return FicheGrainDeSable
     */
    public function setTransactioncurrencyidValue(?string $transactioncurrencyidValue): FicheGrainDeSable
    {
        return $this->setTrigger('transactioncurrencyidValue', $transactioncurrencyidValue);
    }

    /**
     * @return string
     */
    public function getOwningbusinessunitValue(): ?string
    {
        return $this->owningbusinessunitValue;
    }

    /**
     * @param string $owningbusinessunitValue
     *
     * @return FicheGrainDeSable
     */
    public function setOwningbusinessunitValue(?string $owningbusinessunitValue): FicheGrainDeSable
    {
        return $this->setTrigger('owningbusinessunitValue', $owningbusinessunitValue);
    }

    /**
     * @return int
     */
    public function getStatuscode(): ?int
    {
        return $this->statuscode;
    }

    /**
     * @param int $statuscode
     *
     * @return FicheGrainDeSable
     */
    public function setStatuscode(?int $statuscode): FicheGrainDeSable
    {
        return $this->setTrigger('statuscode', $statuscode);
    }

    /**
     * @return \DateTime
     */
    public function getApsDatedequalification(): ?\DateTime
    {
        return $this->apsDatedequalification;
    }

    /**
     * @param \DateTime $apsDatedequalification
     *
     * @return FicheGrainDeSable
     */
    public function setApsDatedequalification(?\DateTime $apsDatedequalification): FicheGrainDeSable
    {
        return $this->setTrigger('apsDatedequalification', $apsDatedequalification);
    }

    /**
     * @return \DateTime
     */
    public function getCreatedon(): ?\DateTime
    {
        return $this->createdon;
    }

    /**
     * @param \DateTime $createdon
     *
     * @return FicheGrainDeSable
     */
    public function setCreatedon(?\DateTime $createdon): FicheGrainDeSable
    {
        return $this->setTrigger('createdon', $createdon);
    }

    /**
     * @return int
     */
    public function getApsDayofweek(): ?int
    {
        return $this->apsDayofweek;
    }

    /**
     * @param int $apsDayofweek
     *
     * @return FicheGrainDeSable
     */
    public function setApsDayofweek(?int $apsDayofweek): FicheGrainDeSable
    {
        return $this->setTrigger('apsDayofweek', $apsDayofweek);
    }

    /**
     * @return int
     */
    public function getApsSujet(): ?int
    {
        return $this->apsSujet;
    }

    /**
     * @param int $apsSujet
     *
     * @return FicheGrainDeSable
     */
    public function setApsSujet(?int $apsSujet): FicheGrainDeSable
    {
        return $this->setTrigger('apsSujet', $apsSujet);
    }

    /**
     * @return bool
     */
    public function isApsCopiebooleenmois(): ?bool
    {
        return $this->apsCopiebooleenmois;
    }

    /**
     * @param bool $apsCopiebooleenmois
     *
     * @return FicheGrainDeSable
     */
    public function setApsCopiebooleenmois(?bool $apsCopiebooleenmois): FicheGrainDeSable
    {
        return $this->setTrigger('apsCopiebooleenmois', $apsCopiebooleenmois);
    }

    /**
     * @return int
     */
    public function getApsPoidsdelafiche(): ?int
    {
        return $this->apsPoidsdelafiche;
    }

    /**
     * @param int $apsPoidsdelafiche
     *
     * @return FicheGrainDeSable
     */
    public function setApsPoidsdelafiche(?int $apsPoidsdelafiche): FicheGrainDeSable
    {
        return $this->setTrigger('apsPoidsdelafiche', $apsPoidsdelafiche);
    }

    /**
     * @return string|array
     */
    public function getOwneridValue()
    {
        return $this->owneridValue;
    }

    /**
     * @param string $owneridValue
     *
     * @return FicheGrainDeSable
     */
    public function setOwneridValue(?string $owneridValue): FicheGrainDeSable
    {
        return $this->setTrigger('owneridValue', $owneridValue);
    }

    /**
     * Method to call when persisting new value
     * Need to target which entity is bound
     *
     * @param string $owneridValue
     * @param string $target
     *
     * @return FicheGrainDeSable
     */
    public function setOwnerIdValueGuid(string $owneridValue, string $target): self
    {
        return $this->setTriggerGuid(
            'owneridValue',
            array(
                'target' => $target,
                'value' => $owneridValue,
            )
        );
    }

    /**
     * @return string
     */
    public function getApsName(): ?string
    {
        return $this->apsName;
    }

    /**
     * @param string $apsName
     *
     * @return FicheGrainDeSable
     */
    public function setApsName(?string $apsName): FicheGrainDeSable
    {
        return $this->setTrigger('apsName', $apsName);
    }

    /**
     * @return \DateTime
     */
    public function getModifiedon(): ?\DateTime
    {
        return $this->modifiedon;
    }

    /**
     * @param \DateTime $modifiedon
     *
     * @return FicheGrainDeSable
     */
    public function setModifiedon(?\DateTime $modifiedon): FicheGrainDeSable
    {
        return $this->setTrigger('modifiedon', $modifiedon);
    }

    /**
     * @return \DateTime
     */
    public function getApsDatedeclotureprevue(): ?\DateTime
    {
        return $this->apsDatedeclotureprevue;
    }

    /**
     * @param \DateTime $apsDatedeclotureprevue
     *
     * @return FicheGrainDeSable
     */
    public function setApsDatedeclotureprevue(?\DateTime $apsDatedeclotureprevue): FicheGrainDeSable
    {
        return $this->setTrigger('apsDatedeclotureprevue', $apsDatedeclotureprevue);
    }

    /**
     * @return int
     */
    public function getStatecode(): ?int
    {
        return $this->statecode;
    }

    /**
     * @param int $statecode
     *
     * @return FicheGrainDeSable
     */
    public function setStatecode(?int $statecode): FicheGrainDeSable
    {
        return $this->setTrigger('statecode', $statecode);
    }

    /**
     * @return int
     */
    public function getApsDiffrencemoisactuelmoisdecreation(): ?int
    {
        return $this->apsDiffrencemoisactuelmoisdecreation;
    }

    /**
     * @param int $apsDiffrencemoisactuelmoisdecreation
     *
     * @return FicheGrainDeSable
     */
    public function setApsDiffrencemoisactuelmoisdecreation(
        ?int $apsDiffrencemoisactuelmoisdecreation
    ): FicheGrainDeSable {
        return $this->setTrigger('apsDiffrencemoisactuelmoisdecreation', $apsDiffrencemoisactuelmoisdecreation);
    }

    /**
     * @return int
     */
    public function getApsDodgydiv(): ?int
    {
        return $this->apsDodgydiv;
    }

    /**
     * @param int $apsDodgydiv
     *
     * @return FicheGrainDeSable
     */
    public function setApsDodgydiv(?int $apsDodgydiv): FicheGrainDeSable
    {
        return $this->setTrigger('apsDodgydiv', $apsDodgydiv);
    }

    /**
     * @return int
     */
    public function getVersionnumber(): ?int
    {
        return $this->versionnumber;
    }

    /**
     * @param int $versionnumber
     *
     * @return FicheGrainDeSable
     */
    public function setVersionnumber(?int $versionnumber): FicheGrainDeSable
    {
        return $this->setTrigger('versionnumber', $versionnumber);
    }

    /**
     * @return string
     */
    public function getApsFournisseurValue(): ?string
    {
        return $this->apsFournisseurValue;
    }

    /**
     * @param string $apsFournisseurValue
     *
     * @return FicheGrainDeSable
     */
    public function setApsFournisseurValue(?string $apsFournisseurValue): FicheGrainDeSable
    {
        return $this->setTrigger('apsFournisseurValue', $apsFournisseurValue);
    }

    /**
     * @return int
     */
    public function getExchangerate(): ?int
    {
        return $this->exchangerate;
    }

    /**
     * @param int $exchangerate
     *
     * @return FicheGrainDeSable
     */
    public function setExchangerate(?int $exchangerate): FicheGrainDeSable
    {
        return $this->setTrigger('exchangerate', $exchangerate);
    }

    /**
     * @return int
     */
    public function getTimezoneruleversionnumber(): ?int
    {
        return $this->timezoneruleversionnumber;
    }

    /**
     * @param int $timezoneruleversionnumber
     *
     * @return FicheGrainDeSable
     */
    public function setTimezoneruleversionnumber(?int $timezoneruleversionnumber): FicheGrainDeSable
    {
        return $this->setTrigger('timezoneruleversionnumber', $timezoneruleversionnumber);
    }

    /**
     * @return \DateTime
     */
    public function getApsDatereference(): ?\DateTime
    {
        return $this->apsDatereference;
    }

    /**
     * @param \DateTime $apsDatereference
     *
     * @return FicheGrainDeSable
     */
    public function setApsDatereference(?\DateTime $apsDatereference): FicheGrainDeSable
    {
        return $this->setTrigger('apsDatereference', $apsDatereference);
    }

    /**
     * @return string
     */
    public function getApsFournisseurfgsValue(): ?string
    {
        return $this->apsFournisseurfgsValue;
    }

    /**
     * @param string $apsFournisseurfgsValue
     *
     * @return FicheGrainDeSable
     */
    public function setApsFournisseurfgsValue(?string $apsFournisseurfgsValue): FicheGrainDeSable
    {
        return $this->setTrigger('apsFournisseurfgsValue', $apsFournisseurfgsValue);
    }

    /**
     * @return string
     */
    public function getModifiedbyValue(): ?string
    {
        return $this->modifiedbyValue;
    }

    /**
     * @param string $modifiedbyValue
     *
     * @return FicheGrainDeSable
     */
    public function setModifiedbyValue(?string $modifiedbyValue): FicheGrainDeSable
    {
        return $this->setTrigger('modifiedbyValue', $modifiedbyValue);
    }

    /**
     * @return string
     */
    public function getApsTestid(): ?string
    {
        return $this->apsTestid;
    }

    /**
     * @param string $apsTestid
     *
     * @return FicheGrainDeSable
     */
    public function setApsTestid(?string $apsTestid): FicheGrainDeSable
    {
        return $this->setTrigger('apsTestid', $apsTestid);
    }

    /**
     * @return string|array
     */
    public function getApsAdherentValue()
    {
        return $this->apsAdherentValue;
    }

    /**
     * @param string $apsAdherentValue
     *
     * @return FicheGrainDeSable
     */
    public function setApsAdherentValue(?string $apsAdherentValue): FicheGrainDeSable
    {
        return $this->setTrigger('apsAdherentValue', $apsAdherentValue);
    }

    /**
     * @param null|string $apsAdhrentValue
     *
     * @return FicheGrainDeSable
     */
    public function setApsAdhrentValueGuid(?string $apsAdhrentValue): self
    {
        return $this->setTriggerGuid(
            'apsAdherentValue',
            array(
                'target' => 'accounts',
                'value' => $apsAdhrentValue,
            )
        );
    }

    /**
     * @return int
     */
    public function getApsMontantrembourserBase(): ?int
    {
        return $this->apsMontantrembourserBase;
    }

    /**
     * @param int $apsMontantrembourserBase
     *
     * @return FicheGrainDeSable
     */
    public function setApsMontantrembourserBase(?int $apsMontantrembourserBase): FicheGrainDeSable
    {
        return $this->setTrigger('apsMontantrembourserBase', $apsMontantrembourserBase);
    }

    /**
     * @return string|array
     */
    public function getApsLignedecommandeValue()
    {
        return $this->apsLignedecommandeValue;
    }

    /**
     * @param string $apsLignedecommandeValue
     *
     * @return FicheGrainDeSable
     */
    public function setApsLignedecommandeValue(?string $apsLignedecommandeValue): FicheGrainDeSable
    {
        return $this->setTrigger('apsLignedecommandeValue', $apsLignedecommandeValue);
    }

    /**
     * @param null|string $apsLignedecommandeValue
     *
     * @return FicheGrainDeSable
     */
    public function setApsLignedecommandeValueGuid(?string $apsLignedecommandeValue): FicheGrainDeSable
    {
        return $this->setTriggerGuid(
            'apsLignedecommandeValue',
            array(
                'target' => 'aps_lignedecommandes',
                'value' => $apsLignedecommandeValue,
            )
        );
    }

    /**
     * @return int
     */
    public function getApsDotwnumber(): ?int
    {
        return $this->apsDotwnumber;
    }

    /**
     * @param int $apsDotwnumber
     *
     * @return FicheGrainDeSable
     */
    public function setApsDotwnumber(?int $apsDotwnumber): FicheGrainDeSable
    {
        return $this->setTrigger('apsDotwnumber', $apsDotwnumber);
    }

    /**
     * @return int
     */
    public function getApsMontantrembourser(): ?int
    {
        return $this->apsMontantrembourser;
    }

    /**
     * @param int $apsMontantrembourser
     *
     * @return FicheGrainDeSable
     */
    public function setApsMontantrembourser(?int $apsMontantrembourser): FicheGrainDeSable
    {
        return $this->setTrigger('apsMontantrembourser', $apsMontantrembourser);
    }

    /**
     * @return bool
     */
    public function isApsBlocagedate(): ?bool
    {
        return $this->apsBlocagedate;
    }

    /**
     * @param bool $apsBlocagedate
     *
     * @return FicheGrainDeSable
     */
    public function setApsBlocagedate(?bool $apsBlocagedate): FicheGrainDeSable
    {
        return $this->setTrigger('apsBlocagedate', $apsBlocagedate);
    }

    /**
     * @return \DateTime
     */
    public function getApsCopiedateprvue(): ?\DateTime
    {
        return $this->apsCopiedateprvue;
    }

    /**
     * @param \DateTime $apsCopiedateprvue
     *
     * @return FicheGrainDeSable
     */
    public function setApsCopiedateprvue(?\DateTime $apsCopiedateprvue): FicheGrainDeSable
    {
        return $this->setTrigger('apsCopiedateprvue', $apsCopiedateprvue);
    }

    /**
     * @return int
     */
    public function getApsOrigine(): ?int
    {
        return $this->apsOrigine;
    }

    /**
     * @param int $apsOrigine
     *
     * @return FicheGrainDeSable
     */
    public function setApsOrigine(?int $apsOrigine): FicheGrainDeSable
    {
        return $this->setTrigger('apsOrigine', $apsOrigine);
    }

    /**
     * @return int
     */
    public function getApsService(): ?int
    {
        return $this->apsService;
    }

    /**
     * @param int $apsService
     *
     * @return FicheGrainDeSable
     */
    public function setApsService(?int $apsService): FicheGrainDeSable
    {
        return $this->setTrigger('apsService', $apsService);
    }

    /**
     * @return bool
     */
    public function isApsCreecemoici(): ?bool
    {
        return $this->apsCreecemoici;
    }

    /**
     * @param bool $apsCreecemoici
     *
     * @return FicheGrainDeSable
     */
    public function setApsCreecemoici(?bool $apsCreecemoici): FicheGrainDeSable
    {
        return $this->setTrigger('apsCreecemoici', $apsCreecemoici);
    }

    /**
     * @return string
     */
    public function getCreatedbyValue(): ?string
    {
        return $this->createdbyValue;
    }

    /**
     * @param string $createdbyValue
     *
     * @return FicheGrainDeSable
     */
    public function setCreatedbyValue(?string $createdbyValue): FicheGrainDeSable
    {
        return $this->setTrigger('createdbyValue', $createdbyValue);
    }

    /**
     * @return string
     */
    public function getApsIdfgs(): ?string
    {
        return $this->apsIdfgs;
    }

    /**
     * @param string $apsIdfgs
     *
     * @return FicheGrainDeSable
     */
    public function setApsIdfgs(?string $apsIdfgs): FicheGrainDeSable
    {
        return $this->setTrigger('apsIdfgs', $apsIdfgs);
    }

    /**
     * @return \DateTime
     */
    public function getApsDatedujour(): ?\DateTime
    {
        return $this->apsDatedujour;
    }

    /**
     * @param \DateTime $apsDatedujour
     *
     * @return FicheGrainDeSable
     */
    public function setApsDatedujour(?\DateTime $apsDatedujour): FicheGrainDeSable
    {
        return $this->setTrigger('apsDatedujour', $apsDatedujour);
    }

    /**
     * @return bool
     */
    public function isApsResolutionhorsdelai(): ?bool
    {
        return $this->apsResolutionhorsdelai;
    }

    /**
     * @param bool $apsResolutionhorsdelai
     *
     * @return FicheGrainDeSable
     */
    public function setApsResolutionhorsdelai(?bool $apsResolutionhorsdelai): FicheGrainDeSable
    {
        return $this->setTrigger('apsResolutionhorsdelai', $apsResolutionhorsdelai);
    }

    /**
     * @return string
     */
    public function getApsFichegraindesableid(): ?string
    {
        return $this->apsFichegraindesableid;
    }

    /**
     * @param string $apsFichegraindesableid
     *
     * @return FicheGrainDeSable
     */
    public function setApsFichegraindesableid(?string $apsFichegraindesableid): FicheGrainDeSable
    {
        return $this->setTrigger('apsFichegraindesableid', $apsFichegraindesableid);
    }

    /**
     * @return int
     */
    public function getApsType(): ?int
    {
        return $this->apsType;
    }

    /**
     * @param int $apsType
     *
     * @return FicheGrainDeSable
     */
    public function setApsType(?int $apsType): FicheGrainDeSable
    {
        return $this->setTrigger('apsType', $apsType);
    }

    /**
     * @return string
     */
    public function getOwninguserValue(): ?string
    {
        return $this->owninguserValue;
    }

    /**
     * @param string $owninguserValue
     *
     * @return FicheGrainDeSable
     */
    public function setOwninguserValue(?string $owninguserValue): FicheGrainDeSable
    {
        return $this->setTrigger('owninguserValue', $owninguserValue);
    }

    /**
     * @return int
     */
    public function getApsResponsableincident(): ?int
    {
        return $this->apsResponsableincident;
    }

    /**
     * @param int $apsResponsableincident
     *
     * @return FicheGrainDeSable
     */
    public function setApsResponsableincident(?int $apsResponsableincident): FicheGrainDeSable
    {
        return $this->setTrigger('apsResponsableincident', $apsResponsableincident);
    }

    /**
     * @return int
     */
    public function getApsPriorite(): ?int
    {
        return $this->apsPriorite;
    }

    /**
     * @param int $apsPriorite
     *
     * @return FicheGrainDeSable
     */
    public function setApsPriorite(?int $apsPriorite): FicheGrainDeSable
    {
        return $this->setTrigger('apsPriorite', $apsPriorite);
    }

    /**
     * @return int
     */
    public function getApsServiceldc(): ?int
    {
        return $this->apsServiceldc;
    }

    /**
     * @param int $apsServiceldc
     *
     * @return FicheGrainDeSable
     */
    public function setApsServiceldc(?int $apsServiceldc): FicheGrainDeSable
    {
        return $this->setTrigger('apsServiceldc', $apsServiceldc);
    }

    /**
     * @return float
     */
    public function getApsMontantfournisseurttcBase(): ?float
    {
        return $this->apsMontantfournisseurttcBase;
    }

    /**
     * @param float $apsMontantfournisseurttcBase
     *
     * @return FicheGrainDeSable
     */
    public function setApsMontantfournisseurttcBase(?float $apsMontantfournisseurttcBase): FicheGrainDeSable
    {
        return $this->setTrigger('apsMontantfournisseurttcBase', $apsMontantfournisseurttcBase);
    }

    /**
     * @return int
     */
    public function getImportsequencenumber(): ?int
    {
        return $this->importsequencenumber;
    }

    /**
     * @param int $importsequencenumber
     *
     * @return FicheGrainDeSable
     */
    public function setImportsequencenumber(?int $importsequencenumber): FicheGrainDeSable
    {
        return $this->setTrigger('importsequencenumber', $importsequencenumber);
    }

    /**
     * @return \DateTime
     */
    public function getApsDatedecloture(): ?\DateTime
    {
        return $this->apsDatedecloture;
    }

    /**
     * @param \DateTime $apsDatedecloture
     *
     * @return FicheGrainDeSable
     */
    public function setApsDatedecloture(?\DateTime $apsDatedecloture): FicheGrainDeSable
    {
        return $this->setTrigger('apsDatedecloture', $apsDatedecloture);
    }

    /**
     * @return int
     */
    public function getUtcconversiontimezonecode(): ?int
    {
        return $this->utcconversiontimezonecode;
    }

    /**
     * @param int $utcconversiontimezonecode
     *
     * @return FicheGrainDeSable
     */
    public function setUtcconversiontimezonecode(?int $utcconversiontimezonecode): FicheGrainDeSable
    {
        return $this->setTrigger('utcconversiontimezonecode', $utcconversiontimezonecode);
    }

    /**
     * @return string
     */
    public function getApsNdavoir(): ?string
    {
        return $this->apsNdavoir;
    }

    /**
     * @param string $apsNdavoir
     *
     * @return FicheGrainDeSable
     */
    public function setApsNdavoir(?string $apsNdavoir): FicheGrainDeSable
    {
        return $this->setTrigger('apsNdavoir', $apsNdavoir);
    }

    /**
     * @return int
     */
    public function getApsTraitementdemandedeux(): ?int
    {
        return $this->apsTraitementdemandedeux;
    }

    /**
     * @param int $apsTraitementdemandedeux
     *
     * @return FicheGrainDeSable
     */
    public function setApsTraitementdemandedeux(?int $apsTraitementdemandedeux): FicheGrainDeSable
    {
        return $this->setTrigger('apsTraitementdemandedeux', $apsTraitementdemandedeux);
    }

    /**
     * @return int
     */
    public function getApsActionralise(): ?int
    {
        return $this->apsActionralise;
    }

    /**
     * @param int $apsActionralise
     *
     * @return FicheGrainDeSable
     */
    public function setApsActionralise(?int $apsActionralise): FicheGrainDeSable
    {
        return $this->setTrigger('apsActionralise', $apsActionralise);
    }

    /**
     * @return string
     */
    public function getCreatedonbehalfbyValue(): ?string
    {
        return $this->createdonbehalfbyValue;
    }

    /**
     * @param string $createdonbehalfbyValue
     *
     * @return FicheGrainDeSable
     */
    public function setCreatedonbehalfbyValue(?string $createdonbehalfbyValue): FicheGrainDeSable
    {
        return $this->setTrigger('createdonbehalfbyValue', $createdonbehalfbyValue);
    }

    /**
     * @return string
     */
    public function getModifiedonbehalfbyValue(): ?string
    {
        return $this->modifiedonbehalfbyValue;
    }

    /**
     * @param string $modifiedonbehalfbyValue
     *
     * @return FicheGrainDeSable
     */
    public function setModifiedonbehalfbyValue(?string $modifiedonbehalfbyValue): FicheGrainDeSable
    {
        return $this->setTrigger('modifiedonbehalfbyValue', $modifiedonbehalfbyValue);
    }

    /**
     * @return string
     */
    public function getApsConciergerieValue(): ?string
    {
        return $this->apsConciergerieValue;
    }

    /**
     * @param string $apsConciergerieValue
     *
     * @return FicheGrainDeSable
     */
    public function setApsConciergerieValue(?string $apsConciergerieValue): FicheGrainDeSable
    {
        return $this->setTrigger('apsConciergerieValue', $apsConciergerieValue);
    }

    /**
     * @return string
     */
    public function getApsCommentaires(): ?string
    {
        return $this->apsCommentaires;
    }

    /**
     * @param string $apsCommentaires
     *
     * @return FicheGrainDeSable
     */
    public function setApsCommentaires(?string $apsCommentaires): FicheGrainDeSable
    {
        return $this->setTrigger('apsCommentaires', $apsCommentaires);
    }

    /**
     * @return string
     */
    public function getApsTitre(): ?string
    {
        return $this->apsTitre;
    }

    /**
     * @param string $apsTitre
     *
     * @return FicheGrainDeSable
     */
    public function setApsTitre(?string $apsTitre): FicheGrainDeSable
    {
        return $this->setTrigger('apsTitre', $apsTitre);
    }

    /**
     * @return string|array
     */
    public function getApsCommandeadherentValue()
    {
        return $this->apsCommandeadherentValue;
    }

    /**
     * @param string $apsCommandeadherentValue
     *
     * @return FicheGrainDeSable
     */
    public function setApsCommandeadherentValue(?string $apsCommandeadherentValue): FicheGrainDeSable
    {
        return $this->setTrigger('apsCommandeadherentValue', $apsCommandeadherentValue);
    }

    /**
     * @param null|string $apsCOmmandeAdherentValue
     *
     * @return FicheGrainDeSable
     */
    public function setApsCommandeAdherentValueGuid(?string $apsCOmmandeAdherentValue): FicheGrainDeSable
    {
        return $this->setTriggerGuid(
            'apsCommandeadherentValue',
            array(
                'target' => 'salesorders',
                'value' => $apsCOmmandeAdherentValue,
            )
        );
    }

    /**
     * @return float
     */
    public function getApsMontantavoirttcBase(): ?float
    {
        return $this->apsMontantavoirttcBase;
    }

    /**
     * @param float $apsMontantavoirttcBase
     *
     * @return FicheGrainDeSable
     */
    public function setApsMontantavoirttcBase(?float $apsMontantavoirttcBase): FicheGrainDeSable
    {
        return $this->setTrigger('apsMontantavoirttcBase', $apsMontantavoirttcBase);
    }

    /**
     * @return string
     */
    public function getApsContactValue(): ?string
    {
        return $this->apsContactValue;
    }

    /**
     * @param string $apsContactValue
     *
     * @return FicheGrainDeSable
     */
    public function setApsContactValue(?string $apsContactValue): FicheGrainDeSable
    {
        return $this->setTrigger('apsContactValue', $apsContactValue);
    }

    /**
     * @return float
     */
    public function getApsMontantfournisseurttc(): ?float
    {
        return $this->apsMontantfournisseurttc;
    }

    /**
     * @param float $apsMontantfournisseurttc
     *
     * @return FicheGrainDeSable
     */
    public function setApsMontantfournisseurttc(?float $apsMontantfournisseurttc): FicheGrainDeSable
    {
        return $this->setTrigger('apsMontantfournisseurttc', $apsMontantfournisseurttc);
    }

    /**
     * @return \DateTime
     */
    public function getOverriddencreatedon(): ?\DateTime
    {
        return $this->overriddencreatedon;
    }

    /**
     * @param \DateTime $overriddencreatedon
     *
     * @return FicheGrainDeSable
     */
    public function setOverriddencreatedon(?\DateTime $overriddencreatedon): FicheGrainDeSable
    {
        return $this->setTrigger('overriddencreatedon', $overriddencreatedon);
    }

    /**
     * @return string
     */
    public function getApsMotif(): ?string
    {
        return $this->apsMotif;
    }

    /**
     * @param string $apsMotif
     *
     * @return FicheGrainDeSable
     */
    public function setApsMotif(?string $apsMotif): FicheGrainDeSable
    {
        return $this->setTrigger('apsMotif', $apsMotif);
    }

    /**
     * @return float
     */
    public function getApsMontantavoirttc(): ?float
    {
        return $this->apsMontantavoirttc;
    }

    /**
     * @param float $apsMontantavoirttc
     *
     * @return FicheGrainDeSable
     */
    public function setApsMontantavoirttc(?float $apsMontantavoirttc): FicheGrainDeSable
    {
        return $this->setTrigger('apsMontantavoirttc', $apsMontantavoirttc);
    }

    /**
     * @return int
     */
    public function getApsMotifreclamation(): ?int
    {
        return $this->apsMotifreclamation;
    }

    /**
     * @param int $apsMotifreclamation
     *
     * @return FicheGrainDeSable
     */
    public function setApsMotifreclamation(?int $apsMotifreclamation): FicheGrainDeSable
    {
        return $this->setTrigger('apsMotifreclamation', $apsMotifreclamation);
    }

    /**
     * @return string
     */
    public function getOwningteamValue(): ?string
    {
        return $this->owningteamValue;
    }

    /**
     * @param string $owningteamValue
     *
     * @return FicheGrainDeSable
     */
    public function setOwningteamValue(?string $owningteamValue): FicheGrainDeSable
    {
        return $this->setTrigger('owningteamValue', $owningteamValue);
    }

    /**
     * @return int
     */
    public function getApsTraitementdemande(): ?int
    {
        return $this->apsTraitementdemande;
    }

    /**
     * @param int $apsTraitementdemande
     *
     * @return FicheGrainDeSable
     */
    public function setApsTraitementdemande(?int $apsTraitementdemande): FicheGrainDeSable
    {
        return $this->setTrigger('apsTraitementdemande', $apsTraitementdemande);
    }

    /**
     * @return string
     */
    public function getApsCommandeid(): ?string
    {
        return $this->apsCommandeid;
    }

    /**
     * @param string $apsCommandeid
     *
     * @return FicheGrainDeSable
     */
    public function setApsCommandeid(?string $apsCommandeid): FicheGrainDeSable
    {
        return $this->setTrigger('apsCommandeid', $apsCommandeid);
    }

    /**
     * @return string
     */
    public function getApsLignedecommandeassociee(): ?string
    {
        return $this->apsLignedecommandeassociee;
    }

    /**
     * @param string $apsLignedecommandeassociee
     *
     * @return FicheGrainDeSable
     */
    public function setApsLignedecommandeassociee(?string $apsLignedecommandeassociee): FicheGrainDeSable
    {
        return $this->setTrigger('apsLignedecommandeassociee', $apsLignedecommandeassociee);
    }

    /**
     * @return null|string
     */
    public function getStatuscodeString(): ?string
    {
        return $this->statuscodeString;
    }

    /**
     * @param null|string $statuscodeString
     *
     * @return $this
     */
    public function setStatuscodeString(?string $statuscodeString): FicheGrainDeSable
    {
        $this->statuscodeString = $statuscodeString;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getApsSujetmarketing2(): ?int
    {
        return $this->apsSujetmarketing2;
    }

    /**
     * @param int|null $apsSujetmarketing2
     *
     * @return FicheGrainDeSable
     */
    public function setApsSujetmarketing2(?int $apsSujetmarketing2): FicheGrainDeSable
    {
        return $this->setTrigger('apsSujetmarketing2', $apsSujetmarketing2);
    }
}
