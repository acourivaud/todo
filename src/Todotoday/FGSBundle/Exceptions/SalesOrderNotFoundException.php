<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 02/08/17
 * Time: 13:11
 */

namespace Todotoday\FGSBundle\Exceptions;

use Throwable;

/**
 * Class SalesOrderNotFoundException
 * @package Todotoday\FGSBundle\Exceptions
 */
class SalesOrderNotFoundException extends \Exception
{
    /**
     * SalesOrderNotFoundException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = '', $code = 0, Throwable $previous = null)
    {
        $message = 'Sales Order in CRM not found for AX order ref : ' . $message;
        parent::__construct($message, $code, $previous);
    }
}
