<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 02/08/17
 * Time: 13:12
 */

namespace Todotoday\FGSBundle\Exceptions;

use Throwable;

/**
 * Class LigneDeCommandeNotFoundException
 * @package Todotoday\FGSBundle\Exceptions
 */
class LigneDeCommandeNotFoundException extends \Exception
{
    /**
     * LigneDeCommandeNotFoundException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = '', $code = 0, Throwable $previous = null)
    {
        $message = 'LigneDeCommand not found in CRM for inventory lot id AX ' . $message;
        parent::__construct($message, $code, $previous);
    }
}
