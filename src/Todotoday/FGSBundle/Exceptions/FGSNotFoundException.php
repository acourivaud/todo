<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 11/05/17
 * Time: 14:14
 */

namespace Todotoday\FGSBundle\Exceptions;

use Throwable;

/**
 * Class FGSNotFoundException
 * @package Todotoday\FGSBundle\Exceptions
 */
class FGSNotFoundException extends \Exception
{
    /**
     * FGSNotFoundException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = 'FGS not found', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
