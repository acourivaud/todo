<?php declare(strict_types = 1);

namespace Todotoday\QuotationBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class TodotodayQuotationBundle extends Bundle
{
}
