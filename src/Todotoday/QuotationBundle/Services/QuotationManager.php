<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 13/01/2017
 * Time: 19:17
 */

namespace Todotoday\QuotationBundle\Services;

use Actiane\ApiConnectorBundle\Services\Connections\Microsoft\MicrosoftDynamicsConnection;
use Cocur\Slugify\Slugify;
use Knp\Snappy\Pdf;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CartBundle\Entity\CartProduct;
use Todotoday\CatalogBundle\Entity\Api\Microsoft\RetailCatalog;
use Todotoday\CatalogBundle\Services\CatalogManager;
use Todotoday\CheckoutBundle\Entity\Api\Microsoft\SalesOrderHeader;
use Todotoday\CheckoutBundle\Entity\Api\Microsoft\SalesOrderLine;
use Todotoday\CheckoutBundle\Entity\Checkout;
use Todotoday\CheckoutBundle\Service\CheckoutManager;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\QuotationBundle\Entity\Api\Microsoft\SalesDocument;
use Todotoday\QuotationBundle\Entity\Api\Microsoft\TransactionInfoCode;
use Todotoday\QuotationBundle\Exceptions\QuotationMissingException;
use Todotoday\QuotationBundle\Form\Type\QuotationOptionnalFileType;

/**
 * Class QuotationManager
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 *
 * @package Todotoday\QuotationBundle\Services
 */
class QuotationManager
{
    /**
     * DELIVERY_MODE_CODE
     */
    public const DELIVERY_MODE_CODE = 'Devis';
    /**
     * ORDER_ORIGIN_CODE
     */
    public const ORDER_ORIGIN_CODE = 'Front';

    /**
     * @var TwigEngine
     */
    private $twigEngine;

    /**
     * @var Pdf
     */
    private $pdf;

    /**
     * @var Slugify
     */
    private $slugify;

    /**
     * @var MicrosoftDynamicsConnection
     */
    private $microsoft;

    /**
     * @var CatalogManager
     */
    private $catalogManager;

    /**
     * @var CheckoutManager
     */
    private $checkoutManager;

    /**
     * PDFCreator constructor.
     *
     * @param TwigEngine                  $twigEngine
     * @param Pdf                         $pdf
     * @param Slugify                     $slugify
     * @param MicrosoftDynamicsConnection $microsoft
     * @param CatalogManager              $catalogManager
     * @param CheckoutManager             $checkoutManager
     */
    public function __construct(
        TwigEngine $twigEngine,
        Pdf $pdf,
        Slugify $slugify,
        MicrosoftDynamicsConnection $microsoft,
        CatalogManager $catalogManager,
        CheckoutManager $checkoutManager
    ) {
        $this->twigEngine = $twigEngine;
        $this->pdf = $pdf;
        $this->slugify = $slugify;
        $this->microsoft = $microsoft;
        $this->catalogManager = $catalogManager;
        $this->checkoutManager = $checkoutManager;
    }

    /**
     * @param Agency   $agency
     * @param Adherent $adherent
     * @param string   $itemId
     * @param string   $locale
     * @param array    $formAll
     *
     * @return SalesOrderHeader
     * @throws \UnexpectedValueException
     * @throws \Todotoday\CheckoutBundle\Exception\NoProductToCheckoutException
     * @throws \Todotoday\CheckoutBundle\Exception\CheckoutWithoutPaymentException
     * @throws \Todotoday\CheckoutBundle\Exception\CheckoutWithInvalidPaymentMethodException
     * @throws \Todotoday\CartBundle\Exception\NotAdherentCartBundleException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \LogicException
     * @throws \Todotoday\CatalogBundle\Exceptions\ProductNotFoundException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Todotoday\CheckoutBundle\Exception\OrderException
     * @throws \Todotoday\CheckoutBundle\Exception\CheckoutWithoutInitializeException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \InvalidArgumentException
     * @throws \Todotoday\QuotationBundle\Exceptions\QuotationMissingException
     * @throws \Twig_Error
     * @throws \RuntimeException
     */
    public function createAndSendQuotationFile(
        Agency $agency,
        Adherent $adherent,
        string $itemId,
        string $locale,
        array $formAll
    ): SalesOrderHeader {
        $product = $this->getProductQuotation($agency, $itemId, $locale);
        $inputs = $this->getInputsDatas($formAll);
        $fileName = $this->getQuotationFileName($agency->getName(), $adherent);

        $lastForm = end($inputs);
        $additionalFile = $lastForm['is_file'] && $lastForm['value'] instanceof UploadedFile
            ? $lastForm['value']
            : null;

        $fileContent = $this->pdf->getOutputFromHtml(
            $this->getTwigQuotationTemplate($agency->getName(), $adherent, $product, $inputs),
            $this->getQuotationFooter()
        );

        return $this->sendQuotationToMicrosoft(
            $agency,
            $adherent,
            $product,
            $fileName,
            $fileContent,
            $inputs,
            $additionalFile
        );
    }

    /**
     * @param Agency   $agency
     * @param Adherent $adherent
     * @param string   $itemId
     * @param string   $locale
     * @param array    $formAll
     *
     * @return SalesOrderHeader
     * @throws QuotationMissingException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Todotoday\CatalogBundle\Exceptions\ProductNotFoundException
     * @throws \Todotoday\CheckoutBundle\Exception\CheckoutWithInvalidPaymentMethodException
     * @throws \Todotoday\CheckoutBundle\Exception\CheckoutWithoutInitializeException
     * @throws \Todotoday\CheckoutBundle\Exception\CheckoutWithoutPaymentException
     * @throws \Todotoday\CheckoutBundle\Exception\NoProductToCheckoutException
     * @throws \Todotoday\CheckoutBundle\Exception\OrderException
     */
    public function createSimpleQuotation(
        Agency $agency,
        Adherent $adherent,
        string $itemId,
        string $locale,
        array $formAll
    ): SalesOrderHeader {
        $product = $this->getProductQuotation($agency, $itemId, $locale);
        $inputs = $this->getInputsDatas($formAll);
        $lastForm = end($inputs);
        $additionalFile = $lastForm['is_file'] && $lastForm['value'] instanceof UploadedFile
            ? $lastForm['value']
            : null;

        return $this->sendSimpleQuotationToMicrosoft(
            $agency,
            $adherent,
            $product,
            $inputs[0]['value'],
            $additionalFile
        );
    }

    /**
     * @param Agency $agency
     * @param string $itemId
     * @param string $locale
     *
     * @return RetailCatalog
     * @throws \Todotoday\CatalogBundle\Exceptions\ProductNotFoundException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \InvalidArgumentException
     * @throws QuotationMissingException
     */
    public function getProductQuotation(Agency $agency, string $itemId, string $locale): RetailCatalog
    {
        $product = $this->catalogManager->getProduct($agency, $itemId, $locale);

        if (!$product->isQuotation()) {
            throw new QuotationMissingException($itemId, $locale);
        }

        return $product;
    }

    /**
     * @param Agency            $agency
     * @param Adherent          $adherent
     * @param RetailCatalog     $product
     * @param string            $fileName
     * @param string            $fileContent
     * @param array             $inputs
     * @param null|UploadedFile $additionalFile
     *
     * @return SalesOrderHeader
     * @throws \UnexpectedValueException
     * @throws \Todotoday\CheckoutBundle\Exception\NoProductToCheckoutException
     * @throws \Todotoday\CheckoutBundle\Exception\CheckoutWithoutPaymentException
     * @throws \Todotoday\CartBundle\Exception\NotAdherentCartBundleException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \LogicException
     * @throws \Todotoday\CheckoutBundle\Exception\OrderException
     * @throws \Todotoday\CheckoutBundle\Exception\CheckoutWithoutInitializeException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \InvalidArgumentException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function sendQuotationToMicrosoft(
        Agency $agency,
        Adherent $adherent,
        RetailCatalog $product,
        string $fileName,
        string &$fileContent,
        array $inputs,
        ?UploadedFile $additionalFile = null
    ): SalesOrderHeader {
        $checkout = $this->createCheckout($agency, $adherent, $product);
        $salesOrderHeader = $this->checkoutManager->createFullOrder($checkout);
        $this->createSalesDocument($salesOrderHeader, $fileName, $fileContent);

        if ($additionalFile) {
            $this->sendAdditionalFile($additionalFile, $salesOrderHeader);
        }

        $this->createTransactionInfoCodes($salesOrderHeader, $inputs);

        return $salesOrderHeader;
    }

    /**
     * @param Agency            $agency
     * @param Adherent          $adherent
     * @param RetailCatalog     $product
     * @param null|string       $quotationComment
     * @param null|UploadedFile $additionalFile
     *
     * @return SalesOrderHeader
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Todotoday\CheckoutBundle\Exception\CheckoutWithoutInitializeException
     * @throws \Todotoday\CheckoutBundle\Exception\CheckoutWithoutPaymentException
     * @throws \Todotoday\CheckoutBundle\Exception\NoProductToCheckoutException
     * @throws \Todotoday\CheckoutBundle\Exception\OrderException
     */
    public function sendSimpleQuotationToMicrosoft(
        Agency $agency,
        Adherent $adherent,
        RetailCatalog $product,
        ?string $quotationComment = '',
        ?UploadedFile $additionalFile = null
    ): SalesOrderHeader {
        $checkout = $this->createCheckout($agency, $adherent, $product, $quotationComment);
        $salesOrderHeader = $this->checkoutManager->createFullOrder($checkout);

        if ($additionalFile) {
            $this->sendAdditionalFile($additionalFile, $salesOrderHeader);
        }

        return $salesOrderHeader;
    }

    /**
     * @param UploadedFile     $additionalFile
     * @param SalesOrderHeader $salesOrderHeader
     */
    public function sendAdditionalFile(UploadedFile $additionalFile, SalesOrderHeader $salesOrderHeader): void
    {
        $content = file_get_contents($additionalFile->getRealPath());
        $this->createSalesDocument($salesOrderHeader, $additionalFile->getClientOriginalName(), $content);
    }

    /**
     * @param Agency        $agency
     * @param Adherent      $adherent
     * @param RetailCatalog $product
     *
     * @param null|string   $quotationComment
     *
     * @return \Todotoday\CheckoutBundle\Entity\Checkout
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Todotoday\CheckoutBundle\Exception\CheckoutWithoutPaymentException
     * @throws \Todotoday\CheckoutBundle\Exception\NoProductToCheckoutException
     */
    public function createCheckout(
        Agency $agency,
        Adherent $adherent,
        RetailCatalog $product,
        ?string $quotationComment = ''
    ): Checkout {
        $cartProduct = (new CartProduct())
            ->setAgency($agency)
            ->setAdherent($adherent)
            ->setComment($quotationComment)
            ->setQuantity(1);
        $mapping = $this->microsoft->getMapping(RetailCatalog::class);
        $mapping->fillEntityInEntityDoctrine($product, $cartProduct);
        $checkout = $this->checkoutManager->initCheckoutProcess(
            $adherent,
            $agency,
            null,
            static::ORDER_ORIGIN_CODE,
            $cartProduct
        );
        $checkout->setDelivery(static::DELIVERY_MODE_CODE);

        return $checkout;
    }

    /**
     * @param SalesOrderHeader $salesOrderHeader
     * @param string           $fileName
     * @param string           $fileContent
     *
     * @return SalesDocument
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \InvalidArgumentException
     */
    public function createSalesDocument(
        SalesOrderHeader $salesOrderHeader,
        string $fileName,
        string &$fileContent
    ): SalesDocument {
        $salesDocument = new SalesDocument();
        $salesDocument
            ->setSalesNumber($salesOrderHeader->getSalesOrderNumber())
            ->setDataAreaId($salesOrderHeader->getDataAreaId())
            ->setFileName($fileName)
            ->setFileType('PDF')
            ->setTypeId('File')
            ->setCompanyId($salesOrderHeader->getDataAreaId())
            ->setFileContents(base64_encode($fileContent));

        $this->microsoft->persist($salesDocument);
        $this->microsoft->flush();

        return $salesDocument;
    }

    /**
     * @param SalesOrderHeader $salesOrderHeader
     * @param array            $inputs
     *
     * @return array
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \InvalidArgumentException
     */
    public function createTransactionInfoCodes(
        SalesOrderHeader $salesOrderHeader,
        array $inputs
    ): array {
        $transactionInfoCodes = array();

        foreach ($inputs as $input) {
            if ($input['is_file']) {
                continue;
            }
            $subInfoCodeId = $input['is_choice'] ? $input['value'] : '';
            $subInfoCodeId = \is_array($subInfoCodeId) ? implode(';', $subInfoCodeId) : $subInfoCodeId;
            /** @var SalesOrderLine $firstOrderLine */
            $firstOrderLine = $salesOrderHeader->getOrderLines()[0];

            $input['value'] = \is_array($input['value']) ? implode(';', $input['value']) : $input['value'];

            $transactionInfoCode = new TransactionInfoCode();
            $transactionInfoCode
                ->setSalesOrderId($salesOrderHeader->getSalesOrderNumber())
                ->setDataAreaId($salesOrderHeader->getDataAreaId())
                ->setIdActiane($salesOrderHeader->getIdActiane())
                ->setLineNum($firstOrderLine->getLineNum())
                ->setInfocodeId($input['id'])
                ->setInformation($input['value'])
                ->setSubInfocodeId($subInfoCodeId);

            $this->microsoft->persist($transactionInfoCode);

            $transactionInfoCodes[] = $transactionInfoCode;
        }

        $this->microsoft->flush();

        return $transactionInfoCodes;
    }

    /**
     * @param array $formAll
     *
     * @return array
     */
    public function getInputsDatas(array $formAll): array
    {
        $inputs = [];
        /** @var FormInterface|Form $data */
        foreach ($formAll as $data) {
            $isFile = $data->has(QuotationOptionnalFileType::NAME);
            if ($isFile) {
                $data = $data->get(QuotationOptionnalFileType::NAME);
            }
            $isChoice = ($data->getConfig()->getType()->getInnerType() instanceof ChoiceType);
            $id = $data->getConfig()->getName();
            $label = $data->getConfig()->getOption('label');
            $inputData = $data->getData();
            if (($label !== null) && ($inputData !== null)) {
                $inputs[] = array(
                    'id' => $id,
                    'label' => $label,
                    'value' => $inputData,
                    'is_choice' => $isChoice,
                    'is_file' => $isFile,
                );
            }
        }

        return $inputs;
    }

    /**
     * @param string   $agency
     * @param Adherent $adherent
     *
     * @return string
     *
     */
    public function getQuotationFileName(string $agency, Adherent $adherent): string
    {
        $timestamp = (new \DateTime())->getTimestamp();
        $agencyLower = strtolower($agency);
        $fileName =
            'quotation-' . $agencyLower . '-' . $adherent->getLastName() . '-' . $adherent->getFirstName() . '-'
            . $timestamp;

        return $this->slugify->slugify($fileName) . '.pdf';
    }

    /**
     * @return array
     * @throws \Twig_Error
     * @throws \RuntimeException
     */
    public function getQuotationFooter(): array
    {
        return array(
            'footer-html' => $this->twigEngine->render('TodotodayPaymentBundle:Pdf/footer:sepa_footer.html.twig'),
        );
    }

    /**
     * @param string        $agencyName
     * @param Adherent      $adherent
     * @param RetailCatalog $product
     * @param array         $inputs
     *
     * @return string
     * @throws \Twig_Error
     * @throws \RuntimeException
     */
    public function getTwigQuotationTemplate(
        string $agencyName,
        Adherent $adherent,
        RetailCatalog $product,
        array $inputs
    ): string {
        $address = '';
        if ($adherent->getContactPrincipal()) {
            $address =
                $adherent->getContactPrincipal()->getPrimaryAddressStreet() . ' ' . $adherent->getContactPrincipal()
                    ->getPrimaryAddressZipCode() . ' ' . $adherent->getContactPrincipal()->getPrimaryAddressCity();
        }

        $productFamilyName = $this->catalogManager->getProductFamilyName($product);

        return $this->twigEngine->render(
            'TodotodayQuotationBundle:Pdf:quotation.html.twig',
            array(
                'agency' => ucwords($agencyName),
                'user' => array(
                    'numAdherent' => $adherent->getCustomerAccount(),
                    'lastname' => $adherent->getLastName(),
                    'firstname' => $adherent->getFirstName(),
                    'phone' => $adherent->getPhone(),
                    'address' => $address,
                ),
                'productFamilyName' => $productFamilyName,
                'product' => $product,
                'inputs' => $inputs,
            )
        );
    }
}
