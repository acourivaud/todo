<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 23/03/2017
 * Time: 09:12
 */

namespace Todotoday\QuotationBundle\Form\Api;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\Exception\AccessException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Todotoday\QuotationBundle\Exceptions\QuotationMissingException;
use Todotoday\QuotationBundle\Repository\Api\Microsoft\RetailQuotationRepository;

/**
 * Class QuotationSimpleApiType
 *
 * @category   Todo-Todev
 * @package    Todotoday\QuotationBundle
 * @subpackage Todotoday\QuotationBundle\Form\Api
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class QuotationSimpleApiType extends AbstractType
{
    /**
     * QuotationType constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @throws \Symfony\Component\Validator\Exception\ConstraintDefinitionException
     * @throws \Symfony\Component\Validator\Exception\InvalidOptionsException
     * @throws \Symfony\Component\Validator\Exception\MissingOptionsException
     */
    public function buildForm(
        FormBuilderInterface $builder,
        array $options
    ): void {

        $builder->add(
            'quotation_comment',
            TextareaType::class,
            array(
                'label' => 'quotation.simple.label.comment',
                'translation_domain' => 'todotoday',
                'required' => true,
            )
        );
    }
}
