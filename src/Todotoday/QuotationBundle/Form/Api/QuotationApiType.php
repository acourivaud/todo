<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 23/03/2017
 * Time: 09:12
 */

namespace Todotoday\QuotationBundle\Form\Api;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\Exception\AccessException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Count;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Valid;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\MicrosoftBundle\Entity\MicrosoftDateTimeFormater;
use Todotoday\QuotationBundle\Entity\Api\Microsoft\RetailQuotation;
use Todotoday\QuotationBundle\Exceptions\QuotationMissingException;
use Todotoday\QuotationBundle\Form\Type\QuotationNoneType;
use Todotoday\QuotationBundle\Repository\Api\Microsoft\RetailQuotationRepository;

/**
 * Class QuotationApiType
 *
 * @category   Todo-Todev
 * @package    Todotoday\QuotationBundle
 * @subpackage Todotoday\QuotationBundle\Form\Api
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class QuotationApiType extends AbstractType
{
    /**
     * CHOICES_VALUES_DELIMITER
     */
    public const CHOICES_VALUES_DELIMITER = ';';

    /**
     * @var RetailQuotationRepository
     */
    private $retailQuotationRepository;

    /**
     * @var string
     */
    private $env;

    /**
     * QuotationType constructor.
     *
     * @param RetailQuotationRepository $retailQuotationRepository
     * @param string                    $env
     */
    public function __construct(RetailQuotationRepository $retailQuotationRepository, string $env)
    {
        $this->retailQuotationRepository = $retailQuotationRepository;
        $this->env = $env;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @throws QuotationMissingException
     * @throws \Symfony\Component\Validator\Exception\ConstraintDefinitionException
     * @throws \Symfony\Component\Validator\Exception\InvalidOptionsException
     * @throws \Symfony\Component\Validator\Exception\MissingOptionsException
     */
    public function buildForm(
        FormBuilderInterface $builder,
        array $options
    ): void {

        if (!$quotations = $this->retailQuotationRepository->findRetailQuotationByItemIdAndLanguage(
            $options['itemId'],
            $options['language']
        )) {
            throw new QuotationMissingException($options['itemId'], $options['language']);
        }

        /** @var RetailQuotation $quotation */
        foreach ($quotations as $quotation) {
            $type = $quotation->getType();
            $options['type'] = $type;
            if (strtolower($type) === 'date_') {
                $type = 'Date';
            } elseif (\in_array(strtolower($type), ['subcode', 'subcodebuttons'], true)) {
                $type = 'Choice';
            }
            $method = 'buildForm' . ucfirst($type);
            if (method_exists($this, $method)) {
                $this->$method(
                    $builder,
                    $options,
                    $quotation->getInfoCodeId(),
                    $this->createInputOptions(
                        $quotation->getInfoCodePrompt(),
                        $quotation->isMandatory(),
                        $quotation->getInfoCodeDescription()
                    ),
                    $quotation->getValues()
                );
            }
        }
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @param string               $id
     * @param array                $inputOptions
     * @param string|null          $values
     *
     * @return QuotationApiType
     */
    public function buildFormText(
        FormBuilderInterface $builder,
        array $options,
        string $id,
        array $inputOptions,
        ?string $values
    ): self {

        /**
         * @var \Todotoday\AccountBundle\Entity\Account
         */
        $user = $builder->getData()['user'];

        if ($inputOptions['required']) {
            $inputOptions['data'] = $this->mappingUserWithData($id, $user);
        }

        $builder->add(
            $id,
            TextType::class,
            $inputOptions
        );

        return $this;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @param string               $id
     * @param array                $inputOptions
     *
     * @return QuotationApiType
     */
    public function buildFormNone(
        FormBuilderInterface $builder,
        array $options,
        string $id,
        array $inputOptions
    ): self {
        $builder->add(
            $id,
            QuotationNoneType::class,
            ['label' => $inputOptions['label']]
        );

        return $this;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @param string               $id
     * @param array                $inputOptions
     * @param string|null          $values
     *
     * @return QuotationApiType
     *
     * @throws \Symfony\Component\Form\Exception\InvalidArgumentException
     */
    public function buildFormNumeric(
        FormBuilderInterface $builder,
        array $options,
        string $id,
        array $inputOptions,
        ?string $values
    ): self {
        $builder->add(
            $id,
            NumberType::class,
            $inputOptions
        );
        $builder->get($id)->addModelTransformer(
            new CallbackTransformer(
                function ($value) {
                    if (!$value) {
                        return null;
                    }

                    return (string) $value;
                },
                function ($value) {
                    if (!$value) {
                        return null;
                    }

                    return (string) $value;
                }
            )
        );

        return $this;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @param string               $id
     * @param array                $inputOptions
     * @param string|null          $values
     *
     * @return self
     * @throws \Symfony\Component\Form\Exception\InvalidArgumentException
     */
    public function buildFormDate(
        FormBuilderInterface $builder,
        array $options,
        string $id,
        array $inputOptions,
        ?string $values
    ) {
        $isForApi = !$options['csrf_protection'];

        if ($isForApi) {
            $inputOptions['widget'] = 'single_text';
            $inputOptions['format'] = 'dd/MM/yy';
        }

        $inputOptions['years'] = range(1900, (new \DateTime('now'))->format('Y'));
        $builder->add(
            $id,
            DateType::class,
            $inputOptions
        );

        $builder->get($id)->addModelTransformer(
            new CallbackTransformer(
                function (?\DateTime $date) {
                    if (!$date) {
                        return null;
                    }

                    return $date->format(MicrosoftDateTimeFormater::$defaultFormat);
                },
                function (?\DateTime $date) {
                    if (!$date) {
                        return null;
                    }

                    return $date->format(MicrosoftDateTimeFormater::$defaultFormat);
                }
            )
        );

        return $this;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @param string               $id
     * @param array                $inputOptions
     * @param string|null          $values
     *
     * @return QuotationApiType
     *
     * @throws \Symfony\Component\Validator\Exception\MissingOptionsException
     * @throws \Symfony\Component\Validator\Exception\InvalidOptionsException
     * @throws \Symfony\Component\Validator\Exception\ConstraintDefinitionException
     */
    public function buildFormChoice(
        FormBuilderInterface $builder,
        array $options,
        string $id,
        array $inputOptions,
        ?string $values
    ): self {

        if (!$values) {
            return $this;
        }

        $choices = explode(static::CHOICES_VALUES_DELIMITER, $values);
        $choicesArray = [];

        foreach ($choices as $choice) {
            $choicesArray[$choice] = $choice;
        }
        $inputOptions['choices'] = $choicesArray;
        $inputOptions['expanded'] = $options['type'] === 'SubcodeButtons' || \count($choices) < 11;
        $inputOptions['multiple'] = $options['type'] === 'SubcodeButtons';

        if ($inputOptions['required'] === true) {
            $inputOptions['constraints'] = [
                new Valid(),
                new Count(
                    [
                        'min' => 1,
                        'minMessage' => 'quotation.form.error.at_least_one_answer',
                    ]
                ),
            ];
        }

        $inputOptions['required'] = true;//to remove the 'None' option

        $builder->add(
            $id,
            ChoiceType::class,
            $inputOptions
        );

        return $this;
    }

    /**
     * {@inheritdoc}
     * @throws AccessException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setRequired(['itemId', 'language']);
        $resolver->setAllowedTypes('itemId', 'string');
        $resolver->setAllowedTypes('language', 'string');
    }

    /**
     * @param string|null $label
     * @param bool|null   $isMandatory
     * @param string|null $placeholder
     *
     * @return array
     * @throws \Symfony\Component\Validator\Exception\MissingOptionsException
     * @throws \Symfony\Component\Validator\Exception\InvalidOptionsException
     * @throws \Symfony\Component\Validator\Exception\ConstraintDefinitionException
     */
    private function createInputOptions(
        ?string $label = '',
        ?bool $isMandatory = false,
        ?string $placeholder = null
    ): array {
        $options = array(
            'label' => $label,
            'attr' => array('placeholder' => $placeholder),
            'required' => $isMandatory,
            'label_attr' => ['class' => 'quotation_label_title'],
        );

        if ($isMandatory) {
            $options['constraints'] = array(
                new NotBlank(),
            );
        }

        return $options;
    }

    private function mappingUserWithData($id, Adherent $user)
    {
        $data = [
            'Nom' => $user->getLastName(),
            'Prenom' => $user->getFirstName(),
            'Adresse' => $user->getAddressStreet(),
            'Ville' => $user->getAddressCity(),
            'CP' => $user->getAddressZipCode(),
            'TelPorta' => $user->getPhone(),
            'AdrMail' => $user->getEmail(),
        ];

        return $data[$id] ?? '';
    }
}
