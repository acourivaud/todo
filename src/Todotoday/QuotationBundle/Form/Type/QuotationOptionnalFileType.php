<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 23/03/2017
 * Time: 09:12
 */

namespace Todotoday\QuotationBundle\Form\Type;

use Actiane\ToolsBundle\Form\DateTimePickerType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;
use Todotoday\QuotationBundle\Form\Api\QuotationApiType;
use Todotoday\QuotationBundle\Form\Api\QuotationSimpleApiType;

/**
 * Class QuotationOptionnalFileType
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Form\Api
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class QuotationOptionnalFileType extends QuotationSimpleApiType
{
    public const NAME = 'optionnalFile';

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @throws \Symfony\Component\Validator\Exception\MissingOptionsException
     * @throws \Symfony\Component\Validator\Exception\InvalidOptionsException
     * @throws \Symfony\Component\Validator\Exception\ConstraintDefinitionException
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add(
            self::NAME,
            FileType::class,
            array(
                'label' => 'quotation.label.optionnal_file',
                'translation_domain' => 'todotoday',
                'required' => false,
                'label_attr' => ['style' => 'margin-top:70px;'],
                'constraints' => [
                    new File(
                        [
                            'maxSize' => '8M',
                            'mimeTypes' => 'application/pdf',
                        ]
                    ),
                ],
            )
        );
    }
}
