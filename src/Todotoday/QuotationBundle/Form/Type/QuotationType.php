<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 23/03/2017
 * Time: 09:12
 */

namespace Todotoday\QuotationBundle\Form\Type;

use Actiane\ToolsBundle\Form\DateTimePickerType;
use Actiane\ToolsBundle\Form\TimePickerType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Todotoday\QuotationBundle\Form\Api\QuotationApiType;

/**
 * Class QuotationType
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle
 * @subpackage Todotoday\AccountBundle\Form\Api
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class QuotationType extends QuotationApiType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @throws \Todotoday\QuotationBundle\Exceptions\QuotationMissingException
     * @throws \Symfony\Component\Validator\Exception\MissingOptionsException
     * @throws \Symfony\Component\Validator\Exception\InvalidOptionsException
     * @throws \Symfony\Component\Validator\Exception\ConstraintDefinitionException
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        parent::buildForm($builder, $options);
        $builder->add(
            QuotationOptionnalFileType::NAME,
            QuotationOptionnalFileType::class,
            [
                'label_attr' => [
                    'style' => 'display:none;',
                ],
            ]
        );
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @param string               $id
     * @param array                $inputOptions
     * @param null|string          $values
     *
     * @return QuotationType
     * @throws \Symfony\Component\Validator\Exception\MissingOptionsException
     * @throws \Symfony\Component\Validator\Exception\InvalidOptionsException
     * @throws \Symfony\Component\Validator\Exception\ConstraintDefinitionException
     */
    public function buildFormDate(
        FormBuilderInterface $builder,
        array $options,
        string $id,
        array $inputOptions,
        ?string $values
    ): self {
        $dateOption = $inputOptions['attr']['placeholder'];

        if ($dateOption === 'time') {
            if ($inputOptions['required']) {
                $inputOptions['constraints'][] = new Regex(
                    [
                        'pattern' => '/^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/',
                        'match' => true,
                        'message' => 'timepicker_error_invalid_format',
                    ]
                );
            }
            $builder->add($id, TimePickerType::class, $inputOptions);
        } else {
            $inputOptions['time'] = $dateOption === 'datetime';
            $builder->add($id, DateTimePickerType::class, $inputOptions);
        }

        return $this;
    }
}
