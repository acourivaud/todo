<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 15/11/17
 * Time: 18:34
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\QuotationBundle\Form\Type
 *
 * @subpackage Todotoday\QuotationBundle\Form\Type
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\QuotationBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class QuotationNoneType
 */
class QuotationNoneType extends AbstractType
{
    /**
     * @param OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setRequired(['label']);
        $resolver->setAllowedTypes('label', 'string');
    }
}
