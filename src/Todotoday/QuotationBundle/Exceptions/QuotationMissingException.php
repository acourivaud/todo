<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 11/05/17
 * Time: 14:14
 */

namespace Todotoday\QuotationBundle\Exceptions;

use Throwable;

/**
 * Class QuotationMissingException
 * @package Todotoday\QuotationBundle\Exceptions
 */
class QuotationMissingException extends \Exception
{
    /**
     * QuotationMissing constructor.
     *
     * @param string         $itemId
     * @param string         $language
     * @param int            $code
     * @param Throwable|null $previous
     *
     */
    public function __construct(string $itemId, string $language, $code = 0, Throwable $previous = null)
    {
        $message = 'No Quotations for ItemId="' . $itemId . '" and Language="' . $language . '"';
        parent::__construct($message, $code, $previous);
    }
}
