<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 13/04/17
 * Time: 12:36
 */

namespace Todotoday\QuotationBundle\Repository\Api\Microsoft;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiRepository;
use Actiane\ApiConnectorBundle\Lib\Request;

/**
 * Class RetailQuotationRepository
 * @package Todotoday\QuotationBundle\Repository\Api\Microsoft
 */
class RetailQuotationRepository extends AbstractApiRepository
{
    /**
     * @param string $itemId
     * @param string $language
     *
     * @return array|null
     */
    public function findRetailQuotationByItemIdAndLanguage(string $itemId, string $language = 'fr'): ?array
    {
        $filter = 'ItemId eq \'' . $itemId . '\' and Language eq \'' . $this->getLanguage($language) . '\'';

        return $this->getRequest($filter)->execute();
    }

    /**
     * @param string $language
     *
     * @return string
     */
    private function getLanguage(string $language)
    {
        if ($language === 'en') {
            return 'en-US';
        }

        return $language;
    }

    /**
     * @param string $filter
     *
     * @return Request
     * @throws \InvalidArgumentException
     */
    private function getRequest(string $filter): Request
    {
        return $this
            ->createQueryBuilder()
            ->addCustomQuery(
                '$filter',
                $filter
            )
            ->addCustomQuery(
                '$orderby',
                'InfoCodeOrder'
            )
            ->getRequest();
    }
}
