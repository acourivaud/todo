<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 13/04/17
 * Time: 12:36
 */

namespace Todotoday\QuotationBundle\Repository\Api\Microsoft;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiRepository;

/**
 * Class SalesDocumentRepository
 * @package Todotoday\QuotationBundle\Repository\Api\Microsoft
 */
class SalesDocumentRepository extends AbstractApiRepository
{
}
