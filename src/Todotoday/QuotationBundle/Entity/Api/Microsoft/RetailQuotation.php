<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 13/04/17
 * Time: 11:18
 */

namespace Todotoday\QuotationBundle\Entity\Api\Microsoft;

use Actiane\ApiConnectorBundle\Lib\EntityProxyTrait;
use Actiane\ApiConnectorBundle\Lib\Mapping as APIConnector;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class RetailQuotation
 * @SuppressWarnings(PHPMD)
 * @Serializer\ExclusionPolicy("all")
 * @APIConnector\Entity(path="RetailQuotations", repositoryId="todotoday.quotation.repository.api.retail_quotation")
 * @package Todotoday\QuotationBundle\Entity\Api\Microsoft
 */
class RetailQuotation
{
    use EntityProxyTrait;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ItemId", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $itemId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="Description", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $description;

    /**
     * @var string
     *
     * @APIConnector\Column(name="Language", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $language;

    /**
     * @var string
     *
     * @APIConnector\Column(name="InfoCodeDescription", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $infoCodeDescription;

    /**
     * @var string
     *
     * @APIConnector\Column(name="InfoCodeId", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $infoCodeId;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @APIConnector\Column(name="dataAreaId", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $dataAreaId;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @APIConnector\Column(name="GroupId", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $groupId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="Values", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $values;

    /**
     * @var string
     *
     * @APIConnector\Column(name="Type", type="string", mType="Microsoft.Dynamics.DataEntities.RetailInfocodeInputType")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $type;

    /**
     * @var string
     *
     * @APIConnector\Column(name="Mandatory", type="string", mType="Microsoft.Dynamics.DataEntities.NoYes")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $mandatory;

    /**
     * @var int
     *
     * @APIConnector\Column(name="InfoCodeOrder", type="int")
     * @Serializer\Type("int")
     * @Serializer\Expose()
     */
    protected $infoCodeOrder;

    /**
     * @var string
     *
     * @APIConnector\Column(name="InfoCodePrompt", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $infoCodePrompt;

    /**
     * @return string
     */
    public function getItemId(): ?string
    {
        return $this->itemId;
    }

    /**
     * @param string|null $itemId
     *
     * @return RetailQuotation
     */
    public function setItemId(?string $itemId): RetailQuotation
    {
        return $this->setTrigger('itemId', $itemId);
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     *
     * @return RetailQuotation
     */
    public function setDescription(?string $description): RetailQuotation
    {
        return $this->setTrigger('description', $description);
    }

    /**
     * @return string
     */
    public function getLanguage(): ?string
    {
        return $this->language;
    }

    /**
     * @param string|null $language
     *
     * @return RetailQuotation
     */
    public function setLanguage(?string $language): RetailQuotation
    {
        return $this->setTrigger('language', $language);
    }

    /**
     * @return string
     */
    public function getInfoCodeDescription(): ?string
    {
        return $this->infoCodeDescription;
    }

    /**
     * @param string|null $infoCodeDescription
     *
     * @return RetailQuotation
     */
    public function setInfoCodeDescription(?string $infoCodeDescription): RetailQuotation
    {
        return $this->setTrigger('infoCodeDescription', $infoCodeDescription);
    }

    /**
     * @return string
     */
    public function getInfoCodeId(): ?string
    {
        return $this->infoCodeId;
    }

    /**
     * @param string|null $infoCodeId
     *
     * @return RetailQuotation
     */
    public function setInfoCodeId(?string $infoCodeId): RetailQuotation
    {
        return $this->setTrigger('infoCodeId', $infoCodeId);
    }

    /**
     * @return string
     */
    public function getDataAreaId(): ?string
    {
        return $this->dataAreaId;
    }

    /**
     * @param string|null $dataAreaId
     *
     * @return RetailQuotation
     */
    public function setDataAreaId(?string $dataAreaId): RetailQuotation
    {
        return $this->setTrigger('dataAreaId', $dataAreaId);
    }

    /**
     * @return string
     */
    public function getGroupId(): ?string
    {
        return $this->groupId;
    }

    /**
     * @param string|null $groupId
     *
     * @return RetailQuotation
     */
    public function setGroupId(?string $groupId): RetailQuotation
    {
        return $this->setTrigger('groupId', $groupId);
    }

    /**
     * @return string
     */
    public function getValues(): ?string
    {
        return $this->values;
    }

    /**
     * @param string|null $values
     *
     * @return RetailQuotation
     */
    public function setValues(?string $values): RetailQuotation
    {
        return $this->setTrigger('values', $values);
    }

    /**
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     *
     * @return RetailQuotation
     */
    public function setType(?string $type): RetailQuotation
    {
        return $this->setTrigger('type', $type);
    }

    /**
     * @return string
     */
    public function getMandatory(): ?string
    {
        return $this->mandatory;
    }

    /**
     * @return bool|null
     */
    public function isMandatory(): ?bool
    {
        return (strtolower($this->mandatory) === 'yes');
    }

    /**
     * @param string|null $mandatory
     *
     * @return RetailQuotation
     */
    public function setMandatory(?string $mandatory): RetailQuotation
    {
        return $this->setTrigger('mandatory', $mandatory);
    }

    /**
     * @return int
     */
    public function getInfoCodeOrder(): ?int
    {
        return $this->infoCodeOrder;
    }

    /**
     * @param int|null $infoCodeOrder
     *
     * @return RetailQuotation
     */
    public function setInfoCodeOrder(?int $infoCodeOrder): RetailQuotation
    {
        return $this->setTrigger('infoCodeOrder', $infoCodeOrder);
    }

    /**
     * @return string
     */
    public function getInfoCodePrompt(): ?string
    {
        return $this->infoCodePrompt;
    }

    /**
     * @param string|null $infoCodePrompt
     *
     * @return RetailQuotation
     */
    public function setInfoCodePrompt(?string $infoCodePrompt): RetailQuotation
    {
        return $this->setTrigger('infoCodePrompt', $infoCodePrompt);
    }
}
