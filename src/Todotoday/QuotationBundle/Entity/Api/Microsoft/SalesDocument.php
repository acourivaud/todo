<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 13/04/17
 * Time: 11:18
 */

namespace Todotoday\QuotationBundle\Entity\Api\Microsoft;

use Actiane\ApiConnectorBundle\Lib\EntityProxyTrait;
use Actiane\ApiConnectorBundle\Lib\Mapping as APIConnector;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class SalesDocument
 * @SuppressWarnings(PHPMD)
 * @Serializer\ExclusionPolicy("all")
 * @APIConnector\Entity(path="SalesDocuments", repositoryId="todotoday.quotation.repository.api.sales_document")
 * @package Todotoday\QuotationBundle\Entity\Api\Microsoft
 */
class SalesDocument
{
    use EntityProxyTrait;

    /**
     * @var string
     *
     * @APIConnector\Column(name="FileName", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $fileName;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @APIConnector\Column(name="dataAreaId", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $dataAreaId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DefaultAttachment", type="string", mType="Microsoft.Dynamics.DataEntities.NoYes")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $defaultAttachment;

    /**
     * @var string
     *
     * @APIConnector\Column(name="RefCompanyId", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $refCompanyId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="FileType", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $fileType;

    /**
     * @var string
     *
     * @APIConnector\Column(name="Restriction", type="string", mType="Microsoft.Dynamics.DataEntities.DocuRestriction")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $restriction;

    /**
     * @var string
     *
     * @APIConnector\Column(name="SalesNumber", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $salesNumber;

    /**
     * @var int
     *
     * @APIConnector\Column(name="RefRecId", type="int")
     * @Serializer\Type("int")
     * @Serializer\Expose()
     */
    protected $refRecId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CompanyId", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $companyId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="Name", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $name;

    /**
     * @var string
     *
     * @APIConnector\Column(name="Notes", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $notes;

    /**
     * @var int
     *
     * @APIConnector\Column(name="RefTableId", type="int")
     * @Serializer\Type("int")
     * @Serializer\Expose()
     */
    protected $refTableId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="FileContents", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $fileContents;

    /**
     * @var string
     *
     * @APIConnector\Column(name="TypeId", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $typeId;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @APIConnector\Column(name="DocumentId", type="string", guid="id")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $documentId;

    /**
     * @return string|null
     */
    public function getFileName(): ?string
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     *
     * @return SalesDocument
     */
    public function setFileName(?string $fileName)
    {
        return $this->setTrigger('fileName', $fileName);
    }

    /**
     * @return string|null
     */
    public function getDataAreaId(): ?string
    {
        return $this->dataAreaId;
    }

    /**
     * @param string|null $dataAreaId
     *
     * @return SalesDocument
     */
    public function setDataAreaId(?string $dataAreaId)
    {
        return $this->setTrigger('dataAreaId', $dataAreaId);
    }

    /**
     * @return string|null
     */
    public function getDefaultAttachment(): ?string
    {
        return $this->defaultAttachment;
    }

    /**
     * @param string|null $defaultAttachment
     *
     * @return SalesDocument
     */
    public function setDefaultAttachment(?string $defaultAttachment)
    {
        return $this->setTrigger('defaultAttachment', $defaultAttachment);
    }

    /**
     * @return string|null
     */
    public function getRefCompanyId(): ?string
    {
        return $this->refCompanyId;
    }

    /**
     * @param string|null $refCompanyId
     *
     * @return SalesDocument
     */
    public function setRefCompanyId(?string $refCompanyId)
    {
        return $this->setTrigger('refCompanyId', $refCompanyId);
    }

    /**
     * @return string|null
     */
    public function getFileType(): ?string
    {
        return $this->fileType;
    }

    /**
     * @param string|null $fileType
     *
     * @return SalesDocument
     */
    public function setFileType(?string $fileType)
    {
        return $this->setTrigger('fileType', $fileType);
    }

    /**
     * @return string|null
     */
    public function getRestriction(): ?string
    {
        return $this->restriction;
    }

    /**
     * @param string|null $restriction
     *
     * @return SalesDocument
     */
    public function setRestriction(?string $restriction)
    {
        return $this->setTrigger('restriction', $restriction);
    }

    /**
     * @return string|null
     */
    public function getSalesNumber(): ?string
    {
        return $this->salesNumber;
    }

    /**
     * @param string|null $salesNumber
     *
     * @return SalesDocument
     */
    public function setSalesNumber(?string $salesNumber)
    {
        return $this->setTrigger('salesNumber', $salesNumber);
    }

    /**
     * @return int|null
     */
    public function getRefRecId(): ?int
    {
        return $this->refRecId;
    }

    /**
     * @param int|null $refRecId
     *
     * @return SalesDocument
     */
    public function setRefRecId(?int $refRecId)
    {
        return $this->setTrigger('refRecId', $refRecId);
    }

    /**
     * @return string|null
     */
    public function getCompanyId(): ?string
    {
        return $this->companyId;
    }

    /**
     * @param string|null $companyId
     *
     * @return SalesDocument
     */
    public function setCompanyId(?string $companyId)
    {
        return $this->setTrigger('companyId', $companyId);
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     *
     * @return SalesDocument
     */
    public function setName(?string $name)
    {
        return $this->setTrigger('name', $name);
    }

    /**
     * @return string|null
     */
    public function getNotes(): ?string
    {
        return $this->notes;
    }

    /**
     * @param string|null $notes
     *
     * @return SalesDocument
     */
    public function setNotes(?string $notes)
    {
        return $this->setTrigger('notes', $notes);
    }

    /**
     * @return int|null
     */
    public function getRefTableId(): ?int
    {
        return $this->refTableId;
    }

    /**
     * @param int|null $refTableId
     *
     * @return SalesDocument
     */
    public function setRefTableId(?int $refTableId)
    {
        return $this->setTrigger('refTableId', $refTableId);
    }

    /**
     * @return string|null
     */
    public function getFileContents(): ?string
    {
        return $this->fileContents;
    }

    /**
     * @param string|null $fileContents
     *
     * @return SalesDocument
     */
    public function setFileContents(?string $fileContents)
    {
        return $this->setTrigger('fileContents', $fileContents);
    }

    /**
     * @return string|null
     */
    public function getTypeId(): ?string
    {
        return $this->typeId;
    }

    /**
     * @param string|null $typeId
     *
     * @return SalesDocument
     */
    public function setTypeId(?string $typeId)
    {
        return $this->setTrigger('typeId', $typeId);
    }

    /**
     * @return string|null
     */
    public function getDocumentId(): ?string
    {
        return $this->documentId;
    }

    /**
     * @param string|null $documentId
     *
     * @return SalesDocument
     */
    public function setDocumentId(?string $documentId)
    {
        return $this->setTrigger('documentId', $documentId);
    }
}
