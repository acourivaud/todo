<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 13/04/17
 * Time: 11:18
 */

namespace Todotoday\QuotationBundle\Entity\Api\Microsoft;

use Actiane\ApiConnectorBundle\Lib\EntityProxyTrait;
use Actiane\ApiConnectorBundle\Lib\Mapping as APIConnector;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class TransactionInfoCode
 * @SuppressWarnings(PHPMD)
 * @Serializer\ExclusionPolicy("all")
 * @APIConnector\Entity(path="TransactionInfoCodes", repositoryId="todotoday.quotation.repository.api.transaction_info_code")
 *
 * @package Todotoday\QuotationBundle\Entity\Api\Microsoft
 */
class TransactionInfoCode
{
    use EntityProxyTrait;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @APIConnector\Column(name="salesOrderId", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $salesOrderId;

    /**
     * @var float
     *
     * @APIConnector\Id()
     * @APIConnector\Column(name="lineNum", type="float")
     * @Serializer\Type("float")
     * @Serializer\Expose()
     */
    protected $lineNum;

    /**
     * @var string
     *
     * @APIConnector\Column(name="subInfocodeId", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $subInfocodeId;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @APIConnector\Column(name="dataAreaId", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $dataAreaId;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @APIConnector\Column(name="infocodeId", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $infocodeId;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @APIConnector\Column(name="IdActiane", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $idActiane;

    /**
     * @var string
     *
     * @APIConnector\Column(name="inputType", type="string",
     *                                        mType="Microsoft.Dynamics.DataEntities.RetailInfocodeInputType")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $inputType;

    /**
     * @var string
     *
     * @APIConnector\Column(name="information", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $information;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @APIConnector\Column(name="Type", type="string", mType="Microsoft.Dynamics.DataEntities.RetailInfocodeTransType")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $type;

    /**
     * @return string
     */
    public function getSalesOrderId(): ?string
    {
        return $this->salesOrderId;
    }

    /**
     * @param string|null $salesOrderId
     *
     * @return TransactionInfoCode
     */
    public function setSalesOrderId(?string $salesOrderId): TransactionInfoCode
    {
        return $this->setTrigger('salesOrderId', $salesOrderId);
    }

    /**
     * @return float
     */
    public function getLineNum(): ?float
    {
        return $this->lineNum;
    }

    /**
     * @param float|null $lineNum
     *
     * @return TransactionInfoCode
     */
    public function setLineNum(?float $lineNum): TransactionInfoCode
    {
        return $this->setTrigger('lineNum', $lineNum);
    }

    /**
     * @return string
     */
    public function getSubInfocodeId(): ?string
    {
        return $this->subInfocodeId;
    }

    /**
     * @param string|null $subInfocodeId
     *
     * @return TransactionInfoCode
     */
    public function setSubInfocodeId(?string $subInfocodeId): TransactionInfoCode
    {
        return $this->setTrigger('subInfocodeId', $subInfocodeId);
    }

    /**
     * @return string
     */
    public function getDataAreaId(): ?string
    {
        return $this->dataAreaId;
    }

    /**
     * @param string|null $dataAreaId
     *
     * @return TransactionInfoCode
     */
    public function setDataAreaId(?string $dataAreaId): TransactionInfoCode
    {
        return $this->setTrigger('dataAreaId', $dataAreaId);
    }

    /**
     * @return string
     */
    public function getInfocodeId(): ?string
    {
        return $this->infocodeId;
    }

    /**
     * @param string|null $infocodeId
     *
     * @return TransactionInfoCode
     */
    public function setInfocodeId(?string $infocodeId): TransactionInfoCode
    {
        return $this->setTrigger('infocodeId', $infocodeId);
    }

    /**
     * @return string
     */
    public function getIdActiane(): ?string
    {
        return $this->idActiane;
    }

    /**
     * @param string|null $idActiane
     *
     * @return TransactionInfoCode
     */
    public function setIdActiane(?string $idActiane): TransactionInfoCode
    {
        return $this->setTrigger('idActiane', $idActiane);
    }

    /**
     * @return string
     */
    public function getInputType(): ?string
    {
        return $this->inputType;
    }

    /**
     * @param string|null $inputType
     *
     * @return TransactionInfoCode
     */
    public function setInputType(?string $inputType): TransactionInfoCode
    {
        return $this->setTrigger('inputType', $inputType);
    }

    /**
     * @return mixed
     */
    public function getInformation()
    {
        return $this->information;
    }

    /**
     * @param mixed $information
     *
     * @return TransactionInfoCode
     */
    public function setInformation($information): TransactionInfoCode
    {
        if (is_array($information)) {
            $information = implode(';', $information);
        }

        return $this->setTrigger('information', $information);
    }

    /**
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     *
     * @return TransactionInfoCode
     */
    public function setType(?string $type): TransactionInfoCode
    {
        return $this->setTrigger('type', $type);
    }
}
