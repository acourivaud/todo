<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 06/06/17
 * Time: 17:38
 */

namespace Todotoday\QuotationBundle\Tests\Controller\Test;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Actiane\UnitFonctionalTestBundle\Traits\AccessDeniedForNotAdherentTestableTrait;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAccountData;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAdherentLinkedMicrosoftData;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyLinkedMicrosoft;

/**
 * Class QuotationApiCOntrollerTest
 * @package Todotoday\QuotationBundle\Tests\Controller\Test
 */
class QuotationApiCOntrollerTest extends WebTestCase
{
    use AccessDeniedForNotAdherentTestableTrait;

    /**
     * @small
     * @dataProvider notAdherentProvider
     *
     * @param string $accountSlug
     */
    public function testAccesDeniedForNotAdherent(string $accountSlug): void
    {
        $url = $this->getUrl(
            'todotoday.quotation.api.quotation.post_quotation',
            array(
                'itemId' => 'osef',
            )
        );
        $this->accessDeniedForNotAdherent($accountSlug, 'api', $url);
    }

    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAgencyLinkedMicrosoft::class,
            LoadAccountData::class,
            LoadAdherentLinkedMicrosoftData::class,
        );
    }
}
