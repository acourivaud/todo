<?php declare(strict_types = 1);

/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 01/03/2017
 * Time: 12:40
 */

namespace Todotoday\QuotationBundle\Tests\Repository\Api\Microsoft;

use Actiane\ApiConnectorBundle\Tests\Repository\Api\AbstractRepositoryTestCase;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use Todotoday\QuotationBundle\Entity\Api\Microsoft\RetailQuotation;
use Todotoday\QuotationBundle\Repository\Api\Microsoft\RetailQuotationRepository;

/**
 * Class SalesDocumentRepositoryTest
 *
 * @category   Todo-Todev
 * @package    Todotoday\QuotationBundle
 * @subpackage Todotoday\QuotationBundle\Tests\Repository\Api\Microsoft
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class SalesDocumentRepositoryTest extends AbstractRepositoryTestCase
{
    /**
     * Do getRepositoryId
     *
     * @return string
     */
    protected function getRepositoryId(): string
    {
        return 'todotoday.quotation.repository.api.sales_document';
    }
}
