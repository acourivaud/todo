<?php

declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 06/03/17
 * Time: 11:56
 */

namespace Todotoday\QuotationBundle\Controller\Api;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as FosRest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Acl\Model\DomainObjectInterface;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Exceptions\AgencyMissingException;
use Todotoday\QuotationBundle\Form\Api\QuotationApiType;

/**
 * Class QuotationApiController
 *
 * @package Todotoday\QuotationBundle\Controller\Api
 * @FosRest\NamePrefix("todotoday.quotation.api.quotation.")
 * @FosRest\Prefix(value="/quotation/{itemId}")
 */
class QuotationApiController extends FOSRestController implements DomainObjectInterface
{
    /**
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Post a quotation with an itemId for current agency",
     *     description="Post a quotation with an itemId for current agency",
     *     output={
     *           "class"="Todotoday\CheckoutBundle\Entity\Api\Microsoft\SalesOrderHeader",
     *           "groups"={"Default"}
     *     },
     *     views={"Quotation"},
     *     section="Quotation"
     * )
     *
     * @FosRest\Route("", options={"expose"=true})
     * @FosRest\QueryParam(
     *     name="fields",
     *     requirements="((Default),?)*",
     *     default="",
     *     description="Fields you need"
     * )
     *
     * @param string  $itemId
     * @param Request $request
     *
     * @return Response
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \UnexpectedValueException
     * @throws \Todotoday\CartBundle\Exception\NotAdherentCartBundleException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \RuntimeException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \LogicException
     * @throws AgencyMissingException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Todotoday\CatalogBundle\Exceptions\ProductNotFoundException
     * @throws \Todotoday\CheckoutBundle\Exception\CheckoutWithInvalidPaymentMethodException
     * @throws \Todotoday\CheckoutBundle\Exception\CheckoutWithoutInitializeException
     * @throws \Todotoday\CheckoutBundle\Exception\CheckoutWithoutPaymentException
     * @throws \Todotoday\CheckoutBundle\Exception\NoProductToCheckoutException
     * @throws \Todotoday\CheckoutBundle\Exception\OrderException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\QuotationBundle\Exceptions\QuotationMissingException
     * @throws \Twig_Error
     */
    public function postQuotationAction(string $itemId, Request $request): Response
    {
        if (!$this->isGranted('CREATE', $this)) {
            throw $this->createAccessDeniedException();
        }

        $agency = $this->getAgency();
        $locale = $request->getLocale();

        $data = $form = $this->createForm(
            QuotationApiType::class,
            null,
            array(
                'itemId' => $itemId,
                'language' => $locale,
                'csrf_protection' => false,
            )
        );

        $form->submit($request->request->all());

        $quotationManager = $this->get('todotoday.quotation.quotation_manager');
        $data = $quotationManager->createAndSendQuotationFile(
            $agency,
            $this->getUser(),
            $itemId,
            $locale,
            $form->all()
        );
        $view = $this->view($data);

        return $this->handleView($view);
    }

    /**
     * Returns a unique identifier for this domain object.
     *
     * @return string
     */
    public function getObjectIdentifier(): string
    {
        return 'class';
    }

    /**
     * @return null|Agency
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \UnexpectedValueException
     * @throws AgencyMissingException
     */
    private function getAgency(): ?Agency
    {
        if (!$agency = $this->get('todotoday.core.domain_context.api')->getAgency()) {
            throw new AgencyMissingException('Agency is missing');
        }

        return $agency;
    }
}
