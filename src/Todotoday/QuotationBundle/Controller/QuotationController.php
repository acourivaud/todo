<?php declare(strict_types=1);

namespace Todotoday\QuotationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Services\AdherentManager;
use Todotoday\CatalogBundle\Entity\Api\Microsoft\RetailCatalog;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\QuotationBundle\Exceptions\QuotationMissingException;
use Todotoday\QuotationBundle\Form\Type\QuotationSimpleType;
use Todotoday\QuotationBundle\Form\Type\QuotationType;

/**
 * Class QuotationController
 *
 * @package Todotoday\QuotationBundle\Controller
 * @Route("/quotation")
 */
class QuotationController extends Controller
{
    /**
     * @Route("/{itemId}", name="quotation_index", options={"expose"=true})
     *
     * @param Request $request
     * @param string  $itemId
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Todotoday\CheckoutBundle\Exception\NoProductToCheckoutException
     * @throws \Todotoday\CheckoutBundle\Exception\CheckoutWithoutPaymentException
     * @throws \Todotoday\CheckoutBundle\Exception\CheckoutWithInvalidPaymentMethodException
     * @throws \Todotoday\CartBundle\Exception\NotAdherentCartBundleException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \Twig_Error
     * @throws \Todotoday\QuotationBundle\Exceptions\QuotationMissingException
     * @throws \Todotoday\CheckoutBundle\Exception\OrderException
     * @throws \Todotoday\CheckoutBundle\Exception\CheckoutWithoutInitializeException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\Form\Exception\UnexpectedTypeException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \RuntimeException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Symfony\Component\Form\Exception\LogicException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \Todotoday\CatalogBundle\Exceptions\ProductNotFoundException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \InvalidArgumentException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     */
    public function indexAction(Request $request, string $itemId): Response
    {
        if (!$this->isGranted('ROLE_QUOTATION')) {
            throw $this->createAccessDeniedException();
        }

        $agency = $this->getAgency();
        $locale = $request->getLocale();

        $product = $this->get('todotoday_catalog.catalog_manager')->getProduct($agency, $itemId, $locale);
        if (!$product->isQuotation()) {
            throw $this->createNotFoundException(
                sprintf(
                    'Product %s is not a quotation. Type is %s',
                    $product->getItemId(),
                    $product->getItemModelGroup()
                )
            );
        }

        /** @var AdherentManager $adherentManager */
        $adherentManager = $this->get('todotoday.account.services.adherent_manager');

        /** @var Adherent $account */
        $account = $adherentManager->getUser(
            null,
            array(
                'contactPrincipals' => true,
                'contactChildren' => false,
                'contactAssistants' => false,
                'bankAccount' => false,
            )
        );

        try {
            $form = $this->createForm(
                QuotationType::class,
                [
                    'user' => $account,
                ],
                array(
                    'itemId' => $itemId,
                    'language' => $locale,
                )
            );
        } catch (QuotationMissingException $exception) {
            return $this->forward(
                'TodotodayQuotationBundle:Quotation:quotationSimple',
                array(
                    'request' => $request,
                    'itemId' => $itemId,
                    'product' => $product,
                )
            );
        }

        if ($this->isGranted('ROLE_QUOTATION_CREATE')) {
            $form->add('submit', SubmitType::class, array('label' => 'valid', 'translation_domain' => 'todotoday'));
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $quotationManager = $this->get('todotoday.quotation.quotation_manager');
            $salesOrderHeader = $quotationManager->createAndSendQuotationFile(
                $agency,
                $this->getUser(),
                $itemId,
                $locale,
                $form->all()
            );

            return $this->forward(
                'TodotodayCheckoutBundle:Checkout:checkoutQuotationSuccess',
                array(
                    'product' => $product,
                    'salesOrderHeader' => $salesOrderHeader,
                )
            );
        }

        $productFamily = $this->get('todotoday_catalog.catalog_manager')
            ->getProductFamilyName($product);

        return $this->render(
            'quotation/quotation.html.twig',
            array(
                'product' => $product,
                'productFamily' => $productFamily,
                'form' => $form->createView(),
                'errors' => $form->getErrors(true),
            )
        );
    }

    /**
     * @param Request       $request
     * @param RetailCatalog $product
     *
     * @return Response
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \Symfony\Component\Form\Exception\UnexpectedTypeException
     * @throws \Symfony\Component\Form\Exception\LogicException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \LogicException
     * @throws QuotationMissingException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Todotoday\CatalogBundle\Exceptions\ProductNotFoundException
     * @throws \Todotoday\CheckoutBundle\Exception\CheckoutWithInvalidPaymentMethodException
     * @throws \Todotoday\CheckoutBundle\Exception\CheckoutWithoutInitializeException
     * @throws \Todotoday\CheckoutBundle\Exception\CheckoutWithoutPaymentException
     * @throws \Todotoday\CheckoutBundle\Exception\NoProductToCheckoutException
     * @throws \Todotoday\CheckoutBundle\Exception\OrderException
     */
    public function quotationSimpleAction(Request $request, RetailCatalog $product): Response
    {
        $agency = $this->getAgency();
        $locale = $request->getLocale();

        $form = $this->createForm(
            QuotationSimpleType::class,
            null
        );

        if ($this->isGranted('ROLE_QUOTATION_CREATE')) {
            $form->add('submit', SubmitType::class, array('label' => 'valid', 'translation_domain' => 'todotoday'));
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $quotationManager = $this->get('todotoday.quotation.quotation_manager');
            $salesOrderHeader = $quotationManager->createSimpleQuotation(
                $agency,
                $this->getUser(),
                $product->getItemId(),
                $locale,
                $form->all()
            );

            return $this->forward(
                'TodotodayCheckoutBundle:Checkout:checkoutQuotationSuccess',
                array(
                    'product' => $product,
                    'salesOrderHeader' => $salesOrderHeader,
                )
            );
        }

        $productFamily = $this->get('todotoday_catalog.catalog_manager')
            ->getProductFamilyName($product);

        return $this->render(
            'quotation/quotation.html.twig',
            array(
                'product' => $product,
                'productFamily' => $productFamily,
                'form' => $form->createView(),
                'errors' => $form->getErrors(true),
            )
        );
    }

    /**
     * Do getAgency
     *
     * @return Agency
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \UnexpectedValueException
     * @throws NotFoundHttpException
     */
    protected function getAgency(): Agency
    {
        if (!$agency = $this->get('todotoday.core.domain_context')->getCurrentContext()->getAgency()) {
            throw $this->createNotFoundException();
        }

        return $agency;
    }
}
