<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 01/11/17
 * Time: 17:26
 */

namespace Todotoday\QuotationBundle\Twig;

/**
 * Class InstanceOfExtension
 *
 * @package Todotoday\QuotationBundle\Twig
 */
class InstanceOfExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getTests(): array
    {
        return [
            new \Twig_SimpleTest('instanceOf', [$this, 'isInstanceOf']),
        ];
    }

    /**
     * @param mixed $value
     * @param mixed $instanceTest
     *
     * @return bool
     * @throws \ReflectionException
     */
    public function isInstanceOf($value, $instanceTest): bool
    {
        if (is_scalar($value) || \is_array($value)) {
            return false;
        }
        $reflection = new \ReflectionClass($instanceTest);

        return $reflection->isInstance($value);
    }
}
