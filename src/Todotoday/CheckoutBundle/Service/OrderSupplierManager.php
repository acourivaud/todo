<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 29/12/17
 * Time: 14:15
 *
 * @category   tdtd
 *
 * @package    Todotoday\CheckoutBundle\Service
 *
 * @subpackage Todotoday\CheckoutBundle\Service
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\CheckoutBundle\Service;

use Actiane\ApiConnectorBundle\Services\Connections\Microsoft\MicrosoftDynamicsConnection;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CheckoutBundle\Entity\Api\Microsoft\SalesOrderDigHeaders;
use Todotoday\CheckoutBundle\Entity\Api\Microsoft\SalesOrderHeader;
use Todotoday\CheckoutBundle\Repository\Api\Microsoft\SalesOrderDigHeadersRepository;
use Todotoday\CheckoutBundle\Repository\Api\Microsoft\SalesOrderHeaderRepository;

/**
 * Class OrderSupplierManager
 */
class OrderSupplierManager
{
    /**
     * @var MicrosoftDynamicsConnection
     */
    private $connection;

    /**
     * @var OrderManager
     */
    private $orderManager;

    /**
     * OrderSupplierManager constructor.
     *
     * @param MicrosoftDynamicsConnection $connection
     * @param OrderManager                $orderManager
     */
    public function __construct(MicrosoftDynamicsConnection $connection, OrderManager $orderManager)
    {
        $this->connection = $connection;
        $this->orderManager = $orderManager;
    }

    /**
     * @param Adherent $adherent
     *
     * @return SalesOrderHeader[]|null
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    public function getSupplierOrder(Adherent $adherent): ?array
    {
        /** @var SalesOrderDigHeadersRepository $repoSupplier */
        $repoSupplier = $this->connection->getRepository(SalesOrderDigHeaders::class);
        /** @var SalesOrderHeaderRepository $repoOrder */
        $repoOrder = $this->connection->getRepository(SalesOrderHeader::class);

        $supplierOrders = $repoSupplier->getRefusedOrder($adherent);
        $orders = [];

        if (!$supplierOrders) {
            return $orders;
        }

        foreach ($supplierOrders as $supplierOrder) {
            // we retrieve all the order
            /** @var SalesOrderHeader $order */
            $order = $repoOrder->find(
                [
                    'dataAreaId' => $adherent->getDataAreaId(),
                    'salesOrderNumber' => $supplierOrder->getSalesOrderNumber(),
                ]
            );
            $order->setSupplierOrder($supplierOrder);
            // we add line and picts
            $this->orderManager->addLineAndPictos($order);
            $orders[] = $order;
        }

        return $orders;
    }
}
