<?php

declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 13/04/17
 * Time: 16:29
 */

namespace Todotoday\CheckoutBundle\Service;

use Actiane\ApiConnectorBundle\Services\Connections\Microsoft\MicrosoftDynamicsConnection;
use JMS\Serializer\Serializer;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AnimationBundle\Services\AnimationManager;
use Todotoday\CheckoutBundle\Entity\Api\Microsoft\SalesOrderDigHeaders;
use Todotoday\CheckoutBundle\Entity\Api\Microsoft\SalesOrderHeader;
use Todotoday\CheckoutBundle\Entity\Api\Microsoft\SalesOrderLine;
use Todotoday\CheckoutBundle\Enum\OrderSatisfactionSubjectEnum;
use Todotoday\CheckoutBundle\Enum\OrderStatusEnum;
use Todotoday\CheckoutBundle\Exception\OrderNotBelongsToAdherentException;
use Todotoday\CheckoutBundle\Exception\OrderNotCancellableException;
use Todotoday\CheckoutBundle\Exception\OrderNotFoundException;
use Todotoday\CheckoutBundle\Exception\OrderNotRatableException;
use Todotoday\CheckoutBundle\Exception\OrderNotRefusedException;
use Todotoday\CheckoutBundle\Repository\Api\Microsoft\SalesOrderDigHeadersRepository;
use Todotoday\CheckoutBundle\Repository\Api\Microsoft\SalesOrderHeaderRepository;
use Todotoday\CheckoutBundle\Repository\Api\Microsoft\SalesOrderLineRepository;
use BadMethodCallException;
use DateTime;

/**
 * Class OrderManager
 *
 * @package Todotoday\CheckoutBundle\Service
 */
class OrderManager
{
    /**
     * @var MicrosoftDynamicsConnection
     */
    private $connection;

    /**
     * @var AdapterInterface
     */
    private $cache;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var BookingManager
     */
    private $bookingManager;

    /**
     * @var AdapterInterface
     */
    private $historyCache;

    /**
     * @var AnimationManager
     */
    private $animationManager;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * CommandService constructor.
     *
     * @param MicrosoftDynamicsConnection $connection
     * @param AdapterInterface            $cache
     * @param Serializer                  $serializer
     * @param BookingManager              $bookingManager
     * @param AdapterInterface            $historyCache
     * @param AnimationManager            $animationManager
     * @param TranslatorInterface         $translator
     */
    public function __construct(
        MicrosoftDynamicsConnection $connection,
        AdapterInterface $cache,
        Serializer $serializer,
        BookingManager $bookingManager,
        AdapterInterface $historyCache,
        AnimationManager $animationManager,
        TranslatorInterface $translator
    ) {
        $this->connection = $connection;
        $this->cache = $cache;
        $this->serializer = $serializer;
        $this->bookingManager = $bookingManager;
        $this->historyCache = $historyCache;
        $this->animationManager = $animationManager;
        $this->translator = $translator;
    }

    /**
     * @param array $orders
     *
     * @return SalesOrderHeader[]|null
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    public function addLineAndPictosAll(array $orders): ?array
    {
        /** @var SalesOrderHeader $order */
        foreach ($orders as $order) {
            $this->addLineAndPictos($order);
        }

        return $orders;
    }

    /**
     * @param SalesOrderHeader $order
     *
     * @return SalesOrderHeader
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    public function addLineAndPictos(SalesOrderHeader $order): SalesOrderHeader
    {
        if ($lines = $this->getOrderLines($order)) {
            foreach ($lines as $line) {
                $order->addOrderLines($line);
                $order->addMainPicto($line->getHierarchy());
            }
        }

        return $order;
    }

    /**
     * @param Adherent $adherent
     * @param string   $orderId
     *
     * @return string
     * @throws \Todotoday\CheckoutBundle\Exception\OrderNotFoundException
     * @throws \Todotoday\CheckoutBundle\Exception\OrderNotBelongsToAdherentException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \InvalidArgumentException
     * @throws \Todotoday\CheckoutBundle\Exception\BookingCantBeCancelledException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws OrderNotCancellableException
     */
    public function cancelOrder(Adherent $adherent, string $orderId): string
    {
        $cancelResult = $this->cancelOrderWithoutChecking($adherent, $orderId);
        $order = $cancelResult['order'];
        $result = $cancelResult['result'];

        if ($order->hasRDV()) {
            $this->bookingManager->cancelBookingWithCommandeid($adherent, $orderId);
        }
        if ($order->hasAnimation()) {
            $this->animationManager->cancelParticipationAnimationWithCommandeid($adherent, $orderId);
        }

        return $result;
    }

    /**
     * @param Adherent $adherent
     * @param string   $orderId
     *
     * @return array
     * @throws \Todotoday\CheckoutBundle\Exception\OrderNotBelongsToAdherentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Todotoday\CheckoutBundle\Exception\OrderNotCancellableException
     * @throws OrderNotCancellableException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws OrderNotFoundException
     */
    public function cancelOrderWithoutChecking(Adherent $adherent, string $orderId): array
    {
        /** @var SalesOrderHeaderRepository $repo */
        $repo = $this->connection->getRepository(SalesOrderHeader::class);

        $order = $this->getOrder($adherent, $orderId);
        if (!$order->isCancellable()) {
            throw new OrderNotCancellableException();
        }

        $result = $repo->cancelOrder($adherent->getDataAreaId(), $orderId);
        $this->deleteHistoryCache($adherent, $order->getCreationDate());

        return array(
            'order' => $order,
            'result' => $result,
        );
    }

    /**
     * @param Adherent $adherent
     *
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function deleteCache(Adherent $adherent): void
    {
        $ordersCached = $this->cache->getItem($this->getKeys($adherent, 'pending'));
        $this->cache->deleteItem($ordersCached);
    }

    /**
     * @param Adherent  $adherent
     * @param \DateTime $creationDate
     *
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function deleteHistoryCache(Adherent $adherent, \DateTime $creationDate): void
    {
        $monthYear = $creationDate->format('Y-m');
        $this->historyCache->deleteItem($this->getMonthKeys($adherent, 'history', $monthYear));
    }

    /**
     * @param Adherent $adherent
     * @param string   $status
     * @param string   $repoMethod
     *
     * @return SalesOrderHeader[]|null
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \BadMethodCallException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    public function getOrders(Adherent $adherent, string $status, string $repoMethod): ?array
    {
        $ordersCached = $this->cache->getItem($this->getKeys($adherent, $status));

        if (!$ordersCached->isHit()) {

            /** @var SalesOrderHeaderRepository $repo */
            $repo = $this->connection->getRepository(SalesOrderHeader::class);

            if (!method_exists($repo, $repoMethod)) {
                throw new BadMethodCallException(
                    sprintf('Method "%s" not found on "%s"', $repoMethod, \get_class($repo))
                );
            }
            if ($orders = $repo->$repoMethod($adherent, $status)) {
                $orders = $this->addLineAndPictosAll($orders);

                /** @var SalesOrderDigHeadersRepository $supplierRepo */
                $supplierRepo = $this->connection->getRepository(SalesOrderDigHeaders::class);

                foreach ($orders as $order) {
                    if ($supplierOrder =
                        $supplierRepo->getSupplierOrder($adherent, $order->getSalesOrderNumber())) {
                        $order->setSupplierOrder($supplierOrder);
                    }
                }
            }
            $ordersCached->set($this->serializer->serialize($orders, 'json'));
            $this->cache->save($ordersCached);
        }
        if ($ordersCached->get() === 'null') {
            return null;
        }

        return $this->serializer->deserialize($ordersCached->get(), 'array<' . SalesOrderHeader::class . '>', 'json');
    }

    /**
     * @param Adherent $adherent
     * @param string   $status
     *
     * @return SalesOrderHeader[]|null
     * @throws \BadMethodCallException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     */
    public function getFullOrder(Adherent $adherent, string $status = 'pending'): ?array
    {
        return $this->getOrders($adherent, $status, 'getOrders');
    }

    /**
     * @param Adherent $adherent
     *
     * @return null|array
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \BadMethodCallException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function getRatableOrders(Adherent $adherent): ?array
    {
        return $this->getOrders($adherent, 'ratable', 'getRatableOrders');
    }

    /**
     * @param Adherent $adherent
     *
     * @return null|array
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \BadMethodCallException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function getQuotationOrders(Adherent $adherent): ?array
    {
        return $this->getOrders($adherent, 'pending', 'getQuotationOrders');
    }

    /**
     * @param Adherent $adherent
     * @param string   $monthYear
     * @param string   $repoMethod
     * @param string   $status
     * @param bool     $useNormalCache
     *
     * @return SalesOrderHeader[]|null
     * @throws \BadMethodCallException
     *
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    public function getMonthOrders(
        Adherent $adherent,
        string $monthYear,
        string $repoMethod,
        string $status,
        bool $useNormalCache = true
    ): ?array {
        $cache = $useNormalCache ? $this->cache : $this->historyCache;

        $datetime = new DateTime($monthYear);
        $ordersCached = $cache->getItem($this->getMonthKeys($adherent, $status, $monthYear));

        if (!$ordersCached->isHit()) {

            /** @var SalesOrderHeaderRepository $repo */
            $repo = $this->connection->getRepository(SalesOrderHeader::class);

            if (!method_exists($repo, $repoMethod)) {
                throw new BadMethodCallException(
                    sprintf('Method "%s" not found on "%s"', $repoMethod, \get_class($repo))
                );
            }
            if ($orders = $repo->$repoMethod($adherent, $datetime)) {
                $orders = $this->addLineAndPictosAll($orders);
            }
            $ordersCached->set($this->serializer->serialize($orders, 'json'));
            $cache->save($ordersCached);
        }

        if ($ordersCached->get() === 'null') {
            return null;
        }

        return $this->serializer->deserialize($ordersCached->get(), 'array<' . SalesOrderHeader::class . '>', 'json');
    }

    /**
     * @param Adherent $adherent
     * @param string   $monthYear
     *
     * @return SalesOrderHeader[]|null
     * @throws \BadMethodCallException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    public function getAllMonthOrders(Adherent $adherent, string $monthYear): ?array
    {
        return $this->getMonthOrders($adherent, $monthYear, 'getMonthOrders', 'all');
    }

    /**
     * @param Adherent $adherent
     * @param string   $monthYear
     *
     * @return SalesOrderHeader[]|null
     * @throws \BadMethodCallException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    public function getMonthHistoryOrders(Adherent $adherent, string $monthYear): ?array
    {
        return $this->getMonthOrders($adherent, $monthYear, 'getMonthHistoryOrders', 'history', false);
    }

    /**
     * @param Adherent    $adherent
     * @param string|null $status
     *
     * @return SalesOrderHeader[]|null
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    public function getOrderHeader(Adherent $adherent, string $status = 'pending'): ?array
    {
        /** @var SalesOrderHeaderRepository $repo */
        $repo = $this->connection->getRepository(SalesOrderHeader::class);

        return $repo->getOrders($adherent, $status);
    }

    /**
     * @param SalesOrderHeader $salesOrderHeader
     *
     * @return SalesOrderLine[]
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     */
    public function getOrderLines(SalesOrderHeader $salesOrderHeader): ?array
    {
        /** @var SalesOrderLineRepository $repo */
        $repo = $this->connection->getRepository(SalesOrderLine::class);

        return $repo->getLinesFromOrder($salesOrderHeader->getSalesOrderNumber());
    }

    /**
     * @param Adherent $adherent
     * @param string   $orderId
     * @param string   $rating
     *
     * @return string
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Todotoday\CheckoutBundle\Exception\OrderNotBelongsToAdherentException
     * @throws \Todotoday\CheckoutBundle\Exception\OrderNotRatableException
     * @throws OrderNotFoundException
     */
    public function rateOrder(Adherent $adherent, string $orderId, string $rating): string
    {
        $order = $this->getOrder($adherent, $orderId);

        if (!$order->isRatable()) {
            throw new OrderNotRatableException();
        }

        /** @var SalesOrderHeaderRepository $repo */
        $repo = $this->connection->getRepository(SalesOrderHeader::class);

        $this->deleteHistoryCache($adherent, $order->getCreationDate());
        /*$result = */
        $repo->rateOrder($adherent->getDataAreaId(), $orderId, $rating);

        return $rating;
//        return $order->getDeliveryTermsCode();
    }

    /**
     * @param Adherent $adherent
     *
     * @return int
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    public function getNumberRatableOrders(Adherent $adherent): int
    {
        /** @var SalesOrderHeaderRepository $repo */
        $repo = $this->connection->getRepository(SalesOrderHeader::class);

        $orders = $repo->getRatableOrdersShortSelect($adherent);

        return is_array($orders) ? count($orders) : 0;
    }

    /**
     * @param Adherent $adherent
     * @param string   $orderId
     *
     * @return SalesOrderHeader
     * @throws \Todotoday\CheckoutBundle\Exception\OrderNotBelongsToAdherentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     * @throws OrderNotBelongsToAdherentException
     * @throws OrderNotFoundException
     */
    public function getOrder(Adherent $adherent, string $orderId): SalesOrderHeader
    {
        /** @var SalesOrderHeaderRepository $repo */
        $repo = $this->connection->getRepository(SalesOrderHeader::class);

        /** @var SalesOrderHeader $order */
        if (!$order = $repo->find(
            array(
                'dataAreaId' => $adherent->getDataAreaId(),
                'salesOrderNumber' => $orderId,
            )
        )) {
            throw new OrderNotFoundException($orderId);
        }

        /** @var SalesOrderDigHeadersRepository $supplierRepo */
        $supplierRepo = $this->connection->getRepository(SalesOrderDigHeaders::class);

        if ($supplierOrder =
            $supplierRepo->getSupplierOrder($adherent, $order->getSalesOrderNumber())
        ) {
            $order->setSupplierOrder($supplierOrder);
        }

        $order = $this->setOrderStatus($order);

        $cutomerAccount = $adherent->getCustomerAccount();
        if ($order->getOrderingCustomerAccountNumber() !== $cutomerAccount
            && $order->getInvoiceCustomerAccountNumber() !== $cutomerAccount
        ) {
            throw new OrderNotBelongsToAdherentException();
        }

        return $order;
    }

    /**
     * @param null|string $locale
     *
     * @return array
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     */
    public function getOrderSatisfactionSubjectsTranslate(?string $locale): array
    {
        $satisfactionSubjects = OrderSatisfactionSubjectEnum::getChoicesForFront();

        foreach ($satisfactionSubjects as $key => $subject) {
            $satisfactionSubjects[$key]['text'] = $this->translator->trans($subject['text'], [], 'todotoday', $locale);
        }

        return $satisfactionSubjects;
    }

    /**
     * @param Adherent $adherent
     * @param array    $order
     * @param array    $orderLines
     *
     * @return SalesOrderHeader
     * @throws \Todotoday\CheckoutBundle\Exception\OrderNotRefusedException
     * @throws \Todotoday\CheckoutBundle\Exception\OrderNotBelongsToAdherentException
     * @throws OrderNotBelongsToAdherentException
     * @throws OrderNotRefusedException
     */
    public function editOrderRefused(Adherent $adherent, array $order, array $orderLines): SalesOrderHeader
    {
        /** @var SalesOrderHeaderRepository $salesOrderHeaderRepo */
        $salesOrderHeaderRepo = $this->connection->getRepository(SalesOrderHeader::class);

        /** @var SalesOrderHeader $orderMicrosoft */
        $orderMicrosoft = $salesOrderHeaderRepo->find($order['id']);

        if (!($adherent instanceof Adherent)
            || !\in_array(
                $adherent->getCustomerAccount(),
                array(
                    $orderMicrosoft->getOrderingCustomerAccountNumber(),
                    $orderMicrosoft->getInvoiceCustomerAccountNumber(),
                ),
                true
            )
        ) {
            throw new OrderNotBelongsToAdherentException();
        }

        /** @var SalesOrderDigHeadersRepository $supplierRepo */
        $supplierRepo = $this->connection->getRepository(SalesOrderDigHeaders::class);
        $supplier = $supplierRepo->getSupplierOrder($adherent, $orderMicrosoft->getSalesOrderNumber());

        if (!$supplier || $supplier->getResponseState() !== OrderStatusEnum::SUPPLIER_REJECTED) {
            throw new OrderNotRefusedException();
        }

        $orderMicrosoft->setCustomerRefDepot($order['customerRefDepot']);
        $this->connection->persist($orderMicrosoft);

        foreach ($orderLines as $orderLine) {
            /** @var SalesOrderLineRepository $orderLineRepo */
            $orderLineRepo = $this->connection->getRepository(SalesOrderLine::class);

            /** @var SalesOrderLine $orderLineMicrosoft */
            $orderLineMicrosoft = $orderLineRepo->find($orderLine['id']);
            $orderLineMicrosoft->setCustomerRef($orderLine['customerRef']);
            $this->connection->persist($orderLineMicrosoft);
        }

        $this->connection->flush();

        return $orderMicrosoft;
    }

    /**
     * @param Adherent $adherent
     * @param string   $type
     *
     * @return string
     */
    private function getKeys(Adherent $adherent, string $type): string
    {
        return 'order_' . $adherent->getCustomerAccount() . '_' . $type;
    }

    /**
     * @param Adherent $adherent
     * @param string   $type
     *
     * @param string   $monthYear
     *
     * @return string
     */
    private function getMonthKeys(Adherent $adherent, string $type, string $monthYear): string
    {
        return 'order_' . $adherent->getCustomerAccount() . '_' . $type . '_' . $monthYear;
    }

    /**
     * @param SalesOrderHeader $order
     *
     * @return SalesOrderHeader
     */
    private function setOrderStatus(SalesOrderHeader $order): SalesOrderHeader
    {
        $order->setCancellable(OrderStatusEnum::isCancellable($order));
        $order->setRatable(OrderStatusEnum::isRatable($order));
        $order->setFgsable(OrderStatusEnum::isFGSable($order));

        return $order;
    }
}
