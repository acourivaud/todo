<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 19/04/17
 * Time: 21:41
 */

namespace Todotoday\CheckoutBundle\Service;

use Actiane\ApiConnectorBundle\Services\Connections\Microsoft\MicrosoftDynamicsConnection;
use Actiane\ToolsBundle\Enum\LoggerEnum;
use Actiane\ToolsBundle\Traits\LoggerTrait;
use Carbon\Carbon;
use Monolog\Logger;
use Predis\Client;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CartBundle\Entity\CartProduct;
use Todotoday\CatalogBundle\Entity\Api\Microsoft\RetailCatalog;
use Todotoday\CatalogBundle\Services\CatalogManager;
use Todotoday\CheckoutBundle\Entity\Api\Microsoft\SalesOrderHeader;
use Todotoday\CheckoutBundle\Entity\Api\MicrosoftCRM\ServiceAppointment;
use Todotoday\CheckoutBundle\Exception\BookingAgencyCrmSiteMissingException;
use Todotoday\CheckoutBundle\Exception\BookingCantBeCancelledException;
use Todotoday\CheckoutBundle\Exception\BookingCrmIdAdherentMissingException;
use Todotoday\CheckoutBundle\Exception\BookingInvalidProductBookedException;
use Todotoday\CheckoutBundle\Exception\BookingLockedException;
use Todotoday\CheckoutBundle\Exception\BookingNotBelongsToAdherentException;
use Todotoday\CheckoutBundle\Exception\BookingNotFoundException;
use Todotoday\CheckoutBundle\Exception\BookingProductCrmIdServiceMissingException;
use Todotoday\CheckoutBundle\Exception\BookingSlotNotAvailableException;
use Todotoday\CheckoutBundle\Repository\Api\MicrosoftCRM\ServiceAppointmentRepository;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class BookingManager
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 *
 * @package Todotoday\CheckoutBundle\Service
 */
class BookingManager
{
    use LoggerTrait;
    public const LOCK = 'lock';

    /**
     * @var MicrosoftDynamicsConnection
     */
    private $connection;

    /**
     * @var array
     */
    private $rules;

    /**
     * @var CheckoutManager
     */
    private $checkoutManager;

    /**
     * @var CatalogManager
     */
    private $catalogManager;

    /**
     * @var Logger
     */
    private $loggerBooking;

    /**
     * @var string
     */
    private $loggerRef;

    /**
     * @var Client
     */
    private $bookingLock;

    /**
     * BookingManager constructor.
     *
     * @param MicrosoftDynamicsConnection $connection
     * @param CheckoutManager             $checkoutManager
     * @param CatalogManager              $catalogManager
     * @param Logger                      $logger
     * @param Logger                      $loggerBooking
     * @param Client                      $bookingLock
     */
    public function __construct(
        MicrosoftDynamicsConnection $connection,
        CheckoutManager $checkoutManager,
        CatalogManager $catalogManager,
        Logger $logger,
        Logger $loggerBooking,
        Client $bookingLock
    ) {
        $this->connection = $connection;
        $this->checkoutManager = $checkoutManager;
        $this->catalogManager = $catalogManager;
        $this->logger = $logger;
        $this->loggerBooking = $loggerBooking;
        $this->loggerRef = uniqid();
        $this->bookingLock = $bookingLock;
    }

    /**
     * Return all avalaible slot for agency, product id
     *
     * @param Agency $agency
     * @param string $productId
     * @param int    $nextWeek
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Exception
     *
     * @return array|null
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Todotoday\CheckoutBundle\Exception\BookingAgencyCrmSiteMissingException
     * @throws \Todotoday\CheckoutBundle\Exception\BookingProductCrmIdServiceMissingException
     * @throws \Todotoday\CheckoutBundle\Exception\BookingInvalidProductBookedException
     * @throws \Todotoday\CatalogBundle\Exceptions\ProductNotFoundException
     */
    public function getAvailableSlots(Agency $agency, string $productId, int $nextWeek = null): ?array
    {
        if ($nextWeek > 10) {
            return null;
        }

        $product = $this->getProductBookable($agency, $productId);

        $carbonDate = $nextWeek
            ? Carbon::now('UTC')->addWeek($nextWeek)->startOfWeek()
            : Carbon::now('UTC');

        $this->loggerBooking->info(
            '(' . $this->loggerRef . ') [' . $agency->getSlug() . '] - Getting slot available for ' .
            $product->getNameTrans() . ' (' . $product->getServiceLength() / 60 . ' minutes)'
        );

        $startSearch = $this->getFirstDateValidFromStartDate($carbonDate)->format('c');
        $endSearch = $carbonDate->endOfWeek()->format('c');

        return $this->searchProposalsFromMicrosoft(
            $agency->getCrmSite(),
            $product->getCrmIdService(),
            $startSearch,
            $endSearch
        );
    }

    /**
     * @param Agency $agency
     * @param string $productId
     * @param array  $requestedSlot
     *
     * @return bool
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Todotoday\CheckoutBundle\Exception\BookingAgencyCrmSiteMissingException
     * @throws \Todotoday\CheckoutBundle\Exception\BookingProductCrmIdServiceMissingException
     * @throws \Todotoday\CheckoutBundle\Exception\BookingInvalidProductBookedException
     * @throws \Todotoday\CatalogBundle\Exceptions\ProductNotFoundException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function isSlotAvailabe(Agency $agency, string $productId, array $requestedSlot): bool
    {
        $requestedSlot = $this->setSlot($requestedSlot);

        $requestedStart = Carbon::createFromFormat('Y-m-d\TH:i:s\Z', $requestedSlot['Start'], 'UTC');

        if ($requestedStart->lessThan(Carbon::now('UTC'))) {
            return false;
        }

        $validStartDateFromRules = $this->getFirstDateValidFromStartDate($requestedStart);

        $this->loggerBooking->info(
            '(' . $this->loggerRef . ') [' . $agency->getSlug() . '] - Testing if is slot available ' . json_encode(
                $requestedSlot
            )
        );

        // on test si la date demandée est bien supérieure à la première date possible défini par les règles
        if ($requestedStart->lessThan($validStartDateFromRules)) {
            $this->loggerBooking->error(
                '(' . $this->loggerRef . ') [' . $agency->getSlug() . '] - date requested less than first granted'
            );

            return false;
        }

        $agency = $this->getAgency($agency);
        $product = $this->getProductBookable($agency, $productId);
        $requestedSlot = $this->setSlot($requestedSlot);
        if (!$search = $this->searchProposalsFromMicrosoft(
            $agency->getCrmSite(),
            $product->getCrmIdService(),
            $requestedSlot['Start'],
            $requestedSlot['End']
        )
        ) {
            $this->loggerBooking->error(
                '(' . $this->loggerRef . ') [' . $agency->getSlug() . '] - date requested no longer available'
            );

            return false;
        }

        if ($search[0]['Start'] === $requestedSlot['Start']) {
            $this->loggerBooking->info(
                '(' . $this->loggerRef . ') [' . $agency->getSlug() . '] - Slot AVAILABLE !'
            );

            return true;
        }

        return false;
    }

    /**
     * @param Carbon $startDate
     *
     * @return Carbon
     */
    public function getFirstDateValidFromStartDate(Carbon $startDate): Carbon
    {
        $firstDateValid = $startDate->copy();
        $now = Carbon::now('UTC');
        $diffDays = $now->copy()->setTime(0, 0)->diffInDays($startDate->copy()->setTime(0, 0), false);
        foreach ($this->getRules() as $rule) {
            if ($diffDays === $rule['numberDaysBeforeReservation']) {
                $minHour = $now->copy()->setTimeFromTimeString($rule['minHour']);

                if ($now->gte($minHour)) {
                    $noUntilRule = $startDate->copy()->setTimeFromTimeString($rule['noReservationUntil'])
                        ->addHours($rule['deltaHours']);

                    $noUntil = $noUntilRule->gte($now->copy()->addHours($rule['deltaHours']))
                        ? $noUntilRule : $now->copy()->addHours($rule['deltaHours']);

                    if ($noUntil->gt($firstDateValid)) {
                        $firstDateValid = $noUntil;
                    }
                }
            }
        }

        return $firstDateValid;
    }

    /**
     * @return array
     */
    public function getRules(): array
    {
        return $this->rules;
    }

    /**
     * @param array $rules
     *
     * @return BookingManager
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     */
    public function setRules(array $rules): self
    {
        $resolver = new OptionsResolver();
        $this->configureOptionsRules($resolver);
        foreach ($rules as $rulesName => $rule) {
            $this->rules[$rulesName] = $resolver->resolve($rule);
        }

        return $this;
    }

    /**
     * @param Agency     $agency
     * @param Adherent   $adherent
     * @param string     $productId
     * @param array      $slot
     *
     * @param array|null $data
     *
     * @return ServiceAppointment
     * @throws \Todotoday\CheckoutBundle\Exception\BookingLockedException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Todotoday\CheckoutBundle\Exception\CheckoutWithInvalidPaymentMethodException
     * @throws \Todotoday\CheckoutBundle\Exception\BookingAgencyCrmSiteMissingException
     * @throws \Todotoday\CheckoutBundle\Exception\BookingProductCrmIdServiceMissingException
     * @throws \Todotoday\CheckoutBundle\Exception\BookingInvalidProductBookedException
     * @throws \Todotoday\CheckoutBundle\Exception\NoProductToCheckoutException
     * @throws \Todotoday\CheckoutBundle\Exception\CheckoutWithoutPaymentException
     * @throws \Todotoday\CheckoutBundle\Exception\OrderException
     * @throws \UnexpectedValueException
     * @throws \Todotoday\CatalogBundle\Exceptions\ProductNotFoundException
     * @throws \Todotoday\CartBundle\Exception\NotAdherentCartBundleException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \LogicException
     * @throws \Todotoday\CheckoutBundle\Exception\CheckoutWithoutInitializeException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws BookingSlotNotAvailableException
     * @throws BookingCrmIdAdherentMissingException
     */
    public function checkoutBooking(
        Agency $agency,
        Adherent $adherent,
        string $productId,
        array $slot,
        ?array $data = null
    ) {

        $this->loggerBooking->info(
            '(' . $this->loggerRef . ') [' . $agency->getSlug() . '] ' . $adherent->getFullName() . ' ( ' .
            $adherent->getCustomerAccount() .
            ' ) Start checkout Process'
        );

        if (!$adherent->getCrmIdAdherent()) {
            $this->loggerBooking->error(
                '(' . $this->loggerRef . ') [' . $agency->getSlug() . '] ' . $adherent->getFullName() . ' ( ' .
                $adherent->getCustomerAccount() .
                ' ) CRM ID missing'
            );
            throw new BookingCrmIdAdherentMissingException();
        }
        $agency = $this->getAgency($agency);
        $product = $this->getProductBookable($agency, $productId);

        $this->loggerBooking->info(
            '(' . $this->loggerRef . ') [' . $agency->getSlug() . '] ' . $adherent->getFullName() . ' ( ' .
            $adherent->getCustomerAccount() .
            ' ) Check if lock is set'
        );

        if (!$this->isSlotAvailabe($agency, $product->getItemId(), $slot)) {
            throw new BookingSlotNotAvailableException();
        }

        $this->isLocked($adherent, $product, $slot);

        $cartProduct = (new CartProduct())
            ->setAgency($agency)
            ->setAdherent($adherent)
            ->setQuantity(1);
        $mapping = $this->connection->getMapping(RetailCatalog::class);
        $mapping->fillEntityInEntityDoctrine($product, $cartProduct);

        $this->loggerBooking->info(
            '(' . $this->loggerRef . ') [' . $agency->getSlug() . '] ' . $adherent->getFullName() . ' ( ' .
            $adherent->getCustomerAccount() .
            ' ) Checkout Booking initialized'
        );

        $checkout = $this->checkoutManager->initCheckoutProcess($adherent, $agency, null, 'Front', $cartProduct);

        $this->loggerBooking->info(
            '(' . $this->loggerRef . ') [' . $agency->getSlug() . '] ' . $adherent->getFullName() . ' ( ' .
            $adherent->getCustomerAccount() .
            ' ) Process Initialized'
        );

        $salesOrderHeader = $this->checkoutManager->createFullOrder($checkout);

        $this->loggerBooking->info(
            '(' . $this->loggerRef . ') [' . $agency->getSlug() . '] ' . $adherent->getFullName() . ' ( ' .
            $adherent->getCustomerAccount() .
            ' ) Order created ' . $salesOrderHeader->getSalesOrderNumber()
        );

        $serviceAppointment = $this->createServiceAppointment(
            $agency,
            $adherent,
            $product,
            $slot,
            $salesOrderHeader,
            $data
        );

        return $serviceAppointment;
    }

    /**
     * @param Agency $agency
     * @param string $productId
     *
     * @return RetailCatalog
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Todotoday\CheckoutBundle\Exception\BookingAgencyCrmSiteMissingException
     * @throws \Todotoday\CatalogBundle\Exceptions\ProductNotFoundException
     * @throws BookingProductCrmIdServiceMissingException
     * @throws BookingInvalidProductBookedException
     */
    public function getProductBookable(Agency $agency, string $productId): RetailCatalog
    {
        $agency = $this->getAgency($agency);
        $product = $this->catalogManager->getProduct($agency, $productId);
        if (!$product->isBookable()) {
            $this->loggerBooking->error(
                '(' . $this->loggerRef . ') [' . $agency->getSlug() . '] ' . $product->getNameTrans()
                . ' is not bookable'
            );
            throw new BookingInvalidProductBookedException(
                'Item with modelItem ' . $product->getItemModelGroup() . ' can\'t be booked'
            );
        }

        if (!$product->getCrmIdService()) {
            $this->logger->error(
                '(' . $this->loggerRef . ') [' . $agency->getSlug() . '] ' . $product->getItemId()
                . ' has crm_id_service null'
            );

            throw new BookingProductCrmIdServiceMissingException($productId);
        }

        return $product;
    }

    /**
     * Check if string date is correctly formatted for UTC
     *
     * @param string $date
     *
     * @return bool
     */
    public function isDateValid(string $date): bool
    {
        $requestedFormat = 'Y-m-d\TH:i:s\Z';
        $d = \DateTime::createFromFormat($requestedFormat, $date);

        return $d && $d->format($requestedFormat) === $date;
    }

    /**
     * @param Agency $agency
     * @param string $productId
     *
     * @return array|null
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Todotoday\CheckoutBundle\Exception\BookingAgencyCrmSiteMissingException
     * @throws \Todotoday\CheckoutBundle\Exception\BookingProductCrmIdServiceMissingException
     * @throws \Todotoday\CheckoutBundle\Exception\BookingInvalidProductBookedException
     * @throws \Todotoday\CatalogBundle\Exceptions\ProductNotFoundException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Exception
     */
    public function getFirstAvailableSlot(Agency $agency, string $productId): ?array
    {
        $agency = $this->getAgency($agency);
        $product = $this->getProductBookable($agency, $productId);
        $i = 0;
        do {
            $serviceToBook = $this->getAvailableSlots($agency, $product->getItemId(), $i);
            $i++;
        } while (empty($serviceToBook) && $i < 11);

        return $serviceToBook[0];
    }

    /**
     * @param array $slot
     *
     * @return array
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function setSlot(array $slot): array
    {
        $resolver = new OptionsResolver();
        $this->configureOptionsSlot($resolver);

        $slot = $resolver->resolve($slot);
        $slot['ProposalParties'] = $this->setProposal($slot['ProposalParties']);

        return $slot;
    }

    /**
     * Don't use this method to cancel a booking !
     * Use the method cancelOrder in OrderManager and pass the commandeid of the booking
     *
     * @param ServiceAppointment $appointment
     *
     * @return ServiceAppointment
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     * @throws BookingCantBeCancelledException
     */
    public function cancelBooking(ServiceAppointment $appointment): ServiceAppointment
    {
        $appointment->setStatecode(2)
            ->setStatuscode(9);

        $this->connection->persist($appointment)->flush();

        return $appointment;
    }

    /**
     * @param Adherent $adherent
     * @param string   $activityId
     *
     * @return ServiceAppointment
     * @throws \Todotoday\CheckoutBundle\Exception\BookingNotFoundException
     * @throws \Todotoday\CheckoutBundle\Exception\BookingNotBelongsToAdherentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     * @throws \Todotoday\CheckoutBundle\Exception\BookingCantBeCancelledException
     */
    public function cancelBookingWithActivityId(Adherent $adherent, string $activityId): ServiceAppointment
    {
        $appointement = $this->getAppointement($activityId, $adherent);

        return $this->cancelBooking($appointement);
    }

    /**
     * @param Adherent $adherent
     * @param string   $commandeid
     *
     * @return ServiceAppointment
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     * @throws \Todotoday\CheckoutBundle\Exception\BookingCantBeCancelledException
     */
    public function cancelBookingWithCommandeid(Adherent $adherent, string $commandeid): ServiceAppointment
    {
        /** @var ServiceAppointmentRepository $repo */
        $repo = $this->connection->getRepository(ServiceAppointment::class);
        $appointement = $repo->getAppointmentFromAdherentAndCommandeid($adherent, $commandeid);

        return $this->cancelBooking($appointement);
    }

    /**
     * @param string   $activityId
     * @param Adherent $adherent
     *
     * @return ServiceAppointment
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     * @throws BookingNotBelongsToAdherentException
     * @throws BookingNotFoundException
     */
    public function getAppointement(string $activityId, Adherent $adherent): ServiceAppointment
    {
        /** @var ServiceAppointment $appointement */
        if (!preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/', $activityId)
            || !$appointement =
                $this->connection->getRepository(ServiceAppointment::class)
                    ->find(
                        array(
                            'activityid' => $activityId,
                        )
                    )
        ) {
            throw new BookingNotFoundException($activityId);
        }

        if ($appointement->getApsClientsadherentsValue() !== $adherent->getCrmIdAdherent()) {
            throw new BookingNotBelongsToAdherentException($adherent->getCustomerAccount());
        }

        return $appointement;
    }

    /**
     * @param array $proposals
     *
     * @return array
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    private function setProposal(array $proposals): array
    {
        $resolver = new OptionsResolver();
        $this->configureOptionsProposal($resolver);

        foreach ($proposals as &$proposal) {
            $proposal = $resolver->resolve($proposal);
        }

        return $proposals;
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     */
    private function configureOptionsProposal(OptionsResolver $resolver): void
    {
        $resolver->setRequired(
            array(
                'ResourceId',
                'ResourceSpecId',
                'DisplayName',
                'EntityName',
                'EffortRequired',
            )
        )
            ->setAllowedTypes('ResourceId', 'string')
            ->setAllowedTypes('ResourceSpecId', 'string')
            ->setAllowedTypes('DisplayName', 'string')
            ->setAllowedTypes('EntityName', 'string')
            ->setAllowedTypes('EffortRequired', array('float', 'int'));
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     */
    private function configureOptionsSlot(OptionsResolver $resolver): void
    {
        $resolver->setRequired(
            array(
                'Start',
                'End',
                'SiteId',
                'SiteName',
                'ProposalParties',
            )
        )
            ->setAllowedTypes('Start', 'string')
            ->setAllowedTypes('End', 'string')
            ->setAllowedTypes('SiteId', 'string')
            ->setAllowedTypes('SiteName', 'string')
            ->setAllowedTypes('ProposalParties', 'array');
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     */
    private function configureOptionsRules(OptionsResolver $resolver): void
    {
        $resolver->setRequired(
            array(
                'numberDaysBeforeReservation',
                'minHour',
                'noReservationUntil',
                'deltaHours',
            )
        )
            ->setAllowedTypes('numberDaysBeforeReservation', 'int')
            ->setAllowedTypes('minHour', 'string')
            ->setAllowedTypes('noReservationUntil', 'string')
            ->setAllowedTypes('deltaHours', 'int');
    }

    /**
     * Post a service appointment in Microsoft CRM
     *
     * @param Agency           $agency
     * @param Adherent         $adherent
     * @param RetailCatalog    $product
     * @param array            $slot
     * @param SalesOrderHeader $salesOrderHeader
     * @param array|null       $data
     *
     * @return ServiceAppointment
     * @throws \Todotoday\CheckoutBundle\Exception\BookingAgencyCrmSiteMissingException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    private function createServiceAppointment(
        Agency $agency,
        Adherent $adherent,
        RetailCatalog $product,
        array $slot,
        SalesOrderHeader $salesOrderHeader,
        ?array $data
    ): ServiceAppointment {

        $agency = $this->getAgency($agency);

        $start = Carbon::createFromFormat('Y-m-d\TH:i:s\Z', $slot['Start'], 'UTC');
//        $end = Carbon::createFromFormat('Y-m-d\TH:i:s\Z', $slot['End']);
        $end = clone $start;
        $end->addSeconds((int) $product->getServiceLength());

        $diffMinute = $start->diffInMinutes($end);

        $serviceAppointment = new ServiceAppointment();
        $serviceAppointment
            ->setApsRendezvousperiodique(false)
            ->setSiteidValue($slot['SiteId'])
            ->setApsEquipement($slot['ProposalParties'][0]['ResourceId'])
            ->setApsClientsadherents($adherent->getCrmIdAdherent())
            ->setApsOrigine(100000001)
            ->setApsCommandeid($salesOrderHeader->getSalesOrderNumber())
            ->setScheduleddurationminutes($diffMinute)
            ->setServiceidValue($product->getCrmIdService())
            ->setOwnerIdValueGuid($agency->getCrmTeam(), 'teams')
            ->setDescription($data['comment'])
            ->setSubject(
                $adherent->getFirstName() . ' ' . $adherent->getLastName() . ' - ' . $product->getNameTrans()
            )
            ->setScheduledstart($start)
            ->setScheduledend($end);

        $this->loggerBooking->info(
            '(' . $this->loggerRef . ') [' . $agency->getSlug() . '] CREATE APPOINTMENT ' . $adherent->getFullName() .
            ' ' . $product->getLanguageTrans() . ' (' . $product->getServiceLength() / 60 . ' minutes)' . ' for ' .
            $start->format(
                'c'
            ) . ' ending ' .
            $end->format('c')
        );

        $this->connection->persist($serviceAppointment)->flush();

        $this->loggerBooking->info(
            '(' . $this->loggerRef . ') [' . $agency->getSlug() . '] ' . $adherent->getFullName()
            . ' successfully booked ' . $product->getNameTrans() . ' for ' . $start->format('c') . ' ending ' .
            $end->format('c')
        );

        $this->createLock($adherent, $product, $slot);

        return $serviceAppointment;
    }

    /**
     * @param Adherent      $adherent
     * @param RetailCatalog $product
     * @param array         $slot
     */
    private function createLock(Adherent $adherent, RetailCatalog $product, array $slot): void
    {
        $key = $this->getLockedKey($adherent, $product, $slot);
        $this->bookingLock->setex($key, 300, self::LOCK);
        $this->loggerBooking->info(
            '(' . $this->loggerRef . ') ' . $adherent->getFullName() . ' ( ' .
            $adherent->getCustomerAccount() .
            ' ) Lock created'
        );
    }

    /**
     * @param Adherent      $adherent
     * @param RetailCatalog $product
     *
     * @param array         $slot
     *
     * @throws BookingLockedException
     */
    private function isLocked(Adherent $adherent, RetailCatalog $product, array $slot): void
    {
        $key = $this->getLockedKey($adherent, $product, $slot);
        if ($this->bookingLock->get($key) === self::LOCK) {
            $this->loggerBooking->error(
                '(' . $this->loggerRef . ') ' . $adherent->getFullName() . ' ( ' .
                $adherent->getCustomerAccount() .
                ' ) LOCK IS HERE, can\'t booked'
            );
            throw new BookingLockedException($product->getCrmIdService());
        }
    }

    /**
     * @param Adherent      $adherent
     * @param RetailCatalog $product
     *
     * @param array         $slot
     *
     * @return string
     */
    private function getLockedKey(Adherent $adherent, RetailCatalog $product, array $slot): string
    {
        return $adherent->getCustomerAccount() . '_' . $product->getCrmIdService() . '_' . $slot['Start'];
    }

    /**
     * Just throw exception if agency doesn't have crm id service
     *
     * @param Agency $agency
     *
     * @return Agency
     * @throws BookingAgencyCrmSiteMissingException
     */
    private function getAgency(Agency $agency): Agency
    {
        if (!$agency->getCrmSite()) {
            $this->loggerBooking->error(
                '(' . $this->loggerRef . ') [' . $agency->getSlug() . '] Agency CRM Site missing'
            );

            throw new BookingAgencyCrmSiteMissingException($agency->getSlug());
        }

        return $agency;
    }

    /**
     * @param string $crmSite
     * @param string $crmService
     * @param string $startSearch
     * @param string $endSearch
     *
     * @return array
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    private function searchProposalsFromMicrosoft(
        string $crmSite,
        string $crmService,
        string $startSearch,
        string $endSearch
    ): array {
        $token = $this->connection->connect()->getAccessToken();
        $url = $this->connection->getBaseUrl() . '/Search(AppointmentRequest=@p)?@p=';

        $filter = array(
            'Direction' => '0',
            'NumberOfResults' => '1000',
            'SearchWindowStart' => $startSearch,
            'SearchWindowEnd' => $endSearch,
            'ServiceId' => $crmService,
            'Sites' => array($crmSite),
        );

        $url .= rawurlencode(json_encode($filter));
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            array(
                'Content-Type: application/json',
                'Authorization: Bearer ' .
                $token,
            )
        );

        $response = curl_exec($ch);
        $info = curl_getinfo($ch);

        $this->log(
            LoggerEnum::INFO,
            'SERVICE_APPOINTMENT: ' . urldecode($info['url'])
        );

        $this->loggerBooking->info('(' . $this->loggerRef . ') SERVICE_APPOINTMENT: ' . urldecode($info['url']));

        if ($response === false
            || (intdiv($info['http_code'], 100) !== 2)
        ) {
            $this->loggerBooking->error(
                'Curl return error ' . $info['http_code'] . ' for ' . $info['url'] . ' with message ' . $response
            );

            throw new \InvalidArgumentException(
                'Curl return error ' . $info['http_code'] . ' for ' . $info['url'] . ' with message ' . $response
            );
        }

        $results = json_decode($response, true);
        $slots = [];
        foreach ($results['SearchResults']['Proposals'] as $slot) {
            $slots[] = $this->setSlot($slot);
        }

        return $slots;
    }
}
