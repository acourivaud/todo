<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 10/04/17
 * Time: 17:41
 */

namespace Todotoday\CheckoutBundle\Service;

use Actiane\ApiConnectorBundle\Services\Connections\Microsoft\MicrosoftDynamicsConnection;
use Actiane\PaymentBundle\Enum\PaymentTypeEnum;
use Actiane\PaymentBundle\Services\PaymentMethodManager;
use Actiane\PaymentBundle\Services\PaymentMethods\Stripe;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Enum\PaymentStatusEnum;
use Todotoday\AccountBundle\Services\AdherentManager;
use Todotoday\CartBundle\Entity\CartProduct;
use Todotoday\CartBundle\Services\CartProductManager;
use Todotoday\CheckoutBundle\Entity\Api\Microsoft\SalesOrderHeader;
use Todotoday\CheckoutBundle\Entity\Api\Microsoft\SalesOrderLine;
use Todotoday\CheckoutBundle\Entity\Checkout;
use Todotoday\CheckoutBundle\Event\BookingSuccessEvent;
use Todotoday\CheckoutBundle\Event\CheckoutSuccessEvent;
use Todotoday\CheckoutBundle\Exception\CheckoutWithoutInitializeException;
use Todotoday\CheckoutBundle\Exception\CheckoutWithoutMultipleSuppliersException;
use Todotoday\CheckoutBundle\Exception\CheckoutWithoutPaymentException;
use Todotoday\CheckoutBundle\Exception\NoProductToCheckoutException;
use Todotoday\CheckoutBundle\Exception\OrderException;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\PaymentBundle\Entity\CustomerStripeAccount;
use InvalidArgumentException;

/**
 * Class CheckoutManager
 *
 * @package Todotoday\CheckoutBundle\Service
 */
class CheckoutManager
{
    /**
     * @var MicrosoftDynamicsConnection
     */
    private $microsoft;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * @var CartProductManager
     */
    private $cartManager;

    /**
     * @var bool
     */
    private $isCheckoutInitialised = false;

    /**
     * @var bool
     */
    private $isBooking = false;

    /**
     * @var PaymentMethodManager
     */
    private $paymentMethod;

    /**
     * @var AdherentManager
     */
    private $adherentManager;

    /**
     * CheckoutManager constructor.
     *
     * @param MicrosoftDynamicsConnection $microsoft
     * @param CartProductManager          $cartManager
     * @param PaymentMethodManager        $paymentMethod
     * @param EventDispatcherInterface    $dispatcher
     * @param AdherentManager             $adherentManager
     */
    public function __construct(
        MicrosoftDynamicsConnection $microsoft,
        CartProductManager $cartManager,
        PaymentMethodManager $paymentMethod,
        EventDispatcherInterface $dispatcher,
        AdherentManager $adherentManager
    ) {
        $this->microsoft = $microsoft;
        $this->cartManager = $cartManager;
        $this->paymentMethod = $paymentMethod;
        $this->dispatcher = $dispatcher;
        $this->adherentManager = $adherentManager;
    }

    /**
     * @param Agency   $agency
     * @param Checkout $checkout
     * @param array    $commentsProduct
     *
     * @return Checkout
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \UnexpectedValueException
     * @throws \Todotoday\CartBundle\Exception\NotAdherentCartBundleException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function associateProductCommentFromApi(Agency $agency, Checkout $checkout, array $commentsProduct): Checkout
    {
        $resolver = new OptionsResolver();
        $this->configureOptionApiComment($resolver);

        foreach ($commentsProduct as &$commentProduct) {
            $commentProduct = $resolver->resolve($commentProduct);

            if ($product = $this->cartManager->getCartProduct($agency, $commentProduct['id'])) {
                $product->setComment($commentProduct['comment']);
            }
        }

        return $checkout;
    }

    /**
     * @param Agency   $agency
     * @param Checkout $checkout
     * @param array    $deliveriesProduct
     *
     * @return Checkout
     * @throws \UnexpectedValueException
     * @throws \Todotoday\CartBundle\Exception\NotAdherentCartBundleException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function associateProductDeliveryFromApi(
        Agency $agency,
        Checkout $checkout,
        array $deliveriesProduct
    ): Checkout {
        $resolver = new OptionsResolver();
        $this->configureOptionApiDelivery($resolver, $agency);

        foreach ($deliveriesProduct as &$delivery) {
            $delivery = $resolver->resolve($delivery);

            if ($product = $this->cartManager->getCartProduct($agency, $delivery['id'])) {
                $product->setDelivery($delivery['delivery']);
            }
        }

        return $checkout;
    }

    /**
     * @param Checkout         $checkout
     * @param SalesOrderHeader $salesOrderHeader
     *
     * @return SalesOrderLine[]
     * @throws \Todotoday\CheckoutBundle\Exception\OrderException
     */
    public function bindOrderLines(Checkout $checkout, SalesOrderHeader $salesOrderHeader): ?array
    {
        $result = array();
        $cartProducts = $checkout->getCartProducts();
        $salesOrderLine = new SalesOrderLine();
        $salesOrderLine
            ->setDataAreaId($salesOrderHeader->getDataAreaId())
            ->setSalesOrderNumber($salesOrderHeader->getSalesOrderNumber());
        try {
            foreach ($cartProducts as $product) {
                $lines = clone $salesOrderLine;
                $lines->setItemNumber($product->getProduct())
                    ->setOrderedSalesQuantity($product->getQuantity())
                    ->setCustomerRef($product->getComment())
                    ->setSalesPrice(
                        $product->getProductPrice()
                    )
                    ->setDeliveryModeCode($product->getDelivery());
                $this->microsoft->persist($lines);
                $result[] = $lines;
            }
            $this->microsoft->flush();
            if ($this->isBooking) {
                $this->dispatcher->dispatch(BookingSuccessEvent::NAME, new BookingSuccessEvent($checkout));
            } else {
                $this->dispatcher->dispatch(CheckoutSuccessEvent::NAME, new CheckoutSuccessEvent($checkout));
            }

            return $result;
        } catch (\Exception $e) {
            throw new OrderException($e->getMessage());
        }
    }

    /**
     * @param Checkout $checkout
     *
     * @return SalesOrderHeader
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \InvalidArgumentException
     * @throws \Todotoday\CheckoutBundle\Exception\OrderException
     * @throws CheckoutWithoutInitializeException
     */
    public function createFullOrder(Checkout $checkout): SalesOrderHeader
    {
//        $this->chargeCheckoutWithCardToken($checkout);
        if (!$this->isCheckoutInitialised) {
            throw new CheckoutWithoutInitializeException('Checkout hasn\'t been initialized');
        }
        $salesOrderHeader = $this->createOrder($checkout);
        $salesOrderLines = $this->bindOrderLines($checkout, $salesOrderHeader);

        foreach ($salesOrderLines as $salesOrderLine) {
            $salesOrderHeader->addOrderLines($salesOrderLine);
        }

        return $salesOrderHeader;
    }

    /**
     * @param Checkout $checkout
     *
     * @return array
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \InvalidArgumentException
     * @throws CheckoutWithoutInitializeException
     * @throws CheckoutWithoutMultipleSuppliersException
     * @throws OrderException
     */
    public function createFullMultipleOrders(Checkout $checkout): array
    {
        if (!$checkout->hasMultipleSuppliers()) {
            throw new CheckoutWithoutMultipleSuppliersException();
        }

        $ordersCheckouts = array();
        $suppliers = $checkout->getSuppliers();

        foreach ($suppliers as $supplier) {
            $cartProducts = $supplier->getCartProducts();
            $checkout->setCartProduct($cartProducts);
            $checkout->setTotalAmount($this->cartManager->getTotalPriceCartProducts($cartProducts));
            $checkout->setTotalAmountExclTax(10);

            $ordersCheckouts[] = array(
                'checkout' => clone $checkout,
                'order' => $this->createFullOrder($checkout),
            );
        }

        return $ordersCheckouts;
    }

    /**
     * @param Checkout $checkout
     *
     * @return Checkout
     * @throws \UnexpectedValueException
     * @throws \Psr\Log\InvalidArgumentException
     */
    public function chargeCheckoutWithCardToken(Checkout $checkout): Checkout
    {
        if ($checkout->getAdherent()
            && ($paymentType = $checkout->getAdherent()->getPaymentTypeSelectedString())
            === PaymentTypeEnum::STRIPE
        ) {
            if (!$checkout->getCustomerStripeAccount()
                || !($stripeToken = $checkout->getCustomerStripeAccount()->getNewStripeCardToken())
            ) {
                throw new InvalidArgumentException('customerStripeAccount or Stripe token can\'t be null');
            }

            $this->adherentManager->updateEntityBankAccount(null, $checkout->getAdherent());
            /** @var Stripe $paymentMethod */
            $paymentMethod = $this->paymentMethod->getPaymentMethod($paymentType);
            $paymentMethod->chargeWithCardToken($checkout, $stripeToken);
        }

        return $checkout;
    }

    /**
     * @param Checkout    $checkout
     * @param null|string $stripeToken
     *
     * @return Checkout
     */
    public function setCustomerStripeAccount(Checkout $checkout, ?string $stripeToken): Checkout
    {
        if ($stripeToken) {
            $checkout->setCustomerStripeAccount(
                (new CustomerStripeAccount())->setNewStripeCardToken($stripeToken)
            );
        }

        return $checkout;
    }

    /**
     * @param Checkout $checkout
     *
     * @return SalesOrderHeader
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \InvalidArgumentException
     */
    public function createOrder(Checkout $checkout): SalesOrderHeader
    {
        $adherent = $checkout->getAdherent();
        $salesOrderHeader = new SalesOrderHeader();
        $salesOrderHeader
            ->setIdActiane($checkout->getAgency()->getSlug())
            ->setSalesOrderOriginCode($checkout->getSource())
            ->setDataAreaId($adherent->getDataAreaId())
            ->setOrderingCustomerAccountNumber($adherent->getCustomerAccount())
            ->setCurrencyCode($checkout->getAgency()->getCurrency())
            ->setInvoiceCustomerAccountNumber($adherent->getCustomerAccount())
            ->setLanguageId($adherent->getLanguageMicrosoft())
            ->setCustomersOrderReference($checkout->getDeliveryComment())
            ->setDepotModeCode($checkout->getDepository())
            ->setDeliveryModeCode($checkout->getDelivery())
            ->setCustomerRefDepot($checkout->getDepositoryComment())
            ->setStripePaymentId($checkout->getStripeId())
            ->setDefaultShippingSiteId($checkout->getAgency()->getRetailChannelId())
            ->setDefaultShippingWarehouseId($checkout->getAgency()->getRetailChannelId());

        if ($adherent->isCoAdherent()) {
            $salesOrderHeader->setInvoiceCustomerAccountNumber(
                $adherent
                    ->getParent()
                    ->getCustomerAccount()
            );
        }

        $this->microsoft->persist($salesOrderHeader);
        $this->microsoft->flush();

        return $salesOrderHeader;
    }

    /**
     * @param Adherent         $adherent
     * @param Agency           $agency
     * @param null|Checkout    $checkout
     * @param null|string      $source
     * @param null|CartProduct $cartProduct
     *
     * @return Checkout
     * @throws \UnexpectedValueException
     * @throws \Todotoday\CheckoutBundle\Exception\NoProductToCheckoutException
     * @throws \Todotoday\CartBundle\Exception\NotAdherentCartBundleException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     * @throws CheckoutWithoutPaymentException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function initCheckoutProcess(
        Adherent $adherent,
        Agency $agency,
        ?Checkout $checkout = null,
        ?string $source = 'Front',
        ?CartProduct $cartProduct = null
    ): Checkout {
        if (!$checkout) {
            $checkout = new Checkout();
        }

        $checkout
            ->setAdherent($adherent)
            ->setPayment($adherent->getPaymentTypeSelectedString())
            ->setAgency($agency)
            ->setSource($source);

        if (!$cartProduct) {
            $checkout = $this->processCart($checkout);
        } else {
            $checkout = $this->processBooking($checkout, $cartProduct);
        }

        if (!$adherent->getPaymentTypeSelectedString() && !$checkout->getPayment()) {
            throw new CheckoutWithoutPaymentException();
        }

        if (($status = $adherent->getPaymentMethodStatusString()) !== PaymentStatusEnum::VALID) {
            throw new CheckoutWithInvalidPaymentMethodException($status);
        }

        $this->isCheckoutInitialised = true;

        return $checkout;
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     */
    private function configureOptionApiComment(OptionsResolver $resolver): void
    {
        $resolver->setRequired(
            array(
                'id',
                'comment',
            )
        )
            ->setAllowedTypes('id', 'string')
            ->setAllowedTypes('comment', 'string');
    }

    /**
     * @param OptionsResolver $resolver
     * @param Agency          $agency
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     */
    private function configureOptionApiDelivery(OptionsResolver $resolver, Agency $agency): void
    {
        $deliveryMode = $agency->getDeliveryMode();

        $resolver->setRequired(
            array(
                'id',
                'delivery',
            )
        )
            ->setAllowedTypes('id', 'string')
            ->setAllowedTypes('delivery', 'string');

        $resolver->setAllowedValues('delivery', $deliveryMode);
    }

    /**
     * @param Checkout    $checkout
     * @param CartProduct $cartProduct
     *
     * @return Checkout
     * @throws \InvalidArgumentException
     */
    private function processBooking(Checkout $checkout, CartProduct $cartProduct): Checkout
    {
        $checkout->addCartProduct($cartProduct);
        $this->isBooking = true;
        $checkout->setTotalAmount($cartProduct->getProductPrice())
            ->setDelivery('CCG');

        return $checkout;
    }

    /**
     * @param Checkout $checkout
     *
     * @return Checkout
     * @throws \UnexpectedValueException
     * @throws \Todotoday\CartBundle\Exception\NotAdherentCartBundleException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     * @throws NoProductToCheckoutException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    private function processCart(Checkout $checkout): Checkout
    {
        $productsToOrder = $this->cartManager->getCart($checkout->getAgency());

        if (empty($productsToOrder)) {
            throw new NoProductToCheckoutException();
        }

        foreach ($productsToOrder as $product) {
            $checkout->addCartProduct($product);
        }
        $this->isBooking = false;
        $checkout->setTotalAmount($this->cartManager->cartTotalPrice($checkout->getAgency()));
        $checkout->setTotalAmountExclTax($this->cartManager->cartTotalPriceExclTax($checkout->getAgency()));
        $checkout->setDepositoryNeeded($this->cartManager->isCartNeedDepository($checkout->getAgency()));

        return $checkout;
    }
}
