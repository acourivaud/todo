<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 12/01/18
 * Time: 17:29
 *
 * @category   tdtd
 *
 * @package    Todotoday\CheckoutBundle\Service
 *
 * @subpackage Todotoday\CheckoutBundle\Service
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\CheckoutBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Todotoday\CatalogBundle\Services\CatalogManager;
use Todotoday\CheckoutBundle\Entity\CheckoutInfoNeeded;
use Todotoday\CoreBundle\Services\DomainContextManager;

/**
 * Class CheckoutInfoManager
 */
class CheckoutInfoManager
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var DomainContextManager
     */
    private $contextManager;

    /**
     * CheckoutInfoManager constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param DomainContextManager   $contextManager
     */
    public function __construct(EntityManagerInterface $entityManager, DomainContextManager $contextManager)
    {
        $this->entityManager = $entityManager;
        $this->contextManager = $contextManager;
    }

    /**
     *
     * @param string|null $hierarchy
     *
     * @return bool
     * @throws \UnexpectedValueException
     */
    public function hasCheckoutInfo(?string $hierarchy): bool
    {
        if (!$hierarchy) {
            return false;
        }

        return null !== $this->retrieveCheckoutInfo($hierarchy);
    }

    /**
     * @param string|null $hierarchy
     *
     * @return array
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     * @throws \UnexpectedValueException
     */
    public function getCheckoutInfo(?string $hierarchy): array
    {
        if (!$hierarchy || !$infos = $this->retrieveCheckoutInfo($hierarchy)) {
            return [];
        }

        return array_map(
            function ($el) {
                return CheckoutInfoNeeded::PREFIX_TRANSLATION_KEY . $el;
            },
            explode(';;', $infos)
        );
    }

    /**
     * @param string $hierarchy
     *
     * @return string|null
     * @throws \UnexpectedValueException
     */
    private function retrieveCheckoutInfo(string $hierarchy): ?string
    {
        if (!$this->contextManager->getCurrentRequest()
            || !$agency = $this->contextManager->getCurrentContext()->getAgency()) {
            return null;
        }

        $repo = $this->entityManager->getRepository(CheckoutInfoNeeded::class);

        if (!$infos = $repo->getInfoByAgency($agency)) {
            return null;
        }

        $infosNeeded = null;
        foreach ($infos as $info) {
            $family = $info->getProductFamily();
            $pattern = sprintf('/\|\|%s%s/', $family, CatalogManager::FAMILY_DELIMITER);
            preg_match($pattern, $hierarchy, $matches);
            if (!empty($matches)) {
                $infosNeeded = $info->getInfoNeeded();
                break;
            }
        }

        return $infosNeeded;
    }
}
