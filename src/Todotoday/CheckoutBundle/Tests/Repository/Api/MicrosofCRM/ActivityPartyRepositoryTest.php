<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 25/04/17
 * Time: 10:24
 */

namespace Todotoday\CheckoutBundle\Tests\Repository\Api\MicrosoftCRM;

use Actiane\ApiConnectorBundle\Tests\Repository\Api\AbstractRepositoryTestCase;

class ActivityPartyRepositoryTest extends AbstractRepositoryTestCase
{
    /**
     * Do getRepositoryId
     *
     * @return string
     */
    protected function getRepositoryId(): string
    {
        return 'todotoday.checkout.repository.api.activity_party';
    }
}
