<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 05/04/17
 * Time: 13:32
 */

namespace Todotoday\CheckoutBundle\Tests\Repository\Api\Microsoft;

use Actiane\ApiConnectorBundle\Tests\Repository\Api\AbstractRepositoryTestCase;

/**
 * Class SalesOrderLineRepositoryTest
 * @package Todotoday\CheckoutBundle\Tests\Repository\Api\Microsoft
 */
class SalesOrderLineRepositoryTest extends AbstractRepositoryTestCase
{
    /**
     * Do getRepositoryId
     *
     * @return string
     */
    protected function getRepositoryId(): string
    {
        return 'todotoday.checkout.repository.api.sales_order_line';
    }
}
