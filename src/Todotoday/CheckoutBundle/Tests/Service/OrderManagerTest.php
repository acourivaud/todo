<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 13/04/17
 * Time: 17:22
 */

namespace Todotoday\CheckoutBundle\Tests\Service;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAdherentLinkedMicrosoftData;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CartBundle\Entity\CartProduct;
use Todotoday\CatalogBundle\Entity\Api\Microsoft\RetailCatalog;
use Todotoday\CheckoutBundle\Entity\Api\Microsoft\SalesOrderHeader;
use Todotoday\CheckoutBundle\Entity\Api\Microsoft\SalesOrderLine;
use Todotoday\CheckoutBundle\Entity\Api\MicrosoftCRM\ServiceAppointment;
use Todotoday\CheckoutBundle\Entity\Checkout;
use Todotoday\CheckoutBundle\Exception\OrderNotCancellableException;
use Todotoday\CheckoutBundle\Service\CommandManager;
use Todotoday\CheckoutBundle\Service\OrderManager;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyData;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class OrderManagerTest
 * @package Todotoday\CheckoutBundle\Tests\Service
 */
class OrderManagerTest extends WebTestCase
{
    /**
     * @var RetailCatalog
     */
    private $product;

    /**
     * @var Adherent
     */
    private $adherent;

    /**
     * @var Agency
     */
    private $agency;

    /**
     * @var OrderManager
     */
    private $manager;

    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAgencyData::class,
            LoadAdherentLinkedMicrosoftData::class,
        );
    }

    /**
     * Setup method
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->adherent = self::getFRR()->getReference('adherent_linked_arevalta');
        $this->agency = self::getFRR()->getReference('agency_arevalta');
        $this->manager = $this->getContainer()->get('todotoday.order.manager');
        $catalogRepo = $this->getContainer()->get('todotoday.catalog.repository.api.retail_catalog');

        // TODO le premier item est en erreur (item model group etc.)
        $this->product = $catalogRepo->getFirstProduct($this->agency->getSlug(), 'fr')[4];
    }

    /**
     * Testing full process for command and finally checking we get the command in to CommandManager
     * @small
     */
    public function testGetOrder(): void
    {
        $requestStack = $this->getContainer()->get('request_stack');
        $requestStack->push(Request::create('http://' . $this->agency->getSlug() . '.todotoday.local'));
        $this->tokenAs($this->adherent, 'main');

        $checkoutManager = $this->getContainer()->get('todotoday.checkout.manager');
        $cartManager = $this->getContainer()->get('todotoday_cart_product.manager');

        $cartProduct = new CartProduct();
        $cartProduct->setAgency($this->agency)
            ->setAdherent($this->adherent)
            ->setQuantity(1)
            ->setCurrency('EUR')
            ->setProduct($this->product->getItemId());

        $cartManager->addProduct($this->agency, $cartProduct);

        $em = $this->getDoctrine()->getManager();
        $em->persist($cartProduct);
        $em->flush();

        $checkout = new Checkout();
        $checkout
            ->setAdherent($this->adherent)
            ->setAgency($this->agency)
            ->addCartProduct($cartProduct)
            ->setDelivery($this->agency->getDeliveryMode()[0])
            ->setPayment('sepa')
            ->setDepository($this->agency->getDeliveryMode()[0]);

        // testing the order
        $checkout = $checkoutManager->initCheckoutProcess($this->adherent, $this->agency, $checkout);
        $orderFromCommand = $checkoutManager->createFullOrder($checkout);
        $orders = $this->manager->getOrderHeader($this->adherent);
        static::assertInstanceOf(SalesOrderHeader::class, $orders[0]);
        static::assertEquals($orderFromCommand->getSalesOrderNumber(), $orders[0]->getSalesOrderNumber());

        $orderLines = $this->manager->getOrderLines($orders[0]);
        static::assertInstanceOf(SalesOrderLine::class, $orderLines[0]);
        static::assertEquals($this->product->getItemId(), $orderLines[0]->getItemNumber());

//        $this->manager->deleteCache($this->adherent);
        $result = $this->manager->getFullOrder($this->adherent);
        static::assertNotNull($result);
        static::assertInstanceOf(SalesOrderLine::class, $result[0]->getOrderLines()[0]);
//        $this->getContainer()->get('actiane.api_connector.connection.microsoft')->delete($orders[0])->flush();
    }

    /**
     * @small
     */
    public function testGetBookedServices(): void
    {
        $repo = $this->getContainer()->get('todotoday.checkout.repository.api.service_appointment');
        if ($results = $repo->getAppointmentFromAdherent($this->adherent)) {
            static::assertInstanceOf(ServiceAppointment::class, $results[0]);
        }
        static::assertTrue(true);
    }
}
