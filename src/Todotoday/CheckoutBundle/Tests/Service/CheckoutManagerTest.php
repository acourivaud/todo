<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 10/04/17
 * Time: 22:10
 */

namespace Todotoday\CheckoutBundle\Tests\Service;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAccountData;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAdherentLinkedMicrosoftData;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CartBundle\Entity\CartProduct;
use Todotoday\CheckoutBundle\Entity\Api\Microsoft\SalesOrderHeader;
use Todotoday\CheckoutBundle\Entity\Checkout;
use Todotoday\CheckoutBundle\Exception\CheckoutWithoutInitializeException;
use Todotoday\CheckoutBundle\Exception\NoProductToCheckoutException;
use Todotoday\CheckoutBundle\Service\CheckoutManager;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyData;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class CheckoutManagerTest
 * @package Todotoday\CheckoutBundle\Tests\Service
 */
class CheckoutManagerTest extends WebTestCase
{
    /** @var CheckoutManager */
    private $manager;

    /**
     * @var Adherent
     */
    private $adherent;

    /**
     * @var Agency
     */
    private $agency;

    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAccountData::class,
            LoadAdherentLinkedMicrosoftData::class,
            LoadAgencyData::class,
        );
    }

    /**
     * Setup Method
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->manager = $this->getContainer()->get('todotoday.checkout.manager');
    }

    /**
     * @small
     */
    public function testCheckoutWithoutInitlialize()
    {
        $this->expectException(CheckoutWithoutInitializeException::class);
        $this->getMockedStackAndAdherent();
        $checkout = new Checkout();
        $this->manager->createFullOrder($checkout);
    }

    /**
     * @small
     */
    public function testInitializeCheckoutCartEmpty()
    {
        $this->expectException(NoProductToCheckoutException::class);
        $this->getMockedStackAndAdherent();
        $this->manager->initCheckoutProcess($this->adherent, $this->agency);
    }

    /**
     * @runInSeparateProcess
     * @small
     */
    public function testInitializeCheckoutSuccess()
    {
        $this->getMockedStackAndAdherent();

        $productFromCatalog = $this->getContainer()->get('todotoday.catalog.repository.api.retail_catalog')
                                  ->getFirstProduct(
                                      $this->agency->getSlug(),
                                      'fr'
                                  )[3];

        $product = new CartProduct();
        $product
            ->setAdherent($this->adherent)
            ->setAgency($this->agency)
            ->setProduct($productFromCatalog->getItemId())
            ->setQuantity(1)
            ->setCurrency('EUR')
            ->setProductPrice($productFromCatalog->getAmount());

        $em = $this->getDoctrine()->getManager();

        $em->persist($product);
        $em->flush();

        $checkout = $this->manager->initCheckoutProcess($this->adherent, $this->agency);
        $this->assertInstanceOf(Checkout::class, $checkout);
        $this->assertEquals($checkout->getAdherent()->getId(), $this->adherent->getId());
        $this->assertEquals($checkout->getAgency()->getId(), $this->agency->getId());
        $this->assertEquals($checkout->getCartProducts()[0]->getProduct(), $product->getProduct());

        $salesOrderHeader = $this->manager->createOrder($checkout);
        $this->assertInstanceOf(SalesOrderHeader::class, $salesOrderHeader);

        $result = $this->manager->bindOrderLines($checkout, $salesOrderHeader);
        $this->assertNotEmpty($result);
//        $this->getContainer()->get('actiane.api_connector.connection.microsoft')->delete($salesOrderHeader)->flush();
    }

    /**
     * Give stack and adherent
     */
    private function getMockedStackAndAdherent()
    {
        /** @var Adherent $adherent */
        $this->adherent = self::getFRR()->getReference('adherent_linked_arevalta');
        /** @var Agency $agency */
        $this->agency = $this->adherent->getLinkAgencies()->first()->getAgency();

        $requestStack = $this->getContainer()->get('request_stack');
        $requestStack->push(Request::create('http://' . $this->agency->getSlug() . '.todotoday.local'));
        $this->tokenAs($this->adherent, 'main');
    }
}
