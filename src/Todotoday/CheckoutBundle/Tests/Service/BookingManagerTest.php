<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 19/04/17
 * Time: 21:46
 */

namespace Todotoday\CheckoutBundle\Tests\Service;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAdherentLinkedMicrosoftData;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Entity\LinkAccountAgency;
use Todotoday\CatalogBundle\Entity\Api\Microsoft\RetailCatalog;
use Todotoday\CatalogBundle\Exceptions\ProductNotFoundException;
use Todotoday\CheckoutBundle\Entity\Api\MicrosoftCRM\ServiceAppointment;
use Todotoday\CheckoutBundle\Exception\BookingCantBeCancelledException;
use Todotoday\CheckoutBundle\Exception\BookingCrmIdAdherentMissingException;
use Todotoday\CheckoutBundle\Exception\BookingInvalidProductBookedException;
use Todotoday\CheckoutBundle\Exception\BookingNotFoundException;
use Todotoday\CheckoutBundle\Exception\BookingProductCrmIdServiceMissingException;
use Todotoday\CheckoutBundle\Exception\BookingSlotNotAvailableException;
use Todotoday\CheckoutBundle\Service\BookingManager;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyData;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class BookingManagerTest
 * @package Todotoday\CheckoutBundle\Tests\Service
 */
class BookingManagerTest extends WebTestCase
{
    /**
     * @var BookingManager
     */
    private $manager;

    /**
     * @var Agency
     */
    private $agency;

    /**
     * @var  RetailCatalog
     */
    private $product;

    /**
     * @group ignore
     * @return string
     * small
     */
    public function testCheckoutBookingSuccess(): string
    {
//        $connection = $this->getContainer()->get('actiane.api_connector.connection.microsoft');
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent_linked_carrefour-massy');
        $slot = $this->manager->getFirstAvailableSlot($this->agency, $this->product->getItemId());
        $serviceAppointment = $this->manager->checkoutBooking(
            $this->agency,
            $adherent,
            $this->product->getItemId(),
            $slot
        );
        static::assertNotNull($serviceAppointment);
        static::assertNotNull($serviceAppointment->getApsCommandeid());
        static::assertInstanceOf(ServiceAppointment::class, $serviceAppointment);

        return $serviceAppointment->getActivityid();
//        $connection->delete($serviceAppointment)->flush();
    }

    /**
     * @depends testCheckoutBookingSuccess
     * @group   ignore
     * @small
     */
    public function testCancelBooking(): void
    {
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent_linked_carrefour-massy');
        $activityId = $this->testCheckoutBookingSuccess();
        $manager = $this->getContainer()->get('todotoday.checkout.booking.manager');
        $manager->cancelBookingWithActivityId($adherent, $activityId);
    }

    /**
     * @small
     */
    public function testBookingNotFound(): void
    {
        $this->expectException(BookingNotFoundException::class);
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent_linked_carrefour-massy');
        $manager = $this->getContainer()->get('todotoday.checkout.booking.manager');
        $manager->cancelBookingWithActivityId($adherent, 'blabla');
    }

    /**
     * @depends testCheckoutBookingSuccess
     * @small
     */
    public function testBookingFound(): void
    {
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent_linked_carrefour-massy');
        $activityId = $this->testCheckoutBookingSuccess();
        $manager = $this->getContainer()->get('todotoday.checkout.booking.manager');
        $result = $manager->getAppointement($activityId, $adherent);
        static::assertInstanceOf(ServiceAppointment::class, $result);
    }

    /**
     * @group  ignore
     * @small
     */
    public function testCheckoutSameSlotTwice(): void
    {
        $this->expectException(BookingSlotNotAvailableException::class);
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent_linked_carrefour-massy');
        $slot = $this->manager->getFirstAvailableSlot($this->agency, $this->product->getItemId());
        $this->manager->checkoutBooking($this->agency, $adherent, $this->product->getItemId(), $slot);
        // L'api microsoft prend un certain délai avant de retirer le créneau
        sleep(30);
        $this->manager->checkoutBooking($this->agency, $adherent, $this->product->getItemId(), $slot);
    }

    /**
     * @small
     */
    public function testCheckoutWithWrongProduct(): void
    {
        // we load wrong product
        $product = $this->getContainer()
                       ->get('todotoday.catalog.repository.api.retail_catalog')
                       ->getFirstService($this->agency->getSlug())[0];

        $this->expectException(BookingInvalidProductBookedException::class);
        $this->expectExceptionMessage('Item with modelItem ' . $product->getItemModelGroup() . ' can\'t be booked');
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent_linked_carrefour-massy');
        $slot = array('osef');
        $this->manager->checkoutBooking($this->agency, $adherent, $product->getItemId(), $slot);
    }

    /**
     *
     * @small
     */
    public function testCheckoutWithNoCrmIdService(): void
    {
        // we load ServiceRDV product with no CRM ID Service
        $product = $this->getContainer()
                       ->get('todotoday.catalog.repository.api.retail_catalog')
                       ->getServiceRdvWithNoCrmIdService($this->agency->getSlug())[0];
        if (!$product) {
            static::assertTrue(true);

            return;
        }

        $this->expectException(BookingProductCrmIdServiceMissingException::class);
        $this->expectExceptionMessage('Missing crm id service to book this item');
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent_linked_carrefour-massy');
        $slot = array('osef');
        $this->manager->checkoutBooking($this->agency, $adherent, $product->getItemId(), $slot);
    }

    /**
     * @small
     */
    public function testCheckoutWithMissingAdherentIdCrm(): void
    {
        $adherent = new Adherent();
        $linkAgency = (new LinkAccountAgency())
            ->setAgency($this->agency)
            ->setAccount($adherent)
            ->setRoles(array('ROLE_ADHERENT'));
        $adherent->addLinkAgency($linkAgency);
        $this->expectException(BookingCrmIdAdherentMissingException::class);
        $this->expectExceptionMessage('crm_id_adherent is missing');
        /** @var Adherent $adherent */
        $slot = array('osef');
        $this->manager->checkoutBooking($this->agency, $adherent, 'unknowProduct', $slot);
    }

    /**
     * @small
     */
    public function testCheckoutWithUnknownProduct(): void
    {
        $this->expectException(ProductNotFoundException::class);
        $this->expectExceptionMessage('Product not found');
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent_linked_carrefour-massy');
        $slot = array('osef');
        $this->manager->checkoutBooking($this->agency, $adherent, 'unknowProduct', $slot);
    }

    /**
     * @small
     */
    public function testSetSlotFail(): void
    {
        $this->expectException(UndefinedOptionsException::class);
        $slot = array('toto', 'blabla');
        $this->manager->setSlot($slot);
    }

    /**
     * @small
     */
    public function testFailedDateValid(): void
    {
        static::assertFalse($this->manager->isDateValid('blablabla'));
    }

    /**
     * @small
     */
    public function testSuccesDateValid(): void
    {
        $now = (new \DateTime('now'))->format('Y-m-d\TH:i:s\Z');
        static::assertTrue($this->manager->isDateValid($now));
    }

    /**
     * @small
     */
    public function testGetFirstAvailableSLot(): void
    {
        $result = $this->manager->getFirstAvailableSlot($this->agency, $this->product->getItemId());
        $slot = $this->manager->setSlot($result);
        static::assertNotNull($slot);
        static::assertArrayHasKey('Start', $slot);
        static::assertArrayHasKey('End', $slot);
        static::assertArrayHasKey('SiteId', $slot);
        static::assertArrayHasKey('SiteName', $slot);
        static::assertArrayHasKey('ProposalParties', $slot);
        static::assertArrayHasKey('ResourceId', $slot['ProposalParties'][0]);
        static::assertArrayHasKey('ResourceSpecId', $slot['ProposalParties'][0]);
        static::assertArrayHasKey('DisplayName', $slot['ProposalParties'][0]);
        static::assertArrayHasKey('EntityName', $slot['ProposalParties'][0]);
        static::assertArrayHasKey('EffortRequired', $slot['ProposalParties'][0]);
    }

    /**
     * @dataProvider weekProvider
     * @small
     *
     * @param int $week
     */
    public function testGetAvalableSlot(int $week): void
    {
        $week = $week === 0 ? null : $week;
        $result = $this->manager->getAvailableSlots($this->agency, $this->product->getItemId(), $week);
        static::assertNotNull($result);
    }

    /**
     * @small
     */
    public function testAvailableSLotReturnNull(): void
    {
        $result = $this->manager->getAvailableSlots($this->agency, $this->product->getItemId(), 11);
        static::assertNull($result);
    }

    /**
     * @group ignore
     * @small
     */
    public function testTrueIsSlotAvailable(): void
    {
        $slot = $this->manager->getFirstAvailableSlot($this->agency, $this->product->getItemId());
        $result = $this->manager->isSlotAvailabe($this->agency, $this->product->getItemId(), $slot);
        static::assertTrue($result);
    }

    /**
     * @small
     */
    public function testFalseIsSlobAvailable(): void
    {
        $slot = array(
            'Start' => '2017-01-05T16:45:00Z',
            'End' => '2017-01-05T17:00:00Z',
            'SiteId' => '026b5d1d-901b-e711-8103-5065f38b9561',
            'SiteName' => 'Areva',
            'ProposalParties' => array(
                0 => array(
                    'ResourceId' => '41619574-682b-e711-8103-5065f38b05a1',
                    'ResourceSpecId' => '64e13e85-1f2b-e711-8103-5065f38b05a1',
                    'DisplayName' => 'AREVA COIFFURE',
                    'EntityName' => 'equipment',
                    'EffortRequired' => 1,
                ),
            ),
        );
        $result = $this->manager->isSlotAvailabe($this->agency, $this->product->getItemId(), $slot);
        static::assertFalse($result);
    }

    /**
     * @small
     */
    public function testGetRules(): void
    {
        $rules = $this->manager->getRules();
        static::assertNotNull($rules);
        foreach ($rules as $rule) {
            static::assertArrayHasKey('numberDaysBeforeReservation', $rule);
            static::assertArrayHasKey('minHour', $rule);
            static::assertArrayHasKey('noReservationUntil', $rule);
            static::assertArrayHasKey('deltaHours', $rule);
        }
    }

    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAgencyData::class,
            LoadAdherentLinkedMicrosoftData::class,
        );
    }

    /**
     * Setup method
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->agency = self::getFRR()->getReference('agency_carrefour-massy');
        $this->manager = $this->getContainer()->get('todotoday.checkout.booking.manager');
        $this->product = $this->getContainer()
                             ->get('todotoday.catalog.repository.api.retail_catalog')
                             ->getServiceRdv($this->agency->getSlug())[0];
    }

    /**
     * @return array
     */
    public function weekProvider(): array
    {
        $provider = [];
        for ($i = 0; $i < 10; $i++) {
            $provider[$i] = array($i);
        }

        return $provider;
    }
}
