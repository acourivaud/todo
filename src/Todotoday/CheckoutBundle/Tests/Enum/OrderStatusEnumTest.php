<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 15/05/17
 * Time: 22:28
 */

namespace Todotoday\CheckoutBundle\Tests\Enum;

use PHPUnit\Framework\TestCase;
use Todotoday\CheckoutBundle\Entity\Api\Microsoft\SalesOrderHeader;
use Todotoday\CheckoutBundle\Enum\OrderStatusEnum;

/**
 * Class OrderStatusEnumTest
 * @package Todotoday\CheckoutBundle\Tests\Enum
 */
class OrderStatusEnumTest extends TestCase
{
    /**
     * @small
     */
    public function testGetStatusCanceled()
    {
        $order = new SalesOrderHeader();
        $order->setSalesOrderStatus('Canceled');
        $status = OrderStatusEnum::getStatus($order);
        $this->assertEquals(OrderStatusEnum::CANCELED, $status);
    }

    /**
     * @small
     */
    public function testGetStatusConfirmed()
    {
        $order = new SalesOrderHeader();
        $order->setOrderResponsiblePersonnelNumber('blabla')
            ->setDocumentStatus('Confirmation');
        $status = OrderStatusEnum::getStatus($order);
        $this->assertEquals(OrderStatusEnum::CONFIRMED, $status);
    }

    /**
     * @small
     */
    public function testGetStatusInvoced()
    {
        $order = new SalesOrderHeader();
        $order->setOrderResponsiblePersonnelNumber('blabla')
            ->setSalesOrderStatus('Invoiced')
            ->setDeliveryTermsCode('1');
        $status = OrderStatusEnum::getStatus($order);
        $this->assertEquals(OrderStatusEnum::INVOICED, $status);
    }

    /**
     * @small
     */
    public function testGetStatusPending()
    {
        $order = new SalesOrderHeader();
        $order->setOrderResponsiblePersonnelNumber('Front');

        $status = OrderStatusEnum::getStatus($order);
        $this->assertEquals(OrderStatusEnum::PENDING, $status);
    }

    /**
     * @small
     */
    public function testGetStatusRated()
    {
        $order = new SalesOrderHeader();
        $order->setOrderResponsiblePersonnelNumber('blabla')
            ->setDocumentStatus('PackingSlip')
            ->setDeliveryTermsCode('1');
        $status = OrderStatusEnum::getStatus($order);
        $this->assertEquals(OrderStatusEnum::RATED, $status);
    }

    /**
     * @small
     */
    public function testGetStatusReady()
    {
        $order = new SalesOrderHeader();
        $order->setOrderResponsiblePersonnelNumber('blabla')
            ->setDocumentStatus('PackingSlip')
            ->setDeliveryTermsCode('0');
        $status = OrderStatusEnum::getStatus($order);
        $this->assertEquals(OrderStatusEnum::READY, $status);
    }
}
