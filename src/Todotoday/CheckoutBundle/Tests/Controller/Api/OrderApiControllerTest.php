<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 30/04/17
 * Time: 18:14
 */

namespace Todotoday\CheckoutBundle\Tests\Controller\Api;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Actiane\UnitFonctionalTestBundle\Traits\AccessDeniedForNotAdherentTestableTrait;
use Actiane\UnitFonctionalTestBundle\Traits\AccessGrantedForAdherentMicrosoftTestableTrait;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAccountData;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAdherentLinkedMicrosoftData;
use Todotoday\AccountBundle\Entity\Adherent;

/**
 * Class OrderApiControllerTest
 * @package Todotoday\CheckoutBundle\Tests\Controller\Api
 */
class OrderApiControllerTest extends WebTestCase
{
    use AccessDeniedForNotAdherentTestableTrait, AccessGrantedForAdherentMicrosoftTestableTrait;

    /**
     * @dataProvider adherentMicrosoftProvider
     * @small
     *
     * @param string $agencySlug
     * @param string $adherentSlug
     */
    public function testAccessGrantedAdherentGetOrder(string $agencySlug, string $adherentSlug): void
    {
        $url = $this->getUrl('todotoday.checkout.api.order.get_order');
        $this->succesRoute($adherentSlug, 'api', $url, $agencySlug);
    }

    /**
     * @dataProvider adherentMicrosoftProvider
     * @small
     *
     * @param string $agencySlug
     * @param string $adherentSlug
     */
    public function testAccessGrantedAdherentGetOrderHistory(string $agencySlug, string $adherentSlug): void
    {
        $url = $this->getUrl('todotoday.checkout.api.order.get_order_history');
        $this->succesRoute($adherentSlug, 'api', $url, $agencySlug);
    }

    /**
     * @dataProvider adherentMicrosoftProvider
     * @small
     *
     * @param string $agencySlug
     * @param string $adherentSlug
     */
    public function testAccessGrantedAdherentGetBookedServices(string $agencySlug, string $adherentSlug): void
    {
        $url = $this->getUrl('todotoday.checkout.api.order.get_booking');
        $this->succesRoute($adherentSlug, 'api', $url, $agencySlug);
    }

    /**
     * @dataProvider adherentMicrosoftProvider
     * @small
     *
     * @param string $agencySlug
     * @param string $adherentSlug
     */
    public function testAccessGrantedAdherentGetBookedServicesHistory(string $agencySlug, string $adherentSlug): void
    {
        $url = $this->getUrl('todotoday.checkout.api.order.get_booking_history');
        $this->succesRoute($adherentSlug, 'api', $url, $agencySlug);
    }

    /**
     * @small
     * @dataProvider notAdherentProvider
     *
     * @param string $slug
     */
    public function testAccessDeniedGetOrderForNotAdherent(string $slug): void
    {
        $url = $this->getUrl('todotoday.checkout.api.order.get_order');
        $this->accessDeniedForNotAdherent($slug, 'api', $url);
    }

    /**
     * @small
     * @dataProvider notAdherentProvider
     *
     * @param string $slug
     */
    public function testAccessDeniedGetOrderHistoryForNotAdherent(string $slug): void
    {
        $url = $this->getUrl('todotoday.checkout.api.order.get_order_history');
        $this->accessDeniedForNotAdherent($slug, 'api', $url);
    }

    /**
     * @small
     * @dataProvider notAdherentProvider
     *
     * @param string $slug
     */
    public function testAccessDeniedGetBookedServiceForNotAdherent(string $slug): void
    {
        $url = $this->getUrl('todotoday.checkout.api.order.get_booking');
        $this->accessDeniedForNotAdherent($slug, 'api', $url);
    }

    /**
     * @small
     * @dataProvider notAdherentProvider
     *
     * @param string $slug
     */
    public function testAccessDeniedGetBookedServiceHistoryForNotAdherent(string $slug): void
    {
        $url = $this->getUrl('todotoday.checkout.api.order.get_booking_history');
        $this->accessDeniedForNotAdherent($slug, 'api', $url);
    }

    /**
     * @return array
     */
    public function adherentProvider(): array
    {
        $provider = [];
        foreach (LoadAdherentLinkedMicrosoftData::getAdherentsSlug() as $slug) {
            $provider[$slug] = array($slug);
        }

        return $provider;
    }

    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAccountData::class,
            LoadAdherentLinkedMicrosoftData::class,
        );
    }
}
