<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 19/04/17
 * Time: 22:41
 */

namespace Todotoday\CheckoutBundle\Tests\Controller\Api;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Actiane\UnitFonctionalTestBundle\Traits\AccessDeniedForNotAdherentTestableTrait;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAccountData;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAdherentLinkedMicrosoftData;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CatalogBundle\Entity\Api\Microsoft\RetailCatalog;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyData;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class BookingApiControllerTest
 *
 * @package Todotoday\CheckoutBundle\Tests\Controller\Api
 */
class BookingApiControllerTest extends WebTestCase
{
    use AccessDeniedForNotAdherentTestableTrait;

    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAgencyData::class,
            LoadAccountData::class,
            LoadAdherentLinkedMicrosoftData::class,
        );
    }

    /**
     * @return string[]
     */
    public function adherentLinkedMicrosoftProvider(): array
    {
//        $provider = [];
//        foreach (LoadAdherentLinkedMicrosoftData::getAdherentsSlug() as $slug) {
//            $provider[$slug] = array($slug);
//        }
        $provider['adherent_linked_carrefour-massy'] = array('adherent_linked_carrefour-massy');

        return $provider;
    }

    /**
     * @return array
     */
    public function adherentOutOfMicrosoftProvider(): array
    {
        $provider = array();
        foreach (LoadAccountData::getAdherentsSlugs() as $slug) {
            $provider[$slug] = array($slug);
        }

        return $provider;
    }

    /**
     * @dataProvider notAdherentProvider
     * @small
     *
     * @param string $slug
     */
    public function testAccessDeniedForNotAdherentGettingSlot(string $slug): void
    {
        $url = $this->getUrl('todotoday.checkout.api.booking.get_slot');
        $this->accessDeniedForNotAdherent($slug, 'api', $url);
    }

    /**
     * @small
     */
    public function testAccessDeniedForNotAnonymous(): void
    {
        $url = $this->getUrl(
            'todotoday.checkout.api.booking.get_slot',
            array('activityId' => 'toto')
        );
        $client = $this->makeClient();
        $client->request('GET', $url);

        $this->assertStatusCode(401, $client);
    }

    /**
     * Le catalogue retourné par microsoft me semble pas encore complet
     *
     * @group        ignore
     * @small
     * @dataProvider adherentLinkedMicrosoftProvider
     *
     * @param string $slug
     */
    public function testSuccessGetSlot(string $slug): void
    {
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference($slug);
        /** @var Agency $agency */
        $agency = $adherent->getLinkAgencies()->first()->getAgency();
        $bookingManager = $this->getContainer()->get('todotoday.checkout.booking.manager');
        $product = $this->getContainer()
                       ->get('todotoday.catalog.repository.api.retail_catalog')
                       ->getFirstRdv($agency->getSlug())[0];
        static::assertInstanceOf(RetailCatalog::class, $product);
        $this->loginAs($adherent, 'api');
        $url = $this->getUrl('todotoday.checkout.api.booking.get_slot');
        $client = $this->makeClient();
        $client->request(
            'GET',
            $url,
            [],
            [],
            array(
                'HTTP_AGENCY' => $agency->getSlug(),
                'HTTP_PRODUCT' => $product->getItemId(),
            )
        );
        $slot = json_decode($client->getResponse()->getContent(), true)[0];
        $this->assertStatusCode(200, $client);
        $slot = $bookingManager->setSlot($slot);
        static::assertNotNull($slot);
        static::assertArrayHasKey('Start', $slot);
        static::assertArrayHasKey('End', $slot);
        static::assertArrayHasKey('SiteId', $slot);
        static::assertArrayHasKey('SiteName', $slot);
        static::assertArrayHasKey('ProposalParties', $slot);
        static::assertArrayHasKey('ResourceId', $slot['ProposalParties'][0]);
        static::assertArrayHasKey('ResourceSpecId', $slot['ProposalParties'][0]);
        static::assertArrayHasKey('DisplayName', $slot['ProposalParties'][0]);
        static::assertArrayHasKey('EntityName', $slot['ProposalParties'][0]);
        static::assertArrayHasKey('EffortRequired', $slot['ProposalParties'][0]);
    }

    /**
     * @dataProvider adherentOutOfMicrosoftProvider
     * @small
     *
     * @param string $slug
     */
    public function testFailGetSlotForAdherentOutOfMicrosoft(string $slug): void
    {
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference($slug);
        /** @var Agency $agency */
        $agency = $adherent->getLinkAgencies()->first()->getAgency();
        $this->loginAs($adherent, 'api');
        $url = $this->getUrl('todotoday.checkout.api.booking.get_slot');
        $client = $this->makeClient();
        $client->request(
            'GET',
            $url,
            [],
            [],
            array(
                'HTTP_AGENCY' => $agency->getSlug(),
                'HTTP_PRODUCT' => 'toto',
            )
        );
        $response = json_decode($client->getResponse()->getContent(), true);
        $this->assertStatusCode(500, $client);
        static::assertEquals(
            'Agency CRM site is missing [' . $agency->getSlug() . ']',
            $response['error']['exception'][0]['message']
        );
    }

    /**
     * @dataProvider adherentOutOfMicrosoftProvider
     * @small
     *
     * @param string $slug
     */
    public function testGetSlotWithoutProduct(string $slug): void
    {
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference($slug);
        /** @var Agency $agency */
        $agency = $adherent->getLinkAgencies()->first()->getAgency();
        $this->loginAs($adherent, 'api');
        $url = $this->getUrl('todotoday.checkout.api.booking.get_slot');
        $client = $this->makeClient();
        $client->request('GET', $url, [], [], array('HTTP_AGENCY' => $agency->getSlug()));
        $response = $client->getResponse()->getContent();
        $this->assertStatusCode(500, $client);
        static::assertEquals('Product is missing', json_decode($response, true)['error']['exception'][0]['message']);
    }

    /**
     * @dataProvider notAdherentProvider
     * @small
     *
     * @param string $slug
     */
    public function testPostBookingForNotAdherent(string $slug): void
    {
        /** @var Account $adherent */
        $account = self::getFRR()->getReference($slug);
        /** @var Agency $agency */
        $agency = self::getFRR()->getReference('agency_arevalta');
        $this->loginAs($account, 'api');
        $url = $this->getUrl('todotoday.checkout.api.booking.post_booking');
        $client = $this->makeClient();
        $client->request('POST', $url, [], [], array('HTTP_AGENCY' => $agency->getSlug()));
        $this->assertStatusCode(403, $client);
    }

    /**
     * @small
     */
    public function testPostBookingWithNoProduct(): void
    {
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent_linked_arevalta');
        /** @var Agency $agency */
        $agency = self::getFRR()->getReference('agency_arevalta');
        $this->loginAs($adherent, 'api');
        $url = $this->getUrl('todotoday.checkout.api.booking.post_booking');
        $client = $this->makeClient();
        $client->request('POST', $url, [], [], array('HTTP_AGENCY' => $agency->getSlug()));
        $response = $client->getResponse()->getContent();
        $this->assertStatusCode(500, $client);
        static::assertEquals('Product is missing', json_decode($response, true)['error']['exception'][0]['message']);
    }

    /**
     * @group ignore
     * @small
     */
    public function testSuccesPostBooking(): void
    {
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent_linked_carrefour-massy');
        /** @var Agency $agency */
        $agency = self::getFRR()->getReference('agency_carrefour-massy');
        $this->loginAs($adherent, 'api');
        $url = $this->getUrl('todotoday.checkout.api.booking.post_booking');
        $client = $this->makeClient();

        $product = $this->getContainer()->get('todotoday.catalog.repository.api.retail_catalog')
                       ->getServiceRdv($agency->getSlug())[0];
        $slot = $this->getContainer()->get('todotoday.checkout.booking.manager')
            ->getFirstAvailableSlot($agency, $product->getItemId());

        $data = array(
            'slot' => $slot,
            'product' => $product->getItemId(),
        );

        $client->request('POST', $url, $data, [], array('HTTP_AGENCY' => $agency->getSlug()));
        $this->assertStatusCode(200, $client);
    }

    /**
     * @group ignore
     * @small
     */
    public function testFailPostWithoutPaymentMethodBooking(): void
    {
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent_linked_carrefour-massy');
        $adherent->setPaymentTypeSelectedString(null);
        /** @var Agency $agency */
        $agency = self::getFRR()->getReference('agency_carrefour-massy');
        $this->loginAs($adherent, 'api');
        $url = $this->getUrl('todotoday.checkout.api.booking.post_booking');
        $client = $this->makeClient();

        $product = $this->getContainer()->get('todotoday.catalog.repository.api.retail_catalog')
                       ->getServiceRdv($agency->getSlug())[5];
        $slot = $this->getContainer()->get('todotoday.checkout.booking.manager')
            ->getFirstAvailableSlot($agency, $product->getItemId());

        $data = array(
            'slot' => $slot,
            'product' => $product->getItemId(),
        );
        $client->request('POST', $url, $data, [], array('HTTP_AGENCY' => $agency->getSlug()));
        $response = json_decode($client->getResponse()->getContent(), true);
        $this->assertStatusCode(500, $client);
        static::assertEquals('No payment method selected', $response['error']['exception'][0]['message']);
    }

    /**
     * @small
     */
    public function testPostFailedWithWrongSlot(): void
    {
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent_linked_carrefour-massy');
        /** @var Agency $agency */
        $agency = self::getFRR()->getReference('agency_carrefour-massy');
        $this->loginAs($adherent, 'api');
        $url = $this->getUrl('todotoday.checkout.api.booking.post_booking');

        $slot = array('blabla');
        $product = $this->getContainer()->get('todotoday.catalog.repository.api.retail_catalog')
                       ->getServiceRdv($agency->getSlug())[0];

        $client = $this->makeClient();
        $client->request(
            'POST',
            $url,
            array(
                'slot' => $slot,
                'product' => $product->getItemId(),
            ),
            [],
            array(
                'HTTP_AGENCY' => $agency->getSlug(),
            )
        );
        $this->assertStatusCode(500, $client);
    }
}
