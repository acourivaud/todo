<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 17/04/17
 * Time: 16:41
 */

namespace Todotoday\CheckoutBundle\Tests\Controller\Api;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAccountData;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAdherentLinkedMicrosoftData;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CartBundle\Entity\CartProduct;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyData;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class CheckoutApiControllerTest
 * @package Todotoday\CheckoutBundle\Tests\Controller\Api
 */
class CheckoutApiControllerTest extends WebTestCase
{
    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAgencyData::class,
            LoadAccountData::class,
            LoadAdherentLinkedMicrosoftData::class,
        );
    }

    /**
     * @return array
     */
    public function getNotAdherentProvider(): array
    {
        $provider = array(
            'microsoft' => array('microsoft'),
            'noosphere' => array('noosphere'),
            'root' => array('root'),
            'admin' => array('admin'),
        );

        foreach (LoadAccountData::getConciergesSlugs() as $conciergeSlug) {
            $provider[$conciergeSlug] = array($conciergeSlug);
        }

        return $provider;
    }

    /**
     * @small
     */
    public function testCheckoutSuccess(): void
    {
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent_linked_arevalta');
        /** @var Agency $agency */
        $agency = $adherent->getLinkAgencies()->first()->getAgency();

        $productFromCatalog = $this->getContainer()->get('todotoday.catalog.repository.api.retail_catalog')
                                  ->getFirstProduct(
                                      $agency->getSlug(),
                                      'fr'
                                  )[4];

        $product = new CartProduct();
        $product
            ->setAdherent($adherent)
            ->setAgency($agency)
            ->setProduct($productFromCatalog->getItemId())
            ->setQuantity(1)
            ->setCurrency('EUR')
            ->setProductPrice($productFromCatalog->getAmount());

        $em = $this->getDoctrine()->getManager();

        $em->persist($product);
        $em->flush();

        $choice = $agency->getDeliveryMode();

        $this->loginAs($adherent, 'api');
        $url = $this->getUrl('todotoday.checkout.api.checkout.post_checkout');
        $client = $this->makeClient();
        $form = array(
            'depository' => $choice[0],
            'delivery' => $choice[0],
        );

        $client->request('POST', $url, $form, array(), array('HTTP_AGENCY' => $agency->getSlug()));
        $this->assertStatusCode(200, $client);
    }

    /**
     * @dataProvider getNotAdherentProvider
     *
     * @small
     *
     * @param string $slug
     */
    public function testAccessDeniedForNotAdherent(string $slug): void
    {
        /** @var Account $account */
        $account = self::getFRR()->getReference($slug);
        $this->loginAs($account, 'api');
        $url = $this->getUrl('todotoday.checkout.api.checkout.post_checkout');
        $client = $this->makeClient();
        $client->request('POST', $url);
        $this->assertStatusCode(403, $client);
    }

    /**
     * @small
     */
    public function testAccessDeniedForNotLoggedIn(): void
    {
        $url = $this->getUrl('todotoday.checkout.api.checkout.post_checkout');
        $client = $this->makeClient();
        $client->request('POST', $url);
        $this->assertStatusCode(401, $client);
    }

    /**
     * @small
     */
    public function testCheckoutFailWithInccorectDeliveryOrDepositoryValue(): void
    {
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent_linked_arevalta');
        /** @var Agency $agency */
        $agency = $adherent->getLinkAgencies()->first()->getAgency();

        $this->loginAs($adherent, 'api');
        $url = $this->getUrl('todotoday.checkout.api.checkout.post_checkout');
        $client = $this->makeClient();
        $form = array(
            'depository' => 'valeur bidon',
            'delivery' => 'valeur bidon',
        );

        $client->request('POST', $url, $form, array(), array('HTTP_AGENCY' => $agency->getSlug()));
        $this->assertStatusCode(400, $client);
    }

    /**
     * @small
     */
    public function testCheckoutWithEmptyCart(): void
    {
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent_linked_arevalta');
        /** @var Agency $agency */
        $agency = $adherent->getLinkAgencies()->first()->getAgency();
        $this->loginAs($adherent, 'api');
        $url = $this->getUrl('todotoday.checkout.api.checkout.post_checkout');
        $client = $this->makeClient();

        $choice = $agency->getDeliveryMode();

        $form = array(
            'depository' => $choice[0],
            'delivery' => $choice[0],
        );

        $client->request('POST', $url, $form, [], array('HTTP_AGENCY' => $agency->getSlug()));
        $response = json_decode($client->getResponse()->getContent(), true);
        $this->assertStatusCode(500, $client);
        static::assertEquals('There is no product to checkout', $response['error']['exception'][0]['message']);
    }

    /**
     * @small
     */
    public function testInvalidBody(): void
    {
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent_linked_arevalta');
        /** @var Agency $agency */
        $agency = $adherent->getLinkAgencies()->first()->getAgency();
        $this->loginAs($adherent, 'api');
        $url = $this->getUrl('todotoday.checkout.api.checkout.post_checkout');
        $client = $this->makeClient();
        $form = array(
            'depository' => 'blabla',
            'delivery' => 'blabla',
        );

        $client->request('POST', $url, $form, array(), array('HTTP_AGENCY' => $agency->getSlug()));
        $this->assertStatusCode(400, $client);
    }
}
