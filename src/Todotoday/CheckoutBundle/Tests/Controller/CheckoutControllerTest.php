<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 09/04/17
 * Time: 16:48
 */

namespace Todotoday\CheckoutBundle\Tests\Controller;

use Actiane\PaymentBundle\Enum\PaymentTypeEnum;
use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAccountData;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Enum\PaymentStatusEnum;
use Todotoday\CartBundle\Entity\CartProduct;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyData;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class CheckoutControllerTest
 *
 * @package Todotoday\CheckoutBundle\Tests\Controller
 */
class CheckoutControllerTest extends WebTestCase
{
    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAccountData::class,
            LoadAgencyData::class,
        );
    }

    /**
     * @return array
     */
    public function providerAdherent(): array
    {
        $provider = array();
        $adherents = LoadAccountData::getAdherentsSlugs();

        foreach ($adherents as $adherent) {
            $provider[$adherent] = array($adherent);
        }

        return $provider;
    }

    /**
     * @return array
     */
    public function providerNotAdherent(): array
    {
        return array(
            'noosphere' => array('noosphere'),
            'microsoft' => array('microsoft'),
            'admin' => array('admin'),
            'root' => array('root'),
        );
    }

    /**
     * @small
     */
    public function testAccessGrantedForAdherent(): void
    {
        $em = $this->getDoctrine()->getManager();

        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent0');
        $adherent->setPaymentTypeSelectedString(PaymentTypeEnum::STRIPE);
        /** @var Agency $agency */
        $agency = $adherent->getLinkAgencies()->first()->getAgency();

        $product = new CartProduct();
        $em->merge($adherent);
        $product
            ->setAgency($agency)
            ->setAdherent($adherent)
            ->setHierarchy('blabla')
            ->setProduct('blabla')
            ->setCurrency('EUR')
            ->setQuantity(1);
        $em->persist($product);
        $em->flush();

        $this->loginAs($adherent, 'main');
        $client = $this->makeClient();
        $url = 'http://' . $agency->getSlug() . '.todotoday.lol' . $this->getUrl('order_checkout');
        $client->request('GET', $url);
        $this->assertStatusCode(200, $client);
    }

    /**
     * @dataProvider providerAdherent
     * @small
     *
     * @param string $adherentSlug
     */
    public function testAccessDeniedForAdherentWithNoProductInCart(string $adherentSlug): void
    {
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference($adherentSlug);
        /** @var Agency $agency */
        $agency = $adherent->getLinkAgencies()->first()->getAgency();
        $this->loginAs($adherent, 'main');
        $client = $this->makeClient();
        // we empty cart
        $client->request(
            'GET',
            'http://' . $agency->getSlug() . '.todotoday.lol' . $this->getUrl(
                'todotoday_cart_cart_emptycart'
            )
        );
        $url = 'http://' . $agency->getSlug() . '.todotoday.lol' . $this->getUrl('order_checkout');
        $client->request('GET', $url);
        $this->assertStatusCode(500, $client);
    }

    /**
     * @small
     */
    public function testAccessDeniedForAnonymous(): void
    {
        /** @var Agency $agency */
        $agency = self::getFRR()->getReference('agency_demo0');
        $url = 'http://' . $agency->getSlug() . '.todotoday.lol' . $this->getUrl('order_checkout');
        $client = $this->makeClient();
        $client->request('GET', $url);
        $this->assertStatusCode(302, $client);
    }

    /**
     * @dataProvider providerNotAdherent
     * @small
     *
     * @param string $accountSlug
     */
    public function testAccessDeniedForLoggedButNotAdherent(string $accountSlug): void
    {
//        if ($accountSlug === 'admin' || $accountSlug === 'root') {
//            return;
//        }
        /** @var Adherent $adherent */
        $adherent = static::getFRR()->getReference($accountSlug);
        /** @var Agency $agency */
        $agency = static::getFRR()->getReference('agency_demo0');
        $this->loginAs($adherent, 'main');
        $client = $this->makeClient();
        $url = 'http://' . $agency->getSlug() . '.todotoday.lol' . $this->getUrl('order_checkout');
        $client->request('GET', $url);
        $this->assertStatusCode(302, $client);
//        static::assertContains(
//            $this->getUrl('todotoday_account_registration_mandatoryworkflow'),
//            $client->followRedirect()->getUri()
//        );
    }
}
