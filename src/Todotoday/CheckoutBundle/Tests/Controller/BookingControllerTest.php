<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 26/04/17
 * Time: 22:20
 */

namespace Todotoday\CheckoutBundle\Tests\Controller;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAdherentLinkedMicrosoftData;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CatalogBundle\Entity\Api\Microsoft\RetailCatalog;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyData;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class BookingControllerTest
 * @package Todotoday\CheckoutBundle\Tests\Controller
 */
class BookingControllerTest extends WebTestCase
{
    /**
     * @small
     */
    public function testFailedWithProductUnknown(): void
    {
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent_linked_arevalta');
        /** @var Agency $agency */
        $agency = $adherent->getLinkAgencies()->first()->getAgency();
        $this->loginAs($adherent, 'main');
        $url = 'http://' . $agency->getSlug() . '.todotolol.blabla';
        $url .= $this->getUrl('booking_calendar', array('productId' => 'osef'));
        $client = $this->makeClient();
        $client->request('GET', $url);
        $this->assertStatusCode(400, $client);
    }

    /**
     * @dataProvider agencyProvider
     * @small
     *
     * @param string $agencySlug
     */
    public function testAccessDeniedForAnonyous(string $agencySlug): void
    {
        /** @var Agency $agency */
        $agency = self::getFRR()->getReference('agency_' . $agencySlug);
        $url = 'http://' . $agency->getSlug() . '.todotolol.blabla';
        $url .= $this->getUrl('booking_calendar', array('productId' => 'osef'));
        $client = $this->makeClient();
        $client->request('GET', $url);
        $this->assertStatusCode(302, $client);
    }

    /**
     * @small
     */
    public function testBookingWithProductNotRDV(): void
    {
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent_linked_carrefour-massy');
        /** @var Agency $agency */
        $agency = $adherent->getLinkAgencies()->first()->getAgency();
        $service = $this->getContainer()->get('todotoday.catalog.repository.api.retail_catalog')
            ->getFirstService($agency->getSlug(), 'fr');
        $this->loginAs($adherent, 'main');
        $url = 'http://' . $agency->getSlug() . '.todotolol.blabla';
        $url .= $this->getUrl('booking_calendar', array('productId' => $service[0]->getItemId()));
        $client = $this->makeClient();
        $client->request('GET', $url);
        $this->assertStatusCode(404, $client);
    }

    /**
     * @small
     */
    public function testBookingSuccess(): void
    {
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent_linked_carrefour-massy');
        /** @var Agency $agency */
        $agency = $adherent->getLinkAgencies()->first()->getAgency();
        $service = $this->getContainer()->get('todotoday.catalog.repository.api.retail_catalog')
            ->getFirstRdv($agency->getSlug(), 'fr');
        $this->loginAs($adherent, 'main');
        $url = 'http://' . $agency->getSlug() . '.todotolol.blabla';
        $url .= $this->getUrl('booking_calendar', array('productId' => $service[0]->getItemId()));
        $client = $this->makeClient();
        $client->request('GET', $url);
        $this->assertStatusCode(200, $client);
    }

    /**
     * @return array
     */
    public function agencyProvider(): array
    {
        $provider = array();
        foreach (LoadAgencyData::getAgenciesSlug() as $slug) {
            $provider[$slug] = array($slug);
        }

        return $provider;
    }

    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAgencyData::class,
            LoadAdherentLinkedMicrosoftData::class,
        );
    }
}
