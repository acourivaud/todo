<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 12/04/17
 * Time: 22:57
 */

namespace Todotoday\CheckoutBundle\Tests\Controller;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAccountData;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAdherentLinkedMicrosoftData;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyData;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class OrderControllerTest
 * @package Todotoday\CheckoutBundle\Tests\Controller
 */
class OrderControllerTest extends WebTestCase
{
    /**
     * @small
     * @dataProvider providerUrl
     *
     * @param string $url
     */
    public function testAccessGrantedForAdhernet(string $url)
    {
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent_linked_arevalta');
        /** @var Agency $agency */
        $agency = self::getFRR()->getReference('agency_arevalta');
        $this->loginAs($adherent, 'main');
        $client = $this->makeClient();
        $url = 'http://' . $agency->getSlug() . '.todotoday.lol' . $url;
        $client->request('GET', $url);
        $this->assertStatusCode(200, $client);
    }

    /**
     * @dataProvider providerUrl
     * @small
     *
     * @param string $url
     */
    public function testAccessDenidForAnonymous(string $url)
    {
        /** @var Agency $agency */
        $agency = self::getFRR()->getReference('agency_arevalta');
        $url = 'http://' . $agency->getSlug() . 'todotoday.lol' . $url;
        $client = $this->makeClient();
        $client->request('GET', $url);
        $this->assertStatusCode(302, $client);
    }

    /**
     * Provider that give all the url to be tested
     *
     * @return array
     */
    public function providerUrl()
    {
        return array(
            'pending-commands' => array($this->getUrl('order.pending')),
            'history-commands' => array($this->getUrl('order.history')),
            'booked' => array($this->getUrl('order.booked')),
            'booked-history' => array($this->getUrl('order.booked_history')),
        );
    }

    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAgencyData::class,
            LoadAccountData::class,
            LoadAdherentLinkedMicrosoftData::class,
        );
    }
}
