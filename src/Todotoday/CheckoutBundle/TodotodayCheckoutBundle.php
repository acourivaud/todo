<?php declare(strict_types=1);

namespace Todotoday\CheckoutBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class TodotodayCheckoutBundle
 * @package Todotoday\CheckoutBundle
 */
class TodotodayCheckoutBundle extends Bundle
{
}
