<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 17/04/17
 * Time: 14:24
 */

namespace Todotoday\CheckoutBundle\Form\Api;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Todotoday\CheckoutBundle\Entity\Checkout;
use Todotoday\CheckoutBundle\Form\ProductDeliveryType;
use Todotoday\CoreBundle\Interfaces\Services\DomainContext\DomainContextInterface;

/**
 * Class CheckoutApiType
 * @package Todotoday\CheckoutBundle\Form\Api
 */
class CheckoutApiType extends AbstractType
{
    /**
     * @var DomainContextInterface
     */
    private $domainContext;

    /**
     * CheckoutApiType constructor.
     *
     * @param DomainContextInterface $domainContext
     */
    public function __construct(DomainContextInterface $domainContext)
    {
        $this->domainContext = $domainContext;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $choices = [];
        if ($agency = $this->domainContext->getAgency()) {
            $choices = [];
            foreach ($agency->getDeliveryMode() as $mode) {
                $choices[$mode] = $mode;
            }
        }

        $builder
            ->add(
                'delivery',
                ChoiceType::class,
                array(
                    'choices' => $choices,
                    'multiple' => false,
                    'expanded' => true,
                )
            )
            ->add(
                'depository',
                ChoiceType::class,
                array(
                    'choices' => $choices,
                    'label' => 'order.command.choice.depository',
                    'choice_translation_domain' => 'todotoday',
                    'expanded' => true,
                    'multiple' => false,
                )
            )
//            ->add(
//                'cartProducts',
//                CollectionType::class,
//                array(
//                    'label' => false,
//                    'entry_type' => ProductDeliveryType::class,
//                    'entry_options' => array(
//                        'choices' => $choices,
//                    ),
//                )
//            )
            ->add('depositoryComment', TextType::class)
            ->add('deliveryComment', TextType::class);
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            array(
                'data_class' => Checkout::class,
                'csrf_protection' => false,
                'allow_extra_fields' => true,
            )
        );
    }

    /**
     * @return string
     */
    public function getBlockPrefix(): string
    {
        return '';
    }
}
