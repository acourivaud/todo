<?php declare(strict_types=1);

namespace Todotoday\CheckoutBundle\Form;

use Actiane\PaymentBundle\Enum\PaymentTypeEnum;
use Craue\FormFlowBundle\Form\FormFlow;
use Craue\FormFlowBundle\Form\FormFlowInterface;
use Todotoday\CartBundle\Services\CartProductManager;
use Todotoday\CheckoutBundle\Entity\Checkout;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Interfaces\Services\DomainContext\DomainContextInterface;

/**
 * Class CheckoutFlow
 * @SuppressWarnings(PHPMD.UnusedLocalVariable)
 *
 * @package Todotoday\CheckoutBundle\Form
 */
class CheckoutFlow extends FormFlow
{
    /**
     * @var CartProductManager
     */
    protected $cartProductManager;

    /**
     * @var DomainContextInterface
     */
    protected $context;

    /**
     * CheckoutFlow constructor.
     *
     * @param DomainContextInterface $context
     * @param CartProductManager     $cartProductManager
     */
    public function __construct(DomainContextInterface $context, CartProductManager $cartProductManager)
    {
        $this->cartProductManager = $cartProductManager;
        $this->context = $context;
        $this->allowDynamicStepNavigation = true;
        $this->allowRedirectAfterSubmit = true;
    }

    /**
     * {@inheritdoc}
     * @throws \InvalidArgumentException
     * @throws \LogicException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \Todotoday\CartBundle\Exception\NotAdherentCartBundleException
     * @throws \UnexpectedValueException
     */
    protected function loadStepsConfig(): array
    {
        $steps = array(
            array(
                'label' => 'cart.ariane_comments',
                'form_type' => CheckoutCommentType::class,
            ),
            array(
                'label' => 'cart.ariane_delivery',
                'form_type' => CheckoutDeliveryType::class,
            ),
            array(
                'label' => 'cart.ariane_payment_method',
                'form_type' => CheckoutStripe::class,
                'form_options' => array(
                    'flow' => array(
                        'enable' => true,
                        'formFlowName' => $this->getName(),
                        'step' => 3,
                    ),
                    'stripe_class' => 'column one-half stripe-form-center',
                ),
                'skip' => function (int $estimatedCurrentStepNumber, FormFlowInterface $flow) {
                    /** @var Checkout $checkout */
                    $checkout = $flow->getFormData();
                    $account = $checkout->getAdherent();

                    return ($account->getPaymentTypeSelectedString() === PaymentTypeEnum::STRIPE_DIRECT
                        || $account->getPaymentTypeSelectedString() === PaymentTypeEnum::STRIPE
                        || $account->getPaymentTypeSelectedString() === PaymentTypeEnum::SEPA
                        || $account->getPaymentTypeSelectedString() === PaymentTypeEnum::PAYPAL
                        || $account->getPaymentTypeSelectedString() === PaymentTypeEnum::CHEQUE
                        || $account->getPaymentTypeSelectedString() === PaymentTypeEnum::VIREMENT
                        || $account->getPaymentTypeSelectedString() === PaymentTypeEnum::POSTFINANCE
                    );
                },
            ),
            array(
                'label' => 'cart.ariane_validation',
            ),

        );

        /** @var Agency $agency */
        $agency = $this->context->getAgency();

        if ($this->cartProductManager->isCartNeedDepository($agency)) {
            array_splice(
                $steps,
                1,
                0,
                [
                    [
                        'label' => 'cart.ariane_deposit',
                        'form_type' => CheckoutDepositoryType::class,
                    ],
                ]
            );
            /*//todo : PARTIE AU CAS OÙ IL FAUT AFFICHER AUTANT DE CASIER QU'IL Y A DE FOURNISSEURS
            $suppliers = $this->cartProductManager->getAllSuppliers($agency);

            if ($agency->isDigital() && (\count($suppliers) > 1)) {
                array_unshift(
                    $steps,
                    array(
                        'label' => 'cart.ariane_deposit',
                        'form_type' => CheckoutMultipleDepositoryType::class,
                    )
                );
            } else {
                array_unshift(
                    $steps,
                    array(
                        'label' => 'cart.ariane_deposit',
                        'form_type' => CheckoutDepositoryType::class,
                    )
                );
            }*/
        }

        return $steps;
    }
}
