<?php declare(strict_types=1);

namespace Todotoday\CheckoutBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Todotoday\CheckoutBundle\Entity\Checkout;

/**
 * Class CheckoutPayment
 * @package Todotoday\CheckoutBundle\Form
 */
class CheckoutPayment extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'payment',
            ChoiceType::class,
            array(
                'choice_translation_domain' => 'todotoday',
                'label' => 'account.paiement_methods',
                'expanded' => true,
                'multiple' => false,
                'choices' => array(
                    '[afficher le moyen de paiement par défaut]' => 'default',
                    'checkout.method_payment' => 'checkout.method_payment',
                ),
                'translation_domain' => 'todotoday',
            )
        );
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => Checkout::class,
            )
        );
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'todotoday_checkout_bundle_checkout_payment';
    }
}
