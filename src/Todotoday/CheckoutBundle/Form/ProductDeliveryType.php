<?php declare(strict_types=1);

namespace Todotoday\CheckoutBundle\Form;

use Actiane\ToolsBundle\Services\AssetManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Todotoday\CartBundle\Entity\CartProduct;
use Todotoday\CoreBundle\Interfaces\Services\DomainContext\DomainContextInterface;

/**
 * Class CheckoutStep2
 *
 * @package Todotoday\CheckoutBundle\Form
 */
class ProductDeliveryType extends AbstractType
{

    /**
     * @var DomainContextInterface
     */
    private $context;

    /**
     * CheckoutDepositoryType constructor.
     *
     * @param AssetManager           $assetManager
     * @param DomainContextInterface $context
     */
    public function __construct(AssetManager $assetManager, DomainContextInterface $context)
    {
        $this->context = $context;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if (!($choices = $options['choices']) && ($agency = $this->context->getAgency())) {
            foreach ($agency->getDeliveryMode() as $deliveryChoice) {
                $choices['checkout.delivery_' . $deliveryChoice] = $deliveryChoice;
            }
        }

        $builder
            ->add(
                'delivery',
                ProductChoiceDeliveryType::class,
                array(
                    'choices' => $choices,
                    'multiple' => false,
                    'expanded' => false,
                    'horizontal' => true,
                    'required' => true,
                )
            );
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            array(
                'data_class' => CartProduct::class,
                'label' => false,
                'choices' => array(),
                'required' => false,
                'translation_domain' => 'todotoday',
            )
        );
    }

    /**
     * @param FormView      $view
     * @param FormInterface $form
     * @param array         $options
     */
    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        $view->vars['displayPriceExclTaxes'] = false;

        if ($agency = $this->context->getAgency()) {
            $view->vars['displayPriceExclTaxes'] =  $agency->getDisplayPriceExclTaxes();
        }
    }
}
