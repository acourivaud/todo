<?php declare(strict_types = 1);

namespace Todotoday\CheckoutBundle\Form;

use Actiane\PaymentBundle\Form\StripeType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Todotoday\CheckoutBundle\Entity\Checkout;

/**
 * @package Todotoday\CheckoutBundle\Form
 */
class CheckoutStripe extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'customerStripeAccount',
            StripeType::class,
            array(
                'fields_required' => true,
                'flow' => $options['flow'],
                'stripe_class' => $options['stripe_class'],
                'card_save_message_success' => $options['card_save_message_success'],
            )
        );
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            array(
                'data_class' => Checkout::class,
                'flow' => array(
                    'enable' => false,
                    'formFlowName' => '',
                    'step' => 0,
                ),
                'label' => 'cart.ariane_payment_method',
                'translation_domain' => 'todotoday',
                'stripe_class' => '',
                'card_save_message_success' => 'stripe.checkout.card_save_success',
            )
        );
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'todotoday_checkout_bundle_checkout_stripe';
    }
}
