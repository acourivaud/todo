<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 30/05/2017
 * Time: 12:21
 */

namespace Todotoday\CheckoutBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * Class ProductChoiceDeliveryType
 *
 * @category   Todo-Todev
 * @package    Todotoday\CheckoutBundle\Form
 * @subpackage Todotoday\CheckoutBundle\Form
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class ProductChoiceDeliveryType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function getParent(): string
    {
        return ChoiceType::class;
    }
}
