<?php declare(strict_types=1);

namespace Todotoday\CheckoutBundle\Form;

use Actiane\ToolsBundle\Services\AssetManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Todotoday\CheckoutBundle\Entity\Checkout;
use Todotoday\CoreBundle\Interfaces\Services\DomainContext\DomainContextInterface;

/**
 * Class CheckoutMultipleDepositoryType
 * @package Todotoday\CheckoutBundle\Form
 */
class CheckoutMultipleDepositoryType extends AbstractType
{
    /**
     * @var AssetManager
     */
    private $assetManager;

    /**
     * @var DomainContextInterface
     */
    private $context;

    /**
     * CheckoutMultipleDepositoryType constructor.
     *
     * @param AssetManager           $assetManager
     * @param DomainContextInterface $context
     */
    public function __construct(AssetManager $assetManager, DomainContextInterface $context)
    {
        $this->assetManager = $assetManager;
        $this->context = $context;
//        $this->assetManager->addJavascript('build/depository_case.js');
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $choices = [];
        foreach ($this->context->getAgency()->getDeliveryMode() as $deliveryChoice) {
            $choices['checkout.delivery_' . $deliveryChoice] = $deliveryChoice;
        }

        $builder->add(
            'depositories',
            CollectionType::class,
            array(
                'label' => false,
                'entry_type' => ChoiceType::class,
                'entry_options' => array(
                    'choices' => $choices,
                    'label' => 'cart.checkout_depot_subtitle',
                    'translation_domain' => 'todotoday',
                    'expanded' => true,
                    'multiple' => false,
                ),
            )
        )
            ->add(
                'depositoriesComment',
                CollectionType::class,
                array(
                    'label' => false,
                    'entry_type' => TextType::class,
                    'entry_options' => array(
                        'required' => false,
                        'label' => false,
                        'translation_domain' => 'todotoday',
                    ),
                )
            );
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => Checkout::class,
                'label' => false,
                'translation_domain' => 'todotoday',
            )
        );
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'todotoday_checkout_bundle_checkout_depository';
    }
}
