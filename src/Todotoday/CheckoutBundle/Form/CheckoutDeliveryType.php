<?php declare(strict_types=1);

namespace Todotoday\CheckoutBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Todotoday\CheckoutBundle\Entity\Checkout;
use Todotoday\CoreBundle\Interfaces\Services\DomainContext\DomainContextInterface;

/**
 * Class CheckoutStep2
 *
 * @package Todotoday\CheckoutBundle\Form
 */
class CheckoutDeliveryType extends AbstractType
{
    /**
     * @var DomainContextInterface
     */
    private $context;

    /**
     * CheckoutDeliveryType constructor.
     *
     * @param DomainContextInterface $context
     */
    public function __construct(DomainContextInterface $context)
    {
        $this->context = $context;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $choices = [];
        if ($agency = $this->context->getAgency()) {
            foreach ($agency->getDeliveryMode() as $deliveryChoice) {
                $choices['checkout.delivery_' . $deliveryChoice] = $deliveryChoice;
            }
        }

        $builder
            ->add(
                'delivery',
                ChoiceType::class,
                array(
                    'choices' => $choices,
                    'multiple' => false,
                    'required' => true,
                    'expanded' => false,
                    'horizontal' => true,
                    'attr' => array('v-model' => 'taskform'),
                    'label' => 'cart.checkout_delivery_subtitle',
                    'translation_domain' => 'todotoday',
                )
            )
            ->add(
                'cartProducts',
                CollectionType::class,
                array(
                    'label' => false,
                    'entry_type' => ProductDeliveryType::class,
                    'required' => true,
                    'entry_options' => array(
                        'choices' => $choices,
                        'required' => true,
                    ),

                )
            )
            ->add(
                'deliveryComment',
                TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'cart.delivery_comment',
                    'translation_domain' => 'todotoday',
                )
            );
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            array(
                'data_class' => Checkout::class,
                'label' => ' ',
                'translation_domain' => 'todotoday',
            )
        );
    }
}
