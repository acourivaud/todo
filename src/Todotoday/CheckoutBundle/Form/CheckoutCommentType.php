<?php declare(strict_types=1);

namespace Todotoday\CheckoutBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Todotoday\CheckoutBundle\Entity\Checkout;

/**
 * Class CheckoutCommentType
 *
 * @package Todotoday\CheckoutBundle\Form
 */
class CheckoutCommentType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add(
            'cartProducts',
            CollectionType::class,
            array(
                'entry_type' => CartProductCommentType::class,
                'entry_options' => [
                    'label' => false,
                ],
                'label' => false,
                'translation_domain' => 'todotoday',
            )
        );
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            array(
                'data_class' => Checkout::class,
                'label' => 'booking.comment_to_concierge',
                'translation_domain' => 'todotoday',
            )
        );
    }

    /**
     * @return string
     */
    public function getBlockPrefix(): string
    {
        return 'todotoday_checkout_bundle_checkout_comment';
    }
}
