<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 01/05/17
 * Time: 17:34
 */

namespace Todotoday\CheckoutBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Todotoday\CartBundle\Entity\CartProduct;
use Todotoday\CheckoutBundle\Service\CheckoutInfoManager;

/**
 * Class CartProductCommentType
 *
 * @package Todotoday\CheckoutBundle\Form
 */
class CartProductCommentType extends AbstractType
{
    /**
     * @var CheckoutInfoManager
     */
    private $checkoutInfoManager;

    /**
     * CartProductCommentType constructor.
     *
     * @param CheckoutInfoManager $checkoutInfoManager
     */
    public function __construct(CheckoutInfoManager $checkoutInfoManager)
    {
        $this->checkoutInfoManager = $checkoutInfoManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @throws \Symfony\Component\Form\Exception\UnexpectedTypeException
     * @throws \Symfony\Component\Form\Exception\LogicException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \UnexpectedValueException
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->addEventListener(
                FormEvents::POST_SET_DATA,
                function (FormEvent $event) {
                    /** @var CartProduct $cartProduct */
                    $cartProduct = $event->getData();
                    $form = $event->getForm();
                    $form->add(
                        'comment',
                        TextareaType::class,
                        array(
                            'label' => false,
                            'attr' => array(
                                'placeholder' => 'checkout.placeholder_comment',
                            ),
                            'translation_domain' => 'todotoday',
                            'required' => $this->checkoutInfoManager->hasCheckoutInfo($cartProduct->getHierarchy()),
                        )
                    );
                }
            );
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            array(
                'data_class' => CartProduct::class,
            )
        );
    }
}
