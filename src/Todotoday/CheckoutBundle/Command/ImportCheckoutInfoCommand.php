<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 27/01/2017
 * Time: 11:52
 */

namespace Todotoday\CheckoutBundle\Command;

use Actiane\ToolsBundle\Traits\OutputFormatLoggerTrait;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Exception as SFDException;
use Todotoday\CheckoutBundle\Entity\CheckoutInfoNeeded;
use Todotoday\CheckoutBundle\Entity\DeliveryDelay;
use Todotoday\CoreBundle\Repository\AgencyRepository;

/**
 * Class ImportCheckoutInfoCommand
 *
 * @category   Todo-Todev
 * @package    Todotoday\AccountBundle\Command
 * @subpackage Todotoday\AccountBundle\Command
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class ImportCheckoutInfoCommand extends ContainerAwareCommand
{
    use OutputFormatLoggerTrait;

    /**
     * {@inheritdoc}
     * @throws InvalidArgumentException
     */
    protected function configure(): void
    {
        $this->setName('tdtd:checkout:import-info')
            ->setDescription('CheckoutInfoNeeded and DeliveryDelay import');
    }

    /**
     * {@inheritdoc}
     * @throws InvalidArgumentException
     * @throws \LogicException
     * @throws SFDException\ServiceCircularReferenceException
     * @throws SFDException\ServiceNotFoundException
     * @throws \InvalidArgumentException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \RuntimeException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $retailChannelAgencies = array('EPA041', 'EPA010', 'EPA029', 'EPA057', 'EPA302');
        $productFamilies = array('102', '103', '104', '105', '117');
        $infoNeeded = 'marque;;taille;;couleur;;specificites';

        /** @var AgencyRepository $agencyRepository */
        $agencyRepository = $this->getContainer()->get('todotoday_core.repository.agency_repository');
        $em = $this->getContainer()->get('doctrine')->getManager();

        foreach ($retailChannelAgencies as $retailChannel) {
            $agency = $agencyRepository->findOneBy(array('retailChannelId' => $retailChannel));

            if (!$agency) {
                $output->writeln('[WARNING] - The retailChannelId "' . $retailChannel . '" does not exist !');
                continue;
            }

            foreach ($productFamilies as $productFamily) {
                $checkoutInfo = new CheckoutInfoNeeded();
                $checkoutInfo->setAgency($agency)
                    ->setProductFamily($productFamily)
                    ->setInfoNeeded($infoNeeded);

                $deliveryDelay = new DeliveryDelay();
                $delay = ($productFamily === '103') ? 48 : 72;
                $deliveryDelay->setProductFamily($productFamily)
                    ->setAgency($agency)
                    ->setDelay($delay);

                $em->persist($checkoutInfo);
                $em->persist($deliveryDelay);
            }
        }

        $em->flush();
        $output->writeln('CheckoutInfo and DeliveryDelay are successfully imported !');
    }
}
