<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 08/06/17
 * Time: 11:06
 */

namespace Todotoday\CheckoutBundle\Enum;

use Actiane\ToolsBundle\Enum\AbstractEnum;

/**
 * Class OrderSatisfactionSubjectEnum
 * @package Todotoday\CheckoutBundle\Enum
 */
class OrderSatisfactionSubjectEnum extends AbstractEnum
{
    const CONCIERGE = 100000000;
    const PRESTATION_QUALITY = 100000001;
    const TREATMENT_DELAY = 100000002;
    const AGENCY_AMBIANCE = 100000003;

    /**
     * @return array
     */
    public static function getChoices(): array
    {
        return array(
            'order.satisfaction_subject.concierge' => self::CONCIERGE,
            'order.satisfaction_subject.prestation_quality' => self::PRESTATION_QUALITY,
            'order.satisfaction_subject.treament_delay' => self::TREATMENT_DELAY,
            'order.satisfaction_subject.agency_ambience' => self::AGENCY_AMBIANCE,
        );
    }

    /**
     * @return array
     */
    public static function getChoicesForFront(): array
    {
        $choices = array();

        foreach (static::getChoices() as $key => $choice) {
            $choices[] = array(
                'text' => $key,
                'value' => $choice,
            );
        }

        return $choices;
    }
}
