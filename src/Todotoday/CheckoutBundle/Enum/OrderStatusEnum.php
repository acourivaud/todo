<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 15/05/17
 * Time: 22:17
 */

namespace Todotoday\CheckoutBundle\Enum;

use Actiane\ToolsBundle\Enum\AbstractEnum;
use Todotoday\CheckoutBundle\Entity\Api\Microsoft\SalesOrderHeader;

/**
 * @SuppressWarnings(PHPMD)
 * Class OrderStatusEnum
 *
 * @package Todotoday\CheckoutBundle\Enum
 */
class OrderStatusEnum extends AbstractEnum
{
    // commande adhérent ok mais en attente traitement back
    public const PENDING = 'pending';
    // commande pris en compte côté back
    public const CONFIRMED = 'confirmed';
    // commande à récupérer en CCGIE (ou annexe). Signature + rating au desk
    public const READY = 'ready';
    // commande à récupérer dans casier (ou autre). Signature + rating sur le front
    public const AWAITING = 'awaiting';
    // commande recetté + signature + rating
    public const RATED = 'rated';
    // commande facturé
    public const INVOICED = 'invoiced';
    // si on affiche ça c'est qu'on à un cas qui n'est pas pris en compte
    public const UNKNOWN = 'unknown';
    // status de la commande annulée
    public const CANCELED = 'canceled';
    // status du fournisseur rejetée
    public const SUPPLIER_REJECTED = 'Rejected';
    // status du fournisseur confirmée
    public const SUPPLIER_CONFIRMED = 'Confirmed';
    // status du fournisseur pending
    public const SUPPLIER_PENDING = 'Pending';

    /**
     * @param SalesOrderHeader $order
     *
     * @return string
     */
    public static function getStatus(SalesOrderHeader $order): ?string
    {
        $responsible = $order->getOrderResponsiblePersonnelNumber();
        $doc = $order->getDocumentStatus();
        $delivery = $order->getDeliveryModeCode();
        $mark = $order->getDeliveryTermsCode();
        $status = $order->getSalesOrderStatus();

        if ($status === 'Canceled') {
            return self::CANCELED;
        }
        if ($status === 'Invoiced') {
            return self::INVOICED;
        }

        if ($responsible === 'Front') {
            return self::PENDING;
        }

        if ($delivery !== 'devis'
            && $delivery !== 'precommand'
            && $doc === 'Confirmation'
        ) {
            return self::CONFIRMED;
        }

        if ('0' === $mark || null === $mark) {
            return self::READY;
        }

//        if ('-1' === $mark) {
//            return self::AWAITING;
//        }

        if ('' !== $mark && null !== $mark) {
            return self::RATED;
        }

        return self::UNKNOWN;
    }

    /**
     * @param SalesOrderHeader $order
     *
     * @return bool
     */
    public static function isRatable(SalesOrderHeader $order): bool
    {
        $mark = $order->getDeliveryTermsCode();

        return $order->getSalesOrderStatus() === 'Delivered'
            && ($mark === null || $mark === '' || $mark === '0' || $mark === '-1');
    }

    /**
     * @param SalesOrderHeader $order
     *
     * @return bool
     */
    public static function isCancellable(SalesOrderHeader $order): bool
    {
        return ($order->getOrderResponsiblePersonnelNumber() === 'Front'
            && static::getStatus($order) !== self::CANCELED
            && ($order->getSupplierOrder()
                && $order->getSupplierOrder()->getResponseState() !== self::SUPPLIER_CONFIRMED)
        );
    }

    /**
     * @param SalesOrderHeader $order
     *
     * @return bool
     */
    public static function isFGSable(SalesOrderHeader $order): bool
    {
        $fgsable = array(
            self::INVOICED,
            self::RATED,
        );

        $status = static::getStatus($order);

        return \in_array($status, $fgsable, true) || $order->getSalesOrderStatus() === 'Delivered';
    }

    /**
     * Return prefix that handle tranlsation
     *
     * @return string
     */
    public static function getTranslationPrefix(): string
    {
        return 'order.status.';
    }

    /**
     * @return string
     */
    public static function getFilterPending(): string
    {
        $filter = ' and (SalesOrderStatus ne Microsoft.Dynamics.DataEntities.SalesStatus\'Invoiced\'';
        $filter .= ' and SalesOrderStatus ne Microsoft.Dynamics.DataEntities.SalesStatus\'Canceled\')';

        return $filter;
    }

    /**
     * @return string
     */
    public static function getFilterHistory(): string
    {
        $filter = ' and (SalesOrderStatus eq Microsoft.Dynamics.DataEntities.SalesStatus\'Invoiced\'';
        $filter .= ' or SalesOrderStatus eq Microsoft.Dynamics.DataEntities.SalesStatus\'Canceled\')';

        return $filter;
    }

    /**
     * @return string
     */
    public static function getFilterRatable(): string
    {
        $filter =
            ' and SalesOrderStatus eq Microsoft.Dynamics.DataEntities.SalesStatus\'Delivered\'' .
            ' and (DeliveryTermsCode eq \'\' or DeliveryTermsCode eq \'0\' or DeliveryTermsCode eq \'-1\')';

        return $filter;
    }

    /**
     * @return string
     */
    public static function getFilterQuotation(): string
    {
        $filter = ' and DeliveryModeCode eq \'Devis\'';

        return $filter;
    }
}
