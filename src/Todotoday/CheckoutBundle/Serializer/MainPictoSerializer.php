<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 14/05/17
 * Time: 08:40
 */

namespace Todotoday\CheckoutBundle\Serializer;

use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\ObjectEvent;
use JMS\Serializer\JsonSerializationVisitor;
use Todotoday\CatalogBundle\Services\CatalogManager;
use Todotoday\CheckoutBundle\Entity\Api\Microsoft\SalesOrderHeader;

/**
 * Class MainPictoSerializer
 * @package Todotoday\CheckoutBundle\Serializer
 */
class MainPictoSerializer implements EventSubscriberInterface
{
    /**
     * @var CatalogManager
     */
    private $catalogManager;

    /**
     * MainPictoSerializer constructor.
     *
     * @param CatalogManager $catalogManager
     */
    public function __construct(CatalogManager $catalogManager)
    {
        $this->catalogManager = $catalogManager;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            array(
                'event' => 'serializer.post_serialize',
                'method' => 'addLinksToPicto',
                'class' => SalesOrderHeader::class,
                'format' => 'json',
            ),
        );
    }

    /**
     * @param ObjectEvent $event
     *
     * @return mixed
     */
    public function addLinksToPicto(ObjectEvent $event)
    {
        /** @var JsonSerializationVisitor $visitor */
        $visitor = $event->getVisitor();
        /** @var SalesOrderHeader $salesOrderHeader */
        $salesOrderHeader = $event->getObject();
        $urls = [];
        $i = 0;
        foreach ($salesOrderHeader->getMainPictos() as $picto) {
            $urls[$i]['category'] = $picto;
            $urls[$i]['url'] = $this->catalogManager->getPictoUrl($picto);
            $i++;
        }

        $visitor->setData('path_pictos', $urls);
    }
}
