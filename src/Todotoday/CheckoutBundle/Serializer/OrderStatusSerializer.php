<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 16/05/17
 * Time: 11:04
 */

namespace Todotoday\CheckoutBundle\Serializer;

use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\ObjectEvent;
use JMS\Serializer\JsonSerializationVisitor;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Translation\Translator;
use Todotoday\CheckoutBundle\Entity\Api\Microsoft\SalesOrderHeader;
use Todotoday\CheckoutBundle\Enum\OrderStatusEnum;

/**
 * Class OrderStatusSerializer
 * @package Todotoday\CheckoutBundle\Serializer
 */
class OrderStatusSerializer implements EventSubscriberInterface
{
    /**
     * @var Translator
     */
    private $translator;

    /**
     * @var string
     */
    private $locale;

    /**
     * OrderStatusSerializer constructor.
     *
     * @param Translator   $translator
     * @param RequestStack $requestStack
     */
    public function __construct(Translator $translator, RequestStack $requestStack)
    {
        $this->translator = $translator;
        $this->locale = $requestStack->getCurrentRequest()->getLocale();
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return array(
            array(
                'event' => 'serializer.post_serialize',
                'method' => 'addStatusOrder',
                'class' => SalesOrderHeader::class,
                'format' => 'json',
            ),
        );
    }

    /**
     * @param ObjectEvent $event
     *
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     */
    public function addStatusOrder(ObjectEvent $event): void
    {
        /** @var JsonSerializationVisitor $visitor */
        $visitor = $event->getVisitor();
        /** @var SalesOrderHeader $salesOrderHeader */
        $salesOrderHeader = $event->getObject();

        $status = OrderStatusEnum::getStatus($salesOrderHeader);

        $visitor->setData('order_status', $status);
        $visitor->setData('order_status_display', $this->getTranslation($status));
        $visitor->setData('ratable', OrderStatusEnum::isRatable($salesOrderHeader));
        $visitor->setData('cancellable', OrderStatusEnum::isCancellable($salesOrderHeader));
//        $visitor->setData('fgsable', OrderStatusEnum::isFGSable($salesOrderHeader));
    }

    /**
     * @param string $status
     *
     * @return string
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     */
    private function getTranslation(string $status): string
    {
        $key = OrderStatusEnum::getTranslationPrefix() . $status;

        return $this->translator->trans($key, [], 'todotoday', $this->locale);
    }
}
