<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 12/04/17
 * Time: 14:12
 */

namespace Todotoday\CheckoutBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Todotoday\CheckoutBundle\Entity\Checkout;

/**
 * Class CheckoutSuccessEvent
 * @package Todotoday\CheckoutBundle\Event
 */
class CheckoutSuccessEvent extends Event
{
    /**
     * Name of the event
     */
    const NAME = 'checkout.success';

    /**
     * @var Checkout
     */
    private $checkout;

    /**
     * CheckoutSuccessEvent constructor.
     *
     * @param Checkout $checkout
     */
    public function __construct(Checkout $checkout)
    {
        $this->checkout = $checkout;
    }

    /**
     * @return Checkout
     */
    public function getCheckout(): ?Checkout
    {
        return $this->checkout;
    }
}
