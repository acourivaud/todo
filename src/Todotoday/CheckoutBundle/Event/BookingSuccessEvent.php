<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 24/04/17
 * Time: 14:09
 */

namespace Todotoday\CheckoutBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Todotoday\CheckoutBundle\Entity\Checkout;

/**
 * Class BookingSuccessEvent
 * @package Todotoday\CheckoutBundle\Event
 */
class BookingSuccessEvent extends Event
{
    /**
     *
     */
    const NAME = 'booking.success';

    /**
     * @var Checkout
     */
    private $checkout;

    /**
     * BookingSuccessEvent constructor.
     *
     * @param Checkout $checkout
     */
    public function __construct(Checkout $checkout)
    {
        $this->checkout = $checkout;
    }

    /**
     * @return Checkout
     */
    public function getCheckout(): ?Checkout
    {
        return $this->checkout;
    }
}
