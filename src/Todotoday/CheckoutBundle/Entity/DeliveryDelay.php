<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 20/12/17
 * Time: 15:18
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\CheckoutBundle\Entity
 *
 * @subpackage Todotoday\CheckoutBundle\Entity
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\CheckoutBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * @ORM\Entity(repositoryClass="Todotoday\CheckoutBundle\Repository\DeliveryDelayRepository")
 * @ORM\Table(name="delivery_delay", schema="checkout")
 */
class DeliveryDelay
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $productFamily;

    /**
     * @var int
     *
     * @ORM\Column(type="smallint")
     */
    private $delay;

    /**
     * @ORM\ManyToOne(targetEntity="Todotoday\CoreBundle\Entity\Agency", inversedBy="deliveryDelays")
     */
    private $agency;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return DeliveryDelay
     */
    public function setId(int $id): DeliveryDelay
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getProductFamily(): ?string
    {
        return $this->productFamily;
    }

    /**
     * @param string $productFamily
     *
     * @return DeliveryDelay
     */
    public function setProductFamily(string $productFamily): DeliveryDelay
    {
        $this->productFamily = $productFamily;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getDelay(): ?int
    {
        return $this->delay;
    }

    /**
     * @param int $delay
     *
     * @return DeliveryDelay
     */
    public function setDelay(int $delay): DeliveryDelay
    {
        $this->delay = $delay;

        return $this;
    }

    /**
     * @return Agency|null
     */
    public function getAgency(): ?Agency
    {
        return $this->agency;
    }

    /**
     * @param Agency|null $agency
     *
     * @return DeliveryDelay
     */
    public function setAgency(?Agency $agency = null): self
    {
        $this->agency = $agency;

        return $this;
    }
}
