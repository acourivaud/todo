<?php declare(strict_types=1);

namespace Todotoday\CheckoutBundle\Entity;

/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 09/04/17
 * Time: 17:17
 */
/**
 * Class Checkout
 * @package Todotoday\CheckoutBundle\Entity
 */
use Symfony\Component\Validator\Constraints as Assert;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CartBundle\Entity\CartProduct;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\PaymentBundle\Entity\CustomerStripeAccount;

/**
 * Class Checkout
 * @package Todotoday\CheckoutBundle\Entity
 */
class Checkout
{
    /**
     * @var string
     */
    protected $stripeId;

    /**
     * @var CustomerStripeAccount
     */
    protected $customerStripeAccount;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $delivery;

    /**
     * @var string
     */
    private $depository;

    /**
     * @var string
     */
    private $case;

    /**
     * @var float
     */
    private $totalAmount = 0;

    /**
     * @var float
     */
    private $totalAmountExclTax = 0;

    /**
     * @var string
     */
    private $deliveryComment;

    /**
     * @var string
     */
    private $depositoryComment;

    /**
     * @var string[]
     *
     * Pour le cas d'une conciergerie digitale avec plusieurs fournisseurs
     */
    private $depositories;

    /**
     * @var string[]
     *
     * Pour le cas d'une conciergerie digitale avec plusieurs fournisseurs
     */
    private $depositoriesComment;

    /**
     * @var CartProduct[]
     */
    private $cartProducts;

    /**
     * @var Adherent
     */
    private $adherent;

    /**
     * @var Agency
     */
    private $agency;

    /**
     * @var string
     */
    private $payment;

    /**
     * @var string
     */
    private $source;

    /**
     * @var bool
     */
    private $depositoryNeeded = false;

    /**
     * @var Supplier[]
     */
    private $suppliers;

    /**
     * Checkout constructor.
     */
    public function __construct()
    {
        $this->cartProducts = array();
        $this->suppliers = array();
        $this->depositories = array();
        $this->depositoriesComment = array();
    }

    /**
     * @param CartProduct $cartProduct
     *
     * @return Checkout
     */
    public function addCartProduct(CartProduct $cartProduct): self
    {
        $this->cartProducts[] = $cartProduct;

        return $this;
    }

    /**
     * @param CartProduct[] $cartProduct
     *
     * @return Checkout
     */
    public function setCartProduct(array $cartProduct): self
    {
        $this->cartProducts = $cartProduct;

        return $this;
    }

    /**
     * @return Adherent
     */
    public function getAdherent(): ?Adherent
    {
        return $this->adherent;
    }

    /**
     * @param Adherent $adherent
     *
     * @return Checkout
     */
    public function setAdherent(Adherent $adherent): Checkout
    {
        $this->adherent = $adherent;

        return $this;
    }

    /**
     * @return Agency
     */
    public function getAgency(): ?Agency
    {
        return $this->agency;
    }

    /**
     * @param Agency $agency
     *
     * @return Checkout
     */
    public function setAgency(Agency $agency): Checkout
    {
        $this->agency = $agency;

        return $this;
    }

    /**
     * @return CartProduct[]
     */
    public function getCartProducts(): array
    {
        return $this->cartProducts;
    }

    /**
     * @return string
     */
    public function getCase(): ?string
    {
        return $this->case;
    }

    /**
     * @param string $case
     *
     * @return Checkout
     */
    public function setCase(string $case): Checkout
    {
        $this->case = $case;

        return $this;
    }

    /**
     * @return string
     */
    public function getDelivery(): ?string
    {
        return $this->delivery;
    }

    /**
     * @param string $delivery
     *
     * @return Checkout
     */
    public function setDelivery(string $delivery): ?Checkout
    {
        $this->delivery = $delivery;

        return $this;
    }

    /**
     * @return string
     */
    public function getDeliveryComment(): ?string
    {
        return $this->deliveryComment;
    }

    /**
     * @param string $deliveryComment
     *
     * @return Checkout
     */
    public function setDeliveryComment(?string $deliveryComment): Checkout
    {
        $this->deliveryComment = $deliveryComment;

        return $this;
    }

    /**
     * @return string
     */
    public function getDepository(): ?string
    {
        return $this->depository;
    }

    /**
     * @param string $depository
     *
     * @return Checkout
     */
    public function setDepository(string $depository): ?Checkout
    {
        $this->depository = $depository;

        return $this;
    }

    /**
     * @return string
     */
    public function getDepositoryComment(): ?string
    {
        return $this->depositoryComment;
    }

    /**
     * @param string $depositoryComment
     *
     * @return Checkout
     */
    public function setDepositoryComment(?string $depositoryComment): Checkout
    {
        $this->depositoryComment = $depositoryComment;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPayment(): ?string
    {
        return $this->payment;
    }

    /**
     * @param string|null $payment
     *
     * @return Checkout
     */
    public function setPayment(?string $payment): Checkout
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * @return string
     */
    public function getSource(): ?string
    {
        return $this->source;
    }

    /**
     * @param string $source
     *
     * @return Checkout
     */
    public function setSource(string $source): Checkout
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get stripeId
     *
     * @return string
     */
    public function getStripeId(): ?string
    {
        return $this->stripeId;
    }

    /**
     * Set stripeId
     *
     * @param string $stripeId
     *
     * @return Checkout
     */
    public function setStripeId(?string $stripeId): self
    {
        $this->stripeId = $stripeId;

        return $this;
    }

    /**
     * @return float
     */
    public function getTotalAmount(): float
    {
        return $this->totalAmount;
    }

    /**
     * @param float $totalAmount
     *
     * @return Checkout
     */
    public function setTotalAmount(float $totalAmount): self
    {
        $this->totalAmount = $totalAmount;

        return $this;
    }

    /**
     * @return float
     */
    public function getTotalAmountExclTax(): float
    {
        return $this->totalAmountExclTax;
    }

    /**
     * @param float $totalAmountExclTax
     *
     * @return Checkout
     */
    public function setTotalAmountExclTax(float $totalAmountExclTax): self
    {
        $this->totalAmountExclTax = $totalAmountExclTax;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDepositoryNeeded(): bool
    {
        return $this->depositoryNeeded;
    }

    /**
     * @param mixed $depositoryNeeded
     *
     * @return Checkout
     */
    public function setDepositoryNeeded(bool $depositoryNeeded): self
    {
        $this->depositoryNeeded = $depositoryNeeded;

        return $this;
    }

    /**
     * @return CustomerStripeAccount
     */
    public function getCustomerStripeAccount(): ?CustomerStripeAccount
    {
        return $this->customerStripeAccount;
    }

    /**
     * @param CustomerStripeAccount $customerStripeAccount
     *
     * @return Checkout
     */
    public function setCustomerStripeAccount(CustomerStripeAccount $customerStripeAccount): self
    {
        $this->customerStripeAccount = $customerStripeAccount;

        return $this;
    }

    /**
     * @return Supplier[]
     */
    public function getSuppliers(): array
    {
        return $this->suppliers;
    }

    /**
     * @param Supplier[] $suppliers
     *
     * @return Checkout
     */
    public function setSuppliers(array $suppliers): self
    {
        $this->suppliers = $suppliers;

        return $this;
    }

    /**
     * @param Supplier $supplier
     *
     * @return Checkout
     */
    public function addSuppliers(Supplier $supplier): self
    {
        $this->suppliers[] = $supplier;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasMultipleSuppliers(): bool
    {
        return (\count($this->getSuppliers()) > 1);
    }

    /**
     * @return string[]
     */
    public function getDepositories(): array
    {
        return $this->depositories;
    }

    /**
     * @param string[] $depositories
     *
     * @return Checkout
     */
    public function setDepositories(array $depositories): self
    {
        $this->depositories = $depositories;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getDepositoriesComment(): array
    {
        return $this->depositoriesComment;
    }

    /**
     * @param string[] $depositoriesComment
     *
     * @return Checkout
     */
    public function setDepositoriesComment(array $depositoriesComment): self
    {
        $this->depositoriesComment = $depositoriesComment;

        return $this;
    }
}
