<?php declare(strict_types=1);

namespace Todotoday\CheckoutBundle\Entity;

/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 09/04/17
 * Time: 17:17
 */
use Todotoday\CartBundle\Entity\CartProduct;

/**
 * Class Supplier
 * @package Todotoday\CheckoutBundle\Entity
 */
class Supplier
{
    /**
     * @var string
     */
    private $supplierName;

    /**
     * @var string
     */
    private $depository;

    /**
     * @var string
     */
    private $depositoryComment;

    /**
     * @var CartProduct[]
     */
    private $cartProducts;

    /**
     * Checkout constructor.
     */
    public function __construct()
    {
        $this->cartProducts = array();
    }

    /**
     * @return string
     */
    public function getSupplierName(): string
    {
        return $this->supplierName;
    }

    /**
     * @param string $supplierName
     *
     * @return Supplier
     */
    public function setSupplierName(string $supplierName): self
    {
        $this->supplierName = $supplierName;

        return $this;
    }

    /**
     * @param CartProduct $cartProduct
     *
     * @return Supplier
     */
    public function addCartProduct(CartProduct $cartProduct): self
    {
        $this->cartProducts[] = $cartProduct;

        return $this;
    }

    /**
     * @return CartProduct[]
     */
    public function getCartProducts(): array
    {
        return $this->cartProducts;
    }

    /**
     * @return string
     */
    public function getDepository(): ?string
    {
        return $this->depository;
    }

    /**
     * @param string $depository
     *
     * @return Supplier
     */
    public function setDepository(string $depository): ?self
    {
        $this->depository = $depository;

        return $this;
    }

    /**
     * @return string
     */
    public function getDepositoryComment(): ?string
    {
        return $this->depositoryComment;
    }

    /**
     * @param string $depositoryComment
     *
     * @return Supplier
     */
    public function setDepositoryComment(?string $depositoryComment): self
    {
        $this->depositoryComment = $depositoryComment;

        return $this;
    }
}
