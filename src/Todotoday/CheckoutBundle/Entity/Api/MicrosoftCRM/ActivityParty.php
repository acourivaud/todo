<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 25/04/17
 * Time: 10:13
 */

namespace Todotoday\CheckoutBundle\Entity\Api\MicrosoftCRM;

use Actiane\ApiConnectorBundle\Lib\EntityProxyTrait;
use JMS\Serializer\Annotation as Serializer;
use Actiane\ApiConnectorBundle\Lib\Mapping as APIConnector;

/**
 * @Serializer\ExclusionPolicy("all")
 * @APIConnector\Entity(path="activityparties",
 *      repositoryId="todotoday.checkout.repository.api.activity_party")
 * @SuppressWarnings(PHPMD)
 * Class ServiceAppointment
 * @package Todotoday\CMSBundle\Entity\Api\MicrosoftCRM
 */
class ActivityParty
{
    use EntityProxyTrait;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_ownerid_value", type="string", guid="yapa")
     * @Serializer\Expose()
     */
    protected $owneridValue;

    /**
     * @var int
     *
     * @APIConnector\Column(name="participationtypemask", type="int")
     * @Serializer\Expose()
     */
    protected $participationtypemask;

    /**
     * @var bool
     *
     * @APIConnector\Column(name="ispartydeleted", type="bool")
     * @Serializer\Expose()
     */
    protected $ispartydeleted;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_activityid_value", type="string", guid="activityid")
     * @Serializer\Expose()
     */
    protected $activityidValue;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @APIConnector\Column(name="activitypartyid", type="string", guid="id")
     * @Serializer\Expose()
     */
    protected $activitypartyid;

    /**
     * @var int
     *
     * @APIConnector\Column(name="versionnumber", type="int")
     * @Serializer\Expose()
     */
    protected $versionnumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_partyid_value", type="string", guid="incidentid")
     * @Serializer\Expose()
     */
    protected $partyidValue;

    /**
     * @var int
     *
     * @APIConnector\Column(name="instancetypecode", type="int")
     * @Serializer\Expose()
     */
    protected $instancetypecode;

    /**
     * @var bool
     *
     * @APIConnector\Column(name="donotemail", type="bool")
     * @Serializer\Expose()
     */
    protected $donotemail;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="scheduledend", type="datetimez")
     * @Serializer\Expose()
     */
    protected $scheduledend;

    /**
     * @var string
     *
     * @APIConnector\Column(name="exchangeentryid", type="string")
     * @Serializer\Expose()
     */
    protected $exchangeentryid;

    /**
     * @var bool
     *
     * @APIConnector\Column(name="donotpostalmail", type="bool")
     * @Serializer\Expose()
     */
    protected $donotpostalmail;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="scheduledstart", type="datetimez")
     * @Serializer\Expose()
     */
    protected $scheduledstart;

    /**
     * @var float
     *
     * @APIConnector\Column(name="effort", type="float")
     * @Serializer\Expose()
     */
    protected $effort;

    /**
     * @var bool
     *
     * @APIConnector\Column(name="donotphone", type="bool")
     * @Serializer\Expose()
     */
    protected $donotphone;

    /**
     * @var int
     *
     * @APIConnector\Column(name="addressusedemailcolumnnumber", type="int")
     * @Serializer\Expose()
     */
    protected $addressusedemailcolumnnumber;

    /**
     * @var bool
     *
     * @APIConnector\Column(name="donotfax", type="bool")
     * @Serializer\Expose()
     */
    protected $donotfax;

    /**
     * @var int
     *
     * @APIConnector\Column(name="addressused", type="int")
     * @Serializer\Expose()
     */
    protected $addressused;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_resourcespecid_value", type="string", guid="resourcespecid")
     * @Serializer\Expose()
     */
    protected $resourcespecidValue;

    /**
     * @return string
     */
    public function getOwneridValue(): ?string
    {
        return $this->owneridValue;
    }

    /**
     * @param string $owneridValue
     *
     * @return ActivityParty
     */
    public function setOwneridValue(?string $owneridValue): ActivityParty
    {
        return $this->setTrigger('owneridValue', $owneridValue);
    }

    /**
     * @return int
     */
    public function getParticipationtypemask(): ?int
    {
        return $this->participationtypemask;
    }

    /**
     * @param int $participationtypemask
     *
     * @return ActivityParty
     */
    public function setParticipationtypemask(?int $participationtypemask): ActivityParty
    {
        return $this->setTrigger('participationtypemask', $participationtypemask);
    }

    /**
     * @return bool
     */
    public function isIspartydeleted(): ?bool
    {
        return $this->ispartydeleted;
    }

    /**
     * @param bool $ispartydeleted
     *
     * @return ActivityParty
     */
    public function setIspartydeleted(?bool $ispartydeleted): ActivityParty
    {
        return $this->setTrigger('ispartydeleted', $ispartydeleted);
    }

    /**
     * @return string|array
     */
    public function getActivityidValue()
    {
        return $this->activityidValue;
    }

    /**
     * @param string $activityidValue
     *
     * @return ActivityParty
     */
    public function setActivityidValue(?string $activityidValue): ActivityParty
    {
        return $this->setTriggerGuid(
            'activityidValue',
            array(
                'target' => 'serviceappointments',
                'value' => $activityidValue,
            )
        );
    }

    /**
     * @return string
     */
    public function getActivitypartyid(): ?string
    {
        return $this->activitypartyid;
    }

    /**
     * @param string $activitypartyid
     *
     * @return ActivityParty
     */
    public function setActivitypartyid(?string $activitypartyid): ActivityParty
    {
        return $this->setTrigger('activitypartyid', $activitypartyid);
    }

    /**
     * @return int
     */
    public function getVersionnumber(): ?int
    {
        return $this->versionnumber;
    }

    /**
     * @param int $versionnumber
     *
     * @return ActivityParty
     */
    public function setVersionnumber(?int $versionnumber): ActivityParty
    {
        return $this->setTrigger('versionnumber', $versionnumber);
    }

    /**
     * @return string|null
     */
    public function getPartyidValue()
    {
        return $this->partyidValue;
    }

    /**
     * @param string $partyidValue
     *
     * @return ActivityParty
     */
    public function setPartyidValue(?string $partyidValue): ActivityParty
    {
        return $this->setTriggerGuid(
            'partyidValue',
            array(
                'target' => 'accounts',
                'value' => $partyidValue,
            )
        );
    }

    /**
     * @param null|string $partyidValue
     * @param string      $target
     *
     * @return EntityProxyTrait|mixed
     */
    public function setPartyidValueGuid(string $partyidValue, string $target)
    {
        return $this->setTriggerGuid(
            'partyidValue',
            array(
                'target' => $target,
                'value' => $partyidValue,
            )
        );
    }

    /**
     * @return int
     */
    public function getInstancetypecode(): ?int
    {
        return $this->instancetypecode;
    }

    /**
     * @param int $instancetypecode
     *
     * @return ActivityParty
     */
    public function setInstancetypecode(?int $instancetypecode): ActivityParty
    {
        return $this->setTrigger('instancetypecode', $instancetypecode);
    }

    /**
     * @return bool
     */
    public function isDonotemail(): ?bool
    {
        return $this->donotemail;
    }

    /**
     * @param bool $donotemail
     *
     * @return ActivityParty
     */
    public function setDonotemail(?bool $donotemail): ActivityParty
    {
        return $this->setTrigger('donotemail', $donotemail);
    }

    /**
     * @return \DateTime
     */
    public function getScheduledend(): ?\DateTime
    {
        return $this->scheduledend;
    }

    /**
     * @param \DateTime $scheduledend
     *
     * @return ActivityParty
     */
    public function setScheduledend(?\DateTime $scheduledend): ActivityParty
    {
        return $this->setTrigger('scheduledend', $scheduledend);
    }

    /**
     * @return string
     */
    public function getExchangeentryid(): ?string
    {
        return $this->exchangeentryid;
    }

    /**
     * @param string $exchangeentryid
     *
     * @return ActivityParty
     */
    public function setExchangeentryid(?string $exchangeentryid): ActivityParty
    {
        return $this->setTrigger('exchangeentryid', $exchangeentryid);
    }

    /**
     * @return bool
     */
    public function isDonotpostalmail(): ?bool
    {
        return $this->donotpostalmail;
    }

    /**
     * @param bool $donotpostalmail
     *
     * @return ActivityParty
     */
    public function setDonotpostalmail(?bool $donotpostalmail): ActivityParty
    {
        return $this->setTrigger('donotpostalmail', $donotpostalmail);
    }

    /**
     * @return \DateTime
     */
    public function getScheduledstart(): ?\DateTime
    {
        return $this->scheduledstart;
    }

    /**
     * @param \DateTime $scheduledstart
     *
     * @return ActivityParty
     */
    public function setScheduledstart(?\DateTime $scheduledstart): ActivityParty
    {
        return $this->setTrigger('scheduledstart', $scheduledstart);
    }

    /**
     * @return float
     */
    public function getEffort(): ?float
    {
        return $this->effort;
    }

    /**
     * @param float $effort
     *
     * @return ActivityParty
     */
    public function setEffort(?float $effort): ActivityParty
    {
        return $this->setTrigger('effort', $effort);
    }

    /**
     * @return bool
     */
    public function isDonotphone(): ?bool
    {
        return $this->donotphone;
    }

    /**
     * @param bool $donotphone
     *
     * @return ActivityParty
     */
    public function setDonotphone(?bool $donotphone): ActivityParty
    {
        return $this->setTrigger('donotphone', $donotphone);
    }

    /**
     * @return int
     */
    public function getAddressusedemailcolumnnumber(): ?int
    {
        return $this->addressusedemailcolumnnumber;
    }

    /**
     * @param int $addressusedemailcolumnnumber
     *
     * @return ActivityParty
     */
    public function setAddressusedemailcolumnnumber(?int $addressusedemailcolumnnumber): ActivityParty
    {
        return $this->setTrigger('addressusedemailcolumnnumber', $addressusedemailcolumnnumber);
    }

    /**
     * @return bool
     */
    public function isDonotfax(): ?bool
    {
        return $this->donotfax;
    }

    /**
     * @param bool $donotfax
     *
     * @return ActivityParty
     */
    public function setDonotfax(?bool $donotfax): ActivityParty
    {
        return $this->setTrigger('donotfax', $donotfax);
    }

    /**
     * @return int
     */
    public function getAddressused(): ?int
    {
        return $this->addressused;
    }

    /**
     * @param int $addressused
     *
     * @return ActivityParty
     */
    public function setAddressused(?int $addressused): ActivityParty
    {
        return $this->setTrigger('addressused', $addressused);
    }

    /**
     * @return string
     */
    public function getResourcespecidValue(): ?string
    {
        return $this->resourcespecidValue;
    }

    /**
     * @param string $resourcespecidValue
     *
     * @return ActivityParty
     */
    public function setResourcespecidValue(?string $resourcespecidValue): ActivityParty
    {
        return $this->setTrigger('resourcespecidValue', $resourcespecidValue);
    }
}
