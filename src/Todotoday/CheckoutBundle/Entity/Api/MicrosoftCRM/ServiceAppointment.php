<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 20/04/17
 * Time: 17:46
 */

namespace Todotoday\CheckoutBundle\Entity\Api\MicrosoftCRM;

use JMS\Serializer\Annotation as Serializer;
use Actiane\ApiConnectorBundle\Lib\Mapping as APIConnector;
use Todotoday\CMSBundle\Entity\Api\MicrosoftCRM\ActivityPointer;

/**
 * @Serializer\ExclusionPolicy("all")
 * @APIConnector\Entity(path="serviceappointments",
 *      repositoryId="todotoday.checkout.repository.api.service_appointment")
 * @SuppressWarnings(PHPMD)
 * Class ServiceAppointment
 * @package Todotoday\CMSBundle\Entity\Api\MicrosoftCRM
 */
class ServiceAppointment extends ActivityPointer
{
    /**
     * @var string
     *
     * @APIConnector\Column(name="_siteid_value", type="string", guid="siteid")
     * @Serializer\Expose()
     */
    protected $siteidValue;

    /**
     * @var bool
     *
     * @APIConnector\Column(name="aps_datederendezvouspassee", type="bool")
     * @Serializer\Expose()
     */
    protected $apsDatederendezvouspassee;

    /**
     * @var bool
     *
     * @APIConnector\Column(name="aps_depassementdatedefin", type="bool")
     * @Serializer\Expose()
     */
    protected $apsDepassementdatedefin;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="aps_datedefindepreiodicite", type="datetimez")
     * @Serializer\Expose()
     */
    protected $apsDatedefindepreiodicite;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_nombreactueldeparticipants_state", type="int")
     * @Serializer\Expose()
     */
    protected $apsNombreactueldeparticipantsState;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="aps_datedefincorrigee", type="datetimez")
     * @Serializer\Expose()
     */
    protected $apsDatedefincorrigee;

    /**
     * @var bool
     *
     * @APIConnector\Column(name="isalldayevent", type="bool")
     * @Serializer\Expose()
     */
    protected $isalldayevent;

    /**
     * @var float
     *
     * @APIConnector\Column(name="aps_pourcentagedoccupationjournalier", type="float")
     * @Serializer\Expose()
     */
    protected $apsPourcentagedoccupationjournalier;

    /**
     * @var bool
     *
     * @APIConnector\Column(name="aps_rendezvousperiodique", type="bool")
     * @Serializer\Expose()
     */
    protected $apsRendezvousperiodique;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="aps_datedujour", type="datetimez")
     * @Serializer\Expose()
     */
    protected $apsDatedujour;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_periodicite", type="int")
     * @Serializer\Expose()
     */
    protected $apsPeriodicite;

    /**
     * @var bool
     *
     * @APIConnector\Column(name="aps_inforecontact", type="bool")
     * @Serializer\Expose()
     */
    protected $apsInforecontact;

    /**
     * @var string
     *
     * @APIConnector\Column(name="category", type="string")
     * @Serializer\Expose()
     */
    protected $category;

    /**
     * @var int
     *
     * @APIConnector\Column(name="importsequencenumber", type="int")
     * @Serializer\Expose()
     */
    protected $importsequencenumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="subcategory", type="string")
     * @Serializer\Expose()
     */
    protected $subcategory;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_origine", type="int")
     * @Serializer\Expose()
     */
    protected $apsOrigine;

    /**
     * @var string
     *
     * @APIConnector\Column(name="location", type="string")
     * @Serializer\Expose()
     */
    protected $location;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_aps_lignedecommande_value", type="string", guid="aps_lignedecommandeid")
     * @Serializer\Expose()
     */
    protected $apsLignedecommandeValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_aps_clientsadherents_value", type="string", guid="accountid")
     * @Serializer\Expose()
     */
    protected $apsClientsadherentsValue;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="overriddencreatedon", type="datetimez")
     * @Serializer\Expose()
     */
    protected $overriddencreatedon;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_tempsmoyendetravailparjour", type="int")
     * @Serializer\Expose()
     */
    protected $apsTempsmoyendetravailparjour;

    /**
     * @var string
     *
     * @APIConnector\Column(name="subscriptionid", type="string", guid="yapa")
     * @Serializer\Expose()
     */
    protected $subscriptionid;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_nombreactueldeparticipants", type="int")
     * @Serializer\Expose()
     */
    protected $apsNombreactueldeparticipants;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="aps_nombreactueldeparticipants_date", type="datetimez")
     * @Serializer\Expose()
     */
    protected $apsNombreactueldeparticipantsDate;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="aps_datelimitederecontact", type="datetimez")
     * @Serializer\Expose()
     */
    protected $apsDatelimitederecontact;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_aps_commande_value", type="string", guid="salesorderid")
     * @Serializer\Expose()
     */
    protected $apsCommandeValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_equipement", type="string", guid="aps_Equipements_ServiceAppointment")
     * @Serializer\Expose()
     */
    protected $apsEquipement;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_clientsadherents", type="string", guid="aps_ClientsAdherents_ServiceAppointment")
     * @Serializer\Expose()
     */
    protected $apsClientsadherents;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_commandeid", type="string")
     * @Serializer\Expose()
     */
    protected $apsCommandeid;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_canaldecreation", type="string")
     * @Serializer\Expose()
     */
    protected $apsCanaldecreation;

    /**
     * @var bool
     * @Serializer\Expose()
     */
    protected $cancellable = false;

    /**
     * @var bool
     * @Serializer\Expose()
     */
    protected $cancelled = false;

    /**
     * ServiceAppointment constructor.
     */
    public function __construct()
    {
        $this->apsCanaldecreation = 'FRONT';
    }

    /**
     * @return string|array
     */
    public function getSiteidValue()
    {
        return $this->siteidValue;
    }

    /**
     * @param string $siteidValue
     *
     * @return ServiceAppointment
     */
    public function setSiteidValue(?string $siteidValue): ServiceAppointment
    {
        return $this->setTriggerGuid(
            'siteidValue',
            array(
                'target' => 'sites',
                'value' => $siteidValue,
            )
        );
    }

    /**
     * @return bool
     */
    public function isApsDatederendezvouspassee(): ?bool
    {
        return $this->apsDatederendezvouspassee;
    }

    /**
     * @param bool $apsDatederendezvouspassee
     *
     * @return ServiceAppointment
     */
    public function setApsDatederendezvouspassee(?bool $apsDatederendezvouspassee): ServiceAppointment
    {
        $this->cancellable = !$apsDatederendezvouspassee;

        return $this->setTrigger('apsDatederendezvouspassee', $apsDatederendezvouspassee);
    }

    /**
     * @return bool
     */
    public function isApsDepassementdatedefin(): ?bool
    {
        return $this->apsDepassementdatedefin;
    }

    /**
     * @param bool $apsDepassementdatedefin
     *
     * @return ServiceAppointment
     */
    public function setApsDepassementdatedefin(?bool $apsDepassementdatedefin): ServiceAppointment
    {
        return $this->setTrigger('apsDepassementdatedefin', $apsDepassementdatedefin);
    }

    /**
     * @return \DateTime
     */
    public function getApsDatedefindepreiodicite(): ?\DateTime
    {
        return $this->apsDatedefindepreiodicite;
    }

    /**
     * @param \DateTime $apsDatedefindepreiodicite
     *
     * @return ServiceAppointment
     */
    public function setApsDatedefindepreiodicite(?\DateTime $apsDatedefindepreiodicite): ServiceAppointment
    {
        return $this->setTrigger('apsDatedefindepreiodicite', $apsDatedefindepreiodicite);
    }

    /**
     * @return int
     */
    public function getApsNombreactueldeparticipantsState(): ?int
    {
        return $this->apsNombreactueldeparticipantsState;
    }

    /**
     * @param int $apsNombreactueldeparticipantsState
     *
     * @return ServiceAppointment
     */
    public function setApsNombreactueldeparticipantsState(?int $apsNombreactueldeparticipantsState): ServiceAppointment
    {
        return $this->setTrigger('apsNombreactueldeparticipantsState', $apsNombreactueldeparticipantsState);
    }

    /**
     * @return \DateTime
     */
    public function getApsDatedefincorrigee(): ?\DateTime
    {
        return $this->apsDatedefincorrigee;
    }

    /**
     * @param \DateTime $apsDatedefincorrigee
     *
     * @return ServiceAppointment
     */
    public function setApsDatedefincorrigee(?\DateTime $apsDatedefincorrigee): ServiceAppointment
    {
        return $this->setTrigger('apsDatedefincorrigee', $apsDatedefincorrigee);
    }

    /**
     * @return bool
     */
    public function isIsalldayevent(): ?bool
    {
        return $this->isalldayevent;
    }

    /**
     * @param bool $isalldayevent
     *
     * @return ServiceAppointment
     */
    public function setIsalldayevent(?bool $isalldayevent): ServiceAppointment
    {
        return $this->setTrigger('isalldayevent', $isalldayevent);
    }

    /**
     * @return float
     */
    public function getApsPourcentagedoccupationjournalier(): ?float
    {
        return $this->apsPourcentagedoccupationjournalier;
    }

    /**
     * @param float $apsPourcentagedoccupationjournalier
     *
     * @return ServiceAppointment
     */
    public function setApsPourcentagedoccupationjournalier(
        ?float $apsPourcentagedoccupationjournalier
    ): ServiceAppointment {
        return $this->setTrigger('apsPourcentagedoccupationjournalier', $apsPourcentagedoccupationjournalier);
    }

    /**
     * @return bool
     */
    public function isApsRendezvousperiodique(): ?bool
    {
        return $this->apsRendezvousperiodique;
    }

    /**
     * @param bool $apsRendezvousperiodique
     *
     * @return ServiceAppointment
     */
    public function setApsRendezvousperiodique(?bool $apsRendezvousperiodique): ServiceAppointment
    {
        return $this->setTrigger('apsRendezvousperiodique', $apsRendezvousperiodique);
    }

    /**
     * @return \DateTime
     */
    public function getApsDatedujour(): ?\DateTime
    {
        return $this->apsDatedujour;
    }

    /**
     * @param \DateTime $apsDatedujour
     *
     * @return ServiceAppointment
     */
    public function setApsDatedujour(?\DateTime $apsDatedujour): ServiceAppointment
    {
        return $this->setTrigger('apsDatedujour', $apsDatedujour);
    }

    /**
     * @return int
     */
    public function getApsPeriodicite(): ?int
    {
        return $this->apsPeriodicite;
    }

    /**
     * @param int $apsPeriodicite
     *
     * @return ServiceAppointment
     */
    public function setApsPeriodicite(?int $apsPeriodicite): ServiceAppointment
    {
        return $this->setTrigger('apsPeriodicite', $apsPeriodicite);
    }

    /**
     * @return bool
     */
    public function isApsInforecontact(): ?bool
    {
        return $this->apsInforecontact;
    }

    /**
     * @param bool $apsInforecontact
     *
     * @return ServiceAppointment
     */
    public function setApsInforecontact(?bool $apsInforecontact): ServiceAppointment
    {
        return $this->setTrigger('apsInforecontact', $apsInforecontact);
    }

    /**
     * @return string
     */
    public function getCategory(): ?string
    {
        return $this->category;
    }

    /**
     * @param string $category
     *
     * @return ServiceAppointment
     */
    public function setCategory(?string $category): ServiceAppointment
    {
        return $this->setTrigger('category', $category);
    }

    /**
     * @return int
     */
    public function getImportsequencenumber(): ?int
    {
        return $this->importsequencenumber;
    }

    /**
     * @param int $importsequencenumber
     *
     * @return ServiceAppointment
     */
    public function setImportsequencenumber(?int $importsequencenumber): ServiceAppointment
    {
        return $this->setTrigger('importsequencenumber', $importsequencenumber);
    }

    /**
     * @return string
     */
    public function getSubcategory(): ?string
    {
        return $this->subcategory;
    }

    /**
     * @param string $subcategory
     *
     * @return ServiceAppointment
     */
    public function setSubcategory(?string $subcategory): ServiceAppointment
    {
        return $this->setTrigger('subcategory', $subcategory);
    }

    /**
     * @return int
     */
    public function getApsOrigine(): ?int
    {
        return $this->apsOrigine;
    }

    /**
     * @param int $apsOrigine
     *
     * @return ServiceAppointment
     */
    public function setApsOrigine(?int $apsOrigine): ServiceAppointment
    {
        return $this->setTrigger('apsOrigine', $apsOrigine);
    }

    /**
     * @return string
     */
    public function getLocation(): ?string
    {
        return $this->location;
    }

    /**
     * @param string $location
     *
     * @return ServiceAppointment
     */
    public function setLocation(?string $location): ServiceAppointment
    {
        return $this->setTrigger('location', $location);
    }

    /**
     * @return string
     */
    public function getApsLignedecommandeValue(): ?string
    {
        return $this->apsLignedecommandeValue;
    }

    /**
     * @param string $apsLignedecommandeValue
     *
     * @return ServiceAppointment
     */
    public function setApsLignedecommandeValue(?string $apsLignedecommandeValue): ServiceAppointment
    {
        return $this->setTrigger('apsLignedecommandeValue', $apsLignedecommandeValue);
    }

    /**
     * @return string
     */
    public function getApsClientsadherentsValue(): ?string
    {
        return $this->apsClientsadherentsValue;
    }

    /**
     * @param string $apsClientsadherentsValue
     *
     * @return ServiceAppointment
     */
    public function setApsClientsadherentsValue(?string $apsClientsadherentsValue): ServiceAppointment
    {
        return $this->setTrigger('apsClientsadherentsValue', $apsClientsadherentsValue);
    }

    /**
     * @return \DateTime
     */
    public function getOverriddencreatedon(): ?\DateTime
    {
        return $this->overriddencreatedon;
    }

    /**
     * @param \DateTime $overriddencreatedon
     *
     * @return ServiceAppointment
     */
    public function setOverriddencreatedon(?\DateTime $overriddencreatedon): ServiceAppointment
    {
        return $this->setTrigger('overriddencreatedon', $overriddencreatedon);
    }

    /**
     * @return int
     */
    public function getApsTempsmoyendetravailparjour(): ?int
    {
        return $this->apsTempsmoyendetravailparjour;
    }

    /**
     * @param int $apsTempsmoyendetravailparjour
     *
     * @return ServiceAppointment
     */
    public function setApsTempsmoyendetravailparjour(?int $apsTempsmoyendetravailparjour): ServiceAppointment
    {
        return $this->setTrigger('apsTempsmoyendetravailparjour', $apsTempsmoyendetravailparjour);
    }

    /**
     * @return string
     */
    public function getSubscriptionid(): ?string
    {
        return $this->subscriptionid;
    }

    /**
     * @param string $subscriptionid
     *
     * @return ServiceAppointment
     */
    public function setSubscriptionid(?string $subscriptionid): ServiceAppointment
    {
        return $this->setTrigger('subscriptionid', $subscriptionid);
    }

    /**
     * @return int
     */
    public function getApsNombreactueldeparticipants(): ?int
    {
        return $this->apsNombreactueldeparticipants;
    }

    /**
     * @param int $apsNombreactueldeparticipants
     *
     * @return ServiceAppointment
     */
    public function setApsNombreactueldeparticipants(?int $apsNombreactueldeparticipants): ServiceAppointment
    {
        return $this->setTrigger('apsNombreactueldeparticipants', $apsNombreactueldeparticipants);
    }

    /**
     * @return \DateTime
     */
    public function getApsNombreactueldeparticipantsDate(): ?\DateTime
    {
        return $this->apsNombreactueldeparticipantsDate;
    }

    /**
     * @param \DateTime $apsNombreactueldeparticipantsDate
     *
     * @return ServiceAppointment
     */
    public function setApsNombreactueldeparticipantsDate(
        ?\DateTime $apsNombreactueldeparticipantsDate
    ): ServiceAppointment {
        return $this->setTrigger('apsNombreactueldeparticipantsDate', $apsNombreactueldeparticipantsDate);
    }

    /**
     * @return \DateTime
     */
    public function getApsDatelimitederecontact(): ?\DateTime
    {
        return $this->apsDatelimitederecontact;
    }

    /**
     * @param \DateTime $apsDatelimitederecontact
     *
     * @return ServiceAppointment
     */
    public function setApsDatelimitederecontact(?\DateTime $apsDatelimitederecontact): ServiceAppointment
    {
        return $this->setTrigger('apsDatelimitederecontact', $apsDatelimitederecontact);
    }

    /**
     * @return string
     */
    public function getApsCommandeValue(): ?string
    {
        return $this->apsCommandeValue;
    }

    /**
     * @param string $apsCommandeValue
     *
     * @return ServiceAppointment
     */
    public function setApsCommandeValue(?string $apsCommandeValue): ServiceAppointment
    {
        return $this->setTrigger('apsCommandeValue', $apsCommandeValue);
    }

    /**
     * @return string|array
     */
    public function getApsEquipement()
    {
        return $this->apsEquipement;
    }

    /**
     * @param string $apsEquipement
     *
     * @return ServiceAppointment
     */
    public function setApsEquipement(?string $apsEquipement): ServiceAppointment
    {
        return $this->setTriggerGuid(
            'apsEquipement',
            array(
                'target' => 'equipments',
                'value' => $apsEquipement,
            )
        );
    }

    /**
     * @return string|array
     */
    public function getApsClientsadherents()
    {
        return $this->apsClientsadherents;
    }

    /**
     * @param string $apsClientsadherents
     *
     * @return ServiceAppointment
     */
    public function setApsClientsadherents(?string $apsClientsadherents): ServiceAppointment
    {
        return $this->setTriggerGuid(
            'apsClientsadherents',
            array(
                'target' => 'accounts',
                'value' => $apsClientsadherents,
            )
        );
    }

    /**
     * @return string
     */
    public function getApsCommandeid(): ?string
    {
        return $this->apsCommandeid;
    }

    /**
     * @param string $apsCommandeid
     *
     * @return ServiceAppointment
     */
    public function setApsCommandeid(?string $apsCommandeid): ServiceAppointment
    {
        return $this->setTrigger('apsCommandeid', $apsCommandeid);
    }

    /**
     * @return string
     */
    public function getApsCanaldecreation(): ?string
    {
        return $this->apsCanaldecreation;
    }

    /**
     * @param string $apsCanaldecreation
     *
     * @return ServiceAppointment
     */
    public function setApsCanaldecreation(?string $apsCanaldecreation): ServiceAppointment
    {
        return $this->setTrigger('apsCanaldecreation', $apsCanaldecreation);
    }

    /**
     * @return bool
     */
    public function isCancellable(): bool
    {
        return $this->cancellable;
    }

    /**
     * @param bool $cancellable
     *
     * @return ServiceAppointment
     */
    public function setCancellable(bool $cancellable): ServiceAppointment
    {
        $this->cancellable = $cancellable;

        return $this;
    }

    /**
     * @return bool
     */
    public function isCancelled(): bool
    {
        return $this->cancelled;
    }

    /**
     * @param bool $cancelled
     *
     * @return ServiceAppointment
     */
    public function setCancelled(bool $cancelled): ServiceAppointment
    {
        $this->cancelled = $cancelled;

        return $this;
    }

    /**
     * @param int $statuscode
     *
     * @return ActivityPointer
     */
    public function setStatuscode(int $statuscode): ActivityPointer
    {
        $this->setTrigger('statuscode', $statuscode);

        return $this->refreshStatus();
    }

    /**
     * @param int $statecode
     *
     * @return ActivityPointer
     */
    public function setStatecode(int $statecode): ActivityPointer
    {
        $this->setTrigger('statecode', $statecode);

        return $this->refreshStatus();
    }

    /**
     * @return ServiceAppointment
     */
    private function refreshStatus(): self
    {
        if (parent::getStatecode() === 2 && parent::getStatuscode() === 9) {
            $this->cancelled = true;
            $this->cancellable = false;
        }

        return $this;
    }
}
