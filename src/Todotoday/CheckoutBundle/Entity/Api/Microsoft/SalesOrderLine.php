<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 05/04/17
 * Time: 16:02
 */

namespace Todotoday\CheckoutBundle\Entity\Api\Microsoft;

use Actiane\ApiConnectorBundle\Lib\EntityProxyTrait;
use Actiane\ApiConnectorBundle\Lib\Mapping as APIConnector;
use JMS\Serializer\Annotation as Serializer;

/**
 * @SuppressWarnings(PHPMD)
 * @APIConnector\Entity(path="TDTDSalesOrderLines", repositoryId="todotoday.checkout.repository.api.sales_order_line")
 * Class SalesOrderLine
 * @package Todotoday\CheckoutBundle\Entity\Api\Microsoft
 * @Serializer\ExclusionPolicy("all")
 */
class SalesOrderLine
{
    use EntityProxyTrait;

    /**
     * @var string
     *
     * @APIConnector\Column(name="IntrastatSpecialMovementCode", type="string")
     * @Serializer\Type("string")
     */
    protected $intrastatSpecialMovementCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="SalesUnitSymbol", type="string")
     * @Serializer\Type("string")
     */
    protected $salesUnitSymbol;

    /**
     * @var float
     *
     * @APIConnector\Column(name="AllowedUnderdeliveryPercentage", type="float")
     * @Serializer\Type("float")
     */
    protected $allowedUnderdeliveryPercentage;

    /**
     * @var string
     *
     * @APIConnector\Column(name="IntrastatPortId", type="string")
     * @Serializer\Type("string")
     */
    protected $intrastatPortId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="OrderedInventoryStatusId", type="string")
     * @Serializer\Type("string")
     */
    protected $orderedInventoryStatusId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ProductStyleId", type="string")
     * @Serializer\Type("string")
     */
    protected $productStyleId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="WillAutomaticInventoryReservationConsiderBatchAttributes", type="string",
     *                                                                                       mType="Microsoft.Dynamics.DataEntities.NoYes")
     * @Serializer\Type("string")
     */
    protected $willAutomaticInventoryReservationConsiderBatchAttributes;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ShippingSiteId", type="string")
     * @Serializer\Type("string")
     */
    protected $shippingSiteId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DeliveryAddressLocationId", type="string")
     * @Serializer\Type("string")
     */
    protected $deliveryAddressLocationId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DeliveryAddressCountyId", type="string")
     * @Serializer\Type("string")
     */
    protected $deliveryAddressCountyId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="IntrastatCommodityCode", type="string")
     * @Serializer\Type("string")
     */
    protected $intrastatCommodityCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="SalesRebateProductGroupId", type="string")
     * @Serializer\Type("string")
     */
    protected $salesRebateProductGroupId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="SalesOrderPromisingMethod", type="string",
     *                                                        mType="Microsoft.Dynamics.DataEntities.SalesDeliveryDateControlType")
     * @Serializer\Type("string")
     */
    protected $salesOrderPromisingMethod;

    /**
     * @var float
     *
     * @APIConnector\Column(name="MultilineDiscountAmount", type="float")
     * @Serializer\Type("float")
     */
    protected $multilineDiscountAmount;

    /**
     * @var float
     *
     * @APIConnector\Column(name="LineDiscountAmount", type="float")
     * @Serializer\Type("float")
     */
    protected $lineDiscountAmount;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DeliveryTermsId", type="string")
     * @Serializer\Type("string")
     */
    protected $deliveryTermsId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="WithholdingTaxGroupCode", type="string")
     * @Serializer\Type("string")
     */
    protected $withholdingTaxGroupCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="MainAccountIdDisplayValue", type="string")
     * @Serializer\Type("string")
     */
    protected $mainAccountIdDisplayValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="LineDescription", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $lineDescription;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ItemNumber", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $itemNumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DeliveryAddressZipCode", type="string")
     * @Serializer\Type("string")
     */
    protected $deliveryAddressZipCode;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="ConfirmedShippingDate", type="datetimez")
     * @Serializer\Type("DateTime")
     */
    protected $confirmedShippingDate;

    /**
     * @var string
     *
     * @APIConnector\Column(name="RouteId", type="string")
     * @Serializer\Type("string")
     */
    protected $routeId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ShippingWarehouseId", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $shippingWarehouseId;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="RequestedReceiptDate", type="datetimez")
     * @Serializer\Type("DateTime")
     */
    protected $requestedReceiptDate;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DeliverySalesTaxGroupCode", type="string")
     * @Serializer\Type("string")
     */
    protected $deliverySalesTaxGroupCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="PackingUnitSymbol", type="string")
     * @Serializer\Type("string")
     */
    protected $packingUnitSymbol;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ServiceOrderNumber", type="string")
     * @Serializer\Type("string")
     */
    protected $serviceOrderNumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="EInvoicePropertyNumber", type="string")
     * @Serializer\Type("string")
     */
    protected $eInvoicePropertyNumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="FormattedDelveryAddress", type="string")
     * @Serializer\Type("string")
     */
    protected $formattedDelveryAddress;

    /**
     * @var float
     *
     * @APIConnector\Column(name="OrderedSalesQuantity", type="float")
     * @Serializer\Expose()
     * @Serializer\Type("float")
     */
    protected $orderedSalesQuantity;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DeliveryAddressDistrictName", type="string")
     * @Serializer\Type("string")
     */
    protected $deliveryAddressDistrictName;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DeliveryAddressDunsNumber", type="string")
     * @Serializer\Type("string")
     */
    protected $deliveryAddressDunsNumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="BOMId", type="string")
     * @Serializer\Type("string")
     */
    protected $bomId;

    /**
     * @var float
     *
     * @APIConnector\Column(name="LineAmount", type="float")
     * @Serializer\Expose()
     * @Serializer\Type("float")
     */
    protected $lineAmount;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ServiceFiscalInformationCode", type="string")
     * @Serializer\Type("string")
     */
    protected $serviceFiscalInformationCode;

    /**
     * @var float
     *
     * @APIConnector\Column(name="SalesPriceQuantity", type="float")
     * @Serializer\Expose()
     * @Serializer\Type("float")
     */
    protected $salesPriceQuantity;

    /**
     * @var string
     *
     * @APIConnector\Column(name="IsIntrastatTriangularDeal", type="string",
     *                                                        mType="Microsoft.Dynamics.DataEntities.NoYes")
     * @Serializer\Type("string")
     */
    protected $isIntrastatTriangularDeal;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ProductSizeId", type="string")
     * @Serializer\Type("string")
     */
    protected $productSizeId;

    /**
     * @var float
     *
     * @APIConnector\Column(name="LineDiscountPercentage", type="float")
     * @Serializer\Type("float")
     */
    protected $lineDiscountPercentage;

    /**
     * @var string
     *
     * @APIConnector\Column(name="IntrastatTransactionCode", type="string")
     * @Serializer\Type("string")
     */
    protected $intrastatTransactionCode;

    /**
     * @var float
     *
     * @APIConnector\Column(name="LineNum", type="float")
     * @Serializer\Type("float")
     * @Serializer\Expose()
     */
    protected $lineNum;

    /**
     * @var float
     *
     * @APIConnector\Column(name="FixedPriceCharges", type="float")
     * @Serializer\Type("float")
     */
    protected $fixedPriceCharges;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DeliverySalesTaxItemGroupCode", type="string")
     * @Serializer\Type("string")
     */
    protected $deliverySalesTaxItemGroupCode;

    /**
     * @var float
     *
     * @APIConnector\Column(name="SalesPrice", type="float", required=true)
     * @Serializer\Expose()
     * @Serializer\Type("float")
     */
    protected $salesPrice;

    /**
     * @var string
     *
     * @APIConnector\Column(name="SalesTaxGroupCode", type="string")
     * @Serializer\Type("string")
     */
    protected $salesTaxGroupCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ItemWithholdingTaxCodeGroupCode", type="string")
     * @Serializer\Type("string")
     */
    protected $itemWithholdingTaxCodeGroupCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="SalesOrderNumber", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $salesOrderNumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DeliveryCFOPCode", type="string")
     * @Serializer\Type("string")
     */
    protected $deliveryCfopCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CommissionSalesRepresentativeGroupId", type="string")
     * @Serializer\Type("string")
     */
    protected $commissionSalesRepresentativeGroupId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="IntrastatTransportModeCode", type="string")
     * @Serializer\Type("string")
     */
    protected $intrastatTransportModeCode;

    /**
     * @var string
     * @APIConnector\Id()
     * @APIConnector\Column(name="InventoryLotId", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $inventoryLotId;

    /**
     * @var int
     *
     * @APIConnector\Column(name="NGPCode", type="int")
     * @Serializer\Type("int")
     */
    protected $ngpCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DeliveryAddressStreet", type="string")
     * @Serializer\Type("string")
     */
    protected $deliveryAddressStreet;

    /**
     * @var string
     *
     * @APIConnector\Column(name="IsDeliveryAddressPrivate", type="string",
     *                                                       mType="Microsoft.Dynamics.DataEntities.NoYes")
     * @Serializer\Type("string")
     */
    protected $isDeliveryAddressPrivate;

    /**
     * @var string
     *
     * @APIConnector\Column(name="InventoryReservationMethod", type="string",
     *                                                         mType="Microsoft.Dynamics.DataEntities.ItemReservation")
     * @Serializer\Type("string")
     */
    protected $inventoryReservationMethod;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DeliveryAddressCity", type="string")
     * @Serializer\Type("string")
     */
    protected $deliveryAddressCity;

    /**
     * @var string
     *
     * @APIConnector\Column(name="WillRebateCalculationExcludeLine", type="string",
     *                                                               mType="Microsoft.Dynamics.DataEntities.NoYes")
     * @Serializer\Type("string")
     */
    protected $willRebateCalculationExcludeLine;

    /**
     * @var float
     *
     * @APIConnector\Column(name="MultilineDiscountPercentage", type="float")
     * @Serializer\Type("float")
     */
    protected $multilineDiscountPercentage;

    /**
     * @var float
     *
     * @APIConnector\Column(name="SuframaDiscountPercentage", type="float")
     * @Serializer\Type("float")
     */
    protected $suframaDiscountPercentage;

    /**
     * @var string
     *
     * @APIConnector\Column(name="IsLineStopped", type="string", mType="Microsoft.Dynamics.DataEntities.NoYes")
     * @Serializer\Type("string")
     */
    protected $isLineStopped;

    /**
     * @var string
     *
     * @APIConnector\Column(name="SalesOrderLineStatus", type="string",
     *                                                   mType="Microsoft.Dynamics.DataEntities.SalesStatus")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $salesOrderLineStatus;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CFOPCode", type="string")
     * @Serializer\Type("string")
     */
    protected $cfopCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ProductConfigurationId", type="string")
     * @Serializer\Type("string")
     */
    protected $productConfigurationId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DeliveryAddressCountryRegionId", type="string")
     * @Serializer\Type("string")
     */
    protected $deliveryAddressCountryRegionId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DeliveryBuildingCompliment", type="string")
     * @Serializer\Type("string")
     */
    protected $deliveryBuildingCompliment;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CreditNoteReasonCode", type="string")
     * @Serializer\Type("string")
     */
    protected $creditNoteReasonCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CustomsDocumentName", type="string")
     * @Serializer\Type("string")
     */
    protected $customsDocumentName;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DeliveryAddressPostBox", type="string")
     * @Serializer\Type("string")
     */
    protected $deliveryAddressPostBox;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DeliveryAddressDescription", type="string")
     * @Serializer\Type("string")
     */
    protected $deliveryAddressDescription;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="RequestedShippingDate", type="datetimez")
     * @Serializer\Type("DateTime")
     */
    protected $requestedShippingDate;

    /**
     * @var string
     *
     * @APIConnector\Column(name="EInvoiceDimensionAccountCode", type="string")
     * @Serializer\Type("string")
     */
    protected $eInvoiceDimensionAccountCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="SalesProductCategoryName", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $salesProductCategoryName;

    /**
     * @var string
     *
     * @APIConnector\Column(name="SalesTaxItemGroupCode", type="string")
     * @Serializer\Type("string")
     */
    protected $salesTaxItemGroupCode;

    /**
     * @var float
     *
     * @APIConnector\Column(name="DeliveryAddressLatitude", type="float")
     * @Serializer\Type("float")
     */
    protected $deliveryAddressLatitude;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CustomerRequisitionNumber", type="string")
     * @Serializer\Type("string")
     */
    protected $customerRequisitionNumber;

    /**
     * @var float
     *
     * @APIConnector\Column(name="IntrastatStatisticValue", type="float")
     * @Serializer\Type("float")
     */
    protected $intrastatStatisticValue;

    /**
     * @var float
     *
     * @APIConnector\Column(name="AllowedOverdeliveryPercentage", type="float")
     * @Serializer\Type("float")
     */
    protected $allowedOverdeliveryPercentage;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DeliveryAddressTimeZone", type="string",
     *                                                      mType="Microsoft.Dynamics.DataEntities.Timezone")
     * @Serializer\Type("float")
     */
    protected $deliveryAddressTimeZone;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DeliveryModeCode", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $deliveryModeCode;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="CustomsDocumentDate", type="datetimez")
     * @Serializer\Type("DateTime")
     */
    protected $customsDocumentDate;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CustomsDocumentNumber", type="string")
     * @Serializer\Type("string")
     */
    protected $customsDocumentNumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ExternalItemNumber", type="string")
     * @Serializer\Type("string")
     */
    protected $externalItemNumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ItemBatchNumber", type="string")
     * @Serializer\Type("string")
     */
    protected $itemBatchNumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="OrderLineReference", type="string")
     * @Serializer\Type("string")
     */
    protected $orderLineReference;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DeliveryAddressName", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $deliveryAddressName;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ProductColorId", type="string")
     * @Serializer\Type("string")
     */
    protected $productColorId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="IntrastatStatisticsProcedureCode", type="string")
     * @Serializer\Type("string")
     */
    protected $intrastatStatisticsProcedureCode;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="ConfirmedReceiptDate", type="datetimez")
     * @Serializer\Type("DateTime")
     */
    protected $confirmedReceiptDate;

    /**
     * @var string
     *
     * @APIConnector\Column(name="InventTransId", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $inventTransId;

    /**
     * @var int
     *
     * @APIConnector\Column(name="CustomersLineNumber", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $customersLineNumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DeliveryAddressStateId", type="string")
     * @Serializer\Type("string")
     */
    protected $deliveryAddressStateId;

    /**
     * @var string
     * @APIConnector\Id()
     * @APIConnector\Column(name="dataAreaId", type="string", required=true)
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $dataAreaId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DeliveryAddressStreetNumber", type="string")
     * @Serializer\Type("string")
     */
    protected $deliveryAddressStreetNumber;

    /**
     * @var float
     *
     * @APIConnector\Column(name="DeliveryAddressLongitude", type="float")
     * @Serializer\Type("float")
     */
    protected $deliveryAddressLongitude;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DefaultLedgerDimensionDisplayValue", type="string")
     * @Serializer\Type("string")
     */
    protected $defaultLedgerDimensionDisplayValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="IsDeliveryAddressOrderSpecific", type="string",
     *                                                             mType="Microsoft.Dynamics.DataEntities.NoYes")
     * @Serializer\Type("string")
     */
    protected $isDeliveryAddressOrderSpecific;

    /**
     * @var string
     *
     * @APIConnector\Column(name="FiscalDocumentTypeId", type="string")
     * @Serializer\Type("string")
     */
    protected $fiscalDocumentTypeId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CustomerRef", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $customerRef;

    /**
     * @var string
     *
     * @APIConnector\Column(name="Hierarchy", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $hierarchy;

    /**
     * @return float
     */
    public function getAllowedOverdeliveryPercentage(): ?float
    {
        return $this->allowedOverdeliveryPercentage;
    }

    /**
     * @param float $allowedOverdeliveryPercentage
     *
     * @return SalesOrderLine
     */
    public function setAllowedOverdeliveryPercentage(float $allowedOverdeliveryPercentage): SalesOrderLine
    {
        return $this->setTrigger('allowedOverdeliveryPercentage', $allowedOverdeliveryPercentage);
    }

    /**
     * @return float
     */
    public function getAllowedUnderdeliveryPercentage(): ?float
    {
        return $this->allowedUnderdeliveryPercentage;
    }

    /**
     * @param float $allowedUnderdeliveryPercentage
     *
     * @return SalesOrderLine
     */
    public function setAllowedUnderdeliveryPercentage(float $allowedUnderdeliveryPercentage): SalesOrderLine
    {
        return $this->setTrigger('allowedUnderdeliveryPercentage', $allowedUnderdeliveryPercentage);
    }

    /**
     * @return string
     */
    public function getBomId(): ?string
    {
        return $this->bomId;
    }

    /**
     * @param string $bomId
     *
     * @return SalesOrderLine
     */
    public function setBomId(string $bomId): SalesOrderLine
    {
        return $this->setTrigger('bomId', $bomId);
    }

    /**
     * @return string
     */
    public function getCfopCode(): ?string
    {
        return $this->cfopCode;
    }

    /**
     * @param string $cfopCode
     *
     * @return SalesOrderLine
     */
    public function setCfopCode(string $cfopCode): SalesOrderLine
    {
        return $this->setTrigger('cfopCode', $cfopCode);
    }

    /**
     * @return string
     */
    public function getCommissionSalesRepresentativeGroupId(): ?string
    {
        return $this->commissionSalesRepresentativeGroupId;
    }

    /**
     * @param string $commissionSalesRepresentativeGroupId
     *
     * @return SalesOrderLine
     */
    public function setCommissionSalesRepresentativeGroupId(
        string $commissionSalesRepresentativeGroupId
    ): SalesOrderLine {
        return $this->setTrigger('commissionSalesRepresentativeGroupId', $commissionSalesRepresentativeGroupId);
    }

    /**
     * @return \DateTime
     */
    public function getConfirmedReceiptDate(): ?\Datetime
    {
        return $this->confirmedReceiptDate;
    }

    /**
     * @param \DateTime $confirmedReceiptDate
     *
     * @return SalesOrderLine
     */
    public function setConfirmedReceiptDate(\DateTime $confirmedReceiptDate): SalesOrderLine
    {
        return $this->setTrigger('confirmedReceiptDate', $confirmedReceiptDate);
    }

    /**
     * @return \DateTime
     */
    public function getConfirmedShippingDate(): ?\Datetime
    {
        return $this->confirmedShippingDate;
    }

    /**
     * @param \DateTime $confirmedShippingDate
     *
     * @return SalesOrderLine
     */
    public function setConfirmedShippingDate(\DateTime $confirmedShippingDate): SalesOrderLine
    {
        return $this->setTrigger('confirmedShippingDate', $confirmedShippingDate);
    }

    /**
     * @return string
     */
    public function getCreditNoteReasonCode(): ?string
    {
        return $this->creditNoteReasonCode;
    }

    /**
     * @param string $creditNoteReasonCode
     *
     * @return SalesOrderLine
     */
    public function setCreditNoteReasonCode(string $creditNoteReasonCode): SalesOrderLine
    {
        return $this->setTrigger('creditNoteReasonCode', $creditNoteReasonCode);
    }

    /**
     * @return string|null
     */
    public function getCustomerRef(): ?string
    {
        return $this->customerRef;
    }

    /**
     * @param string|null $customerRef
     *
     * @return SalesOrderLine
     */
    public function setCustomerRef(?string $customerRef): SalesOrderLine
    {
        return $this->setTrigger('customerRef', $customerRef);
    }

    /**
     * @return string
     */
    public function getCustomerRequisitionNumber(): ?string
    {
        return $this->customerRequisitionNumber;
    }

    /**
     * @param string $customerRequisitionNumber
     *
     * @return SalesOrderLine
     */
    public function setCustomerRequisitionNumber(string $customerRequisitionNumber): SalesOrderLine
    {
        return $this->setTrigger('customerRequisitionNumber', $customerRequisitionNumber);
    }

    /**
     * @return int
     */
    public function getCustomersLineNumber(): ?int
    {
        return $this->customersLineNumber;
    }

    /**
     * @param int $customersLineNumber
     *
     * @return SalesOrderLine
     */
    public function setCustomersLineNumber(int $customersLineNumber): SalesOrderLine
    {
        return $this->setTrigger('customersLineNumber', $customersLineNumber);
    }

    /**
     * @return \DateTime
     */
    public function getCustomsDocumentDate(): ?\Datetime
    {
        return $this->customsDocumentDate;
    }

    /**
     * @param \DateTime $customsDocumentDate
     *
     * @return SalesOrderLine
     */
    public function setCustomsDocumentDate(\DateTime $customsDocumentDate): SalesOrderLine
    {
        return $this->setTrigger('customsDocumentDate', $customsDocumentDate);
    }

    /**
     * @return string
     */
    public function getCustomsDocumentName(): ?string
    {
        return $this->customsDocumentName;
    }

    /**
     * @param string $customsDocumentName
     *
     * @return SalesOrderLine
     */
    public function setCustomsDocumentName(string $customsDocumentName): SalesOrderLine
    {
        return $this->setTrigger('customsDocumentName', $customsDocumentName);
    }

    /**
     * @return string
     */
    public function getCustomsDocumentNumber(): ?string
    {
        return $this->customsDocumentNumber;
    }

    /**
     * @param string $customsDocumentNumber
     *
     * @return SalesOrderLine
     */
    public function setCustomsDocumentNumber(string $customsDocumentNumber): SalesOrderLine
    {
        return $this->setTrigger('customsDocumentNumber', $customsDocumentNumber);
    }

    /**
     * @return string
     */
    public function getDataAreaId(): ?string
    {
        return $this->dataAreaId;
    }

    /**
     * @param string $dataAreaId
     *
     * @return SalesOrderLine
     */
    public function setDataAreaId(string $dataAreaId): SalesOrderLine
    {
        return $this->setTrigger('dataAreaId', $dataAreaId);
    }

    /**
     * @return string
     */
    public function getDefaultLedgerDimensionDisplayValue(): ?string
    {
        return $this->defaultLedgerDimensionDisplayValue;
    }

    /**
     * @param string $defaultLedgerDimensionDisplayValue
     *
     * @return SalesOrderLine
     */
    public function setDefaultLedgerDimensionDisplayValue(string $defaultLedgerDimensionDisplayValue): SalesOrderLine
    {
        return $this->setTrigger('defaultLedgerDimensionDisplayValue', $defaultLedgerDimensionDisplayValue);
    }

    /**
     * @return string
     */
    public function getDeliveryAddressCity(): ?string
    {
        return $this->deliveryAddressCity;
    }

    /**
     * @param string $deliveryAddressCity
     *
     * @return SalesOrderLine
     */
    public function setDeliveryAddressCity(string $deliveryAddressCity): SalesOrderLine
    {
        return $this->setTrigger('deliveryAddressCity', $deliveryAddressCity);
    }

    /**
     * @return string
     */
    public function getDeliveryAddressCountryRegionId(): ?string
    {
        return $this->deliveryAddressCountryRegionId;
    }

    /**
     * @param string $deliveryAddressCountryRegionId
     *
     * @return SalesOrderLine
     */
    public function setDeliveryAddressCountryRegionId(string $deliveryAddressCountryRegionId): SalesOrderLine
    {
        return $this->setTrigger('deliveryAddressCountryRegionId', $deliveryAddressCountryRegionId);
    }

    /**
     * @return string
     */
    public function getDeliveryAddressCountyId(): ?string
    {
        return $this->deliveryAddressCountyId;
    }

    /**
     * @param string $deliveryAddressCountyId
     *
     * @return SalesOrderLine
     */
    public function setDeliveryAddressCountyId(string $deliveryAddressCountyId): SalesOrderLine
    {
        return $this->setTrigger('deliveryAddressCountyId', $deliveryAddressCountyId);
    }

    /**
     * @return string
     */
    public function getDeliveryAddressDescription(): ?string
    {
        return $this->deliveryAddressDescription;
    }

    /**
     * @param string $deliveryAddressDescription
     *
     * @return SalesOrderLine
     */
    public function setDeliveryAddressDescription(string $deliveryAddressDescription): SalesOrderLine
    {
        return $this->setTrigger('deliveryAddressDescription', $deliveryAddressDescription);
    }

    /**
     * @return string
     */
    public function getDeliveryAddressDistrictName(): ?string
    {
        return $this->deliveryAddressDistrictName;
    }

    /**
     * @param string $deliveryAddressDistrictName
     *
     * @return SalesOrderLine
     */
    public function setDeliveryAddressDistrictName(string $deliveryAddressDistrictName): SalesOrderLine
    {
        return $this->setTrigger('deliveryAddressDistrictName', $deliveryAddressDistrictName);
    }

    /**
     * @return string
     */
    public function getDeliveryAddressDunsNumber(): ?string
    {
        return $this->deliveryAddressDunsNumber;
    }

    /**
     * @param string $deliveryAddressDunsNumber
     *
     * @return SalesOrderLine
     */
    public function setDeliveryAddressDunsNumber(string $deliveryAddressDunsNumber): SalesOrderLine
    {
        return $this->setTrigger('deliveryAddressDunsNumber', $deliveryAddressDunsNumber);
    }

    /**
     * @return float
     */
    public function getDeliveryAddressLatitude(): ?float
    {
        return $this->deliveryAddressLatitude;
    }

    /**
     * @param float $deliveryAddressLatitude
     *
     * @return SalesOrderLine
     */
    public function setDeliveryAddressLatitude(float $deliveryAddressLatitude): SalesOrderLine
    {
        return $this->setTrigger('deliveryAddressLatitude', $deliveryAddressLatitude);
    }

    /**
     * @return string
     */
    public function getDeliveryAddressLocationId(): ?string
    {
        return $this->deliveryAddressLocationId;
    }

    /**
     * @param string $deliveryAddressLocationId
     *
     * @return SalesOrderLine
     */
    public function setDeliveryAddressLocationId(string $deliveryAddressLocationId): SalesOrderLine
    {
        return $this->setTrigger('deliveryAddressLocationId', $deliveryAddressLocationId);
    }

    /**
     * @return float
     */
    public function getDeliveryAddressLongitude(): ?float
    {
        return $this->deliveryAddressLongitude;
    }

    /**
     * @param float $deliveryAddressLongitude
     *
     * @return SalesOrderLine
     */
    public function setDeliveryAddressLongitude(float $deliveryAddressLongitude): SalesOrderLine
    {
        return $this->setTrigger('deliveryAddressLongitude', $deliveryAddressLongitude);
    }

    /**
     * @return string
     */
    public function getDeliveryAddressName(): ?string
    {
        return $this->deliveryAddressName;
    }

    /**
     * @param string $deliveryAddressName
     *
     * @return SalesOrderLine
     */
    public function setDeliveryAddressName(string $deliveryAddressName): SalesOrderLine
    {
        return $this->setTrigger('deliveryAddressName', $deliveryAddressName);
    }

    /**
     * @return string
     */
    public function getDeliveryAddressPostBox(): ?string
    {
        return $this->deliveryAddressPostBox;
    }

    /**
     * @param string $deliveryAddressPostBox
     *
     * @return SalesOrderLine
     */
    public function setDeliveryAddressPostBox(string $deliveryAddressPostBox): SalesOrderLine
    {
        return $this->setTrigger('deliveryAddressPostBox', $deliveryAddressPostBox);
    }

    /**
     * @return string
     */
    public function getDeliveryAddressStateId(): ?string
    {
        return $this->deliveryAddressStateId;
    }

    /**
     * @param string $deliveryAddressStateId
     *
     * @return SalesOrderLine
     */
    public function setDeliveryAddressStateId(string $deliveryAddressStateId): SalesOrderLine
    {
        return $this->setTrigger('deliveryAddressStateId', $deliveryAddressStateId);
    }

    /**
     * @return string
     */
    public function getDeliveryAddressStreet(): ?string
    {
        return $this->deliveryAddressStreet;
    }

    /**
     * @param string $deliveryAddressStreet
     *
     * @return SalesOrderLine
     */
    public function setDeliveryAddressStreet(string $deliveryAddressStreet): SalesOrderLine
    {
        return $this->setTrigger('deliveryAddressStreet', $deliveryAddressStreet);
    }

    /**
     * @return string
     */
    public function getDeliveryAddressStreetNumber(): ?string
    {
        return $this->deliveryAddressStreetNumber;
    }

    /**
     * @param string $deliveryAddressStreetNumber
     *
     * @return SalesOrderLine
     */
    public function setDeliveryAddressStreetNumber(string $deliveryAddressStreetNumber): SalesOrderLine
    {
        return $this->setTrigger('deliveryAddressStreetNumber', $deliveryAddressStreetNumber);
    }

    /**
     * @return string
     */
    public function getDeliveryAddressTimeZone(): ?string
    {
        return $this->deliveryAddressTimeZone;
    }

    /**
     * @param string $deliveryAddressTimeZone
     *
     * @return SalesOrderLine
     */
    public function setDeliveryAddressTimeZone(string $deliveryAddressTimeZone): SalesOrderLine
    {
        return $this->setTrigger('deliveryAddressTimeZone', $deliveryAddressTimeZone);
    }

    /**
     * @return string
     */
    public function getDeliveryAddressZipCode(): ?string
    {
        return $this->deliveryAddressZipCode;
    }

    /**
     * @param string $deliveryAddressZipCode
     *
     * @return SalesOrderLine
     */
    public function setDeliveryAddressZipCode(string $deliveryAddressZipCode): SalesOrderLine
    {
        return $this->setTrigger('deliveryAddressZipCode', $deliveryAddressZipCode);
    }

    /**
     * @return string
     */
    public function getDeliveryBuildingCompliment(): ?string
    {
        return $this->deliveryBuildingCompliment;
    }

    /**
     * @param string $deliveryBuildingCompliment
     *
     * @return SalesOrderLine
     */
    public function setDeliveryBuildingCompliment(string $deliveryBuildingCompliment): SalesOrderLine
    {
        return $this->setTrigger('deliveryBuildingCompliment', $deliveryBuildingCompliment);
    }

    /**
     * @return string
     */
    public function getDeliveryCfopCode(): ?string
    {
        return $this->deliveryCfopCode;
    }

    /**
     * @param string $deliveryCfopCode
     *
     * @return SalesOrderLine
     */
    public function setDeliveryCfopCode(string $deliveryCfopCode): SalesOrderLine
    {
        return $this->setTrigger('deliveryCfopCode', $deliveryCfopCode);
    }

    /**
     * @return string
     */
    public function getDeliveryModeCode(): ?string
    {
        return $this->deliveryModeCode;
    }

    /**
     * @param string $deliveryModeCode
     *
     * @return SalesOrderLine
     */
    public function setDeliveryModeCode(?string $deliveryModeCode): SalesOrderLine
    {
        return $this->setTrigger('deliveryModeCode', $deliveryModeCode);
    }

    /**
     * @return string
     */
    public function getDeliverySalesTaxGroupCode(): ?string
    {
        return $this->deliverySalesTaxGroupCode;
    }

    /**
     * @param string $deliverySalesTaxGroupCode
     *
     * @return SalesOrderLine
     */
    public function setDeliverySalesTaxGroupCode(string $deliverySalesTaxGroupCode): SalesOrderLine
    {
        return $this->setTrigger('deliverySalesTaxGroupCode', $deliverySalesTaxGroupCode);
    }

    /**
     * @return string
     */
    public function getDeliverySalesTaxItemGroupCode(): ?string
    {
        return $this->deliverySalesTaxItemGroupCode;
    }

    /**
     * @param string $deliverySalesTaxItemGroupCode
     *
     * @return SalesOrderLine
     */
    public function setDeliverySalesTaxItemGroupCode(string $deliverySalesTaxItemGroupCode): SalesOrderLine
    {
        return $this->setTrigger('deliverySalesTaxItemGroupCode', $deliverySalesTaxItemGroupCode);
    }

    /**
     * @return string
     */
    public function getDeliveryTermsId(): ?string
    {
        return $this->deliveryTermsId;
    }

    /**
     * @param string $deliveryTermsId
     *
     * @return SalesOrderLine
     */
    public function setDeliveryTermsId(string $deliveryTermsId): SalesOrderLine
    {
        return $this->setTrigger('deliveryTermsId', $deliveryTermsId);
    }

    /**
     * @return string
     */
    public function getEInvoiceDimensionAccountCode(): ?string
    {
        return $this->eInvoiceDimensionAccountCode;
    }

    /**
     * @param string $eInvoiceDimensionAccountCode
     *
     * @return SalesOrderLine
     */
    public function setEInvoiceDimensionAccountCode(string $eInvoiceDimensionAccountCode): SalesOrderLine
    {
        return $this->setTrigger('eInvoiceDimensionAccountCode', $eInvoiceDimensionAccountCode);
    }

    /**
     * @return string
     */
    public function getEInvoicePropertyNumber(): ?string
    {
        return $this->eInvoicePropertyNumber;
    }

    /**
     * @param string $eInvoicePropertyNumber
     *
     * @return SalesOrderLine
     */
    public function setEInvoicePropertyNumber(string $eInvoicePropertyNumber): SalesOrderLine
    {
        return $this->setTrigger('eInvoicePropertyNumber', $eInvoicePropertyNumber);
    }

    /**
     * @return string
     */
    public function getExternalItemNumber(): ?string
    {
        return $this->externalItemNumber;
    }

    /**
     * @param string $externalItemNumber
     *
     * @return SalesOrderLine
     */
    public function setExternalItemNumber(string $externalItemNumber): SalesOrderLine
    {
        return $this->setTrigger('externalItemNumber', $externalItemNumber);
    }

    /**
     * @return string
     */
    public function getFiscalDocumentTypeId(): ?string
    {
        return $this->fiscalDocumentTypeId;
    }

    /**
     * @param string $fiscalDocumentTypeId
     *
     * @return SalesOrderLine
     */
    public function setFiscalDocumentTypeId(string $fiscalDocumentTypeId): SalesOrderLine
    {
        return $this->setTrigger('fiscalDocumentTypeId', $fiscalDocumentTypeId);
    }

    /**
     * @return float
     */
    public function getFixedPriceCharges(): ?float
    {
        return $this->fixedPriceCharges;
    }

    /**
     * @param float $fixedPriceCharges
     *
     * @return SalesOrderLine
     */
    public function setFixedPriceCharges(float $fixedPriceCharges): SalesOrderLine
    {
        return $this->setTrigger('fixedPriceCharges', $fixedPriceCharges);
    }

    /**
     * @return string
     */
    public function getFormattedDelveryAddress(): ?string
    {
        return $this->formattedDelveryAddress;
    }

    /**
     * @param string $formattedDelveryAddress
     *
     * @return SalesOrderLine
     */
    public function setFormattedDelveryAddress(string $formattedDelveryAddress): SalesOrderLine
    {
        return $this->setTrigger('formattedDelveryAddress', $formattedDelveryAddress);
    }

    /**
     * @return string|null
     */
    public function getHierarchy(): ?string
    {
        return $this->hierarchy;
    }

    /**
     * @param string|null $hierarchy
     *
     * @return SalesOrderLine
     */
    public function setHierarchy(?string $hierarchy): SalesOrderLine
    {
        return $this->setTrigger('hierarchy', $hierarchy);
    }

    /**
     * @return string
     */
    public function getIntrastatCommodityCode(): ?string
    {
        return $this->intrastatCommodityCode;
    }

    /**
     * @param string $intrastatCommodityCode
     *
     * @return SalesOrderLine
     */
    public function setIntrastatCommodityCode(string $intrastatCommodityCode): SalesOrderLine
    {
        return $this->setTrigger('intrastatCommodityCode', $intrastatCommodityCode);
    }

    /**
     * @return string
     */
    public function getIntrastatPortId(): ?string
    {
        return $this->intrastatPortId;
    }

    /**
     * @param string $intrastatPortId
     *
     * @return SalesOrderLine
     */
    public function setIntrastatPortId(string $intrastatPortId): SalesOrderLine
    {
        return $this->setTrigger('intrastatPortId', $intrastatPortId);
    }

    /**
     * @return string
     */
    public function getIntrastatSpecialMovementCode(): ?string
    {
        return $this->intrastatSpecialMovementCode;
    }

    /**
     * @param string $intrastatSpecialMovementCode
     *
     * @return SalesOrderLine
     */
    public function setIntrastatSpecialMovementCode(string $intrastatSpecialMovementCode): SalesOrderLine
    {
        return $this->setTrigger('intrastatSpecialMovementCode', $intrastatSpecialMovementCode);
    }

    /**
     * @return float
     */
    public function getIntrastatStatisticValue(): ?float
    {
        return $this->intrastatStatisticValue;
    }

    /**
     * @param float $intrastatStatisticValue
     *
     * @return SalesOrderLine
     */
    public function setIntrastatStatisticValue(float $intrastatStatisticValue): SalesOrderLine
    {
        return $this->setTrigger('intrastatStatisticValue', $intrastatStatisticValue);
    }

    /**
     * @return string
     */
    public function getIntrastatStatisticsProcedureCode(): ?string
    {
        return $this->intrastatStatisticsProcedureCode;
    }

    /**
     * @param string $intrastatStatisticsProcedureCode
     *
     * @return SalesOrderLine
     */
    public function setIntrastatStatisticsProcedureCode(string $intrastatStatisticsProcedureCode): SalesOrderLine
    {
        return $this->setTrigger('intrastatStatisticsProcedureCode', $intrastatStatisticsProcedureCode);
    }

    /**
     * @return string
     */
    public function getIntrastatTransactionCode(): ?string
    {
        return $this->intrastatTransactionCode;
    }

    /**
     * @param string $intrastatTransactionCode
     *
     * @return SalesOrderLine
     */
    public function setIntrastatTransactionCode(string $intrastatTransactionCode): SalesOrderLine
    {
        return $this->setTrigger('intrastatTransactionCode', $intrastatTransactionCode);
    }

    /**
     * @return float
     */
    public function getLineNum(): ?float
    {
        return $this->lineNum;
    }

    /**
     * @param float|null $lineNum
     *
     * @return SalesOrderLine
     */
    public function setLineNum(?float $lineNum): self
    {
        return $this->setTrigger('lineNum', $lineNum);
    }

    /**
     * @return string
     */
    public function getIntrastatTransportModeCode(): ?string
    {
        return $this->intrastatTransportModeCode;
    }

    /**
     * @param string $intrastatTransportModeCode
     *
     * @return SalesOrderLine
     */
    public function setIntrastatTransportModeCode(string $intrastatTransportModeCode): SalesOrderLine
    {
        return $this->setTrigger('intrastatTransportModeCode', $intrastatTransportModeCode);
    }

    /**
     * @return string
     */
    public function getInventTransId(): ?string
    {
        return $this->inventTransId;
    }

    /**
     * @param string $inventTransId
     *
     * @return SalesOrderLine
     */
    public function setInventTransId(string $inventTransId): SalesOrderLine
    {
        return $this->setTrigger('inventTransId', $inventTransId);
    }

    /**
     * @return string
     */
    public function getInventoryLotId(): ?string
    {
        return $this->inventoryLotId;
    }

    /**
     * @param string $inventoryLotId
     *
     * @return SalesOrderLine
     */
    public function setInventoryLotId(string $inventoryLotId): SalesOrderLine
    {
        return $this->setTrigger('inventoryLotId', $inventoryLotId);
    }

    /**
     * @return string
     */
    public function getInventoryReservationMethod(): ?string
    {
        return $this->inventoryReservationMethod;
    }

    /**
     * @param string $inventoryReservationMethod
     *
     * @return SalesOrderLine
     */
    public function setInventoryReservationMethod(string $inventoryReservationMethod): SalesOrderLine
    {
        return $this->setTrigger('inventoryReservationMethod', $inventoryReservationMethod);
    }

    /**
     * @return string
     */
    public function getIsDeliveryAddressOrderSpecific(): ?string
    {
        return $this->isDeliveryAddressOrderSpecific;
    }

    /**
     * @param string $isDeliveryAddressOrderSpecific
     *
     * @return SalesOrderLine
     */
    public function setIsDeliveryAddressOrderSpecific(string $isDeliveryAddressOrderSpecific): SalesOrderLine
    {
        return $this->setTrigger('isDeliveryAddressOrderSpecific', $isDeliveryAddressOrderSpecific);
    }

    /**
     * @return string
     */
    public function getIsDeliveryAddressPrivate(): ?string
    {
        return $this->isDeliveryAddressPrivate;
    }

    /**
     * @param string $isDeliveryAddressPrivate
     *
     * @return SalesOrderLine
     */
    public function setIsDeliveryAddressPrivate(string $isDeliveryAddressPrivate): SalesOrderLine
    {
        return $this->setTrigger('isDeliveryAddressPrivate', $isDeliveryAddressPrivate);
    }

    /**
     * @return string
     */
    public function getIsIntrastatTriangularDeal(): ?string
    {
        return $this->isIntrastatTriangularDeal;
    }

    /**
     * @param string $isIntrastatTriangularDeal
     *
     * @return SalesOrderLine
     */
    public function setIsIntrastatTriangularDeal(string $isIntrastatTriangularDeal): SalesOrderLine
    {
        return $this->setTrigger('isIntrastatTriangularDeal', $isIntrastatTriangularDeal);
    }

    /**
     * @return string
     */
    public function getIsLineStopped(): ?string
    {
        return $this->isLineStopped;
    }

    /**
     * @param string $isLineStopped
     *
     * @return SalesOrderLine
     */
    public function setIsLineStopped(string $isLineStopped): SalesOrderLine
    {
        return $this->setTrigger('isLineStopped', $isLineStopped);
    }

    /**
     * @return string
     */
    public function getItemBatchNumber(): ?string
    {
        return $this->itemBatchNumber;
    }

    /**
     * @param string $itemBatchNumber
     *
     * @return SalesOrderLine
     */
    public function setItemBatchNumber(string $itemBatchNumber): SalesOrderLine
    {
        return $this->setTrigger('itemBatchNumber', $itemBatchNumber);
    }

    /**
     * @return string
     */
    public function getItemNumber(): ?string
    {
        return $this->itemNumber;
    }

    /**
     * @param string $itemNumber
     *
     * @return SalesOrderLine
     */
    public function setItemNumber(string $itemNumber): SalesOrderLine
    {
        return $this->setTrigger('itemNumber', $itemNumber);
    }

    /**
     * @return string
     */
    public function getItemWithholdingTaxCodeGroupCode(): ?string
    {
        return $this->itemWithholdingTaxCodeGroupCode;
    }

    /**
     * @param string $itemWithholdingTaxCodeGroupCode
     *
     * @return SalesOrderLine
     */
    public function setItemWithholdingTaxCodeGroupCode(string $itemWithholdingTaxCodeGroupCode): SalesOrderLine
    {
        return $this->setTrigger('itemWithholdingTaxCodeGroupCode', $itemWithholdingTaxCodeGroupCode);
    }

    /**
     * @return float
     */
    public function getLineAmount(): ?float
    {
        return $this->lineAmount;
    }

    /**
     * @param float $lineAmount
     *
     * @return SalesOrderLine
     */
    public function setLineAmount(float $lineAmount): SalesOrderLine
    {
        return $this->setTrigger('lineAmount', $lineAmount);
    }

    /**
     * @return string
     */
    public function getLineDescription(): ?string
    {
        return $this->lineDescription;
    }

    /**
     * @param string $lineDescription
     *
     * @return SalesOrderLine
     */
    public function setLineDescription(string $lineDescription): SalesOrderLine
    {
        return $this->setTrigger('lineDescription', $lineDescription);
    }

    /**
     * @return float
     */
    public function getLineDiscountAmount(): ?float
    {
        return $this->lineDiscountAmount;
    }

    /**
     * @param float $lineDiscountAmount
     *
     * @return SalesOrderLine
     */
    public function setLineDiscountAmount(float $lineDiscountAmount): SalesOrderLine
    {
        return $this->setTrigger('lineDiscountAmount', $lineDiscountAmount);
    }

    /**
     * @return float
     */
    public function getLineDiscountPercentage(): ?float
    {
        return $this->lineDiscountPercentage;
    }

    /**
     * @param float $lineDiscountPercentage
     *
     * @return SalesOrderLine
     */
    public function setLineDiscountPercentage(float $lineDiscountPercentage): SalesOrderLine
    {
        return $this->setTrigger('lineDiscountPercentage', $lineDiscountPercentage);
    }

    /**
     * @return string
     */
    public function getMainAccountIdDisplayValue(): ?string
    {
        return $this->mainAccountIdDisplayValue;
    }

    /**
     * @param string $mainAccountIdDisplayValue
     *
     * @return SalesOrderLine
     */
    public function setMainAccountIdDisplayValue(string $mainAccountIdDisplayValue): SalesOrderLine
    {
        return $this->setTrigger('mainAccountIdDisplayValue', $mainAccountIdDisplayValue);
    }

    /**
     * @return float
     */
    public function getMultilineDiscountAmount(): ?float
    {
        return $this->multilineDiscountAmount;
    }

    /**
     * @param float $multilineDiscountAmount
     *
     * @return SalesOrderLine
     */
    public function setMultilineDiscountAmount(float $multilineDiscountAmount): SalesOrderLine
    {
        return $this->setTrigger('multilineDiscountAmount', $multilineDiscountAmount);
    }

    /**
     * @return float
     */
    public function getMultilineDiscountPercentage(): ?float
    {
        return $this->multilineDiscountPercentage;
    }

    /**
     * @param float $multilineDiscountPercentage
     *
     * @return SalesOrderLine
     */
    public function setMultilineDiscountPercentage(float $multilineDiscountPercentage): SalesOrderLine
    {
        return $this->setTrigger('multilineDiscountPercentage', $multilineDiscountPercentage);
    }

    /**
     * @return int
     */
    public function getNgpCode(): ?int
    {
        return $this->ngpCode;
    }

    /**
     * @param int $ngpCode
     *
     * @return SalesOrderLine
     */
    public function setNgpCode(int $ngpCode): SalesOrderLine
    {
        return $this->setTrigger('ngpCode', $ngpCode);
    }

    /**
     * @return string
     */
    public function getOrderLineReference(): ?string
    {
        return $this->orderLineReference;
    }

    /**
     * @param string|null $orderLineReference
     *
     * @return SalesOrderLine
     */
    public function setOrderLineReference(?string $orderLineReference): SalesOrderLine
    {
        return $this->setTrigger('orderLineReference', $orderLineReference);
    }

    /**
     * @return string
     */
    public function getOrderedInventoryStatusId(): ?string
    {
        return $this->orderedInventoryStatusId;
    }

    /**
     * @param string $orderedInventoryStatusId
     *
     * @return SalesOrderLine
     */
    public function setOrderedInventoryStatusId(string $orderedInventoryStatusId): SalesOrderLine
    {
        return $this->setTrigger('orderedInventoryStatusId', $orderedInventoryStatusId);
    }

    /**
     * @return float
     */
    public function getOrderedSalesQuantity(): ?float
    {
        return $this->orderedSalesQuantity;
    }

    /**
     * @param float $orderedSalesQuantity
     *
     * @return SalesOrderLine
     */
    public function setOrderedSalesQuantity(float $orderedSalesQuantity): SalesOrderLine
    {
        return $this->setTrigger('orderedSalesQuantity', $orderedSalesQuantity);
    }

    /**
     * @return string
     */
    public function getPackingUnitSymbol(): ?string
    {
        return $this->packingUnitSymbol;
    }

    /**
     * @param string $packingUnitSymbol
     *
     * @return SalesOrderLine
     */
    public function setPackingUnitSymbol(string $packingUnitSymbol): SalesOrderLine
    {
        return $this->setTrigger('packingUnitSymbol', $packingUnitSymbol);
    }

    /**
     * @return string
     */
    public function getProductColorId(): ?string
    {
        return $this->productColorId;
    }

    /**
     * @param string $productColorId
     *
     * @return SalesOrderLine
     */
    public function setProductColorId(string $productColorId): SalesOrderLine
    {
        return $this->setTrigger('productColorId', $productColorId);
    }

    /**
     * @return string
     */
    public function getProductConfigurationId(): ?string
    {
        return $this->productConfigurationId;
    }

    /**
     * @param string $productConfigurationId
     *
     * @return SalesOrderLine
     */
    public function setProductConfigurationId(string $productConfigurationId): SalesOrderLine
    {
        return $this->setTrigger('productConfigurationId', $productConfigurationId);
    }

    /**
     * @return string
     */
    public function getProductSizeId(): ?string
    {
        return $this->productSizeId;
    }

    /**
     * @param string $productSizeId
     *
     * @return SalesOrderLine
     */
    public function setProductSizeId(string $productSizeId): SalesOrderLine
    {
        return $this->setTrigger('productSizeId', $productSizeId);
    }

    /**
     * @return string
     */
    public function getProductStyleId(): ?string
    {
        return $this->productStyleId;
    }

    /**
     * @param string $productStyleId
     *
     * @return SalesOrderLine
     */
    public function setProductStyleId(string $productStyleId): SalesOrderLine
    {
        return $this->setTrigger('productStyleId', $productStyleId);
    }

    /**
     * @return \DateTime
     */
    public function getRequestedReceiptDate(): ?\Datetime
    {
        return $this->requestedReceiptDate;
    }

    /**
     * @param \DateTime $requestedReceiptDate
     *
     * @return SalesOrderLine
     */
    public function setRequestedReceiptDate(\DateTime $requestedReceiptDate): SalesOrderLine
    {
        return $this->setTrigger('requestedReceiptDate', $requestedReceiptDate);
    }

    /**
     * @return \DateTime
     */
    public function getRequestedShippingDate(): ?\Datetime
    {
        return $this->requestedShippingDate;
    }

    /**
     * @param \DateTime $requestedShippingDate
     *
     * @return SalesOrderLine
     */
    public function setRequestedShippingDate(\DateTime $requestedShippingDate): SalesOrderLine
    {
        return $this->setTrigger('requestedShippingDate', $requestedShippingDate);
    }

    /**
     * @return string
     */
    public function getRouteId(): ?string
    {
        return $this->routeId;
    }

    /**
     * @param string $routeId
     *
     * @return SalesOrderLine
     */
    public function setRouteId(string $routeId): SalesOrderLine
    {
        return $this->setTrigger('routeId', $routeId);
    }

    /**
     * @return string
     */
    public function getSalesOrderLineStatus(): ?string
    {
        return $this->salesOrderLineStatus;
    }

    /**
     * @param string $salesOrderLineStatus
     *
     * @return SalesOrderLine
     */
    public function setSalesOrderLineStatus(string $salesOrderLineStatus): SalesOrderLine
    {
        return $this->setTrigger('salesOrderLineStatus', $salesOrderLineStatus);
    }

    /**
     * @return string
     */
    public function getSalesOrderNumber(): ?string
    {
        return $this->salesOrderNumber;
    }

    /**
     * @param string $salesOrderNumber
     *
     * @return SalesOrderLine
     */
    public function setSalesOrderNumber(string $salesOrderNumber): SalesOrderLine
    {
        return $this->setTrigger('salesOrderNumber', $salesOrderNumber);
    }

    /**
     * @return string
     */
    public function getSalesOrderPromisingMethod(): ?string
    {
        return $this->salesOrderPromisingMethod;
    }

    /**
     * @param string $salesOrderPromisingMethod
     *
     * @return SalesOrderLine
     */
    public function setSalesOrderPromisingMethod(string $salesOrderPromisingMethod): SalesOrderLine
    {
        return $this->setTrigger('salesOrderPromisingMethod', $salesOrderPromisingMethod);
    }

    /**
     * @return float
     */
    public function getSalesPrice(): ?float
    {
        return $this->salesPrice;
    }

    /**
     * @param float $salesPrice
     *
     * @return SalesOrderLine
     */
    public function setSalesPrice(float $salesPrice): SalesOrderLine
    {
        return $this->setTrigger('salesPrice', $salesPrice);
    }

    /**
     * @return float
     */
    public function getSalesPriceQuantity(): ?float
    {
        return $this->salesPriceQuantity;
    }

    /**
     * @param float $salesPriceQuantity
     *
     * @return SalesOrderLine
     */
    public function setSalesPriceQuantity(float $salesPriceQuantity): SalesOrderLine
    {
        return $this->setTrigger('salesPriceQuantity', $salesPriceQuantity);
    }

    /**
     * @return string
     */
    public function getSalesProductCategoryName(): ?string
    {
        return $this->salesProductCategoryName;
    }

    /**
     * @param string $salesProductCategoryName
     *
     * @return SalesOrderLine
     */
    public function setSalesProductCategoryName(string $salesProductCategoryName): SalesOrderLine
    {
        return $this->setTrigger('salesProductCategoryName', $salesProductCategoryName);
    }

    /**
     * @return string
     */
    public function getSalesRebateProductGroupId(): ?string
    {
        return $this->salesRebateProductGroupId;
    }

    /**
     * @param string $salesRebateProductGroupId
     *
     * @return SalesOrderLine
     */
    public function setSalesRebateProductGroupId(string $salesRebateProductGroupId): SalesOrderLine
    {
        return $this->setTrigger('salesRebateProductGroupId', $salesRebateProductGroupId);
    }

    /**
     * @return string
     */
    public function getSalesTaxGroupCode(): ?string
    {
        return $this->salesTaxGroupCode;
    }

    /**
     * @param string $salesTaxGroupCode
     *
     * @return SalesOrderLine
     */
    public function setSalesTaxGroupCode(string $salesTaxGroupCode): SalesOrderLine
    {
        return $this->setTrigger('salesTaxGroupCode', $salesTaxGroupCode);
    }

    /**
     * @return string
     */
    public function getSalesTaxItemGroupCode(): ?string
    {
        return $this->salesTaxItemGroupCode;
    }

    /**
     * @param string $salesTaxItemGroupCode
     *
     * @return SalesOrderLine
     */
    public function setSalesTaxItemGroupCode(string $salesTaxItemGroupCode): SalesOrderLine
    {
        return $this->setTrigger('salesTaxItemGroupCode', $salesTaxItemGroupCode);
    }

    /**
     * @return string
     */
    public function getSalesUnitSymbol(): ?string
    {
        return $this->salesUnitSymbol;
    }

    /**
     * @param string $salesUnitSymbol
     *
     * @return SalesOrderLine
     */
    public function setSalesUnitSymbol(string $salesUnitSymbol): SalesOrderLine
    {
        return $this->setTrigger('salesUnitSymbol', $salesUnitSymbol);
    }

    /**
     * @return string
     */
    public function getServiceFiscalInformationCode(): ?string
    {
        return $this->serviceFiscalInformationCode;
    }

    /**
     * @param string $serviceFiscalInformationCode
     *
     * @return SalesOrderLine
     */
    public function setServiceFiscalInformationCode(string $serviceFiscalInformationCode): SalesOrderLine
    {
        return $this->setTrigger('serviceFiscalInformationCode', $serviceFiscalInformationCode);
    }

    /**
     * @return string
     */
    public function getServiceOrderNumber(): ?string
    {
        return $this->serviceOrderNumber;
    }

    /**
     * @param string $serviceOrderNumber
     *
     * @return SalesOrderLine
     */
    public function setServiceOrderNumber(string $serviceOrderNumber): SalesOrderLine
    {
        return $this->setTrigger('serviceOrderNumber', $serviceOrderNumber);
    }

    /**
     * @return string
     */
    public function getShippingSiteId(): ?string
    {
        return $this->shippingSiteId;
    }

    /**
     * @param string $shippingSiteId
     *
     * @return SalesOrderLine
     */
    public function setShippingSiteId(string $shippingSiteId): SalesOrderLine
    {
        return $this->setTrigger('shippingSiteId', $shippingSiteId);
    }

    /**
     * @return string
     */
    public function getShippingWarehouseId(): ?string
    {
        return $this->shippingWarehouseId;
    }

    /**
     * @param string $shippingWarehouseId
     *
     * @return SalesOrderLine
     */
    public function setShippingWarehouseId(string $shippingWarehouseId): SalesOrderLine
    {
        return $this->setTrigger('shippingWarehouseId', $shippingWarehouseId);
    }

    /**
     * @return float
     */
    public function getSuframaDiscountPercentage(): ?float
    {
        return $this->suframaDiscountPercentage;
    }

    /**
     * @param float $suframaDiscountPercentage
     *
     * @return SalesOrderLine
     */
    public function setSuframaDiscountPercentage(float $suframaDiscountPercentage): SalesOrderLine
    {
        return $this->setTrigger('suframaDiscountPercentage', $suframaDiscountPercentage);
    }

    /**
     * @return string
     */
    public function getWillAutomaticInventoryReservationConsiderBatchAttributes(): ?string
    {
        return $this->willAutomaticInventoryReservationConsiderBatchAttributes;
    }

    /**
     * @param string $willAutomaticInventoryReservationConsiderBatchAttributes
     *
     * @return SalesOrderLine
     */
    public function setWillAutomaticInventoryReservationConsiderBatchAttributes(
        string $willAutomaticInventoryReservationConsiderBatchAttributes
    ): SalesOrderLine {
        return $this->setTrigger(
            'willAutomaticInventoryReservationConsiderBatchAttributes',
            $willAutomaticInventoryReservationConsiderBatchAttributes
        );
    }

    /**
     * @return string
     */
    public function getWillRebateCalculationExcludeLine(): ?string
    {
        return $this->willRebateCalculationExcludeLine;
    }

    /**
     * @param string $willRebateCalculationExcludeLine
     *
     * @return SalesOrderLine
     */
    public function setWillRebateCalculationExcludeLine(string $willRebateCalculationExcludeLine): SalesOrderLine
    {
        return $this->setTrigger('willRebateCalculationExcludeLine', $willRebateCalculationExcludeLine);
    }

    /**
     * @return string
     */
    public function getWithholdingTaxGroupCode(): ?string
    {
        return $this->withholdingTaxGroupCode;
    }

    /**
     * @param string $withholdingTaxGroupCode
     *
     * @return SalesOrderLine
     */
    public function setWithholdingTaxGroupCode(string $withholdingTaxGroupCode): SalesOrderLine
    {
        return $this->setTrigger('withholdingTaxGroupCode', $withholdingTaxGroupCode);
    }
}
