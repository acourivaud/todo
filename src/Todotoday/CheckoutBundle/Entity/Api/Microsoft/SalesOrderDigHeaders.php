<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 04/04/17
 * Time: 18:13
 */

namespace Todotoday\CheckoutBundle\Entity\Api\Microsoft;

use Actiane\ApiConnectorBundle\Lib\EntityProxyTrait;
use Actiane\ApiConnectorBundle\Lib\Mapping as APIConnector;
use JMS\Serializer\Annotation as Serializer;

/**
 * Supplier order - we shouldn't use setters there
 *
 * Class SalesOrderDigHeaders
 *
 * @APIConnector\Entity(path="SalesOrderDigHeaders", repositoryId="todotoday.checkout.repository.api.order_refused")
 * @Serializer\ExclusionPolicy("all")
 * @package Todotoday\CheckoutBundle\Entity\Api\Microsoft
 */
class SalesOrderDigHeaders
{
    use EntityProxyTrait;
    public const BASE_KEY_TRANSLATION = 'order.supplier_status_';

    /**
     * @var string
     *
     * @APIConnector\Column(name="dataAreaId", type="string")
     * @APIConnector\Id()
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $dataAreaId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="SalesOrderNumber", type="string")
     * @APIConnector\Id()
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $salesOrderNumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="PurchId", type="string")
     * @APIConnector\Id()
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $purchId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="InvoiceCustomerAccountNumber", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $invoiceCustomerAccountNumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="IdActiane", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $idActiane;

    /**
     * @var string
     *
     * @APIConnector\Column(name="StripePaymentId", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $stripePaymentId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ResponseNote", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $responseNote;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DocumentStatus", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $documentStatus;

    /**
     * @var string
     *
     * @APIConnector\Column(name="OrderingCustomerAccountNumber", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $orderingCustomerAccountNumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DepotModeCode", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $depotModeCode;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="CreationDate", type="datetimez")
     * @Serializer\Expose()
     * @Serializer\Type("datetime")
     */
    protected $creationDate;

    /**
     * @var string
     *
     * @APIConnector\Column(name="StripePaymentStatus", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $stripePaymentStatus;

    /**
     * @var string
     *
     * @APIConnector\Column(name="Creator", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $creator;

    /**
     * @var string
     *
     * @APIConnector\Column(name="TDTDValidateDevis", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $tdtdValidateDevis;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ResponseState", type="string",
     *                                            mType="Microsoft.Dynamics.DataEntities.PurchaseOrderResponseState")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $responseState;

    /**
     * @var string
     *
     * @APIConnector\Column(name="OrigDlvMode", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $origDlvMode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CustomerRefDepot", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $customerRefDepot;

    /**
     * @var string
     *
     * @APIConnector\Column(name="SalesOrderStatus", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $salesOrderStatus;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="ResponseDateTime", type="datetimez")
     * @Serializer\Expose()
     * @Serializer\Type("DateTime")
     */
    protected $responseDateTime;

    /**
     * @return string
     */
    public function getDataAreaId(): ?string
    {
        return $this->dataAreaId;
    }

    /**
     * @return string
     */
    public function getSalesOrderNumber(): ?string
    {
        return $this->salesOrderNumber;
    }

    /**
     * @return string
     */
    public function getPurchId(): ?string
    {
        return $this->purchId;
    }

    /**
     * @return string
     */
    public function getInvoiceCustomerAccountNumber(): ?string
    {
        return $this->invoiceCustomerAccountNumber;
    }

    /**
     * @return string
     */
    public function getIdActiane(): ?string
    {
        return $this->idActiane;
    }

    /**
     * @return string
     */
    public function getStripePaymentId(): ?string
    {
        return $this->stripePaymentId;
    }

    /**
     * @return string
     */
    public function getResponseNote(): ?string
    {
        return $this->responseNote;
    }

    /**
     * @return string
     */
    public function getDocumentStatus(): ?string
    {
        return $this->documentStatus;
    }

    /**
     * @return string
     */
    public function getOrderingCustomerAccountNumber(): ?string
    {
        return $this->orderingCustomerAccountNumber;
    }

    /**
     * @return string
     */
    public function getDepotModeCode(): ?string
    {
        return $this->depotModeCode;
    }

    /**
     * @return \DateTime
     */
    public function getCreationDate(): ?\DateTime
    {
        return $this->creationDate;
    }

    /**
     * @return string
     */
    public function getStripePaymentStatus(): ?string
    {
        return $this->stripePaymentStatus;
    }

    /**
     * @return string
     */
    public function getCreator(): ?string
    {
        return $this->creator;
    }

    /**
     * @return string
     */
    public function getTdtdValidateDevis(): ?string
    {
        return $this->tdtdValidateDevis;
    }

    /**
     * @return string
     */
    public function getResponseState(): ?string
    {
        return $this->responseState;
    }

    /**
     * @return string
     */
    public function getOrigDlvMode(): ?string
    {
        return $this->origDlvMode;
    }

    /**
     * @return string
     */
    public function getCustomerRefDepot(): ?string
    {
        return $this->customerRefDepot;
    }

    /**
     * @return string
     */
    public function getSalesOrderStatus(): ?string
    {
        return $this->salesOrderStatus;
    }

    /**
     * @param string|null $dataAreaId
     *
     * @return SalesOrderDigHeaders
     */
    public function setDataAreaId(?string $dataAreaId): SalesOrderDigHeaders
    {
        $this->dataAreaId = $dataAreaId;

        return $this;
    }

    /**
     * @param string|null $salesOrderNumber
     *
     * @return SalesOrderDigHeaders
     */
    public function setSalesOrderNumber(?string $salesOrderNumber): SalesOrderDigHeaders
    {
        $this->salesOrderNumber = $salesOrderNumber;

        return $this;
    }

    /**
     * @param string|null $purchId
     *
     * @return SalesOrderDigHeaders
     */
    public function setPurchId(?string $purchId): SalesOrderDigHeaders
    {
        $this->purchId = $purchId;

        return $this;
    }

    /**
     * @param string|null $invoiceCustomerAccountNumber
     *
     * @return SalesOrderDigHeaders
     */
    public function setInvoiceCustomerAccountNumber(?string $invoiceCustomerAccountNumber): SalesOrderDigHeaders
    {
        $this->invoiceCustomerAccountNumber = $invoiceCustomerAccountNumber;

        return $this;
    }

    /**
     * @param string|null $idActiane
     *
     * @return SalesOrderDigHeaders
     */
    public function setIdActiane(?string $idActiane): SalesOrderDigHeaders
    {
        $this->idActiane = $idActiane;

        return $this;
    }

    /**
     * @param string|null $stripePaymentId
     *
     * @return SalesOrderDigHeaders
     */
    public function setStripePaymentId(?string $stripePaymentId): SalesOrderDigHeaders
    {
        $this->stripePaymentId = $stripePaymentId;

        return $this;
    }

    /**
     * @param string|null $responseNote
     *
     * @return SalesOrderDigHeaders
     */
    public function setResponseNote(?string $responseNote): SalesOrderDigHeaders
    {
        $this->responseNote = $responseNote;

        return $this;
    }

    /**
     * @param string|null $documentStatus
     *
     * @return SalesOrderDigHeaders
     */
    public function setDocumentStatus(?string $documentStatus): SalesOrderDigHeaders
    {
        $this->documentStatus = $documentStatus;

        return $this;
    }

    /**
     * @param string|null $orderingCustomerAccountNumber
     *
     * @return SalesOrderDigHeaders
     */
    public function setOrderingCustomerAccountNumber(?string $orderingCustomerAccountNumber): SalesOrderDigHeaders
    {
        $this->orderingCustomerAccountNumber = $orderingCustomerAccountNumber;

        return $this;
    }

    /**
     * @param string|null $depotModeCode
     *
     * @return SalesOrderDigHeaders
     */
    public function setDepotModeCode(?string $depotModeCode): SalesOrderDigHeaders
    {
        $this->depotModeCode = $depotModeCode;

        return $this;
    }

    /**
     * @param \DateTime|null $creationDate
     *
     * @return SalesOrderDigHeaders
     */
    public function setCreationDate(?\DateTime $creationDate): SalesOrderDigHeaders
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * @param string|null $stripePaymentStatus
     *
     * @return SalesOrderDigHeaders
     */
    public function setStripePaymentStatus(?string $stripePaymentStatus): SalesOrderDigHeaders
    {
        $this->stripePaymentStatus = $stripePaymentStatus;

        return $this;
    }

    /**
     * @param string|null $creator
     *
     * @return SalesOrderDigHeaders
     */
    public function setCreator(?string $creator): SalesOrderDigHeaders
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * @param string|null $tdtdValidateDevis
     *
     * @return SalesOrderDigHeaders
     */
    public function setTdtdValidateDevis(?string $tdtdValidateDevis): SalesOrderDigHeaders
    {
        $this->tdtdValidateDevis = $tdtdValidateDevis;

        return $this;
    }

    /**
     * @param string|null $responseState
     *
     * @return SalesOrderDigHeaders
     */
    public function setResponseState(?string $responseState): SalesOrderDigHeaders
    {
        $this->responseState = $responseState;

        return $this;
    }

    /**
     * @param string|null $origDlvMode
     *
     * @return SalesOrderDigHeaders
     */
    public function setOrigDlvMode(?string $origDlvMode): SalesOrderDigHeaders
    {
        $this->origDlvMode = $origDlvMode;

        return $this;
    }

    /**
     * @param string|null $customerRefDepot
     *
     * @return SalesOrderDigHeaders
     */
    public function setCustomerRefDepot(?string $customerRefDepot): SalesOrderDigHeaders
    {
        $this->customerRefDepot = $customerRefDepot;

        return $this;
    }

    /**
     * @param string|null $salesOrderStatus
     *
     * @return SalesOrderDigHeaders
     */
    public function setSalesOrderStatus(?string $salesOrderStatus): SalesOrderDigHeaders
    {
        $this->salesOrderStatus = $salesOrderStatus;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getResponseDateTime(): ?\DateTime
    {
        return $this->responseDateTime;
    }

    /**
     * @param \DateTime|null $responseDateTime
     *
     * @return SalesOrderDigHeaders
     */
    public function setResponseDateTime(?\DateTime $responseDateTime): SalesOrderDigHeaders
    {
        $this->responseDateTime = $responseDateTime;

        return $this;
    }
}
