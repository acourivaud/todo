<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 04/04/17
 * Time: 18:13
 */

namespace Todotoday\CheckoutBundle\Entity\Api\Microsoft;

use Actiane\ApiConnectorBundle\Lib\EntityProxyTrait;
use Actiane\ApiConnectorBundle\Lib\Mapping as APIConnector;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class SalesOrderHeader
 * @SuppressWarnings(PHPMD)
 *
 * @APIConnector\Entity(path="TDTDSalesOrderHeaders", repositoryId="todotoday.checkout.repository.api.sales_order_header")
 * @Serializer\ExclusionPolicy("all")
 * @package Todotoday\CheckoutBundle\Entity\Api\Microsoft
 */
class SalesOrderHeader
{
    use EntityProxyTrait;

    /**
     * @var string
     *
     * @APIConnector\Column(name="FiscalDocumentOperationTypeId", type="string")
     * @Serializer\Type("string")
     */
    protected $fiscalDocumentOperationTypeId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="OrderTakerPersonnelNumber", type="string")
     * @Serializer\Type("string")
     */
    protected $orderTakerPersonnelNumber;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="RequestedReceiptDate", type="datetimez")
     * @Serializer\Type("DateTime")
     */
    protected $requestedReceiptDate;

    /**
     * @var string
     *
     * @APIConnector\Column(name="EInvoiceDimensionAccountCode", type="string")
     * @Serializer\Type("string")
     */
    protected $eInvoiceDimensionAccountCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="IsOwnEntryCertificateIssued", type="string")
     * @Serializer\Type("string")
     */
    protected $isOwnEntryCertificateIssued;

    /**
     * @var string
     *
     * @APIConnector\Column(name="FormattedDelveryAddress", type="string")
     * @Serializer\Type("string")
     */
    protected $formattedDelveryAddress;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CampaignId", type="string")
     * @Serializer\Type("string")
     */
    protected $campaignId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="Email", type="string")
     * @Serializer\Type("string")
     */
    protected $email;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DefaultShippingSiteId", type="string")
     * @Serializer\Type("string")
     */
    protected $defaultShippingSiteId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="TransportationBrokerId", type="string")
     * @Serializer\Type("string")
     */
    protected $transportationBrokerId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="TransportationModeId", type="string")
     * @Serializer\Type("string")
     */
    protected $transportationModeId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DeliveryAddressDescription", type="string")
     * @Serializer\Type("string")
     */
    protected $deliveryAddressDescription;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CFPSCode", type="string")
     * @Serializer\Type("string")
     */
    protected $cfpsCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="IsSalesProcessingStopped", type="string",
     *                                                       mType="Microsoft.Dynamics.DataEntities.NoYes")
     * @Serializer\Type("string")
     */
    protected $isSalesProcessingStopped;

    /**
     * @var string
     *
     * @APIConnector\Column(name="TMACustomerGroupId", type="string")
     * @Serializer\Type("string")
     */
    protected $tmaCustomerGroupId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="BankSpecificSymbol", type="string")
     * @Serializer\Type("string")
     */
    protected $bankSpecificSymbol;

    /**
     * @var string
     *
     * @APIConnector\Column(name="SalesOrderStatus", type="string", mType="Microsoft.Dynamics.DataEntities.SalesStatus")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $salesOrderStatus;

    /**
     * @var string
     *
     * @APIConnector\Column(name="NumberSequenceGroupId", type="string")
     * @Serializer\Type("string")
     */
    protected $numberSequenceGroupId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="SalesOrderOriginCode", type="string", doctrineAttribute="source")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $salesOrderOriginCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="TransportationTemplateId", type="string")
     * @Serializer\Type("string")
     */
    protected $transportationTemplateId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CustomerPaymentFinancialInterestCode", type="string")
     * @Serializer\Type("string")
     */
    protected $customerPaymentFinancialInterestCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="BankConstantSymbol", type="string")
     * @Serializer\Type("string")
     */
    protected $bankConstantSymbol;

    /**
     * @var string
     *
     * @APIConnector\Column(name="IsOneTimeCustomer", type="string", mType="Microsoft.Dynamics.DataEntities.NoYes")
     * @Serializer\Type("string")
     */
    protected $isOneTimeCustomer;

    /**
     * @var string
     *
     * @APIConnector\Column(name="SalesOrderPoolId", type="string")
     * @Serializer\Type("string")
     */
    protected $salesOrderPoolId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DeliveryAddressCountryRegionId", type="string")
     * @Serializer\Type("string")
     */
    protected $deliveryAddressCountryRegionId;

    /**
     * @var float
     *
     * @APIConnector\Column(name="DeliveryAddressLatitude", type="float")
     * @Serializer\Type("float")
     */
    protected $deliveryAddressLatitude;

    /**
     * @var string
     *
     * @APIConnector\Column(name="TotalDiscountCustomerGroupCode", type="string")
     * @Serializer\Type("string")
     */
    protected $totalDiscountCustomerGroupCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DeliveryAddressCity", type="string")
     * @Serializer\Type("string")
     */
    protected $deliveryAddressCity;

    /**
     * @var string
     *
     * @APIConnector\Column(name="SalesRebateCustomerGroupId", type="string")
     * @Serializer\Type("string")
     */
    protected $salesRebateCustomerGroupId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="SalesOrderPromisingMethod", type="string",
     *                                                        mType="Microsoft.Dynamics.DataEntities.SalesDeliveryDateControlType")
     * @Serializer\Type("string")
     */
    protected $salesOrderPromisingMethod;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ShippingCarrierId", type="string")
     * @Serializer\Type("string")
     */
    protected $shippingCarrierId;

    /**
     * @var float
     *
     * @APIConnector\Column(name="TotalDiscountPercentage", type="float")
     * @Serializer\Type("float")
     */
    protected $totalDiscountPercentage;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DeliveryAddressDistrictName", type="string")
     * @Serializer\Type("string")
     */
    protected $deliveryAddressDistrictName;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DeliveryAddressCountyId", type="string")
     * @Serializer\Type("string")
     */
    protected $deliveryAddressCountyId;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="ConfirmedReceiptDate", type="datetimez")
     * @Serializer\Type("DateTime")
     * @Serializer\Expose()
     */
    protected $confirmedReceiptDate;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DeliveryAddressZipCode", type="string")
     * @Serializer\Type("string")
     */
    protected $deliveryAddressZipCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="FiscalOperationPresenceType", type="string", mType="Microsoft.Dynamics.DataEntities
     * .EFDocPresenceType_BR")
     * @Serializer\Type("string")
     */
    protected $fiscalOperationPresenceType;

    /**
     * @var string
     *
     * @APIConnector\Column(name="QuotationNumber", type="string")
     * @Serializer\Type("string")
     */
    protected $quotationNumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="IsConsolidatedInvoiceTarget", type="string",
     *                                                          mType="Microsoft.Dynamics.DataEntities.NoYes")
     * @Serializer\Type("string")
     */
    protected $isConsolidatedInvoiceTarget;

    /**
     * @var string
     * @APIConnector\Id()
     * @APIConnector\Column(name="dataAreaId", type="string", required=true)
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $dataAreaId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="LanguageId", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $languageId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DeliveryAddressDunsNumber", type="string")
     * @Serializer\Type("string")
     */
    protected $deliveryAddressDunsNumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="MultilineDiscountCustomerGroupCode", type="string")
     * @Serializer\Type("string")
     */
    protected $multilineDiscountCustomerGroupCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ServiceFiscalInformationCode", type="string")
     * @Serializer\Type("string")
     */
    protected $serviceFiscalInformationCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CustomerPaymentMethodSpecificationName", type="string")
     * @Serializer\Type("string")
     */
    protected $customerPaymentMethodSpecificationName;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CommissionCustomerGroupId", type="string")
     * @Serializer\Type("string")
     */
    protected $commissionCustomerGroupId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DeliveryAddressName", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $deliveryAddressName;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="PaymentTermsBaseDate", type="datetimez")
     * @Serializer\Type("string")
     */
    protected $paymentTermsBaseDate;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DeliveryAddressStreetNumber", type="string")
     * @Serializer\Type("string")
     */
    protected $deliveryAddressStreetNumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CreditNoteReasonCode", type="string")
     * @Serializer\Type("string")
     */
    protected $creditNoteReasonCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ChargeCustomerGroupId", type="string")
     * @Serializer\Type("string")
     */
    protected $chargeCustomerGroupId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="IsDeliveryAddressPrivate", type="string",
     *                                                       mType="Microsoft.Dynamics.DataEntities.NoYes")
     * @Serializer\Type("string")
     */
    protected $isDeliveryAddressPrivate;

    /**
     * @var string
     *
     * @APIConnector\Column(name="TaxExemptNumber", type="string")
     * @Serializer\Type("string")
     */
    protected $taxExemptNumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CustomersOrderReference", type="string", doctrineAttribute="deliveryComment")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $customersOrderReference;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ExportReason", type="string")
     * @Serializer\Type("string")
     */
    protected $exportReason;

    /**
     * @var string
     *
     * @APIConnector\Column(name="OrderResponsiblePersonnelNumber", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $orderResponsiblePersonnelNumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CashDiscountCode", type="string")
     * @Serializer\Type("string")
     */
    protected $cashDiscountCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="PaymentScheduleName", type="string")
     * @Serializer\Type("string")
     */
    protected $paymentScheduleName;

    /**
     * @var string
     *
     * @APIConnector\Column(name="IntrastatTransactionCode", type="string")
     * @Serializer\Type("string")
     */
    protected $intrastatTransactionCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="URL", type="string")
     * @Serializer\Type("string")
     */
    protected $url;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CurrencyCode", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $currencyCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="InvoiceType", type="string", mType="Microsoft.Dynamics.DataEntities.InvoiceType_MY")
     * @Serializer\Type("string")
     */
    protected $invoiceType;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ArePricesIncludingSalesTax", type="string", mType="Microsoft.Dynamics.DataEntities
     * .NoYes")
     * @Serializer\Type("string")
     */
    protected $arePricesIncludingSalesTax;

    /**
     * @var string
     *
     * @APIConnector\Column(name="InvoiceCustomerAccountNumber", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $invoiceCustomerAccountNumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DeliveryAddressLocationId", type="string")
     * @Serializer\Type("string")
     */
    protected $deliveryAddressLocationId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CustomerTransactionSettlementType", type="string",
     *                                                                mType="Microsoft.Dynamics.DataEntities.SettlementType")
     * @Serializer\Type("string")
     */
    protected $customerTransactionSettlementType;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CommissionSalesRepresentativeGroupId", type="string")
     * @Serializer\Type("string")
     */
    protected $commissionSalesRepresentativeGroupId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="WillAutomaticInventoryReservationConsiderBatchAttributes", type="string",
     *                                                                                       mType="Microsoft.Dynamics.DataEntities.NoYes")
     * @Serializer\Type("string")
     */
    protected $willAutomaticInventoryReservationConsiderBatchAttributes;

    /**
     * @var string
     *
     * @APIConnector\Column(name="IntrastatStatisticsProcedureCode", type="string")
     * @Serializer\Type("string")
     */
    protected $intrastatStatisticsProcedureCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="IsEInvoiceDimensionAccountCodeSpecifiedPerLine", type="string",
     *                                                                             mType="Microsoft.Dynamics.DataEntities.NoYes")
     * @Serializer\Type("string")
     */
    protected $isEInvoiceDimensionAccountCodeSpecifiedPerLine;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DeliveryAddressStreet", type="string")
     * @Serializer\Type("string")
     */
    protected $deliveryAddressStreet;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DeliveryModeCode", type="string", doctrineAttribute="delivery")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $deliveryModeCode;

    /**
     * @var \Datetime
     *
     * @APIConnector\Column(name="ConfirmedShippingDate", type="datetimez")
     * @Serializer\Type("DateTime")
     */
    protected $confirmedShippingDate;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="FixedDueDate", type="datetimez")
     * @Serializer\Type("DateTime")
     */
    protected $fixedDueDate;

    /**
     * @var string
     *
     * @APIConnector\Column(name="SalesTaxGroupCode", type="string")
     * @Serializer\Type("string")
     */
    protected $salesTaxGroupCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="IsDeliveryAddressOrderSpecific", type="string")
     * @Serializer\Type("string")
     */
    protected $isDeliveryAddressOrderSpecific;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CustomerRequisitionNumber", type="string")
     * @Serializer\Type("string")
     */
    protected $customerRequisitionNumber;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @APIConnector\Column(name="SalesOrderNumber", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $salesOrderNumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="FiscalDocumentTypeId", type="string")
     * @Serializer\Type("string")
     */
    protected $fiscalDocumentTypeId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="IsFinalUser", type="string", mType="Microsoft.Dynamics.DataEntities.NoYes")
     * @Serializer\Type("string")
     */
    protected $isFinalUser;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ShippingCarrierServiceGroupId", type="string")
     * @Serializer\Type("string")
     */
    protected $shippingCarrierServiceGroupId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ContactPersonId", type="string")
     * @Serializer\Type("string")
     */
    protected $contactPersonId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="EUSalesListCode", type="string", mType="Microsoft.Dynamics.DataEntities.Listcode")
     * @Serializer\Type("string")
     */
    protected $euSalesListCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="PaymentTermsName", type="string")
     * @Serializer\Type("string")
     */
    protected $paymentTermsName;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CustomerPostingProfileId", type="string")
     * @Serializer\Type("string")
     */
    protected $customerPostingProfileId;

    /**
     * @var float
     *
     * @APIConnector\Column(name="DeliveryAddressLongitude", type="float")
     * @Serializer\Type("float")
     */
    protected $deliveryAddressLongitude;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DeliveryTermsCode", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $deliveryTermsCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ShippingCarrierServiceId", type="string")
     * @Serializer\Type("string")
     */
    protected $shippingCarrierServiceId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DefaultLedgerDimensionDisplayValue", type="string")
     * @Serializer\Type("string")
     */
    protected $defaultLedgerDimensionDisplayValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DeliveryAddressTimeZone", type="string",
     *                                                      mType="Microsoft.Dynamics.DataEntities.Timezone")
     * @Serializer\Type("string")
     */
    protected $deliveryAddressTimeZone;

    /**
     * @var string
     *
     * @APIConnector\Column(name="SalesOrderName", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $salesOrderName;

    /**
     * @var string
     *
     * @APIConnector\Column(name="IsEntryCertificateRequired", type="string",
     *                                                         mType="Microsoft.Dynamics.DataEntities.NoYes")
     * @Serializer\Type("string")
     */
    protected $isEntryCertificateRequired;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DefaultShippingWarehouseId", type="string")
     * @Serializer\Type("string")
     */
    protected $defaultShippingWarehouseId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DeliveryAddressStateId", type="string")
     * @Serializer\Type("string")
     */
    protected $deliveryAddressStateId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DeliveryBuildingCompliment", type="string")
     * @Serializer\Type("string")
     */
    protected $deliveryBuildingCompliment;

    /**
     * @var string
     *
     * @APIConnector\Column(name="IntrastatTransportModeCode", type="string")
     * @Serializer\Type("string")
     */
    protected $intrastatTransportModeCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="InvoicePaymentAttachmentType", type="string",
     *                                                           mType="Microsoft.Dynamics.DataEntities.PaymentStub")
     * @Serializer\Type("string")
     */
    protected $invoicePaymentAttachmentType;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DeliveryAddressPostBox", type="string")
     * @Serializer\Type("string")
     */
    protected $deliveryAddressPostBox;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DirectDebitMandateId", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $directDebitMandateId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="LineDiscountCustomerGroupCode", type="string")
     * @Serializer\Type("string")
     */
    protected $lineDiscountCustomerGroupCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="IntrastatPortId", type="string")
     * @Serializer\Type("string")
     */
    protected $intrastatPortId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="OrderingCustomerAccountNumber", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $orderingCustomerAccountNumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="IdActiane", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $idActiane;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CustomerPaymentFineCode", type="string")
     * @Serializer\Type("string")
     */
    protected $customerPaymentFineCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="PriceCustomerGroupCode", type="string")
     * @Serializer\Type("string")
     */
    protected $priceCustomerGroupCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DeliveryReasonCode", type="string")
     * @Serializer\Type("string")
     */
    protected $deliveryReasonCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="IsServiceDeliveryAddressBased", type="string",
     *                                                            mType="Microsoft.Dynamics.DataEntities.NoYes")
     * @Serializer\Type("string")
     */
    protected $isServiceDeliveryAddressBased;

    /**
     * @var string
     *
     * @APIConnector\Column(name="InventoryReservationMethod", type="string",
     *                                                         mType="Microsoft.Dynamics.DataEntities.ItemReservation")
     * @Serializer\Type("string")
     */
    protected $inventoryReservationMethod;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="RequestedShippingDate", type="datetimez")
     * @Serializer\Type("DateTime")
     */
    protected $requestedShippingDate;

    /**
     * @var string
     *
     * @APIConnector\Column(name="TransportationRoutePlanId", type="string")
     * @Serializer\Type("string")
     */
    protected $transportationRoutePlanId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CustomerPaymentMethodName", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $customerPaymentMethodName;

    /**
     * @var string
     *
     * @APIConnector\Column(name="SalesUnitId", type="string")
     * @Serializer\Type("string")
     */
    protected $salesUnitId;

    /**
     * @var float
     *
     * @APIConnector\Column(name="FixedExchangeRate", type="float")
     * @Serializer\Expose()
     * @Serializer\Type("float")
     */
    protected $fixedExchangeRate;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="CreationDate", type="datetimez")
     * @Serializer\Expose()
     * @Serializer\Type("DateTime")
     */
    protected $creationDate;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DepotModeCode", type="string", doctrineAttribute="depository")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $depotModeCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CustomerRefDepot", type="string", doctrineAttribute="depositoryComment")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $customerRefDepot;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DocumentStatus", type="string",
     *                                             mType="Microsoft.Dynamics.DataEntities.DocumentStatus")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $documentStatus;

    /**
     * @var float
     * @Serializer\Expose()
     * @Serializer\Type("float")
     */
    protected $totalAmount;

    /**
     * @var array
     * @Serializer\Expose()
     * @Serializer\Type("array")
     */
    protected $mainPictos;

    /**
     * @var array
     * @Serializer\Expose()
     * @Serializer\Type("array")
     */
    protected $pathPictos;

    /**
     * @var string
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $orderStatus;

    /**
     * @var string
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $orderStatusDisplay;

    /**
     * @var bool
     * @Serializer\Expose()
     * @Serializer\Type("boolean")
     */
    protected $ratable;

    /**
     * @var bool
     * @Serializer\Expose()
     * @Serializer\Type("boolean")
     */
    protected $cancellable;

    /**
     * @var bool
     * @Serializer\Expose()
     * @Serializer\Type("boolean")
     */
    protected $fgsable;

    /**
     * @var array
     * @Serializer\Expose()
     * @Serializer\Type("array<Todotoday\CheckoutBundle\Entity\Api\Microsoft\SalesOrderLine>")
     */
    protected $orderLines;

    /**
     * @var SalesOrderDigHeaders|null
     * @Serializer\Expose()
     * @Serializer\Type("Todotoday\CheckoutBundle\Entity\Api\Microsoft\SalesOrderDigHeaders")
     */
    protected $supplierOrder;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="StripePaymentId", type="string")
     * @Serializer\Type("string")
     */
    protected $stripePaymentId;

    /**
     * @var string
     *
     * @APIConnector\Column(
     *     name="StripePaymentStatus",
     *     type="string",
     *     mType="Microsoft.Dynamics.DataEntities.StripePaymentStatus"
     * )
     */
    protected $stripePaymentStatus;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="HasRDV", type="string")
     * @Serializer\Type("string")
     */
    protected $hasRDV;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="HasAnimation", type="string")
     * @Serializer\Type("string")
     */
    protected $hasAnimation;

    /**
     * SalesOrderHeader constructor.
     */
    public function __construct()
    {
        $this->orderLines = array();
        $this->mainPictos = array();
        $this->totalAmount = 0.;
        $this->ratable = false;
        $this->cancellable = false;
        $this->fgsable = true;
    }

    /**
     *
     * @param string|null $hierarchy
     *
     * @return SalesOrderHeader
     */
    public function addMainPicto(?string $hierarchy = null): SalesOrderHeader
    {
        if ($hierarchy && !\in_array($hierarchy, $this->mainPictos, true)) {
            $this->mainPictos[] = $hierarchy;
        }

        return $this;
    }

    /**
     * @param SalesOrderLine $salesOrderLine
     *
     * @return SalesOrderHeader
     */
    public function addOrderLines(SalesOrderLine $salesOrderLine): SalesOrderHeader
    {
        $this->orderLines[] = $salesOrderLine;
        $this->totalAmount += $salesOrderLine->getLineAmount();

        return $this;
    }

    /**
     * @return string
     */
    public function getArePricesIncludingSalesTax(): ?string
    {
        return $this->arePricesIncludingSalesTax;
    }

    /**
     * @param string $arePricesIncludingSalesTax
     *
     * @return SalesOrderHeader
     */
    public function setArePricesIncludingSalesTax(string $arePricesIncludingSalesTax): SalesOrderHeader
    {
        return $this->setTrigger('arePricesIncludingSalesTax', $arePricesIncludingSalesTax);
    }

    /**
     * @return string
     */
    public function getBankConstantSymbol(): ?string
    {
        return $this->bankConstantSymbol;
    }

    /**
     * @param string $bankConstantSymbol
     *
     * @return SalesOrderHeader
     */
    public function setBankConstantSymbol(string $bankConstantSymbol): SalesOrderHeader
    {
        return $this->setTrigger('bankConstantSymbol', $bankConstantSymbol);
    }

    /**
     * @return string
     */
    public function getBankSpecificSymbol(): ?string
    {
        return $this->bankSpecificSymbol;
    }

    /**
     * @param string $bankSpecificSymbol
     *
     * @return SalesOrderHeader
     */
    public function setBankSpecificSymbol(string $bankSpecificSymbol): SalesOrderHeader
    {
        return $this->setTrigger('bankSpecificSymbol', $bankSpecificSymbol);
    }

    /**
     * @return string
     */
    public function getCampaignId(): ?string
    {
        return $this->campaignId;
    }

    /**
     * @param string $campaignId
     *
     * @return SalesOrderHeader
     */
    public function setCampaignId(string $campaignId): SalesOrderHeader
    {
        return $this->setTrigger('campaignId', $campaignId);
    }

    /**
     * @return string
     */
    public function getCashDiscountCode(): ?string
    {
        return $this->cashDiscountCode;
    }

    /**
     * @param string $cashDiscountCode
     *
     * @return SalesOrderHeader
     */
    public function setCashDiscountCode(string $cashDiscountCode): SalesOrderHeader
    {
        return $this->setTrigger('cashDiscountCode', $cashDiscountCode);
    }

    /**
     * @return string
     */
    public function getCfpsCode(): ?string
    {
        return $this->cfpsCode;
    }

    /**
     * @param string $cfpsCode
     *
     * @return SalesOrderHeader
     */
    public function setCfpsCode(string $cfpsCode): SalesOrderHeader
    {
        return $this->setTrigger('cfpsCode', $cfpsCode);
    }

    /**
     * @return string
     */
    public function getChargeCustomerGroupId(): ?string
    {
        return $this->chargeCustomerGroupId;
    }

    /**
     * @param string $chargeCustomerGroupId
     *
     * @return SalesOrderHeader
     */
    public function setChargeCustomerGroupId(string $chargeCustomerGroupId): SalesOrderHeader
    {
        return $this->setTrigger('chargeCustomerGroupId', $chargeCustomerGroupId);
    }

    /**
     * @return string
     */
    public function getCommissionCustomerGroupId(): ?string
    {
        return $this->commissionCustomerGroupId;
    }

    /**
     * @param string $commissionCustomerGroupId
     *
     * @return SalesOrderHeader
     */
    public function setCommissionCustomerGroupId(string $commissionCustomerGroupId): SalesOrderHeader
    {
        return $this->setTrigger('commissionCustomerGroupId', $commissionCustomerGroupId);
    }

    /**
     * @return string
     */
    public function getCommissionSalesRepresentativeGroupId(): ?string
    {
        return $this->commissionSalesRepresentativeGroupId;
    }

    /**
     * @param string $commissionSalesRepresentativeGroupId
     *
     * @return SalesOrderHeader
     */
    public function setCommissionSalesRepresentativeGroupId(
        string $commissionSalesRepresentativeGroupId
    ): SalesOrderHeader {
        return $this->setTrigger('commissionSalesRepresentativeGroupId', $commissionSalesRepresentativeGroupId);
    }

    /**
     * @return \DateTime
     */
    public function getConfirmedReceiptDate(): ?\Datetime
    {
        return $this->confirmedReceiptDate;
    }

    /**
     * @param \DateTime $confirmedReceiptDate
     *
     * @return SalesOrderHeader
     */
    public function setConfirmedReceiptDate(\DateTime $confirmedReceiptDate): SalesOrderHeader
    {
        return $this->setTrigger('confirmedReceiptDate', $confirmedReceiptDate);
    }

    /**
     * @return \Datetime
     */
    public function getConfirmedShippingDate(): ?\Datetime
    {
        return $this->confirmedShippingDate;
    }

    /**
     * @param \Datetime $confirmedShippingDate
     *
     * @return SalesOrderHeader
     */
    public function setConfirmedShippingDate(\Datetime $confirmedShippingDate): SalesOrderHeader
    {
        return $this->setTrigger('confirmedShippingDate', $confirmedShippingDate);
    }

    /**
     * @return string
     */
    public function getContactPersonId(): ?string
    {
        return $this->contactPersonId;
    }

    /**
     * @param string $contactPersonId
     *
     * @return SalesOrderHeader
     */
    public function setContactPersonId(string $contactPersonId): SalesOrderHeader
    {
        return $this->setTrigger('contactPersonId', $contactPersonId);
    }

    /**
     * @return \DateTime
     */
    public function getCreationDate(): ?\DateTime
    {
        return $this->creationDate;
    }

    /**
     * @param \DateTime $creationDate
     *
     * @return SalesOrderHeader
     */
    public function setCreationDate(\DateTime $creationDate): SalesOrderHeader
    {
        $this->creationDate = $creationDate;

        return $this->setTrigger('creationDate', $creationDate);
    }

    /**
     * @return string
     */
    public function getCreditNoteReasonCode(): ?string
    {
        return $this->creditNoteReasonCode;
    }

    /**
     * @param string $creditNoteReasonCode
     *
     * @return SalesOrderHeader
     */
    public function setCreditNoteReasonCode(string $creditNoteReasonCode): SalesOrderHeader
    {
        return $this->setTrigger('creditNoteReasonCode', $creditNoteReasonCode);
    }

    /**
     * @return string
     */
    public function getCurrencyCode(): ?string
    {
        return $this->currencyCode;
    }

    /**
     * @param string $currencyCode
     *
     * @return SalesOrderHeader
     */
    public function setCurrencyCode(string $currencyCode): SalesOrderHeader
    {
        return $this->setTrigger('currencyCode', $currencyCode);
    }

    /**
     * @return string
     */
    public function getCustomerPaymentFinancialInterestCode(): ?string
    {
        return $this->customerPaymentFinancialInterestCode;
    }

    /**
     * @param string $customerPaymentFinancialInterestCode
     *
     * @return SalesOrderHeader
     */
    public function setCustomerPaymentFinancialInterestCode(
        string $customerPaymentFinancialInterestCode
    ): SalesOrderHeader {
        return $this->setTrigger('customerPaymentFinancialInterestCode', $customerPaymentFinancialInterestCode);
    }

    /**
     * @return string
     */
    public function getCustomerPaymentFineCode(): ?string
    {
        return $this->customerPaymentFineCode;
    }

    /**
     * @param string $customerPaymentFineCode
     *
     * @return SalesOrderHeader
     */
    public function setCustomerPaymentFineCode(string $customerPaymentFineCode): SalesOrderHeader
    {
        return $this->setTrigger('customerPaymentFineCode', $customerPaymentFineCode);
    }

    /**
     * @return string
     */
    public function getCustomerPaymentMethodName(): ?string
    {
        return $this->customerPaymentMethodName;
    }

    /**
     * @param string $customerPaymentMethodName
     *
     * @return SalesOrderHeader
     */
    public function setCustomerPaymentMethodName(string $customerPaymentMethodName): SalesOrderHeader
    {
        return $this->setTrigger('customerPaymentMethodName', $customerPaymentMethodName);
    }

    /**
     * @return string
     */
    public function getCustomerPaymentMethodSpecificationName(): ?string
    {
        return $this->customerPaymentMethodSpecificationName;
    }

    /**
     * @param string $customerPaymentMethodSpecificationName
     *
     * @return SalesOrderHeader
     */
    public function setCustomerPaymentMethodSpecificationName(
        string $customerPaymentMethodSpecificationName
    ): SalesOrderHeader {
        return $this->setTrigger('customerPaymentMethodSpecificationName', $customerPaymentMethodSpecificationName);
    }

    /**
     * @return string
     */
    public function getCustomerPostingProfileId(): ?string
    {
        return $this->customerPostingProfileId;
    }

    /**
     * @param string $customerPostingProfileId
     *
     * @return SalesOrderHeader
     */
    public function setCustomerPostingProfileId(string $customerPostingProfileId): SalesOrderHeader
    {
        return $this->setTrigger('customerPostingProfileId', $customerPostingProfileId);
    }

    /**
     * @return string
     */
    public function getCustomerRefDepot(): ?string
    {
        return $this->customerRefDepot;
    }

    /**
     * @param string $customerRefDepot
     *
     * @return SalesOrderHeader
     */
    public function setCustomerRefDepot(?string $customerRefDepot): SalesOrderHeader
    {
        return $this->setTrigger('customerRefDepot', $customerRefDepot);
    }

    /**
     * @return string
     */
    public function getCustomerRequisitionNumber(): ?string
    {
        return $this->customerRequisitionNumber;
    }

    /**
     * @param string $customerRequisitionNumber
     *
     * @return SalesOrderHeader
     */
    public function setCustomerRequisitionNumber(string $customerRequisitionNumber): SalesOrderHeader
    {
        return $this->setTrigger('customerRequisitionNumber', $customerRequisitionNumber);
    }

    /**
     * @return string
     */
    public function getCustomerTransactionSettlementType(): ?string
    {
        return $this->customerTransactionSettlementType;
    }

    /**
     * @param string $customerTransactionSettlementType
     *
     * @return SalesOrderHeader
     */
    public function setCustomerTransactionSettlementType(string $customerTransactionSettlementType): SalesOrderHeader
    {
        return $this->setTrigger('customerTransactionSettlementType', $customerTransactionSettlementType);
    }

    /**
     * @return string
     */
    public function getCustomersOrderReference(): ?string
    {
        return $this->customersOrderReference;
    }

    /**
     * @param string $customersOrderReference
     *
     * @return SalesOrderHeader
     */
    public function setCustomersOrderReference(?string $customersOrderReference): SalesOrderHeader
    {
        return $this->setTrigger('customersOrderReference', $customersOrderReference);
    }

    /**
     * @return string
     */
    public function getDataAreaId(): ?string
    {
        return $this->dataAreaId;
    }

    /**
     * @param string $dataAreaId
     *
     * @return SalesOrderHeader
     */
    public function setDataAreaId(string $dataAreaId): SalesOrderHeader
    {
        return $this->setTrigger('dataAreaId', $dataAreaId);
    }

    /**
     * @return string
     */
    public function getDefaultLedgerDimensionDisplayValue(): ?string
    {
        return $this->defaultLedgerDimensionDisplayValue;
    }

    /**
     * @param string $defaultLedgerDimensionDisplayValue
     *
     * @return SalesOrderHeader
     */
    public function setDefaultLedgerDimensionDisplayValue(string $defaultLedgerDimensionDisplayValue): SalesOrderHeader
    {
        return $this->setTrigger('defaultLedgerDimensionDisplayValue', $defaultLedgerDimensionDisplayValue);
    }

    /**
     * @return string
     */
    public function getDefaultShippingSiteId(): ?string
    {
        return $this->defaultShippingSiteId;
    }

    /**
     * @param string $defaultShippingSiteId
     *
     * @return SalesOrderHeader
     */
    public function setDefaultShippingSiteId(string $defaultShippingSiteId): SalesOrderHeader
    {
        return $this->setTrigger('defaultShippingSiteId', $defaultShippingSiteId);
    }

    /**
     * @return string
     */
    public function getDefaultShippingWarehouseId(): ?string
    {
        return $this->defaultShippingWarehouseId;
    }

    /**
     * @param string $defaultShippingWarehouseId
     *
     * @return SalesOrderHeader
     */
    public function setDefaultShippingWarehouseId(string $defaultShippingWarehouseId): SalesOrderHeader
    {
        return $this->setTrigger('defaultShippingWarehouseId', $defaultShippingWarehouseId);
    }

    /**
     * @return string
     */
    public function getDeliveryAddressCity(): ?string
    {
        return $this->deliveryAddressCity;
    }

    /**
     * @param string $deliveryAddressCity
     *
     * @return SalesOrderHeader
     */
    public function setDeliveryAddressCity(string $deliveryAddressCity): SalesOrderHeader
    {
        return $this->setTrigger('deliveryAddressCity', $deliveryAddressCity);
    }

    /**
     * @return string
     */
    public function getDeliveryAddressCountryRegionId(): ?string
    {
        return $this->deliveryAddressCountryRegionId;
    }

    /**
     * @param string $deliveryAddressCountryRegionId
     *
     * @return SalesOrderHeader
     */
    public function setDeliveryAddressCountryRegionId(string $deliveryAddressCountryRegionId): SalesOrderHeader
    {
        return $this->setTrigger('deliveryAddressCountryRegionId', $deliveryAddressCountryRegionId);
    }

    /**
     * @return string
     */
    public function getDeliveryAddressCountyId(): ?string
    {
        return $this->deliveryAddressCountyId;
    }

    /**
     * @param string $deliveryAddressCountyId
     *
     * @return SalesOrderHeader
     */
    public function setDeliveryAddressCountyId(string $deliveryAddressCountyId): SalesOrderHeader
    {
        return $this->setTrigger('deliveryAddressCountyId', $deliveryAddressCountyId);
    }

    /**
     * @return string
     */
    public function getDeliveryAddressDescription(): ?string
    {
        return $this->deliveryAddressDescription;
    }

    /**
     * @param string $deliveryAddressDescription
     *
     * @return SalesOrderHeader
     */
    public function setDeliveryAddressDescription(string $deliveryAddressDescription): SalesOrderHeader
    {
        return $this->setTrigger('deliveryAddressDescription', $deliveryAddressDescription);
    }

    /**
     * @return string
     */
    public function getDeliveryAddressDistrictName(): ?string
    {
        return $this->deliveryAddressDistrictName;
    }

    /**
     * @param string $deliveryAddressDistrictName
     *
     * @return SalesOrderHeader
     */
    public function setDeliveryAddressDistrictName(string $deliveryAddressDistrictName): SalesOrderHeader
    {
        return $this->setTrigger('deliveryAddressDistrictName', $deliveryAddressDistrictName);
    }

    /**
     * @return string
     */
    public function getDeliveryAddressDunsNumber(): ?string
    {
        return $this->deliveryAddressDunsNumber;
    }

    /**
     * @param string $deliveryAddressDunsNumber
     *
     * @return SalesOrderHeader
     */
    public function setDeliveryAddressDunsNumber(string $deliveryAddressDunsNumber): SalesOrderHeader
    {
        return $this->setTrigger('deliveryAddressDunsNumber', $deliveryAddressDunsNumber);
    }

    /**
     * @return float
     */
    public function getDeliveryAddressLatitude(): ?float
    {
        return $this->deliveryAddressLatitude;
    }

    /**
     * @param float $deliveryAddressLatitude
     *
     * @return SalesOrderHeader
     */
    public function setDeliveryAddressLatitude(float $deliveryAddressLatitude): SalesOrderHeader
    {
        return $this->setTrigger('deliveryAddressLatitude', $deliveryAddressLatitude);
    }

    /**
     * @return string
     */
    public function getDeliveryAddressLocationId(): ?string
    {
        return $this->deliveryAddressLocationId;
    }

    /**
     * @param string $deliveryAddressLocationId
     *
     * @return SalesOrderHeader
     */
    public function setDeliveryAddressLocationId(string $deliveryAddressLocationId): SalesOrderHeader
    {
        return $this->setTrigger('deliveryAddressLocationId', $deliveryAddressLocationId);
    }

    /**
     * @return float
     */
    public function getDeliveryAddressLongitude(): ?float
    {
        return $this->deliveryAddressLongitude;
    }

    /**
     * @param float $deliveryAddressLongitude
     *
     * @return SalesOrderHeader
     */
    public function setDeliveryAddressLongitude(float $deliveryAddressLongitude): SalesOrderHeader
    {
        return $this->setTrigger('deliveryAddressLongitude', $deliveryAddressLongitude);
    }

    /**
     * @return string
     */
    public function getDeliveryAddressName(): ?string
    {
        return $this->deliveryAddressName;
    }

    /**
     * @param string $deliveryAddressName
     *
     * @return SalesOrderHeader
     */
    public function setDeliveryAddressName(string $deliveryAddressName): SalesOrderHeader
    {
        return $this->setTrigger('deliveryAddressName', $deliveryAddressName);
    }

    /**
     * @return string
     */
    public function getDeliveryAddressPostBox(): ?string
    {
        return $this->deliveryAddressPostBox;
    }

    /**
     * @param string $deliveryAddressPostBox
     *
     * @return SalesOrderHeader
     */
    public function setDeliveryAddressPostBox(string $deliveryAddressPostBox): SalesOrderHeader
    {
        return $this->setTrigger('deliveryAddressPostBox', $deliveryAddressPostBox);
    }

    /**
     * @return string
     */
    public function getDeliveryAddressStateId(): ?string
    {
        return $this->deliveryAddressStateId;
    }

    /**
     * @param string $deliveryAddressStateId
     *
     * @return SalesOrderHeader
     */
    public function setDeliveryAddressStateId(string $deliveryAddressStateId): SalesOrderHeader
    {
        return $this->setTrigger('deliveryAddressStateId', $deliveryAddressStateId);
    }

    /**
     * @return string
     */
    public function getDeliveryAddressStreet(): ?string
    {
        return $this->deliveryAddressStreet;
    }

    /**
     * @param string $deliveryAddressStreet
     *
     * @return SalesOrderHeader
     */
    public function setDeliveryAddressStreet(string $deliveryAddressStreet): SalesOrderHeader
    {
        return $this->setTrigger('deliveryAddressStreet', $deliveryAddressStreet);
    }

    /**
     * @return string
     */
    public function getDeliveryAddressStreetNumber(): ?string
    {
        return $this->deliveryAddressStreetNumber;
    }

    /**
     * @param string $deliveryAddressStreetNumber
     *
     * @return SalesOrderHeader
     */
    public function setDeliveryAddressStreetNumber(string $deliveryAddressStreetNumber): SalesOrderHeader
    {
        return $this->setTrigger('deliveryAddressStreetNumber', $deliveryAddressStreetNumber);
    }

    /**
     * @return string
     */
    public function getDeliveryAddressTimeZone(): ?string
    {
        return $this->deliveryAddressTimeZone;
    }

    /**
     * @param string $deliveryAddressTimeZone
     *
     * @return SalesOrderHeader
     */
    public function setDeliveryAddressTimeZone(string $deliveryAddressTimeZone): SalesOrderHeader
    {
        return $this->setTrigger('deliveryAddressTimeZone', $deliveryAddressTimeZone);
    }

    /**
     * @return string
     */
    public function getDeliveryAddressZipCode(): ?string
    {
        return $this->deliveryAddressZipCode;
    }

    /**
     * @param string $deliveryAddressZipCode
     *
     * @return SalesOrderHeader
     */
    public function setDeliveryAddressZipCode(string $deliveryAddressZipCode): SalesOrderHeader
    {
        return $this->setTrigger('deliveryAddressZipCode', $deliveryAddressZipCode);
    }

    /**
     * @return string
     */
    public function getDeliveryBuildingCompliment(): ?string
    {
        return $this->deliveryBuildingCompliment;
    }

    /**
     * @param string $deliveryBuildingCompliment
     *
     * @return SalesOrderHeader
     */
    public function setDeliveryBuildingCompliment(string $deliveryBuildingCompliment): SalesOrderHeader
    {
        return $this->setTrigger('deliveryBuildingCompliment', $deliveryBuildingCompliment);
    }

    /**
     * @return string
     */
    public function getDeliveryModeCode(): ?string
    {
        return $this->deliveryModeCode;
    }

    /**
     * @param string|null $deliveryModeCode
     *
     * @return SalesOrderHeader
     */
    public function setDeliveryModeCode(?string $deliveryModeCode): SalesOrderHeader
    {
        return $this->setTrigger('deliveryModeCode', $deliveryModeCode);
    }

    /**
     * @return string
     */
    public function getDeliveryReasonCode(): ?string
    {
        return $this->deliveryReasonCode;
    }

    /**
     * @param string $deliveryReasonCode
     *
     * @return SalesOrderHeader
     */
    public function setDeliveryReasonCode(string $deliveryReasonCode): SalesOrderHeader
    {
        return $this->setTrigger('deliveryReasonCode', $deliveryReasonCode);
    }

    /**
     * @return string
     */
    public function getDeliveryTermsCode(): ?string
    {
        return $this->deliveryTermsCode;
    }

    /**
     * @param string $deliveryTermsCode
     *
     * @return SalesOrderHeader
     */
    public function setDeliveryTermsCode(string $deliveryTermsCode): SalesOrderHeader
    {
        return $this->setTrigger('deliveryTermsCode', $deliveryTermsCode);
    }

    /**
     * @return string
     */
    public function getDepotModeCode(): ?string
    {
        return $this->depotModeCode;
    }

    /**
     * @param string $depotModeCode
     *
     * @return SalesOrderHeader
     */
    public function setDepotModeCode(?string $depotModeCode): SalesOrderHeader
    {
        return $this->setTrigger('depotModeCode', $depotModeCode);
    }

    /**
     * @return string
     */
    public function getDirectDebitMandateId(): ?string
    {
        return $this->directDebitMandateId;
    }

    /**
     * @param string $directDebitMandateId
     *
     * @return SalesOrderHeader
     */
    public function setDirectDebitMandateId(string $directDebitMandateId): SalesOrderHeader
    {
        return $this->setTrigger('directDebitMandateId', $directDebitMandateId);
    }

    /**
     * @return string
     */
    public function getDocumentStatus(): ?string
    {
        return $this->documentStatus;
    }

    /**
     * @param string $documentStatus
     *
     * @return SalesOrderHeader
     */
    public function setDocumentStatus(?string $documentStatus): SalesOrderHeader
    {
        return $this->setTrigger('documentStatus', $documentStatus);
    }

    /**
     * @return string
     */
    public function getEInvoiceDimensionAccountCode(): ?string
    {
        return $this->eInvoiceDimensionAccountCode;
    }

    /**
     * @param string $eInvoiceDimensionAccountCode
     *
     * @return SalesOrderHeader
     */
    public function setEInvoiceDimensionAccountCode(string $eInvoiceDimensionAccountCode): SalesOrderHeader
    {
        return $this->setTrigger('eInvoiceDimensionAccountCode', $eInvoiceDimensionAccountCode);
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return SalesOrderHeader
     */
    public function setEmail(string $email): SalesOrderHeader
    {
        return $this->setTrigger('email', $email);
    }

    /**
     * @return string
     */
    public function getEuSalesListCode(): ?string
    {
        return $this->euSalesListCode;
    }

    /**
     * @param string $euSalesListCode
     *
     * @return SalesOrderHeader
     */
    public function setEuSalesListCode(string $euSalesListCode): SalesOrderHeader
    {
        return $this->setTrigger('euSalesListCode', $euSalesListCode);
    }

    /**
     * @return string
     */
    public function getExportReason(): ?string
    {
        return $this->exportReason;
    }

    /**
     * @param string $exportReason
     *
     * @return SalesOrderHeader
     */
    public function setExportReason(string $exportReason): SalesOrderHeader
    {
        return $this->setTrigger('exportReason', $exportReason);
    }

    /**
     * @return string
     */
    public function getFiscalDocumentOperationTypeId(): ?string
    {
        return $this->fiscalDocumentOperationTypeId;
    }

    /**
     * @param string $fiscalDocumentOperationTypeId
     *
     * @return SalesOrderHeader
     */
    public function setFiscalDocumentOperationTypeId(string $fiscalDocumentOperationTypeId): SalesOrderHeader
    {
        return $this->setTrigger('fiscalDocumentOperationTypeId', $fiscalDocumentOperationTypeId);
    }

    /**
     * @return string
     */
    public function getFiscalDocumentTypeId(): ?string
    {
        return $this->fiscalDocumentTypeId;
    }

    /**
     * @param string $fiscalDocumentTypeId
     *
     * @return SalesOrderHeader
     */
    public function setFiscalDocumentTypeId(string $fiscalDocumentTypeId): SalesOrderHeader
    {
        return $this->setTrigger('fiscalDocumentTypeId', $fiscalDocumentTypeId);
    }

    /**
     * @return string
     */
    public function getFiscalOperationPresenceType(): ?string
    {
        return $this->fiscalOperationPresenceType;
    }

    /**
     * @param string $fiscalOperationPresenceType
     *
     * @return SalesOrderHeader
     */
    public function setFiscalOperationPresenceType(string $fiscalOperationPresenceType): SalesOrderHeader
    {
        return $this->setTrigger('fiscalOperationPresenceType', $fiscalOperationPresenceType);
    }

    /**
     * @return \DateTime
     */
    public function getFixedDueDate(): ?\Datetime
    {
        return $this->fixedDueDate;
    }

    /**
     * @param \DateTime $fixedDueDate
     *
     * @return SalesOrderHeader
     */
    public function setFixedDueDate(\DateTime $fixedDueDate): SalesOrderHeader
    {
        return $this->setTrigger('fixedDueDate', $fixedDueDate);
    }

    /**
     * @return float
     */
    public function getFixedExchangeRate(): ?float
    {
        return $this->fixedExchangeRate;
    }

    /**
     * @param float $fixedExchangeRate
     *
     * @return SalesOrderHeader
     */
    public function setFixedExchangeRate(float $fixedExchangeRate): SalesOrderHeader
    {
        return $this->setTrigger('fixedExchangeRate', $fixedExchangeRate);
    }

    /**
     * @return string
     */
    public function getFormattedDelveryAddress(): ?string
    {
        return $this->formattedDelveryAddress;
    }

    /**
     * @param string $formattedDelveryAddress
     *
     * @return SalesOrderHeader
     */
    public function setFormattedDelveryAddress(string $formattedDelveryAddress): SalesOrderHeader
    {
        return $this->setTrigger('formattedDelveryAddress', $formattedDelveryAddress);
    }

    /**
     * @return string
     */
    public function getIdActiane(): ?string
    {
        return $this->idActiane;
    }

    /**
     * @param string $idActiane
     *
     * @return SalesOrderHeader
     */
    public function setIdActiane(string $idActiane): SalesOrderHeader
    {
        return $this->setTrigger('idActiane', $idActiane);
    }

    /**
     * @return string
     */
    public function getIntrastatPortId(): ?string
    {
        return $this->intrastatPortId;
    }

    /**
     * @param string $intrastatPortId
     *
     * @return SalesOrderHeader
     */
    public function setIntrastatPortId(string $intrastatPortId): SalesOrderHeader
    {
        return $this->setTrigger('intrastatPortId', $intrastatPortId);
    }

    /**
     * @return string
     */
    public function getIntrastatStatisticsProcedureCode(): ?string
    {
        return $this->intrastatStatisticsProcedureCode;
    }

    /**
     * @param string $intrastatStatisticsProcedureCode
     *
     * @return SalesOrderHeader
     */
    public function setIntrastatStatisticsProcedureCode(string $intrastatStatisticsProcedureCode): SalesOrderHeader
    {
        return $this->setTrigger('intrastatStatisticsProcedureCode', $intrastatStatisticsProcedureCode);
    }

    /**
     * @return string
     */
    public function getIntrastatTransactionCode(): ?string
    {
        return $this->intrastatTransactionCode;
    }

    /**
     * @param string $intrastatTransactionCode
     *
     * @return SalesOrderHeader
     */
    public function setIntrastatTransactionCode(string $intrastatTransactionCode): SalesOrderHeader
    {
        return $this->setTrigger('intrastatTransactionCode', $intrastatTransactionCode);
    }

    /**
     * @return string
     */
    public function getIntrastatTransportModeCode(): ?string
    {
        return $this->intrastatTransportModeCode;
    }

    /**
     * @param string $intrastatTransportModeCode
     *
     * @return SalesOrderHeader
     */
    public function setIntrastatTransportModeCode(string $intrastatTransportModeCode): SalesOrderHeader
    {
        return $this->setTrigger('intrastatTransportModeCode', $intrastatTransportModeCode);
    }

    /**
     * @return string
     */
    public function getInventoryReservationMethod(): ?string
    {
        return $this->inventoryReservationMethod;
    }

    /**
     * @param string $inventoryReservationMethod
     *
     * @return SalesOrderHeader
     */
    public function setInventoryReservationMethod(string $inventoryReservationMethod): SalesOrderHeader
    {
        return $this->setTrigger('inventoryReservationMethod', $inventoryReservationMethod);
    }

    /**
     * @return string
     */
    public function getInvoiceCustomerAccountNumber(): ?string
    {
        return $this->invoiceCustomerAccountNumber;
    }

    /**
     * @param string $invoiceCustomerAccountNumber
     *
     * @return SalesOrderHeader
     */
    public function setInvoiceCustomerAccountNumber(string $invoiceCustomerAccountNumber): SalesOrderHeader
    {
        return $this->setTrigger('invoiceCustomerAccountNumber', $invoiceCustomerAccountNumber);
    }

    /**
     * @return string
     */
    public function getInvoicePaymentAttachmentType(): ?string
    {
        return $this->invoicePaymentAttachmentType;
    }

    /**
     * @param string $invoicePaymentAttachmentType
     *
     * @return SalesOrderHeader
     */
    public function setInvoicePaymentAttachmentType(string $invoicePaymentAttachmentType): SalesOrderHeader
    {
        return $this->setTrigger('invoicePaymentAttachmentType', $invoicePaymentAttachmentType);
    }

    /**
     * @return string
     */
    public function getInvoiceType(): ?string
    {
        return $this->invoiceType;
    }

    /**
     * @param string $invoiceType
     *
     * @return SalesOrderHeader
     */
    public function setInvoiceType(string $invoiceType): SalesOrderHeader
    {
        return $this->setTrigger('invoiceType', $invoiceType);
    }

    /**
     * @return string
     */
    public function getIsConsolidatedInvoiceTarget(): ?string
    {
        return $this->isConsolidatedInvoiceTarget;
    }

    /**
     * @param string $isConsolidatedInvoiceTarget
     *
     * @return SalesOrderHeader
     */
    public function setIsConsolidatedInvoiceTarget(string $isConsolidatedInvoiceTarget): SalesOrderHeader
    {
        return $this->setTrigger('isConsolidatedInvoiceTarget', $isConsolidatedInvoiceTarget);
    }

    /**
     * @return string
     */
    public function getIsDeliveryAddressOrderSpecific(): ?string
    {
        return $this->isDeliveryAddressOrderSpecific;
    }

    /**
     * @param string $isDeliveryAddressOrderSpecific
     *
     * @return SalesOrderHeader
     */
    public function setIsDeliveryAddressOrderSpecific(string $isDeliveryAddressOrderSpecific): SalesOrderHeader
    {
        return $this->setTrigger('isDeliveryAddressOrderSpecific', $isDeliveryAddressOrderSpecific);
    }

    /**
     * @return string
     */
    public function getIsDeliveryAddressPrivate(): ?string
    {
        return $this->isDeliveryAddressPrivate;
    }

    /**
     * @param string $isDeliveryAddressPrivate
     *
     * @return SalesOrderHeader
     */
    public function setIsDeliveryAddressPrivate(string $isDeliveryAddressPrivate): SalesOrderHeader
    {
        return $this->setTrigger('isDeliveryAddressPrivate', $isDeliveryAddressPrivate);
    }

    /**
     * @return string
     */
    public function getIsEInvoiceDimensionAccountCodeSpecifiedPerLine(): ?string
    {
        return $this->isEInvoiceDimensionAccountCodeSpecifiedPerLine;
    }

    /**
     * @param string $isEInvoiceDimensionAccountCodeSpecifiedPerLine
     *
     * @return SalesOrderHeader
     */
    public function setIsEInvoiceDimensionAccountCodeSpecifiedPerLine(
        string $isEInvoiceDimensionAccountCodeSpecifiedPerLine
    ): SalesOrderHeader {
        return $this->setTrigger(
            'isEInvoiceDimensionAccountCodeSpecifiedPerLine',
            $isEInvoiceDimensionAccountCodeSpecifiedPerLine
        );
    }

    /**
     * @return string
     */
    public function getIsEntryCertificateRequired(): ?string
    {
        return $this->isEntryCertificateRequired;
    }

    /**
     * @param string $isEntryCertificateRequired
     *
     * @return SalesOrderHeader
     */
    public function setIsEntryCertificateRequired(string $isEntryCertificateRequired): SalesOrderHeader
    {
        return $this->setTrigger('isEntryCertificateRequired', $isEntryCertificateRequired);
    }

    /**
     * @return string
     */
    public function getIsFinalUser(): ?string
    {
        return $this->isFinalUser;
    }

    /**
     * @param string $isFinalUser
     *
     * @return SalesOrderHeader
     */
    public function setIsFinalUser(string $isFinalUser): SalesOrderHeader
    {
        return $this->setTrigger('isFinalUser', $isFinalUser);
    }

    /**
     * @return string
     */
    public function getIsOneTimeCustomer(): ?string
    {
        return $this->isOneTimeCustomer;
    }

    /**
     * @param string $isOneTimeCustomer
     *
     * @return SalesOrderHeader
     */
    public function setIsOneTimeCustomer(string $isOneTimeCustomer): SalesOrderHeader
    {
        return $this->setTrigger('isOneTimeCustomer', $isOneTimeCustomer);
    }

    /**
     * @return string
     */
    public function getIsOwnEntryCertificateIssued(): ?string
    {
        return $this->isOwnEntryCertificateIssued;
    }

    /**
     * @param string $isOwnEntryCertificateIssued
     *
     * @return SalesOrderHeader
     */
    public function setIsOwnEntryCertificateIssued(string $isOwnEntryCertificateIssued): SalesOrderHeader
    {
        return $this->setTrigger('isOwnEntryCertificateIssued', $isOwnEntryCertificateIssued);
    }

    /**
     * @return string
     */
    public function getIsSalesProcessingStopped(): ?string
    {
        return $this->isSalesProcessingStopped;
    }

    /**
     * @param string $isSalesProcessingStopped
     *
     * @return SalesOrderHeader
     */
    public function setIsSalesProcessingStopped(string $isSalesProcessingStopped): SalesOrderHeader
    {
        return $this->setTrigger('isSalesProcessingStopped', $isSalesProcessingStopped);
    }

    /**
     * @return string
     */
    public function getIsServiceDeliveryAddressBased(): ?string
    {
        return $this->isServiceDeliveryAddressBased;
    }

    /**
     * @param string $isServiceDeliveryAddressBased
     *
     * @return SalesOrderHeader
     */
    public function setIsServiceDeliveryAddressBased(string $isServiceDeliveryAddressBased): SalesOrderHeader
    {
        return $this->setTrigger('isServiceDeliveryAddressBased', $isServiceDeliveryAddressBased);
    }

    /**
     * @return string
     */
    public function getLanguageId(): ?string
    {
        return $this->languageId;
    }

    /**
     * @param string $languageId
     *
     * @return SalesOrderHeader
     */
    public function setLanguageId(string $languageId): SalesOrderHeader
    {
        return $this->setTrigger('languageId', $languageId);
    }

    /**
     * @return string
     */
    public function getLineDiscountCustomerGroupCode(): ?string
    {
        return $this->lineDiscountCustomerGroupCode;
    }

    /**
     * @param string $lineDiscountCustomerGroupCode
     *
     * @return SalesOrderHeader
     */
    public function setLineDiscountCustomerGroupCode(string $lineDiscountCustomerGroupCode): SalesOrderHeader
    {
        return $this->setTrigger('lineDiscountCustomerGroupCode', $lineDiscountCustomerGroupCode);
    }

    /**
     * @return array|null
     */
    public function getMainPictos(): ?array
    {
        return $this->mainPictos;
    }

    /**
     * @return string
     */
    public function getMultilineDiscountCustomerGroupCode(): ?string
    {
        return $this->multilineDiscountCustomerGroupCode;
    }

    /**
     * @param string $multilineDiscountCustomerGroupCode
     *
     * @return SalesOrderHeader
     */
    public function setMultilineDiscountCustomerGroupCode(string $multilineDiscountCustomerGroupCode): SalesOrderHeader
    {
        return $this->setTrigger('multilineDiscountCustomerGroupCode', $multilineDiscountCustomerGroupCode);
    }

    /**
     * @return string
     */
    public function getNumberSequenceGroupId(): ?string
    {
        return $this->numberSequenceGroupId;
    }

    /**
     * @param string $numberSequenceGroupId
     *
     * @return SalesOrderHeader
     */
    public function setNumberSequenceGroupId(string $numberSequenceGroupId): SalesOrderHeader
    {
        return $this->setTrigger('numberSequenceGroupId', $numberSequenceGroupId);
    }

    /**
     * @return array
     */
    public function getOrderLines(): ?array
    {
        return $this->orderLines;
    }

    /**
     * @return string
     */
    public function getOrderResponsiblePersonnelNumber(): ?string
    {
        return $this->orderResponsiblePersonnelNumber;
    }

    /**
     * @param string $orderResponsiblePersonnelNumber
     *
     * @return SalesOrderHeader
     */
    public function setOrderResponsiblePersonnelNumber(string $orderResponsiblePersonnelNumber): SalesOrderHeader
    {
        return $this->setTrigger('orderResponsiblePersonnelNumber', $orderResponsiblePersonnelNumber);
    }

    /**
     * @return string
     */
    public function getOrderStatus(): ?string
    {
        return $this->orderStatus;
    }

    /**
     * @param string $orderStatus
     *
     * @return SalesOrderHeader
     */
    public function setOrderStatus(string $orderStatus): self
    {
        $this->orderStatus = $orderStatus;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getOrderStatusDisplay(): ?string
    {
        return $this->orderStatusDisplay;
    }

    /**
     * @param string|null $orderStatusDisplay
     *
     * @return SalesOrderHeader
     */
    public function setOrderStatusDisplay(?string $orderStatusDisplay): self
    {
        $this->orderStatusDisplay = $orderStatusDisplay;

        return $this;
    }

    /**
     * @return string
     */
    public function getOrderTakerPersonnelNumber(): ?string
    {
        return $this->orderTakerPersonnelNumber;
    }

    /**
     * @param string $orderTakerPersonnelNumber
     *
     * @return SalesOrderHeader
     */
    public function setOrderTakerPersonnelNumber(string $orderTakerPersonnelNumber): SalesOrderHeader
    {
        return $this->setTrigger('orderTakerPersonnelNumber', $orderTakerPersonnelNumber);
    }

    /**
     * @return string
     */
    public function getOrderingCustomerAccountNumber(): ?string
    {
        return $this->orderingCustomerAccountNumber;
    }

    /**
     * @param string $orderingCustomerAccountNumber
     *
     * @return SalesOrderHeader
     */
    public function setOrderingCustomerAccountNumber(string $orderingCustomerAccountNumber): SalesOrderHeader
    {
        return $this->setTrigger('orderingCustomerAccountNumber', $orderingCustomerAccountNumber);
    }

    /**
     * @return array
     */
    public function getPathPictos(): ?array
    {
        return $this->pathPictos;
    }

    /**
     * @param array|null $pathPictos
     *
     * @return SalesOrderHeader
     */
    public function setPathPictos(?array $pathPictos): SalesOrderHeader
    {
        $this->pathPictos = $pathPictos;

        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentScheduleName(): ?string
    {
        return $this->paymentScheduleName;
    }

    /**
     * @param string $paymentScheduleName
     *
     * @return SalesOrderHeader
     */
    public function setPaymentScheduleName(string $paymentScheduleName): SalesOrderHeader
    {
        return $this->setTrigger('paymentScheduleName', $paymentScheduleName);
    }

    /**
     * @return \DateTime
     */
    public function getPaymentTermsBaseDate(): ?\Datetime
    {
        return $this->paymentTermsBaseDate;
    }

    /**
     * @param \DateTime $paymentTermsBaseDate
     *
     * @return SalesOrderHeader
     */
    public function setPaymentTermsBaseDate(\DateTime $paymentTermsBaseDate): SalesOrderHeader
    {
        return $this->setTrigger('paymentTermsBaseDate', $paymentTermsBaseDate);
    }

    /**
     * @return string
     */
    public function getPaymentTermsName(): ?string
    {
        return $this->paymentTermsName;
    }

    /**
     * @param string $paymentTermsName
     *
     * @return SalesOrderHeader
     */
    public function setPaymentTermsName(string $paymentTermsName): SalesOrderHeader
    {
        return $this->setTrigger('paymentTermsName', $paymentTermsName);
    }

    /**
     * @return string
     */
    public function getPriceCustomerGroupCode(): ?string
    {
        return $this->priceCustomerGroupCode;
    }

    /**
     * @param string $priceCustomerGroupCode
     *
     * @return SalesOrderHeader
     */
    public function setPriceCustomerGroupCode(string $priceCustomerGroupCode): SalesOrderHeader
    {
        return $this->setTrigger('priceCustomerGroupCode', $priceCustomerGroupCode);
    }

    /**
     * @return string
     */
    public function getQuotationNumber(): ?string
    {
        return $this->quotationNumber;
    }

    /**
     * @param string $quotationNumber
     *
     * @return SalesOrderHeader
     */
    public function setQuotationNumber(string $quotationNumber): SalesOrderHeader
    {
        return $this->setTrigger('quotationNumber', $quotationNumber);
    }

    /**
     * @return \DateTime
     */
    public function getRequestedReceiptDate(): ?\DateTime
    {
        return $this->requestedReceiptDate;
    }

    /**
     * @param \DateTime $requestedReceiptDate
     *
     * @return SalesOrderHeader
     */
    public function setRequestedReceiptDate(\DateTime $requestedReceiptDate): SalesOrderHeader
    {
        return $this->setTrigger('requestedReceiptDate', $requestedReceiptDate);
    }

    /**
     * @return \DateTime
     */
    public function getRequestedShippingDate(): ?\Datetime
    {
        return $this->requestedShippingDate;
    }

    /**
     * @param \DateTime $requestedShippingDate
     *
     * @return SalesOrderHeader
     */
    public function setRequestedShippingDate(\DateTime $requestedShippingDate): SalesOrderHeader
    {
        return $this->setTrigger('requestedShippingDate', $requestedShippingDate);
    }

    /**
     * @return string
     */
    public function getSalesOrderName(): ?string
    {
        return $this->salesOrderName;
    }

    /**
     * @param string $salesOrderName
     *
     * @return SalesOrderHeader
     */
    public function setSalesOrderName(string $salesOrderName): SalesOrderHeader
    {
        return $this->setTrigger('salesOrderName', $salesOrderName);
    }

    /**
     * @return string
     */
    public function getSalesOrderNumber(): ?string
    {
        return $this->salesOrderNumber;
    }

    /**
     * @param string $salesOrderNumber
     *
     * @return SalesOrderHeader
     */
    public function setSalesOrderNumber(string $salesOrderNumber): SalesOrderHeader
    {
        return $this->setTrigger('salesOrderNumber', $salesOrderNumber);
    }

    /**
     * @return string
     */
    public function getSalesOrderOriginCode(): ?string
    {
        return $this->salesOrderOriginCode;
    }

    /**
     * @param string $salesOrderOriginCode
     *
     * @return SalesOrderHeader
     */
    public function setSalesOrderOriginCode(string $salesOrderOriginCode): SalesOrderHeader
    {
        return $this->setTrigger('salesOrderOriginCode', $salesOrderOriginCode);
    }

    /**
     * @return string
     */
    public function getSalesOrderPoolId(): ?string
    {
        return $this->salesOrderPoolId;
    }

    /**
     * @param string $salesOrderPoolId
     *
     * @return SalesOrderHeader
     */
    public function setSalesOrderPoolId(string $salesOrderPoolId): SalesOrderHeader
    {
        return $this->setTrigger('salesOrderPoolId', $salesOrderPoolId);
    }

    /**
     * @return string
     */
    public function getSalesOrderPromisingMethod(): ?string
    {
        return $this->salesOrderPromisingMethod;
    }

    /**
     * @param string $salesOrderPromisingMethod
     *
     * @return SalesOrderHeader
     */
    public function setSalesOrderPromisingMethod(string $salesOrderPromisingMethod): SalesOrderHeader
    {
        return $this->setTrigger('salesOrderPromisingMethod', $salesOrderPromisingMethod);
    }

    /**
     * @return string
     */
    public function getSalesOrderStatus(): ?string
    {
        return $this->salesOrderStatus;
    }

    /**
     * @param string $salesOrderStatus
     *
     * @return SalesOrderHeader
     */
    public function setSalesOrderStatus(string $salesOrderStatus): SalesOrderHeader
    {
        return $this->setTrigger('salesOrderStatus', $salesOrderStatus);
    }

    /**
     * @return string
     */
    public function getSalesRebateCustomerGroupId(): ?string
    {
        return $this->salesRebateCustomerGroupId;
    }

    /**
     * @param string $salesRebateCustomerGroupId
     *
     * @return SalesOrderHeader
     */
    public function setSalesRebateCustomerGroupId(string $salesRebateCustomerGroupId): SalesOrderHeader
    {
        return $this->setTrigger('salesRebateCustomerGroupId', $salesRebateCustomerGroupId);
    }

    /**
     * @return string
     */
    public function getSalesTaxGroupCode(): ?string
    {
        return $this->salesTaxGroupCode;
    }

    /**
     * @param string $salesTaxGroupCode
     *
     * @return SalesOrderHeader
     */
    public function setSalesTaxGroupCode(string $salesTaxGroupCode): SalesOrderHeader
    {
        return $this->setTrigger('salesTaxGroupCode', $salesTaxGroupCode);
    }

    /**
     * @return string
     */
    public function getSalesUnitId(): ?string
    {
        return $this->salesUnitId;
    }

    /**
     * @param string $salesUnitId
     *
     * @return SalesOrderHeader
     */
    public function setSalesUnitId(string $salesUnitId): SalesOrderHeader
    {
        return $this->setTrigger('salesUnitId', $salesUnitId);
    }

    /**
     * @return string
     */
    public function getServiceFiscalInformationCode(): ?string
    {
        return $this->serviceFiscalInformationCode;
    }

    /**
     * @param string $serviceFiscalInformationCode
     *
     * @return SalesOrderHeader
     */
    public function setServiceFiscalInformationCode(string $serviceFiscalInformationCode): SalesOrderHeader
    {
        return $this->setTrigger('serviceFiscalInformationCode', $serviceFiscalInformationCode);
    }

    /**
     * @return string
     */
    public function getShippingCarrierId(): ?string
    {
        return $this->shippingCarrierId;
    }

    /**
     * @param string $shippingCarrierId
     *
     * @return SalesOrderHeader
     */
    public function setShippingCarrierId(string $shippingCarrierId): SalesOrderHeader
    {
        return $this->setTrigger('shippingCarrierId', $shippingCarrierId);
    }

    /**
     * @return string
     */
    public function getShippingCarrierServiceGroupId(): ?string
    {
        return $this->shippingCarrierServiceGroupId;
    }

    /**
     * @param string $shippingCarrierServiceGroupId
     *
     * @return SalesOrderHeader
     */
    public function setShippingCarrierServiceGroupId(string $shippingCarrierServiceGroupId): SalesOrderHeader
    {
        return $this->setTrigger('shippingCarrierServiceGroupId', $shippingCarrierServiceGroupId);
    }

    /**
     * @return string
     */
    public function getShippingCarrierServiceId(): ?string
    {
        return $this->shippingCarrierServiceId;
    }

    /**
     * @param string $shippingCarrierServiceId
     *
     * @return SalesOrderHeader
     */
    public function setShippingCarrierServiceId(string $shippingCarrierServiceId): SalesOrderHeader
    {
        return $this->setTrigger('shippingCarrierServiceId', $shippingCarrierServiceId);
    }

    /**
     * Get stripePaymentId
     *
     * @return string
     */
    public function getStripePaymentId(): ?string
    {
        return $this->stripePaymentId;
    }

    /**
     * Set stripePaymentId
     *
     * @param string $stripePaymentId
     *
     * @return SalesOrderHeader
     */
    public function setStripePaymentId(?string $stripePaymentId): self
    {
        return $this->setTrigger('stripePaymentId', $stripePaymentId);
    }

    /**
     * Get stripePaymentStatus
     *
     * @return string
     */
    public function getStripePaymentStatus(): ?string
    {
        return $this->stripePaymentStatus;
    }

    /**
     * Set stripePaymentStatus
     *
     * @param string $stripePaymentStatus
     *
     * @return SalesOrderHeader
     */
    public function setStripePaymentStatus(?string $stripePaymentStatus): self
    {
        return $this->setTrigger('stripePaymentStatus', $stripePaymentStatus);
    }

    /**
     * @return string
     */
    public function getTaxExemptNumber(): ?string
    {
        return $this->taxExemptNumber;
    }

    /**
     * @param string $taxExemptNumber
     *
     * @return SalesOrderHeader
     */
    public function setTaxExemptNumber(string $taxExemptNumber): SalesOrderHeader
    {
        return $this->setTrigger('taxExemptNumber', $taxExemptNumber);
    }

    /**
     * @return string
     */
    public function getTmaCustomerGroupId(): ?string
    {
        return $this->tmaCustomerGroupId;
    }

    /**
     * @param string $tmaCustomerGroupId
     *
     * @return SalesOrderHeader
     */
    public function setTmaCustomerGroupId(string $tmaCustomerGroupId): SalesOrderHeader
    {
        return $this->setTrigger('tmaCustomerGroupId', $tmaCustomerGroupId);
    }

    /**
     * @return float
     */
    public function getTotalAmount(): ?float
    {
        return $this->totalAmount;
    }

    /**
     * @param float $totalAmount
     *
     * @return SalesOrderHeader
     */
    public function setTotalAmount(float $totalAmount): self
    {
        $this->totalAmount = $totalAmount;

        return $this;
    }

    /**
     * @return string
     */
    public function getTotalDiscountCustomerGroupCode(): ?string
    {
        return $this->totalDiscountCustomerGroupCode;
    }

    /**
     * @param string $totalDiscountCustomerGroupCode
     *
     * @return SalesOrderHeader
     */
    public function setTotalDiscountCustomerGroupCode(string $totalDiscountCustomerGroupCode): SalesOrderHeader
    {
        return $this->setTrigger('totalDiscountCustomerGroupCode', $totalDiscountCustomerGroupCode);
    }

    /**
     * @return float
     */
    public function getTotalDiscountPercentage(): ?float
    {
        return $this->totalDiscountPercentage;
    }

    /**
     * @param float $totalDiscountPercentage
     *
     * @return SalesOrderHeader
     */
    public function setTotalDiscountPercentage(float $totalDiscountPercentage): SalesOrderHeader
    {
        return $this->setTrigger('totalDiscountPercentage', $totalDiscountPercentage);
    }

    /**
     * @return string
     */
    public function getTransportationBrokerId(): ?string
    {
        return $this->transportationBrokerId;
    }

    /**
     * @param string $transportationBrokerId
     *
     * @return SalesOrderHeader
     */
    public function setTransportationBrokerId(string $transportationBrokerId): SalesOrderHeader
    {
        return $this->setTrigger('transportationBrokerId', $transportationBrokerId);
    }

    /**
     * @return string
     */
    public function getTransportationModeId(): ?string
    {
        return $this->transportationModeId;
    }

    /**
     * @param string $transportationModeId
     *
     * @return SalesOrderHeader
     */
    public function setTransportationModeId(string $transportationModeId): SalesOrderHeader
    {
        return $this->setTrigger('transportationModeId', $transportationModeId);
    }

    /**
     * @return string
     */
    public function getTransportationRoutePlanId(): ?string
    {
        return $this->transportationRoutePlanId;
    }

    /**
     * @param string $transportationRoutePlanId
     *
     * @return SalesOrderHeader
     */
    public function setTransportationRoutePlanId(string $transportationRoutePlanId): SalesOrderHeader
    {
        return $this->setTrigger('transportationRoutePlanId', $transportationRoutePlanId);
    }

    /**
     * @return string
     */
    public function getTransportationTemplateId(): ?string
    {
        return $this->transportationTemplateId;
    }

    /**
     * @param string $transportationTemplateId
     *
     * @return SalesOrderHeader
     */
    public function setTransportationTemplateId(string $transportationTemplateId): SalesOrderHeader
    {
        return $this->setTrigger('transportationTemplateId', $transportationTemplateId);
    }

    /**
     * @return string
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param string $url
     *
     * @return SalesOrderHeader
     */
    public function setUrl(string $url): SalesOrderHeader
    {
        return $this->setTrigger('url', $url);
    }

    /**
     * @return string
     */
    public function getWillAutomaticInventoryReservationConsiderBatchAttributes(): ?string
    {
        return $this->willAutomaticInventoryReservationConsiderBatchAttributes;
    }

    /**
     * @param string $willAutomaticInventoryReservationConsiderBatchAttributes
     *
     * @return SalesOrderHeader
     */
    public function setWillAutomaticInventoryReservationConsiderBatchAttributes(
        string $willAutomaticInventoryReservationConsiderBatchAttributes
    ): SalesOrderHeader {
        return $this->setTrigger(
            'willAutomaticInventoryReservationConsiderBatchAttributes',
            $willAutomaticInventoryReservationConsiderBatchAttributes
        );
    }

    /**
     * @return bool|null
     */
    public function isCancellable(): ?bool
    {
        return $this->cancellable;
    }

    /**
     * @param bool $cancellable
     *
     * @return SalesOrderHeader
     */
    public function setCancellable(bool $cancellable): self
    {
        $this->cancellable = $cancellable;

        return $this;
    }

    /**
     * @return bool
     */
    public function isFgsable(): bool
    {
        return $this->fgsable;
    }

    /**
     * @param bool $fgsable
     *
     * @return SalesOrderHeader
     */
    public function setFgsable(bool $fgsable): self
    {
        $this->fgsable = $fgsable;

        return $this;
    }

    /**
     * @return bool
     */
    public function isRatable(): ?bool
    {
        return $this->ratable;
    }

    /**
     * @param bool $ratable
     *
     * @return SalesOrderHeader
     */
    public function setRatable(bool $ratable): SalesOrderHeader
    {
        $this->ratable = $ratable;

        return $this;
    }

    /**
     * @return string
     */
    public function getHasRDV(): ?string
    {
        return $this->hasRDV;
    }

    /**
     * @param string|null $hasRDV
     *
     * @return SalesOrderHeader
     */
    public function setHasRDV(?string $hasRDV): SalesOrderHeader
    {
        return $this->setTrigger('hasRDV', $hasRDV);
    }

    /**
     * @return bool
     */
    public function hasRDV(): bool
    {
        return ($this->getHasRDV() === '1');
    }

    /**
     * @return string
     */
    public function getHasAnimation(): ?string
    {
        return $this->hasAnimation;
    }

    /**
     * @param string|null $hasAnimation
     *
     * @return SalesOrderHeader
     */
    public function setHasAnimation(?string $hasAnimation): SalesOrderHeader
    {
        return $this->setTrigger('hasAnimation', $hasAnimation);
    }

    /**
     * @return bool
     */
    public function hasAnimation(): bool
    {
        return ($this->getHasAnimation() === '1');
    }

    /**
     * @Serializer\VirtualProperty()
     * @Serializer\SerializedName("owned_by_coadherent")
     * @return bool
     */
    public function ownedByCoAdherent(): bool
    {
        return $this->orderingCustomerAccountNumber !== $this->invoiceCustomerAccountNumber;
    }

    /**
     * @return SalesOrderDigHeaders|null
     */
    public function getSupplierOrder(): ?SalesOrderDigHeaders
    {
        return $this->supplierOrder;
    }

    /**
     * @param SalesOrderDigHeaders $digHeaders
     *
     * @return SalesOrderHeader
     */
    public function setSupplierOrder(SalesOrderDigHeaders $digHeaders): SalesOrderHeader
    {
        $this->supplierOrder = $digHeaders;

        return $this;
    }
}
