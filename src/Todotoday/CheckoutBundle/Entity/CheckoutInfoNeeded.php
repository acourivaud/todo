<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 20/12/17
 * Time: 14:46
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\CheckoutBundle\Entity
 *
 * @subpackage Todotoday\CheckoutBundle\Entity
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\CheckoutBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * @ORM\Entity(repositoryClass="Todotoday\CheckoutBundle\Repository\CheckoutInfoNeededRepository")
 * @ORM\Table(name="checkout_info_needed", schema="checkout")
 */
class CheckoutInfoNeeded
{
    public const PREFIX_TRANSLATION_KEY = 'checkout_fournisseur_info_';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $productFamily;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $infoNeeded;

    /**
     * @ORM\ManyToOne(targetEntity="Todotoday\CoreBundle\Entity\Agency", inversedBy="checkoutInfos")
     */
    protected $agency;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return CheckoutInfoNeeded
     */
    public function setId(int $id): CheckoutInfoNeeded
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getProductFamily(): ?string
    {
        return $this->productFamily;
    }

    /**
     * @param string|null $productFamily
     *
     * @return CheckoutInfoNeeded
     */
    public function setProductFamily(?string $productFamily): CheckoutInfoNeeded
    {
        $this->productFamily = $productFamily;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getInfoNeeded(): ?string
    {
        return $this->infoNeeded;
    }

    /**
     * @param string $infoNeeded
     *
     * @return CheckoutInfoNeeded
     */
    public function setInfoNeeded(string $infoNeeded): CheckoutInfoNeeded
    {
        $this->infoNeeded = $infoNeeded;

        return $this;
    }

    /**
     * @return Agency|null
     */
    public function getAgency(): ?Agency
    {
        return $this->agency;
    }

    /**
     * @param Agency|null $agency
     *
     * @return CheckoutInfoNeeded
     */
    public function setAgency(?Agency $agency = null): CheckoutInfoNeeded
    {
        $this->agency = $agency;

        return $this;
    }
}
