/**
 * Created by nicolas on 10/04/17.
 */
var formCase = document.getElementById('todotoday_checkout_bundle_checkout_depository_depositoryComment')
if (formCase) {
  var header = formCase.parentElement.firstChild
  var radios = document.forms['todotoday_checkout_bundle_checkout_depository'].elements['todotoday_checkout_bundle_checkout_depository[depository]']
  var newItem = document.createElement('p')
  newItem.setAttribute('id', 'disclaimer')
  var textnode = document.createTextNode($t('cart.checkout_depot_no_locker_help'))
  newItem.appendChild(textnode)
  var contentBagInfo = document.getElementById('content-bag')

  var changeValue = function () {
    if (formCase.required = (this.value === 'Casiers' || this.value === 'Casier')) {
      formCase.placeholder = $t('checkout.disclamer_locker_placeholder')
      newItem.style.display = 'block'
      formCase.style.display = 'block'
      contentBagInfo.style.display = 'block'

    } else {
      formCase.value = ''
      formCase.style.display = 'none'
      newItem.style.display = 'none'
      contentBagInfo.style.display = 'none'
    }
  }

  if (formCase.value === '') {
    formCase.style.display = 'none'
    newItem.style.display = 'none'
    contentBagInfo.style.display = 'none'
  }
  else {
    formCase.placeholder = $t('checkout.disclamer_locker_placeholder')
    newItem.style.display = 'block'
    formCase.style.display = 'block'
    contentBagInfo.style.display = 'block'
  }
  formCase.parentNode.insertBefore(newItem, formCase)

  if (isNodeList(radios)) {
    for (var i = 0, max = radios.length; i < max; i++) {
      radios[i].onclick = changeValue
    }
  } else {
    radios.onclick = changeValue
  }
}

function isNodeList (nodes) {
  var stringRepr = Object.prototype.toString.call(nodes)

  return typeof nodes === 'object' &&
    /^\[object (HTMLCollection|RadioNodeList|NodeList|Object)\]$/.test(stringRepr) &&
    (typeof nodes.length === 'number') &&
    (nodes.length === 0 || (typeof nodes[0] === 'object' && nodes[0].nodeType > 0))
}
