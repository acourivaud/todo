<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 20/12/17
 * Time: 17:55
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\CheckoutBundle\Repository
 *
 * @subpackage Todotoday\CheckoutBundle\Repository
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\CheckoutBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Todotoday\CheckoutBundle\Entity\CheckoutInfoNeeded;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class CheckoutInfoNeededRepository
 */
class CheckoutInfoNeededRepository extends EntityRepository
{
    public const REDIS_CACHE_KEY = 'checkout_info_needed';

    /**
     * @param Agency $agency
     *
     * @return CheckoutInfoNeeded[]
     */
    public function getInfoByAgency(Agency $agency): array
    {
        return $this->createQueryBuilder('c')
            ->where('c.agency = :agency')
            ->setParameter('agency', $agency)
            ->getQuery()
            ->useResultCache(true, 60, self::REDIS_CACHE_KEY . '_' . $agency->getSlug())
            ->getResult();
    }
}
