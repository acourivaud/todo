<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 29/12/17
 * Time: 17:28
 *
 * @category   tdtd
 *
 * @package    Todotoday\CheckoutBundle\Repository
 *
 * @subpackage Todotoday\CheckoutBundle\Repository
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\CheckoutBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class DeliveryDelayRepository
 */
class DeliveryDelayRepository extends EntityRepository
{
}
