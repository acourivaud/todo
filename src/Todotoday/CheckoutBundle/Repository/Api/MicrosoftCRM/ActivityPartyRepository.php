<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 25/04/17
 * Time: 10:14
 */

namespace Todotoday\CheckoutBundle\Repository\Api\MicrosoftCRM;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiRepository;

/**
 * Class ActivityPartyRepository
 * @package Todotoday\CheckoutBundle\Repository\Api\MicrosoftCRM
 */
class ActivityPartyRepository extends AbstractApiRepository
{
}
