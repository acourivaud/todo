<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 20/04/17
 * Time: 18:10
 */

namespace Todotoday\CheckoutBundle\Repository\Api\MicrosoftCRM;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiRepository;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CheckoutBundle\Entity\Api\MicrosoftCRM\ServiceAppointment;

/**
 * Class ServiceAppointmentRepository
 * @package Todotoday\CheckoutBundle\Repository\Api\MicrosoftCRM
 */
class ServiceAppointmentRepository extends AbstractApiRepository
{
    /**
     * @param Adherent $adherent
     *
     * @param bool     $past
     *
     * @return array|null|ServiceAppointment[]
     * @throws \InvalidArgumentException
     */
    public function getAppointmentFromAdherent(Adherent $adherent, bool $past = false): ?array
    {
        if (!$adherent->getCrmIdAdherent()) {
            return null;
        }

        $now = (new \DateTime('now', new \DateTimeZone('UTC')))->format('Y-m-d\TH:i:s\Z');
        $filter = $this->getFilterAdherent($adherent);
        $filter .= $past ? ' and scheduledstart lt ' . $now : ' and scheduledstart gt ' . $now;

        $order = $past ? 'desc' : 'asc';

        return $this
            ->createQueryBuilder()
            ->addCustomQuery(
                '$filter',
                $filter
            )
            ->addCustomQuery(
                '$orderby',
                'scheduledstart ' . $order
            )
            ->getRequest()
            ->execute();
    }

    /**
     * @param Adherent $adherent
     * @param string   $apsCommandeid
     *
     * @return null|ServiceAppointment
     */
    public function getAppointmentFromAdherentAndCommandeid(
        Adherent $adherent,
        string $apsCommandeid
    ): ?ServiceAppointment {
        if (!$adherent->getCrmIdAdherent()) {
            return null;
        }

        $filter =
            '_aps_clientsadherents_value eq ' . $adherent->getCrmIdAdherent() .
            ' and aps_commandeid eq \'' . $apsCommandeid . '\'';

        return $this
                   ->createQueryBuilder()
                   ->addCustomQuery(
                       '$filter',
                       $filter
                   )
                   ->getRequest()
                   ->execute()[0];
    }

    /**
     * @param Adherent $adherent
     *
     * @return string
     */
    private function getFilterAdherent(Adherent $adherent): string
    {
        $adherentFilter = '((_aps_clientsadherents_value eq ' . $adherent->getCrmIdAdherent() . ')';

        $childrens = $adherent->getChildrens();

        /** @var Adherent $children */
        foreach ($childrens as $children) {
            $adherentFilter .= ' or (_aps_clientsadherents_value eq ' . $children->getCrmIdAdherent() . ')';
        }

        $adherentFilter .= ')';

        return $adherentFilter;
    }
}
