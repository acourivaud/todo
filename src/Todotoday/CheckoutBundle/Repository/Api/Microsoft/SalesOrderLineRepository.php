<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 05/04/17
 * Time: 13:25
 */

namespace Todotoday\CheckoutBundle\Repository\Api\Microsoft;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiRepository;
use Todotoday\CheckoutBundle\Entity\Api\Microsoft\SalesOrderLine;

/**
 * Class SalesOrderLineRepository
 * @package Todotoday\CheckoutBundle\Repository\Api\Microsoft
 */
class SalesOrderLineRepository extends AbstractApiRepository
{
    /**
     * @param string $orderNumber
     *
     * @return SalesOrderLine[]|null
     * @throws \InvalidArgumentException
     */
    public function getLinesFromOrder(string $orderNumber): ?array
    {
        $filter = 'SalesOrderNumber eq \'' . $orderNumber . '\'';

        return $this
            ->createQueryBuilder()
            ->addCustomQuery(
                '$filter',
                $filter
            )
            ->getRequest()
            ->execute();
    }
}
