<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 05/04/17
 * Time: 13:25
 */

namespace Todotoday\CheckoutBundle\Repository\Api\Microsoft;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiRepository;
use Carbon\Carbon;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CheckoutBundle\Entity\Api\Microsoft\SalesOrderHeader;
use Todotoday\CheckoutBundle\Enum\OrderStatusEnum;

/**
 * Class SalesOrderHeaderRepository
 *
 * @package Todotoday\CheckoutBundle\Repository\Api\Microsoft
 */
class SalesOrderHeaderRepository extends AbstractApiRepository
{
    /**
     * @param Adherent $adherent
     *
     * @return null|array
     * @throws \InvalidArgumentException
     */
    public function getLastOrder(Adherent $adherent): ?array
    {
        $filter = $this->getAdherentAndCoAdherentFilter($adherent);
        $filter .= OrderStatusEnum::getFilterPending();

        return $this
            ->createQueryBuilder()
            ->addCustomQuery(
                '$filter',
                $filter
            )
            ->addCustomQuery(
                '$orderby',
                'CreationDate desc'
            )
            ->addCustomQuery(
                '$top',
                '1'
            )
            ->getRequest()
            ->execute();
    }

    /**
     * Get Pending Order for given adherent
     *
     * @param Adherent    $adherent
     *
     * @param string|null $status
     *
     * @return SalesOrderHeader[]|null
     * @throws \InvalidArgumentException
     */
    public function getOrders(Adherent $adherent, ?string $status = 'pending'): ?array
    {
        $filter = $this->getAdherentAndCoAdherentFilter($adherent);
        if ('pending' === $status) {
            $filter .= OrderStatusEnum::getFilterPending();
        } else {
            $filter .= OrderStatusEnum::getFilterHistory();
        }

        return $this
            ->createQueryBuilder()
            ->addCustomQuery(
                '$filter',
                $filter
            )
            ->addCustomQuery(
                '$orderby',
                'CreationDate desc'
            )
            ->getRequest()
            ->execute();
    }

    /**
     * @param Adherent $adherent
     *
     * @return array|null
     * @throws \InvalidArgumentException
     */
    public function getRatableOrders(Adherent $adherent): ?array
    {
        $filter = $this->getAdherentFilter($adherent);
        $filter .= OrderStatusEnum::getFilterRatable();

        return $this
            ->createQueryBuilder()
            ->addCustomQuery(
                '$filter',
                $filter
            )
            ->addCustomQuery(
                '$orderby',
                'CreationDate desc'
            )
            ->getRequest()
            ->execute();
    }

    /**
     * @param Adherent $adherent
     *
     * @return array|null
     * @throws \InvalidArgumentException
     */
    public function getRatableOrdersShortSelect(Adherent $adherent): ?array
    {
        $filter = $this->getAdherentFilter($adherent);
        $filter .= OrderStatusEnum::getFilterRatable();

        return $this
            ->createQueryBuilder()
            ->addCustomQuery(
                '$select',
                'SalesOrderNumber'
            )
            ->addCustomQuery(
                '$filter',
                $filter
            )
            ->getRequest()
            ->execute();
    }

    /**
     * @param Adherent $adherent
     *
     * @param int      $days
     *
     * @return array|null|SalesOrderHeader[]
     * @throws \InvalidArgumentException
     */
    public function getLastDaysOrder(Adherent $adherent, int $days): ?array
    {
        $now = Carbon::now('UTC');
        $limitDate = $now->subDays($days);

        $filter = $this->getAdherentAndCoAdherentFilter($adherent);
        $filter .= ' and CreationDate lt ' . $limitDate->format('Y-m-d\TH:i:s\Z');
        $filter .= OrderStatusEnum::getFilterPending();

        return $this
            ->createQueryBuilder()
            ->addCustomQuery(
                '$filter',
                $filter
            )
            ->addCustomQuery(
                '$orderby',
                'CreationDate desc'
            )
            ->getRequest()
            ->execute();
    }

    /**
     * @param Adherent  $adherent
     * @param \DateTime $datetime
     *
     * @return array|null|SalesOrderHeader[]
     * @throws \InvalidArgumentException
     *
     */
    public function getMonthOrders(Adherent $adherent, \DateTime $datetime): ?array
    {
        $filter = $this->getAdherentAndCoAdherentFilter($adherent);
        $filter .= $this->getMonthFilter($datetime);

        return $this
            ->createQueryBuilder()
            ->addCustomQuery(
                '$filter',
                $filter
            )
            ->addCustomQuery(
                '$orderby',
                'CreationDate desc'
            )
            ->getRequest()
            ->execute();
    }

    /**
     * @param Adherent  $adherent
     * @param \DateTime $datetime
     *
     * @return array|null|SalesOrderHeader[]
     * @throws \InvalidArgumentException
     *
     */
    public function getMonthHistoryOrders(Adherent $adherent, \DateTime $datetime): ?array
    {
        $filter = $this->getAdherentAndCoAdherentFilter($adherent);
        $filter .= OrderStatusEnum::getFilterHistory();
        $filter .= $this->getMonthFilter($datetime);

        return $this
            ->createQueryBuilder()
            ->addCustomQuery(
                '$filter',
                $filter
            )
            ->addCustomQuery(
                '$orderby',
                'CreationDate desc'
            )
            ->getRequest()
            ->execute();
    }

    /**
     * @param Adherent $adherent
     *
     * @return array|null
     * @throws \InvalidArgumentException
     */
    public function getQuotationOrders(Adherent $adherent): ?array
    {
        $filter = $this->getAdherentAndCoAdherentFilter($adherent);
        $filter .= OrderStatusEnum::getFilterQuotation();

        return $this
            ->createQueryBuilder()
            ->addCustomQuery(
                '$select',
                'SalesOrderNumber'
            )
            ->addCustomQuery(
                '$filter',
                $filter
            )
            ->getRequest()
            ->execute();
    }

    /**
     * @param string $epa
     * @param string $orderId
     *
     * @return string
     */
    public function cancelOrder(string $epa, string $orderId): string
    {
        return $this->sendPostActionOrder($epa, $orderId, 'Cancel', array());
    }

    /**
     * @param string $epa
     * @param string $orderId
     * @param string $rating
     *
     * @return string
     */
    public function rateOrder(string $epa, string $orderId, string $rating): string
    {
        return $this->sendPostActionOrder($epa, $orderId, 'Rate', array('_rate' => $rating));
    }

    /**
     * @param string $epa
     * @param string $orderId
     * @param string $action
     * @param array  $fields
     *
     * @return string
     */
    private function sendPostActionOrder(string $epa, string $orderId, string $action, array $fields): string
    {
        $connection = $this->getApiConnection();
        $token = $connection->connect()->getAccessToken();
        $url = $connection->getBaseUrl();

        $url .= rawurlencode(
            '/TDTDSalesOrderHeaders(SalesOrderNumber=\'' . $orderId . '\',dataAreaId=\'' . $epa
            . '\')/Microsoft.Dynamics.DataEntities.' . $action
        );
        $url .= '?cross-company=true';

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            array(
                'Content-Type: application/json',
                'Authorization: Bearer ' .
                $token,
            )
        );
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields ? json_encode($fields) : []);

        $response = curl_exec($ch);
        $info = curl_getinfo($ch);

        if ($response === false
            || (intdiv($info['http_code'], 100) !== 2)
        ) {
            throw new \InvalidArgumentException(
                'Curl return error ' . $info['http_code'] . ' for ' . $info['url'] . ' with message ' . $response
            );
        }

        $results = json_decode($response, true);

        return $results['value'];
    }

    /**
     * @param Adherent $adherent
     *
     * @return string
     */
    private function getAdherentFilter(Adherent $adherent): string
    {
        $filter = '(OrderingCustomerAccountNumber eq \'' . $adherent->getCustomerAccount() . '\')';

        return $filter;
    }

    /**
     * @param Adherent $adherent
     *
     * @return string
     */
    private function getAdherentAndCoAdherentFilter(Adherent $adherent): string
    {
        $filter = '((OrderingCustomerAccountNumber eq \'' . $adherent->getCustomerAccount() . '\')';
        $filter .= ' or (InvoiceCustomerAccountNumber eq \'' . $adherent->getCustomerAccount() . '\'))';

        return $filter;
    }

    /**
     * @param \DateTime $datetime
     *
     * @return string
     *
     */
    private function getMonthFilter(\DateTime $datetime): string
    {
        $dateFormat = 'Y-m-d\TH:i:s\Z';
        $carbon = Carbon::instance($datetime);
        $firstDayOfMonth = $carbon->startOfMonth()->format($dateFormat);
        $lastDayOfMonth = $carbon->lastOfMonth()->format($dateFormat);

        $filter = ' and CreationDate ge ' . $firstDayOfMonth . ' and CreationDate le ' . $lastDayOfMonth;

        return $filter;
    }
}
