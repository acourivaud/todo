<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 05/04/17
 * Time: 13:25
 */

namespace Todotoday\CheckoutBundle\Repository\Api\Microsoft;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiRepository;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CheckoutBundle\Entity\Api\Microsoft\SalesOrderDigHeaders;

/**
 * Class SalesOrderDigHeadersRepository
 *
 * @package Todotoday\CheckoutBundle\Repository\Api\Microsoft
 */
class SalesOrderDigHeadersRepository extends AbstractApiRepository
{
    /**
     * @param Adherent $adherent
     *
     * @return null|int
     * @throws \InvalidArgumentException
     */
    public function getRefusedOrdersNumber(Adherent $adherent): ?int
    {
        $result = $this->getRefusedOrder($adherent);

        if (!$result) {
            return null;
        }

        return \count($result);
    }

    /**
     * @param Adherent $adherent
     * @param string   $alesOrderNumber
     *
     * @return SalesOrderDigHeaders|null
     * @throws \InvalidArgumentException
     */
    public function getSupplierOrder(Adherent $adherent, string $alesOrderNumber): ?SalesOrderDigHeaders
    {
        $filter = $this->getAdherentFilter($adherent);
        $filter .= ' and SalesOrderNumber eq \'' . $alesOrderNumber . '\'';

        $result = $this->createQueryBuilder()
            ->addCustomQuery(
                '$filter',
                $filter
            )
            ->getRequest()
            ->execute();

        // we should only have on supplier per order
        if (!is_null($result) && \count($result) > 0) {
            return $result[0];
        }

        return null;
    }

    /**
     * @param Adherent $adherent
     *
     * @return SalesOrderDigHeaders[]|null
     * @throws \InvalidArgumentException
     */
    public function getRefusedOrder(Adherent $adherent): ?array
    {
        $filter = $this->getAdherentFilter($adherent);
        $filter .= ' and ResponseState eq Microsoft.Dynamics.DataEntities.PurchaseOrderResponseState\'Rejected\'';

        return $this
            ->createQueryBuilder()
            ->addCustomQuery(
                '$filter',
                $filter
            )
            ->addCustomQuery(
                '$orderby',
                'CreationDate desc'
            )
            ->getRequest()
            ->execute();
    }

    /**
     * @param Adherent $adherent
     *
     * @return string
     */
    private function getAdherentFilter(Adherent $adherent): string
    {
        $filter = '(OrderingCustomerAccountNumber eq \'' . $adherent->getCustomerAccount() . '\')';

        return $filter;
    }

//    /**
//     * @param Adherent $adherent
//     *
//     * @return string
//     */
//    private function getAdherentAndCoAdherentFilter(Adherent $adherent): string
//    {
//        $filter = '((OrderingCustomerAccountNumber eq \'' . $adherent->getCustomerAccount() . '\')';
//        $filter .= ' or (InvoiceCustomerAccountNumber eq \'' . $adherent->getCustomerAccount() . '\'))';
//
//        return $filter;
//    }
}
