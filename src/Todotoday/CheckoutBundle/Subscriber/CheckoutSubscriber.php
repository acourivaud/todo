<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 12/04/17
 * Time: 14:26
 */

namespace Todotoday\CheckoutBundle\Subscriber;

use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Todotoday\CartBundle\Services\CartProductManager;
use Todotoday\CheckoutBundle\Event\BookingSuccessEvent;
use Todotoday\CheckoutBundle\Event\CheckoutSuccessEvent;

/**
 * Class CheckoutSubscriber
 * @package Todotoday\CheckoutBundle\Subscriber
 */
class CheckoutSubscriber implements EventSubscriberInterface
{
    /**
     * @var CartProductManager
     */
    private $cartManager;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * CheckoutSubscriber constructor.
     *
     * @param CartProductManager $cartManager
     * @param LoggerInterface    $logger
     */
    public function __construct(CartProductManager $cartManager, LoggerInterface $logger)
    {
        $this->cartManager = $cartManager;
        $this->logger = $logger;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            CheckoutSuccessEvent::NAME => 'onCheckoutSuccess',
            BookingSuccessEvent::NAME => 'onBookingSuccess',
        );
    }

    /**
     * @param CheckoutSuccessEvent $event
     *
     * @throws \LogicException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \Todotoday\CartBundle\Exception\NotAdherentCartBundleException
     * @throws \UnexpectedValueException
     * @throws \InvalidArgumentException
     */
    public function onCheckoutSuccess(CheckoutSuccessEvent $event)
    {
        $checkout = $event->getCheckout();
        $this->cartManager->emptyCart($checkout->getAgency());
        $this->logger->info('ORDER : success for ' . $checkout->getAdherent()->getUsername());
    }

    /**
     * @param BookingSuccessEvent $event
     */
    public function onBookingSuccess(BookingSuccessEvent $event)
    {
        $checkout = $event->getCheckout();
        $this->logger->info('BOOKING : success for ' . $checkout->getAdherent()->getUsername());
    }
}
