<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 16/03/17
 * Time: 09:45
 */

namespace Todotoday\CheckoutBundle\Twig;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Todotoday\CheckoutBundle\Repository\Api\Microsoft\SalesOrderDigHeadersRepository;

/**
 * Class OrderExtension
 *
 * @package Todotoday\AccountBundle\Twig
 */
class OrderExtension extends \Twig_Extension
{
    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * @var SalesOrderDigHeadersRepository
     */
    private $headerRefusedRepository;

    /**
     * JwtExtension constructor.
     *
     * @param TokenStorage                   $tokenStorage
     * @param SalesOrderDigHeadersRepository $headerRefusedRepository
     */
    public function __construct(TokenStorage $tokenStorage, SalesOrderDigHeadersRepository $headerRefusedRepository)
    {
        $this->tokenStorage = $tokenStorage;
        $this->headerRefusedRepository = $headerRefusedRepository;
    }

    /**
     * @return array
     */
    public function getFunctions(): array
    {
        return array(
            new \Twig_SimpleFunction('getNumberOrderRefused', array($this, 'getNumberOrderRefused')),
        );
    }

    /**
     * @return int
     * @throws \InvalidArgumentException
     */
    public function getNumberOrderRefused(): ?int
    {
        $adherent = $this->tokenStorage->getToken()->getUser();

        return $this->headerRefusedRepository->getRefusedOrdersNumber($adherent);
    }
}
