<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 29/12/17
 * Time: 17:02
 *
 * @category   tdtd
 *
 * @package    Todotoday\CheckoutBundle\Twig
 *
 * @subpackage Todotoday\CheckoutBundle\Twig
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\CheckoutBundle\Twig;

use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use Todotoday\CheckoutBundle\Entity\DeliveryDelay;
use Todotoday\CoreBundle\Services\DomainContextManager;

/**
 * Class CheckoutDeliveryDateExtension
 */
class CheckoutDeliveryDateExtension extends \Twig_Extension
{
    /**
     * @var DomainContextManager
     */
    private $contextManager;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * CheckoutDeliveryDateExtension constructor.
     *
     * @param DomainContextManager   $contextManager
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(DomainContextManager $contextManager, EntityManagerInterface $entityManager)
    {
        $this->contextManager = $contextManager;
        $this->entityManager = $entityManager;
    }

    /**
     * @return array|\Twig_SimpleFunction[]
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('get_delivery_date', [$this, 'getDeliveryDate']),
        ];
    }

    /**
     * @param \DateTime $responseSupplierDate
     *
     * @param string    $mainCategoryName
     *
     * @return \DateTime|null
     * @throws \UnexpectedValueException
     */
    public function getDeliveryDate(\DateTime $responseSupplierDate, string $mainCategoryName): ?\DateTime
    {
        if (!$this->contextManager->getCurrentRequest()
            || !$agency = $this->contextManager->getCurrentContext()->getAgency()) {
            // si par hasard on aurait pas la conciergerie on ajoute 2 jours au pif
            // (je ne vois pas comment ça pourrait arriver mais bon)
            return (new \DateTime())->modify('+5 days');
        }

        $repo = $this->entityManager->getRepository(DeliveryDelay::class);

        $delays = $repo->findBy(
            [
                'agency' => $agency,
            ]
        );
        $dayToAdd = 10;
        foreach ($delays as $delay) {
            $family = $delay->getProductFamily();
            // OMG là ça devient cradre
            // en effet à partir de la commande je n'ai pas accès à la hierarchy du produit
            // je peux récuperer la main category name qui est du type : 104F - Prestation Femme
            // comme dans la bdd sur les delays que l'on ajoute il n'y a que que des catégory name de type 101, 102,
            // 103, ça fonctionne MAIS CEST CRADE
            if (0 === strpos($mainCategoryName, $family)) {
                // on calcul le nombre de jour à ajouter
                $dayToAdd = (int) round((float) $delay->getDelay() / 24);
                // dès que l'on trouve notre première occurence on break
                break;
            }
        }
        // si rien ne correspond on ne calcule pas la date de livraison
        if (!$dayToAdd) {
            return null;
        }

        $deliveryDate = Carbon::create(
            $responseSupplierDate->format('Y'),
            $responseSupplierDate->format('m'),
            $responseSupplierDate->format('d')
        );

        // on ajoute de delay que l'on a trouvé
        return $deliveryDate->addWeekday($dayToAdd);
    }
}
