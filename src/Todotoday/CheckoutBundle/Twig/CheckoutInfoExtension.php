<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 20/12/17
 * Time: 15:45
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\CheckoutBundle\Twig
 *
 * @subpackage Todotoday\CheckoutBundle\Twig
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\CheckoutBundle\Twig;

use Todotoday\CheckoutBundle\Service\CheckoutInfoManager;

/**
 * Class CheckoutInfoExtension
 */
class CheckoutInfoExtension extends \Twig_Extension
{
    /**
     * @var CheckoutInfoManager
     */
    private $checkoutInfoManager;

    /**
     * CheckoutInfoExtension constructor.
     *
     * @param CheckoutInfoManager $checkoutInfoManager
     */
    public function __construct(CheckoutInfoManager $checkoutInfoManager)
    {
        $this->checkoutInfoManager = $checkoutInfoManager;
    }

    /**
     * @return array|\Twig_SimpleFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new \Twig_SimpleFunction('get_checkout_info', [$this, 'getCheckoutInfo']),
            new \Twig_SimpleFunction('has_checkout_info', [$this, 'hasCheckoutInfo']),
        ];
    }

    /**
     * @param string|null $hierarchy
     *
     * @return array
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     */
    public function getCheckoutInfo(?string $hierarchy): array
    {
        return $this->checkoutInfoManager->getCheckoutInfo($hierarchy);
    }

    /**
     * @param string|null $hierarchy
     *
     * @return bool
     * @throws \UnexpectedValueException
     */
    public function hasCheckoutInfo(?string $hierarchy): bool
    {
        return $this->checkoutInfoManager->hasCheckoutInfo($hierarchy);
    }
}
