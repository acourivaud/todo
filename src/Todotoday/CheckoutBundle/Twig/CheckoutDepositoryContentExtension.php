<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 15/01/18
 * Time: 09:36
 *
 * @category   tdtd
 *
 * @package    Todotoday\CheckoutBundle\Twig
 *
 * @subpackage Todotoday\CheckoutBundle\Twig
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\CheckoutBundle\Twig;

use Todotoday\CartBundle\Services\CartProductManager;
use Todotoday\CoreBundle\Services\DomainContextManager;

/**
 * Class CheckoutDepositoryContentExtension
 */
class CheckoutDepositoryContentExtension extends \Twig_Extension
{
    /**
     * @var CartProductManager
     */
    private $cartProductManager;

    /**
     * @var DomainContextManager
     */
    private $contextManager;

    /**
     * CheckoutDepositoryContentExtension constructor.
     *
     * @param CartProductManager   $cartProductManager
     * @param DomainContextManager $contextManager
     */
    public function __construct(CartProductManager $cartProductManager, DomainContextManager $contextManager)
    {
        $this->cartProductManager = $cartProductManager;
        $this->contextManager = $contextManager;
    }

    /**
     * @return array
     */
    public function getFunctions(): array
    {
        return [
            new \Twig_SimpleFunction('makeDepositoryContent', [$this, 'makeDepositoryContent']),
        ];
    }

    /**
     * @return array
     * @throws \UnexpectedValueException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function makeDepositoryContent(): array
    {
        if (!$this->contextManager->getCurrentRequest() || !$this->contextManager->getCurrentContext()
            || !$agency = $this->contextManager->getCurrentContext()->getAgency()) {
            return [];
        }

        return $this->cartProductManager->getAllSuppliers($agency);
    }
}
