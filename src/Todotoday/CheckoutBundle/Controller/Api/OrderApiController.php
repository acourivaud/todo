<?php

declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 30/04/17
 * Time: 16:56
 */

namespace Todotoday\CheckoutBundle\Controller\Api;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as FosRest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Acl\Model\DomainObjectInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CheckoutBundle\Entity\Api\Microsoft\SalesOrderHeader;
use Todotoday\CheckoutBundle\Entity\Api\Microsoft\SalesOrderLine;
use Todotoday\CheckoutBundle\Enum\OrderStatusEnum;
use Todotoday\CheckoutBundle\Exception\OrderNotBelongsToAdherentException;
use Todotoday\CheckoutBundle\Exception\OrderNotRefusedException;
use Todotoday\CheckoutBundle\Repository\Api\Microsoft\SalesOrderDigHeadersRepository;

/**
 * @FosRest\Prefix("/order")
 * @FosRest\NamePrefix("todotoday.checkout.api.order.")
 * Class OrderApiController
 * @package Todotoday\CheckoutBundle\Controller\Api
 */
class OrderApiController extends FOSRestController implements DomainObjectInterface
{
    /**
     * Get list of order for adherent
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Get list of order for adherent",
     *     description="Get list of order for adherent",
     *     views={"order"},
     *     section="order"
     * )
     *
     * @FosRest\Route("", options={"expose"=true})
     * @return Response
     * @throws \BadMethodCallException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     * @throws \LogicException
     **/
    public function getOrderAction(): Response
    {
        if (!$this->isGranted('VIEW', $this)) {
            throw $this->createAccessDeniedException();
        }

        $orderManager = $this->get('todotoday.order.manager');

        $orders = $orderManager->getFullOrder($this->getUser());
        $view = $this->view($orders);
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * Get list of order for adherent
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Get list of order in month and year for adherent",
     *     description="Get list of order in month and year for adherent",
     *     views={"order"},
     *     section="order"
     * )
     *
     * @FosRest\Route("/month/{monthYear}", options={"expose"=true})
     *
     * @param string $monthYear
     *
     * @return Response
     * @throws \BadMethodCallException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     * @throws \LogicException
     **/
    public function getOrderMonthAction(string $monthYear): Response
    {
        if (!$this->isGranted('VIEW', $this)) {
            throw $this->createAccessDeniedException();
        }

        $orderManager = $this->get('todotoday.order.manager');

        $orders = $orderManager->getAllMonthOrders($this->getUser(), $monthYear);
        $view = $this->view($orders);
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * Get list of order for history adherent's order
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Get list of order for history adherent's order",
     *     description="Get list of order for history adherent's order",
     *     views={"order"},
     *     section="order"
     * )
     *
     * @FosRest\Route("/history")
     * @return Response
     * @throws \BadMethodCallException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     * @throws \LogicException
     **/
    public function getOrderHistoryAction(): Response
    {
        if (!$this->isGranted('VIEW', $this)) {
            throw $this->createAccessDeniedException();
        }

        $orderManager = $this->get('todotoday.order.manager');

        $orders = $orderManager->getFullOrder($this->getUser(), 'history');
        $view = $this->view($orders);
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * Get list of order for history adherent's order for a specific month
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Get list of order for history adherent's order for a specific month",
     *     description="Get list of order for history adherent's order for a specific month",
     *     views={"order"},
     *     section="order"
     * )
     *
     * @FosRest\Route("/history/{monthYear}", options={"expose"=true})
     * @param string $monthYear
     *
     * @return Response
     * @throws \BadMethodCallException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \InvalidArgumentException
     * @throws \LogicException
     */
    public function getOrderHistoryMonthAction(string $monthYear): Response
    {
        if (!$this->isGranted('VIEW', $this)) {
            throw $this->createAccessDeniedException();
        }

        $orderManager = $this->get('todotoday.order.manager');

        $orders = $orderManager->getMonthHistoryOrders($this->getUser(), $monthYear);
        $view = $this->view($orders);
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * Get list of orders that needs a rate
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Get list of orders that needs a rate",
     *     description="Get list of orders that needs a rate",
     *     views={"order"},
     *     section="order"
     * )
     *
     * @FosRest\Route("/ratable", options={"expose"=true})
     * @return Response
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \BadMethodCallException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \InvalidArgumentException
     * @throws \LogicException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function getRatableOrderAction(): Response
    {
        if (!$this->isGranted('VIEW', $this)) {
            throw $this->createAccessDeniedException();
        }

        $orderMananger = $this->get('todotoday.order.manager');

        $ratableOrders = $orderMananger->getRatableOrders($this->getUser());
        $view = $this->view($ratableOrders);
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * Get number of orders that needs a rate
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Get number of orders that needs a rate",
     *     description="Get number of orders that needs a rate",
     *     views={"order"},
     *     section="order"
     * )
     *
     * @FosRest\Route("/ratable/number", options={"expose"=true})
     * @return Response
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \InvalidArgumentException
     * @throws \LogicException
     **/
    public function getNumberRatableOrderAction(): Response
    {
        if (!$this->isGranted('VIEW', $this)) {
            throw $this->createAccessDeniedException();
        }

        $orderMananger = $this->get('todotoday.order.manager');

        $numberRatableOrders = $orderMananger->getNumberRatableOrders($this->getUser());
        $view = $this->view($numberRatableOrders);

        return $this->handleView($view);
    }

    /**
     * Get number of orders refused by fournisseur
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Get number of orders refused by fournisseur",
     *     description="Get number of orders refused by fournisseur",
     *     views={"order"},
     *     section="order"
     * )
     *
     * @FosRest\Route("/refused/number", options={"expose"=true})
     * @return Response
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \InvalidArgumentException
     * @throws \LogicException
     **/
    public function getNumberRefusedOrderAction(): Response
    {
        if (!$this->isGranted('VIEW', $this)) {
            throw $this->createAccessDeniedException();
        }

        /** @var SalesOrderDigHeadersRepository $orderRefusedRepo */
        $orderRefusedRepo = $this->get('todotoday.checkout.repository.api.order_refused');

        $numberRatableOrders = $orderRefusedRepo->getRefusedOrdersNumber($this->getUser());
        $view = $this->view($numberRatableOrders);

        return $this->handleView($view);
    }

    /**
     * Get list of booked service for adherent
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Get list of booked service for adherent",
     *     description="Get list of booked service for adherent",
     *     views={"order"},
     *     section="order"
     * )
     *
     * @FosRest\Route("/booked")
     * @return Response
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \InvalidArgumentException
     * @throws \LogicException
     **/
    public function getBookingAction(): Response
    {
        if (!$this->isGranted('VIEW', $this)) {
            throw $this->createAccessDeniedException();
        }

        $repo = $this->get('todotoday.checkout.repository.api.service_appointment');

        $bookedService = $repo->getAppointmentFromAdherent($this->getUser());
        $view = $this->view($bookedService);
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * Get list of history booked service for adherent
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Get list of history booked service for adherent",
     *     description="Get list of history booked service for adherent",
     *     views={"order"},
     *     section="order"
     * )
     *
     * @FosRest\Route("/booked/history")
     * @return Response
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \InvalidArgumentException
     * @throws \LogicException
     **/
    public function getBookingHistoryAction(): Response
    {
        if (!$this->isGranted('VIEW', $this)) {
            throw $this->createAccessDeniedException();
        }

        $repo = $this->get('todotoday.checkout.repository.api.service_appointment');

        $bookedService = $repo->getAppointmentFromAdherent($this->getUser(), true);
        $view = $this->view($bookedService);
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * Cancel an order with refOrder
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Cancel an order with refOrder",
     *     description="Cancel an order with refOrder",
     *     views={"order"},
     *     section="order"
     * )
     *
     * @FosRest\Route("/{refOrder}/cancel", options={"expose"=true})
     * @param string $refOrder
     *
     * @return Response
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \LogicException
     * @throws \Todotoday\CheckoutBundle\Exception\BookingCantBeCancelledException
     * @throws \Todotoday\CheckoutBundle\Exception\OrderNotBelongsToAdherentException
     * @throws \Todotoday\CheckoutBundle\Exception\OrderNotCancellableException
     * @throws \Todotoday\CheckoutBundle\Exception\OrderNotFoundException
     */
    public function putCancelRefOrderAction(string $refOrder): Response
    {
        if (!$this->isGranted('EDIT', $this)) {
            throw $this->createAccessDeniedException();
        }

        $manager = $this->get('todotoday.order.manager');
        $result = $manager->cancelOrder($this->getUser(), $refOrder);

        $view = $this->view($result);

        return $this->handleView($view);
    }

    /**
     * Rate an order with refOrder
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Rate an order with refOrder",
     *     description="Rate an order with refOrder",
     *     views={"order"},
     *     section="order"
     * )
     *
     * @FosRest\Route("/{refOrder}/rate", options={"expose"=true})
     * @param Request $request
     * @param string  $refOrder
     *
     * @return Response
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Todotoday\CheckoutBundle\Exception\OrderNotRatableException
     * @throws \Todotoday\CheckoutBundle\Exception\OrderNotBelongsToAdherentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     * @throws \LogicException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Todotoday\CheckoutBundle\Exception\OrderNotFoundException
     */
    public function putRateRefOrderAction(Request $request, string $refOrder): Response
    {
        if (!$this->isGranted('EDIT', $this)) {
            throw $this->createAccessDeniedException();
        }

        $adherent = $this->getUser();

        $manager = $this->get('todotoday.order.manager');

        //        $isReceipted = $request->get('isReceipted');
        $rate = $request->get('rate');
        //        $comment = $request->get('comment');

        $view = $this->view($manager->rateOrder($adherent, $refOrder, (string) $rate));

        return $this->handleView($view);
    }

    /**
     * Edit an order and order lines
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Edit an order and order lines",
     *     description="Edit an order and order lines",
     *     views={"order"},
     *     section="order"
     * )
     *
     * @FosRest\Route("/edit", options={"expose"=true})
     * @param Request $request
     *
     * @return Response
     * @throws \Todotoday\CheckoutBundle\Exception\OrderNotBelongsToAdherentException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws OrderNotBelongsToAdherentException
     * @throws OrderNotRefusedException
     */
    public function postEditRefusedOrderAction(Request $request): Response
    {
        if (!$this->isGranted('EDIT', $this)) {
            throw $this->createAccessDeniedException();
        }

        $manager = $this->get('todotoday.order.manager');

        $order = $request->request->get('SalesOrderHeader');
        $orderLines = $request->request->get('Lines');
        /** @var Adherent $adherent */
        $adherent = $this->getUser();

        $orderMicrosoftUpdated = $manager->editOrderRefused($adherent, $order, $orderLines);

        $view = $this->view($orderMicrosoftUpdated);

        return $this->handleView($view);
    }

    /**
     * Returns a unique identifier for this domain object.
     *
     * @return string
     */
    public function getObjectIdentifier(): string
    {
        return 'class';
    }
}
