<?php

declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 17/04/17
 * Time: 14:02
 */

namespace Todotoday\CheckoutBundle\Controller\Api;

use Carbon\Carbon;
use Exception;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as FosRest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Acl\Model\DomainObjectInterface;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Exceptions\NotAnAdherentException;
use Todotoday\CheckoutBundle\Exception\BookingAgencyCrmSiteMissingException;
use Todotoday\CheckoutBundle\Exception\BookingCrmIdAdherentMissingException;
use Todotoday\CheckoutBundle\Exception\BookingLockedException;
use Todotoday\CheckoutBundle\Exception\BookingMissingSlotException;
use Todotoday\CheckoutBundle\Exception\BookingProductMissingException;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Exceptions\AgencyMissingException;

/**
 * @FosRest\Prefix("/booking")
 * @FosRest\NamePrefix("todotoday.checkout.api.booking.")
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * Class BookingApiController
 *
 * @package Todotoday\CheckoutBundle\Controller\Api
 */
class BookingApiController extends FOSRestController implements DomainObjectInterface
{
    /**
     * Returns a unique identifier for this domain object.
     *
     * @return string
     */
    public function getObjectIdentifier(): string
    {
        return 'class';
    }

    /**
     * Get available slot for given service and agency
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Get available slot for given service and agency",
     *     description="Get available slot for given service and agency",
     *     views={"booking"},
     *     section="booking"
     * )
     *
     * @FosRest\Route("/slot/{nextWeek}", defaults={"nextWeek" = null}, requirements={"\d+"}, options={"expose"=true})
     *
     * @param int     $nextWeek
     * @param Request $request
     *
     * @return object|Agency
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Todotoday\CheckoutBundle\Exception\BookingProductMissingException
     * @throws \Todotoday\CheckoutBundle\Exception\BookingAgencyCrmSiteMissingException
     * @throws \Todotoday\CheckoutBundle\Exception\BookingProductCrmIdServiceMissingException
     * @throws \Todotoday\CheckoutBundle\Exception\BookingInvalidProductBookedException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingException
     * @throws \Todotoday\CatalogBundle\Exceptions\ProductNotFoundException
     * @throws \UnexpectedValueException
     * @throws \LogicException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Exception
     */
    public function getSlotAction(int $nextWeek = null, Request $request)
    {
        if (!$this->isGranted('VIEW', $this)) {
            throw $this->createAccessDeniedException();
        }

        if (!$productId = $request->headers->get('product')) {
            throw new BookingProductMissingException();
        }

        $agency = $this->getAgency();

        if (!$agency->getCrmSite()) {
            throw new BookingAgencyCrmSiteMissingException($agency->getSlug());
        }

        $bookingManager = $this->get('todotoday.checkout.booking.manager');
        $result = $bookingManager->getAvailableSlots($agency, $productId, $nextWeek);
        $view = $this->view($result);
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * Post a booking date
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Post a booking date",
     *     description="CPost a booking date",
     *     views={"booking"},
     *     section="booking"
     * )
     *
     * @FosRest\Route("", options={"expose"=true})
     *
     * @param Request $request
     *
     * @return object|Agency
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     * @throws \Todotoday\CheckoutBundle\Exception\CheckoutWithInvalidPaymentMethodException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Todotoday\AccountBundle\Exceptions\NotAnAdherentException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingException
     * @throws \UnexpectedValueException
     * @throws \Todotoday\CheckoutBundle\Exception\OrderException
     * @throws \Todotoday\CheckoutBundle\Exception\NoProductToCheckoutException
     * @throws \Todotoday\CheckoutBundle\Exception\CheckoutWithoutPaymentException
     * @throws \Todotoday\CheckoutBundle\Exception\CheckoutWithoutInitializeException
     * @throws \Todotoday\CheckoutBundle\Exception\BookingSlotNotAvailableException
     * @throws \Todotoday\CheckoutBundle\Exception\BookingProductCrmIdServiceMissingException
     * @throws \Todotoday\CheckoutBundle\Exception\BookingInvalidProductBookedException
     * @throws \Todotoday\CheckoutBundle\Exception\BookingCrmIdAdherentMissingException
     * @throws \Todotoday\CheckoutBundle\Exception\BookingAgencyCrmSiteMissingException
     * @throws \Todotoday\CatalogBundle\Exceptions\ProductNotFoundException
     * @throws \Todotoday\CartBundle\Exception\NotAdherentCartBundleException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \LogicException
     * @throws BookingMissingSlotException
     * @throws BookingProductMissingException
     */
    public function postBookingAction(Request $request)
    {
        if (!$this->isGranted('CREATE', $this)) {
            throw $this->createAccessDeniedException();
        }

        if (!($adherent = $this->getUser()) instanceof Adherent) {
            throw new NotAnAdherentException('Only adherent can book a service');
        }

        if (!$productId = $request->get('product')) {
            throw new BookingProductMissingException();
        }

        if (!$slot = $request->get('slot')) {
            throw new BookingMissingSlotException();
        }

        $agency = $this->getAgency();
        $bookingManager = $this->get('todotoday.checkout.booking.manager');

        $data = array(
            'comment' => $request->get('comment'),
        );
        try {
            $result = $bookingManager->checkoutBooking($agency, $adherent, $productId, $slot, $data);
        } catch (BookingLockedException $e) {
            throw new HttpException('400', $e->getMessage());
        }

        $view = $this->view($result);
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * Post a booking request
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Post a booking request",
     *     description="Post a booking request",
     *     views={"booking"},
     *     section="booking"
     * )
     *
     * @FosRest\Route("/slot/request", options={"expose"=true})
     *
     * @param Request $request
     *
     * @return object|Agency
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \LogicException
     * @throws \Todotoday\AccountBundle\Exceptions\NotAnAdherentException
     * @throws \UnexpectedValueException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingException
     * @throws \InvalidArgumentException
     * @throws BookingCrmIdAdherentMissingException
     * @throws \Exception
     */
    public function postBookingRequestAction(Request $request)
    {
        if (!$this->isGranted('CREATE', $this)) {
            throw $this->createAccessDeniedException();
        }

        /** @var Adherent $adherent */
        if (!($adherent = $this->getUser()) instanceof Adherent) {
            throw new NotAnAdherentException('Only adherent can send a service\'s request');
        }

        $serviceId = $request->get('serviceId');
        if (!$serviceId) {
            throw new BookingCrmIdAdherentMissingException();
        }

        if (!$startDate = new Carbon($request->get('startDate'))) {
            throw new Exception('Start date not valid');
        }

        if (!$endDate = new Carbon($request->get('endDate'))) {
            throw new Exception('End date not valid');
        }

        $orderId = $request->get('orderId');

        $agency = $this->getAgency();
        $comment = $request->get('comment');

        $interactionManager = $this->get('todotoday.cms.service.interaction_manager');
        $interaction = $interactionManager->createRDVInteraction(
            $adherent,
            $agency,
            $comment,
            $startDate,
            $endDate,
            $orderId
        );

        $view = $this->view($interaction);

        //        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * Cancel a booking
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Cancel a booking",
     *     description="Cancel a booking",
     *     views={"booking"},
     *     section="booking"
     * )
     *order
     * @FosRest\Route("/{commandeid}/cancel",
     *     options={"expose"=true})
     *
     * @param string $commandeid
     *
     * @return Response
     * @throws \Todotoday\CheckoutBundle\Exception\OrderNotCancellableException
     * @throws \Todotoday\CheckoutBundle\Exception\BookingCantBeCancelledException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     * @throws \LogicException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function putCancelBookingAction(string $commandeid): Response
    {
        if (!$this->isGranted('EDIT', $this)) {
            throw $this->createAccessDeniedException();
        }

        $manager = $this->get('todotoday.order.manager');
        $result = $manager->cancelOrder($this->getUser(), $commandeid);
        $view = $this->view($result);

        return $this->handleView($view);
    }

    //POST /booking/slot/request

    /**
     * @return Agency
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws AgencyMissingException
     * @throws \UnexpectedValueException
     * @throws \InvalidArgumentException
     */
    private function getAgency(): Agency
    {
        if (!$agency = $this->get('todotoday.core.domain_context.api')->getAgency()) {
            throw new AgencyMissingException('Agency is missing');
        }

        return $agency;
    }
}
