<?php

declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 17/04/17
 * Time: 14:02
 */

namespace Todotoday\CheckoutBundle\Controller\Api;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as FosRest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Acl\Model\DomainObjectInterface;
use Todotoday\CheckoutBundle\Entity\Checkout;
use Todotoday\CheckoutBundle\Exception\NoProductToCheckoutException;
use Todotoday\CheckoutBundle\Form\Api\CheckoutApiType;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Exceptions\AgencyMissingException;

/**
 * @FosRest\Prefix("/checkout")
 * @FosRest\NamePrefix("todotoday.checkout.api.checkout.")
 * Class CheckoutApiController
 *
 * @package Todotoday\CheckoutBundle\Controller\Api
 */
class CheckoutApiController extends FOSRestController implements DomainObjectInterface
{
    /**
     * Returns a unique identifier for this domain object.
     *
     * @return string
     */
    public function getObjectIdentifier(): string
    {
        return 'class';
    }

    /**
     * Checkout all item in cart's adherent
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Checkout all item in cart's adherent",
     *     description="Checkout all item in cart's adherent",
     *     output={
     *           "class"="Todotoday\CheckoutBundle\Entity\Api\Microsoft\SalesOrderHeader",
     *           "groups"={"Default"}
     *     },
     *     input={
     *          "class" = "Todotoday\CheckoutBundle\Form\Api\CheckoutApiType"},
     *     views={"checkout"},
     *     section="checkout"
     * )
     *
     * @FosRest\Route("")
     *
     * @param Request $request
     *
     * @return object|\Todotoday\CoreBundle\Entity\Agency
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingException
     * @throws \Todotoday\CheckoutBundle\Exception\OrderException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws NoProductToCheckoutException
     * @throws \Todotoday\CheckoutBundle\Exception\CheckoutWithoutInitializeException
     * @throws \Todotoday\CheckoutBundle\Exception\CheckoutWithoutPaymentException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Todotoday\CatalogBundle\Exceptions\ProductNotFoundException
     * @throws \Todotoday\CartBundle\Exception\NotAdherentCartBundleException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \UnexpectedValueException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function postCheckoutAction(Request $request)
    {
        if (!$this->isGranted('CREATE', $this)) {
            throw $this->createAccessDeniedException();
        }

        $agency = $this->getAgency();
        $checkoutManager = $this->get('todotoday.checkout.manager');
        $data = $form = $this->createForm(CheckoutApiType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Checkout $checkout */
            $checkout = $form->getData();
            $stripeToken = $request->get('token');

            if ($comments = $request->get('comments_product')) {
                $checkout =
                    $checkoutManager->associateProductCommentFromApi(
                        $agency,
                        $checkout,
                        $comments
                    );
            }

            if ($deliveries = $request->get('deliveries_product')) {
                $checkout = $checkoutManager->associateProductDeliveryFromApi(
                    $agency,
                    $checkout,
                    $deliveries
                );
            }

            $checkout = $checkoutManager->initCheckoutProcess($this->getUser(), $agency, $checkout, 'Front');
            $checkout = $checkoutManager->setCustomerStripeAccount($checkout, $stripeToken);
            $data = $checkoutManager->createFullOrder($checkout);
        }

        $view = $this->view($data);
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * @return Agency
     * @throws \UnexpectedValueException
     * @throws AgencyMissingException
     */
    private function getAgency(): Agency
    {
        if (!$agency = $this->get('todotoday.core.domain_context.api')->getAgency()) {
            throw new AgencyMissingException('Agency is missing');
        }

        return $agency;
    }
}
