<?php declare(strict_types=1);

namespace Todotoday\CheckoutBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Entity\Concierge;

/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 09/04/17
 * Time: 16:38
 *
 * @Route("/booking")
 *
 */
class BookingController extends Controller
{
    /**
     * @Route("/{productId}", options={"expose"=true}, name="booking_calendar")
     *
     * @param string  $productId
     * @param Request $request
     *
     * @return Response
     * @throws \LogicException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \UnexpectedValueException
     * @throws \Todotoday\CatalogBundle\Exceptions\ProductNotFoundException
     * @throws \InvalidArgumentException
     */
    public function indexAction(string $productId, Request $request): Response
    {
        if (!$agency = $this->get('todotoday.core.domain_context.agency')->getAgency()) {
            throw $this->createAccessDeniedException();
        }

        /** @var Adherent $adherent */
        $adherent = $this->getUser();

        $bookingManager = $this->get('todotoday.checkout.booking.manager');
        $catalog = $this->get('todotoday_catalog.catalog_manager');

        $product = $catalog->getProduct($agency, $productId, $request->getLocale());

        if (!$product->isBookable() || null === $product->getCrmIdService()) {
            throw $this->createNotFoundException();
        }

        $rulesInJSON = json_encode($bookingManager->getRules());

        return $this->render(
            'booking/booking.html.twig',
            array(
                'currency' => $agency->getCurrency(),
                'product' => $product,
                'defaultPaymentMethod' => $adherent->getPaymentTypeSelectedString(),
                'paymentMethodStatus' => $adherent->getPaymentMethodStatusString(),
                'rules' => $rulesInJSON,
                'activation' => $adherent instanceof Adherent && null !== $adherent->getCrmIdAdherent(),
            )
        );
    }
}
