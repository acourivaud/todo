<?php declare(strict_types=1);

namespace Todotoday\CheckoutBundle\Controller;

use Carbon\Carbon;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Todotoday\CatalogBundle\Entity\Api\Microsoft\RetailCatalog;
use Todotoday\CheckoutBundle\Entity\Api\Microsoft\SalesOrderHeader;
use Todotoday\CheckoutBundle\Entity\Checkout;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 09/04/17
 * Time: 16:38
 *
 * @Route("/checkout")
 *
 */
class CheckoutController extends Controller
{
    /**
     * @Route("/", name="order_checkout")
     *
     * @return Response
     * @throws \Todotoday\CheckoutBundle\Exception\CheckoutWithoutPaymentException
     * @throws \Todotoday\CatalogBundle\Exceptions\ProductNotFoundException
     * @throws \Todotoday\CheckoutBundle\Exception\CheckoutWithoutInitializeException
     * @throws \Todotoday\CheckoutBundle\Exception\OrderException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \InvalidArgumentException
     * @throws \Todotoday\CheckoutBundle\Exception\NoProductToCheckoutException
     * @throws \Todotoday\CartBundle\Exception\NotAdherentCartBundleException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \UnexpectedValueException
     * @throws \LogicException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Todotoday\CheckoutBundle\Exception\CheckoutWithInvalidPaymentMethodException
     * @throws \Todotoday\CheckoutBundle\Exception\CheckoutWithoutMultipleSuppliersException
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function checkOutAction(): Response
    {
        $adherent = $this->getUser();
        $agency = $this->get('todotoday.core.domain_context.agency')->getAgency();
        $checkoutManager = $this->get('todotoday.checkout.manager');
        $cartProductManager = $this->get('todotoday_cart_product.manager');

        $checkout = $checkoutManager->initCheckoutProcess($adherent, $agency);

        if ($agency->isDigital()) {
            $suppliers = $cartProductManager->getAllSuppliers($agency);
            $checkout->setSuppliers($suppliers);
        }

        if (!$checkout->getDelivery() && in_array('Ccgie', $agency->getDeliveryMode(), true)) {
            $checkout->setDelivery('Ccgie');
            foreach ($checkout->getCartProducts() as $product) {
                if (!$product->getDelivery()) {
                    $product->setDelivery('Ccgie');
                }
            }
        }
        $flow = $this->get('todotoday.form.flow.checkout');
        $flow->bind($checkout);
        $form = $flow->createForm();
        if ($flow->isValid($form)) {
            $flow->saveCurrentStepData($form);

            if ($flow->nextStep()) {
                // form for the next step
                $form = $flow->createForm();
            } else {
                // flow finished
                if ($agency->isDigital() && $checkout->hasMultipleSuppliers()) {
                    $ordersCheckouts = $checkoutManager->createFullMultipleOrders($checkout);
                    $flow->reset();

                    return $this->forward(
                        'TodotodayCheckoutBundle:Checkout:checkoutSuccessMultipleOrder',
                        array(
                            'ordersCheckouts' => $ordersCheckouts,
                        )
                    );
                } else {
                    $salesOrderHeader = $checkoutManager->createFullOrder($checkout);
                    $flow->reset();

                    return $this->forward(
                        'TodotodayCheckoutBundle:Checkout:checkoutSuccess',
                        array(
                            'checkout' => $checkout,
                            'salesOrderHeader' => $salesOrderHeader,
                        )
                    );
                }
            }
        }

        return $this->render(
            'checkout/checkout.html.twig',
            array(
                'form' => $form->createView(),
                'flow' => $flow,
                'displayPriceExclTaxes' => $agency->getDisplayPriceExclTaxes(),
            )
        );
    }

    /**
     * @Route("/booking_success", methods={"POST"}, options={"expose"=true}, name="order_checkout_booking_success")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function checkoutBookingSuccessAction(Request $request): Response
    {
        $serviceDate = new Carbon($request->get('serviceDate'));

        return $this->render(
            'checkout/checkout_booking_success.html.twig',
            array(
                'order' => $request->get('order'),
                'serviceId' => $request->get('serviceId'),
                'serviceName' => $request->get('serviceName'),
                'serviceAmount' => $request->get('serviceAmount'),
                'serviceDate' => $serviceDate,
                'serviceDuration' => $request->get('serviceDuration'),
            )
        );
    }

    /**
     * @Route("/animation_success", methods={"POST"}, options={"expose"=true}, name="order_checkout_animation_success")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function checkoutAnimationSuccessAction(Request $request): Response
    {
        /** @var Agency $agency */
        $agency = $this->get('todotoday.core.domain_context.agency')->getAgency();
        $animationStartDate = new Carbon($request->get('animationStartDate'));
        $animationEndDate = new Carbon($request->get('animationEndDate'));

        return $this->render(
            'checkout/checkout_animation_success.html.twig',
            array(
                'currency' => $agency->getCurrency(),
                'animationName' => $request->get('animationName'),
                'animationStartDate' => $animationStartDate,
                'animationEndDate' => $animationEndDate,
                'animationPrice' => $request->get('animationPrice'),
            )
        );
    }

    /**
     * @Route("/quotation_success", methods={"POST"}, options={"expose"=true}, name="order_checkout_quotation_success")
     *
     * @param RetailCatalog    $product
     * @param SalesOrderHeader $salesOrderHeader
     *
     * @return Response
     */
    public function checkoutQuotationSuccessAction(RetailCatalog $product, SalesOrderHeader $salesOrderHeader): Response
    {
        $productFamily = $this->get('todotoday_catalog.catalog_manager')
            ->getProductFamilyName($product);

        return $this->render(
            'checkout/checkout_quotation_success.html.twig',
            array(
                'product' => $product,
                'productFamily' => $productFamily,
                'salesOrderHeader' => $salesOrderHeader,
            )
        );
    }

    /**
     * @Route("/success", name="order_checkout_success")
     *
     * @param Checkout         $checkout
     * @param SalesOrderHeader $salesOrderHeader
     *
     * @return Response
     */
    public function checkoutSuccessAction(Checkout $checkout, SalesOrderHeader $salesOrderHeader): Response
    {
        return $this->render(
            'checkout/checkout_success.html.twig',
            array(
                'products' => $checkout->getCartProducts(),
                'total' => $checkout->getTotalAmount(),
                'delivery' => $checkout->getDelivery(),
                'agency' => $checkout->getAgency(),
                'order' => $salesOrderHeader->getSalesOrderNumber(),
            )
        );
    }

    /**
     * @Route("/success_multiple", name="order_multiple_checkout_success")
     *
     * @param null|array $ordersCheckouts
     *
     * @return Response
     */
    public function checkoutSuccessMultipleOrderAction(?array $ordersCheckouts): Response
    {
        return $this->render(
            'checkout/checkout_success_multiple_orders.html.twig',
            array(
                'orders_checkouts' => $ordersCheckouts,
            )
        );
    }
}
