<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 12/04/17
 * Time: 22:26
 */

namespace Todotoday\CheckoutBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CheckoutBundle\Enum\OrderSatisfactionSubjectEnum;

/**
 * @Route("/orders/")
 * Class OrderController
 *
 * @package Todotoday\CheckoutBundle\Controller
 */
class OrderController extends Controller
{
    /**
     * @Route("pending", name="order.pending", options={"expose"=true})
     * @return Response
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \LogicException
     */
    public function indexAction(): Response
    {
        /** @var Adherent $adherent */
        $adherent = $this->getUser();
        $manager = $this->get('todotoday.order.manager');

        $salesOrder = $manager->getFullOrder($adherent, 'pending');

        return $this->render(
            'order/order.html.twig',
            array(
                'orders' => $salesOrder,
                'satisfactionOrderSubjects' => OrderSatisfactionSubjectEnum::getChoicesForFront(),
                'adherent' => $adherent,
            )
        );
    }

    /**
     * @Route("ratable", name="order.ratable")
     * @return Response
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \LogicException
     */
    public function orderRatableAction(): Response
    {
        /** @var Adherent $adherent */
        $adherent = $this->getUser();
        $orderMananger = $this->get('todotoday.order.manager');

        $ratableOrders = $orderMananger->getRatableOrders($adherent);

        return $this->render(
            ':order:order_ratable.html.twig',
            array(
                'orders' => $ratableOrders,
                'satisfactionOrderSubjects' => OrderSatisfactionSubjectEnum::getChoicesForFront(),
                'adherent' => $adherent,
            )
        );
    }

    /**
     * @Route("refused", name="order.refused")
     * @return Response
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \LogicException
     */
    public function orderRefusedAction(): Response
    {
        /** @var Adherent $adherent */
        $adherent = $this->getUser();
        $manager = $this->get('todotoday.order.supplier_manager');

        $salesOrder = $manager->getSupplierOrder($adherent);

        return $this->render(
            ':order:order_refused.html.twig',
            array(
                'orders' => $salesOrder,
                'adherent' => $adherent,
            )
        );
    }

    /**
     * @Route("history", name="order.history")
     * @return Response
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \LogicException
     */
    public function orderHistoryAction(): Response
    {
        return $this->render(
            'order/order_history.html.twig',
            array(
                'satisfactionOrderSubjects' => OrderSatisfactionSubjectEnum::getChoicesForFront(),
            )
        );
    }

    /**
     * @Route("booked/", name="order.booked")
     * @return Response
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \LogicException
     */
    public function bookedAction(): Response
    {
        /** @var Adherent $adherent */
        $adherent = $this->getUser();
        $repo = $this->get('todotoday.checkout.repository.api.service_appointment');

        $bookedServices = $repo->getAppointmentFromAdherent($adherent);

        return $this->render(
            'order/order_booked.html.twig',
            array(
                'services' => $bookedServices,
                'adherent' => $adherent,
            )
        );
    }

    /**
     * @Route("booked/history", name="order.booked_history")
     * @return Response
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \LogicException
     */
    public function bookedHistoryAction(): Response
    {
        /** @var Adherent $adherent */
        $adherent = $this->getUser();
        $repo = $this->get('todotoday.checkout.repository.api.service_appointment');

        $bookedServices = $repo->getAppointmentFromAdherent($adherent, true);

        return $this->render(
            'order/order_booked.html.twig',
            array(
                'services' => $bookedServices,
                'adherent' => $adherent,
            )
        );
    }

    /**
     * @Route("quotation", name="order.quotation", options={"expose"=true})
     * @return Response
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \LogicException
     */
    public function quotationAction(): Response
    {
        /** @var Adherent $adherent */
        $adherent = $this->getUser();
        $manager = $this->get('todotoday.order.manager');

        $salesOrder = $manager->getQuotationOrders($adherent);

        return $this->render(
            'order/order.html.twig',
            array(
                'orders' => $salesOrder,
                'satisfactionOrderSubjects' => OrderSatisfactionSubjectEnum::getChoicesForFront(),
                'adherent' => $adherent,
            )
        );
    }
}
