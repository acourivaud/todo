<?php declare(strict_types = 1);

namespace Todotoday\CheckoutBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('todotoday_checkout');

        $rootNode->append($this->getReservationsRules());

        return $treeBuilder;
    }

    /**
     * @return \Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition|\Symfony\Component\Config\Definition\Builder\NodeDefinition
     */
    protected function getReservationsRules()
    {
        $treeBuilder = new TreeBuilder();
        $node = $treeBuilder->root('reservation_rules');

        /** @var ArrayNodeDefinition|NodeDefinition $reservationRuleNode */
        $reservationRuleNode = $node
            ->requiresAtLeastOneElement()
            ->useAttributeAsKey('name')
            ->prototype('array');

        $reservationRuleNode
            ->children()
            ->scalarNode('numberDaysBeforeReservation')->end()
            ->scalarNode('minHour')->end()
            ->scalarNode('noReservationUntil')->end()
            ->scalarNode('deltaHours')->end()
            ->end();

        return $node;
    }
}
