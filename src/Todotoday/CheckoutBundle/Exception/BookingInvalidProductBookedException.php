<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 07/05/17
 * Time: 17:18
 */

namespace Todotoday\CheckoutBundle\Exception;

/**
 * Class BookingInvalidProductBookedException
 * @package Todotoday\CheckoutBundle\Exception
 */
class BookingInvalidProductBookedException extends \Exception
{
}
