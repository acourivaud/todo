<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 07/06/17
 * Time: 14:09
 */

namespace Todotoday\CheckoutBundle\Exception;

use Throwable;

/**
 * Class BookingCantBeCancelledException
 * @package Todotoday\CheckoutBundle\Exception
 */
class BookingCantBeCancelledException extends \Exception
{
    /**
     * BookingCantBeCancelledException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = 'Booking can\'t be cancelled', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
