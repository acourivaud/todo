<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 07/05/17
 * Time: 15:07
 */

namespace Todotoday\CheckoutBundle\Exception;

/**
 * Class CheckoutWithoutPaymentException
 * @package Todotoday\CheckoutBundle\Exception
 */
class CheckoutWithoutPaymentException extends \Exception
{
    /**
     * CheckoutWithoutPaymentException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = 'No payment method selected', $code = 500, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
