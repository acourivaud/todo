<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 07/06/17
 * Time: 14:12
 */

namespace Todotoday\CheckoutBundle\Exception;

use Throwable;

/**
 * Class BookingNotFoundException
 *
 * @package Todotoday\CheckoutBundle\Exception
 */
class BookingNotFoundException extends \Exception
{
    /**
     * BookingNotFoundException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = '', $code = 0, Throwable $previous = null)
    {
        $message = 'Booking not found [' . $message . ']';
        parent::__construct($message, $code, $previous);
    }
}
