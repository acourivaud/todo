<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 07/08/17
 * Time: 14:51
 */

namespace Todotoday\CheckoutBundle\Exception;

use Throwable;

/**
 * Class BookingLockedException
 *
 * @package Todotoday\CheckoutBundle\Exception
 */
class BookingLockedException extends \Exception
{
    /**
     * BookingLockedException constructor.
     *
     * @param int            $product
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($product, $code = 0, Throwable $previous = null)
    {
        $message = 'You can\'t book same resource [' . $product . ']' . ' that soon. Please wait (antispam control)';
        parent::__construct($message, $code, $previous);
    }
}
