<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 07/05/17
 * Time: 17:15
 */

namespace Todotoday\CheckoutBundle\Exception;

use Throwable;

/**
 * Class BookingCrmIdAdherentMissingException
 * @package Todotoday\CheckoutBundle\Exception
 */
class BookingCrmIdAdherentMissingException extends \Exception
{
    /**
     * BookingCrmIdAdherentMissingException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = 'crm_id_adherent is missing', $code = 500, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
