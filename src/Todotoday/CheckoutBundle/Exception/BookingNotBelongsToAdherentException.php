<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 07/06/17
 * Time: 14:07
 */

namespace Todotoday\CheckoutBundle\Exception;

use Throwable;

/**
 * Class BookingNotBelongsToAdherentException
 *
 * @package Todotoday\CheckoutBundle\Exception
 */
class BookingNotBelongsToAdherentException extends \Exception
{
    /**
     * BookingNotBelongsToAdherentException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct(
        $message = '',
        $code = 0,
        Throwable $previous = null
    ) {
        $message = 'This appointment dont belongs to current adherent [' . $message . ']';
        parent::__construct($message, $code, $previous);
    }
}
