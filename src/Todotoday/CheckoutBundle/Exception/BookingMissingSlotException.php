<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 07/05/17
 * Time: 17:25
 */

namespace Todotoday\CheckoutBundle\Exception;

use Throwable;

/**
 * Class BookingMissingSlotException
 * @package Todotoday\CheckoutBundle\Exception
 */
class BookingMissingSlotException extends \Exception
{
    /**
     * BookingMissingSlotException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = 'Slot is missing', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
