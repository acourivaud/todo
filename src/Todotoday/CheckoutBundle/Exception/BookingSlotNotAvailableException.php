<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 30/04/17
 * Time: 11:21
 */

namespace Todotoday\CheckoutBundle\Exception;

use Throwable;

/**
 * Class BookingSlotNotAvailableException
 * @package Todotoday\CheckoutBundle\Exception
 */
class BookingSlotNotAvailableException extends \Exception
{
    /**
     * BookingSlotNotAvailableException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct(
        $message = 'This slot is not available for booking',
        $code = 500,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
