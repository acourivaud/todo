<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 07/05/17
 * Time: 17:21
 */

namespace Todotoday\CheckoutBundle\Exception;

use Throwable;

/**
 * Class BookingAgencyCrmSiteMissingException
 *
 * @package Todotoday\CheckoutBundle\Exception
 */
class BookingAgencyCrmSiteMissingException extends \Exception
{
    /**
     * BookingAgencyCrmSiteMissingException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = '', $code = 0, Throwable $previous = null)
    {
        $message = 'Agency CRM site is missing [' . $message . ']';
        parent::__construct($message, $code, $previous);
    }
}
