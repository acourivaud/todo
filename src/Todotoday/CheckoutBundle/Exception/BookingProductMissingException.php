<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 07/05/17
 * Time: 17:24
 */

namespace Todotoday\CheckoutBundle\Exception;

use Throwable;

/**
 * Class BookingProductMissingException
 * @package Todotoday\CheckoutBundle\Exception
 */
class BookingProductMissingException extends \Exception
{
    /**
     * BookingProductMissingException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = 'Product is missing', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
