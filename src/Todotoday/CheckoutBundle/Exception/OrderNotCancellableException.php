<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 22/05/17
 * Time: 22:25
 */

namespace Todotoday\CheckoutBundle\Exception;

use Throwable;

/**
 * Class OrderNotCancellableException
 * @package Todotoday\CheckoutBundle\Exception
 */
class OrderNotCancellableException extends \Exception
{
    /**
     * OrderNotCancellableException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = 'Order is not cancellable', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
