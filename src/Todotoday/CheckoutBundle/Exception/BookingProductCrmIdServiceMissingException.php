<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 07/05/17
 * Time: 17:17
 */

namespace Todotoday\CheckoutBundle\Exception;

use Throwable;

/**
 * Class BookingProductCrmIdServiceMissingException
 *
 * @package Todotoday\CheckoutBundle\Exception
 */
class BookingProductCrmIdServiceMissingException extends \Exception
{
    /**
     * BookingProductCrmIdServiceMissingException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct(
        $message = '',
        $code = 500,
        Throwable $previous
        = null
    ) {
        $message = 'Missing crm id service to book this item [' . $message . ']';
        parent::__construct($message, $code, $previous);
    }
}
