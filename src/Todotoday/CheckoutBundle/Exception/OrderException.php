<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 07/05/17
 * Time: 17:34
 */

namespace Todotoday\CheckoutBundle\Exception;

use Throwable;

/**
 * Class OrderException
 * @package Todotoday\CheckoutBundle\Exception
 */
class OrderException extends \Exception
{
    /**
     * OrderException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = 'Exception during order process', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
