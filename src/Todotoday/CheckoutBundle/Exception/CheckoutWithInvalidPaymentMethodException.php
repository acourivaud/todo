<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 17/06/17
 * Time: 17:31
 */

namespace Todotoday\CheckoutBundle\Exception;

use Throwable;

/**
 * Class CheckoutWithInvalidPaymentMethodException
 * @package Todotoday\CheckoutBundle\Exception
 */
class CheckoutWithInvalidPaymentMethodException extends \Exception
{
    /**
     * CheckoutWithInvalidPaymentMethodException constructor.
     *
     * @param string         $status
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct(
        string $status,
        $message = 'Payment method is not valid',
        $code = 0,
        Throwable $previous = null
    ) {
        $message .= '. Current status is ' . $status;
        parent::__construct($message, $code, $previous);
    }
}
