<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 17/04/17
 * Time: 16:28
 */

namespace Todotoday\CheckoutBundle\Exception;

use Throwable;

/**
 * Class CheckoutWithoutInitializeException
 * @package Todotoday\CheckoutBundle\Exception
 */
class CheckoutWithoutInitializeException extends \Exception
{
    /**
     * CheckoutWithoutInitializeException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = 'Product hasn\'t been initialized', $code = 500, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
