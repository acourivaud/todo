<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 17/04/17
 * Time: 16:28
 */

namespace Todotoday\CheckoutBundle\Exception;

use Throwable;

/**
 * Class CheckoutWithoutMultipleSuppliersException
 * @package Todotoday\CheckoutBundle\Exception
 */
class CheckoutWithoutMultipleSuppliersException extends \Exception
{
    /**
     * CheckoutWithoutMultipleSuppliersException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = 'The checkout has not multiple suppliers', $code = 500, Throwable
    $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
