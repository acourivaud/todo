<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 07/05/17
 * Time: 16:24
 */

namespace Todotoday\CheckoutBundle\Exception;

use Throwable;

/**
 * Class NoProductToCheckoutException
 * @package Todotoday\CheckoutBundle\Exception
 */
class NoProductToCheckoutException extends \Exception
{
    /**
     * NoProductToCheckoutException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = 'There is no product to checkout', $code = 500, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
