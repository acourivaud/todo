<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 02/08/17
 * Time: 19:07
 */

namespace Todotoday\CheckoutBundle\Exception;

use Throwable;

/**
 * Class OrderNotFoundException
 * @package Todotoday\CheckoutBundle\Exception
 */
class OrderNotFoundException extends \Exception
{
    /**
     * OrderNotFoundException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = '', $code = 0, Throwable $previous = null)
    {
        $message = 'Order [' . $message . '] not found';
        parent::__construct($message, $code, $previous);
    }
}
