<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 17/05/17
 * Time: 10:55
 */

namespace Todotoday\CheckoutBundle\Exception;

use Throwable;

/**
 * Class OrderNotBelongsToAdherentException
 * @package Todotoday\CheckoutBundle\Exception
 */
class OrderNotBelongsToAdherentException extends \Exception
{
    /**
     * OrderNotBelongsToAdherentException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct(
        $message = 'This order don\'t belongs to current adherent',
        $code = 0,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
