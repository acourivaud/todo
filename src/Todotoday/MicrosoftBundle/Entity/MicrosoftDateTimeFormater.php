<?php declare(strict_types=1);

namespace Todotoday\MicrosoftBundle\Entity;

/**
 * Class MicrosoftDateTimeFormater
 * @package Todotoday\MicrosoftBundle\Entity
 */
class MicrosoftDateTimeFormater
{
    /**
     * @var string
     */
    public static $defaultFormat = 'Y-m-d\TH:i:s\Z';
}
