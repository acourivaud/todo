<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 17/03/2017
 * Time: 18:49
 */

namespace Todotoday\CMSBundle\Controller\Api;

use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Request\ParamFetcher;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Todotoday\CMSBundle\Entity\Post;
use Todotoday\CMSBundle\Enum\PostStateEnum;
use Todotoday\CoreBundle\Controller\Api\AbstractApiController;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class PostController
 *
 * @category   Todo-Todev
 * @package    Todotoday\CMSBundle
 * @subpackage Todotoday\CMSBundle\Controller\Api
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 * @FOSRest\NamePrefix("todotoday.cms.api.post.")
 * @FOSRest\Prefix(value="/post")
 */
class PostController extends AbstractApiController
{
    /**
     * Get post from agency
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Get post",
     *     description="Get post",
     *     output={
     *           "class"="array<Todotoday\CMSBundle\Entity\Post>",
     *           "groups"={"Default"}
     *     },
     *     views={"Default","Post"},
     *     section="post"
     * )
     *
     * @FOSRest\QueryParam(
     *     name="limit",
     *     requirements="\d+",
     *     default=null,
     *     description="Limit"
     * )
     *
     * @FOSRest\QueryParam(
     *     name="offset",
     *     requirements="\d+",
     *     default=null,
     *     description="Offset"
     * )
     *
     * @param ParamFetcher $paramFetcher
     * @param Request      $request
     *
     * @return Response
     * @throws \LogicException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \UnexpectedValueException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \InvalidArgumentException
     */
    public function getAction(ParamFetcher $paramFetcher, Request $request): Response
    {
        $agency = $this->getAgency();
        $locale = $request->getLocale();
        $posts = $this->get('todotoday_cms.repository.post')->getAllPostByAgencyByLocale(
            $agency,
            $locale,
            $paramFetcher->get('limit') !== '' ? (int) $paramFetcher->get('limit') : null,
            $paramFetcher->get('offset') !== '' ? (int) $paramFetcher->get('offset') : null
        );
        $view = $this->view($posts);
        $view->getContext()->addGroup('Default');

        return $this->handleView($view);
    }

    /**
     * Get post waiting to be validate or refused
     *
     * @return Response
     * @throws \LogicException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getToValidateAction(): Response
    {
        /** @var Post[] $posts */
        $posts = $this->get('todotoday_cms.repository.post')->findPostToValidate();

        $view = $this->view($posts);
        $view->getContext()->addGroups(array('account', 'Default'));

        return $this->handleView($view);
    }

    /**
     * Do patchChangedState
     *
     * @param int    $id
     *
     * @param string $state
     *
     * @return Response
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \InvalidArgumentException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \LogicException
     */
    public function putChangedStateAction(int $id, string $state): Response
    {
        /** @var Post $post */
        $post = $this->get('todotoday_cms.repository.post')->findOneBy(array('id' => $id));

        $post->setState(PostStateEnum::getNewInstance($state));

        $this->getDoctrine()->getManager()->flush();

        $secHandler = $this->get('sonata.admin.security.handler');
        $admin = $this->get('todotoday_cms.admin.post');
        if ($state === PostStateEnum::DRAFT) {
            $secHandler->createObjectSecurity($admin, $post);
        } else {
            $secHandler->deleteObjectSecurity($admin, $post);
        }

        return $this->handleView($this->view($post));
    }

    /**
     * Get the formType for the current entity class link to the controller
     *
     * @return string
     */
    protected function getFormType(): string
    {
        return '';
    }

    /**
     * Get the serivce name from custum repository's entity
     *
     * @return string
     */
    protected function getServiceRepository(): string
    {
        return 'todotoday_cms.repository.post';
    }

    /**
     * @return Agency
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \UnexpectedValueException
     * @throws \InvalidArgumentException
     */
    private function getAgency(): ?Agency
    {
        if (!$agency = $this->get('todotoday.core.domain_context.api')->getAgency()) {
            throw new \InvalidArgumentException('Agency is missing');
        }

        return $agency;
    }
}
