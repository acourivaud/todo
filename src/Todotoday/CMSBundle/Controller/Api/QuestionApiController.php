<?php

declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 27/06/17
 * Time: 18:58
 */

namespace Todotoday\CMSBundle\Controller\Api;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as FosRest;
use Symfony\Component\HttpFoundation\Response;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Exceptions\AgencyMissingException;

/**
 * @FosRest\NamePrefix("todotoday.cms.api.question.")
 *
 * Class QuestionApiController
 *
 * @package Todotoday\CMSBundle\Controller\Api
 */
class QuestionApiController extends FOSRestController
{
    /**
     * @FosRest\Route("/faq")
     *
     * @return Response
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     */
    public function getAllAction(): Response
    {
        $repo = $this->getDoctrine()->getRepository('TodotodayCMSBundle:Question');
        $view = $this->view($repo->findByAgency($this->getAgency()));

        return $this->handleView($view);
    }

    /**
     * Do getAgency
     *
     * @return Agency
     * @throws \UnexpectedValueException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingException
     */
    private function getAgency(): Agency
    {
        if (!$agency = $this->get('todotoday.core.domain_context')->getCurrentContext()->getAgency()) {
            throw new AgencyMissingException('Agency is missing');
        }

        return $agency;
    }
}
