<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 17/03/2017
 * Time: 18:49
 */

namespace Todotoday\CMSBundle\Controller\Api;

use FOS\RestBundle\Controller\Annotations as FOSRest;
use Symfony\Component\HttpFoundation\Response;
use Todotoday\CMSBundle\Entity\LinkPostAgency;
use Todotoday\CMSBundle\Enum\LinkPostAgencyStateEnum;
use Todotoday\CoreBundle\Controller\Api\AbstractApiController;

/**
 * Class LinkPostAgencyController
 *
 * @category   Todo-Todev
 * @package    Todotoday\CMSBundle
 * @subpackage Todotoday\CMSBundle\Controller\Api
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 * @FOSRest\NamePrefix("todotoday.cms.api.link_post_agency.")
 * @FOSRest\Prefix(value="/link_post_agency")
 */
class LinkPostAgencyController extends AbstractApiController
{
    /**
     * Get Latest Posts published and unpublished
     *
     * @return Response
     * @throws \LogicException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function getLatestAction(): Response
    {
        /** @var LinkPostAgency[] $linkPostsAgencies */
        $linkPostsAgencies = $this->get('todotoday_cms.repository.link_post_agency')->findLatestPost();

        $view = $this->view($linkPostsAgencies);

        return $this->handleView($view);
    }

    /**
     * Do patchChangedState
     *
     * @param int    $id
     *
     * @param string $state
     *
     * @return Response
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \InvalidArgumentException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \LogicException
     */
    public function putChangedStateAction(int $id, string $state): Response
    {
        /** @var LinkPostAgency $linkPostAgency */
        $linkPostAgency = $this->get('todotoday_cms.repository.link_post_agency')->findOneBy(array('id' => $id));

        $linkPostAgency->setState(LinkPostAgencyStateEnum::getNewInstance($state));

        $this->getDoctrine()->getManager()->flush();

        return $this->handleView($this->view($linkPostAgency));
    }

    /**
     * Get the formType for the current entity class link to the controller
     *
     * @return string
     */
    protected function getFormType(): string
    {
        return '';
    }

    /**
     * Get the serivce name from custum repository's entity
     *
     * @return string
     */
    protected function getServiceRepository(): string
    {
        return 'todotoday_cms.repository.link_post_agency';
    }
}
