<?php

declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 12/05/17
 * Time: 16:00
 */

namespace Todotoday\CMSBundle\Controller\Api;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as FosRest;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Acl\Model\DomainObjectInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Todotoday\CoreBundle\Exceptions\AgencyMissingException;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * @FosRest\Prefix("/contact")
 * @FosRest\NamePrefix("todotoday.cms.api.contact.")
 * Class ContactApiController
 * @package Todotoday\CheckoutBundle\Controller\Api
 */
class ContactApiController extends FOSRestController implements DomainObjectInterface
{
    /**
     * Get available contact subject
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Get available contact subject",
     *     description="Get available contact subject",
     *     views={"contact"},
     *     section="contact"
     * )
     *
     * @FosRest\Route("")
     *
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingException
     * @throws \UnexpectedValueException
     *
     * @return Response
     */
    public function getAction(): Response
    {
        if (!$this->isGranted('VIEW', $this)) {
            throw $this->createAccessDeniedException();
        }
        $this->getAgency();
        $contacts = $this->getDoctrine()->getRepository('TodotodayCMSBundle:ContactForm')->findAll();
        $view = $this->view($contacts);
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * Returns a unique identifier for this domain object.
     *
     * @return string
     */
    public function getObjectIdentifier()
    {
        return 'class';
    }

    /**
     * @return Agency
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingException
     * @throws \UnexpectedValueException
     * @throws \InvalidArgumentException
     */
    private function getAgency(): Agency
    {
        if (!$agency = $this->get('todotoday.core.domain_context.api')->getAgency()) {
            throw new AgencyMissingException('Agency is missing');
        }

        return $agency;
    }
}
