<?php declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 16/01/2017
 * Time: 17:36
 */

namespace Todotoday\CMSBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Todotoday\ChatBundle\Entity\Conversation;
use Todotoday\CMSBundle\Entity\Cgv;
use Todotoday\CMSBundle\Entity\Post;
use Todotoday\CMSBundle\Types\ContactType;
use Todotoday\CMSBundle\Types\UploadDocumentType;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Services\DomainContext\Www;
use Todotoday\MediaBundle\Entity\File;
use Todotoday\MediaBundle\Repository\FileRepository;

/**
 * Class CgvController
 *
 * @package    Todotoday\CMSBundle
 * @subpackage Todotoday\CMSBundle\Controller
 */
class CgvController extends Controller
{
    /**
     * @param Request $request
     *
     * @return Response
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws NotFoundHttpException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     * @Route("/terms/", name="cms_todotoday_index_cgv")
     * @throws \Exception
     */
    public function indexAction(Request $request): ?Response
    {
        if (($context = $this->get('todotoday.core.domain_context')->getCurrentContext())
            && $agency = $context->getAgency()
        ) {
            try {
                if (!$cgv = $this->get('todotoday_cms.repository.cgv')->getCgvByAgency($agency)) {
                    throw $this->createNotFoundException();
                }

                return $this->render(
                    '@TodotodayCMS/CGV/index.html.twig',
                    array(
                        'cgv' => $cgv,
                        'cgvMedia' => $cgv->getMediaWithLocale($request->getLocale()),
                    )
                );
            } catch (NotFoundHttpException $exception) {
                throw $exception;
            }
        } elseif ($context instanceof Www) {
            return $this->render(':default:layout.html.twig');
        } else {
            throw $this->createNotFoundException();
        }
    }
}
