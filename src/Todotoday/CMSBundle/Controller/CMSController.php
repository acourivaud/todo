<?php declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 16/01/2017
 * Time: 17:36
 */

namespace Todotoday\CMSBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Todotoday\ChatBundle\Entity\Conversation;
use Todotoday\CMSBundle\Entity\Cgv;
use Todotoday\CMSBundle\Entity\Post;
use Todotoday\CMSBundle\Types\ContactType;
use Todotoday\CMSBundle\Types\UploadDocumentType;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Services\DomainContext\Www;
use Todotoday\MediaBundle\Entity\File;
use Todotoday\MediaBundle\Repository\FileRepository;

/**
 * Class CMSController
 *
 * @package    Todotoday\CMSBundle
 * @subpackage Todotoday\CMSBundle\Controller
 */
class CMSController extends Controller
{
    /**
     * @param Request $request
     *
     * @return Response
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws NotFoundHttpException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     * @Route("/", name="index_cms_concierge")
     * @throws \Exception
     */
    public function indexAction(Request $request): ?Response
    {
        $locale = $request->getLocale();
        if (($context = $this->get('todotoday.core.domain_context')->getCurrentContext())
            && $agency = $context->getAgency()
        ) {
            try {
                $paginator = $this->get('actiane.paginator.manager');
                $item = $paginator->generate(
                    Post::class,
                    'getActive',
                    array('limit' => 3, 'callback_options' => ['agency' => $agency, 'locale' => $locale])
                );

                $locale = $request->getLocale();
                $catalogManager = $this->get('todotoday_catalog.catalog_manager');
                $catalogType = $catalogManager->getTypeWithRole($this->isGranted('ROLE_NOT_ADHERENT', $this));
                $mainFamilies = $catalogManager->getMainFamiliesCatalog($agency, $locale, $catalogType);

                //"main+" is use in ACTCatalog.vue in method "goToFamilyWithUrl"
                $catalogRoute = $this->generateUrl(
                    'todotoday_catalog_index',
                    array('hierarchyFamily' => 'main+')
                );

                return $this->render(
                    ':CMS:cms.html.twig',
                    array(
                        'catalogRoute' => $catalogRoute,
                        'mainFamilies' => $mainFamilies,
                        'item' => $item,
                    )
                );
            } catch (NotFoundHttpException $exception) {
                throw $exception;
            }
        } elseif ($context instanceof Www) {
            return $this->render(':default:layout.html.twig');
        } else {
            throw $this->createNotFoundException();
        }
    }

    /**
     * @param Request $request
     *
     * @return Response
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws NotFoundHttpException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     * @Route("/export-iframe/index.html", name="index_cms_concierge_iframe")
     * @throws \Exception
     */
    public function iframeAction(Request $request): ?Response
    {
        $locale = $request->getLocale();
        if (($context = $this->get('todotoday.core.domain_context')->getCurrentContext())
            && $agency = $context->getAgency()
        ) {
            try {
                $paginator = $this->get('actiane.paginator.manager');
                $item = $paginator->generate(
                    Post::class,
                    'getActive',
                    array('limit' => 5, 'callback_options' => ['agency' => $agency, 'locale' => $locale])
                );

                $locale = $request->getLocale();
                $catalogManager = $this->get('todotoday_catalog.catalog_manager');
                $catalogType = $catalogManager->getTypeWithRole($this->isGranted('ROLE_NOT_ADHERENT', $this));
                $mainFamilies = $catalogManager->getMainFamiliesCatalog($agency, $locale, $catalogType);

                //"main+" is use in ACTCatalog.vue in method "goToFamilyWithUrl"
                $catalogRoute = $this->generateUrl(
                    'todotoday_catalog_index',
                    array('hierarchyFamily' => 'main+')
                );

                return $this->render(
                    ':CMS:cms_iframe.html.twig',
                    array(
                        'catalogRoute' => $catalogRoute,
                        'mainFamilies' => $mainFamilies,
                        'item' => $item,
                    )
                );
            } catch (NotFoundHttpException $exception) {
                throw $exception;
            }
        } elseif ($context instanceof Www) {
            return $this->render(':default:iframe.html.twig');
        } else {
            throw $this->createNotFoundException();
        }
    }

    /**
     * @param Request $request
     *
     * @return Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws NotFoundHttpException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     * @Route("/articles/", name="cms_all_news")
     */
    public function allNewsAction(Request $request): ?Response
    {
        $locale = $request->getLocale();
        $agency = $this->getAgency();
        try {
            $paginator = $this->get('actiane.paginator.manager');
            $item = $paginator->generate(
                Post::class,
                'getActive',
                array('limit' => 5, 'callback_options' => ['agency' => $agency, 'locale' => $locale])
            );

            return $this->render(
                ':CMS:allNews.html.twig',
                array(
                    'item' => $item,
                )
            );
        } catch (NotFoundHttpException $exception) {
            throw $exception;
        }
    }

    /**
     * @param string $slug
     *
     * @return Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws NotFoundHttpException
     *
     * @throws \UnexpectedValueException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\NoResultException
     *
     * @Route("/article/{slug}/", name="cms_concierge_show_post", options = { "expose" = true })
     */
    public function showPostAction(string $slug): Response
    {

        if (!$post = $this->get('todotoday_cms.repository.post')->getPost($this->getAgency(), $slug)) {
            throw $this->createNotFoundException();
        }

        return $this->render(
            ':CMS:showPost.html.twig',
            array(
                'post' => $post,
            )
        );
    }

    /**
     * @param string $slug
     *
     * @return Response
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\NoResultException
     * @throws \UnexpectedValueException
     * @throws \LogicException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     *
     * @Route("/article/preview/{slug}", name="cms_concierge_show_preview_post", options={"expose" = true})
     */
    public function showPreviewPostAction(string $slug): Response
    {
        if (!$post = $this->get('todotoday_cms.repository.post')->findOneBy(
            array(
                'slug' => $slug,
            )
        )) {
            throw $this->createNotFoundException();
        }

        return $this->render(
            ':CMS:showPost.html.twig',
            array(
                'post' => $post,
            )
        );
    }

    /**
     * @param Request $request
     *
     * @return Response
     * @throws \LogicException
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws NotFoundHttpException
     *
     * @throws \UnexpectedValueException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\NoResultException
     *
     * @Route("/faqs/", name="cms_concierge_page_faq")
     */
    public function showFaqAction(Request $request): Response
    {
        $locale = $request->getLocale();
        $em = $this->getDoctrine()->getManager();
        $questions = $em->getRepository('TodotodayCMSBundle:Question')->findByAgency($this->getAgency(), $locale);
//        if (!$questions = $em->getRepository('TodotodayCMSBundle:Question')->findByAgency($this->getAgency())) {
//            throw $this->createNotFoundException();
//        }

        return $this->render(
            ':CMS:showFAQ.html.twig',
            array(
                'questions' => $questions,
            )
        );
    }

//    A LA DEMANDE DE STEPHANIE CARDOT ON DESACTIVE CETTE PAGE LOL
//    A LA DEMANDE DE MORGANE ON REMET CETTE PAGE HAHA

    /**
     * @param Request $request
     *
     * @return Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \UnexpectedValueException
     * @throws \LogicException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @Route("/information/", name="cms_concierge_page_info")
     */
    public function showInfoAction(Request $request): Response
    {
        $agency = $this->getAgency();
        $repo = $this->getDoctrine()->getRepository('TodotodayCoreBundle:Agency');
        $agency = $repo->getAgencyTranslation($agency->getId(), $request->getLocale());
        $locale = $request->getLocale();

        try {
            $paginator = $this->get('actiane.paginator.manager');
            $item = $paginator->generate(
                Post::class,
                'getActive',
                array('limit' => 3, 'callback_options' => ['agency' => $agency, 'locale' => $locale])
            );

            return $this->render(
                ':CMS:showInfo.html.twig',
                array(
                    'item' => $item,
                    'agency' => $agency,
                )
            );
        } catch (NotFoundHttpException $exception) {
            throw $exception;
        }
    }

    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @throws \Twig_Error_Syntax
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Loader
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \InvalidArgumentException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \Todotoday\CMSBundle\Exception\InteractionCreateException
     * @throws \Symfony\Component\Form\Exception\UnexpectedTypeException
     * @throws \Symfony\Component\Form\Exception\LogicException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \LogicException
     * @throws \ErrorException
     * @Route("/contact/", name="cms_concierge_page_contact")
     */
    public function contactAction(Request $request)
    {
        $agency = $this->getAgency();
        if ($this->isGranted('IS_AUTHENTICATED_ANONYMOUSLY')) {
            $form = $this->createForm(
                ContactType::class,
                null,
                array(
                    'action' => $this->generateUrl('cms_concierge_page_contact'),
                    'method' => 'POST',
                )
            )
                ->add(
                    'submit',
                    SubmitType::class,
                    array(
                        'label' => 'form.button_send_message',
                        'translation_domain' => 'todotoday',
                    )
                );
        }
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $interactionManager = $this->get('todotoday.cms.service.interaction_manager');
            $interactionManager->handleSendInteraction($agency, $this->getUser(), $form->getData());
            $this->addFlash('submit_form_contact', 'contact.success_message');

            return $this->redirectToRoute('cms_concierge_page_contact');
        }

        return $this->render(
            ':CMS:contact.html.twig',
            array(
                'form' => $form->createView(),
            )
        );
    }

    /**
     *
     * @param Request $request
     *
     * @return Response
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     * @Route("/downloads/", name="cms_concierge_show_downloads", options={"expose" = true})
     */
    public function showDownloadsAction(Request $request): Response
    {
        /** @var \Todotoday\AccountBundle\Services\AdherentManager $adherentManager */
        $adherentManager = $this->get('todotoday.account.services.adherent_manager');

        $agency = $this->getAgency();
        $files = $this->get('todotoday_cms.repository.file')->findAllByLocale($request->getLocale());
        $sharedFiles = $adherentManager->getMedia();
        $cgv = $agency->getCgv();

        $form = null;

        if ($this->isGranted('ROLE_ADHERENT')) {
            $form = $this->createForm(
                UploadDocumentType::class,
                null,
                array(
                    'action' => $this->generateUrl('cms_concierge_show_downloads'),
                    'method' => 'POST',
                )
            )
                ->add(
                    'submit',
                    SubmitType::class,
                    array(
                        'label' => 'form.button_upload_document',
                        'translation_domain' => 'todotoday',
                    )
                );

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $adherentManager->uploadMedia($form->getData());

                $this->addFlash('submit_form_upload_file', 'form.document_success_message');

                return $this->redirectToRoute('cms_concierge_show_downloads');
            }
        }

        return $this->render(
            ':CMS:showDownloads.html.twig',
            array(
                'files' => $files,
                'shared_files' => $sharedFiles,
                'cgv' => $cgv,
                'cgvMedia' => $cgv ? $cgv->getMediaWithLocale($request->getLocale()) : null,
                'form' => $form ? $form->createView() : null,
            )
        );
    }

    /**
     *
     * @param Request $request
     * @param int     $id
     *
     * @return Response
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     * @Route("/delete-file/{id}/", name="cms_concierge_delete_file", options={"expose" = true})
     */
    public function deleteFileAction(Request $request, $id = null): Response
    {
        /** @var \Todotoday\AccountBundle\Services\AdherentManager $adherentManager */
        if (!$this->get('todotoday.account.services.adherent_manager')->removeMedia($id)) {
            throw $this->createNotFoundException();
        }

        return $this->redirectToRoute('cms_concierge_show_downloads');
    }

    /**
     * @Route("/app", name="cms_page_web_app")
     * @return Response
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function appAction(): Response
    {
        $this->getAgency();

        return $this->render(
            ':CMS:webApp.html.twig'
        );
    }

    /**
     * @Route("/cookies", name="cms_page_cnil")
     *
     * @return Response
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function cnilAction(): Response
    {
        $this->getAgency();

        return $this->render(
            ':CMS:cnil.html.twig'
        );
    }

    /**
     * @Route("/lang/{lang}", name="cms_set_lang")
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param string                                    $lang
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function setDefaultLangAction(Request $request, $lang)
    {
        $this->get('session')->set('_locale', $lang);

        return $this->redirectToRoute('index_cms_concierge');
    }

    /**
     * Do getAgency
     *
     * @return Agency
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \UnexpectedValueException
     * @throws NotFoundHttpException
     */
    protected function getAgency(): Agency
    {
        if (!$agency = $this->get('todotoday.core.domain_context')->getCurrentContext()->getAgency()) {
            throw $this->createNotFoundException();
        }

        return $agency;
    }
}
