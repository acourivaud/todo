<?php declare(strict_types=1);

namespace Todotoday\CMSBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Todotoday\CMSBundle\Entity\Post;
use Todotoday\CMSBundle\Enum\PostStateEnum;

/**
 * Class PostAdminController
 */
class PostAdminController extends CRUDController
{
//    /**
//     * @param Request $request
//     *
//     * @return Response
//     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
//     */
//    public function showQuestionAnswerAction(Request $request): Response
//    {
//        $id = $request->get($this->admin->getIdParameter());
//        /** @var Post $questionnaire */
//        $questionnaire = $this->admin->getObject($id);
//
//        if (!$questionnaire) {
//            $id = $request->get($this->admin->getIdParameter());
//            throw $this->createNotFoundException(sprintf('unable to find the object with id : %s', $id));
//        }
//
//        return $this->render(
//            'admin/questionnaire/show_question_answer.html.twig',
//            array(
//                'questionnaire' => $questionnaire,
//            )
//        );
//    }

    /**
     * Do switchStateAction
     *
     * @param int    $id
     * @param string $state
     *
     * @return RedirectResponse
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    public function switchStateAction(int $id, string $state): RedirectResponse
    {
        /** @var Post $post */
        $post = $this->get('todotoday_cms.repository.post')->findOneBy(array('id' => $id));

        $post->setState(PostStateEnum::getNewInstance($state));

        $this->getDoctrine()->getManager()->flush();

        if ($state === PostStateEnum::DRAFT) {
            $this->admin->createObjectSecurity($post);
        } else {
            $this->admin->getSecurityHandler()->deleteObjectSecurity($this->admin, $post);
        }

        return new RedirectResponse(
            $this->admin->generateUrl('list', array('filter' => $this->admin->getFilterParameters()))
        );
    }

    /**
     * {@inheritdoc}
     * @throws \LogicException
     */
    public function createAction(): Response
    {
        if (!$this->isGranted('ROLE_COMMUNITY_MANAGER') || $this->getRequest()->get('agencies_strategy')) {
            return parent::createAction();
        }

        return $this->render(
            '@TodotodayCMS/Admin/Post/select_option.html.twig',
            array(
                'action' => 'create',
                'options' => array(
                    'empty',
                    'all',
                    'us',
                    'fr',
                    'ch',
                ),
            )
        );
    }

    /**
     * @param int $id
     *
     * @return Response
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    public function previewAction(int $id): Response
    {
        /** @var Post $post */
        $post = $this->get('todotoday_cms.repository.post')->findOneBy(array('id' => $id));

        return new RedirectResponse(
            $this->generateUrl(
                'cms_concierge_show_preview_post',
                array(
                    'slug' => $post->getSlug(),
                )
            )
        );
    }

    /**
     * @param string $value
     *
     * @return Response
     * @throws \InvalidArgumentException
     */
    public function originAction(string $value): Response
    {
        return new RedirectResponse(
            $this->generateUrl('admin_todotoday_cms_post_list', ['filter[origin][value]' => $value])
        );
    }
}
