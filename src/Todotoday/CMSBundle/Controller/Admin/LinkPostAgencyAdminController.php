<?php declare(strict_types=1);

namespace Todotoday\CMSBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Todotoday\CMSBundle\Entity\LinkPostAgency;
use Todotoday\CMSBundle\Enum\LinkPostAgencyStateEnum;

/**
 * Class LinkPostAgencyAdminController
 *
 * @package    Todotoday\CMSBundle
 * @subpackage Todotoday\CMSBundle\Controller\Admin
 */
class LinkPostAgencyAdminController extends CRUDController
{
    /**
     * Do switchStateAction
     *
     * @param int    $id
     * @param string $state
     *
     * @return RedirectResponse
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    public function switchStateAction(int $id, string $state)
    {
        /** @var LinkPostAgency $linkPostAgency */
        $linkPostAgency = $this->get('todotoday_cms.repository.link_post_agency')->findOneBy(array('id' => $id));

        $linkPostAgency->setState(LinkPostAgencyStateEnum::getNewInstance($state));

        $this->getDoctrine()->getManager()->flush();

        return new RedirectResponse(
            $this->admin->generateUrl('list', array('filter' => $this->admin->getFilterParameters()))
        );
    }
}
