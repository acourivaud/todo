<?php declare(strict_types = 1);

namespace Todotoday\CMSBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class TodotodayCMSBundle
 *
 * @package    Todotoday\CMSBundle
 */
class TodotodayCMSBundle extends Bundle
{
}
