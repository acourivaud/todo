<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 12/05/17
 * Time: 13:38
 */

namespace Todotoday\CMSBundle\Enum;

use Actiane\ToolsBundle\Enum\AbstractEnum;

/**
 * Class ReceiverEnum
 * @package Todotoday\CMSBundle\Enum
 */
class ReceiverEnum extends AbstractEnum
{
    const CONCIERGE = 'team'; // ie concierge
    const CUSTOMER_SERVICE = 'service_client';
}
