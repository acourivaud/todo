<?php declare(strict_types = 1);

/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 15/03/2017
 * Time: 18:53
 */

namespace Todotoday\CMSBundle\Enum;

use Actiane\ToolsBundle\Enum\AbstractEnum;

/**
 * Class LinkPostAgencyStateEnum
 *
 * @category   Todo-Todev
 * @package    Todotoday\CMSBundle
 * @subpackage Todotoday\CMSBundle\Enum
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class LinkPostAgencyStateEnum extends AbstractEnum
{
    const __DEFAULT = self::UNPUBLISHED;
    const PUBLISHED = 'published';
    const UNPUBLISHED = 'unpublished';
}
