<?php declare(strict_types = 1);

/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 15/03/2017
 * Time: 18:53
 */

namespace Todotoday\CMSBundle\Enum;

use Actiane\ToolsBundle\Enum\AbstractEnum;

/**
 * Class PostStateEnum
 *
 * @category   Todo-Todev
 * @package    Todotoday\CMSBundle
 * @subpackage Todotoday\CMSBundle\Enum
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class PostStateEnum extends AbstractEnum
{
    const __DEFAULT = self::DRAFT;
    const DRAFT = 'draft';
    const TO_VALIDATE = 'to_validate';
    const VALIDATE = 'validate';
    const REFUSED = 'refused';
}
