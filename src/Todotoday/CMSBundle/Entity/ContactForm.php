<?php declare(strict_types=1);

namespace Todotoday\CMSBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Todotoday\CMSBundle\Enum\ReceiverEnum;

/**
 * ContactForm
 *
 * @ORM\Table(name="contact_form", schema="cms")
 * @ORM\Entity(repositoryClass="Todotoday\CMSBundle\Repository\ContactFormRepository")
 * @Serializer\ExclusionPolicy("all")
 */
class ContactForm
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255)
     * @Serializer\Expose()
     * @Serializer\Type("Todotoday\CMSBundle\Serializer\SubjectTranslationHandler")
     */
    private $label;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     * @Serializer\Expose()
     */
    private $email;

    /**
     * @var int
     *
     * @ORM\Column(name="categorie_tag", type="integer", nullable=true)
     * @Serializer\Expose()
     */
    private $categorieTag;

    /**
     * @ORM\Column(name="receiver", type="ReceiverEnum")
     * @Serializer\Expose()
     * @Serializer\Type("Todotoday\CMSBundle\Entity\SerializerHandler\InteractionReceiver")
     */
    private $receiver;

    /**
     * Get operationTag
     *
     * @return int
     */
    public function getCategorieTag(): ?int
    {
        return $this->categorieTag;
    }

    /**
     * Set operationTag
     *
     * @param int|null $categorieTag
     *
     * @return ContactForm
     */
    public function setCategorieTag(?int $categorieTag): self
    {
        $this->categorieTag = $categorieTag;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return ContactForm
     */
    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * Set label
     *
     * @param string $label
     *
     * @return ContactForm
     */
    public function setLabel(?string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return ReceiverEnum
     */
    public function getReceiver(): ReceiverEnum
    {
        return $this->receiver;
    }

    /**
     * @param mixed $receiver
     *
     * @return ContactForm
     */
    public function setReceiver(ReceiverEnum $receiver): self
    {
        $this->receiver = $receiver;

        return $this;
    }
}
