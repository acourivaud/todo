<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 12/05/17
 * Time: 16:41
 */

namespace Todotoday\CMSBundle\Entity\SerializerHandler;

/**
 * Just used for serializing data (jms)
 *
 * Class InteractionReceiver
 * @package Todotoday\CMSBundle\Entity\SerializerHandler
 */
class InteractionReceiver
{
}
