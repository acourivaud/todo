<?php declare(strict_types=1);

namespace Todotoday\CMSBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;

/**
 * Question
 *
 * @ORM\Table(name="question")
 * @ORM\Entity(repositoryClass="Todotoday\CMSBundle\Repository\QuestionRepository")
 * @Gedmo\TranslationEntity(class="QuestionTranslation")
 * @Serializer\ExclusionPolicy("all")
 * @Serializer\AccessorOrder("custom", custom = {"id"})
 */
class Question extends AbstractPersonalTranslatable implements TranslatableInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Expose()
     */
    protected $id;

    /**
     * @var string
     * @Gedmo\Translatable()
     * @ORM\Column(name="title", type="string", length=255)
     * @Serializer\Expose()
     */
    protected $title;

    /**
     * @var string
     * @Gedmo\Slug(fields={"title"}, updatable=true)
     * @Gedmo\Translatable()
     * @ORM\Column(name="slug", type="string", length=255)
     * @Serializer\Expose()
     */
    protected $slug;

    /**
     * @var string
     * @Gedmo\Translatable()
     * @ORM\Column(name="answer", type="text")
     * @Serializer\Expose()
     */
    protected $answer;

    /**
     * @var QuestionTranslation[]
     *
     * @ORM\OneToMany(
     *     targetEntity="Todotoday\CMSBundle\Entity\QuestionTranslation",
     *     mappedBy="object",
     *     cascade={"persist","remove"}
     * )
     */
    protected $translations;

    /**
     * @var int
     * @Gedmo\SortablePosition
     * @ORM\Column(type="integer")
     */
    protected $position;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true, length=32)
     */
    protected $country;

    /**
     * Get id
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Question
     */
    public function setTitle(?string $title): Question
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * Set answer
     *
     * @param string $answer
     *
     * @return Question
     */
    public function setAnswer(?string $answer): Question
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer
     *
     * @return string
     */
    public function getAnswer(): ?string
    {
        return $this->answer;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Question
     */
    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }

    /**
     * @param mixed $position
     *
     * @return Question
     */
    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return null|string
     */
    public function __toString(): ?string
    {
        return (string) $this->getSlug();
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string|null $country
     *
     * @return Question
     */
    public function setCountry(?string $country): Question
    {
        $this->country = $country;

        return $this;
    }
}
