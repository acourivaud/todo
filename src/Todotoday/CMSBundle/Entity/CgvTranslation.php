<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 09/07/17
 * Time: 18:01
 */

namespace Todotoday\CMSBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Translatable\Entity\MappedSuperclass\AbstractPersonalTranslation;

/**
 * Class CgvTranslations
 * @package Todotoday\CMSBundle\Entity
 * @ORM\Entity
 * @ORM\Table(schema="cms", name="cgv_translation",
 *  uniqueConstraints={@ORM\UniqueConstraint(name="lookup_unique_cgv_translation_idx", columns={
 *         "locale", "object_id", "field"})}
 * )
 */
class CgvTranslation extends AbstractPersonalTranslation
{
    /**
     * @ORM\ManyToOne(
     *      targetEntity="Todotoday\CMSBundle\Entity\Cgv",
     *      inversedBy="translations"
     * )
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $object;
}
