<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 16/04/17
 * Time: 17:59
 */

namespace Todotoday\CMSBundle\Entity\Api\MicrosoftCRM;

use Actiane\ApiConnectorBundle\Lib\EntityProxyTrait;
use Actiane\ApiConnectorBundle\Lib\Mapping as APIConnector;
use JMS\Serializer\Annotation as Serializer;

/**
 * @Serializer\ExclusionPolicy("all")
 * @SuppressWarnings(PHPMD)
 * Class ActivityPointer
 * @package Todotoday\CMSBundle\Entity\Api\MicrosoftCRM
 */
abstract class ActivityPointer
{
    use EntityProxyTrait;

    /**
     * @var string
     * @APIConnector\Id()
     * @APIConnector\Column(name="activityid", type="string", guid="id")
     * @Serializer\Expose()
     */
    protected $activityid;

    /**
     * @var int
     *
     * @APIConnector\Column(name="statecode", type="int")
     * @Serializer\Expose()
     */
    protected $statecode;

    /**
     * @var int
     *
     * @APIConnector\Column(name="statuscode", type="int")
     * @Serializer\Expose()
     */
    protected $statuscode;

    /**
     * @var bool
     *
     * @APIConnector\Column(name="leftvoicemail", type="bool")
     * @Serializer\Expose()
     */
    protected $leftvoicemail;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="createdon", type="dateTimez")
     * @Serializer\Expose()
     */
    protected $createdon;

    /**
     * @var bool
     *
     * @APIConnector\Column(name="isbilled", type="bool")
     * @Serializer\Expose()
     */
    protected $isbilled;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_ownerid_value", type="string", guid="ownerid")
     * @Serializer\Expose()
     */
    protected $owneridValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="subject", type="string")
     * @Serializer\Expose()
     */
    protected $subject;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="modifiedon", type="datetimez")
     * @Serializer\Expose()
     */
    protected $modifiedon;

    /**
     * @var int
     *
     * @APIConnector\Column(name="versionnumber", type="int")
     * @Serializer\Expose()
     */
    protected $versionnumber;

    /**
     * @var int
     *
     * @APIConnector\Column(name="prioritycode", type="int")
     * @Serializer\Expose()
     */
    protected $prioritycode;

    /**
     * @var bool
     *
     * @APIConnector\Column(name="isregularactivity", type="bool")
     * @Serializer\Expose()
     */
    protected $isregularactivity;

    /**
     * @var bool
     *
     * @APIConnector\Column(name="ismapiprivate", type="bool")
     * @Serializer\Expose()
     */
    protected $ismapiprivate;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_modifiedby_value", type="string", guid="systemuserid")
     * @Serializer\Expose()
     */
    protected $modifiedbyValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="activitytypecode", type="string")
     * @Serializer\Expose()
     */
    protected $activitytypecode;

    /**
     * @var bool
     *
     * @APIConnector\Column(name="isworkflowcreated", type="bool")
     * @Serializer\Expose()
     */
    protected $isworkflowcreated;

    /**
     * @var int
     *
     * @APIConnector\Column(name="deliveryprioritycode", type="int")
     * @Serializer\Expose()
     */
    protected $deliveryprioritycode;

    /**
     * @var int
     *
     * @APIConnector\Column(name="instancetypecode", type="int")
     * @Serializer\Expose()
     */
    protected $instancetypecode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_createdby_value", type="string", guid="systemuserid")
     * @Serializer\Expose()
     */
    protected $createdbyValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_owningbusinessunit_value", type="string", guid="businessunitid")
     * @Serializer\Expose()
     */
    protected $owningbusinessunitValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_owninguser_value", type="string", guid="systemuserid")
     * @Serializer\Expose()
     */
    protected $owninguserValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="activityadditionalparams", type="string")
     * @Serializer\Expose()
     */
    protected $activityadditionalparams;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_regardingobjectid_value", type="string", guid="regardingobjectid_account")
     * @Serializer\Expose()
     */
    protected $regardingobjectidValue;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="scheduledstart", type="datetimez")
     * @Serializer\Expose()
     */
    protected $scheduledstart;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="lastonholdtime", type="datetimez")
     * @Serializer\Expose()
     */
    protected $lastonholdtime;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_transactioncurrencyid_value", type="string", guid="transactioncurrencyid")
     * @Serializer\Expose()
     */
    protected $transactioncurrencyidValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_sendermailboxid_value", type="string", guid="mailboxid")
     * @Serializer\Expose()
     */
    protected $sendermailboxidValue;

    /**
     * @var int
     *
     * @APIConnector\Column(name="scheduleddurationminutes", type="int")
     * @Serializer\Expose()
     */
    protected $scheduleddurationminutes;

    /**
     * @var string
     *
     * @APIConnector\Column(name="exchangeweblink", type="string")
     * @Serializer\Expose()
     */
    protected $exchangeweblink;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_modifiedonbehalfby_value", type="string", guid="systemuserid")
     * @Serializer\Expose()
     */
    protected $modifiedonbehalfbyValue;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="scheduledend", type="datetimez")
     * @Serializer\Expose()
     */
    protected $scheduledend;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_slainvokedid_value", type="string", guid="slaid")
     * @Serializer\Expose()
     */
    protected $slainvokedidValue;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="postponeactivityprocessinguntil", type="datetimez")
     * @Serializer\Expose()
     */
    protected $postponeactivityprocessinguntil;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_owningteam_value", type="string", guid="teamid")
     * @Serializer\Expose()
     */
    protected $owningteamValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="traversedpath", type="string")
     * @Serializer\Expose()
     */
    protected $traversedpath;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="actualend", type="datetimez")
     * @Serializer\Expose()
     */
    protected $actualend;

    /**
     * @var int
     *
     * @APIConnector\Column(name="actualdurationminutes", type="int")
     * @Serializer\Expose()
     */
    protected $actualdurationminutes;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_serviceid_value", type="string", guid="serviceid")
     * @Serializer\Expose()
     */
    protected $serviceidValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_createdonbehalfby_value", type="string", guid="systemuserid")
     * @Serializer\Expose()
     */
    protected $createdonbehalfbyValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="seriesid", type="string")
     * @Serializer\Expose()
     */
    protected $seriesid;

    /**
     * @var string
     *
     * @APIConnector\Column(name="description", type="string")
     * @Serializer\Expose()
     */
    protected $description;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="senton", type="datetimez")
     * @Serializer\Expose()
     */
    protected $senton;

    /**
     * @var string
     *
     * @APIConnector\Column(name="processid", type="string")
     * @Serializer\Expose()
     */
    protected $processid;

    /**
     * @var int
     *
     * @APIConnector\Column(name="community", type="int")
     * @Serializer\Expose()
     */
    protected $community;

    /**
     * @var string
     *
     * @APIConnector\Column(name="exchangeitemid", type="string")
     * @Serializer\Expose()
     */
    protected $exchangeitemid;

    /**
     * @var int
     *
     * @APIConnector\Column(name="utcconversiontimezonecode", type="int")
     * @Serializer\Expose()
     */
    protected $utcconversiontimezonecode;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="actualstart", type="datetimez")
     * @Serializer\Expose()
     */
    protected $actualstart;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="deliverylastattemptedon", type="datetimez")
     * @Serializer\Expose()
     */
    protected $deliverylastattemptedon;

    /**
     * @var int
     *
     * @APIConnector\Column(name="timezoneruleversionnumber", type="int")
     * @Serializer\Expose()
     */
    protected $timezoneruleversionnumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="stageid", type="string")
     * @Serializer\Expose()
     */
    protected $stageid;

    /**
     * @var int
     *
     * @APIConnector\Column(name="onholdtime", type="int")
     * @Serializer\Expose()
     */
    protected $onholdtime;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="sortdate", type="datetimez")
     * @Serializer\Expose()
     */
    protected $sortdate;

    /**
     * @var float
     *
     * @APIConnector\Column(name="exchangerate", type="float")
     * @Serializer\Expose()
     */
    protected $exchangerate;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_slaid_value", type="string", guid="slaid")
     * @Serializer\Expose()
     */
    protected $slaidValue;

    /**
     * @return string
     */
    public function getActivityadditionalparams(): ?string
    {
        return $this->activityadditionalparams;
    }

    /**
     * @param string $activityadditionalparams
     *
     * @return ActivityPointer
     */
    public function setActivityadditionalparams(string $activityadditionalparams): ActivityPointer
    {
        return $this->setTrigger('activityadditionalparams', $activityadditionalparams);
    }

    /**
     * @return string
     */
    public function getActivityid(): ?string
    {
        return $this->activityid;
    }

    /**
     * @param string $activityid
     *
     * @return ActivityPointer
     */
    public function setActivityid(string $activityid): ActivityPointer
    {
        return $this->setTrigger('activityid', $activityid);
    }

    /**
     * @return string
     */
    public function getActivitytypecode(): ?string
    {
        return $this->activitytypecode;
    }

    /**
     * @param string $activitytypecode
     *
     * @return ActivityPointer
     */
    public function setActivitytypecode(string $activitytypecode): ActivityPointer
    {
        return $this->setTrigger('activitytypecode', $activitytypecode);
    }

    /**
     * @return int
     */
    public function getActualdurationminutes(): ?int
    {
        return $this->actualdurationminutes;
    }

    /**
     * @param int $actualdurationminutes
     *
     * @return ActivityPointer
     */
    public function setActualdurationminutes(int $actualdurationminutes): ActivityPointer
    {
        return $this->setTrigger('actualdurationminutes', $actualdurationminutes);
    }

    /**
     * @return \DateTime
     */
    public function getActualend(): ?\DateTime
    {
        return $this->actualend;
    }

    /**
     * @param \DateTime $actualend
     *
     * @return ActivityPointer
     */
    public function setActualend(?\DateTime $actualend): ActivityPointer
    {
        return $this->setTrigger('actualend', $actualend);
    }

    /**
     * @return \DateTime
     */
    public function getActualstart(): ?\DateTime
    {
        return $this->actualstart;
    }

    /**
     * @param \DateTime $actualstart
     *
     * @return ActivityPointer
     */
    public function setActualstart(?\DateTime $actualstart): ActivityPointer
    {
        return $this->setTrigger('actualstart', $actualstart);
    }

    /**
     * @return int
     */
    public function getCommunity(): ?int
    {
        return $this->community;
    }

    /**
     * @param int $community
     *
     * @return ActivityPointer
     */
    public function setCommunity(int $community): ActivityPointer
    {
        return $this->setTrigger('community', $community);
    }

    /**
     * @return string
     */
    public function getCreatedbyValue(): ?string
    {
        return $this->createdbyValue;
    }

    /**
     * @param string $createdbyValue
     *
     * @return ActivityPointer
     */
    public function setCreatedbyValue(string $createdbyValue): ActivityPointer
    {
        return $this->setTrigger('createdbyValue', $createdbyValue);
    }

    /**
     * @return \DateTime
     */
    public function getCreatedon(): ?\DateTime
    {
        return $this->createdon;
    }

    /**
     * @param \DateTime $createdon
     *
     * @return ActivityPointer
     */
    public function setCreatedon(?\DateTime $createdon): ActivityPointer
    {
        return $this->setTrigger('createdon', $createdon);
    }

    /**
     * @return string
     */
    public function getCreatedonbehalfbyValue(): ?string
    {
        return $this->createdonbehalfbyValue;
    }

    /**
     * @param string $createdonbehalfbyValue
     *
     * @return ActivityPointer
     */
    public function setCreatedonbehalfbyValue(string $createdonbehalfbyValue): ActivityPointer
    {
        return $this->setTrigger('createdonbehalfbyValue', $createdonbehalfbyValue);
    }

    /**
     * @return \DateTime
     */
    public function getDeliverylastattemptedon(): ?\DateTime
    {
        return $this->deliverylastattemptedon;
    }

    /**
     * @param \DateTime $deliverylastattemptedon
     *
     * @return ActivityPointer
     */
    public function setDeliverylastattemptedon(?\DateTime $deliverylastattemptedon): ActivityPointer
    {
        return $this->setTrigger('deliverylastattemptedon', $deliverylastattemptedon);
    }

    /**
     * @return int
     */
    public function getDeliveryprioritycode(): ?int
    {
        return $this->deliveryprioritycode;
    }

    /**
     * @param int $deliveryprioritycode
     *
     * @return ActivityPointer
     */
    public function setDeliveryprioritycode(int $deliveryprioritycode): ActivityPointer
    {
        return $this->setTrigger('deliveryprioritycode', $deliveryprioritycode);
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     *
     * @return ActivityPointer
     */
    public function setDescription(?string $description): ActivityPointer
    {
        return $this->setTrigger('description', $description);
    }

    /**
     * @return string
     */
    public function getExchangeitemid(): ?string
    {
        return $this->exchangeitemid;
    }

    /**
     * @param string $exchangeitemid
     *
     * @return ActivityPointer
     */
    public function setExchangeitemid(string $exchangeitemid): ActivityPointer
    {
        return $this->setTrigger('exchangeitemid', $exchangeitemid);
    }

    /**
     * @return float
     */
    public function getExchangerate(): ?float
    {
        return $this->exchangerate;
    }

    /**
     * @param float $exchangerate
     *
     * @return ActivityPointer
     */
    public function setExchangerate(float $exchangerate): ActivityPointer
    {
        return $this->setTrigger('exchangerate', $exchangerate);
    }

    /**
     * @return string
     */
    public function getExchangeweblink(): ?string
    {
        return $this->exchangeweblink;
    }

    /**
     * @param string $exchangeweblink
     *
     * @return ActivityPointer
     */
    public function setExchangeweblink(string $exchangeweblink): ActivityPointer
    {
        return $this->setTrigger('exchangeweblink', $exchangeweblink);
    }

    /**
     * @return int
     */
    public function getInstancetypecode(): ?int
    {
        return $this->instancetypecode;
    }

    /**
     * @param int $instancetypecode
     *
     * @return ActivityPointer
     */
    public function setInstancetypecode(int $instancetypecode): ActivityPointer
    {
        return $this->setTrigger('instancetypecode', $instancetypecode);
    }

    /**
     * @return \DateTime
     */
    public function getLastonholdtime(): ?\DateTime
    {
        return $this->lastonholdtime;
    }

    /**
     * @param \DateTime $lastonholdtime
     *
     * @return ActivityPointer
     */
    public function setLastonholdtime(?\DateTime $lastonholdtime): ActivityPointer
    {
        return $this->setTrigger('lastonholdtime', $lastonholdtime);
    }

    /**
     * @return string
     */
    public function getModifiedbyValue(): ?string
    {
        return $this->modifiedbyValue;
    }

    /**
     * @param string $modifiedbyValue
     *
     * @return ActivityPointer
     */
    public function setModifiedbyValue(string $modifiedbyValue): ActivityPointer
    {
        return $this->setTrigger('modifiedbyValue', $modifiedbyValue);
    }

    /**
     * @return \DateTime
     */
    public function getModifiedon(): ?\DateTime
    {
        return $this->modifiedon;
    }

    /**
     * @param \DateTime $modifiedon
     *
     * @return ActivityPointer
     */
    public function setModifiedon(?\DateTime $modifiedon): ActivityPointer
    {
        return $this->setTrigger('modifiedon', $modifiedon);
    }

    /**
     * @return string
     */
    public function getModifiedonbehalfbyValue(): ?string
    {
        return $this->modifiedonbehalfbyValue;
    }

    /**
     * @param string $modifiedonbehalfbyValue
     *
     * @return ActivityPointer
     */
    public function setModifiedonbehalfbyValue(string $modifiedonbehalfbyValue): ActivityPointer
    {
        return $this->setTrigger('modifiedonbehalfbyValue', $modifiedonbehalfbyValue);
    }

    /**
     * @return int
     */
    public function getOnholdtime(): ?int
    {
        return $this->onholdtime;
    }

    /**
     * @param int $onholdtime
     *
     * @return ActivityPointer
     */
    public function setOnholdtime(int $onholdtime): ActivityPointer
    {
        return $this->setTrigger('onholdtime', $onholdtime);
    }

    /**
     * Don't add retun type, because of guid thing, it could be array or string
     *
     * @return string|array
     */
    public function getOwneridValue()
    {
        return $this->owneridValue;
    }

    /**
     * @param string $owneridValue
     *
     * @return ActivityPointer
     */
    public function setOwneridValue(string $owneridValue): ActivityPointer
    {
        return $this->setTrigger('owneridValue', $owneridValue);
    }

    /**
     * @return string
     */
    public function getOwningbusinessunitValue(): ?string
    {
        return $this->owningbusinessunitValue;
    }

    /**
     * @param string $owningbusinessunitValue
     *
     * @return ActivityPointer
     */
    public function setOwningbusinessunitValue(string $owningbusinessunitValue): ActivityPointer
    {
        return $this->setTrigger('owningbusinessunitValue', $owningbusinessunitValue);
    }

    /**
     * @return string
     */
    public function getOwningteamValue(): ?string
    {
        return $this->owningteamValue;
    }

    /**
     * @param string $owningteamValue
     *
     * @return ActivityPointer
     */
    public function setOwningteamValue(string $owningteamValue): ActivityPointer
    {
        return $this->setTrigger('owningteamValue', $owningteamValue);
    }

    /**
     * @return string
     */
    public function getOwninguserValue(): ?string
    {
        return $this->owninguserValue;
    }

    /**
     * @param string $owninguserValue
     *
     * @return ActivityPointer
     */
    public function setOwninguserValue(string $owninguserValue): ActivityPointer
    {
        return $this->setTrigger('owninguserValue', $owninguserValue);
    }

    /**
     * @return \DateTime
     */
    public function getPostponeactivityprocessinguntil(): ?\DateTime
    {
        return $this->postponeactivityprocessinguntil;
    }

    /**
     * @param \DateTime $postponeactivityprocessinguntil
     *
     * @return ActivityPointer
     */
    public function setPostponeactivityprocessinguntil(?\DateTime $postponeactivityprocessinguntil): ActivityPointer
    {
        return $this->setTrigger('postponeactivityprocessinguntil', $postponeactivityprocessinguntil);
    }

    /**
     * @return int
     */
    public function getPrioritycode(): ?int
    {
        return $this->prioritycode;
    }

    /**
     * @param int $prioritycode
     *
     * @return ActivityPointer
     */
    public function setPrioritycode(int $prioritycode): ActivityPointer
    {
        return $this->setTrigger('prioritycode', $prioritycode);
    }

    /**
     * @return string
     */
    public function getProcessid(): ?string
    {
        return $this->processid;
    }

    /**
     * @param string $processid
     *
     * @return ActivityPointer
     */
    public function setProcessid(string $processid): ActivityPointer
    {
        return $this->setTrigger('processid', $processid);
    }

    /**
     * @return string|array
     */
    public function getRegardingobjectidValue()
    {
        return $this->regardingobjectidValue;
    }

    /**
     * @param string $regardingobjectidValue
     *
     * @return EntityProxyTrait|mixed
     */
    public function setRegardingobjectidValue(string $regardingobjectidValue)
    {
        return $this->setTrigger('regardingobjectidValue', $regardingobjectidValue);
    }

    /**
     * @return int
     */
    public function getScheduleddurationminutes(): ?int
    {
        return $this->scheduleddurationminutes;
    }

    /**
     * @param int $scheduleddurationminutes
     *
     * @return ActivityPointer
     */
    public function setScheduleddurationminutes(int $scheduleddurationminutes): ActivityPointer
    {
        return $this->setTrigger('scheduleddurationminutes', $scheduleddurationminutes);
    }

    /**
     * @return \DateTime
     */
    public function getScheduledend(): ?\DateTime
    {
        return $this->scheduledend;
    }

    /**
     * @param \DateTime $scheduledend
     *
     * @return ActivityPointer
     */
    public function setScheduledend(?\DateTime $scheduledend): ActivityPointer
    {
        return $this->setTrigger('scheduledend', $scheduledend);
    }

    /**
     * @return \DateTime
     */
    public function getScheduledstart(): ?\DateTime
    {
        return $this->scheduledstart;
    }

    /**
     * @param \DateTime $scheduledstart
     *
     * @return ActivityPointer
     */
    public function setScheduledstart(?\DateTime $scheduledstart): ActivityPointer
    {
        return $this->setTrigger('scheduledstart', $scheduledstart);
    }

    /**
     * @return string
     */
    public function getSendermailboxidValue(): ?string
    {
        return $this->sendermailboxidValue;
    }

    /**
     * @param string $sendermailboxidValue
     *
     * @return ActivityPointer
     */
    public function setSendermailboxidValue(string $sendermailboxidValue): ActivityPointer
    {
        return $this->setTrigger('sendermailboxidValue', $sendermailboxidValue);
    }

    /**
     * @return \DateTime
     */
    public function getSenton(): ?\DateTime
    {
        return $this->senton;
    }

    /**
     * @param \DateTime $senton
     *
     * @return ActivityPointer
     */
    public function setSenton(?\DateTime $senton): ActivityPointer
    {
        return $this->setTrigger('senton', $senton);
    }

    /**
     * @return string
     */
    public function getSeriesid(): ?string
    {
        return $this->seriesid;
    }

    /**
     * @param string $seriesid
     *
     * @return ActivityPointer
     */
    public function setSeriesid(string $seriesid): ActivityPointer
    {
        return $this->setTrigger('seriesid', $seriesid);
    }

    /**
     * @return string|array
     */
    public function getServiceidValue()
    {
        return $this->serviceidValue;
    }

    /**
     * @param string $serviceidValue
     *
     * @return ActivityPointer
     */
    public function setServiceidValue(string $serviceidValue): ActivityPointer
    {
        return $this->setTriggerGuid(
            'serviceidValue',
            array(
                'target' => 'services',
                'value' => $serviceidValue,
            )
        );
    }

    /**
     * @return string
     */
    public function getSlaidValue(): ?string
    {
        return $this->slaidValue;
    }

    /**
     * @param string $slaidValue
     *
     * @return ActivityPointer
     */
    public function setSlaidValue(string $slaidValue): ActivityPointer
    {
        return $this->setTrigger('slaidValue', $slaidValue);
    }

    /**
     * @return string
     */
    public function getSlainvokedidValue(): ?string
    {
        return $this->slainvokedidValue;
    }

    /**
     * @param string $slainvokedidValue
     *
     * @return ActivityPointer
     */
    public function setSlainvokedidValue(string $slainvokedidValue): ActivityPointer
    {
        return $this->setTrigger('slainvokedidValue', $slainvokedidValue);
    }

    /**
     * @return \DateTime
     */
    public function getSortdate(): ?\DateTime
    {
        return $this->sortdate;
    }

    /**
     * @param \DateTime $sortdate
     *
     * @return ActivityPointer
     */
    public function setSortdate(?\DateTime $sortdate): ActivityPointer
    {
        return $this->setTrigger('sortdate', $sortdate);
    }

    /**
     * @return string
     */
    public function getStageid(): ?string
    {
        return $this->stageid;
    }

    /**
     * @param string $stageid
     *
     * @return ActivityPointer
     */
    public function setStageid(string $stageid): ActivityPointer
    {
        return $this->setTrigger('stageid', $stageid);
    }

    /**
     * @return int
     */
    public function getStatecode(): ?int
    {
        return $this->statecode;
    }

    /**
     * @param int $statecode
     *A
     * @return ActivityPointer
     */
    public function setStatecode(int $statecode): self
    {
        return $this->setTrigger('statecode', $statecode);
    }

    /**
     * @return int
     */
    public function getStatuscode(): ?int
    {
        return $this->statuscode;
    }

    /**
     * @param int $statuscode
     *
     * @return ActivityPointer
     */
    public function setStatuscode(int $statuscode): self
    {
        return $this->setTrigger('statuscode', $statuscode);
    }

    /**
     * @return string
     */
    public function getSubject(): ?string
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     *
     * @return ActivityPointer
     */
    public function setSubject(string $subject): ActivityPointer
    {
        return $this->setTrigger('subject', $subject);
    }

    /**
     * @return int
     */
    public function getTimezoneruleversionnumber(): ?int
    {
        return $this->timezoneruleversionnumber;
    }

    /**
     * @param int $timezoneruleversionnumber
     *
     * @return ActivityPointer
     */
    public function setTimezoneruleversionnumber(int $timezoneruleversionnumber): ActivityPointer
    {
        return $this->setTrigger('timezoneruleversionnumber', $timezoneruleversionnumber);
    }

    /**
     * @return string
     */
    public function getTransactioncurrencyidValue(): ?string
    {
        return $this->transactioncurrencyidValue;
    }

    /**
     * @param string $transactioncurrencyidValue
     *
     * @return ActivityPointer
     */
    public function setTransactioncurrencyidValue(string $transactioncurrencyidValue): ActivityPointer
    {
        return $this->setTrigger('transactioncurrencyidValue', $transactioncurrencyidValue);
    }

    /**
     * @return string
     */
    public function getTraversedpath(): ?string
    {
        return $this->traversedpath;
    }

    /**
     * @param string $traversedpath
     *
     * @return ActivityPointer
     */
    public function setTraversedpath(string $traversedpath): ActivityPointer
    {
        return $this->setTrigger('traversedpath', $traversedpath);
    }

    /**
     * @return int
     */
    public function getUtcconversiontimezonecode(): ?int
    {
        return $this->utcconversiontimezonecode;
    }

    /**
     * @param int $utcconversiontimezonecode
     *
     * @return ActivityPointer
     */
    public function setUtcconversiontimezonecode(int $utcconversiontimezonecode): ActivityPointer
    {
        return $this->setTrigger('utcconversiontimezonecode', $utcconversiontimezonecode);
    }

    /**
     * @return int
     */
    public function getVersionnumber(): ?int
    {
        return $this->versionnumber;
    }

    /**
     * @param int $versionnumber
     *
     * @return ActivityPointer
     */
    public function setVersionnumber(int $versionnumber): ActivityPointer
    {
        return $this->setTrigger('versionnumber', $versionnumber);
    }

    /**
     * @return bool
     */
    public function isIsbilled(): ?bool
    {
        return $this->isbilled;
    }

    /**
     * @param bool $isbilled
     *
     * @return ActivityPointer
     */
    public function setIsbilled(bool $isbilled): ActivityPointer
    {
        return $this->setTrigger('isbilled', $isbilled);
    }

    /**
     * @return bool
     */
    public function isIsmapiprivate(): ?bool
    {
        return $this->ismapiprivate;
    }

    /**
     * @param bool $ismapiprivate
     *
     * @return ActivityPointer
     */
    public function setIsmapiprivate(bool $ismapiprivate): ActivityPointer
    {
        return $this->setTrigger('ismapiprivate', $ismapiprivate);
    }

    /**
     * @return bool
     */
    public function isIsregularactivity(): ?bool
    {
        return $this->isregularactivity;
    }

    /**
     * @param bool $isregularactivity
     *
     * @return ActivityPointer
     */
    public function setIsregularactivity(bool $isregularactivity): ActivityPointer
    {
        return $this->setTrigger('isregularactivity', $isregularactivity);
    }

    /**
     * @return bool
     */
    public function isIsworkflowcreated(): ?bool
    {
        return $this->isworkflowcreated;
    }

    /**
     * @param bool $isworkflowcreated
     *
     * @return ActivityPointer
     */
    public function setIsworkflowcreated(bool $isworkflowcreated): ActivityPointer
    {
        return $this->setTrigger('isworkflowcreated', $isworkflowcreated);
    }

    /**
     * @return bool
     */
    public function isLeftvoicemail(): ?bool
    {
        return $this->leftvoicemail;
    }

    /**
     * @param bool $leftvoicemail
     *
     * @return ActivityPointer
     */
    public function setLeftvoicemail(bool $leftvoicemail): ActivityPointer
    {
        return $this->setTrigger('leftvoicemail', $leftvoicemail);
    }

    /**
     * Method to call when persisting new value
     * Need to target which entity is bound
     *
     * @param string $owneridValue
     * @param string $target
     *
     * @return ActivityPointer
     */
    public function setOwnerIdValueGuid(string $owneridValue, string $target): ActivityPointer
    {
        return $this->setTriggerGuid(
            'owneridValue',
            array(
                'target' => $target,
                'value' => $owneridValue,
            )
        );
    }

    /**
     * @param string $regardingobjectidValue
     * @param string $target
     *
     * @return ActivityPointer
     */
    public function setRegardingobjectidValueGuid(?string $regardingobjectidValue, ?string $target): ActivityPointer
    {
        return $this->setTriggerGuid(
            'regardingobjectidValue',
            array(
                'target' => $target,
                'value' => $regardingobjectidValue,
            )
        );
    }
}
