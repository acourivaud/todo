<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 16/04/17
 * Time: 17:30
 */

namespace Todotoday\CMSBundle\Entity\Api\MicrosoftCRM;

use Actiane\ApiConnectorBundle\Lib\EntityProxyTrait;
use Actiane\ApiConnectorBundle\Lib\Mapping as APIConnector;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class Interaction
 * @SuppressWarnings(PHPMD)
 * @Serializer\ExclusionPolicy("all")
 * @APIConnector\Entity(path="aps_interactions", repositoryId="todotoday.cms.repository.api.interaction")
 * @package Todotoday\CMSBundle\Entity\Api\MicrosoftCRM
 */
class Interaction extends ActivityPointer
{
    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="aps_dateinteraction", type="date")
     * @Serializer\Expose()
     */
    protected $apsDateinteraction;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_souscategorie", type="int")
     * @Serializer\Expose()
     */
    protected $apsSouscategorie;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_descriptioninteraction", type="string")
     * @Serializer\Expose()
     */
    protected $apsDescriptioninteraction;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_typeinteraction", type="string")
     * @Serializer\Expose()
     */
    protected $apsTypeinteraction;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_aps_conciergerie_value", type="string", guid="accountid")
     * @Serializer\Expose()
     */
    protected $apsConciergerieValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_aps_commandeinteraction_value", type="string", guid="salesorderid")
     * @Serializer\Expose()
     */
    protected $apsCommandeinteractionValue;

    /**
     * @var int
     *
     * @APIConnector\Column(name="importsequencenumber", type="int")
     * @Serializer\Expose()
     */
    protected $importsequencenumber;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_categorie", type="int")
     * @Serializer\Expose()
     */
    protected $apsCategorie;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="overriddencreatedon", type="datetimez")
     * @Serializer\Expose()
     */
    protected $overriddencreatedon;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_canalinteraction", type="string")
     * @Serializer\Expose()
     */
    protected $apsCanalinteraction;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_sujet", type="int")
     * @Serializer\Expose()
     */
    protected $apsSujet;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_jourdelasemainesouhaite", type="int")
     * @Serializer\Expose()
     */
    protected $apsJourDeLaSemaineSouhaite;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="aps_heurededbutsouhaitee", type="datetimez")
     * @Serializer\Expose()
     */
    protected $apsHeureDeDebutSouhaite;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="aps_heuredefinsouhaitee", type="datetimez")
     * @Serializer\Expose()
     */
    protected $apsHeureDeDeFinSouhaite;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_commandeid", type="string", guid="salesorderid")
     * @Serializer\Expose()
     */
    protected $apsCommandeid;

    /**
     * @return \DateTime
     */
    public function getApsDateinteraction(): ?\DateTime
    {
        return $this->apsDateinteraction;
    }

    /**
     * @param \DateTime $apsDateinteraction
     *
     * @return Interaction
     */
    public function setApsDateinteraction(?\DateTime $apsDateinteraction): Interaction
    {
        return $this->setTrigger('apsDateinteraction', $apsDateinteraction);
    }

    /**
     * @return int
     */
    public function getApsSouscategorie(): ?int
    {
        return $this->apsSouscategorie;
    }

    /**
     * @param int $apsSouscategorie
     *
     * @return Interaction
     */
    public function setApsSouscategorie(int $apsSouscategorie): Interaction
    {
        return $this->setTrigger('apsSouscategorie', $apsSouscategorie);
    }

    /**
     * @return string
     */
    public function getApsDescriptioninteraction(): ?string
    {
        return $this->apsDescriptioninteraction;
    }

    /**
     * @param string $apsDescriptioninteraction
     *
     * @return Interaction
     */
    public function setApsDescriptioninteraction(string $apsDescriptioninteraction): Interaction
    {
        return $this->setTrigger('apsDescriptioninteraction', $apsDescriptioninteraction);
    }

    /**
     * @return string
     */
    public function getApsTypeinteraction(): ?string
    {
        return $this->apsTypeinteraction;
    }

    /**
     * @param string $apsTypeinteraction
     *
     * @return Interaction
     */
    public function setApsTypeinteraction(string $apsTypeinteraction): Interaction
    {
        return $this->setTrigger('apsTypeinteraction', $apsTypeinteraction);
    }

    /**
     * @return string
     */
    public function getApsConciergerieValue(): ?string
    {
        return $this->apsConciergerieValue;
    }

    /**
     * @param string $apsConciergerieValue
     *
     * @return Interaction
     */
    public function setApsConciergerieValue(string $apsConciergerieValue): Interaction
    {
        return $this->setTrigger('apsConciergerieValue', $apsConciergerieValue);
    }

    /**
     * @return string
     */
    public function getApsCommandeinteractionValue(): ?string
    {
        return $this->apsCommandeinteractionValue;
    }

    /**
     * @param string $apsCommandeinteractionValue
     *
     * @return Interaction
     */
    public function setApsCommandeinteractionValue(string $apsCommandeinteractionValue): Interaction
    {
        return $this->setTrigger('apsCommandeinteractionValue', $apsCommandeinteractionValue);
    }

    /**
     * @return int
     */
    public function getImportsequencenumber(): ?int
    {
        return $this->importsequencenumber;
    }

    /**
     * @param int $importsequencenumber
     *
     * @return Interaction
     */
    public function setImportsequencenumber(int $importsequencenumber): Interaction
    {
        return $this->setTrigger('importsequencenumber', $importsequencenumber);
    }

    /**
     * @return int
     */
    public function getApsCategorie(): ?int
    {
        return $this->apsCategorie;
    }

    /**
     * @param int $apsCategorie
     *
     * @return Interaction
     */
    public function setApsCategorie(int $apsCategorie): Interaction
    {
        return $this->setTrigger('apsCategorie', $apsCategorie);
    }

    /**
     * @return \DateTime
     */
    public function getOverriddencreatedon(): ?\DateTime
    {
        return $this->overriddencreatedon;
    }

    /**
     * @param \DateTime $overriddencreatedon
     *
     * @return Interaction
     */
    public function setOverriddencreatedon(?\DateTime $overriddencreatedon): Interaction
    {
        return $this->setTrigger('overriddencreatedon', $overriddencreatedon);
    }

    /**
     * @return string
     */
    public function getApsCanalinteraction(): ?string
    {
        return $this->apsCanalinteraction;
    }

    /**
     * @param string $apsCanalinteraction
     *
     * @return Interaction
     */
    public function setApsCanalinteraction(string $apsCanalinteraction): Interaction
    {
        return $this->setTrigger('apsCanalinteraction', $apsCanalinteraction);
    }

    /**
     * @return int
     */
    public function getApsSujet(): ?int
    {
        return $this->apsSujet;
    }

    /**
     * @param int $apsSujet
     *
     * @return Interaction
     */
    public function setApsSujet(int $apsSujet): Interaction
    {
        return $this->setTrigger('apsSujet', $apsSujet);
    }

    /**
     * @return int
     */
    public function getApsJourDeLaSemaineSouhaite(): ?int
    {
        return $this->apsJourDeLaSemaineSouhaite;
    }

    /**
     * @param int|null $apsJourDeLaSemaineSouhaite
     *
     * @return Interaction
     */
    public function setApsJourDeLaSemaineSouhaite(?int $apsJourDeLaSemaineSouhaite): Interaction
    {
        return $this->setTrigger('apsJourDeLaSemaineSouhaite', $apsJourDeLaSemaineSouhaite);
    }

    /**
     * @return \DateTime
     */
    public function getApsHeureDeDebutSouhaite(): ?\DateTime
    {
        return $this->apsHeureDeDebutSouhaite;
    }

    /**
     * @param \DateTime|null $apsHeureDeDebutSouhaite
     *
     * @return Interaction
     */
    public function setApsHeureDeDebutSouhaite(?\DateTime $apsHeureDeDebutSouhaite): Interaction
    {
        return $this->setTrigger('apsHeureDeDebutSouhaite', $apsHeureDeDebutSouhaite);
    }

    /**
     * @return \DateTime
     */
    public function getApsHeureDeDeFinSouhaite(): ?\DateTime
    {
        return $this->apsHeureDeDeFinSouhaite;
    }

    /**
     * @param \DateTime|null $apsHeureDeDeFinSouhaite
     *
     * @return Interaction
     */
    public function setApsHeureDeDeFinSouhaite(?\DateTime $apsHeureDeDeFinSouhaite): Interaction
    {
        return $this->setTrigger('apsHeureDeDeFinSouhaite', $apsHeureDeDeFinSouhaite);
    }

    /**
     * @return string
     */
    public function getApsCommandeid(): ?string
    {
        return $this->apsCommandeid;
    }

    /**
     * @param string|null $apsCommandeid
     *
     * @return Interaction
     */
    public function setApsCommandeid(?string $apsCommandeid): Interaction
    {
        return $this->setTrigger('apsCommandeid', $apsCommandeid);
    }
}
