<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 26/06/17
 * Time: 16:14
 */

namespace Todotoday\CMSBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Translatable\Entity\MappedSuperclass\AbstractPersonalTranslation;

/**
 * Class QuestionTranslation
 * @package Todotoday\CMSBundle\Entity
 * @ORM\Entity()
 * @ORM\Table(schema="cms",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="lookup_unique_question_translation_idx", columns={
 *         "locale", "object_id", "field"
 * })})
 */
class QuestionTranslation extends AbstractPersonalTranslation
{
    /**
     * @ORM\ManyToOne(
     *     targetEntity="Todotoday\CMSBundle\Entity\Question",
     *     inversedBy="translations"
     * )
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $object;
}
