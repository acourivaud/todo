<?php declare(strict_types=1);

namespace Todotoday\CMSBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\MediaBundle\Entity\File;

/**
 * LinkFileAgency
 *
 * @ORM\Table(name="link_file_agency", schema="cms")
 * @ORM\Entity(repositoryClass="Todotoday\CMSBundle\Repository\LinkFileAgencyRepository")
 * @Serializer\ExclusionPolicy("all")
 */
class LinkFileAgency
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var File
     *
     * @ORM\ManyToOne(targetEntity="Todotoday\MediaBundle\Entity\File", inversedBy="links",
     *     cascade={"persist", "remove", "refresh"})
     * @Serializer\Expose()
     */
    protected $file;

    /**
     * @var Agency
     *
     * @ORM\ManyToOne(targetEntity="Todotoday\CoreBundle\Entity\Agency", inversedBy="files",
     *      cascade={"PERSIST"})
     */
    protected $agency;

    /**
     * @var bool
     *
     * @ORM\Column(name="visibility", type="boolean")
     */
    private $visibility = true;

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getAgency() ? (string) $this->getAgency()->getSlug() : '';
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set visibility
     *
     * @param boolean $visibility
     *
     * @return LinkFileAgency
     */
    public function setVisibility($visibility): self
    {
        $this->visibility = $visibility;

        return $this;
    }

    /**
     * Get visibility
     *
     * @return bool
     */
    public function getVisibility(): ?bool
    {
        return $this->visibility;
    }

    /**
     * Set file
     *
     * @param File $file
     *
     * @return LinkFileAgency
     */
    public function setFile(File $file = null): self
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return File
     */
    public function getFile(): ?File
    {
        return $this->file;
    }

    /**
     * Set agency
     *
     * @param Agency $agency
     *
     * @return LinkFileAgency
     */
    public function setAgency(Agency $agency = null): self
    {
        $this->agency = $agency;

        return $this;
    }

    /**
     * Get agency
     *
     * @return Agency
     */
    public function getAgency(): ?Agency
    {
        return $this->agency;
    }
}
