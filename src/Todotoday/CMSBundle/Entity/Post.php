<?php declare(strict_types=1);

namespace Todotoday\CMSBundle\Entity;

use Actiane\ToolsBundle\Enum\AbstractEnum;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Doctrine\ORM\Mapping\JoinTable;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;
use Sonata\ClassificationBundle\Model\TagInterface;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Symfony\Component\Validator\Constraints as Assert;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\ClassificationBundle\Entity\Category;
use Todotoday\ClassificationBundle\Entity\Tag;
use Todotoday\CMSBundle\Enum\PostStateEnum;
use Todotoday\MediaBundle\Entity\Media;
use Todotoday\PluginBundle\Entity\Plugin;

/**
 * Post
 *
 * @ORM\Table(name="post", schema="cms" ,
 *     indexes={
 *          @Index(name="search_slug_post", columns={"slug"})
 *
 *     })
 * @ORM\Entity(repositoryClass="Todotoday\CMSBundle\Repository\PostRepository")
 * @Gedmo\TranslationEntity(class="Todotoday\CMSBundle\Entity\PostTranslation")
 *
 * @Serializer\ExclusionPolicy("all")
 */
class Post extends AbstractPersonalTranslatable implements TranslatableInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Expose()
     */
    protected $id;

    /**
     * @var string
     * @Gedmo\Slug(fields={"id", "title"}, updatable=false)
     * @ORM\Column(name="slug", type="string", length=32)
     * @Serializer\Expose()
     */
    protected $slug;

    /**
     * @var Category
     *
     * @ORM\ManyToMany(
     *     targetEntity="Todotoday\ClassificationBundle\Entity\Category",
     *     inversedBy="posts",
     *     cascade={"persist"}
     *     )
     * @JoinTable(name="posts_categories", schema="cms")
     */
    protected $categories;

    /**
     * @var Tag
     *
     * @ORM\ManyToMany(
     *     targetEntity="Todotoday\ClassificationBundle\Entity\Tag",
     *     inversedBy="posts",
     *     cascade={"persist"}
     *     )
     * @JoinTable(name="posts_tags", schema="cms")
     */
    protected $tags;

    /**
     * @var Collection|LinkPostAgency[]
     *
     * @ORM\OneToMany(targetEntity="LinkPostAgency", mappedBy="post", cascade={"remove", "persist"}, orphanRemoval=true)
     */
    protected $agencies;

    /**
     * @var string
     *
     * @Gedmo\Translatable()
     * @ORM\Column(name="title", type="string", length=255)
     * @Serializer\Expose()
     * @Serializer\Groups("account")
     */
    protected $title;

    /**
     * @var string
     * @Gedmo\Translatable()
     * @ORM\Column(name="subtitle", type="text")
     * @Serializer\Expose()
     * @Serializer\Groups("account")
     */
    protected $subtitle;

    /**
     * @var string
     * @Gedmo\Translatable()
     * @ORM\Column(name="synopsis", type="text", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups("account")
     */
    protected $synopsis;

    /**
     * @var string
     *
     * @Gedmo\Translatable()
     * @ORM\Column(name="content", type="text")
     * @Serializer\Expose()
     */
    protected $content;

    /**
     * @ORM\ManyToOne(targetEntity="Todotoday\MediaBundle\Entity\Media", cascade={"persist", "remove", "refresh"})
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull()
     * @Serializer\Expose()
     */
    protected $media;

    /**
     * @var PostStateEnum
     *
     * @ORM\Column(name="state", type="PostStateEnum", nullable=true)
     * @Serializer\Expose()
     */
    protected $state;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     * @Serializer\Expose()
     */
    protected $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     * @Serializer\Expose()
     */
    protected $updatedAt;

    /**
     * @var Account
     *
     * @ORM\ManyToOne(targetEntity="Todotoday\AccountBundle\Entity\Account", inversedBy="posts")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     * @Serializer\Expose()
     */
    protected $author;

    /**
     * @var Collection|PostTranslation[]
     *
     * @ORM\OneToMany(
     *     targetEntity="Todotoday\CMSBundle\Entity\PostTranslation",
     *     mappedBy="object",
     *     cascade={"persist","remove"}
     *     )
     */
    protected $translations;

    /**
     * @var Plugin
     *
     * @ORM\ManyToOne(targetEntity="Todotoday\PluginBundle\Entity\Plugin", inversedBy="posts")
     */
    protected $plugin;

    /**
     * @var string
     *
     * @ORM\Column(name="plugin_post_id", type="text", nullable=true)
     */
    protected $pluginPostId;

    /**
     * @var string
     *
     * @ORM\Column(name="plugin_post_url", type="text", nullable=true)
     */
    protected $pluginPostUrl;

    /**
     * #########################################################################################################
     * ############################################ METHODES ###################################################
     * #########################################################################################################
     */

    /**
     * Post constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->categories = new ArrayCollection();
        $this->agencies = new ArrayCollection();
        $this->tags = new ArrayCollection();
    }

    /**
     * Do __toString
     *
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->getTitle();
    }

    /**
     * Add agency
     *
     * @param LinkPostAgency $agency
     *
     * @return Post
     */
    public function addAgency(LinkPostAgency $agency): self
    {
        $this->agencies[] = $agency;
        $agency->setPost($this);

        return $this;
    }

    /**
     * Add category
     *
     * @param Category|Category $category
     *
     * @return Post
     */
    public function addCategory(Category $category): self
    {
        $this->categories[] = $category;

        return $this;
    }

    /**
     * Add tag
     *
     * @param TagInterface|Category $tag
     *
     * @return Post
     */
    public function addTag(TagInterface $tag): self
    {
        $this->tags[] = $tag;

        return $this;
    }

    /**
     * Get agencies
     *
     * @return Collection|LinkPostAgency[]
     */
    public function getAgencies(): Collection
    {
        return $this->agencies;
    }

    /**
     * Get author
     *
     * @return Account
     */
    public function getAuthor(): ?Account
    {
        return $this->author;
    }

    /**
     * Set author
     *
     * @param Account $author
     *
     * @return Post
     */
    public function setAuthor(?Account $author): self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get categories
     *
     * @return Collection|Category[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * Set content
     *
     * @param null|string $content
     *
     * @return Post
     */
    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Post
     */
    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Media|null
     */
    public function getMedia(): ?Media
    {
        return $this->media;
    }

    /**
     * @param Media $media
     *
     * @return Post
     */
    public function setMedia(?Media $media): self
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Post
     */
    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get state
     *
     * @return PostStateEnum
     */
    public function getState(): ?PostStateEnum
    {
        return $this->state;
    }

    /**
     * Set state
     *
     * @param PostStateEnum|AbstractEnum $state
     *
     * @return Post
     */
    public function setState(?PostStateEnum $state): self
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get tags
     *
     * @return Collection|Tag[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Post
     */
    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Post
     */
    public function setUpdatedAt(\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Remove agency
     *
     * @param LinkPostAgency $agency
     *
     * @return Post
     */
    public function removeAgency(LinkPostAgency $agency): self
    {
        $this->agencies->removeElement($agency);
        $agency->setPost(null);

        return $this;
    }

    /**
     * Remove category
     *
     * @param Category $category
     *
     * @return Post
     */
    public function removeCategory(Category $category): self
    {
        $this->categories->removeElement($category);

        return $this;
    }

    /**
     * Remove tag
     *
     * @param Tag $tag
     *
     * @return Post
     */
    public function removeTag(Tag $tag): self
    {
        $this->tags->removeElement($tag);

        return $this;
    }

    /**
     * @return string
     */
    public function getSubtitle(): ?string
    {
        return $this->subtitle;
    }

    /**
     * @param string $subtitle
     *
     * @return Post
     */
    public function setSubtitle(?string $subtitle): self
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    /**
     * @return string
     */
    public function getSynopsis(): ?string
    {
        return $this->synopsis;
    }

    /**
     * @param string $synopsis
     *
     * @return Post
     */
    public function setSynopsis(?string $synopsis): self
    {
        $this->synopsis = $synopsis;

        return $this;
    }

    /**
     * @return Plugin
     */
    public function getPlugin(): ?Plugin
    {
        return $this->plugin;
    }

    /**
     * @param null|Plugin $plugin
     *
     * @return Post
     */
    public function setPlugin(?Plugin $plugin): self
    {
        $this->plugin = $plugin;

        return $this;
    }

    /**
     * @return string
     */
    public function getPluginPostId(): ?string
    {
        return $this->pluginPostId;
    }

    /**
     * @param null|string $pluginPostId
     *
     * @return Post
     */
    public function setPluginPostId(?string $pluginPostId): self
    {
        $this->pluginPostId = $pluginPostId;

        return $this;
    }

    /**
     * @return string
     */
    public function getPluginPostUrl(): ?string
    {
        return $this->pluginPostUrl;
    }

    /**
     * @param string $pluginPostUrl
     *
     * @return Post
     */
    public function setPluginPostUrl(?string $pluginPostUrl): self
    {
        $this->pluginPostUrl = $pluginPostUrl;

        return $this;
    }
}
