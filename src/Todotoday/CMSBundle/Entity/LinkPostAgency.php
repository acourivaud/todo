<?php declare(strict_types=1);

namespace Todotoday\CMSBundle\Entity;

use Actiane\ToolsBundle\Enum\AbstractEnum;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;
use Todotoday\CMSBundle\Enum\LinkPostAgencyStateEnum;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * PostAgency
 *
 * @ORM\Table(name="link_post_agency", schema="cms")
 * @ORM\Entity(repositoryClass="Todotoday\CMSBundle\Repository\LinkPostAgencyRepository")
 * @Serializer\ExclusionPolicy("all")
 * @Serializer\AccessorOrder("custom", custom = {"state","publicationDate","post"} )
 */
class LinkPostAgency
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Expose()
     */
    protected $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    protected $createdAt;

    /**
     * @var Agency
     *
     * @ORM\ManyToOne(targetEntity="Todotoday\CoreBundle\Entity\Agency", inversedBy="posts", cascade={"persist"})
     * @Serializer\Expose()
     */
    protected $agency;

    /**
     * @var Post
     *
     * @ORM\ManyToOne(targetEntity="Post", inversedBy="agencies", cascade={"persist"})
     * @Serializer\Expose()
     */
    protected $post;

    /**
     * @var LinkPostAgencyStateEnum
     *
     * @ORM\Column(name="state", type="LinkPostAgencyStateEnum")
     * @Serializer\Expose()
     */
    protected $state;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="publicationDate", type="datetimetz")
     * @Serializer\Expose()
     */
    protected $publicationDate;

    /**
     * @var bool
     *
     * @ORM\Column(name="editable", type="boolean")
     */
    protected $editable;

    /**
     * LinkPostAgency constructor.
     */
    public function __construct()
    {
        $this->editable = false;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->agency . (string) $this->post;
    }

    /**
     * Get agency
     *
     * @return Agency
     */
    public function getAgency()
    {
        return $this->agency;
    }

    /**
     * Set agency
     *
     * @param Agency $agency
     *
     * @return LinkPostAgency
     */
    public function setAgency(Agency $agency = null): self
    {
        $this->agency = $agency;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return LinkPostAgency
     */
    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Get post
     *
     * @return Post
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Set post
     *
     * @param Post $post
     *
     * @return LinkPostAgency
     */
    public function setPost(?Post $post = null): self
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get publicationDate
     *
     * @return \DateTime
     */
    public function getPublicationDate()
    {
        return $this->publicationDate;
    }

    /**
     * Set publicationDate
     *
     * @param \DateTime $publicationDate
     *
     * @return LinkPostAgency
     */
    public function setPublicationDate(?\DateTime $publicationDate): self
    {
        $this->publicationDate = $publicationDate;

        return $this;
    }

    /**
     * Get state
     *
     * @return LinkPostAgencyStateEnum
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set state
     *
     * @param LinkPostAgencyStateEnum|AbstractEnum $state
     *
     * @return LinkPostAgency
     */
    public function setState(?LinkPostAgencyStateEnum $state): self
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get editable
     *
     * @return bool
     */
    public function isEditable(): bool
    {
        return $this->editable;
    }

    /**
     * Set editable
     *
     * @param bool $editable
     *
     * @return LinkPostAgency
     */
    public function setEditable(bool $editable = false): self
    {
        $this->editable = $editable;

        return $this;
    }
}
