<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 24/06/17
 * Time: 04:03
 */

namespace Todotoday\CMSBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;
use Todotoday\MediaBundle\Entity\Media;
use Todotoday\MediaBundle\Traits\FormatedFilesTrait;

/**
 * Class Cgv
 *
 * @package Todotoday\CMSBundle\Entity
 * @ORM\Table(name="cgv", schema="core")
 * @ORM\Entity(repositoryClass="Todotoday\CMSBundle\Repository\CgvRepository")
 * @Gedmo\TranslationEntity(class="CgvTranslation")
 * @Serializer\ExclusionPolicy("all")
 *
 */
class Cgv extends AbstractPersonalTranslatable implements TranslatableInterface
{
    use FormatedFilesTrait;

    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="title", type="string", nullable=true)
     * @Gedmo\Translatable)
     * @Serializer\Expose()
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(name="model", type="string",length=32)
     * @Serializer\Expose()
     */
    private $model;

    /**
     * @var string
     * @ORM\Column(name="content", type="text")
     * @Gedmo\Translatable)
     * @Serializer\Expose()
     */
    private $content;

    /**
     * @var Media
     *
     * @ORM\ManyToOne(targetEntity="Todotoday\MediaBundle\Entity\Media", cascade={"persist", "remove", "refresh"})
     */
    protected $media;

    /**
     * @var Media
     *
     * @ORM\ManyToOne(targetEntity="Todotoday\MediaBundle\Entity\Media", cascade={"persist", "remove", "refresh"})
     *
     */
    protected $mediaEn;

    /**
     * @var Media
     *
     * @ORM\ManyToOne(targetEntity="Todotoday\MediaBundle\Entity\Media", cascade={"persist", "remove", "refresh"})
     *
     */
    protected $mediaDe;

    /**
     * @var CgvTranslation[]
     *
     * @ORM\OneToMany(targetEntity="Todotoday\CMSBundle\Entity\CgvTranslation",
     *     mappedBy="object",
     *     cascade={"persist","remove"}
     * )
     */
    protected $translations;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Media
     */
    public function getMedia(): ?Media
    {
        return $this->media;
    }

    /**
     * @return Media
     *
     */
    public function getMediaEn(): ?Media
    {
        return $this->mediaEn;
    }

    /**
     * @return Media
     *
     */
    public function getMediaDe(): ?Media
    {
        return $this->mediaDe;
    }

    /**
     * @return string
     */
    public function getModel(): ?string
    {
        return $this->model;
    }

    /**
     * @param string $model
     *
     * @return Cgv
     */
    public function setModel(string $model): Cgv
    {
        $this->model = $model;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param string $content
     *
     * @return Cgv
     */
    public function setContent(string $content): Cgv
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return Cgv
     */
    public function setTitle(string $title): Cgv
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->model;
    }

    /**
     * @param Media $media
     *
     * @return Cgv
     */
    public function setMedia(Media $media): Cgv
    {
        $this->media = $media;

        return $this;
    }

    /**
     * @param Media $mediaDe
     *
     * @return Cgv
     *
     */
    public function setMediaDe(Media $mediaDe): Cgv
    {
        $this->mediaDe = $mediaDe;

        return $this;
    }

    /**
     * @param Media $mediaEn
     *
     * @return Cgv
     *
     */
    public function setMediaEn(Media $mediaEn): Cgv
    {
        $this->mediaEn = $mediaEn;

        return $this;
    }

    /**
     * @param string $locale
     *
     * @return null|Media
     */
    public function getMediaWithLocale(string $locale = 'fr'): ?Media
    {
        if (strtolower($locale) === 'fr') {
            return $this->getMedia();
        } elseif (method_exists($this, 'getMedia' . ucfirst($locale))) {
            return $this->{'getMedia' . ucfirst($locale)}();
        }

        return null;
    }
}
