<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 16/05/2017
 * Time: 10:23
 */

namespace Todotoday\CMSBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslation;

/**
 * Class PostTranslation
 *
 * @ORM\Entity()
 * @ORM\Table(
 *     schema="cms",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="lookup_unique_post_translation_idx", columns={
 *         "locale", "object_id", "field"
 * })})
 *
 * @category   Todo-Todev
 * @package    Todotoday\CMSBundle
 * @subpackage Todotoday\CMSBundle\Entity
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class PostTranslation extends AbstractPersonalTranslation
{
    /**
     * @var Post
     *
     * @ORM\ManyToOne(targetEntity="Todotoday\CMSBundle\Entity\Post", inversedBy="translations")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $object;
}
