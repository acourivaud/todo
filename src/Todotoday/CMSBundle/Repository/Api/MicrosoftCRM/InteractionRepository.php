<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 16/04/17
 * Time: 17:35
 */

namespace Todotoday\CMSBundle\Repository\Api\MicrosoftCRM;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiRepository;

/**
 * Class InteractionRepository
 * @package Todotoday\CMSBundle\Repository\Api\MicrosoftCRM
 */
class InteractionRepository extends AbstractApiRepository
{
}
