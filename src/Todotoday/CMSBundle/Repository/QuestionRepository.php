<?php declare(strict_types=1);

namespace Todotoday\CMSBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Todotoday\CMSBundle\Entity\Question;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * QuestionRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class QuestionRepository extends EntityRepository
{
    /**
     * @param Agency $agency
     * @param string $locale
     *
     * @return array|null
     */
    public function findByAgency(Agency $agency, ?string $locale = null): ?array
    {
        $dataAreaId = strtolower($agency->getDataAreaId());

        $qb = $this->createQueryBuilder('q')
            ->where('q.country is null')
            ->orWhere('lower(q.country) like :dataAreaId')
            ->setParameter('dataAreaId', '%' . $dataAreaId . '%');

        if ($locale) {
            $qb = $qb->innerJoin('q.translations', 'qt')
                ->andWhere('qt.locale = :locale')
                ->setParameter('locale', $locale);
        }

        return $qb->distinct()
            ->orderBy('q.position')
            ->getQuery()
            ->getResult();
    }
}
