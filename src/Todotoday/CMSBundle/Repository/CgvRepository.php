<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 24/06/17
 * Time: 04:04
 */

namespace Todotoday\CMSBundle\Repository;

use Doctrine\ORM\Query\Expr;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CMSBundle\Entity\Cgv;
use Todotoday\CoreBundle\Repository\AbstractEntityRepository;

class CgvRepository extends AbstractEntityRepository
{
    /**
     * @param Agency $agency
     *
     * @return Cgv|null
     */
    public function getCgvByAgency(Agency $agency): ?Cgv
    {
        return $this->createQueryBuilder('c')
            ->innerJoin(Agency::class, 'a', Expr\Join::WITH, 'a.cgv = c.id')
            ->where('a = :agency')
            ->setParameter('agency', $agency)
            ->getQuery()->getOneOrNullResult();
    }
}
