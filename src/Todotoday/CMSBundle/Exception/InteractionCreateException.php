<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 13/05/17
 * Time: 16:39
 */

namespace Todotoday\CMSBundle\Exception;

use Throwable;

/**
 * Class InteractionCreateException
 * @package Todotoday\CMSBundle\Exception
 */
class InteractionCreateException extends \Exception
{
    /**
     * InteractionCreateException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct(
        $message = 'Error while sending interaction to CRM',
        $code = 0,
        Throwable $previous =
        null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
