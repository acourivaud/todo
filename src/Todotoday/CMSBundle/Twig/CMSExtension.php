<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 17/01/2017
 * Time: 11:52
 */

namespace Todotoday\CMSBundle\Twig;

use Todotoday\CMSBundle\Services\Markdown;

/**
 * Class CMSExtension
 *
 * @package    Todotoday\CMSBundle
 * @subpackage Todotoday\CMSBundle\Twig
 */
class CMSExtension extends \Twig_Extension
{
    /**
     * @var Markdown
     */
    private $parser;

    /**
     * CMSExtension constructor.
     *
     * @param Markdown $parser
     */
    public function __construct(Markdown $parser)
    {
        $this->parser = $parser;
    }

    /**
     * @return array
     */
    public function getFilters(): array
    {
        return [
            new \Twig_SimpleFilter('markdown', [$this, 'markdownToHtml'], ['is_safe' => ['html']]),
            new \Twig_SimpleFilter('strip_markdown', [$this, 'strip'], ['is_safe' => ['html']]),
        ];
    }

    /**
     * @param string $content
     *
     * @return string
     */
    public function markdownToHtml(string $content): string
    {
        return $this->parser->toHtml($content);
    }

    /**
     * @param string $text
     *
     * @return string
     */
    public function strip($text)
    {
        return $this->parser->strip($text);
    }
}
