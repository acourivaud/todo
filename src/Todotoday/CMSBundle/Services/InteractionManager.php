<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 26/04/17
 * Time: 13:18
 */

namespace Todotoday\CMSBundle\Services;

use Actiane\ApiConnectorBundle\Services\Connections\Microsoft\MicrosoftDynamicsConnection;
use Actiane\MailerBundle\Services\ActianeMailer;
use Carbon\Carbon;
use Doctrine\Common\Util\Inflector;
use Symfony\Component\Translation\TranslatorInterface;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CMSBundle\Entity\Api\MicrosoftCRM\Interaction;
use Todotoday\CMSBundle\Entity\ContactForm;
use Todotoday\CMSBundle\Exception\InteractionCreateException;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\MicrosoftBundle\Entity\MicrosoftDateTimeFormater;

/**
 * Class InteractionManager
 * @package Todotoday\CMSBundle\Services
 */
class InteractionManager
{
    /**
     * @var MicrosoftDynamicsConnection
     */
    private $connection;

    /**
     * @var ActianeMailer
     */
    private $mailer;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * InteractionManager constructor.
     *
     * @param MicrosoftDynamicsConnection $connection
     * @param ActianeMailer               $mailer
     * @param TranslatorInterface         $translator
     */
    public function __construct(
        MicrosoftDynamicsConnection $connection,
        ActianeMailer $mailer,
        TranslatorInterface $translator
    ) {
        $this->connection = $connection;
        $this->mailer = $mailer;
        $this->translator = $translator;
    }

    /**
     * Determine if we send a mail or a user Interaction
     *
     * @param Agency        $agency
     * @param null|Adherent $adherent
     * @param array         $data
     *
     * @return mixed
     * @throws \Twig_Error_Syntax
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Loader
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     * @throws InteractionCreateException
     */
    public function handleSendInteraction(Agency $agency, ?Adherent $adherent, array $data): int
    {
        /** @var ContactForm $contactForm */
        $contactForm = $data['subject'];
        // if we get an adherent we send a user interaction to microsoft CRM
        if ($adherent) {
            $target = $contactForm->getReceiver()->get();
            $ownerTarget = 'getCrm' . Inflector::camelize($target);
            if (!$ownerId = $agency->{$ownerTarget}()) {
                throw new InteractionCreateException();
            }

            $this->createAdherentInteraction(
                $adherent,
                $contactForm->getCategorieTag(),
                $ownerId,
                'Demande de contact depuis le FRONT',
                $data['message']
            );

            return 1;
        }

        return $this->sendEmailToReceiver($agency, $data);
    }

    /**
     * @param Agency $agency
     * @param array  $data
     *
     * @return int
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     * @throws \Twig_Error_Syntax
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Loader
     * @throws \Psr\Log\InvalidArgumentException
     */
    public function sendEmailToReceiver(Agency $agency, array $data): int
    {
        /** @var ContactForm $contactForm */
        $contactForm = $data['subject'];
        $variables = array(
            'agency' => $agency->getName(),
            'lastname' => $data['lastname'],
            'firstname' => $data['firstname'],
            'email' => $data['email'],
            'subject' => $contactForm->getLabel(),
            'content' => $data['message'],
        );

        $bodyTxt =
            $this->mailer->renderTemplate('@TodotodayCMS/ContactMail/ToAdherent/contact_mail.txt.twig', $variables);
        $bodyHtml =
            $this->mailer->renderTemplate('@TodotodayCMS/ContactMail/ToAdherent/contact_mail.html.twig', $variables);
        $this->mailer->createMail(
            $agency->getEmail(),
            $this->translator->trans('contact_email_subject_title', [], 'todotoday')
        );
        $this->mailer->getMessage()->setBody($bodyHtml, 'text/html')->addPart($bodyTxt, 'text/plain');

        return $this->mailer->send();
    }

    /**
     * @param Adherent $adherent
     * @param int      $category
     * @param string   $ownerId
     * @param string   $subject
     * @param string   $content
     *
     * @return Interaction
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    public function createAdherentInteraction(
        Adherent $adherent,
        int $category,
        string $ownerId,
        string $subject,
        string $content
    ): Interaction {
        $interaction = $this->createEntityAdherentInteraction($adherent, $category, $ownerId, $subject, $content);

        $this->connection->persist($interaction)->flush();

        return $interaction;
    }

    /**
     * @param Adherent    $adherent
     * @param Agency      $agency
     * @param string      $content
     * @param \DateTime   $startDateTime
     * @param \DateTime   $endDateTime
     * @param null|string $orderId
     *
     * @return Interaction
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    public function createRDVInteraction(
        Adherent $adherent,
        Agency $agency,
        string $content,
        \DateTime $startDateTime,
        \DateTime $endDateTime,
        ?string $orderId
    ): Interaction {
        $ownerId = $agency->getCrmTeam();
        $interaction = $this->createEntityAdherentInteraction($adherent, 100000006, $ownerId, '100000008', $content);
        $interaction
            ->setApsJourDeLaSemaineSouhaite($this->getJourDeLaSemaineCode($startDateTime))
            ->setApsHeureDeDebutSouhaite($startDateTime)
            ->setApsHeureDeDeFinSouhaite($endDateTime)
            ->setApsCommandeid($orderId);

        $this->connection->persist($interaction)->flush();

        return $interaction;
    }

    /**
     * @param Adherent $adherent
     * @param int      $category
     * @param string   $ownerId
     * @param string   $subject
     * @param string   $content
     *
     * @return Interaction
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    public function createEntityAdherentInteraction(
        Adherent $adherent,
        int $category,
        string $ownerId,
        string $subject,
        string $content
    ): Interaction {
        $interaction = new Interaction();
        $interaction
            ->setApsDateinteraction(new \DateTime('now'))
            ->setApsCategorie($category)
            ->setApsDescriptioninteraction($content)
            ->setApsCanalinteraction('front')
            ->setSubject($subject)
            ->setRegardingobjectidValueGuid($adherent->getCrmIdAdherent(), 'accounts')
            ->setOwnerIdValueGuid($ownerId, 'teams');

        return $interaction;
    }

    /**
     * @param \DateTime $dateTime
     *
     * @return int
     */
    public function getJourDeLaSemaineCode(\DateTime $dateTime): int
    {
        $carbon = Carbon::instance($dateTime);

        return 100000000 + (($carbon->dayOfWeek + 6) % 7);
    }
}
