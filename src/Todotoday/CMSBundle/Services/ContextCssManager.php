<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 20/01/2017
 * Time: 16:05
 */

namespace Todotoday\CMSBundle\Services;

use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Todotoday\CMSBundle\Interfaces\Services\CssTranspilerInterface;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Entity\Style;

class ContextCssManager
{
    /**
     * @var mixed|string
     */
    private $directory;

    /**
     * @var CssTranspilerInterface
     */
    private $scssPhp;

    /**
     * @var string
     */
    private $templateScss;

    /**
     * ContextCssManager constructor.
     *
     * @param CssTranspilerInterface $scssPhp
     * @param string                 $directory
     * @param string                 $template
     */
    public function __construct(CssTranspilerInterface $scssPhp, string $directory, string $template)
    {
        $this->directory = $directory;
        $this->scssPhp = $scssPhp;
        $this->templateScss = $template;
    }

    /**
     * @param string $scss
     * @param Agency $agency
     *
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     */
    public function createCss(string $scss, Agency $agency)
    {
        $fileContent = $this->scssPhp->transpile($scss);
        $fileContent = $this->scssPhp->minify($fileContent);

        $directory = $this->directory . '/' . $agency->getSlug();
        if (!@mkdir($directory, 0777, true) && !is_dir($directory)) {
            throw new FileNotFoundException(
                "The folder $directory can't be found or cannot be created.
             Please check the path or your server's rights"
            );
        }

        $file = fopen($directory . '/template.css', 'wb');
        if (!$file) {
            throw new FileNotFoundException("Can't reach or read the file '$directory/template.css'");
        }

        fwrite($file, $fileContent);
        fclose($file);
    }

    /**
     * @param Agency $agency
     *
     * @return string
     * @throws EntityNotFoundException
     */
    public function mergeDBScss(Agency $agency)
    {

        $scss = '';
        /**
         * @var Style
         */
        if (!$style = $agency->getStyle()) {
            throw new EntityNotFoundException('No Style associated');
        }

        $reflexion = new \ReflectionClass(Style::class);
        foreach ($reflexion->getProperties() as $property) {
            $propertyName = $property->getName();
            if ($propertyName !== 'id' && $propertyName !== 'agency') {
                $method = 'get' . ucfirst($propertyName);
                $scss .= '$' . $propertyName . ': ' . $style->$method() . ';';
            }
        }

        $template = file_get_contents($this->templateScss);
//        var_dump($template);
//        die();
        $scss .= $template;

        return $scss;
    }

    /**
     * @param Agency $agency
     */
    public function removeScss(Agency $agency)
    {
        $directory = $this->directory . '/' . $agency->getSlug();
        unlink($directory . '/template.css');
        rmdir($directory);
    }
}
