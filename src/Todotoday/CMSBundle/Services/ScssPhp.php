<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 20/01/2017
 * Time: 11:42
 */

namespace Todotoday\CMSBundle\Services;

use Leafo\ScssPhp\Compiler;
use Todotoday\CMSBundle\Interfaces\Services\CssTranspilerInterface;

/**
 * Class ScssPhp
 *
 * @package Todotoday\CMSBundle\Services
 */
class ScssPhp implements CssTranspilerInterface
{
    /**
     * @param string $css
     *
     * @return string
     */
    public function minify(string $css): string
    {
        $css = trim($css);
        $css = str_replace('\r\n', '\n', $css);

        $search = array('/\/\*[^!][\d\D]*?\*\/|\t+/', '/\s+/', '/\}\s+/');
        $replace = array(null, ' ', '}\n');
        $css = preg_replace($search, $replace, $css);

        $search = array(
            '/;[\s+]/',
            '/[\s+];/',
            '/\s+\{\\s+/',
            '/\\:\s+\\#/',
            '/,\s+/i',
            '/\\:\s+\\\'/i',
            '/\\:\s+([0-9]+|[A-F]+)/i',
            '/\{\\s+/',
            '/;}/',
        );
        $replace = array(';', ';', '{', ':#', ',', ':\'', ':$1', '{', '}');
        $css = preg_replace($search, $replace, $css);
        $css = str_replace("\n", null, $css);

        return $css;
    }

    /**
     * @param string $scss
     *
     * @return string
     */
    public function transpile(string $scss): string
    {
        $compiler = new Compiler();

        return $compiler->compile($scss);
    }
}
