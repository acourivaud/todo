<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 17/01/2017
 * Time: 11:45
 */

namespace Todotoday\CMSBundle\Services;

class Markdown
{
    private $parser;

    /**
     * Markdown constructor.
     */
    public function __construct()
    {
        $this->parser = new \Parsedown();
    }

    /**
     * @param string $text
     *
     * @return string
     */
    public function strip($text)
    {
        return html_entity_decode(strip_tags($this->toHtml($text)), ENT_QUOTES | ENT_HTML5, 'UTF-8');
    }

    /**
     * @param string $text
     *
     * @return string
     */
    public function toHtml(string $text): string
    {
        return $this->parser->text($text);
    }
}
