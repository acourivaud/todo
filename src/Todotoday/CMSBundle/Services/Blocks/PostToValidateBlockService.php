<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 16/03/2017
 * Time: 15:49
 */

namespace Todotoday\CMSBundle\Services\Blocks;

use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\BlockBundle\Block\Service\AbstractBlockService;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Response;
use Todotoday\CMSBundle\Admin\PostAdmin;
use Todotoday\CMSBundle\Repository\PostRepository;

/**
 * Class PostToValidateBlockService
 *
 * @category   Todo-Todev
 * @package    Todotoday\CMSBundle\Services\Blocks
 * @subpackage Todotoday\CMSBundle\Services\Blocks
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class PostToValidateBlockService extends AbstractBlockService
{
    /**
     * @var PostRepository
     */
    protected $postRepository;

    /**
     * @var PostAdmin
     */
    protected $postAdmin;

    /**
     * {@inheritdoc}
     *
     * @param PostRepository $postRepository
     */
    public function __construct(
        $name = null,
        EngineInterface $templating = null,
        PostRepository $postRepository,
        PostAdmin $postAdmin
    ) {
        parent::__construct($name, $templating);

        $this->postRepository = $postRepository;
        $this->postAdmin = $postAdmin;
    }

    /**
     * {@inheritdoc}
     * @throws \InvalidArgumentException
     * @throws \LogicException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        return $this->renderResponse(
            '@TodotodayCMS/Blocks/post_to_validate.html.twig',
            array(
                'block_context' => $blockContext,
                'block' => $blockContext->getBlock(),
                'postAdmin' => $this->postAdmin,
            ),
            $response
        );
    }
}
