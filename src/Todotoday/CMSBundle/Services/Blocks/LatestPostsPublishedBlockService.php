<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 16/03/2017
 * Time: 17:00
 */

namespace Todotoday\CMSBundle\Services\Blocks;

use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\BlockBundle\Block\Service\AbstractBlockService;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Response;
use Todotoday\CMSBundle\Repository\LinkPostAgencyRepository;
use Todotoday\CMSBundle\Repository\PostRepository;

/**
 * Class LatestPostsPublishedBlockService
 *
 * @category   Todo-Todev
 * @package    Todotoday\CMSBundle
 * @subpackage Todotoday\CMSBundle\Services\Blocks
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class LatestPostsPublishedBlockService extends AbstractBlockService
{
    /**
     * @var LinkPostAgencyRepository
     */
    private $linkPostAgencyRepository;

    /**
     * {@inheritdoc}
     *
     * @param PostRepository $postRepository
     */
    public function __construct(
        $name = null,
        EngineInterface $templating = null,
        LinkPostAgencyRepository $linkPostAgencyRepository
    ) {
        parent::__construct($name, $templating);

        $this->linkPostAgencyRepository = $linkPostAgencyRepository;
    }

    /**
     * {@inheritdoc}
     * @throws \InvalidArgumentException
     * @throws \LogicException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        return $this->renderResponse(
            '@TodotodayCMS/Blocks/latest_posts.html.twig',
            array(
                'block_context' => $blockContext,
                'block' => $blockContext->getBlock(),
            ),
            $response
        );
    }
}
