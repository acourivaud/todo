<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 24/06/17
 * Time: 04:13
 */

namespace Todotoday\CoreBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Todotoday\CMSBundle\Entity\Cgv;

class LoadCvgData extends Fixture
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     * @SuppressWarnings(PHPMD)
     */
    public function load(ObjectManager $manager): void
    {
        // @codingStandardsIgnoreStart
        $cgv = (new Cgv())
            ->setModel('cgv1')
            ->setContent(
                <<<EOF
CONTRAT D’ADHESION ET CONDITIONS GENERALES

Article 1 : Objet
La société TO DO TODAY propose à ses Clients (entreprises ou propriétaires d’immeubles) une offre de services de conciergeries sur mesure via une implantation physique dans les locaux de ses Clients.

Ces services sont exclusivement destinés aux Adhérents des conciergeries, qu’ils soient salariés ou collaborateurs des Clients entreprises, ou salariés et collaborateurs des entreprises locataires des Clients propriétaires d’immeubles de bureaux et à leurs co- Adhérents tels que désignés dans la fiche d’adhésion, étant précisé que ne peuvent être désignés comme co-Adhérents, que le conjoint, concubin ou pacs, et le(s) enfant(s) majeurs de l’Adhérent.

Dans certains cas, des prestataires externes au Client peuvent être habilités par ce dernier à utiliser tout ou partie des Services. Le cas échéant, ces accès et spécificités d’utilisation sont décrites dans les documents d’information remis par le concierge.

Afin de simplifier et de sécuriser l’utilisation de ses services par les Adhérents, TO DO TODAY se substitue à ses fournisseurs et prestataires, devenant par là même leur interlocuteur unique, tant pour la prise de commande que pour le suivi et la facturation.

Article 2 : Prise d’effet du Contrat
Le Contrat d’adhésion prend effet au retour à TO DO TODAY du Contrat d’adhésion dûment complété, signé par l’Adhérent et accompagné des pièces justificatives demandées. 
L’adhésion est confirmée par TO DO TODAY par l’envoi d’un mail dès validation du Contrat.

Article 3 : Durée du Contrat
Le Contrat d’adhésion est conclu pour une période de douze (12) mois renouvelable tacitement, sauf dénonciation expresse du Contrat d’adhésion par l’Adhérent par toute voie de communication écrite, au moins un (1) mois avant la date anniversaire du Contrat.
Les cas de résiliation anticipée du Contrat font l’objet de l’article 17 des présentes.

Article 4 : Accès au service
L’Adhérent dispose de différents modes d’accès au service TO DO TODAY : physiquement à la conciergerie, par téléphone et par mail.

Afin d’assurer la sécurité des Adhérents, le système informatique TO DO TODAY génère un numéro d’adhérent lié à chaque Contrat.

Seuls les co-Adhérents expressément désignés dans la fiche d’adhésion incluse aux présentes sont habilités à passer une commande sur le compte de l’Adhèrent qui se porte garant de ses co-Adhérents. Pour être identifiés et par mesure de sécurité, les co-Adhérents devront répondre à la question de sécurité également stipulée dans la fiche d’adhésion.

Article 5 : Commande 

5.1 Modalités de la prise de commande :
La commande peut être passée selon les modes d’accès décrits à l’article 4 ci-dessus.
Les informations relatives à la commande (nature de la prestation, prix, modalités de livraison, heure de rendez-vous par exemple) sont saisies informatiquement par l’équipe de TO DO TODAY.

5.2 Confirmation de la commande
Un bordereau de confirmation reprenant les informations relatives à la commande est envoyé à l’Adhérent par mail une fois la disponibilité des produits commandés validée par le Concierge ou, pour les prestations nécessitant le passage de l’Adhérent à la conciergerie (pressing, cordonnerie par exemple), une fois le contrôle des biens confiés exécuté par le concierge.
La confirmation par TO DO TODAY de la commande atteste de la disponibilité du produit ou du service commandé. Sont notamment précisés dans la confirmation de commande le prix à payer au titre du produit ou du service commandé, l’heure de livraison ou de réalisation de la prestation et le délai maximum d’annulation de la commande.

5.3 Modification d’une commande
Pour l’ensemble des commandes confirmées, tout désaccord de la part de l’Adhérent sur l’une ou l’ensemble des informations contenues dans le bordereau de confirmation doit, dans un délai maximal de deux heures, être porté à l’attention des membres de la conciergerie de TO DO TODAY qui s’attacheront à rectifier la commande dans les meilleurs délais. Passé ce délai de deux heures, TO DO TODAY fera ses meilleurs efforts pour vous satisfaire mais ne peut vous garantir de pouvoir enregistrer votre modification de commande.

5.4 Annulation d’une commande
Toute commande d’un produit en stock chez TO DO TODAY peut être annulée à tout moment, sans pénalité.
Dans l’hypothèse où l’Adhérent annule la commande d’un produit ou service pour lequel TO DO TODAY se fournit auprès d’un fournisseur ou prestataire externe, et si cette annulation intervient au-delà du délai inscrit sur le bordereau de confirmation, la commande est réputée consommée et lui est facturée.

En cas d’annulation par l’Adhérent d’une commande de prestation de service (hors courses de proximité)  moins de 12 heures avant l’heure prévue d’exécution de la prestation telle qu’indiquée sur le bordereau de confirmation, la prestation est réputée consommée et lui est facturée.

TO DO TODAY s’efforce de maintenir des listes d’attentes permettant de transférer le rendez-vous annulé à un autre Adhérent.  Dans ce cas seulement, la prestation annulée n’est pas facturée à l’Adhérent.

Article 6: Disponibilité
Certains services proposés par TO DO TODAY sont effectués par des prestataires de services externes avec lesquels TO DO TODAY a contracté.

Dans certains cas, des ruptures ou indisponibilités de la part de nos prestataires peuvent avoir lieu et ne sauraient être imputées à TO DO TODAY.

TO DO TODAY fera son meilleur effort pour que les fournisseurs et prestataires soient en mesure d’honorer toutes les commandes des Adhérents.  Des indications sur la disponibilité des produits et services sont fournies lors de l’émission du bordereau de validation de la commande.  Les modalités de cette confirmation sont détaillées à l’article 5.2 ci-dessus.

Article 7 : Prix
Les prix proposés par TO DO TODAY pour les produits ou services sont indiqués en euro toutes taxes comprises, au taux de TVA en vigueur à la date de commande des produits et service. 

Si le taux de la TVA venait à être modifié, que ce soit à la hausse ou à la baisse, le changement pourra immédiatement être répercuté sur le prix des produits et services, même si ce changement intervient après la confirmation de la commande par l’Adhérent.

TO DO TODAY se réserve le droit de modifier ses prix à tout moment mais les produits et services seront facturés sur la base des tarifs affichés au moment de la passation de la commande par l’Adhérent.

Article 8 : Livraison
Les produits ou services choisis sont livrés à l’espace réservé à TO DO TODAY au sein des locaux du Client dans les délais indiqués sur la confirmation de commande.
Les délais de livraison indiqués dans la confirmation de commande correspondent aux délais maximums de traitement et de livraison pour les produits et services commandés.

En cas de dépassement du délai de livraison, TO DO TODAY s’engage à informer immédiatement l’Adhérent et à lui proposer soit un nouveau délai, soit l’annulation et le remboursement de sa commande.  Le remboursement s’effectue par le biais d’un crédit porté au compte de l’Adhérent.

Article 9 – Garanties

9.1 Garantie légale
Les Adhérents bénéficient de la garantie légale des vices cachés sur les produits livrés.

9.2 Délai de rétractation
L’Adhérent dispose d’un délai légal de quatorze (14) jours francs à compter de la livraison de sa commande pour faire retour à TO DO TODAY du produit dans son emballage d’origine en parfait état.  Le retour devra se faire au comptoir de la conciergerie installée au sein de l’entreprise Cliente.

Le droit de retour ne pourra être exercé pour :
Les services qui auront déjà reçu un début d’exécution avec l’accord de l’Adhérent avant la fin du délai de quatorze jours (le pressing par exemple) ;
Les produits alimentaires pour lesquels la date de péremption est arrivée à échéance ou les produits ayant une très courte durée de vie, comme, notamment, les fleurs ;
Les articles retournés incomplets, abîmés, endommagés ou salis.

Article 10 : Dispositions Financières
Les commandes de produits et services effectuées auprès de TO DO TODAY et délivrées sur le site font l’objet d’une facture mensuelle unique payable par prélèvement automatique. Le relevé d’opérations reprenant l’ensemble des prestations et produits commandés dans le mois est envoyé à la fin du mois par mail.

Le prélèvement s’effectue dans les 48 à 72 heures suivantes, terme échu.

Le cas échéant, le montant de la cotisation annuelle est indiqué dans les documents d’information remis à l’Adhèrent par le concierge.   Celle-ci est incluse dans la première facture mensuelle suivant la date d’adhésion ou de renouvellement du Contrat. 

Dans le cas où un titre de paiement émis au profit de TO DO TODAY ne serait pas honoré, les sommes dues sont majorées des frais divers liés à l’impayé que TO DO TODAY a été contrainte d’engager et sont refacturées à l’Adhérent (notamment les frais de rejet de prélèvement automatique).

Les commandes de services effectuées auprès de prestataires externes par le biais du service de Conciergerie à distance (services à la personne, artisans…) feront l’objet d’un règlement à la prestation, effectué directement auprès du prestataire délivrant le service.

Pour les prestataires agréés par les pouvoirs publics, nous vous rappelons que certaines prestations pourront bénéficier d'un taux de TVA réduit. 
Le taux applicable sera celui en vigueur pour le service concerné à la date de commande.
Le cas échéant, une attestation fiscale est envoyée à l’Adhérent par le prestataire au plus tard le 31 janvier de l’année suivant la date d’exécution du service.  Cette attestation permet à l’Adhérent de faire valoir son droit à une réduction d’impôt sur le revenu, dans les limites et conditions fixées par la loi fiscale en vigueur.

Article 11 : Changement de domiciliation bancaire
En cas de changement de coordonnées bancaires, l’Adhérent adressera sans délai à TO DO TODAY un nouveau mandat portant autorisation de prélèvement accompagné d’un IBAN.
Les impayés résultant du défaut de communication de la nouvelle domiciliation bancaire, majorés des frais liés à l’impayé que TO DO TODAY aura été contrainte d’engager seront refacturés à l’Adhérent (notamment les frais de rejet de prélèvement automatique).

Article 12 : Obligations de TO DO TODAY
TO DO TODAY s’engage à tout mettre en œuvre pour assurer la permanence, la continuité et la qualité du service fourni à ses Adhérents, conformément à l’article 6 des présentes.

La responsabilité de TO DO TODAY ne peut être engagée :
En cas de mauvaise utilisation du service par l’Adhérent (adresse d’intervention erronée, mauvaise spécification du mode de livraison…) ;
En cas de non-respect par l’Adhérent de ses obligations contractuelles ;
En cas de force majeure au sens de la jurisprudence française.

Article 13 : Obligations de l’Adhérent
L’Adhérent est responsable du paiement de l’ensemble des sommes dues au titre du Contrat d’adhésion comme de l’exécution des obligations souscrites au titre du Contrat.

L’Adhérent s’engage à prévenir TO DO TODAY dans un délai maximum de sept (7) jours ouvrables de tout changement ou modification relatifs à la fiche d’adhésion annexée au présent Contrat.

De même, l’Adhérent s’engage à informer TO DO TODAY de son départ du site ou de son entreprise afin que TO DO TODAY puisse procéder à la clôture de son compte, dans les conditions prévues à l’article 17.

Cette obligation est justifiée par la nature même des conditions d’organisation et de livraison des prestations de TO DO TODAY. A défaut, l’Adhérent ne pourra s’en prévaloir auprès de TO DO TODAY pour quelque cause que ce soit.

L’Adhérent s’interdit expressément de solliciter et/ou d’utiliser, directement ou indirectement, les services des prestataires et fournisseurs de TO DO TODAY sans en référer à TO DO TODAY.

Article 14: Responsabilité

14.1 Fiches produits
TO DO TODAY présente dans son catalogue papier et Intranet les caractéristiques essentielles des produits et services proposés, conformément à l'article L 111-1 du Code de la Consommation.

Les produits et services proposés sont conformes à la législation française en vigueur.

Les fiches de présentation des produits et services sont établies à partir des informations données par les fournisseurs et prestataires de services, partenaires de TO DO TODAY. Les photographies, graphismes et les textes illustrant les produits et services présentés ont valeur indicative et non contractuelle. De ce fait, la responsabilité de TO DO TODAY ne saurait être engagée et la validité d’une commande passée et réalisée ne saurait être contestée, en cas d’erreur ou d’imprécision sur ces éléments de présentation.

14.2 Force majeure
La responsabilité de TO DO TODAY, ne saurait être engagée, en cas de retard dans l’exécution du Contrat ou en cas d’inexécution du Contrat, en raison d’un cas de force majeure.

Si l’événement de force majeure retarde ou rend impossible l’exécution de la commande pendant une durée excédant 30 jours, la commande pourra être annulée et TO DO TODAY remboursera, le cas échéant l’Adhérent par un crédit porté au compte de l’Adhérent.

Article 15 : Propriété Intellectuelle 
Tous les textes, commentaires, ouvrages, illustrations et images reproduits dans les catalogues papier, applications mobiles et sites web de TO DO TODAY, qu’ils soient la propriété ou non de TO DO TODAY, sont protégés au titre de la propriété intellectuelle pour la durée de protection des droits et  pour le monde entier.

A ce titre et conformément aux dispositions du code de la propriété intellectuelle, seule l'utilisation pour un usage privé, réduite au cercle de famille (sous réserve de dispositions différentes voire plus restrictives du code de la propriété intellectuelle) et la reproduction (impression, téléchargement) pour un usage strictement personnel sont autorisés.

A défaut d'autorisation préalable communiquée par écrit par TO DO TODAY ou du titulaire des droits, toute autre utilisation est constitutive de contrefaçon sanctionnée par le code de la propriété intellectuelle.

Toute reproduction totale ou partielle du catalogue de TO DO TODAY est strictement interdite. 

TO DO TODAY et son logo sont des marques déposées et protégées à ce titre par le droit des marques. Elles sont la propriété exclusive de TO DO TODAY.

Article 16 : Protection des informations nominatives
Les informations communiquées par l’Adhérent dans le cadre du Contrat d’adhésion et de ses commandes ne seront transmises à aucun tiers en dehors des fournisseurs et prestataires de services de TO DO TODAY. Ces informations seront considérées par TO DO TODAY, ses fournisseurs et ses prestataires comme strictement confidentielles. Elles ne seront utilisées par les services internes de TO DO TODAY et ses fournisseurs et prestataires que pour le traitement des commandes et pour renforcer et personnaliser la communication et l'offre des produits et services réservée aux Adhérents de TO DO TODAY.

Nonobstant les dispositions des alinéas précédents, l’Adhérent autorise expressément TO DO TODAY à informer son employeur ou bailleur dans le cas suivant : non-paiement d’une ou plusieurs facture(s) malgré une lettre de relance ; dans ce cas, cette information portera sur les nom, prénom de l’Adhérent et les date et montant de l’impayé.

Pour les besoins des présentes, ne seront pas assimilées à des tiers, les entités ou personnes reprenant l’activité de TO DO TODAY à la suite d’une cession ou d’un transfert.

Conformément à la loi informatique et libertés du 6 janvier 1978, l’Adhérent dispose d'un droit d'accès, de rectification, et d'opposition aux données personnelles le concernant.  Pour cela il lui suffit d’en faire la demande à TO DO TODAY par mail en indiquant son nom, prénom et adresse (contact@todotoday.com).

Article 17 : Fin du Contrat d’adhésion – Résiliation
TO DO TODAY se réserve la faculté de résilier le Contrat d’adhésion de plein droit et à tout moment, après une mise en demeure de sept (7) jours restée sans effet, pour les cas de violation par l’Adhérent de ses obligations ou de non-paiement d’une seule des échéances mensuelles au titre d’une de ses commandes.

En outre, TO DO TODAY procédera à la résiliation immédiate et de plein droit de l’adhésion : 
en cas de rupture du Contrat de prestations de services conclu avec la société Cliente pour quelque cause que ce soit ;
en cas de rupture du Contrat de collaboration ou de travail de l’Adhérent avec son employeur, dès le jour effectif de ladite rupture ;
en cas de départ de l’Adhérent du Site, sauf à ce que son employeur ait spécifiquement autorisé le maintien de son adhésion.

Il est expressément convenu qu’à compter de la date de résiliation effective de l’adhésion, l’Adhérent n’aura plus accès aux produits ou services proposés par TO DO TODAY. La cotisation annuelle éventuellement acquittée par l’Adhèrent restera acquise à TO DO TODAY. 

TO DO TODAY s’engage à exécuter la commande de l’Adhérent si cette dernière lui est parvenue avant la date effective de résiliation, même si l’exécution doit intervenir après la date effective de résiliation. 

Article 18 : Modification des Conditions Générales
Compte tenu des évolutions possibles des services, TO DO TODAY se réserve le droit d’adapter ou de modifier à tout moment les présentes Conditions Générales,  en en informant l’Adhérent qui peut par ailleurs les consulter simplement, librement et à tout moment en s’adressant à la Conciergerie. 
L’Adhérent pourra résilier librement son Contrat d’adhésion en cas de désaccord.

Article 19 : Différend
Le présent Contrat d’adhésion est soumis au droit français.
EOF
            );
        $manager->persist($cgv);
        $cgv = (new Cgv())
            ->setModel('cgv2')
            ->setContent(
                <<<EOF
Contrat d’adhesion ET CONDITIONS GENERALES DE VENTE AUX UTILISATEURS 


Vous avez souhaité devenir adhérent de la conciergerie TO DO TODAY et nous vous en remercions. Afin de mieux vous connaître, nous vous remercions de bien vouloir :

(i) Prendre connaissance des Conditions Générales de Vente et en accepter les termes ;
(ii) Remplir et signer la fiche d’adhésion ci-jointe. Les informations que vous y indiquerez seront utilisées par TO DO TODAY pour personnaliser votre compte Adhérent, vous faire gagner du temps lorsque vous passerez une commande, et répondre donc plus efficacement à vos attentes ;
(iii) Remplir et signer le mandat de prélèvement annexé au présent Contrat et d’y joindre un IBAN le cas échéant, afin de valider votre adhésion.

L’ensemble de votre dossier d’adhésion doit être remis à la Conciergerie. 

Nous vous rappelons que la signature de ces documents a pour objet de garantir votre sécurité et d’assurer le respect de la plus stricte confidentialité concernant vos données personnelles, tant vis-à-vis des tiers que de votre employeur et/ou de votre bailleur.

N’hésitez pas à vous adresser à votre Concierge si vous avez la moindre question concernant ces documents.

CONTRAT D’ADHESION ET CONDITIONS GENERALES

Article 1 : Objet
La société TO DO TODAY propose à ses Clients (entreprises ou propriétaires d’immeubles) une offre de services de conciergeries sur mesure via une implantation physique dans les locaux de ses Clients et/ou à distance.

Ces services sont exclusivement destinés aux Adhérents des conciergeries, qu’ils soient salariés ou collaborateurs des Clients entreprises, ou salariés et collaborateurs des entreprises locataires des Clients propriétaires d’immeubles de bureaux et à leurs co-Adhérents tels que désignés dans la fiche d’adhésion, étant précisé que ne peuvent être désignés comme co-Adhérents, que le conjoint, concubin ou pacs, et le(s) enfant(s) majeurs de l’Adhérent.

Dans certains cas, des prestataires externes au Client peuvent être habilités par ce dernier à utiliser tout ou partie des Services. Le cas échéant, ces accès et spécificités d’utilisation sont décrites dans les documents d’information remis par le concierge.

Afin de simplifier et de sécuriser l’utilisation de ses services par les Adhérents, TO DO TODAY se substitue à ses fournisseurs et prestataires, devenant par là même leur interlocuteur unique, tant pour la prise de commande que pour le suivi et la facturation.

Article 2 : Prise d’effet du Contrat
Le Contrat d’adhésion prend effet au retour à TO DO TODAY du Contrat d’adhésion dûment complété, signé par l’Adhérent et accompagné des pièces justificatives demandées. 
L’adhésion est confirmée par TO DO TODAY par l’envoi d’un mail dès validation du Contrat.

Article 3 : Durée du Contrat
Le Contrat d’adhésion est conclu pour une période de douze (12) mois renouvelable tacitement, sauf dénonciation expresse du Contrat d’adhésion par l’Adhérent par toute voie de communication écrite, au moins un (1) mois avant la date anniversaire du Contrat.
Les cas de résiliation anticipée du Contrat font l’objet de l’article 17 des présentes.

Article 4 : Accès au service
L’Adhérent dispose de différents modes d’accès au service TO DO TODAY : physiquement au comptoir de la conciergerie TO DO TODAY installé dans son entreprise, par téléphone, par mail, par portail web et/ou par le biais d’une application mobile.

Afin d’assurer la sécurité des Adhérents, le système informatique TO DO TODAY génère un numéro d’adhérent lié à chaque Contrat ainsi qu’un mot de passe qui doit être utilisé à chaque commande passée par le portail ou l’application mobile. L’Adhérent accepte de prendre soin de ce mot de passe et de ne le confier qu’aux co-Adhérents expressément désignés dans la fiche d’adhésion incluse aux présentes et dont il se porte garant. Pour être identifiés et par mesure de sécurité, les co-Adhérents utilisant les services de la conciergerie sur site devront répondre à la question de sécurité également stipulée dans la fiche d’adhésion.

La bonne prise en charge de la commande par la Conciergerie est confirmée à l’Adhérent par mail, une fois le contrôle des articles effectué par le concierge.  
L’Adhérent est informé que la responsabilité de TO DO TODAY n’est engagée qu’à compter de la date et heure d’envoi du mail de confirmation de commande.


Article 5 : Commande 

5.1 Modalités de la prise de commande :
La commande peut être passée selon les modes d’accès décrits à l’article 4 ci-dessus.
Les informations relatives à la commande (nature de la prestation, prix, modalités de livraison, heure de rendez-vous par exemple) sont saisies informatiquement par l’équipe de TO DO TODAY ou par l’Adhérent directement dans le portail et/ou dans l’application mobile.

5.2 Confirmation de la commande
Un bordereau de confirmation reprenant les informations relatives à la commande est envoyé à l’Adhérent par mail ou mis à disposition sur l’application SmartPhone une fois la disponibilité des produits commandés validée par le Concierge ou, pour les prestations nécessitant le passage de l’Adhérent à la conciergerie (pressing, cordonnerie par exemple), une fois le contrôle des biens confiés exécuté par le concierge.
La confirmation par TO DO TODAY de la commande atteste de la disponibilité du produit ou du service commandé. Sont notamment précisés dans la confirmation de commande le prix à payer au titre du produit ou du service commandé, l’heure de livraison ou de réalisation de la prestation et le délai maximum d’annulation de la commande.

5.3 Modification d’une commande
Les commandes exprimées via le portail et/ou l’application mobile ne peuvent être modifiées en ligne et nécessitent l’intervention du Concierge.
Pour l’ensemble des commandes confirmées, tout désaccord de la part de l’Adhérent sur l’une ou l’ensemble des informations contenues dans le bordereau de confirmation doit, dans un délai maximal de deux heures, être porté à l’attention des membres de la conciergerie de TO DO TODAY qui s’attacheront à rectifier la commande dans les meilleurs délais. Passé ce délai de deux heures, TO DO TODAY fera ses meilleurs efforts pour vous satisfaire mais ne peut vous garantir de pouvoir enregistrer votre modification de commande.

5.4 Annulation d’une commande
Toute commande d’un produit en stock chez TO DO TODAY peut être annulée à tout moment, sans pénalité.
Dans l’hypothèse où l’Adhérent annule la commande d’un produit ou service pour lequel TO DO TODAY se fournit auprès d’un fournisseur ou prestataire externe, et si cette annulation intervient au-delà du délai inscrit sur le bordereau de confirmation, la commande est réputée consommée et lui est facturée.

En cas d’annulation par l’Adhérent d’une commande de prestation de service (hors courses de proximité)  moins de 12 heures avant l’heure prévue d’exécution de la prestation telle qu’indiquée sur le bordereau de confirmation, la prestation est réputée consommée et lui est facturée.

TO DO TODAY s’efforce de maintenir des listes d’attentes permettant de transférer le rendez-vous annulé à un autre Adhérent.  Dans ce cas seulement, la prestation annulée n’est pas facturée à l’Adhérent.

Article 6: Disponibilité
Certains services proposés par TO DO TODAY sont effectués par des prestataires de services externes avec lesquels TO DO TODAY a contracté.

Dans certains cas, des ruptures ou indisponibilités de la part de nos prestataires peuvent avoir lieu et ne sauraient être imputées à TO DO TODAY.

TO DO TODAY fera son meilleur effort pour que les fournisseurs et prestataires soient en mesure d’honorer toutes les commandes des Adhérents.  Des indications sur la disponibilité des produits et services sont fournies lors de l’émission du bordereau de validation de la commande.  Les modalités de cette confirmation sont détaillées à l’article 5.2 ci-dessus.

Article 7 : Prix
Les prix proposés par TO DO TODAY pour les produits ou services sont indiqués en euro toutes taxes comprises, au taux de TVA en vigueur à la date de commande des produits et service. 

Si le taux de la TVA venait à être modifié, que ce soit à la hausse ou à la baisse, le changement pourra immédiatement être répercuté sur le prix des produits et services, même si ce changement intervient après la confirmation de la commande par l’Adhérent.

TO DO TODAY se réserve le droit de modifier ses prix à tout moment mais les produits et services seront facturés sur la base des tarifs affichés au moment de la passation de la commande par l’Adhérent.

Article 8 : Livraison
Les produits ou services choisis sont livrés à l’espace réservé à TO DO TODAY au sein des locaux du Client dans les délais indiqués sur la confirmation de commande.
Les délais de livraison indiqués dans la confirmation de commande correspondent aux délais maximums de traitement et de livraison pour les produits et services commandés.

En cas de dépassement du délai de livraison, TO DO TODAY s’engage à informer immédiatement l’Adhérent et à lui proposer soit un nouveau délai, soit l’annulation et le remboursement de sa commande.  Le remboursement s’effectue par le biais d’un crédit porté au compte de l’Adhérent.

Article 9 – Garanties

9.1 Garantie légale
Les Adhérents bénéficient de la garantie légale des vices cachés sur les produits livrés.

9.2 Délai de rétractation
L’Adhérent dispose d’un délai légal de quatorze (14) jours francs à compter de la livraison de sa commande pour faire retour à TO DO TODAY du produit dans son emballage d’origine en parfait état.  Le retour devra se faire au comptoir de la conciergerie installée au sein de l’entreprise Cliente.

Le droit de retour ne pourra être exercé pour :
Les services qui auront déjà reçu un début d’exécution avec l’accord de l’Adhérent avant la fin du délai de quatorze jours (le pressing par exemple) ;
Les produits alimentaires pour lesquels la date de péremption est arrivée à échéance ou les produits ayant une très courte durée de vie, comme, notamment, les fleurs ;
Les articles retournés incomplets, abîmés, endommagés ou salis.

Article 10 : Dispositions Financières
Les commandes de produits et services effectuées auprès de TO DO TODAY et délivrées sur le site font l’objet d’une facture mensuelle unique payable par prélèvement automatique. Le relevé d’opérations reprenant l’ensemble des prestations et produits commandés dans le mois est envoyé à la fin du mois par mail.

Le prélèvement s’effectue dans les 48 à 72 heures suivantes, terme échu.

Le cas échéant, le montant de la cotisation annuelle est indiqué dans les documents d’information remis à l’Adhèrent par le concierge.   Celle-ci est incluse dans la première facture mensuelle suivant la date d’adhésion ou de renouvellement du Contrat. 
Dans le cas où un titre de paiement émis au profit de TO DO TODAY ne serait pas honoré, les sommes dues sont majorées des frais divers liés à l’impayé que TO DO TODAY a été contrainte d’engager et sont refacturées à l’Adhérent (notamment les frais de rejet de prélèvement automatique).

Les commandes de services effectuées auprès de prestataires externes par le biais du service de Conciergerie à distance (services à la personne, artisans…) feront l’objet d’un règlement à la prestation, effectué directement auprès du prestataire délivrant le service.

Pour les prestataires de services à la personne agréés par les pouvoirs publics, nous vous rappelons que certaines prestations pourront bénéficier d'un taux de TVA réduit. 
Le taux applicable sera celui en vigueur pour le service concerné à la date de commande.
Une attestation fiscale est envoyée à l’Adhérent par le prestataire au plus tard le 31 janvier de l’année suivant la date d’exécution du service.  Cette attestation permet à l’Adhérent de faire valoir son droit à une réduction d’impôt sur le revenu, dans les limites et conditions fixées par la loi fiscale en vigueur.

Article 11 : Changement de domiciliation bancaire
En cas de changement de coordonnées bancaires, l’Adhérent adressera sans délai à TO DO TODAY un nouveau mandat portant autorisation de prélèvement accompagné d’un IBAN.
Les impayés résultant du défaut de communication de la nouvelle domiciliation bancaire, majorés des frais liés à l’impayé que TO DO TODAY aura été contrainte d’engager, seront refacturés à l’Adhérent (notamment les frais de rejet de prélèvement automatique).

Article 12: Obligations de TO DO TODAY
TO DO TODAY s’engage à tout mettre en œuvre pour assurer la permanence, la continuité et la qualité du service fourni à ses Adhérents, conformément à l’article 6 des présentes.

La responsabilité de TO DO TODAY ne peut être engagée :
En cas de mauvaise utilisation du service par l’Adhérent (adresse d’intervention erronée, mauvaise spécification du mode de livraison…) ;
En cas de non-respect par l’Adhérent de ses obligations contractuelles ;
En cas de force majeure au sens de la jurisprudence française.

Article 13 : Obligations de l’Adhérent
L’Adhérent est responsable du paiement de l’ensemble des sommes dues au titre du Contrat d’adhésion comme de l’exécution des obligations souscrites au titre du Contrat.

L’Adhérent s’engage à prévenir TO DO TODAY dans un délai maximum de sept (7) jours ouvrables de tout changement ou modification relatifs à la fiche d’adhésion annexée au présent Contrat.

De même, l’Adhérent s’engage à informer TO DO TODAY de son départ du site ou de son entreprise afin que TO DO TODAY puisse procéder à la clôture de son compte, dans les conditions prévues à l’article 17.

Cette obligation est justifiée par la nature même des conditions d’organisation et de livraison des prestations de TO DO TODAY. A défaut, l’Adhérent ne pourra s’en prévaloir auprès de TO DO TODAY pour quelque cause que ce soit.

L’Adhérent s’interdit expressément de solliciter et/ou d’utiliser, directement ou indirectement, les services des prestataires et fournisseurs de TO DO TODAY sans en référer à TO DO TODAY.

Article 14: Responsabilité

14.1 Fiches produits
TO DO TODAY présente dans son catalogue papier et Intranet les caractéristiques essentielles des produits et services proposés, conformément à l'article L 111-1 du Code de la Consommation.

Les produits et services proposés sont conformes à la législation française en vigueur.

Les fiches de présentation des produits et services sont établies à partir des informations données par les fournisseurs et prestataires de services, partenaires de TO DO TODAY. Les photographies, graphismes et les textes illustrant les produits et services présentés ont valeur indicative et non contractuelle. De ce fait, la responsabilité de TO DO TODAY ne saurait être engagée et la validité d’une commande passée et réalisée ne saurait être contestée, en cas d’erreur ou d’imprécision sur ces éléments de présentation.

14.2 Force majeure
La responsabilité de TO DO TODAY, ne saurait être engagée, en cas de retard dans l’exécution du Contrat ou en cas d’inexécution du Contrat, en raison d’un cas de force majeure.

Si l’événement de force majeure retarde ou rend impossible l’exécution de la commande pendant une durée excédant 30 jours, la commande pourra être annulée et TO DO TODAY remboursera, le cas échéant l’Adhérent par un crédit porté au compte de l’Adhérent.

Article 15 : Propriété Intellectuelle 
Tous les textes, commentaires, ouvrages, illustrations et images reproduits dans les catalogues papier, applications mobiles et sites web de TO DO TODAY, qu’ils soient la propriété ou non de TO DO TODAY, sont protégés au titre de la propriété intellectuelle pour la durée de protection des droits et  pour le monde entier.

A ce titre et conformément aux dispositions du code de la propriété intellectuelle, seule l'utilisation pour un usage privé, réduite au cercle de famille (sous réserve de dispositions différentes voire plus restrictives du code de la propriété intellectuelle) et la reproduction (impression, téléchargement) pour un usage strictement personnel sont autorisés.

A défaut d'autorisation préalable communiquée par écrit par TO DO TODAY ou du titulaire des droits, toute autre utilisation est constitutive de contrefaçon sanctionnée par le code de la propriété intellectuelle.

Toute reproduction totale ou partielle du catalogue de TO DO TODAY est strictement interdite. 

TO DO TODAY et son logo sont des marques déposées et protégées à ce titre par le droit des marques. Elles sont la propriété exclusive de TO DO TODAY.

Article 16 : Protection des informations nominatives
Les informations communiquées par l’Adhérent dans le cadre du Contrat d’adhésion et de ses commandes ne seront transmises à aucun tiers en dehors des fournisseurs et prestataires de services de TO DO TODAY. Ces informations seront considérées par TO DO TODAY, ses fournisseurs et ses prestataires comme strictement confidentielles. Elles ne seront utilisées par les services internes de TO DO TODAY et ses fournisseurs et prestataires que pour le traitement des commandes et pour renforcer et personnaliser la communication et l'offre des produits et services réservée aux Adhérents de TO DO TODAY.

Nonobstant les dispositions des alinéas précédents, l’Adhérent autorise expressément TO DO TODAY à informer son employeur ou bailleur dans le cas suivant : non-paiement d’une ou plusieurs facture(s) malgré une lettre de relance ; dans ce cas, cette information portera sur les nom, prénom de l’Adhérent et les date et montant de l’impayé.

Pour les besoins des présentes, ne seront pas assimilées à des tiers, les entités ou personnes reprenant l’activité de TO DO TODAY à la suite d’une cession ou d’un transfert.

Conformément à la loi informatique et libertés du 6 janvier 1978, l’Adhérent dispose d'un droit d'accès, de rectification, et d'opposition aux données personnelles le concernant.  Pour cela il lui suffit d’en faire la demande à TO DO TODAY par mail en indiquant son nom, prénom et adresse (contact@todotoday.com).

Article 17 : Fin du Contrat d’adhésion – Résiliation
TO DO TODAY se réserve la faculté de résilier le Contrat d’adhésion de plein droit et à tout moment, après une mise en demeure de sept (7) jours restée sans effet, pour les cas de violation par l’Adhérent de ses obligations ou de non-paiement d’une seule des échéances mensuelles au titre d’une de ses commandes.

En outre, TO DO TODAY procédera à la résiliation immédiate et de plein droit de l’adhésion : 
en cas de rupture du Contrat de prestations de services conclu avec la société Cliente pour quelque cause que ce soit ;
en cas de rupture du Contrat de collaboration ou de travail de l’Adhérent avec son employeur, dès le jour effectif de ladite rupture ;
en cas de départ de l’Adhérent du Site, sauf à ce que son employeur ait spécifiquement autorisé le maintien de son adhésion.

Il est expressément convenu qu’à compter de la date de résiliation effective de l’adhésion, l’Adhérent n’aura plus accès aux produits ou services proposés par TO DO TODAY. La cotisation annuelle éventuellement acquittée par l’Adhèrent restera acquise à TO DO TODAY. 

TO DO TODAY s’engage à exécuter la commande de l’Adhérent si cette dernière lui est parvenue avant la date effective de résiliation, même si l’exécution doit intervenir après la date effective de résiliation. 

Article 18 : Modification des Conditions Générales
Compte tenu des évolutions possibles des services, TO DO TODAY se réserve le droit d’adapter ou de modifier à tout moment les présentes Conditions Générales, en en informant l’Adhérent qui peut par ailleurs les consulter simplement, librement et à tout moment en s’adressant à la Conciergerie, ou sur le site web et l’application mobile.
L’Adhérent pourra résilier librement son Contrat d’adhésion en cas de désaccord.

Article 19 : Différend
Le présent Contrat d’adhésion est soumis au droit français.
EOF
            );
        $manager->persist($cgv);
        $cgv = (new Cgv())
            ->setModel('cgv3')
            ->setContent(
                <<<EOF
Contrat d’adhesion ET CONDITIONS GENERALES DE VENTE AUX UTILISATEURS 

Vous avez souhaité devenir adhérent de la conciergerie TO DO TODAY et nous vous en remercions. Afin de mieux vous connaître, nous vous remercions de bien vouloir :

(i) Prendre connaissance des Conditions Générales de Vente et en accepter les termes ;
(ii) Remplir et signer la fiche d’adhésion ci-jointe. Les informations que vous y indiquerez seront utilisées par TO DO TODAY pour personnaliser votre compte Adhérent, vous faire gagner du temps lorsque vous passerez une commande, et répondre donc plus efficacement à vos attentes ;
(iii) Remplir et signer le mandat de prélèvement annexé au présent Contrat et d’y joindre un IBAN le cas échéant, afin de valider votre adhésion.

L’ensemble de votre dossier d’adhésion doit être remis à la Conciergerie. 

Nous vous rappelons que la signature de ces documents a pour objet de garantir votre sécurité et d’assurer le respect de la plus stricte confidentialité concernant vos données personnelles, tant vis-à-vis des tiers que de votre employeur et/ou de votre bailleur.

N’hésitez pas à vous adresser à votre Concierge si vous avez la moindre question concernant ces documents.
.

CONTRAT D’ADHESION ET CONDITIONS GENERALES

Article 1 : Objet
La société TO DO TODAY propose à ses Clients (entreprises ou propriétaires d’immeubles) une offre de services de conciergeries sur mesure via une implantation physique et/ou à distance dans les locaux de ses Clients.

Ces services sont exclusivement destinés aux Adhérents des conciergeries, qu’ils soient salariés ou collaborateurs des Clients entrepris, ou salariés et collaborateurs des entreprises locataires des Clients propriétaires d’immeubles de bureaux et à leurs co-Adhérents tels que désignés dans la fiche d’adhésion, étant précisé que ne peuvent être désignés comme co-Adhérents, que le conjoint, concubin ou pacs, et le(s) enfant(s) majeurs de l’Adhérent.

Dans certains cas, des prestataires externes au Client peuvent être habilités par ce dernier à utiliser tout ou partie des Services. Le cas échéant, ces accès et spécificités d’utilisation sont décrites dans les documents d’information remis par le concierge.

Afin de simplifier et de sécuriser l’utilisation de ses services par les Adhérents, TO DO TODAY se substitue à ses fournisseurs et prestataires, devenant par là même leur interlocuteur unique, tant pour la prise de commande que pour le suivi et la facturation. Ces derniers restent cependant seuls responsables des prestations effectuées par eux lorsque les services sont rendus lorsque les services sont rendus par le biais d’un casier et sans l’intermédiaire d’un concierge.

Article 2 : Prise d’effet du Contrat
Le Contrat d’adhésion prend effet au retour à TO DO TODAY du Contrat d’adhésion dûment complété, signé par l’Adhérent et accompagné des pièces justificatives demandées. 
L’adhésion est confirmée par TO DO TODAY par l’envoi d’un mail dès validation du Contrat.

Article 3 : Durée du Contrat
Le Contrat d’adhésion est conclu pour une période de douze (12) mois renouvelable tacitement, sauf dénonciation expresse du Contrat d’adhésion par l’Adhérent par toute voie de communication écrite, au moins un (1) mois avant la date anniversaire du Contrat.
Les cas de résiliation anticipée du Contrat font l’objet de l’article 17 des présentes.

Article 4 : Accès au service
L’Adhérent dispose de différents modes d’accès au service TO DO TODAY : physiquement au comptoir de la conciergerie TO DO TODAY installé dans son entreprise (le cas échéant), par téléphone, par mail, par portail web et/oupar le biais d’une application mobile.

Afin d’assurer la sécurité des Adhérents, le système informatique TO DO TODAY génère un numéro d’adhérent lié à chaque Contrat ainsi qu’un mot de passe qui doit être utilisé à chaque commande passée par le portail ou l’application mobile. L’Adhérent accepte de prendre soin de ce mot de passe et de ne le confier qu’aux co-Adhérents expressément désignés dans la fiche de renseignement incluse aux présentes et dont il se porte garant. Pour être identifiés et par mesure de sécurité, les co-Adhérents utilisant les services de la conciergerie sur site devront répondre à la question de sécurité également stipulée dans la fiche d’adhésion.

L’Adhérent a également la possibilité de déposer une commande dans les casiers prévus à cet effet et installés dans l’immeuble à cet effet.
Le dépôt dans un casier doit s’accompagner d’une commande, passée par le biais du portail ou de l’application mobile, dans laquelle le numéro de casier utilisé par l’adhérent doit obligatoirement être renseigné. Le casier doit être fermé par l’Adhérent au moyen du code confidentiel personnel qui lui aura été communiqué lors de son adhésion.

En l’absence de Concierge sur site, et pour permettre la bonne exécution de la commande, le code confidentiel sera communiqué au Prestataire en charge de son exécution.

La bonne prise en charge de la commande par la Conciergerie est confirmée à l’adhérent par mail, une fois le contrôle des articles effectué par le concierge ou directement par le prestataire.  
L’Adhérent est informé que la responsabilité de TO DO TODAY n’est engagée qu’à compter de la date et heure d’envoi du mail de confirmation de commande qui comprendra le cas échéant les réserves nécessaires.

De même, l’Adhérent pourra demander au Concierge que le retour de sa commande soit déposé dans un casier. L’Adhérent recevra alors un mail de confirmation de livraison indiquant le numéro du casier utilisé à cet effet. Celui-ci sera accessible par l’Adhérent au moyen de son code confidentiel personnel.
L’Adhérent est informé que la responsabilité de TO DO TODAY n’est plus engagée à compter de la réception de ce mail de confirmation.

Article 5 : Commande 

5.1 Modalités de la prise de commande :
La commande peut être passée selon les modes d’accès décrits à l’article 4 ci-dessus.
Les informations relatives à la commande (nature de la prestation, prix, modalités de livraison, heure de rendez-vous par exemple) sont saisies informatiquement par l’équipe de TO DO TODAY ou par l’Adhérent directement dans le portail et/ou dans l’application mobile.

5.2 Confirmation de la commande
Un bordereau de confirmation reprenant les informations relatives à la commande est envoyé à l’Adhérent par mail ou mis à disposition sur l’application SmartPhone une fois la disponibilité des produits commandés validée par TO DO TODAY ou, pour les prestations nécessitant le passage de l’Adhérent à la conciergerie ou le dépôt dans un casier (pressing, cordonnerie par exemple), une fois le contrôle des biens confiés exécuté par le concierge ou, par le prestataire.
La confirmation par TO DO TODAY de la commande atteste de la disponibilité du produit ou du service commandé. Sont notamment précisés dans la confirmation de commande le prix à payer au titre du produit ou du service commandé, l’heure de livraison ou de réalisation de la prestation et le délai maximum d’annulation de la commande.

5.3 Modification d’une commande
Les commandes exprimées via le portail et/ou l’application mobile ne peuvent être modifiées en ligne et nécessitent l’intervention du Concierge.
Pour l’ensemble des commandes confirmées, tout désaccord de la part de l’Adhérent sur l’une ou l’ensemble des informations contenues dans le bordereau de confirmation doit, dans un délai maximal de deux heures, être porté à l’attention des membres de la conciergerie de TO DO TODAY qui s’attacheront à rectifier la commande dans les meilleurs délais. Passé ce délai de deux heures, TO DO TODAY fera ses meilleurs efforts pour vous satisfaire mais ne peut vous garantir de pouvoir enregistrer votre modification de commande.

5.4 Annulation d’une commande
Toute commande d’un produit en stock chez TO DO TODAY peut être annulée à tout moment, sans pénalité.
Dans l’hypothèse où l’Adhérent annule la commande d’un produit ou service pour lequel TO DO TODAY se fournit auprès d’un fournisseur ou prestataire externe, et si cette annulation intervient au-delà du délai inscrit sur le bordereau de confirmation, la commande est réputée consommée et lui est facturée.

En cas d’annulation par l’Adhérent d’une commande de prestation de service (hors courses de proximité)  moins de 12 heures avant l’heure prévue d’exécution de la prestation telle qu’indiquée sur le bordereau de confirmation, la prestation est réputée consommée et lui est facturée.

TO DO TODAY s’efforce de maintenir des listes d’attentes permettant de transférer le rendez-vous annulé à un autre Adhérent.  Dans ce cas seulement, la prestation annulée n’est pas facturée à l’Adhérent.


Article 6: Disponibilité
Certains services proposés par TO DO TODAY sont effectués par des prestataires de services externes avec lesquels TO DO TODAY a contracté.

Dans certains cas, des ruptures ou indisponibilités de la part de nos prestataires peuvent avoir lieu et ne sauraient être imputées à TO DO TODAY.

TO DO TODAY fera son meilleur effort pour que les fournisseurs et prestataires soient en mesure d’honorer toutes les commandes des Adhérents.  Des indications sur la disponibilité des produits et services sont fournies lors de l’émission du bordereau de validation de la commande.  Les modalités de cette confirmation sont détaillées à l’article 5.2 ci-dessus.

Article 7 : Prix
Les prix proposés par TO DO TODAY pour les produits ou services sont indiqués en euro toutes taxes comprises, au taux de TVA en vigueur à la date de commande des produits et service. 

Si le taux de la TVA venait à être modifié, que ce soit à la hausse ou à la baisse, le changement pourra immédiatement être répercuté sur le prix des produits et services, même si ce changement intervient après la confirmation de la commande par l’Adhérent.

TO DO TODAY se réserve le droit de modifier ses prix à tout moment mais les produits et services seront facturés sur la base des tarifs affichés au moment de la passation de la commande par l’Adhérent.

Article 8 : Livraison
Les produits ou services choisis sont livrés à l’espace réservé à TO DO TODAY au sein des locaux du Client dans les délais indiqués sur la confirmation de commande.
Les délais de livraison indiqués dans la confirmation de commande correspondent aux délais maximums de traitement et de livraison pour les produits et services commandés.

En cas de dépassement du délai de livraison, TO DO TODAY s’engage à informer immédiatement l’Adhérent et à lui proposer soit un nouveau délai, soit l’annulation et le remboursement de sa commande.  Le remboursement s’effectue par le biais d’un crédit porté au compte de l’Adhérent.

Article 9 – Garanties

9.1 Garantie légale
Les Adhérents bénéficient de la garantie légale des vices cachés sur les produits livrés.

9.2 Délai de rétractation
L’Adhérent dispose d’un délai légal de quatorze (14)jours francs à compter de la livraison de sa commande pour faire retour à TO DO TODAY du produit dans son emballage d’origine en parfait état.  Le retour devra se faire au comptoir de la conciergerie installée au sein de l’entreprise Cliente ou par un dépôt dans le casier choisi par l’Adhérent et fermé par son code confidentiel doublé d’un mail d’information à la conciergerie.

Le droit de retour ne pourra être exercé pour :
Les services qui auront déjà reçu un début d’exécution avec l’accord de l’Adhérent avant la fin du délai de quatorze jours (le pressing par exemple) ;
Les produits alimentaires pour lesquels la date de péremption est arrivée à échéance ou les produits ayant une très courte durée de vie, comme, notamment, les fleurs ;
Les articles retournés incomplets, abîmés, endommagés ou salis.

Article 10 : Dispositions Financières
Les commandes de produits et services effectuées auprès de TO DO TODAY et délivrées sur le site font l’objet d’une facture mensuelle unique payable par prélèvement automatique. Le relevé d’opérations reprenant l’ensemble des prestations et produits commandés dans le mois est envoyé à la fin du mois par mail.

Le prélèvement s’effectue dans les 48 à 72 heures suivantes, terme échu.

Le cas échéant, le montant de la cotisation annuelle est indiqué dans les documents d’information remis à l’Adhèrent par le concierge.   Celle-ci est incluse dans la première facture mensuelle suivant la date d’adhésion ou de renouvellement du Contrat. 
Dans le cas où un titre de paiement émis au profit de TO DO TODAY ne serait pas honoré, les sommes dues sont majorées des frais divers liés à l’impayé que TO DO TODAY a été contrainte d’engager et sont refacturées à l’Adhérent (notamment les frais de rejet de prélèvement automatique).

Les commandes de services effectuées auprès de prestataires externes par le biais du service de Conciergerie à distance (services à la personne, artisans…) feront l’objet d’un règlement à la prestation, effectué directement auprès du prestataire délivrant le service.

Pour les prestataires de services à la personne agréés par les pouvoirs publics, nous vous rappelons que certaines prestations pourront bénéficier d'un taux de TVA réduit. 
Le taux applicable sera celui en vigueur pour le service concerné à la date de commande.
Une attestation fiscale est envoyée à l’Adhérent par le prestataire au plus tard le 31 janvier de l’année suivant la date d’exécution du service.  Cette attestation permet à l’Adhérent de faire valoir son droit à une réduction d’impôt sur le revenu, dans les limites et conditions fixées par la loi fiscale en vigueur.

Article 11 : Changement de domiciliation bancaire
En cas de changement de coordonnées bancaires, l’Adhérent adressera sans délai à TO DO TODAY  un mandat portant autorisation de prélèvement accompagné d’un IBAN.
Les impayés résultant du défaut de communication de la nouvelle domiciliation bancaire majorés des frais liés à l’impayé que TO DO TODAY aura été contrainte d’engager seront refacturés à l’Adhérent (notamment les frais de rejet de prélèvement automatique).

Article 12 : Obligations de TO DO TODAY
TO DO TODAY s’engage à tout mettre en œuvre pour assurer la permanence, la continuité et la qualité du service fourni à ses Adhérents, conformément à l’article 6 des présentes.

La responsabilité de TO DO TODAY ne peut être engagée :
En cas de mauvaise utilisation du service par l’Adhérent (adresse d’intervention erronée, mauvaise spécification du mode de livraison…) ;
En cas de non-respect par l’Adhérent de ses obligations contractuelles ;
En cas de force majeure au sens de la jurisprudence française.

Article 13 : Obligations de l’Adhérent
L’Adhérent est responsable du paiement de l’ensemble des sommes dues au titre du Contrat d’adhésion comme de l’exécution des obligations souscrites au titre du Contrat.

L’Adhérent s’engage à prévenir TO DO TODAY dans un délai maximum de sept (7) jours ouvrables de tout changement ou modification relatifs à la fiche d’adhésion annexée au présent Contrat.

De même, l’Adhérent s’engage à informer TO DO TODAY de son départ du site ou de son entreprise afin que TO DO TODAY puisse procéder à la clôture de son compte, dans les conditions prévues à l’article 17.

Cette obligation est justifiée par la nature même des conditions d’organisation et de livraison des prestations de TO DO TODAY. A défaut, l’Adhérent ne pourra s’en prévaloir auprès de TO DO TODAY pour quelque cause que ce soit.

L’Adhérent s’interdit expressément de solliciter et/ou d’utiliser, directement ou indirectement, les services des prestataires et fournisseurs de TO DO TODAY sans en référer à TO DO TODAY.

Article 14: Responsabilité

14.1 Fiches produits
TO DO TODAY présente dans son catalogue papier et Intranet les caractéristiques essentielles des produits et services proposés, conformément à l'article L 111-1 du Code de la Consommation.

Les produits et services proposés sont conformes à la législation française en vigueur.

Les fiches de présentation des produits et services sont établies à partir des informations données par les fournisseurs et prestataires de services, partenaires de TO DO TODAY. Les photographies, graphismes et les textes illustrant les produits et services présentés ont valeur indicative et non contractuelle. De ce fait, la responsabilité de TO DO TODAY ne saurait être engagée et la validité d’une commande passée et réalisée ne saurait être contestée, en cas d’erreur ou d’imprécision sur ces éléments de présentation.

14.2 Force majeure
La responsabilité de TO DO TODAY, ne saurait être engagée, en cas de retard dans l’exécution du Contrat ou en cas d’inexécution du Contrat, en raison d’un cas de force majeure.

Si l’événement de force majeure retarde ou rend impossible l’exécution de la commande pendant une durée excédant 30 jours, la commande pourra être annulée et TO DO TODAY remboursera, le cas échéant l’Adhérent par un crédit porté au compte de l’Adhérent.

Article 15 : Propriété Intellectuelle 
Tous les textes, commentaires, ouvrages, illustrations et images reproduits dans les catalogues papier, applications mobiles et sites web de TO DO TODAY, qu’ils soient la propriété ou non de TO DO TODAY, sont protégés au titre de la propriété intellectuelle pour la durée de protection des droits et pour le monde entier.

A ce titre et conformément aux dispositions du code de la propriété intellectuelle, seule l'utilisation pour un usage privé, réduite au cercle de famille (sous réserve de dispositions différentes voire plus restrictives du code de la propriété intellectuelle) et la reproduction (impression, téléchargement) pour un usage strictement personnel sont autorisés.

A défaut d'autorisation préalable communiquée par écrit par TO DO TODAY ou du titulaire des droits, toute autre utilisation est constitutive de contrefaçon sanctionnée par le code de la propriété intellectuelle.

Toute reproduction totale ou partielle du catalogue de TO DO TODAY est strictement interdite. 

TO DO TODAY et son logo sont des marques déposées et protégées à ce titre par le droit des marques. Elles sont la propriété exclusive de TO DO TODAY.

Article 16 : Protection des informations nominatives
Les informations communiquées par l’Adhérent dans le cadre du Contrat d’adhésion et de ses commandes ne seront transmises à aucun tiers en dehors des fournisseurs et prestataires de services de TO DO TODAY. Ces informations seront considérées par TO DO TODAY, ses fournisseurs et ses prestataires comme strictement confidentielles. Elles ne seront utilisées par les services internes de TO DO TODAY et ses fournisseurs et prestataires que pour le traitement des commandes et pour renforcer et personnaliser la communication et l'offre des produits et services réservée aux Adhérents de TO DO TODAY.

Nonobstant les dispositions des alinéas précédents, l’Adhérent autorise expressément TO DO TODAY à informer son employeur ou bailleur dans le cas suivant : non-paiement d’une ou plusieurs facture(s) malgré une lettre de relance ; dans ce cas, cette information portera sur les nom, prénom de l’Adhérent et les date et montant de l’impayé.

Pour les besoins des présentes, ne seront pas assimilées à des tiers, les entités ou personnes reprenant l’activité de TO DO TODAY à la suite d’une cession ou d’un transfert.

Conformément à la loi informatique et libertés du 6 janvier 1978, l’Adhérent dispose d'un droit d'accès, de rectification, et d'opposition aux données personnelles le concernant.  Pour cela il lui suffit d’en faire la demande à TO DO TODAY par mail en indiquant son nom, prénom et adresse (contact@todotoday.com)

Article 17 : Fin du Contrat d’adhésion – Résiliation
TO DO TODAY se réserve la faculté de résilier le Contrat d’adhésion de plein droit et à tout moment, après une mise en demeure de sept (7) jours restée sans effet, pour les cas de violation par l’Adhérent de ses obligations ou de non-paiement d’une seule des échéances mensuelles au titre d’une de ses commandes.

En outre, TO DO TODAY procédera à la résiliation immédiate et de plein droit de l’adhésion : 
en cas de rupture du Contrat de prestations de services conclu avec la société Cliente pour quelque cause que ce soit ;
en cas de rupture du Contrat de collaboration ou de travail de l’Adhérent avec son employeur, dès le jour effectif de ladite rupture ;
en cas de départ de l’Adhérent du Site, sauf à ce que son employeur ait spécifiquement autorisé le maintien de son adhésion.

Il est expressément convenu qu’à compter de la date de résiliation effective de l’adhésion, l’Adhérent n’aura plus accès aux produits ou services proposés par TO DO TODAY. La cotisation annuelle éventuellement acquittée par l’Adhèrent restera acquise à TO DO TODAY. 

TO DO TODAY s’engage à exécuter la commande de l’Adhérent si cette dernière lui est parvenue avant la date effective de résiliation, même si l’exécution doit intervenir après la date effective de résiliation. 

Article 18 : Modification des Conditions Générales
Compte tenu des évolutions possibles des services, TO DO TODAY se réserve le droit d’adapter ou de modifier à tout moment les présentes Conditions Générales, en en informant l’Adhérent qui peut par ailleurs les consulter simplement, librement et à tout moment en s’adressant à la Conciergerie, ou sur le site web et l’application mobile.
. 
L’Adhérent pourra résilier librement son Contrat d’adhésion en cas de désaccord.

Article 19 : Différend
Le présent Contrat d’adhésion est soumis au droit français.
EOF
            );

        $manager->persist($cgv);
        $cgv = (new Cgv())
            ->setModel('cgv4')
            ->setContent(
                <<<EOF
Article 1 : Objet

La société TO DO TODAY offre pour le compte de la société ORI SYGMA, propriétaire du Centre commercial
DOMUS (ci-après « le Client ») des services de conciergerie sur mesure via une implantation physique dans les locaux du Centre commercial et un accès distant par le biais d’un portail et d’une application mobile.
Ces services sont exclusivement destinés aux Adhérents de la conciergerie titulaires d’une carte de fidélité du Centre commercial et à leurs co-Adhérents tels que désignés dans la fiche de renseignement, étant précisé que ne peuvent être désignés comme co-Adhérents, que le conjoint, concubin ou pacs, et le(s) enfant(s) de l’Adhérent.
Afin de simplifier et de sécuriser l’utilisation de ses services par les Adhérents, TO DO TODAY se substitue à ses fournisseurs et prestataires, devenant par là même leur interlocuteur unique, tant pour la prise de commande que pour le suivi et la facturation.

Article 2 : Prise d’effet du contrat

Le Contrat d’adhésion prend effet au retour à TO DO TODAY du Contrat d’adhésion dûment complété, signé par l’Adhérent et accompagné des pièces justificatives demandées.
L’adhésion est confirmée par TO DO TODAY par l’envoi d’un mail dès validation du Contrat.

Article 3 : Durée du contrat

Le Contrat d’adhésion est conclu pour une période de douze (12) mois renouvelable tacitement, sauf dénonciation expresse du Contrat d’adhésion par l’Adhérent par toute voie de communication écrite, au moins un (1) mois avant la date anniversaire du Contrat.
Les cas de résiliation anticipée du Contrat font l’objet de l’article 17 des présentes.

Article 4 : Accès au service

L’Adhérent dispose de différents modes d’accès au service TO DO TODAY : physiquement à la conciergerie, par téléphone, par mail, par portail web et par le biais d’une application mobile.
Afin d’assurer la sécurité des Adhérents, TO DO TODAY attribue un numéro d’adhérent lié à chaque Contrat ainsi qu’un mot de passe qui doit être utilisé à chaque commande. L’Adhérent accepte de prendre soin de ce mot de passe et de ne le confier qu’aux co-adhérents expressément désignés dans la fiche de renseignement incluse aux présentes et dont il se porte garant.

Article 5 : Commande

5.1 Modalités de la prise de commande :
La commande peut être passée selon les modes d’accès décrits à l’article 4 ci-dessus.
Les informations relatives à la commande (nature de la prestation, prix, modalités de livraison, heure de rendez-vous par exemple) sont saisies informatiquement par l’équipe de TO DO TODAY ou par l’Adhérent directement dans le portail et/ou dans l’application mobile.
5.2 Confirmation de la commande
Un bordereau de confirmation reprenant les informations relatives à la commande est envoyé à l’Adhérent par mail ou via l’application une fois la disponibilité des produits ou services commandés validée par le Concierge ou, pour les prestations nécessitant le passage de l’Adhérent à la conciergerie, une fois le contrôle des biens confiés exécuté par le concierge.
La confirmation par TO DO TODAY de la commande atteste de la disponibilité du produit ou du service commandé. Sont notamment précisés dans la confirmation de commande le prix à payer au titre du produit ou du service commandé, l’heure de livraison ou de réalisation de la prestation et le délai maximum d’annulation de la commande.
5.3 Modification d’une commande
Les commandes exprimées via le portail et/ou l’application mobile peuvent être modifiées en ligne sous réserve que son exécution ne soit pas déjà en cours, selon le délai ci-dessous.
Pour l’ensemble des commandes confirmées, tout désaccord de la part de l’Adhérent sur l’une ou l’ensemble des informations contenues dans le bordereau de confirmation doit, dans un délai maximal de deux heures, être porté à l’attention des membres de la conciergerie de TO DO TODAY qui s’attacheront à rectifier la commande dans les meilleurs délais. Passé ce délai de deux heures, TO DO TODAY fera ses meilleurs efforts pour vous satisfaire mais ne peut vous garantir de pouvoir enregistrer votre modification de commande.
5.4 Annulation d’une commande
Toute commande d’un produit en stock chez TO DO TODAY peut être annulée à tout moment, sans pénalité.
Dans l’hypothèse où l’Adhérent annule la commande d’un produit ou service pour lequel TO DO TODAY se fournit auprès d’un fournisseur ou prestataire externe, et si cette annulation intervient au-delà du délai inscrit sur le bordereau de confirmation, la commande est réputée consommée et lui est facturée.
En cas d’annulation par l’Adhérent d’une commande de prestation de service moins de 12 heures avant l’heure prévue d’exécution de la prestation telle qu’indiquée sur le bordereau de confirmation, la prestation est réputée consommée et lui est facturée.

Article 6 : Disponibilité

Certains services proposés par TO DO TODAY sont effectués par des prestataires de services externes avec lesquels TO DO TODAY a contracté.
Dans certains cas, des ruptures ou indisponibilités de la part de nos prestataires peuvent avoir lieu et ne sauraient être imputées à TO DO TODAY.
TO DO TODAY fera son meilleur effort pour que les fournisseurs et prestataires soient en mesure d’honorer toutes les commandes des Adhérents. Des indications sur la disponibilité des produits et services sont fournies lors de l’émission du bordereau de validation de la commande. Les modalités de cette confirmation sont détaillées à l’article 5.2 ci-dessus.

Article 7 : Prix

Les prix proposés par TO DO TODAY pour les produits ou services sont indiqués en euro toutes taxes comprises, au taux de TVA en vigueur à la date de commande des produits et service.
Si le taux de la TVA venait à être modifié, que ce soit à la hausse ou à la baisse, le changement pourra immédiatement être répercuté sur le prix des produits et services, même si ce changement intervient après la confirmation de la commande par l’Adhérent.
TO DO TODAY se réserve le droit de modifier ses prix à tout moment mais les produits et services seront facturés sur la base des tarifs affichés au moment de la passation de la commande par l’Adhérent.

Article 8 : Livraison

Les produits ou services choisis sont livrés soit à la conciergerie DOMUS par TO DO TODAY installée au sein du centre commercial DOMUS, soit au domicile de l’Adhérent, dans les délais indiqués sur la confirmation de commande.
Les délais de livraison indiqués dans la confirmation de commande correspondent aux délais maximums de traitement et de livraison pour les produits et services commandés.
En cas de dépassement du délai de livraison TO DO TODAY s’engage à informer immédiatement l’Adhérent et à lui proposer soit un nouveau délai, soit l’annulation et le remboursement de sa commande. Le remboursement s’effectue par le biais d’un crédit porté au compte de l’Adhérent.

Article 9 : Garanties

9.1 Garantie légale
Les Adhérents bénéficient de la garantie légale des vices cachés sur les produits livrés.
9.2 Délai de rétractation
L’Adhérent dispose d’un délai légal de quatorze (14) jours francs à compter de la livraison de sa commande pour faire retour à TO DO TODAY du produit dans son emballage d’origine en parfait état. Le retour devra se faire au comptoir de la conciergerie installée au sein de DOMUS.
Le droit de retour ne pourra être exercé pour :
- Les services qui auront déjà reçu un début d’exécution avec l’accord de l’Adhérent avant la fin du délai de sept jours (le pressing par exemple) ;
- Les produits alimentaires pour lesquels la date de péremption est arrivée à échéance ou les produits ayant une très courte durée de vie, comme, notamment, les fleurs ;
- Les articles retournés incomplets, abîmés, endommagés ou salis.

Article 10 : Dispositions Financières

Les commandes de produits et services effectuées auprès de TO DO TODAY et délivrées sur le site font l’objet d’une facture mensuelle unique payable par prélèvement automatique sur le compte bancaire ou la carte bancaire de l’Adhérent. Le relevé d’opérations reprenant l’ensemble des prestations et produits commandés dans le mois est envoyé le 3 du mois suivant par mail.
Le prélèvement s’effectue le 5 du mois suivant, terme échu.
Dans le cas où un titre de paiement émis au profit de TO DO TODAY ne serait pas honoré, les sommes dues sont majorées des frais divers liés à l’impayé que TO DO TODAY a été contrainte d’engager et sont refacturées à l’Adhérent (notamment les frais de rejet de prélèvement automatique).
Pour les prestataires agréés par les pouvoirs publics, nous vous rappelons que certaines prestations pourront bénéficier d’un taux de TVA réduit.
Le taux applicable sera celui en vigueur pour le service concerné à la date de commande.
Certains services à la personne ouvrent par ailleurs droit à une réduction d’impôt dans les conditions fixées par la loi. Le cas échéant, une attestation fiscale est envoyée à l’Adhérent par le prestataire au plus tard le 31 janvier de l’année suivant la date d’exécution du service. Cette attestation permet à l’Adhérent de faire valoir son droit à une réduction d’impôt sur le revenu, dans les limites et conditions fixées par la loi fiscale en vigueur.

Article 11 : Changement de domiciliation bancaire

En cas de changement de coordonnées bancaires, l’Adhérent adressera sans délai à TO DO TODAY un mandat portant autorisation de prélèvement accompagné d’un IBAN.
Les impayés résultant du défaut de communication de la nouvelle domiciliation bancaire majorés des frais liés à l’impayé que TO DO TODAY aura été contrainte d’engager seront refacturés à l’Adhérent (notamment les frais de rejet de prélèvement automatique).
Article 12 : Obligations de TO DO TODAY

TO DO TODAY s’engage à tout mettre en œuvre pour assurer la permanence, la continuité et la qualité du service fourni à ses Adhérents, conformément à l’article 6 des présentes.
La responsabilité de TO DO TODAY ne peut être engagée :
- En cas de mauvaise utilisation du service par l’Adhérent (adresse d’intervention erronée, mauvaise spécification du mode de livraison…) ;
- En cas de non-respect par l’Adhérent de ses obligations contractuelles ;
- En cas de force majeure au sens de la jurisprudence française.

Article 13 : Obligations de l’Adhérent
L’Adhérent est responsable du paiement de l’ensemble des sommes dues au titre du Contrat d’adhésion comme de l’exécution des obligations souscrites au titre du Contrat.
L’Adhérent s’engage à prévenir TO DO TODAY dans un délai maximum de sept (7) jours ouvrables de tout changement ou modification relatifs à la fiche d’adhésion annexée au présent Contrat.
Cette obligation est justifiée par la nature même des conditions d’organisation et de livraison des prestations de TO DO TODAY. A défaut, l’Adhérent ne pourra s’en prévaloir auprès de TO DO TODAY pour quelque cause que ce soit.
L’Adhérent s’interdit expressément de solliciter et/ou d’utiliser, directement ou indirectement, les services des prestataires et fournisseurs de TO DO TODAY sans en référer à TO DO TODAY.

Article 14: Responsabilité

14.1 Fiches produits
TO DO TODAY présente dans son catalogue papier et Intranet les caractéristiques essentielles des produits et services proposés, conformément à l’article L 111-1 du Code de la Consommation.
Les produits et services proposés sont conformes à la législation française en vigueur.
Les fiches de présentation des produits et services sont établies à partir des informations données par les fournisseurs et prestataires de services, partenaires de TO DO TODAY. Les photographies, graphismes et les textes illustrant les produits et services présentés ont valeur indicative et non contractuelle. De ce fait, la responsabilité de TO DO TODAY ne saurait être engagée et la validité d’une commande passée et réalisée ne saurait être contestée, en cas d’erreur ou d’imprécision sur ces éléments de présentation.
14.2 Responsabilité des prestataires
Afin de simplifier et de sécuriser l’utilisation des services proposés par TO DO TODAY à ses Adhérents, les
Adhérents mandatent TO DO TODAY pour les représenter vis-à-vis des fournisseurs et prestataires référencés par TO DO TODAY.
TO DO TODAY est ainsi, pour les services qui ne donnent pas lieu à l’établissement d’un devis ou d’un bon de commande, l’interlocuteur unique des Adhérents, tant pour la prise de commande que pour le suivi et la facturation.
En revanche, pour les travaux ou prestations réalisés sur devis ou commande, ceux-ci sont validés directement par les Adhérents et TO DO TODAY n’assure que le suivi de la réalisation des travaux ou prestations et la facturation.
En tant que mandataire des Adhérents, TO DO TODAY ne saurait engager sa responsabilité au titre des travaux ou prestations de services réalisés par les fournisseurs et prestataires référencés par TO DO TODAY pour le compte de l’Adhérent.
14.3 Force majeure
La responsabilité de TO DO TODAY, ne saurait être engagée, en cas de retard dans l’exécution du Contrat ou en cas d’inexécution du Contrat, en raison d’un cas de force majeure.
Si l’événement de force majeure retarde ou rend impossible l’exécution de la commande pendant une durée excédant 30 jours, la commande pourra être annulée et TO DO TODAY remboursera, le cas échéant l’Adhérent par un crédit porté au compte de l’Adhérent.

Article 15 : Propriété Intellectuelle

Tous les textes, commentaires, ouvrages, illustrations et images reproduits dans les catalogues papier, applications mobiles et sites web de TO DO TODAY, qu’ils soient la propriété ou non de TO DO TODAY, sont protégés au titre de la propriété intellectuelle pour la durée de protection des droits et pour le monde entier.
A défaut d’autorisation préalable communiquée par écrit par TO DO TODAY ou du titulaire des droits, toute autre utilisation est constitutive de contrefaçon sanctionnée par le code de la propriété intellectuelle.
Toute reproduction totale ou partielle du catalogue de TO DO TODAY est strictement interdite.
TO DO TODAY et son logo sont des marques déposées et protégées à ce titre par le droit des marques. Elles sont la propriété exclusive de TO DO TODAY.

Article 16 : Protection des informations nominatives

Afin de pouvoir offrir ses services de conciergerie, TO DO TODAY collecte auprès de l’Adhérent différentes informations nominatives pour le compte de la société ORI SYGMA, propriétaire du Centre commercial DOMUS.
Les informations communiquées par l’Adhérent dans le cadre du Contrat d’adhésion et de ses commandes ne seront transmises à aucun tiers en dehors des fournisseurs et prestataires de services de TO DO TODAY et de la société ORI SYGMA.
Ainsi, pour les besoins des présentes, n’est pas assimilée à un tiers la société ORI SYGMA, propriétaire du
Centre commercial DOMUS pour le compte de laquelle TO DO TODAY gère la conciergerie et les informations nominatives transmises par les Adhérents, ni les entités ou personnes reprenant l’activité de TO DO TODAY à la suite d’une cession ou d’un transfert. En tant que de besoin, il est précisé que les enseignes et commerçants du centre commercial DOMUS sont considérés comme des tiers et n’ont pas accès aux données nominatives visées aux présentes.
A l’issue des relations contractuelles entre TO DO TODAY et la société ORI SYGMA, propriétaire du Centre commercial DOMUS pour le compte de laquelle TO DO TODAY gère la conciergerie, les informations nominatives seront transmises à la société ORI SYGMA afin que celle-ci puisse poursuivre l’activité de conciergerie inaugurée par TO DO TODAY.
Ces informations seront considérées par la société ORI SYGMA, propriétaire du Centre commercial DOMUS, par TO DO TODAY, ses fournisseurs, ses prestataires comme strictement confidentielles.
Elles ne seront utilisées par les services internes de TO DO TODAY, ses fournisseurs, prestataires et par la société ORI SYGMA, propriétaire du Centre commercial DOMUS, que pour le traitement des commandes et pour renforcer et personnaliser la communication et l’offre des produits et services réservés aux Adhérents de TO DO TODAY et aux titulaires de cartes de fidélité du centre commercial DOMUS.
Conformément à la loi informatique et libertés du 6 janvier 1978, l’Adhérent dispose d’un droit d’accès, de rectification, et d’opposition aux données personnelles le concernant. Pour cela il lui suffit d’en faire la demande à TO DO TODAY qui gère ces données personnelles pour le compte de ORI SYGMA, par mail (contact@todotoday.com) ou par courrier en indiquant son nom, prénom et adresse.

Article 17 : Fin du contrat d’adhésion – Résiliation

TO DO TODAY se réserve la faculté de résilier le Contrat d’adhésion de plein droit et à tout moment, après une mise en demeure de sept (7) jours restée sans effet, pour les cas de violation par l’Adhérent de ses obligations ou de non-paiement d’une seule des échéances mensuelles au titre d’une de ses commandes.
En cas de rupture du contrat liant TO DO TODAY et DOMUS, le contrat d’Adhésion liant l’adhérent et TO DO TODAY sera résilié de plein droit, le Centre Commercial faisant son affaire de la reprise de la relation contractuelle avec l’Adhérent, soit en son nom, soit au nom d’un nouveau prestataire de Conciergerie. La responsabilité de TO DO TODAY ne saurait être engagée par l’Adhérent pour quelque motif que ce soit, à compter de la date de résiliation effective.
Il est expressément convenu qu’à compter de la date de résiliation effective de l’adhésion, l’Adhérent n’aura plus accès aux produits ou services proposés par TO DO TODAY.
TO DO TODAY s’engage à exécuter la commande de l’Adhérent si cette dernière lui est parvenue avant la date effective de résiliation, même si l’exécution doit intervenir après la date effective de résiliation.

Article 18 : Modification des Conditions Générales

Compte tenu des évolutions possibles des services, TO DO TODAY se réserve le droit d’adapter ou de modifier à tout moment les présentes Conditions Générales, en en informant l’Adhérent qui peut par ailleurs les consulter simplement, librement et à tout moment en s’adressant à la Conciergerie, ou sur le site web et l’application mobile.
L’Adhérent pourra résilier librement son Contrat d’adhésion en cas de désaccord.

Article 19 : Différend

Le présent Contrat d’adhésion est soumis au droit français.
EOF
            );
        $manager->persist($cgv);
        $manager->persist($cgv);

        $cgv = (new Cgv())
            ->setModel('cgv6')
            ->setContent(
                <<<EOF
Article 1 - Purpose 
TO DO TODAY, Inc., a Delaware corporation (“TO DO TODAY”), provides to his Clients (corporations or owners of office buildings and office parks and their corporate tenants) Concierge Services to employees (each such individual a “Prospective Member”) through a physical presence on the Client’s premises (the “Conciergerie”). “Concierge Services” means the products and services offered to the Members by TO DO TODAY.

The Concierge Services are exclusively reserved for the Prospective Members (corporate client’s employees). Once a Prospective Member executes this Membership Agreement (this “Agreement”) and the Membership Form, he or she shall become a “Member.”

In order to provide the Concierge Services to the Members, TO DO TODAY acts on behalf of its suppliers and Service Providers, serving as the single point of contact for ordering, tracking and invoicing.

Article 2 - Effective Date
This Agreement shall be effective once it is returned to TO DO TODAY along with the requested documents, after having been completed and duly executed by the Member. 
TO DO TODAY shall confirm the Membership by e-mail once the Agreement is approved (the “Effective Date”).

Article 3 - Duration
This Agreement shall be effective for an initial period of twelve (12) months. This Agreement shall automatically renew for successive one year terms (each a “Renewal Period”) unless the Member provides notice of non-renewal in writing at least 30 days prior to the anniversary of the Effective Date. The Membership fee is non refundable.
The circumstances providing for early termination are described in Article 16 below.

Article 4 - Access
The Member can access the Concierge Services in several ways: physically at the Conciergerie, by phone, by e-mail, on the web portal, and using a mobile application.

In order to protect its Members, TO DO TODAY assigns to each Member a member number as well as a password that must be used for all orders placed through web portal and mobile application. The Member agrees to protect this password and to share it only with the co-member(s) expressly designated in the Membership Form included in this Agreement, and for whom he or she vouches.

In order to be identified and for security purposes, a co-member needs to respond to the security question chosen by the Member prior to using the services. Members can also use the Concierge Service lockers to drop their orders. Orders must be placed directly by Members through the web portal or the mobile application, and must include the Member’ locker PIN that was communicated to him by the Concierge.

Members can ask to pick-up their order from a locker. He or she will receive an e-mail confirming the locker number, that will be accessible to the Member using his or her locker PIN.

Article 5 - Orders
5.1 Placing an order
Orders can be placed using any of the methods described above in Article 4.

The order information (e.g. type of service, price, delivery method, and appointment time) shall be entered electronically by the TO DO TODAY team or directly by the Member in the web portal or mobile application.

5.2 Order confirmation
TO DO TODAY shall e-mail to the Member a confirmation slip detailing the order information once the Concierge team has verified that the products ordered are available, or, for services which require the Member to stop at the Conciergerie (e.g. dry-cleaning and shoe repair), once the Concierge team has verified the items are scheduled for delivery.

TO DO TODAY’s confirmation of the order guarantees that the product or service ordered is available. The confirmation slip shall specify the price for the product or service ordered, the delivery time or service time, and the order cancellation deadline.
TO DO TODAY becomes liable once the Member receives the confirmation of the order.

5.3 Order modification
Orders made through the web portal and/or the mobile application cannot be modified online. They require the involvement of the Concierge team.

For all confirmed orders, if the Member disagrees with any or all of the information on the confirmation slip, he or she has two (2) hours to notify the TO DO TODAY team who will endeavor to rectify the order as soon as possible. Beyond this two-hour period, TO DO TODAY will make every effort to satisfy the Member, but cannot guarantee that the order modification will be honored.

5.4 Order cancellation
All orders of products currently in stock with TO DO TODAY can be canceled at any time, without penalty. If the Member cancels an order for a product which TO DO TODAY must order from a supplier or third-party service provider, and the cancellation occurs after the deadline given on the order slip, the order will be considered delivered and invoiced as such.

If the Member cancels a service (other than local personal errands) less than 12 hours prior to the planned performance time as indicated on the confirmation slip, the service will be considered used and invoiced as such. 
However, TO DO TODAY will maintain a waiting list so that canceled appointments can be transferred to another Member. If TO DO TODAY confirms such transfer, and only in this case, the Member will not be billed for the canceled service.

Article 6 - Availability
Certain services offered by TO DO TODAY are performed or delivered by external suppliers or service providers (the “Service Providers”) under contract with TO DO TODAY. In some instances, the Service Providers may be unavailable or certain products may be out-of-stock. TO DO TODAY shall not be liable in these circumstances.
TO DO TODAY shall endeavor to ensure that its Service Providers are able to honor all of the Member’s orders. Information on product and service availability are provided on the order validation slip. The confirmation process is described above in Article 5.2.

Article 7 - Pricing for products and services
The prices for the products and services offered by TO DO TODAY shall be indicated in United States dollars, excluding any federal or local taxes that may apply. Applicable taxes shall be included in each monthly invoice. TO DO TODAY reserves the right, upon seven days written notice, to modify its prices at any time, but products and services will be invoiced on the basis of the prices displayed at the time the Member placed his or her order.

Article 8 - Delivery
The products and services selected will be delivered to the Conciergerie within the timeframe indicated on the order confirmation. 

The delivery times indicated on the order confirmation correspond to the maximum processing and delivery times for the products and services ordered. 
If a delivery is late, TO DO TODAY shall promptly inform the Member and offer either a new delivery time or a cancellation and reimbursement of the order. Reimbursement will take the form of a credit to the Member’s account.

Article 9 - Limited Warranty
9.1 Warranty
TO DO TODAY warrants to Member that it shall provide the Concierge Services using personnel of required skill, experience and qualifications and such services shall be delivered in a professional and workmanlike manner in accordance with generally recognized industry standards for similar services. All Services provided shall be in accordance with all applicable rules, laws and regulations.

THERE ARE NO OTHER WARRANTIES WITH RESPECT TO SERVICES PERFORMED OR GOODS DELIVERED HEREUNDER, INCLUDING WITHOUT LIMITATION ANY WARRANTY OF MERCHANTABILITY OR FITNESS WHICH ARE HEREBY EXPRESSLY DISCLAIMED. 

All Services provided shall be in accordance with all applicable rules, laws and regulations and TO DO TODAY shall require all of its third party service providers to have the required skill, experience and qualification to deliver the Concierge Services in a professional manner in accordance with recognized industry standards.

9.2 Returns
The Member has a period of seven (7) calendar days from the delivery of his or her order to return it to TO DO TODAY in the original packaging and in perfect condition. It must be returned to the Conciergerie.

The right of return does not apply to: 
- services which have already started with the Member’s agreement before the end of the seven-day period (e.g. dry-cleaning); 
- food products which have already reached their expiration date or products with a very short lifespan, particularly flowers; 
- items which are incomplete, worn, damaged or dirty when returned.

THE SOLE AND EXCLUSIVE REMEDY OF THE MEMBER WITH RESPECT TO GOODS OR SERVICES THAT DO NOT MEET THE REQUIREMENTS OF SECTION 9.1 SHALL BE EITHER, AT TO DO TODAY’S SOLE OPTION, (I) WITH RESPECT TO GOODS, THE REPAIR OF THE GOOD OR DELIVERY OF A REPLACEMENT GOOD OF THE SAME ORIGINAL QUALITY OR THE CREDITING OF THE MEMBER’S ACCOUNT IN AN AMOUNT EQUAL TO TO DO TODAY’S CHARGE THEREFOR OR THE COST OF THE GOOD, AND (II) WITH RESPECT TO SERVICES, THE PERFORMANCE OF THE SERVICE OR THE CREDITING OF THE MEMBER’S ACCOUNT IN AN AMOUNT EQUAL TO TO DO TODAY’S CHARGE THEREFOR.

Article 10 - Financial Conditions
Orders for products and services placed with TO DO TODAY and delivered to the Conciergerie are subject to a single monthly invoice. A statement showing all of the products and services ordered over the course of the month shall be sent to the Member by e-mail by the beginning of the following month.

The amount invoiced shall be paid by the Member upon receipt. 

Where applicable, the amount of the annual Membership fee is indicated in the information documents given to the Member. The annual fee is included in the first monthly invoice sent following the Effective Date and in the first monthly invoice sent during each Renewal Period.

If a payment request sent by TO DO TODAY is not honored, the Member shall be responsible for any fees and expenses paid by TO DO TODAY to recover the sum (including any legal fees and expenses) and shall be invoiced to the Member in addition to the unpaid sum.

The cost of services ordered remotely through web portal or mobile application (i.e., lifestyle and assistance services) may require that payment be made directly to the Service Provider at the time the service is performed. 

Article 11 - Obligations of TO DO TODAY
Subject to Article 9.1, TO DO TODAY shall endeavor to ensure that the Concierge Services are available during the hours advertised and of high quality, whether performed by TO DO TODAY or a Service Provider.

TO DO TODAY shall not be held responsible: 
- if the Member uses the Concierge Services incorrectly (incorrect service address, delivery method defined incorrectly, etc.); 
- if the Member breaches his or her obligations pursuant to this Agreement; 
- in the event of a Force Majeure as described in Section 13.2 below.

Article 12 - Obligations of the Member
The Member is responsible for paying all of the sums due under this Agreement, and for fulfilling all of the obligations under this Agreement.

The Member shall inform TO DO TODAY within seven (7) business days of any change or modification to the information provided in the Membership form attached to this Agreement.  

In particular, the Member shall inform TO DO TODAY if he or she leaves the site where the Conciergerie is installed or leaves his or her employer; so that TO DO TODAY can close his or her account.

The Member agrees not to directly or indirectly solicit and/or use the Service Providers independently and outside of this Agreement.

Article 13 - Liability
13.1 Product information
The product and service descriptions provided to the Member are based on the information provided by TO DO TODAY’s Service Providers and suppliers and does not intend to be comprehensive. The photographs, graphics and texts used to illustrate the products and services are for illustrative purposes only. TO DO TODAY shall not be held responsible, and the validity of an order placed may not be disputed, in the event of any errors or inaccuracy in any of the foregoing.

13.2 Force Majeure
TO DO TODAY shall not be liable or responsible to a Member, nor be deemed to have defaulted or breached this Agreement, for any failure or delay in fulfilling or performing any term of this Agreement when and to the extent such failure or delay is caused by or results from acts or circumstances beyond the reasonable control of a Member including, without limitation, acts of God, flood, fire, earthquake, explosion, governmental actions, war, invasion or hostilities (whether war is declared or not), terrorist threats or acts, riot, or other civil unrest, national emergency, revolution, insurrection, epidemic, lockouts, strikes or other labor disputes (whether or not relating to either party’s workforce), or restraints or delays affecting carriers or inability or delay in obtaining supplies of adequate or suitable materials, materials or telecommunication breakdown or power outage (any such event shall be considered a “Force Majeure”).

13.3 Liability of TO DO TODAY
TO DO TODAY shall have no liability to the Member other than as set forth in Section 9.2. 

THE MEMBER HEREBY ACKNOWLEDGES THAT INCIDENTAL, CONSEQUENTIAL, PUNITIVE AND EXEMPLARY DAMAGES ARE HEREBY SPECIFICALLY EXCLUDED. 

Article 14 - Intellectual Property
Any full or partial reproduction of TO DO TODAY’s website or other promotional materials is strictly prohibited. TO DO TODAY and its logo are registered trademarks and protected by trademark law. They are the exclusive property of TO DO TODAY and may not be reproduced by the Member.

Article 15 - Use of personal information
The information provided by the Member under this Agreement and for his or her orders will not be disclosed to any third parties, with the exception of TO DO TODAY’s suppliers and Service Providers. TO DO TODAY and its suppliers and Service Providers will treat this information as strictly confidential. It will be used by TO DO TODAY’s internal staff, suppliers and Service Providers only to handle orders.

Notwithstanding the foregoing, the Member expressly authorizes TO DO TODAY to inform his or her employer in the event the Member fails to pay one or more invoices after a reminder notice. In this case, the information provided will include the Member’s first and last names and the date and amount of the unpaid invoice. However, in no event shall employer be liable for any unpaid monies due and owing from Member. 
An entity or person that purchases all or substantially all of the assets of TO DO TODAY or is the successor of the business of TO DO TODAY by consolidation or merger shall not be considered a third party for purposes of this section and the information provided under this Agreement may be disclosed to such entity or person, provided that Client has consented to the assignment of this Agreement to such successor business.

The Member has the right to access, correct or delete any personal information provided to TO DO TODAY. To exercise this right, the Member may send a request to TO DO TODAY by letter or email, including his or her first and last names and address. If the Member removes information required to perform or to bill the Services, the Member must provide accurate updated information. The Member will not be able to place an order for Services until such updated information is provided.

Article 16 - Termination
If the Member breaches his or her obligations under this Agreement, fails to provide updated information as required in Article 12, or fails to pay any of the monthly order invoices, TO DO TODAY shall provide written notice of non-compliance. If the Member does not cure the breach within seven (7) business days, TO DO TODAY may terminate the Agreement.

TO DO TODAY may terminate this Agreement effective immediately in the following cases: 
- termination for any reason of the Service Agreement with the Corporate Client;
- termination of the Member’s employment with the Corporate Client, from the effective date of termination;
- departure of the Member from the Client site 

Following the expiration or termination of this Agreement, the Member will no longer have access to the products and Services supplied by TO DO TODAY. The annual Membership fee the Member may have paid will be retained by TO DO TODAY. 

TO DO TODAY shall fulfill any orders placed by the Member prior to expiration or termination of this Agreement, even if fulfillment is to take place after the date of expiration or termination.

Article 17 - Amendments
TO DO TODAY reserves the right to adjust or modify the terms of this Agreement upon written notice to the Members but only with respect to the provision of future Services. 

Article 18 - Governing Law
This Agreement, and all matters arising out of or relating to this Agreement, is governed by, and is to be construed in accordance with, the laws of the State of Massachusetts without regard to the conflict of laws provisions thereof to the extent such principles or rules would require or permit the application of the laws of any jurisdiction other than those of the State of Massachusetts.

Article 19 - Notice
All notices required to be provided to TO DO TODAY shall be made either by email to info@todotoday.com or by mail to TO DO TODAY at 4304 East-West Highway, Bethesda, Maryland, 20814.
EOF
            );
        $manager->persist($cgv);

        $manager->flush();
        // @codingStandardsIgnoreStop
    }
}
