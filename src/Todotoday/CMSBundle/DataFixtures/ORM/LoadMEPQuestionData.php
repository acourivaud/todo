<?php declare(strict_types=1);
/**
 * PHP version 7
 */

namespace Todotoday\CMSBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Todotoday\CMSBundle\Entity\Question;

/**
 * Class LoadQuestionData
 * @package Todotoday\CMSBundle\DataFixtures\ORM
 * @SuppressWarnings(PHPMD)
 */
class LoadMEPQuestionData extends AbstractFixture
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        // @codingStandardsIgnoreStart
        $questions = array(
            array(
                'q' => 'Comment changer les informations de mon compte?',
                'a' => <<<EOF
Depuis l’application ou le portail, rendez-vous dans l’onglet « Mon Compte » afin d’avoir accès à vos données personnelles et complémentaires. Vous avez la possibilité d’apporter toutes les modifications nécessaires.
EOF
                ,
            ),
            array(
                'q' => 'Comment sont utilisées les données personnelles de mon compte ?',
                'a' => <<<EOF
TO DO TODAY instaure une relation de confiance avec ses adhérents. Les données confiées sont utilisées pour permettre la bonne exécution des prestations. Elles peuvent être également utilisées à des fins commerciales mais uniquement en interne, afin de vous faire profiter d’offres exclusives dont seuls bénéficient nos adhérents.
EOF
                ,
            ),
            array(
                'q' => 'Comment puis-je contacter ma Conciergerie?',
                'a' => <<<EOF
Vous pouvez prendre contact avec votre Concierge de différentes manières. Vous pouvez utiliser le numéro de téléphone dédié à votre Conciergerie ou bien envoyer un mail. Vous pouvez également lancer une discussion en direct sur le tchat TO DO TODAY depuis le portail et l’application.
EOF
                ,
            ),
            array(
                'q' => 'Pour quelles types de demandes puis-je contacter mon Concierge ?',
                'a' => <<<EOF
Votre Concierge est votre assistant personnel au quotidien. Chaque demande est prise en considération et étudiée précisément afin d’y répondre au mieux. Que ce soit pour confier votre veste au pressing, faire nettoyer votre voiture, réaliser une démarche administrative ou effectuer une demande personnelle, TO DO TODAY est là pour répondre à tous vos besoins.
EOF
                ,
            ),
            array(
                'q' => 'Je me suis trompé(e), j\'aimerais modifier ou annuler ma commande en cours, est-ce possible ?',
                'a' => <<<EOF
Depuis l’application ou le portail, rendez-vous dans le menu « Mes commandes » et appuyer sur le bouton «Annuler» de votre commande. Seules les commandes encore non validées par le concierge peuvent être annulées.
EOF
                ,
            ),
            array(
                'q' => 'Quels sont les moyens de paiement acceptés par TO DO TODAY?',
                'a' => <<<EOF
En tant qu’adhérent TO DO TODAY, vous êtes débité en début de chaque mois de toutes les prestations consommées à travers la Conciergerie. De façon exceptionnelle, et faisant office de précision écrite, TO DO TODAY accepte également les chèques et cartes bancaires
EOF
                ,
            ),
            array(
                'q' => 'Quelles sont les conditions d\'annulation ?',
                'a' => ''
                ,
            ),
            array(
                'q' => 'Quel est le délai de traitement d\'une commande?',
                'a' => <<<EOF
Les délais de traitement sont directement liés aux horaires d’ouverture de votre Conciergerie. Il faut compter 2 à 3 jours ouvrés.
EOF
                ,
            ),
            array(
                'q' => 'Comment puis-je utiliser les casiers ?',
                'a' => <<<EOF
Les casiers sont à votre disposition pour le dépôt et le retrait de vos commandes. Un code personnel vous est remis lors de votre inscription et vous permet d’utiliser les casiers en toute sécurité. Vous pouvez ensuite gérer et suivre vos commandes depuis l’application ou le portail TO DO TODAY de votre Conciergerie.
EOF,

            ),
            array(
                'q' => 'Qu\'est ce qu\'une fiche grain de sable ?',
                'a' => ''
                ,
            ),
            array(
                'q' => 'Je souhaite consulter mon dernier relevé d\'opérations. Où aller ?',
                'a' => <<<EOF
La Conciergerie à distance vous permet d’accéder aux services TO DO TODAY en dehors des horaires d’ouverture de votre Conciergerie. Elle est disponible du lundi au vendredi, entre 8h et 20h au XX.XX.XX.XX.XX
EOF
                ,
            ),
            array(
                'q' => 'Comment prendre un rendez-vous bien-être ?',
                'a' => <<<EOF
Depuis l’application ou le portail, rendez-vous dans le menu « Produits & Services », naviguez dans la famille « Une vie en pleine forme », choisissez votre prestation ainsi qu'un créneau horaire. Enfin validez votre choix
EOF
                ,
            ),
            array(
                'q' => 'Comment adhérer à la Conciergerie ou résilier mon compte?',
                'a' => <<<EOF
L’adhésion à la Conciergerie TO DO TODAY est très simple. Il vous suffit de vous rendre sur le portail de votre Conciergerie et de suivre les étapes d'inscription en renseignant les différents champs proposés. Cela ne prend pas plus de 5 minutes. Quant à la résiliation de votre compte, elle est tout aussi simple: contactez votre Concierge pour lui faire part de votre décision et il s’occupe de supprimer votre compte et les données associées.
EOF
                ,
            ),
            array(
                'q' => 'Je ne me souviens plus de mon identifiant et/ou mot de passe',
                'a' => <<<EOF
Vous n’arrivez pas à vous rappeler de votre mot de passe d’accès à votre espace perso ? Pas de panique, vous pouvez cliquer très simplement sur « mot de passe oublié ». Nous vous demandons ensuite de saisir une adresse email à laquelle vous sera envoyé très rapidement un lien vous permettant de créer un nouveau mot de passe.
EOF
                ,
            ),

        );
        // @codingStandardsIgnoreStop
        foreach ($questions as $question) {
            $question = (new Question())
                ->setTitle($question['q'])
                ->setAnswer($question['a']);
            $manager->persist($question);
        }
        $manager->flush();
    }
}
