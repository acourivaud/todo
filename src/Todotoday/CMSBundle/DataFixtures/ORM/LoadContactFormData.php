<?php declare(strict_types=1);
/**
 * PHP version 7
 */

namespace Todotoday\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Todotoday\CMSBundle\Entity\ContactForm;
use Todotoday\CMSBundle\Enum\ReceiverEnum;

/**
 * Class LoadQuestionData
 * @package Todotoday\CoreBundle\DataFixtures\ORM
 */
class LoadContactFormData extends AbstractFixture
{
    /**
     *
     */
    const PREFIX = 'contact.content.';

    /**
     * @var int[]
     */
    protected static $contactFormsSlugs;

    /**
     * @return int[]
     */
    public static function getContactForms(): array
    {
        if (!static::$contactFormsSlugs) {
            static::$contactFormsSlugs = array(
                'article' => 100000000,
                'information' => 100000001,
                'order' => 100000002,
                'invoice' => 100000003,
                'problem' => 100000004,
                'other' => 100000005,
                'booking_slot' => 100000006,
                'chat' => 100000007,
                'suggest' => 100000008,
            );
        }

        return static::$contactFormsSlugs;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     *
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     */
    public function load(ObjectManager $manager)
    {
        $modelContactForm = (new ContactForm())
            ->setEmail('technique-tdtd@actiane.com');

        foreach (static::getContactForms() as $slug => $tag) {
            $contactForm = clone $modelContactForm;
            $contactForm->setLabel(self::PREFIX . $slug)
                ->setCategorieTag($tag)
                ->setReceiver(ReceiverEnum::getNewInstance(ReceiverEnum::randomEnum()));
            $manager->persist($contactForm);
        }
        $manager->flush();
    }
}
