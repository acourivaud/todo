<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 18/05/17
 * Time: 17:36
 */

namespace Todotoday\CMSBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Sonata\MediaBundle\Entity\MediaManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAccountData;
use Todotoday\ClassificationBundle\DataFixtures\ORM\LoadCategoryData;
use Todotoday\CMSBundle\Entity\LinkPostAgency;
use Todotoday\CMSBundle\Entity\Post;
use Todotoday\CMSBundle\Enum\LinkPostAgencyStateEnum;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyLinkedMicrosoft;
use Todotoday\MediaBundle\Entity\Media;
use Sonata\AdminBundle\Admin\AdminInterface;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Todotoday\CMSBundle\Enum\PostStateEnum;

/**
 * @SuppressWarnings(PHPMD)
 * Class LoadMEPArticles
 * @package Todotoday\CoreBundle\DataFixtures\ORM
 */
class LoadMEPArticles extends AbstractFixture implements DependentFixtureInterface, ContainerAwareInterface
{
    /**
     * @var MediaManager
     */
    private $media;

    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var array
     */
    static protected $posts = array();

    /**
     * @return array
     */
    public static function getPosts(): array
    {
        // @codingStandardsIgnoreStart
        if (empty(static::$posts)) {
            static::$posts = array_reverse(
                array(
                    array(
                        'fileName' => 'visuel_actu-portail_fdm_rectangle.jpg',
                        'agencies' => 'EPA',
                        'translation' => array(
                            'fr' => array(
                                'title' => 'Fête des Mères',
                                'subtitle' => 'Mamans aux mille et un visages',
                                'content' => <<<EOF
<p>Maman coquette ou encore maman gourmande…</p>
<p>TO DO TODAY célèbre toutes les mamans le 28 mai 2017. </p>
<br>
<p>La Conciergerie vous propose deux offres exclusives et spécialement conçues pour vous.</p>
<br>
<br>
<p>Les mamans coquettes apprécieront le sautoir Odicé de la créatrice Anaïre bijoux, réalisation unique pour les 
adhérents TO DO TODAY.</p>
<p><strong>Sautoir Odicé - 28€ au lieu de 32€*</strong></p>
<br>
<p>Les mamans gourmandes auront le plaisir d'un réveil en douceur grâce à la livraison d'un petit déjeuner à domicile 
ou d'un bon brunch par notre partenaire labellevie.com</p>
<p><strong>*Petit déjeuner  pour 2 personnes - 14,90€*</strong></p>
<p><strong>Brunch pour 2 personnes- 21,90€*</strong></p>
<p>Le petit bonus ? Votre maman recevra également un petit message personnalisé de votre part.</p>
<br>
<p>Vous avez la possibilité de commander également des portions supplémentaires. </p>
<p>Prenez contact avec votre Concierge pour toute commande.</p>
EOF
                                ,
                            ),
                        ),
                    ),
                    array(
                        'fileName' => 'visuel_actu-portail_fdm_rectangle.jpg',
                        'agencies' => 'EPA',
                        'translation' => array(
                            'fr' => array(
                                'title' => 'Fête des Mères',
                                'subtitle' => 'Mamans aux mille et un visages',
                                'content' => <<<EOF
<p>Maman coquette ou encore maman gourmande…</p>
<p>TO DO TODAY célèbre toutes les mamans le 28 mai 2017. </p>
<br>
<br>
<p>La Conciergerie vous propose deux offres exclusives et spécialement conçues pour vous.</p>
<br>
<p>Les mamans coquettes apprécieront le pendentif de la créatrice Anaïre bijoux.</p>
<p><strong>Sautoir Odicé - 28€ au lieu de 32€</strong></p>
<br>
<p>Les mamans gourmandes auront le plaisir d'un réveil en douceur grâce à la livraison d'un petit déjeuner à domicile 
spécialement élaboré par notre partenaire baguette à bicyclette</p>
<br>
<p><strong>Petite faim - 8,50€</strong></p>
<p><strong>Petit dej classique - 11,50€*</strong></p>
<p><strong>Petit dej gourmand - 15€</strong></p>
<br>
<p>La livraison est incluse pour la ville de Lyon. 3,50€ de frais de livraison pour le Grand Lyon. Prenez contact avec 
votre Concierge pour toute commande. </p>
EOF
                                ,
                            ),
                        ),
                    ),
                    array(
                        'fileName' => 'visuel_actu-portail_fdm_rectangle.jpg',
                        'agencies' => 'EPA',
                        'translation' => array(
                            'fr' => array(
                                'title' => 'Fête des Mères',
                                'subtitle' => 'Mamans aux mille et un visages',
                                'content' => <<<EOF
<p>Toutes les mamans méritent d'être célébrées le 28 mai prochain…TO DO TODAY vous aide à trouver la petite attention 
qui lui fera plaisir</p>
<br>
<p>La Conciergerie vous propose une offre exclusive et spécialement conçue pour vous.</p>
<br>
<p>Les mamans coquettes apprécieront le sautoir Odicé de la créatrice Anaïre bijoux, réalisation unique pour les 
adhérents TO DO TODAY.</p>
<p><strong>Sautoir Odicé - 28€ au lieu de 32€</strong></p>
EOF
                                ,
                            ),
                            'en' => array(
                                'title' => 'Mother\'s day Mum of thousand faces',
                                'subtitle' => 'Mum of thousand faces',
                                'content' => <<<EOF
<p>Every mum deserves to be celebrated on the next 28th of may…TO DO TODAY helps you to get the little attention that 
will please her.</p>
<p>The Conciergerie proposes to you an exclusive offer.</p>
<p>Every coquette mum would enjoy the Odicé Necklace, created by Anaïre Jewlery only for TO DO TODAY adherents.</p>
<p><strong>Odicé Necklace - 28€ instead of 32€</strong></p> 
EOF
                                ,
                            ),
                        ),
                    ),
                    array(
                        'fileName' => 'Image_portail-fetemusique.jpg',
                        'agencies' => 'EPA,EPC,ERA',
                        'translation' => array(
                            'fr' => array(
                                'title' => 'Fête de la musique',
                                'subtitle' => <<<EOF
<p><strong>Les bons plans TO DO TODAY</strong></p>

<p>Vous n'avez pas pu obtenir de places pour les concerts de Coldplay, U2, ou Céline Dion ? La Conciergerie peut vous 
aider...</p>
EOF
                                ,
                                'content' => <<<EOF
<p>C'est avec un peu d'avance  que TO DO TODAY célèbre la fête de la musique 2017.</p>
<p>Vous souhaitiez voir sur scène les légendes de Gun N'Roses, chanter "pour que tu m'aimes encore" avec Céline Dion, 
rêver avec Coldplay ou encore vous époumoner sur "Sunday bloody Sunday" avec U2 mais... vous n'avez pas réussi à 
obtenir de places pour les concerts les plus prisés de l'été ?</p>
<p>TO DO TODAY vous offre une chance de vous rattraper et vous propose les dernières places pour les Concerts de ces légendes de la musique.</p>
<p>
Dates des concerts: <br>
<strong>Gun N'Roses</strong>: 7.07.2017 à Paris<br>
<strong>Coldplay</strong>: 15 & 16.2017 à Paris<br>
<strong>Céline Dion</strong>: 15 et 16.2017 à Paris / 20.07.2017 à Nice<br>
<strong>U2</strong>: 25 & 26.07.2917 à Paris<br>
</p>
Renseignements et réservations auprès de votre Conciergerie.
EOF
                                ,
                            ),
                        ),
                    ),
                    array(
                        'fileName' => 'fizzup-image-portail.jpg',
                        'agencies' => LoadAgencyLinkedMicrosoft::getAgenciesLinkedSlug(),
                        'translation' => array(
                            'fr' => array(
                                'title' => 'Fête des pères',
                                'subtitle' => <<<EOF
<p><strong>PAPA EN PLEINE FORME</strong></p>
<p>TO DO TODAY vous présente FizzUp, l'appli de coaching personnalisée, pour une fête des pères sportive et musclée.</p>
EOF
                                ,
                                'content' => <<<EOF
<p>Cette année, votre Conciergerie célèbre les papas en pleine forme et vous propose une offre exceptionnelle avec son 
partenaire FizzUp.</p>
<p>Qui est FizzUp ?</p> 
<p>Il s'agit d'une application de coaching sportif permettant à ses utilisateurs de créer un programe personnalisé et 
adapté à leurs besoins.</p>
<p>La force de FizzUp réside dans la simplicité de réalisation des exercices depuis chez soi et sans matériel 
spécifique.</p> 
<p>Profitez d'une offre exceptionnelle sur les abonnements avec TO DO TODAY:<br>
- 6 mois = 39,99€ au lieu de 59,90€<br>
-12 mois = 49,99€ au lieu de 119,90€<br>
</p>
<p>Contactez rapidement votre Concierge pour profiter de l'offre, valable jusqu'au 30 juin 2017.</p>
EOF
                                ,
                            ),
                        ),
                    ),
                    array(
                        'fileName' => 'LOGO_TDTD_EFS-2017.png',
                        'agencies' => LoadAgencyLinkedMicrosoft::getAgenciesLinkedSlug(),
                        'translation' => array(
                            'fr' => array(
                                'title' => 'JOURNEE MONDIALE DU DON DU SANG',
                                'subtitle' => <<<EOF
<p><strong>TO DO TODAY participe à l'opération nationale #Missing type</strong></p>
<p>Du 10 au 17 juin 2017 les lettres A et O disparaissent du logo de votre Conciergerie pour la bonne cause</p>
EOF
                                ,
                                'content' => <<<EOF
<p>Le 14 juin 2017, c'est la Journée mondiale des donneurs de sang. A cette occasion, TO DO TODAY répond à l'appel de 
l'Etablissement Français du Sang et montre son soutien en particpant à l'opération #missingtype.</p>
<p>Parce que certaines lettres comptent plus que d'autres, en références aux groupes sanguins A, B, O et AB, l'EFS 
invite les enseignes et marques à ôter ces lettres de leur logo. Du 10 au 17 juin 2017, les lettres A et O 
disparaissent du logo TO DO TODAY.</p>
EOF
                                ,
                            ),
                        ),
                    ),
                )
            );
        }

        // @codingStandardsIgnoreStop
        return static::$posts;
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return array(
            LoadAgencyLinkedMicrosoft::class,
            LoadCategoryData::class,
            LoadAccountData::class,
        );
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     *
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\Security\Acl\Exception\InvalidDomainObjectException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     */
    public function load(ObjectManager $manager): void
    {
        foreach (static::getPosts() as $post) {
            $imagePath =
                $this->kernel->locateResource('@TodotodayCoreBundle/Resources/public/img/' . $post['fileName']);

            $media = new Media();
            $media->setName($post['fileName']);
            $media->setContext($this->getReference('default_context')->getId());
            $media->setCategory($this->getReference('agency_category'));
            $media->setProviderName('sonata.media.provider.image');
            $media->setBinaryContent($imagePath);

            $manager->persist($media);

            $article = (new Post())
                ->setState(PostStateEnum::getNewInstance(PostStateEnum::VALIDATE))
                ->setAuthor($this->getReference('admin'))
                ->setMedia($media);

            foreach ($post['translation'] as $lang => $value) {
                $article->setTitle($value['title'])
                    ->setSubtitle($value['subtitle'])
                    ->setContent($value['content']);
                $article->setLocale($lang);
                $manager->persist($article);
                $manager->flush();
            }

            if (is_array($post['agencies'])) {
                $this->setLinkgPostAgency($article, $post['agencies'], $manager);
            } else {
                if ($agencies = $this->getAgencies(explode(',', $post['agencies']), $manager)) {
                    $this->setLinkgPostAgency($article, $agencies, $manager);
                }
            }

            $admin = $this->container->get('todotoday_cms.admin.post');

            $this->createObjectSecurity($admin, $article);
        }

        $manager->flush();
    }

    /**
     * {@inheritdoc}
     * @throws \Symfony\Component\Security\Acl\Exception\InvalidDomainObjectException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     */
    public function createObjectSecurity(AdminInterface $admin, Post $object): void
    {
        $secHandler = $this->container->get('sonata.admin.security.handler');
        // retrieving the ACL for the object identity
        $objectIdentity = ObjectIdentity::fromDomainObject($object);
        $acl = $secHandler->getObjectAcl($objectIdentity);
        if (null === $acl) {
            $acl = $secHandler->createAcl($objectIdentity);
        }

        $secHandler->addObjectClassAces($acl, $secHandler->buildSecurityInformation($admin));
        $secHandler->updateAcl($acl);
        if ($object->getState()->get() !== PostStateEnum::DRAFT) {
            $secHandler->deleteObjectSecurity($admin, $object);
        }
    }

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     *
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     */
    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
        $this->kernel = $container->get('kernel');
        $this->media = $container->get('sonata.media.manager.media');
    }

    /**
     * @param Post          $article
     * @param array         $agencies
     * @param ObjectManager $manager
     *
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     */
    private function setLinkgPostAgency(Post $article, array $agencies, ObjectManager $manager): void
    {
        foreach ($agencies as $slug => $agency) {
            $linkAgency = (new LinkPostAgency())
                ->setPost($article)
                ->setAgency($this->getReference('agency_' . $slug))
                ->setPublicationDate(new \DateTime())
                ->setState(LinkPostAgencyStateEnum::getNewInstance(LinkPostAgencyStateEnum::PUBLISHED))
                ->setEditable(true);
            $manager->persist($linkAgency);
        }
    }

    /**
     * @param array         $epa
     * @param ObjectManager $manager
     *
     * @return array
     */
    private function getAgencies(array $epa, ObjectManager $manager): ?array
    {
        $repo = $manager->getRepository('TodotodayCoreBundle:Agency');
        if ($agencies = $repo->getEPAAgencies($epa)) {
            foreach ($agencies as $agency) {
                $result[$agency->getSlug()] = $agency->getName();
            }

            return $result;
        }

        return null;
    }
}
