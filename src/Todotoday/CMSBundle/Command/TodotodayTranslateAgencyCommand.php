<?php declare(strict_types=1);

namespace Todotoday\CMSBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class TodotodayTranslateAgencyCommand
 * @package Todotoday\CMSBundle\Command
 */
class TodotodayTranslateAgencyCommand extends ContainerAwareCommand
{
    /**
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function configure(): void
    {
        $this
            ->setName('todotoday:translate:agency')
            ->setDescription('Migrate agency information into its translate state');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     * @throws \LogicException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();
        $agencyRepo = $doctrine->getRepository('TodotodayCoreBundle:Agency');
        /** @var Agency[] $agencies */
        $agencies = $agencyRepo->findAll();

        foreach ($agencies as $agency) {
            $information = $agency->getInformation();
            $quickInfo = $agency->getInformation();
            $schedule = $agency->getSchedule();
            $agency->setLocale($agency->getDefaultLanguage());
            $agency->setInformation($information)
                ->setQuickInfo($quickInfo)
                ->setSchedule($schedule);
            $em->flush();
        }

        $output->writeln('Done');
    }
}
