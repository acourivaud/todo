<?php declare(strict_types=1);

namespace Todotoday\CMSBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Todotoday\CMSBundle\Entity\Cgv;

/**
 * Class TodotodayTranslateCgvCommand
 * @package Todotoday\CMSBundle\Command
 */
class TodotodayTranslateCgvCommand extends ContainerAwareCommand
{
    /**
     * do configure
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function configure(): void
    {
        $this
            ->setName('todotoday:translate:cgv')
            ->setDescription('Migrate cgv into its translate state');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     * @throws \LogicException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();
        $cgvRepo = $doctrine->getRepository('TodotodayCMSBundle:Cgv');
        /** @var Cgv[] $cgvs */
        $cgvs = $cgvRepo->findAll();

        $cgvLocalised = new Cgv();
        $cgvLocalised->setLocale('fr');

        foreach ($cgvs as $cgv) {
            $cgvToPersist = clone $cgvLocalised;
            $cgvToPersist->setContent($cgv->getContent())
                ->setModel($cgv->getModel())
                ->setLocale('fr');
            $em->persist($cgvToPersist);
            $em->remove($cgv);
        }
        $em->flush();
        $output->writeln('Done');
    }
}
