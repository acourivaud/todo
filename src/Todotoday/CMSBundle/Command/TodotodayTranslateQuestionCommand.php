<?php declare(strict_types=1);

namespace Todotoday\CMSBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Todotoday\CMSBundle\Entity\Question;

/**
 * Class TodotodayTranslateQuestionCommand
 * @package Todotoday\CMSBundle\Command
 */
class TodotodayTranslateQuestionCommand extends ContainerAwareCommand
{
    /**
     * do configure
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function configure(): void
    {
        $this
            ->setName('todotoday:translate:question')
            ->setDescription('Migrate questions into its translate state');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     * @throws \LogicException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();
        $questionRepo = $doctrine->getRepository('TodotodayCMSBundle:Question');
        /** @var Question[] $questions */
        $questions = $questionRepo->findAll();

        $questionLocalised = new Question();
        $questionLocalised->setLocale('fr');

        foreach ($questions as $question) {
            $questionToPersist = clone $questionLocalised;
            $questionToPersist->setTitle($question->getTitle())
                ->setAnswer($question->getAnswer())
                ->setLocale('fr');
            $em->persist($questionToPersist);
            $em->remove($question);
        }
        $em->flush();
        $output->writeln('Done');
    }
}
