<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 11/05/17
 * Time: 17:16
 */

namespace Todotoday\CMSBundle\Serializer;

use JMS\Serializer\Context;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\JsonSerializationVisitor;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\HttpFoundation\RequestStack;
use Todotoday\CoreBundle\Entity\DeliveryMode;

/**
 * Class SubjectTranslationHandler
 * @package Todotoday\CoreBundle\Serializer
 */
class SubjectTranslationHandler implements SubscribingHandlerInterface
{
    /**
     * @var string
     */
    const DOMAIN = 'todotoday';

    /**
     * @var Translator
     */
    private $translator;

    /**
     * @var string
     */
    private $locale;

    /**
     * DeliveryModeHandler constructor.
     *
     * @param Translator   $translator
     * @param RequestStack $stack
     */
    public function __construct(Translator $translator, RequestStack $stack)
    {
        $this->translator = $translator;
        $this->locale = $stack->getCurrentRequest()->getLocale();
    }

    /**
     * @return array
     */
    public static function getSubscribingMethods()
    {
        return array(
            array(
                'direction' => GraphNavigator::DIRECTION_SERIALIZATION,
                'format' => 'json',
                'type' => SubjectTranslationHandler::class,
                'method' => 'deliveryModeToJson',
            ),
        );
    }

    /**
     * @param JsonSerializationVisitor $visitor
     * @param string                   $subject
     * @param array                    $type
     * @param Context                  $context
     *
     * @return string
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     */
    public function deliveryModeToJson(
        JsonSerializationVisitor $visitor,
        string $subject,
        array $type,
        Context $context
    ) {

        $name = $this->getKey($subject, null);

        return $visitor->visitArray(
            [
                'slug' => $subject,
                'name' => $this->translate($name),
            ],
            $type,
            $context
        );
    }

    /**
     * @param string $key
     *
     * @return null|string
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     */
    private function translate(string $key): ?string
    {
        $translation = $this->translator->trans($key, [], self::DOMAIN, $this->locale);

        return $translation === $key ? null : $translation;
    }

    /**
     * Format key depending on slug and type
     *
     * @param string      $slug
     * @param string|null $type
     *
     * @return string
     */
    private function getKey(string $slug, ?string $type): string
    {
        $separator = $type ? '_' : '';

        return $type . $separator . $slug;
    }
}
