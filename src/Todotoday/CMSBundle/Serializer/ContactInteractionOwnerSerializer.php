<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 12/05/17
 * Time: 16:27
 */

namespace Todotoday\CMSBundle\Serializer;

use Doctrine\Common\Util\Inflector;
use JMS\Serializer\Context;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\JsonSerializationVisitor;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\HttpFoundation\RequestStack;
use Todotoday\CMSBundle\Entity\SerializerHandler\InteractionReceiver;
use Todotoday\CMSBundle\Enum\ReceiverEnum;
use Todotoday\CoreBundle\Interfaces\Services\DomainContext\DomainContextInterface;

/**
 * Class ContactInteractionOwnerSerializer
 * @package Todotoday\CMSBundle\Serializer
 */
class ContactInteractionOwnerSerializer implements SubscribingHandlerInterface
{
    /**
     * @var DomainContextInterface
     */
    private $domainContext;

    /**
     * ContactSubjectSerializer constructor.
     *
     * @param DomainContextInterface $domainContext
     */
    public function __construct(DomainContextInterface $domainContext)
    {
        $this->domainContext = $domainContext;
    }

    /**
     * @return array
     */
    public static function getSubscribingMethods()
    {
        return array(
            array(
                'direction' => GraphNavigator::DIRECTION_SERIALIZATION,
                'format' => 'json',
                'type' => InteractionReceiver::class,
                'method' => 'contactToJson',
            ),
        );
    }

    /**
     * @param JsonSerializationVisitor $visitor
     * @param ReceiverEnum             $receiver
     * @param array                    $type
     * @param Context                  $context
     *
     * @return mixed
     */
    public function contactToJson(
        JsonSerializationVisitor $visitor,
        ReceiverEnum $receiver,
        array $type,
        Context $context
    ) {
        $target = $receiver->get();
        $getter = 'getCrm' . Inflector::camelize($target);
        $ownerId = $this->domainContext->getAgency()->{$getter}();

        return $visitor->visitArray(
            [
                'target' => $target,
                'ownerId' => $ownerId,
            ],
            $type,
            $context
        );
    }
}
