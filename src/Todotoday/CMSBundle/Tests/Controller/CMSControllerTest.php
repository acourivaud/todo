<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 13/05/17
 * Time: 09:40
 */

namespace Todotoday\CMSBundle\Tests\Controller;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Symfony\Component\Routing\Route;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAdherentLinkedMicrosoftData;
use Todotoday\CMSBundle\Controller\CMSController;
use Todotoday\CMSBundle\DataFixtures\ORM\LoadMEPQuestionData;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyLinkedMicrosoft;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class CMSControllerTest
 * @package Todotoday\CMSBundle\Tests\Controller
 */
class CMSControllerTest extends WebTestCase
{
    /**
     * @dataProvider agencyProvider
     *
     * @param string $agencySlug
     */
    public function testNews(string $agencySlug): void
    {
        /** @var Agency $agency */
        $agency = self::getFRR()->getReference('agency_' . $agencySlug);
        $url = 'http://' . $agency->getSlug() . '.todotoday.lol' . $this->getUrl('cms_all_news');
        $client = $this->makeClient();
        $client->request('GET', $url);
        $this->assertStatusCode(200, $client);
    }

    /**
     * @dataProvider agencyProvider
     *
     * @param string $agencySlug
     */
    public function testFaq(string $agencySlug): void
    {
        /** @var Agency $agency */
        $agency = self::getFRR()->getReference('agency_' . $agencySlug);
        $url = 'http://' . $agency->getSlug() . '.todotoday.lol' . $this->getUrl('cms_concierge_page_faq');
        $client = $this->makeClient();
        $client->request('GET', $url);
        $this->assertStatusCode(200, $client);
    }

    /**
     * @dataProvider agencyProvider
     *
     * @param string $agencySlug
     */
    public function testContact(string $agencySlug): void
    {
        /** @var Agency $agency */
        $agency = self::getFRR()->getReference('agency_' . $agencySlug);
        $url = 'http://' . $agency->getSlug() . '.todotoday.lol' . $this->getUrl('cms_concierge_page_contact');
        $client = $this->makeClient();
        $client->request('GET', $url);
        $this->assertStatusCode(200, $client);
    }

    /**
     * @dataProvider agencyProvider
     *
     * @param string $agencySlug
     *
     * @throws \Doctrine\Common\DataFixtures\OutOfBoundsException
     */
    public function testInfo(string $agencySlug): void
    {
        /** @var Agency $agency */
        $agency = self::getFRR()->getReference('agency_' . $agencySlug);
        $url = 'http://' . $agency->getSlug() . '.todotoday.lol' . $this->getUrl('cms_concierge_page_info');
        $client = $this->makeClient();
        $client->request('GET', $url);
        $this->assertStatusCode(200, $client);
    }

    /**
     * @small
     * @dataProvider agencyProvider
     *
     * @param string $agencySLug
     *
     * @throws \Doctrine\Common\DataFixtures\OutOfBoundsException
     */
    public function testIndex(string $agencySLug): void
    {
        /** @var Agency $agency */
        $agency = self::getFRR()->getReference('agency_' . $agencySLug);

        $url = 'http://' . $agency->getSlug() . '.todotodolol.com' . $this->getUrl('index_cms_concierge');
        $client = $this->makeClient();
        $client->request('GET', $url);
        $this->assertStatusCode(200, $client);
    }

    /**
     * @dataProvider routeProvider
     * @small
     *
     * @param string $route
     */
    public function testNotFoundForIndexWihtoutAgency(string $route): void
    {
        $url = 'http://todotoday.lol' . $this->getUrl($route);
        $client = $this->makeClient();
        $client->request('GET', $url);
        $this->assertStatusCode(404, $client);
    }

    /**
     * @return array
     */
    public function agencyProvider(): array
    {
        $provider = [];
        foreach (LoadAdherentLinkedMicrosoftData::getAdherentsSlug() as $agencySlug => $adherentSlug) {
            $provider[$agencySlug] = array($agencySlug, $adherentSlug);
        }

        return $provider;
    }

    /**
     * @return array
     */
    public function routeProvider(): array
    {
        $provider = [];
        $routeCollection =
            $this->getContainer()->get('routing.loader')->load(CMSController::class);

        /** @var Route $routes */
        foreach ($routeCollection->all() as $routeName => $route) {
            // we exclude route with param
            if ($routeName === 'index_cms_concierge' || strpos($route->getPath(), '{')) {
                continue;
            }
            $provider[$routeName] = array($routeName);
        }

        return $provider;
    }

    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAgencyLinkedMicrosoft::class,
            LoadAdherentLinkedMicrosoftData::class,
            LoadMEPQuestionData::class,
        );
    }
}
