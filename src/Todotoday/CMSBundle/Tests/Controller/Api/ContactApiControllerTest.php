<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 13/05/17
 * Time: 11:33
 */

namespace Todotoday\CMSBundle\Tests\Controller\Api;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Actiane\UnitFonctionalTestBundle\Traits\AccessDeniedForNotAdherentTestableTrait;
use Actiane\UnitFonctionalTestBundle\Traits\AccessGrantedForAdherentMicrosoftTestableTrait;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAccountData;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAdherentLinkedMicrosoftData;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyLinkedMicrosoft;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class ContactApiControllerTest
 * @package Todotoday\CMSBundle\Tests\Controller\Api
 */
class ContactApiControllerTest extends WebTestCase
{
    use AccessDeniedForNotAdherentTestableTrait, AccessGrantedForAdherentMicrosoftTestableTrait;

    /**
     * @dataProvider notAdherentProvider
     *
     * @param string $adherentSlug
     */
    public function testAccessDeniedForNotAdherent(string $adherentSlug): void
    {
        $url = 'http:' . $this->getUrl('todotoday.cms.api.contact.get');
        $this->accessDeniedForNotAdherent($adherentSlug, 'api', $url, 'arevalta');
    }

    /**
     * @dataProvider adherentMicrosoftProvider
     *
     * @param string $agencySLug
     * @param string $adherentSlug
     */
    public function testAccessGrantedForAdherent(string $agencySLug, string $adherentSlug)
    {
        /** @var Agency $agency */
        $agency = self::getFRR()->getReference('agency_' . $agencySLug);
        $url = 'http:' . $this->getUrl('todotoday.cms.api.contact.get');
        $this->succesRoute($adherentSlug, 'api', $url, $agency->getSlug());
    }

    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAccountData::class,
            LoadAgencyLinkedMicrosoft::class,
            LoadAdherentLinkedMicrosoftData::class,
        );
    }
}
