<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 03/11/17
 * Time: 16:11
 */

namespace Todotoday\CMSBundle\Tests\Controller\Api;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Actiane\UnitFonctionalTestBundle\Traits\AccessGrantedForAdherentMicrosoftTestableTrait;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAdherentLinkedMicrosoftData;
use Todotoday\CMSBundle\DataFixtures\ORM\LoadMEPArticles;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyLinkedMicrosoft;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class PostControllerTest
 * @package Todotoday\CMSBundle\Tests\Controller\Api
 */
class PostControllerTest extends WebTestCase
{
    use AccessGrantedForAdherentMicrosoftTestableTrait;

    /**
     * @small
     */
    public function testGetPostSuccess()
    {
        /** @var Agency $agency */
        $agency = self::getFRR()->getReference('agency_carrefour-massy');
        $url = 'http:' . $this->getUrl('todotoday.cms.api.post.get');
        $this->succesRoute('adherent_linked_carrefour-massy', 'main', $url, $agency->getSlug());
    }

    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return [
            LoadAgencyLinkedMicrosoft::class,
            LoadMEPArticles::class,
            LoadAdherentLinkedMicrosoftData::class,
        ];
    }
}
