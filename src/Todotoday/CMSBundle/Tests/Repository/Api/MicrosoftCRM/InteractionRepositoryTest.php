<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 16/04/17
 * Time: 18:09
 */

namespace Todotoday\CMSBundle\Tests\Repository\Api\MicrosoftCRM;

use Actiane\ApiConnectorBundle\Tests\Repository\Api\AbstractRepositoryTestCase;

/**
 * Class InteractionRepositoryTest
 * @package Todotoday\CMSBundle\Tests\Repository\Api\MicrosoftCRM
 */
class InteractionRepositoryTest extends AbstractRepositoryTestCase
{
    /**
     * Do getRepositoryId
     *
     * @return string
     */
    protected function getRepositoryId(): string
    {
        return 'todotoday.cms.repository.api.interaction';
    }
}
