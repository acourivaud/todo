<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 26/04/17
 * Time: 13:18
 */

namespace Todotoday\CMSBundle\Tests\Services;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAdherentLinkedMicrosoftData;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CMSBundle\Entity\Api\MicrosoftCRM\Interaction;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyData;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class InteractionManagerTest
 * @package Todotoday\CMSBundle\Tests\Services
 */
class InteractionManagerTest extends WebTestCase
{
    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAgencyData::class,
            LoadAdherentLinkedMicrosoftData::class,
        );
    }

    /**
     * @group ignore
     * @smallzz
     */
    public function testCreateAdherentInteraction()
    {
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent_linked_arevalta');
        /** @var Agency $agency */
        $agency = $adherent->getLinkAgencies()->first()->getAgency();
        $manager = $this->getContainer()->get('todotoday.cms.service.interaction_manager');
        $result = $manager->createAdherentInteraction(
            $adherent,
            100000003,
            $agency->getCrmServiceClient(),
            'Test unitaire depuis le front',
            'contenu de mon test'
        );
        $this->assertNotNull($result);
        $this->assertInstanceOf(Interaction::class, $result);
    }
}
