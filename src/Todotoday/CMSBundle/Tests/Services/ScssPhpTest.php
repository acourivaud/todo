<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 20/01/2017
 * Time: 12:04
 */

namespace Todotoday\CMSBundle\Tests\Services;

use Liip\FunctionalTestBundle\Test\WebTestCase;
use Todotoday\CMSBundle\Services\ScssPhp;

class ScssPhpTest extends WebTestCase
{
    /**
     * @var ScssPhp
     */
    private $scssPhp;

    /**
     * ScssPhpTest constructor.
     *
     * @param null   $name
     * @param array  $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->scssPhp = new ScssPhp();
    }

    /**
     * @return array
     */
    public function compileProvider()
    {
        // TODO : Ajouter des cas supplementaires
        return [
            [
                '$font-stack:    Helvetica, sans-serif;
                    $primary-color: #333;
            
                body {
                  font: 100% $font-stack;
                  color: $primary-color;
                }',
                'body{font:100% Helvetica,sans-serif;color:#333}',
            ],
        ];
    }

    /**
     * @return array
     */
    public function minificationProvider()
    {
        // TODO : Ajouter des cas supplementaires
        return [
            [
                'body {
                    font: 100% Helvetica, sans-serif;
                     color: #333;
                 }',
                'body{font:100% Helvetica,sans-serif;color:#333}',
            ],
        ];
    }

    /**
     * @param string $css
     * @param string $cssMin
     *
     * @Small
     * @dataProvider compileProvider
     */
    public function testCompile(string $css, string $cssMin)
    {
        $this->assertEquals($cssMin, $this->scssPhp->minify($this->scssPhp->transpile($css)));
    }

    /**
     * @param string $css
     * @param string $cssMin
     *
     * @Small
     * @dataProvider minificationProvider
     */
    public function testMinification(string $css, string $cssMin)
    {
        $this->assertEquals($cssMin, $this->scssPhp->minify($css));
    }
}
