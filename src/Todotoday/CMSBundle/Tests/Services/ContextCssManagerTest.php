<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 20/01/2017
 * Time: 17:29
 */

namespace Todotoday\CMSBundle\Tests\Services;

use Liip\FunctionalTestBundle\Test\WebTestCase;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Entity\Style;

/**
 * Class ContextCssManagerTest
 *
 * @package    Todotoday\CMSBundle
 * @subpackage Todotoday\CMSBundle\Tests\Services
 */
class ContextCssManagerTest extends WebTestCase
{
    /**
     * @return array
     */
    public function createCssProvider()
    {
        $concierge = $this->createMock(Agency::class);

        $concierge->method('getSlug')->willReturn('conciergerie-a');

        // TODO : Ajouter des cas supplementaires
        return [
            [
                'body {
                    font: 100% Helvetica, sans-serif;
                     color: #666;
                 }',
                $concierge,
            ],
        ];
    }

    /**
     * @return array
     */
    public function mergeDBScssProvider()
    {
        $concierge = $this->createMock(Agency::class);
        $style = new Style();

        $style->setMainFontColor('#123456');
        $style->setMainBackground('#123456');

        $concierge->method('getStyle')->willReturn($style);
        $concierge->method('getSlug')->willReturn('conciergerie-a');

        return [
            [
                $concierge,
            ],
        ];
    }

    /**
     * @param string $scss
     * @param Agency $agency
     *
     * @Small
     * @dataProvider createCssProvider
     */
    public function testCreateCss(string $scss, Agency $agency)
    {
        $ccCss = $this->getContainer()->get('todotay.cms.services.context_css_manager');

        $ccCss->createCss($scss, $agency);

        static::assertTrue(true);
    }

    /**
     * @param Agency $agency
     *
     * @Small
     * @dataProvider mergeDBScssProvider
     */
    public function mergeDBScss(Agency $agency)
    {
        $ccCss = $this->getContainer()->get('todotay.cms.services.context_css_manager');

        $ccCss->mergeDBScss($agency);
    }

    /**
     * @param Agency $agency
     *
     * @Small
     * @dataProvider mergeDBScssProvider
     */
    public function fullconfig(Agency $agency)
    {
        $ccCss = $this->getContainer()->get('todotay.cms.services.context_css_manager');

        $scss = $ccCss->mergeDBScss($agency);

        $ccCss->createCss($scss, $agency);
    }
}
