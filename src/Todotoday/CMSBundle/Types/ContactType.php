<?php declare(strict_types=1);

namespace Todotoday\CMSBundle\Types;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Todotoday\CMSBundle\Entity\ContactForm;

/**
 * Class ContactType
 * @package Todotoday\CMSBundle\Types
 */
class ContactType extends AbstractType
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * ContactType constructor.
     *
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @throws \Symfony\Component\Validator\Exception\ConstraintDefinitionException
     * @throws \Symfony\Component\Validator\Exception\InvalidOptionsException
     * @throws \Symfony\Component\Validator\Exception\MissingOptionsException
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($this->tokenStorage->getToken()->getUser() === 'anon.') {
            $builder->add(
                'lastname',
                EmailType::class,
                array(
                    'label' => 'form.your_email',
                    'translation_domain' => 'todotoday',
                )
            )
                ->add(
                    'firstname',
                    TextType::class,
                    array(
                        'label' => 'form.your_firstname',
                        'constraints' => array(
                            new
                            NotBlank(),
                        ),
                        'translation_domain' => 'todotoday',
                    )
                )
                ->add(
                    'email',
                    TextType::class,
                    array(
                        'label' => 'form.your_lastname',
                        'constraints' => array(
                            new NotBlank(),
                        ),
                        'translation_domain' => 'todotoday',
                    )
                );
        }

        $builder
            ->add(
                'subject',
                EntityType::class,
                array(
                    'label' => 'form.contact_subject',
                    'class' => ContactForm::class,
                    'choice_label' => 'label',
                    'choice_translation_domain' => 'todotoday',
                    'placeholder' => 'form.contact_subject_default',
                    'translation_domain' => 'todotoday',
                )
            )
//            Si jamais on veut rajouter un titre au message en plus de la rubrique
//            ->add(
//                'subject',
//                TextType::class,
//                array(
//                    'label' => 'form.contact_subject_default',
//                    'attr' => array(
//                        'placeholder' => 'form.contact_subject_default',
//                    ),
//                    'constraints' => array(
//                        new NotBlank(array('message' => '')),
//                    ),
//                    'translation_domain' => 'todotoday',
//                )
//            )
            ->add(
                'message',
                TextareaType::class,
                array(
                    'label' => 'form.your_message',
                    //                    'attr' => array('placeholder' => 'form.your_message'),
                    'constraints' => array(
                        new NotBlank(),
                    ),
                    'translation_domain' => 'todotoday',
                )
            );
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'error_bubbling' => true,
            )
        );
    }
}
