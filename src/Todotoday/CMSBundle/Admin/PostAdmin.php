<?php declare(strict_types=1);

namespace Todotoday\CMSBundle\Admin;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\QueryBuilder;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Sonata\FormatterBundle\Form\Type\SimpleFormatterType;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Todotoday\CMSBundle\Entity\LinkPostAgency;
use Todotoday\CMSBundle\Entity\Post;
use Todotoday\CMSBundle\Enum\LinkPostAgencyStateEnum;
use Todotoday\CMSBundle\Enum\PostStateEnum;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Services\DomainContextManager;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

/**
 * Class PostAdmin
 * @SuppressWarnings(PHPMD.UnusedLocalVariable)
 *
 * @package    Todotoday\CMSBundle
 * @subpackage Todotoday\CMSBundle\Admin
 */
class PostAdmin extends AbstractAdmin
{
    /**
     * @var DomainContextManager
     */
    private $domainContextManager;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @var bool
     */
    protected $persistFilters;

    /**
     * @var array
     */
    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'DESC',
        '_sort_by' => 'id',
    );

    /**
     * LinkPostAgencyAdmin constructor.
     *
     * @param string                $code
     * @param string                $class
     * @param string                $baseControllerName
     * @param DomainContextManager  $domainContextManager
     * @param TokenStorageInterface $tokenStorage
     * @param ObjectManager         $objectManager
     */
    public function __construct(
        string $code,
        string $class,
        string $baseControllerName,
        DomainContextManager $domainContextManager,
        TokenStorageInterface $tokenStorage,
        ObjectManager $objectManager
    ) {
        parent::__construct($code, $class, $baseControllerName);

        $this->domainContextManager = $domainContextManager;
        $this->tokenStorage = $tokenStorage;
        $this->objectManager = $objectManager;
        $this->persistFilters = true;
    }

    /**
     *
     */
    public function configure()
    {
        parent::configure(); // TODO: Change the autogenerated stub
        $this->setTemplate('edit', ':Admin:edit_test.html.twig');
    }

    /**
     * @param string $action
     * @param Mixed  $object
     *
     * @return array
     */
    public function configureActionButtons($action, $object = null): array
    {
        $list = parent::configureActionButtons($action, $object);
        $list['origin'] = [
            'template' => 'TodotodayCMSBundle:Admin:Post/post_admin_list_view.html.twig',
        ];

        return $list;
    }

    /**
     * {@inheritdoc}
     * @throws \RuntimeException
     */
    public function generateUrl(
        $name,
        array $parameters = array(),
        $absolute = UrlGeneratorInterface::ABSOLUTE_PATH
    ): string {
        if ($name === 'create') {
            try {
                if ($agenciesStrategy = $this->getRequest()->get('agencies_strategy')) {
                    $parameters = array_merge($parameters, ['agencies_strategy' => $agenciesStrategy]);
                }
            } catch (\RuntimeException $e) {
            }
        }

        return parent::generateUrl($name, $parameters, $absolute);
    }

    /**
     * {@inheritdoc}
     * @throws \UnexpectedValueException
     */
    public function createQuery($context = 'list')
    {
        /** @var ProxyQueryInterface|QueryBuilder $query */
        $query = parent::createQuery($context);

        if ($agency = $this->domainContextManager->getCurrentContext()->getAgency()) {
            $query
                ->join($query->getRootAliases()[0] . '.agencies', 'link_post_agency')
                ->andWhere('link_post_agency.agency = :agency')
                ->setParameter('agency', $agency);
        }

        return $query;
    }

    /**
     * Do getNewInstance
     *
     * @return Post
     * @throws \RuntimeException
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getNewInstance(): Post
    {
        /** @var Post $post */
        $post = parent::getNewInstance();

        if (!$post->getId()
            && $post->getAgencies()->isEmpty()
            && $agency = $this->domainContextManager->getCurrentContext()->getAgency()
        ) {
            $post->addAgency(
                $link = (new LinkPostAgency())
                    ->setEditable(true)
                    ->setAgency($agency)
                    ->setState(LinkPostAgencyStateEnum::getNewInstance(LinkPostAgencyStateEnum::PUBLISHED))
                    ->setPublicationDate(new \DateTime())
            );
        }
        $post->setAuthor($this->tokenStorage->getToken()->getUser());
        $post->setState(PostStateEnum::getNewInstance(PostStateEnum::DRAFT));

        if ($this->isGranted('ROLE_COMMUNITY_MANAGER')
            && $agencies = $this->getAgencyByStrategy(
                $this->getRequest()->get('agencies_strategy')
            )
        ) {
            foreach ($agencies as $agency) {
                $post->addAgency(
                    $link = (new LinkPostAgency())
                        ->setEditable(true)
                        ->setAgency($agency)
                        ->setState(LinkPostAgencyStateEnum::getNewInstance(LinkPostAgencyStateEnum::PUBLISHED))
                        ->setPublicationDate(new \DateTime())
                );
            }
        }

        return $post;
    }

    /**
     * @param string $strategy
     *
     * @return array|null
     */
    protected function getAgencyByStrategy(?string $strategy = 'empty'): ?array
    {
        $repo = $this->objectManager->getRepository(Agency::class);

        if ($strategy === 'all') {
            return $repo->findAll();
        }

        if ($strategy === 'us') {
            return $repo->getEPAAgencies(['EUS']);
        }

        if ($strategy === 'ch') {
            return $repo->getEPAAgencies(['ECH']);
        }

        if ($strategy === 'fr') {
            return $repo->getEPAAgencies(['EPA', 'ERA']);
        }

        return null;
    }

    /**
     * @param DatagridMapper $datagridMapper
     *
     * @throws \RuntimeException
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('id')
            ->add('title')
            ->add('subtitle', null, ['label' => 'admin_post_short_description_label'])
            ->add('synopsis', null, ['label' => 'admin_post_subtitle_label'])
            ->add('state')
            ->add('plugin')
            ->add('content')
            ->add('slug')
            ->add('agencies.publicationDate', 'doctrine_orm_date_range')
            ->add('agencies.agency', null, ['label' => 'Agency'], null, ['multiple' => true])
            ->add(
                'origin',
                'doctrine_orm_callback',
                [
                    'label' => 'post_origin',
                    'callback' => function ($queryBuilder, string $alias, string $field, array $value) {
                        if ($value['value'] === 'post_plugin') {
                            /** @var QueryBuilder $queryBuilder */
                            $queryBuilder->andWhere($alias . '.plugin is not null');

                            return true;
                        }
                        if ($value['value'] === 'post_tdtd') {
                            /** @var QueryBuilder $queryBuilder */
                            $queryBuilder->andWhere($alias . '.plugin is null');

                            return true;
                        }

                        return false;
                    },
                ],
                'choice',
                [
                    'choices' => [
                        'post_tdtd' => 'post_tdtd',
                        'post_plugin' => 'post_plugin',
                    ],
                ]
            );
    }

    /**
     * @param FormMapper $formMapper
     *
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->add('title')
//            ->add('slug')
            ->add(
                'subtitle',
                SimpleFormatterType::class,
                array(
                    'label' => 'admin_post_short_description_label',
                    'format' => 'richhtml',
                    'ckeditor_toolbar_icons' => array(
                        array(
                            'Bold',
                            'Italic',
                            'Underline',
                            '-',
                            'Cut',
                            'Copy',
                        ),
                    ),
                )
            )
            ->add('synopsis', TextareaType::class, [
                'label' => 'admin_post_subtitle_label',
            ])
            ->add(
                'content',
                SimpleFormatterType::class,
                array(
                    'format' => 'richhtml',
                    'ckeditor_toolbar_icons' => [[
                        'Bold', 'Italic', 'Underline', 'TextColor',
                        '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord',
                        '-', 'Undo', 'Redo',
                        '-', 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent',
                        '-', 'Blockquote',
                        '-', 'Image', 'Link', 'Unlink', 'Table', ],
                        ['Maximize', 'Source'],
                    ],
                )
            )
            ->add('media', ModelListType::class, array('required' => true));
        if ($this->isGranted('ROLE_COMMUNITY_MANAGER')) {
            $formMapper
                ->add(
                    'agencies',
                    CollectionType::class,
                    array('by_reference' => false),
                    array(
                        'edit' => 'inline',
                        'inline' => 'table',
                        'sortable' => 'position',
                    )
                );
        }
    }

    /**
     * @param ShowMapper $show
     *
     * @throws \RuntimeException
     */
    protected function configureShowFields(ShowMapper $show)
    {
        $show->add('title')
            ->add('slug')
            ->add(
                'subtitle',
                'html'
            )
            ->add(
                'synopsis',
                'html'
            )
            ->add(
                'content',
                'html'
            )
            ->add('media', ModelListType::class);
    }

    /**
     * @param ListMapper $listMapper
     *
     * @throws \RuntimeException
     */
    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('id')
            ->add('title')
            ->add('author')
            ->add('plugin', null, ['label' => 'Plugin'])
            ->add(
                'subtitle',
                'string',
                [
                    'template' => 'TodotodayCMSBundle:Admin/Post:subtitle_list_view.html.twig',
                    'label' => 'admin_post_short_description_label',
                ]
            )
//            ->add('createdAt')
            ->add('updatedAt')
//            ->add('agencies','collection')
            ->add('state', 'trans')
            ->add(
                '_action',
                null,
                array(
                    'actions' => array(
                        'preview' => array(
                            'template' => 'TodotodayCMSBundle:Admin/Post:preview.html.twig',
                        ),
                        'edit' => array(),
                        'delete' => array(),
                        'switch_state' => array(
                            'template' => 'TodotodayCMSBundle:Admin/Post:switch_state.html.twig',
                        ),
                        'show' => array(),
                    ),
                )
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection): void
    {
        $collection
            ->add(
                'show_question_answer',
                sprintf('%s/show_question_answer', $this->getRouterIdParameter())
            )
            ->add('switch_state', $this->getRouterIdParameter() . '/switch/{state}')
            ->add('preview', $this->getRouterIdParameter() . '/preview')
            ->add('origin', 'origin/{value}');
    }
}
