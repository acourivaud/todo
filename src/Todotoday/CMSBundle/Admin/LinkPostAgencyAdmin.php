<?php declare(strict_types=1);

namespace Todotoday\CMSBundle\Admin;

use Doctrine\ORM\QueryBuilder;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\CMSBundle\Enum\LinkPostAgencyStateEnum;
use Todotoday\CoreBundle\Services\DomainContextManager;

/**
 * Class LinkPostAgencyAdmin
 *
 * @package    Todotoday\CMSBundle
 * @subpackage Todotoday\CMSBundle\Admin
 */
class LinkPostAgencyAdmin extends AbstractAdmin
{
    /**
     * @var DomainContextManager
     */
    private $domainContextManager;

    /**
     * LinkPostAgencyAdmin constructor.
     *
     * @param string               $code
     * @param string               $class
     * @param string               $baseControllerName
     * @param DomainContextManager $domainContextManager
     */
    public function __construct($code, $class, $baseControllerName, DomainContextManager $domainContextManager)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->domainContextManager = $domainContextManager;
    }

    /**
     * {@inheritdoc}
     * @throws \UnexpectedValueException
     */
    public function createQuery($context = 'list')
    {
        /** @var ProxyQueryInterface|QueryBuilder $query */
        $query = parent::createQuery($context);

        if ($agency = $this->domainContextManager->getCurrentContext()->getAgency()) {
            $query->andWhere($query->getRootAliases()[0] . '.agency = :agency')
                ->setParameter('agency', $agency);
        }

        return $query;
    }

    /**
     * @param DatagridMapper $datagridMapper
     *
     * @throws \RuntimeException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('post')
            ->add('agency')
            ->add('publicationDate', 'doctrine_orm_date_range')
            ->add(
                'post.author',
                'doctrine_orm_choice',
                array(
                    'field_options' => array(
                        'choices' => $this->getAuthorFilter(),
                        'required' => false,
                        'multiple' => true,
                        'expanded' => false,
                    ),
                    'field_type' => 'choice',
                )
            )
            ->add('state');
    }

    /**
     * @param FormMapper $formMapper
     *
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
//            ->add('post')
            ->add(
                'agency',
                ModelListType::class,
                array(
                    'btn_add' => false,
                    'btn_delete' => false,
                )
            )
//            ->add('post.author')
            ->add(
                'state',
                ChoiceType::class,
                array(
                    'choices' => LinkPostAgencyStateEnum::getChoices(),
                    'attr' => array(
                        'class' => 'state-to-copy',
                    ),
                )
            )
            ->add(
                'publicationDate',
                DateTimePickerType::class,
                array(
                    'dp_use_seconds' => false,
                    'dp_side_by_side' => true,
                    'attr' => array(
                        'class' => 'publication-date-to-copy',
                    ),
                )
            );
    }

    /**
     * @param ListMapper $listMapper
     *
     * @throws \RuntimeException
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('post')
            ->add('agency')
            ->add('post.author')
            ->add('state', 'trans')
            ->add('publicationDate')
            ->add(
                '_action',
                null,
                array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                        'publish_unpublish' => array(
                            'template' => 'TodotodayCMSBundle:Admin/LinkPostAgency:publish_unpublish.html.twig',
                        ),
                    ),
                )
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        parent::configureRoutes($collection);

        $collection->add('switch_state', $this->getRouterIdParameter() . '/switch/{state}');
    }

    /**
     * @param ShowMapper $showMapper
     *
     * @throws \RuntimeException
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('post')
            ->add('agency')
            ->add('post.author')
            ->add('state')
            ->add('publicationDate');
    }

    /**
     * @return mixed
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     */
    private function getAuthorFilter()
    {
        $repo = $this->getConfigurationPool()->getContainer()
            ->get('doctrine')->getRepository('TodotodayAccountBundle:Account');
        $authors = $repo->getPostAuthor();

        $authorChoices = array();
        /** @var Account[] $authors */
        foreach ($authors as $author) {
            $authorChoices[$author->getFullName()] = $author->getId();
        }

        return $authorChoices;
    }
}
