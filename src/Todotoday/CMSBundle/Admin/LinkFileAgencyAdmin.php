<?php declare(strict_types=1);

namespace Todotoday\CMSBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\AdminBundle\Show\ShowMapper;

class LinkFileAgencyAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     *
     * @throws \RuntimeException
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('file')
            ->add('agency')
            ->add('visibility');
    }

    /**
     * @param ListMapper $listMapper
     *
     * @throws \RuntimeException
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('file')
            ->add('agency')
            ->add('visibility')
            ->add(
                '_action',
                null,
                array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    ),
                )
            );
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add(
                'agency',
                ModelListType::class,
                array(
                    'btn_add' => false,
                    'btn_delete' => false,
                )
            )
            ->add('visibility');
    }

    /**
     * @param ShowMapper $showMapper
     *
     * @throws \RuntimeException
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('file')
            ->add('agency')
            ->add('visibility');
    }
}
