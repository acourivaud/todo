<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 09/07/17
 * Time: 17:33
 */

namespace Todotoday\CMSBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\FormatterBundle\Form\Type\SimpleFormatterType;

/**
 * Class CgvAdmin
 * @package Todotoday\CMSBundle\Admin
 */
class CgvAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $filter
     *
     * @throws \RuntimeException
     */
    protected function configureDatagridFilters(DatagridMapper $filter): void
    {
        $filter->add('model')
            ->add('content');
    }

    /**
     * @param FormMapper $form
     */
    protected function configureFormFields(FormMapper $form): void
    {
        $form
            ->add('title', null, [
                'required' => true,
            ])
            ->add('model')
            ->add(
                'content',
                SimpleFormatterType::class,
                array(
                    'format' => 'markdown',
                    'attr' => array(
                        'style' => 'height:500px;',
                    ),
                )
            );

        if (($tl = $this->getRequest()->get('tl')) && $tl !== 'fr') {
            $form
                ->add(
                    'media' . ucfirst($tl),
                    ModelListType::class,
                    array('btn_delete' => false),
                    array('link_parameters' => array('context' => 'file'))
                );
        } elseif (($loc = $this->getRequest()->getLocale()) !== 'fr') {
            $form
                ->add(
                    'media' . ucfirst($loc),
                    ModelListType::class,
                    array('btn_delete' => false),
                    array('link_parameters' => array('context' => 'file'))
                );
        } else {
            $form
                ->add(
                    'media',
                    ModelListType::class,
                    array('btn_delete' => false),
                    array('link_parameters' => array('context' => 'file'))
                );
        }
    }

    /**
     * @param ListMapper $list
     *
     * @throws \RuntimeException
     */
    protected function configureListFields(ListMapper $list): void
    {
        $list->add('model');
        if (($tl = $this->getRequest()->get('tl')) && $tl !== 'fr') {
            $list
                ->add(
                    'media' . ucfirst($tl),
                    null,
                    ['label' => 'linked_file']
                );
        } elseif (($loc = $this->getRequest()->getLocale()) !== 'fr') {
            $list
                ->add(
                    'media' . ucfirst($loc),
                    null,
                    ['label' => 'linked_file']
                );
        } else {
            $list
                ->add(
                    'media',
                    null,
                    ['label' => 'linked_file']
                );
        }

        $list->add(
            '_action',
            null,
            array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            )
        );
    }

    /**
     * @param ShowMapper $show
     *
     * @throws \RuntimeException
     */
    protected function configureShowFields(ShowMapper $show): void
    {
        $show
            ->add('model')
            ->add('content');
    }
}
