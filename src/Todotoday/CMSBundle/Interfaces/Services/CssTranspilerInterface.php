<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 20/01/2017
 * Time: 11:48
 */

namespace Todotoday\CMSBundle\Interfaces\Services;

interface CssTranspilerInterface
{
    /**
     * @param string $cssCode
     *
     * @return string
     */
    public function minify(string $cssCode): string;

    /**
     * @param string $scssCode
     *
     * @return string
     */
    public function transpile(string $scssCode): string;
}
