<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 11/08/17
 * Time: 15:57
 */

namespace Todotoday\CMSBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CountryAgencyType
 *
 * @package Todotoday\CMSBundle\Form
 */
class CountryAgencyType extends AbstractType
{
    /**
     * @param OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            array(
                'choices' => array(
                    'country_agency_all' => null,
                    'France' => 'EPA,ERA',
                    'Canada' => 'ECA',
                    'Suisse' => 'ECH',
                    'US' => 'EUS',
                ),
                'translation_domain' => 'messages',
            )
        );
    }

    /**
     * @return string
     */
    public function getParent(): string
    {
        return ChoiceType::class;
    }
}
