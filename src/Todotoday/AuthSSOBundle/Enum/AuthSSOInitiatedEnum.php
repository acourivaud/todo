<?php declare(strict_types=1);


namespace Todotoday\AuthSSOBundle\Enum;

use Actiane\ToolsBundle\Enum\AbstractEnum;

/**
 * Class AuthSSOInitiatedEnum
 *
 * @package Todotoday\AuthSSOBundle\Enum
 */
class AuthSSOInitiatedEnum extends AbstractEnum
{
    public const IDP = 'idp';
    //public const SP = 'sp';
}
