<?php declare(strict_types=1);

namespace Todotoday\AuthSSOBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Todotoday\AuthSSOBundle\Enum\AuthSSOInitiatedEnum;

/**
 * AuthProvider
 *
 * @SuppressWarnings(PHPMD)
 * @ORM\Table(name="auth_provider", schema="core")
 * @ORM\Entity(repositoryClass="Todotoday\AuthSSOBundle\Repository\AuthProviderRepository")
 */
class AuthProvider
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Agency
     *
     * @ORM\ManyToOne(targetEntity="Todotoday\CoreBundle\Entity\Agency")
     * @ORM\JoinColumn(nullable=true)
     */
    private $agency;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;

    /**
     * @var AuthSSOInitiatedEnum
     *
     * @ORM\Column(name="initiated", type="AuthSSOInitiatedEnum")
     */
    private $initiated;

    /**
     * @var string
     *
     * @ORM\Column(name="sp_servicename", type="string")
     */
    private $spServicename;

    /**
     * @var string
     *
     * @ORM\Column(name="sp_servicedescription", type="string")
     */
    private $spServicedescription;

    /**
     * @var string
     *
     * @ORM\Column(name="sp_entityid", type="string")
     */
    private $spEntityid;

    /**
     * @var string
     *
     * @ORM\Column(name="sp_acsurl", type="string")
     */
    private $spAcsurl;

    /**
     * @var string
     *
     * @ORM\Column(name="sp_slourl", type="string")
     */
    private $spSlourl;
    /**
     * @var string
     *
     * @ORM\Column(name="sp_nameidformat", type="string", nullable=true)
     */
    private $spNameidformat;

    /**
     * @var string
     *
     * @ORM\Column(name="idp_entityid", type="string", nullable=true)
     */
    private $idpEntityid;

    /**
     * @var string
     *
     * @ORM\Column(name="idp_ssourl", type="string", nullable=true)
     */
    private $idpSsourl;

    /**
     * @var string
     *
     * @ORM\Column(name="idp_slourl", type="string", nullable=true)
     */
    private $idpSlourl;

    /**
     * @var string
     *
     * @ORM\Column(name="idp_sloresponseurl", type="string", nullable=true)
     */
    private $idpSloresponseurl;

    /**
     * @var string
     *
     * @ORM\Column(name="idp_x509cert", type="text")
     */
    private $idpX509cert;

    /**
     * @var string
     *
     * @ORM\Column(name="uuid", type="string", nullable=true)
     */
    private $uuid;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", nullable=true)
     */
    private $language;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set agency.
     *
     * @param int $agency
     *
     * @return AuthProvider
     */
    public function setAgency($agency)
    {
        $this->agency = $agency;

        return $this;
    }

    /**
     * Get agency.
     *
     * @return int
     */
    public function getAgency()
    {
        return $this->agency;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Agency
     */
    public function setCreatedAt(\DateTime $createdAt = null): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Set initiated.
     *
     * @param string $initiated
     *
     * @return AuthProvider
     */
    public function setInitiated($initiated)
    {
        $this->initiated = $initiated;

        return $this;
    }

    /**
     * Get initiated.
     *
     * @return string
     */
    public function getInitiated()
    {
        return $this->initiated;
    }

    /**
     * Set spServicename.
     *
     * @param string $spServicename
     *
     * @return AuthProvider
     */
    public function setSpServicename($spServicename)
    {
        $this->spServicename = $spServicename;

        return $this;
    }

    /**
     * Get spServicename.
     *
     * @return string
     */
    public function getSpServicename()
    {
        return $this->spServicename;
    }

    /**
     * Set spServicedescription.
     *
     * @param string $spServicedescription
     *
     * @return AuthProvider
     */
    public function setSpServicedescription($spServicedescription)
    {
        $this->spServicedescription = $spServicedescription;

        return $this;
    }

    /**
     * Get spServicedescription.
     *
     * @return string
     */
    public function getSpServicedescription()
    {
        return $this->spServicedescription;
    }

    /**
     * Set spEntityid.
     *
     * @param string $spEntityid
     *
     * @return AuthProvider
     */
    public function setSpEntityid($spEntityid)
    {
        $this->spEntityid = $spEntityid;

        return $this;
    }

    /**
     * Get spEntityid.
     *
     * @return string
     */
    public function getSpEntityid()
    {
        return $this->spEntityid;
    }

    /**
     * Set spAcsurl.
     *
     * @param string $spAcsurl
     *
     * @return AuthProvider
     */
    public function setSpAcsurl($spAcsurl)
    {
        $this->spAcsurl = $spAcsurl;

        return $this;
    }

    /**
     * Get spAcsurl.
     *
     * @return string
     */
    public function getSpAcsurl()
    {
        return $this->spAcsurl;
    }

    /**
     * Set spSlourl.
     *
     * @param string $spSlourl
     *
     * @return AuthProvider
     */
    public function setSpSlourl($spSlourl)
    {
        $this->spSlourl = $spSlourl;

        return $this;
    }

    /**
     * Get spSlourl.
     *
     * @return string
     */
    public function getSpSlourl()
    {
        return $this->spSlourl;
    }

    /**
     * Set spNameidformat.
     *
     * @param string $spNameidformat
     *
     * @return AuthProvider
     */
    public function setSpNameidformat($spNameidformat)
    {
        $this->spNameidformat = $spNameidformat;

        return $this;
    }

    /**
     * Get spNameidformat.
     *
     * @return string
     */
    public function getSpNameidformat()
    {
        return $this->spNameidformat;
    }

    /**
     * Set idpEntityid.
     *
     * @param string $idpEntityid
     *
     * @return AuthProvider
     */
    public function setIdpEntityid($idpEntityid)
    {
        $this->idpEntityid = $idpEntityid;

        return $this;
    }

    /**
     * Get idpEntityid.
     *
     * @return string
     */
    public function getIdpEntityid()
    {
        return $this->idpEntityid;
    }

    /**
     * Set idpSsourl.
     *
     * @param string $idpSsourl
     *
     * @return AuthProvider
     */
    public function setIdpSsourl($idpSsourl)
    {
        $this->idpSsourl = $idpSsourl;

        return $this;
    }

    /**
     * Get idpSsourl.
     *
     * @return string
     */
    public function getIdpSsourl()
    {
        return $this->idpSsourl;
    }

    /**
     * Set idpSlourl.
     *
     * @param string $idpSlourl
     *
     * @return AuthProvider
     */
    public function setIdpSlourl($idpSlourl)
    {
        $this->idpSlourl = $idpSlourl;

        return $this;
    }

    /**
     * Get idpSlourl.
     *
     * @return string
     */
    public function getIdpSlourl()
    {
        return $this->idpSlourl;
    }

    /**
     * Set idpSloresponseurl.
     *
     * @param string $idpSloresponseurl
     *
     * @return AuthProvider
     */
    public function setIdpSloresponseurl($idpSloresponseurl): self
    {
        $this->idpSloresponseurl = $idpSloresponseurl;

        return $this;
    }

    /**
     * Get idpSloresponseurl.
     *
     * @return string
     */
    public function getIdpSloresponseurl(): ?string
    {
        return $this->idpSloresponseurl;
    }

    /**
     * Set idpX509cert.
     *
     * @param string $idpX509cert
     *
     * @return AuthProvider
     */
    public function setIdpX509cert($idpX509cert)
    {
        $this->idpX509cert = $idpX509cert;

        return $this;
    }

    /**
     * Get idpX509cert.
     *
     * @return string
     */
    public function getIdpX509cert()
    {
        return $this->idpX509cert;
    }

    /**
     * Set uuid.
     *
     * @param string $uuid
     *
     * @return AuthProvider
     */
    public function setUuid($uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid.
     *
     * @return string
     */
    public function getUuid(): ?string
    {
        return $this->uuid;
    }

     /**
     * Set language.
     *
     * @param string $language
     *
     * @return AuthProvider
     */
    public function setLanguage($language): self
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language.
     *
     * @return string
     */
    public function getLanguage(): ?string
    {
        return $this->language;
    }
}
