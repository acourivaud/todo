<?php declare(strict_types=1);

namespace Todotoday\AuthSSOBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Cookie;
use Todotoday\AuthSSOBundle\Services\AuthSSOManager;

/**
 * Class LocaleListener
 *
 * @category   Todo-Todev
 * @package    Todotoday\AuthSSOBundle
 * @subpackage Todotoday\AuthSSOBundle\EventListener
 */
class LocaleListener implements EventSubscriberInterface
{
    /**
     * @var Session
     */
    private $session;

    /**
     * @var AuthSSOManager
     */
    private $authSSOManager;

    /**
     * LocaleListener constructor.
     *
     * @param Session        $session
     * @param AuthSSOManager $authSSOManager
     */
    public function __construct(
        Session $session,
        AuthSSOManager $authSSOManager
    ) {
        $this->session = $session;
        $this->authSSOManager = $authSSOManager;
    }

    /**
     * Do getSubscribedEvents
     *
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return array(
            // must be registered after the default Locale listener
            KernelEvents::RESPONSE => array(array('onKernelResponse', 15)),
        );
    }

    /**
     * Do onKernelResponse
     *
     * @param GetResponseEvent $event
     *
     * @return void
     * @throws \UnexpectedValueException
     */
    public function onKernelResponse(FilterResponseEvent $event): void
    {
        /*$request = $event->getRequest();
        $routeName = $request->get('_route');

        $response = $event->getResponse();
        if ($this->session->get('samlUserdata') && !$request->cookies->get('smartbanner-closed')) {
            $cookie = new Cookie('smartbanner-closed', "true", strtotime('+1 year'), '/', null, false, false);
            $response->headers->setCookie($cookie);
        } */

        $this->authSSOManager->redirectSAMLDestination($event);
    }
}
