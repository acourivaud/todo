<?php declare(strict_types=1);

namespace Todotoday\AuthSSOBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Todotoday\AuthSSOBundle\Enum\AuthSSOInitiatedEnum;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * Class AuthProviderAdmin
 */
class AuthProviderAdmin extends AbstractAdmin
{
    public $authProviderRepo;

    /**
     * @var array
     */
    protected $datagridValues = [
        '_page' => 1,
        '_sort_order' => 'DESC',
        '_sort_by' => 'id',
    ];

    /**
     * @param ListMapper $list
     *
     * @throws \RuntimeException
     */
    protected function configureListFields(ListMapper $list)
    {
        $list
            ->add('id')
            ->add('initiated')
            ->add('language')
            ->add('spServicename')
            ->add('agency')
            ->add('createdAt')
            ->add(
                '_action',
                null,
                array(
                    'actions' => array(
                        'edit' => array(),
                    ),
                )
            );
    }

    /**
     * @param FormMapper $form
     *
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     */
    protected function configureFormFields(FormMapper $form)
    {
        $form->add(
            'initiated',
            ChoiceType::class,
            array(
                    'choices' => AuthSSOInitiatedEnum::getChoices(),
                    'attr' => array(
                        'class' => 'state-to-copy',
                    ),
                )
        )
            ->add(
                'language',
                ChoiceType::class,
                array(
                    'choices' => [
                        'en' => 'en',
                        'fr' => 'fr',
                    ],
                    'attr' => array(
                        'class' => 'state-to-copy',
                    ),
                )
            )
            ->add('spServicename')
            ->add('spServicedescription')
            ->add('uuid')
            ->add(
                'agency',
                'entity',
                array(
                    'class' => 'Todotoday\CoreBundle\Entity\Agency',
                )
            )
            ->add('spEntityid')
            ->add('spAcsurl')
            ->add('spSlourl')
            ->add('idpX509cert');
    }
}
