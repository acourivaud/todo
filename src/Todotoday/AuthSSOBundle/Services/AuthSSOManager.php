<?php declare(strict_types=1);

namespace Todotoday\AuthSSOBundle\Services;

use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Entity\Concierge;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Todotoday\CoreBundle\Services\UrlGenerator;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Todotoday\AuthSSOBundle\Entity\AuthProvider;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Todotoday\AccountBundle\Entity\LinkAccountAgency;
use Symfony\Component\HttpFoundation\RedirectResponse;
use \InvalidArgumentException;
use \DOMDocument;
use \DOMXpath;
use \DateTime;

/**
 * Class AuthSSOManager
 *
 * @package Todotoday\AuthSSOBundle\Services
 */
class AuthSSOManager
{

    /**
    * @var Logger
    */
    public $logger;

    /**
     * @var string
     */
    public $uuid;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var AuthorizationChecker
     */
    private $authorization;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var UrlGenerator
     */
    private $urlGenerator;

    /**
     * @var Agency
     */
    private $agency;

    /**
     * @var Router
     */
    protected $router;

    /**
     * @var AuthProvider
     */
    protected $authProvider;

    /**
     * AuthSSOManager constructor.
     *
     * @param LoggerInterface          $logger
     * @param EntityManagerInterface   $entityManager
     * @param TokenStorage             $tokenStorage
     * @param Session                  $session
     * @param AuthorizationChecker     $authorization
     * @param EventDispatcherInterface $eventDispatcher
     * @param UrlGenerator             $urlGenerator
     * @param Router                   $router
     */
    public function __construct(
        LoggerInterface $logger,
        EntityManagerInterface $entityManager,
        TokenStorage $tokenStorage,
        Session $session,
        AuthorizationChecker $authorization,
        EventDispatcherInterface $eventDispatcher,
        UrlGenerator $urlGenerator,
        Router $router
    ) {
        $this->logger = $logger;
        $this->entityManager = $entityManager;
        $this->tokenStorage = $tokenStorage;
        $this->session = $session;
        $this->authorization = $authorization;
        $this->eventDispatcher = $eventDispatcher;
        $this->urlGenerator = $urlGenerator;
        $this->router = $router;
    }

/**
     * Extract UUID from SAML Payload
     *
     * @param Request $request
     *
     * @return string|boolean
     */
    public function extractUUID(Request $request)
    {
        if ($request->request->has('SAMLResponse')) {
            $xml = base64_decode($request->request->get('SAMLResponse'));

            $dom = new DOMDocument();
            $dom->loadXML($xml);
            $doc = $dom->documentElement;

            $xpath = new DOMXpath($dom);
            $xpath->registerNamespace('samlp', 'urn:oasis:names:tc:SAML:2.0:protocol');
            $xpath->registerNamespace('saml', 'urn:oasis:names:tc:SAML:2.0:assertion');
            $attrs = $xpath->query('/samlp:Response/saml:Assertion/saml:AttributeStatement/saml:Attribute', $doc);
            foreach ($attrs as $attr) {
                $name = $attr->getAttribute('Name');
                if ($name == 'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier') {
                    foreach ($xpath->query('saml:AttributeValue', $attr) as $value) {
                        return $value->textContent;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Get AuthProvider
     *
     * @return AuthProvider
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function getAuthProvider()
    {
        if ($this->authProvider) {
            return $this->authProvider;
        }

        return false;
    }

    /**
     * Get related Agency
     *
     * @return Agency
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function getAgency()
    {
        if ($this->agency) {
            return $this->agency;
        }

        return false;
    }

    /**
     * Get related Agency url
     *
     * @param Account $account
     *
     * @return Agency
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function getAgencyUrl(Account $account)
    {
        if ($this->agency) {
            $userLanguage = $account->getLanguage();
            $locale = ($userLanguage) ? $userLanguage : $this->authProvider->getLanguage();

            return $this->urlGenerator->generateAbsoluteUrl($this->agency->getSlug(), '/lang/' . $locale);
        }

        return false;
    }

    /**
     * Get related Agency Registration url
     *
     * @return string
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function getAgencyRegistrationUrl()
    {
        $registrationUrl = $this->router->generate('todotoday_authsso_authsso_registration');
        $returnUrl = $registrationUrl;

        if ($this->agency) {
            //$locale = $this->authProvider->getLanguage();

            $agencyUrl = $this->urlGenerator->generateAbsoluteUrl($this->agency->getSlug());
            $returnUrl = $agencyUrl . $returnUrl;
        }

        return $returnUrl;
    }

    /**
     * Set related Agency by UUID
     *
     * @param Request $request
     *
     * @return Agency
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function setAgency(Request $request)
    {
        $authProviderRepo = $this->entityManager->getRepository('TodotodayAuthSSOBundle:AuthProvider');

        if ($this->uuid = $this->extractUUID($request)) {
            if ($this->authProvider = $authProviderRepo->fetchAuthProviderByUuid($this->uuid)) {
                if ($agency = $this->authProvider->getAgency()) {
                    $this->agency = $agency;

                    return $agency;
                }
            }
        }

        return false;
    }

    /**
     * Log the user in
     *
     * @param Account $account
     * @param Request $request
     *
     * @return bool
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function login(Account $account, Request $request)
    {

         /** @var Adherent $account */
        if ($account instanceof Adherent) {
            /** @var UsernamePasswordToken $token */
            $token = $this->tokenStorage->getToken();
            $token->setUser($account);
            $newToken = new UsernamePasswordToken(
                $account,
                null,
                'main',
                $account->getRoles()
            );

            //$event = new RegistrationSuccessEvent($account);
            //$this->eventDispatcher->dispatch(RegistrationSuccessEvent::USER_REGISTRATION_SUCCESS, $event);
            $this->tokenStorage->setToken($newToken);

            return true;
        } else {
            $token = new UsernamePasswordToken($account, null, 'main', $account->getRoles());
            $this->tokenStorage->setToken($token);

            $this->session->set('_security_main', serialize($token));

            // Fire the login event manually
            $event = new InteractiveLoginEvent($request, $token);
            if ($this->eventDispatcher->dispatch("security.interactive_login", $event)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Performs the logout if requested.
     *
     * @return bool
     */
    public function logout()
    {
        $this->tokenStorage->setToken(null);
    }

    /**
     * Get the user's linked agencies
     *
     * @param Account $user
     * @return array
     */
    public function getUserAllowedAgencies($user)
    {
        $allowedAgencies = [];
        foreach ($user->getLinkAgencies()->filter(
            function (LinkAccountAgency $linkAccountAgency) {
                return null === $linkAccountAgency->getExpiredAt()
                    || $linkAccountAgency->getExpiredAt() > new DateTime();
            }
        )->getIterator() as $linkAccountAgency) {
            /** @var LinkAccountAgency $linkAccountAgency */
            $allowedAgencies[] = $linkAccountAgency->getAgency()->getId();
        }

        return $allowedAgencies;
    }

    /**
     * Log the user in
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @param FilterResponseEvent $event
     *
     * @return bool
     */
    public function redirectSAMLDestination(FilterResponseEvent $event)
    {
        $request = $event->getRequest();
        $routeName = $request->get('_route');

         /* Redirection after SAML targeted content */
        if ($token = $this->tokenStorage->getToken()) {
            $user = $token->getUser();
            if (is_object($user) && $user instanceof Account) {
                $allowedAgencies = $this->getUserAllowedAgencies($user);

                /* Do not process SAML content redirect if user is not allowed in agency */
                if ($user instanceof Adherent
                    || $user instanceof Concierge) {
                    if (!in_array($this->session->get('samlAgency'), $allowedAgencies)
                        && $this->session->has('samlDestination')) {
                        $this->session->remove('samlDestination');
                    }
                }

                /* Only process SAML content redirect if user has custom role */
                if ($this->authorization->isGranted('ROLE_TODOTODAY_AUTHSSO_REDIRECT')) {
                    if ($this->session->has('samlDestination')
                        && $routeName !== 'todotoday_authsso_authsso_login') {
                        $destination = $this->session->get('samlDestination');
                        $this->session->remove('samlDestination');
                        $event->setResponse(new RedirectResponse($destination));
                    }
                }
            }
        }
    }
}
