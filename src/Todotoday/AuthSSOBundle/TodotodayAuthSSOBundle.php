<?php declare(strict_types = 1);

namespace Todotoday\AuthSSOBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class TodotodayAuthSSOBundle extends Bundle
{
}
