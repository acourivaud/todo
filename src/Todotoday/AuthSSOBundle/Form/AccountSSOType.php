<?php declare(strict_types=1);
/**
 * PHP version 7
 */

namespace Todotoday\AuthSSOBundle\Form;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\AccountBundle\Enum\AdherentTypeEnum;
use Todotoday\AccountBundle\Form\Transformer\CustomerGroupTransformer;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class AccountSSOType
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 *
 * @package    Todotoday\AuthSSOBundle
 * @subpackage Todotoday\AuthSSOBundle\Form
 */
class AccountSSOType extends AbstractType
{
    /**
     * AccountSSOType constructor.
     *
     * @param Session $session
     */
    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    /**
     * {@inheritdoc}
     * @throws \Symfony\Component\Form\Exception\InvalidArgumentException
     * @throws \Symfony\Component\Validator\Exception\ConstraintDefinitionException
     * @throws \Symfony\Component\Validator\Exception\InvalidOptionsException
     * @throws \Symfony\Component\Validator\Exception\MissingOptionsException
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $samlUserdata = $this->session->get('samlUserdata');

        $email = '';
        $firstName = '';
        $lastName = '';

        if ($samlUserdata = $this->session->get('samlUserdata')) {
            $emailData = $samlUserdata['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress'];
            if (isset($emailData) && is_array($emailData)) {
                $email = trim($emailData[0]);
            }

            $firstNameData = $samlUserdata['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname'];
            if (isset($firstNameData) && is_array($firstNameData)) {
                $firstName = trim($firstNameData[0]);
            }

            $lastNameData = $samlUserdata['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname'];
            if (isset($lastNameData) && is_array($lastNameData)) {
                $lastName = trim($lastNameData[0]);
            }
        }

        $builder
            ->add(
                'customerGroup',
                ChoiceType::class,
                array(
                    'translation_domain' => 'todotoday',
                    'choices' => array(
                        'account.type_adherent_description' => AdherentTypeEnum::ADH,
                        'account.type_non_adherent_description' => AdherentTypeEnum::NON_ADH,
                    ),
                    'expanded' => true,
                    'required' => true,
                )
            )
            ->add(
                'email',
                EmailType::class,
                array(
                    'required' => true,
                    'label' => 'signin.email_adresse',
                    'translation_domain' => 'todotoday',
                    //'disabled' => true,
                    'data'  => $email,
                    'attr' => array(
                        'readonly' => true,
                    ),
                )
            )
            ->add(
                'firstName',
                TextType::class,
                array(
                    'required' => true,
                    'label' => 'firstname',
                    'translation_domain' => 'todotoday',
                    'data'  => $firstName,
                    'constraints' => [
                        new NotBlank(),
                    ],
                )
            )
            ->add(
                'lastName',
                TextType::class,
                array(
                    'required' => true,
                    'label' => 'lastname',
                    'translation_domain' => 'todotoday',
                    'data'  => $lastName,
                    'constraints' => [
                        new NotBlank(),
                    ],
                )
            )
            ->add(
                'plainPassword',
                RepeatedType::class,
                array(
                    'type' => PasswordType::class,
                    'options' => ['translation_domain' => 'FOSUserBundle'],
                    'first_options' => ['label' => 'form.password'],
                    'second_options' => ['label' => 'form.password_confirmation'],
                    'invalid_message' => 'fos_user.password.mismatch',
                )
            );

        $builder->get('customerGroup')->addModelTransformer(new CustomerGroupTransformer());
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): ?string
    {
        return 'todotoday_authsso_bundle_account_sso';
    }
}
