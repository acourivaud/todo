<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 23/03/2017
 * Time: 09:09
 */

namespace Todotoday\AuthSSOBundle\Form;

use Todotoday\AccountBundle\Form\AccountCgvType;
use Todotoday\AccountBundle\Form\AccountMandatoryFlow;

/**
 * Class AuthSSORegistrationFlow
 *
 * @SuppressWarnings(PHPMD.UnusedLocalVariable)
 * @category   Todo-Todev
 * @package    Todotoday\AuthSSOBundle
 * @subpackage Todotoday\AuthSSOBundle\Form
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class AuthSSORegistrationFlow extends AccountMandatoryFlow
{
    /**
     * {@inheritdoc}
     */
    protected function loadStepsConfig(): array
    {
        return array(
            array(
                'label' => 'signin.start_title',
                'form_type' => AccountSSOType::class,
                'form_options' => [
                    'validation_groups' => [
                        'Default',
                        'emailWhiteList',
                    ],
                ],
            ),
            array(
                'label' => 'signin.ariane_cgv',
                'form_type' => AccountCgvType::class,
                'form_options' => [
                    'validation_groups' => ['Default'],
                ],
            ),
        );
    }
}
