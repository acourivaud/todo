<?php declare(strict_types=1);

namespace Todotoday\AuthSSOBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration as SFConf;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\FOSUserEvents;
use Todotoday\PaymentBundle\Event\ChangeAccountRolesEvent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;
use Todotoday\AuthSSOBundle\Entity\AuthProvider;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Event\RegistrationSuccessEvent;
use Todotoday\AuthSSOBundle\Services\AuthSSOManager;
use InvalidArgumentException;
use OneLogin\Saml2\Settings as OneLogin_Saml2_Settings;
use OneLogin\Saml2\Error as OneLogin_Saml2_Error;
use OneLogin\Saml2\Auth as OneLogin_Saml2_Auth;
use OneLogin\Saml2\Utils as OneLogin_Saml2_Utils;
use Exception;
use DOMDocument;

/**
 * Class AuthSSOController
 * @package Todotoday\AuthSSOBundle\Controller
 * @Route("/auth/sso")
 */
class AuthSSOController extends Controller
{
    /**
     * @Route("/saml", methods={"POST"})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \InvalidArgumentException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \LogicException
     */
    public function samlAction(Request $request): Response
    {
        $authssoManager = $this->get('todotoday_authsso.manager');

        $postedData = $request->request->all();
        $jsonPostedData = json_encode($postedData);

        if ($postedData) {
            $logInfos = 'IP : ' . $request->getClientIp();
            $logInfos .= ' - SENT DATA : ' . $jsonPostedData;

            $authssoManager->logger->info($logInfos);
        }

        $response = new Response($jsonPostedData);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Set sessions variables from SAML datas
     *
     * @param OneLogin_Saml2_Auth $auth
     */
    public function setSessionVars($auth)
    {
        $authssoManager = $this->get('todotoday_authsso.manager');

        $document = new DOMDocument();
        $document->preserveWhiteSpace = false;
        $document->formatOutput = true;
        $document = OneLogin_Saml2_Utils::loadXML($document, $auth->getLastResponseXML());
        $destination = $document->documentElement->getAttribute('Destination');
        if ($destinationQuery = parse_url($destination, PHP_URL_QUERY)) {
            $destinationParams = [];
            parse_str($destinationQuery, $destinationParams);
            $destinationUrl = '/' . ltrim(urlencode($destinationParams['url']), '/');
            $this->get('session')->set('samlDestination', $destinationUrl);
        }

        $this->get('session')->set('samlAgency', $authssoManager->getAgency()->getId());
        $this->get('session')->set('samlUserdata', $auth->getAttributes());
        $this->get('session')->set('samlNameId', $auth->getNameId());
        $this->get('session')->set('samlNameIdFormat', $auth->getNameIdFormat());
        $this->get('session')->set('samlNameIdNameQualifier', $auth->getNameIdNameQualifier());
        $this->get('session')->set('samlNameIdSPNameQualifier', $auth->getNameIdSPNameQualifier());
        $this->get('session')->set('samlSessionIndex', $auth->getSessionIndex());
    }

    /**
     * Process login or redirect to registration if user does not exists
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @param string         $email
     * @param AuthSSOManager $authssoManager
     * @param Request        $request
     */
    public function processLoginOrRedirect(
        $email,
        AuthSSOManager $authssoManager,
        Request $request
    ) {
        $account = $this->getDoctrine()->getRepository(Account::class)->findOneBy(
            [
                'email' => $email,
            ]
        );

        if ($account) {
            /* The account exists : the user is logged in */
            if ($authssoManager->login($account, $request)) {
                if ($agencyUrl = $authssoManager->getAgencyUrl($account)) {
                    return $this->redirect($agencyUrl);
                }
            }
        } else {
            /* Log out the user first */
            $authssoManager->logout();

            $language = $authssoManager->getAuthProvider()->getLanguage();
            $request->setLocale($language);
            $request->getSession()->set('_locale', $language);

            /* The account does not exist : the user is redirected to sso registration */
            return $this->redirect($authssoManager->getAgencyRegistrationUrl());
        }
    }

    /**
     * @Route("/login", methods={"POST"})
     *
     * @param Request $request
     *
     * @return null
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \InvalidArgumentException
     * @throws \UnexpectedValueException
     * @throws \LogicException
     */
    public function loginAction(Request $request)
    {

        $authssoManager = $this->get('todotoday_authsso.manager');

        $postedData = $request->request->all();
        $jsonPostedData = json_encode($postedData);

        if ($postedData) {
            $logInfos = 'IP : ' . $request->getClientIp();
            $logInfos .= ' - SENT DATA : ' . $jsonPostedData;

            $authssoManager->logger->info($logInfos);
        }

        if (!$agency = $authssoManager->setAgency($request)) {
            throw new InvalidArgumentException('Agency is missing');
        }

        $settings = $this->getDoctrine()->getRepository(AuthProvider::class)
                                        ->getSettings($agency, $authssoManager->uuid);

        $auth = new OneLogin_Saml2_Auth($settings);

        $authNRequestID = $this->get('session')->get('AuthNRequestID');

        if (isset($authNRequestID)) {
            $requestID = $authNRequestID;
        } else {
            $requestID = null;
        }

        $auth->processResponse($requestID);

        $errors = $auth->getErrors();

        if (!empty($errors)) {
            /* Log errors */
            $errorInfos = 'ERROR - PROVIDER : ' . $settings['sp']['attributeConsumingService']['serviceName'];
            $errorInfos .= ' - Agency : ' . $agency->getName();
            $errorInfos .= ' - ERROR INFO : ' . json_encode($errors);

            $authssoManager->logger->info($errorInfos);

            throw new InvalidArgumentException('Unable to process SAML connection.');
        }

        if (!$auth->isAuthenticated()) {
            throw new InvalidArgumentException('Unable to process SAML authentication.');
        }

        $this->setSessionVars($auth);

        $this->get('session')->remove('AuthNRequestID');

        $samlUserdata = $this->get('session')->get('samlUserdata');

        $emailData = $samlUserdata['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress'];

        if (isset($emailData) && is_array($emailData)) {
            if ($email = $emailData[0]) {
                return $this->processLoginOrRedirect($email, $authssoManager, $request);
            } else {
                throw new InvalidArgumentException('SAML email has not been set.');
            }
        } else {
            throw new InvalidArgumentException('SAML data is not compliant');
        }

        return null;
    }

    /**
     * @Route("/metadata/{uuid}", methods={"GET"})
     *
     * @param Request $request
     * @param string  $uuid
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \InvalidArgumentException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \LogicException
     */
    public function metadataAction(Request $request, string $uuid): Response
    {

        if (!$agency = $this->get('todotoday.core.domain_context.agency')->getAgency()) {
            throw new InvalidArgumentException('Agency is missing');
        }

        $settings = $this->getDoctrine()->getRepository(AuthProvider::class)->getSettings($agency, $uuid);

        try {
            $settings = new OneLogin_Saml2_Settings($settings, true);
            $metadata = $settings->getSPMetadata();
            $errors = $settings->validateMetadata($metadata);
            if (empty($errors)) {
                header('Content-Type: text/xml');
                echo $metadata;
            } else {
                throw new OneLogin_Saml2_Error(
                    'Invalid SP metadata: ' . implode(', ', $errors),
                    OneLogin_Saml2_Error::METADATA_SP_INVALID
                );
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        $response = new Response("");
        $response->headers->set('Content-Type', 'application/xml');

        return $response;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     *
     * @Route("/registration", methods={"GET", "POST"})
     * @SFConf\Template()
     *
     * @param Request $request
     *
     * @return array|Response
     * @throws \ReflectionException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \RuntimeException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Symfony\Component\Form\Exception\UnexpectedTypeException
     * @throws \Symfony\Component\Form\Exception\LogicException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    public function registrationAction(Request $request)
    {
        if (!$agency = $this->get('todotoday.core.domain_context')->getCurrentContext()->getAgency()) {
            throw $this->createNotFoundException();
        }

        if (!$agency->isAllowAdherent() || !$agency->isAllowRegistration()) {
            throw $this->createNotFoundException();
        }

        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute('index_cms_concierge');
        }

        if (!$this->get('session')->has('samlUserdata')) {
            return $this->redirectToRoute('todotoday_account_registration_new');
        }

        $accountManager = $this->get('todotoday.account.services.account_manager');
        $account = new Adherent();

        $flow = $this->get('todotoday.authsso.form.authsso_registration_flow');
        $flow->bind($account);

        $form = $flow->createForm();
        if ($flow->isValid($form)) {
            $flow->saveCurrentStepData($form);
            try {
                if ($flow->nextStep()) {
                    $form = $flow->createForm();
                } else {
                    $accountManager->searchLinkAccount(
                        $account,
                        $agency
                    );
                    $accountManager
                        ->createRegistrationToken($account);
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($account);
                    $dispatcher = $this->get('event_dispatcher');
                    $event = new RegistrationSuccessEvent($account);
                    $dispatcher->dispatch(RegistrationSuccessEvent::USER_CREATED, $event);

                    $userManager = $this->get('fos_user.user_manager');
                    $userManager->updateUser($account);
                    $dispatcher = $this->get('event_dispatcher');
                    $account->setRegistrationToken(null)->setEnabled(true);
                    $event = new FilterUserResponseEvent(
                        $account,
                        $request,
                        $this->redirectToRoute('index_cms_concierge')
                    );
                    $dispatcher->dispatch(FOSUserEvents::REGISTRATION_CONFIRMED, $event);
                    $event = new ChangeAccountRolesEvent($account);
                    $dispatcher->dispatch($event::NAME, $event);

                    $em->flush();

                    return $this->redirectToRoute('todotoday_account_registration_mandatoryworkflow');
                }
            } catch (\Exception $e) {
                $flow->invalidateStepData($flow->getCurrentStepNumber());
            }
        }

        return array(
            'form' => $form->createView(),
            'flow' => $flow,
            'cgv' => $agency->getCgv() ? $agency->getCgv()->getContent() : '',
            'showAlert' => false,
        );
    }

    /**
     * @Route("/logout", methods={"GET"})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function logoutAction(Request $request): Response
    {

        $authssoManager = $this->get('todotoday_authsso.manager');
        $authssoManager->logout();

        return $this->redirectToRoute('index_cms_concierge');
    }
}
