<?php declare(strict_types=1);

namespace Todotoday\AuthSSOBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController;

/**
 * Class AuthProviderAdminController
 * @package Todotoday\AuthSSOBundle\Controller\Admin
 */
class AuthProviderAdminController extends CRUDController
{
}
