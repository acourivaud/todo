<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 26/05/17
 * Time: 14:36
 */

namespace Todotoday\AnimationBundle\Tests\Services;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAdherentLinkedMicrosoftData;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AnimationBundle\Entity\Api\MicrosoftCRM\Animation;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyLinkedMicrosoft;
use Todotoday\CoreBundle\Entity\Api\Microsoft\Agency;

/**
 * Class AnimationManagerTest
 * @package Todotoday\AnimationBundle\Tests\Services
 */
class AnimationManagerTest extends WebTestCase
{
    /**
     * @small
     */
    public function testGetAvailableAnimation(): void
    {
        /** @var Agency $areva */
        $areva = self::getFRR()->getReference('agency_arevalta');

        $manager = $this->getContainer()->get('todotoday.animation_manager');
        $result = $manager->getAnimationsAvailableForAgency($areva);
        if ($result) {
            static::assertInstanceOf(Animation::class, $result[0]);
        }
        // just to valid test if no animation was found
        static::assertTrue(true);
    }

    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAgencyLinkedMicrosoft::class,
            LoadAdherentLinkedMicrosoftData::class,
        );
    }
}
