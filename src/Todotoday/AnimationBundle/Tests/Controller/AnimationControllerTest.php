<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 26/05/17
 * Time: 17:15
 */

namespace Todotoday\AnimationBundle\Tests\Controller;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Actiane\UnitFonctionalTestBundle\Traits\AccessDeniedForNotAdherentTestableTrait;
use Actiane\UnitFonctionalTestBundle\Traits\AccessGrantedForAdherentMicrosoftTestableTrait;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAccountData;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAdherentLinkedMicrosoftData;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyLinkedMicrosoft;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class AnimationControllerTest
 * @package Todotoday\AnimationBundle\Tests\Controller
 */
class AnimationControllerTest extends WebTestCase
{
    use AccessDeniedForNotAdherentTestableTrait, AccessGrantedForAdherentMicrosoftTestableTrait;

    /**
     * @dataProvider adherentMicrosoftProvider
     * @small
     *
     * @param string $agencySlug
     * @param string $accountSlug
     */
    public function testAccessGrantedForAdherent(string $agencySlug, string $accountSlug): void
    {
        $url = 'http://' . $agencySlug . '.todotolol.com' . $this->getUrl('animation_index');
        $this->succesRoute($accountSlug, 'main', $url);
    }

    /**
     * @small
     * @dataProvider notAdherentProvider
     *
     * @param string $accountSlug
     */
    public function testAccessDeniedForNotAdherent(string $accountSlug): void
    {
        /** @var Agency $agency */
        $agency = self::getFRR()->getReference('agency_arevalta');

        $url = 'http://' . $agency->getSlug() . '.todotolol.com' . $this->getUrl('animation_index');
        $this->accessDeniedForNotAdherent($accountSlug, 'main', $url);
    }

    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAccountData::class,
            LoadAgencyLinkedMicrosoft::class,
            LoadAdherentLinkedMicrosoftData::class,
        );
    }
}
