<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 27/05/17
 * Time: 17:54
 */

namespace Todotoday\AnimationBundle\Tests\Controller\Api;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Actiane\UnitFonctionalTestBundle\Traits\AccessDeniedForNotAdherentTestableTrait;
use Actiane\UnitFonctionalTestBundle\Traits\AccessGrantedForAdherentMicrosoftTestableTrait;
use Symfony\Component\Security\Acl\Model\DomainObjectInterface;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAccountData;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAdherentLinkedMicrosoftData;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyLinkedMicrosoft;

/**
 * Class AnimationApiControllerTest
 * @package Todotoday\AnimationBundle\Tests\Controller\Api
 */
class AnimationApiControllerTest extends WebTestCase
{
    use AccessGrantedForAdherentMicrosoftTestableTrait, AccessDeniedForNotAdherentTestableTrait;

    /**
     * @dataProvider notAdherentProvider
     * @small
     *
     * @param string $accountSlug
     */
    public function testAccessDeniedForNotAdherentPostingParticipation(string $accountSlug): void
    {
        $url = $this->getUrl(
            'todotoday.animation.api.animation.post_participation_animation',
            array(
                'animationId' => '9a774d98-b430-e711-8105-5065f38b03b1',
            )
        );
        $this->accessDeniedForNotAdherent($accountSlug, 'api', $url);
    }

    /**
     * @dataProvider notAdherentProvider
     * @small
     *
     * @param string $accountSlug
     */
    public function testAccessDeniedForNotAdherentGetAnimation(string $accountSlug): void
    {
        $url = $this->getUrl(
            'todotoday.animation.api.animation.get_animation'
        );
        $this->accessDeniedForNotAdherent($accountSlug, 'api', $url);
    }

    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAccountData::class,
            LoadAgencyLinkedMicrosoft::class,
            LoadAdherentLinkedMicrosoftData::class,
        );
    }
}
