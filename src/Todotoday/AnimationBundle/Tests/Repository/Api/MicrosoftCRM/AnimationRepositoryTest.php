<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 26/05/17
 * Time: 11:10
 */

namespace Todotoday\AnimationBundle\Tests\Repository\Api\MicrosoftCRM;

use Actiane\ApiConnectorBundle\Tests\Repository\Api\AbstractRepositoryTestCase;

/**
 * Class AnimationRepositoryTest
 * @package Todotoday\AnimationBundle\Tests\Repository\Api\MicrosoftCRM
 */
class AnimationRepositoryTest extends AbstractRepositoryTestCase
{
    /**
     * Do getRepositoryId
     *
     * @return string
     */
    protected function getRepositoryId(): string
    {
        return 'todotoday.animation.repository.api.animation';
    }
}
