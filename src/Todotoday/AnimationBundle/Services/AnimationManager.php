<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 26/05/17
 * Time: 13:17
 */

namespace Todotoday\AnimationBundle\Services;

use Actiane\ApiConnectorBundle\Services\Connections\Microsoft\MicrosoftDynamicsConnection;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AnimationBundle\Entity\Animation as AnimationFront;
use Todotoday\AnimationBundle\Entity\Api\MicrosoftCRM\Animation;
use Todotoday\AnimationBundle\Entity\Api\MicrosoftCRM\CategorieAnimation;
use Todotoday\AnimationBundle\Entity\Api\MicrosoftCRM\ParticipationAnimation;
use Todotoday\AnimationBundle\Exceptions\AnimationAlreadyRegisteredException;
use Todotoday\AnimationBundle\Exceptions\AnimationIsFullException;
use Todotoday\AnimationBundle\Exceptions\AnimationNotAvailableForAgencyException;
use Todotoday\AnimationBundle\Exceptions\AnimationNotBookableYetException;
use Todotoday\AnimationBundle\Exceptions\AnimationNotFoundException;
use Todotoday\AnimationBundle\Exceptions\AnimationNotRegisteredException;
use Todotoday\AnimationBundle\Exceptions\NotAnimationException;
use Todotoday\AnimationBundle\Repository\Api\MicrosoftCRM\AnimationRepository;
use Todotoday\AnimationBundle\Repository\Api\MicrosoftCRM\CategorieAnimationRepository;
use Todotoday\AnimationBundle\Repository\Api\MicrosoftCRM\ParticipationAnimationRepository;
use Todotoday\CartBundle\Entity\CartProduct;
use Todotoday\CatalogBundle\Entity\Api\Microsoft\RetailCatalog;
use Todotoday\CatalogBundle\Exceptions\ProductNotFoundException;
use Todotoday\CatalogBundle\Services\CatalogManager;
use Todotoday\CheckoutBundle\Service\CheckoutManager;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class AnimationManager
 * @package Todotoday\AnimationBundle\Services
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.UnusedLocalVariable)
 */
class AnimationManager
{
    /**
     * @var MicrosoftDynamicsConnection
     */
    protected $connection;

    /**
     * @var CatalogManager
     */
    protected $catalogManager;

    /**
     * @var CheckoutManager
     */
    protected $checkoutManager;

    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * AnimationManager constructor.
     *
     * @param MicrosoftDynamicsConnection $connection
     * @param CatalogManager              $catalogManager
     * @param CheckoutManager             $checkoutManager
     * @param ObjectManager               $objectManager
     * @param ContainerInterface          $container
     *
     */
    public function __construct(
        MicrosoftDynamicsConnection $connection,
        CatalogManager $catalogManager,
        CheckoutManager $checkoutManager,
        ObjectManager $objectManager,
        ContainerInterface $container
    ) {
        $this->connection = $connection;
        $this->catalogManager = $catalogManager;
        $this->checkoutManager = $checkoutManager;
        $this->objectManager = $objectManager;
        $this->container = $container;
    }

    /**
     * @param ParticipationAnimation $participationAnimation
     *
     * @return ParticipationAnimation
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    public function cancelParticipationAnimation(ParticipationAnimation $participationAnimation): ParticipationAnimation
    {
        $participationAnimation
            ->setStatecode(ParticipationAnimation::STATE_CODE_INACTIVE);
//            ->setStatuscode(ParticipationAnimation::STATUS_CODE_INACTIVE);
        $this->connection->persist($participationAnimation)->flush();

        return $participationAnimation;
    }

    /**
     * @param Agency   $agency
     * @param Adherent $adherent
     * @param string   $idAnimation
     *
     * @return Animation
     * @throws \Todotoday\AnimationBundle\Exceptions\AnimationNotFoundException
     * @throws \Todotoday\AnimationBundle\Exceptions\AnimationNotBookableYetException
     * @throws \Todotoday\AnimationBundle\Exceptions\AnimationNotAvailableForAgencyException
     * @throws \Todotoday\AnimationBundle\Exceptions\AnimationIsFullException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Todotoday\CheckoutBundle\Exception\OrderNotCancellableException
     * @throws AnimationNotRegisteredException
     */
    public function cancelParticipationAnimationAndOrder(
        Agency $agency,
        Adherent $adherent,
        string $idAnimation
    ): Animation {
        $animation = $this->getAnimation($agency, $idAnimation, $adherent);

        if (!$animation->isRegister()) {
            throw new AnimationNotRegisteredException();
        }

        $participantAnimation = $this->getParticipationFromAdherentAndAnimation($adherent, $animation);

        if ($animation->getPrice() && $participantAnimation->getApsCommandeId()) {
            $orderManager = $this->container->get('todotoday.order.manager');
            $orderManager->cancelOrderWithoutChecking($adherent, $participantAnimation->getApsCommandeId());
        }
        $participantAnimation = $this->cancelParticipationAnimation($participantAnimation);

        /** @var AnimationRepository $animationRepository */
        $animationRepository = $this->connection->getRepository(Animation::class);

        return $animationRepository->getAnimationWithId($agency, $idAnimation);
    }

    /**
     * @param Adherent $adherent
     * @param string   $commandeid
     *
     * @return ParticipationAnimation
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    public function cancelParticipationAnimationWithCommandeid(
        Adherent $adherent,
        string $commandeid
    ): ParticipationAnimation {
        /** @var ParticipationAnimationRepository $repo */
        $repo = $this->connection->getRepository(ParticipationAnimation::class);
        $participationAnimation = $repo->getParticipationAnimationFromAdherentAndCommandeid($adherent, $commandeid);

        return $this->cancelParticipationAnimation($participationAnimation);
    }

    /**
     * @param Agency   $agency
     * @param Adherent $adherent
     * @param string   $idAnimation
     *
     * @param string   $stripeToken
     *
     * @return ParticipationAnimation
     * @throws \Todotoday\CheckoutBundle\Exception\CheckoutWithInvalidPaymentMethodException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Todotoday\AnimationBundle\Exceptions\AnimationNotFoundException
     * @throws \Todotoday\AnimationBundle\Exceptions\AnimationNotBookableYetException
     * @throws \Todotoday\AnimationBundle\Exceptions\AnimationNotAvailableForAgencyException
     * @throws \Todotoday\AnimationBundle\Exceptions\AnimationIsFullException
     * @throws \Todotoday\AnimationBundle\Exceptions\NotAnimationException
     * @throws \UnexpectedValueException
     * @throws \Todotoday\CheckoutBundle\Exception\NoProductToCheckoutException
     * @throws \Todotoday\CheckoutBundle\Exception\CheckoutWithoutPaymentException
     * @throws \Todotoday\CatalogBundle\Exceptions\ProductNotFoundException
     * @throws \Todotoday\CartBundle\Exception\NotAdherentCartBundleException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \LogicException
     * @throws \Todotoday\CheckoutBundle\Exception\OrderException
     * @throws \Todotoday\CheckoutBundle\Exception\CheckoutWithoutInitializeException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     * @throws AnimationAlreadyRegisteredException
     */
    public function checkoutAnimation(
        Agency $agency,
        Adherent $adherent,
        string $idAnimation,
        string $stripeToken = null
    ): ParticipationAnimation {
        $animation = $this->getAnimation($agency, $idAnimation, $adherent);

        if ($animation->getApsNbplacerestante() < 1) {
            throw new AnimationIsFullException();
        }

        if ($animation->isRegister()) {
            throw new AnimationAlreadyRegisteredException();
        }

        if ($animation->getApsCodeproduitax() && $animation->getPrice()) {
            $cartProduct = (new CartProduct())
                ->setAgency($agency)
                ->setAdherent($adherent)
                ->setQuantity(1);

            $product = $this->getProductAnimation($agency, $animation->getApsCodeproduitax());

            $mapping = $this->connection->getMapping(RetailCatalog::class);
            $mapping->fillEntityInEntityDoctrine($product, $cartProduct);
            $checkout = $this->checkoutManager->initCheckoutProcess($adherent, $agency, null, 'Front', $cartProduct);
            $checkout = $this->checkoutManager->setCustomerStripeAccount($checkout, $stripeToken);
            $salesOrderHeader = $this->checkoutManager->createFullOrder($checkout);
        }

        $participation = (new ParticipationAnimation())
            ->setApsName($adherent->getFirstName() . ' ' . $adherent->getLastName())
            ->setOwnerIdValueGuid($agency->getCrmTeam(), 'teams')
            ->setApsAnimationValueGuid($animation->getApsAnimationid())
            ->setApsAnimationParticipationuneanimaidValueGuid($animation->getApsAnimationid())
            ->setApsAdhrentValueGuid($adherent->getCrmIdAdherent());

        if (isset($salesOrderHeader) && $salesOrderHeader->getSalesOrderNumber()) {
            $participation->setApsCommandeId($salesOrderHeader->getSalesOrderNumber());
        }

        $this->connection->persist($participation)->flush();
        $animation = $this->getAnimation($agency, $idAnimation, $adherent);
        $animation->setRegister(true);
        $participation->setAnimation($animation);

        return $participation;
    }

    /**
     * @param Agency        $agency
     * @param string        $animationId
     * @param Adherent|null $adherent
     *
     * @return Animation
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Todotoday\AnimationBundle\Exceptions\AnimationNotAvailableForAgencyException
     * @throws \Todotoday\AnimationBundle\Exceptions\AnimationIsFullException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     * @throws AnimationNotBookableYetException
     * @throws AnimationNotFoundException
     */
    public function getAnimation(Agency $agency, string $animationId, ?Adherent $adherent): Animation
    {
        /** @var Animation $animation */
        if (!$animation = $this->connection->getRepository(Animation::class)->find(
            array(
                'apsAnimationid' => $animationId,
            )
        )
        ) {
            throw new AnimationNotFoundException();
        }

        if ($animation->getApsSiteValue() !== $agency->getCrmSite()) {
            throw new AnimationNotAvailableForAgencyException();
        }

//        $now = new \DateTime();
//        if ($now > $animation->getApsFermeturedesinscriptions()
//            || $now < $animation->getApsOuverturedesinscriptions()
//        ) {
//            throw new AnimationNotBookableYetException();
//        }

        return $this->handleAnimationInformation($agency, $adherent, $animation);
    }

    /**
     * @param Agency   $agency
     *
     * @param Adherent $adherent
     *
     * @return array|null
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \InvalidArgumentException
     */
    public function getAnimationFront(Agency $agency, ?Adherent $adherent): ?array
    {
        $animationFrontRepo = $this->objectManager->getRepository('TodotodayAnimationBundle:Animation');

        $animationToShow = [];
        if ($animationsFront = $animationFrontRepo->findAll()) {
            foreach ($animationsFront as $animation) {
                try {
                    if ($animation->getAnimationCategory() !== '1063'
                        && $this->catalogManager->getProduct(
                            $agency,
                            $animation->getAnimationCategory()
                        )
                    ) {
                        $animationToShow[] = $animation;
                    }
                } catch (\Exception $e) {
                }
            }
        }

        if ($genericAnimation = $animationFrontRepo->findOneBy(
            array(
                'animationCategory' =>
                    '1063',
            )
        )
        ) {
            array_unshift($animationToShow, $genericAnimation);
        }

        /**
         * @var array
         */
        $animationBag = [];

        /**
         * @var $animation \Todotoday\AnimationBundle\Entity\Animation
         */
        foreach ($animationToShow as $animation) {
            $sessions = $this->getAnimationsForCategorie($agency, $animation->getAnimationCategory(), $adherent);
            $animation->setSessions($sessions);
            $animationBag[] = $animation;
        }

        return $animationBag;
    }

    /**
     * @param Agency $agency
     * @param string $codeAx
     *
     * @return null|AnimationFront[]
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function getAnimationFrontByCodeAx(Agency $agency, string $codeAx): ?array
    {
        $animationFrontRepo = $this->objectManager->getRepository('TodotodayAnimationBundle:Animation');

        try {
            if ($this->catalogManager->getProduct($agency, $codeAx)) {
                return $animationFrontRepo->findBy(
                    array(
                        'animationCategory' => $codeAx,
                    )
                );
            }
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * @param Agency        $agency
     * @param Adherent|null $adherent
     *
     * @return Animation[]|null
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    public function getAnimationsAvailableForAgency(Agency $agency, ?Adherent $adherent = null): ?array
    {
        /** @var AnimationRepository $repo */
        $repo = $this->connection->getRepository(Animation::class);
        $animations = $repo->getAnimationsAvailableForAgency($agency);

        return $this->handleAnimationsInformation($agency, $adherent, $animations);
    }

    /**
     * @param Agency        $agency
     * @param string        $codeAx
     * @param Adherent|null $adherent
     *
     * @return array|null|CategorieAnimation[]
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    public function getAnimationsForCategorie(Agency $agency, string $codeAx, ?Adherent $adherent): ?array
    {
        /** @var CategorieAnimationRepository $repoCategorie */
        $repoCategorie = $this->connection->getRepository(CategorieAnimation::class);
        /** @var AnimationRepository $repoAnimation */
        $repoAnimation = $this->connection->getRepository(Animation::class);
        $animations = [];
        if ($categorieAnimations = $repoCategorie->getCategorieAnimation($codeAx)) {
            $animations = $repoAnimation->getAnimationsAvailableForCategorie(
                $agency,
                $categorieAnimations[0]->getApsCatgoriedanimationid()
            );
        }

        return $this->handleAnimationsInformation($agency, $adherent, $animations);
    }

    /**
     * @param Adherent $adherent
     *
     * @return ParticipationAnimation[]
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    public function getParticipationFromAdherent(Adherent $adherent): ?array
    {
        /** @var ParticipationAnimationRepository $repo */
        $repo = $this->connection->getRepository(ParticipationAnimation::class);
        $participations = $repo->getParticipationAnimationForAdherent($adherent);
        $participationArray = [];
        /** @var ParticipationAnimation $participation */
        foreach ((array) $participations as $participation) {
            if ($participation->isRegister()) {
                $participationArray[] = $participation->getApsAnimationParticipationuneanimaidValue();
            }
        }

        return array_unique(array_filter($participationArray));
    }

    /**
     * @param Adherent  $adherent
     * @param Animation $animation
     *
     * @return null|ParticipationAnimation
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    public function getParticipationFromAdherentAndAnimation(
        Adherent $adherent,
        Animation $animation
    ): ?ParticipationAnimation {
        /** @var ParticipationAnimationRepository $repo */
        $repo = $this->connection->getRepository(ParticipationAnimation::class);

        return $repo->getParticipationAnimationForAdherentAndAnimation($adherent, $animation);
    }

    /**
     * @param Agency $agency
     * @param string $productId
     *
     * @return RetailCatalog
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Todotoday\CatalogBundle\Exceptions\ProductNotFoundException
     * @throws \InvalidArgumentException
     * @throws NotAnimationException
     */
    public function getProductAnimation(Agency $agency, string $productId): RetailCatalog
    {
        $product = $this->catalogManager->getProduct($agency, $productId);
        if (!$product->isAnimation()) {
            throw new NotAnimationException();
        }

        return $product;
    }

    /**
     * @param Agency        $agency
     * @param null|Adherent $adherent
     * @param Animation     $animation
     *
     * @return Animation
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    private function handleAnimationInformation(Agency $agency, ?Adherent $adherent, Animation $animation): Animation
    {
        if ($animation->getApsCodeproduitax()) {
            try {
                $product = $this->catalogManager->getProduct($agency, $animation->getApsCodeproduitax());
                $animation->setCurrency($agency->getCurrency())
                    ->setPrice($product->getAmount());
            } catch (ProductNotFoundException $e) {
            }
        }

        if ($adherent
            && in_array(
                $animation->getApsAnimationid(),
                $this->getParticipationFromAdherent($adherent),
                true
            )
        ) {
            $animation->setRegister(true);
        }

        return $animation;
    }

    /**
     * @param Agency           $agency
     * @param Adherent|null    $adherent
     * @param Animation[]|null $animations
     *
     * @return Animation[]
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    private function handleAnimationsInformation(Agency $agency, ?Adherent $adherent, ?array $animations = []): array
    {
        $result = [];
        /** @var Animation $animation */
        foreach ((array) $animations as $animation) {
            $result[] = $this->handleAnimationInformation($agency, $adherent, $animation);
        }

        return $result;
    }
}
