<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 29/05/17
 * Time: 13:59
 */

namespace Todotoday\AnimationBundle\Exceptions;

use Throwable;

/**
 * Class AnimationIsFullException
 * @package Todotoday\AnimationBundle\Exceptions
 */
class AnimationIsFullException extends \Exception
{
    /**
     * AnimationIsFullException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = 'Animation is full', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
