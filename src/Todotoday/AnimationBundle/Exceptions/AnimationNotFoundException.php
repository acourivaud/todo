<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 29/05/17
 * Time: 13:46
 */

namespace Todotoday\AnimationBundle\Exceptions;

use Throwable;

/**
 * Class AnimationNotFoundException
 * @package Todotoday\AnimationBundle\Exceptions
 */
class AnimationNotFoundException extends \Exception
{
    /**
     * AnimationNotFoundException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = 'Animation not found', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
