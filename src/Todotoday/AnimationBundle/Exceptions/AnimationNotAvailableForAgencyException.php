<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 29/05/17
 * Time: 13:57
 */

namespace Todotoday\AnimationBundle\Exceptions;

use Throwable;

/**
 * Class AnimationNotAvailableForAgencyException
 * @package Todotoday\AnimationBundle\Exceptions
 */
class AnimationNotAvailableForAgencyException extends \Exception
{
    /**
     * AnimationNotAvailableForAgencyException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct(
        $message = 'Animation not available for given agency',
        $code = 0,
        Throwable $previous =
        null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
