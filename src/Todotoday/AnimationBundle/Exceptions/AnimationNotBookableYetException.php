<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 29/05/17
 * Time: 13:47
 */

namespace Todotoday\AnimationBundle\Exceptions;

use Throwable;

/**
 * Class AnimationNotBookableYetException
 * @package Todotoday\AnimationBundle\Exceptions
 */
class AnimationNotBookableYetException extends \Exception
{
    /**
     * AnimationNotBookableYetException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct(
        $message = 'Registration for this animation is closed',
        $code = 0,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
