<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 29/05/17
 * Time: 13:18
 */

namespace Todotoday\AnimationBundle\Exceptions;

use Throwable;

/**
 * Class NotAnimationException
 * @package Todotoday\AnimationBundle\Exceptions
 */
class NotAnimationException extends \Exception
{
    /**
     * NotAnimationException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = 'This product is not an animation', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
