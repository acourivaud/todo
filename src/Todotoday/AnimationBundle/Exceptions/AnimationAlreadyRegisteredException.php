<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 31/05/17
 * Time: 22:11
 */

namespace Todotoday\AnimationBundle\Exceptions;

use Throwable;

/**
 * Class AnimationAlreadyRegisteredException
 * @package Todotoday\AnimationBundle\Exceptions
 */
class AnimationAlreadyRegisteredException extends \Exception
{
    /**
     * AnimationAlreadyRegistered constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = 'Adherent already registered', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
