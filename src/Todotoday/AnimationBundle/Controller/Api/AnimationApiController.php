<?php

declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 27/05/17
 * Time: 17:46
 */

namespace Todotoday\AnimationBundle\Controller\Api;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as FosRest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Acl\Model\DomainObjectInterface;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Exceptions\AgencyMissingException;

/**
 * @FosRest\Prefix("/animation")
 * @FosRest\NamePrefix("todotoday.animation.api.animation.")
 * Class AnimationApiController
 *
 * @package Todotoday\AnimationBundle\Controller\Api
 */
class AnimationApiController extends FOSRestController implements DomainObjectInterface
{
    /**
     * Get list of all animation for agency
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Get list of all animation for agency",
     *     description="Get list of all animation for agency",
     *     views={"animation"},
     *     section="animation"
     * )
     * @return Response
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \Psr\Cache\InvalidArgumentException
     * @FosRest\Route("", options={"expose"=true})
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \LogicException
     */
    public function getAnimationAction(): Response
    {
        if (!$this->isGranted('VIEW', $this)) {
            throw $this->createAccessDeniedException();
        }

        $agency = $this->getAgency();

        $manager = $this->get('todotoday.animation_manager');
        $view = $this->view($manager->getAnimationsAvailableForAgency($agency, $this->getUser()));
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * Get list of all animation for agency for given category
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Get list of all animation for agency for given category",
     *     description="Get list of all animation for agency for given category",
     *     views={"animation"},
     *     section="animation"
     * )
     *
     * @FosRest\Route("/categorie/{codeAx}", options={"expose"=true})
     *
     * @param string $codeAx
     *
     * @return Response
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \LogicException
     */
    public function getAnimationCategorieAction(string $codeAx): Response
    {
        if (!$this->isGranted('VIEW', $this)) {
            throw $this->createAccessDeniedException();
        }

        $agency = $this->getAgency();

        $manager = $this->get('todotoday.animation_manager');
        $view = $this->view($manager->getAnimationsForCategorie($agency, $codeAx, $this->getUser()));
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * Get list of all animation for agency for given category
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Get list of all animation stored in front for agency for given category",
     *     description="Get list of all animation stored in front for agency for given category",
     *     views={"animation"},
     *     section="animation"
     * )
     *
     * @FosRest\Route("/front", options={"expose"=true})
     * @return Response
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \LogicException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \UnexpectedValueException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function getAnimationFrontAction(): Response
    {
        $agency = $this->getAgency();
        $manager = $this->get('todotoday.animation_manager');
        $view = $this->view($manager->getAnimationFront($agency, $this->getUser()));
        $view->getContext()->addGroups(['animation']);
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * Get list of all animation for agency for given category
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Get list of all animation stored in front for agency for given category",
     *     description="Get list of all animation stored in front for agency for given category",
     *     views={"animation"},
     *     section="animation"
     * )
     *
     * @FosRest\Route("/front/{codeAx}", options={"expose"=true})
     * @param string $codeAx
     *
     * @return Response
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \UnexpectedValueException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function getAnimationFrontByCodeAxAction(string $codeAx): Response
    {
        $agency = $this->getAgency();
        $manager = $this->get('todotoday.animation_manager');
        $view = $this->view($manager->getAnimationFrontByCodeAx($agency, $codeAx));
        $view->getContext()->addGroups(['animation']);
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * {@inheritdoc}
     */
    public function getObjectIdentifier(): string
    {
        return 'class';
    }

    /**
     * Register adherent to animation
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Register adherent to animation",
     *     description="Register adherent to animation",
     *     views={"animation"},
     *     section="animation"
     * )
     *
     * @FosRest\Route("/participation/{animationId}",
     *     requirements={"animationId": "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"},
     *     options={"expose"=true})
     * @param Request $request
     * @param string  $animationId
     *
     * @return Response
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \Todotoday\CheckoutBundle\Exception\CheckoutWithInvalidPaymentMethodException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Todotoday\CheckoutBundle\Exception\OrderException
     * @throws \Todotoday\CheckoutBundle\Exception\NoProductToCheckoutException
     * @throws \Todotoday\CheckoutBundle\Exception\CheckoutWithoutPaymentException
     * @throws \Todotoday\CheckoutBundle\Exception\CheckoutWithoutInitializeException
     * @throws \Todotoday\CatalogBundle\Exceptions\ProductNotFoundException
     * @throws \Todotoday\CartBundle\Exception\NotAdherentCartBundleException
     * @throws \Todotoday\AnimationBundle\Exceptions\NotAnimationException
     * @throws \Todotoday\AnimationBundle\Exceptions\AnimationNotFoundException
     * @throws \Todotoday\AnimationBundle\Exceptions\AnimationNotBookableYetException
     * @throws \Todotoday\AnimationBundle\Exceptions\AnimationNotAvailableForAgencyException
     * @throws \Todotoday\AnimationBundle\Exceptions\AnimationIsFullException
     * @throws \Todotoday\AnimationBundle\Exceptions\AnimationAlreadyRegisteredException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \InvalidArgumentException
     * @throws \UnexpectedValueException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingException
     * @throws \LogicException
     */
    public function postParticipationAnimationAction(Request $request, string $animationId): Response
    {
        if (!$this->isGranted('CREATE', $this)) {
            throw $this->createAccessDeniedException();
        }

        $manager = $this->get('todotoday.animation_manager');
        $participation = $manager->checkoutAnimation(
            $this->getAgency(),
            $this->getUser(),
            $animationId,
            $request->get('token')
        );

        $view = $this->view($participation->getAnimation());

        return $this->handleView($view);
    }

    /**
     * Cancel animation for agency
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Cancel animation for agency",
     *     description="Cancel animation for agency",
     *     views={"animation"},
     *     section="animation"
     * )
     *
     * @FosRest\Route("/cancel/{animationId}", options={"expose"=true})
     * @param string $animationId
     *
     * @return Response
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \LogicException
     * @throws \Todotoday\CheckoutBundle\Exception\OrderNotCancellableException
     * @throws \Todotoday\AnimationBundle\Exceptions\AnimationNotRegisteredException
     * @throws \Todotoday\AnimationBundle\Exceptions\AnimationNotFoundException
     * @throws \Todotoday\AnimationBundle\Exceptions\AnimationNotBookableYetException
     * @throws \Todotoday\AnimationBundle\Exceptions\AnimationNotAvailableForAgencyException
     * @throws \Todotoday\AnimationBundle\Exceptions\AnimationIsFullException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \UnexpectedValueException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function putCancelParticipationAnimationAction(string $animationId): Response
    {
        if (!$this->isGranted('EDIT', $this)) {
            throw $this->createAccessDeniedException();
        }

        $agency = $this->getAgency();
        $adherent = $this->getUser();

        $manager = $this->get('todotoday.animation_manager');
        $result = $manager->cancelParticipationAnimationAndOrder($agency, $adherent, $animationId);
        $view = $this->view($result);

        return $this->handleView($view);
    }

    /**
     * @return null|Agency
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \UnexpectedValueException
     * @throws AgencyMissingException
     */
    private function getAgency(): ?Agency
    {
        if (!$agency = $this->get('todotoday.core.domain_context.api')->getAgency()) {
            throw new AgencyMissingException('Agency is missing');
        }

        return $agency;
    }
}
