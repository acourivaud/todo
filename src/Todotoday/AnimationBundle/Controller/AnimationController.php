<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 26/05/17
 * Time: 13:04
 */

namespace Todotoday\AnimationBundle\Controller;

use Actiane\PaymentBundle\Enum\PaymentTypeEnum;
use Actiane\PaymentBundle\Form\StripeType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Exceptions\AgencyMissingException;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * Class AnimationController
 *
 * @package Todotoday\AnimationBundle\Controller
 * @Route("animation")
 */
class AnimationController extends Controller
{
    /**
     * @Route("/{codeAx}", name="categorie_index", requirements={"codeAx": "\w+"}, options={"expose"=true})
     * @param string $codeAx
     *
     * @return null|Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \LogicException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     * @throws \UnexpectedValueException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingException
     */
    public function categorieAction(string $codeAx): ?Response
    {
        if (!$this->isGranted('ROLE_ANIMATION')) {
            $this->createAccessDeniedException();
        }

        $form = null;
        $manager = $this->get('todotoday.animation_manager');

        /** @var Adherent $adherent */
        if (!($adherent = $this->getUser()) instanceof Adherent) {
            $adherent = null;
        } elseif (($paymentSelected = $adherent->getPaymentTypeSelected())
            && $paymentSelected->get() === PaymentTypeEnum::STRIPE_DIRECT
        ) {
            $form = $this->createForm(
                StripeType::class,
                null,
                array(
                    'action' => null,
                    'as_component' => true,
                    'fields_required' => true,
                    'card_save_message_success' => 'stripe.animation.card_save_success',
                )
            )->createView();
        }

        $animations = $manager->getAnimationsForCategorie($this->getAgency(), $codeAx, $adherent);

        $animationsFront = $manager->getAnimationFrontByCodeAx($this->getAgency(), $codeAx);

        return $this->render(
            '@TodotodayAnimation/animation_categorie.html.twig',
            array(
                'animationsFront' => $animationsFront,
                'animations' => $animations,
                'codeAx' => $codeAx,
                'form' => $form,
            )
        );
    }

    /**
     * @Route("/", name="animation_index", options={"expose"=true})
     * @return Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \Todotoday\CatalogBundle\Exceptions\ProductNotFoundException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \LogicException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     * @throws \UnexpectedValueException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingException
     */
    public function indexAction(): ?Response
    {
        if (!$this->isGranted('ROLE_ANIMATION')) {
            $this->createAccessDeniedException();
        }

        $agency = $this->getAgency();

        $manager = $this->get('todotoday.animation_manager');

        $options = array();
        /** @var Adherent $adherent */
        if (!($adherent = $this->getUser()) instanceof Adherent) {
            $adherent = null;
        } elseif (($paymentSelected = $adherent->getPaymentTypeSelected())
            && $paymentSelected->get() === PaymentTypeEnum::STRIPE_DIRECT
        ) {
            $options['form'] = $this->createForm(
                StripeType::class,
                null,
                array(
                    'action' => null,
                    'as_component' => true,
                    'fields_required' => true,
                    'card_save_message_success' => 'stripe.animation.card_save_success',
                )
            )->createView();
        }

        $animationToShow = $manager->getAnimationFront($agency, $adherent);

        $options['animations'] = $manager->getAnimationsAvailableForAgency($this->getAgency(), $adherent);
        $options['animationsFront'] = $animationToShow;
        $this->get('actiane.tools.services.asset_manager')->addJavascript('build/bundles/animation.js');

        return $this->render('@TodotodayAnimation/animation.html.twig', $options);
    }

    /**
     * @Route("/cancel/success/{route}", name="animation_cancel_success", options={"expose"=true})
     * @param string  $route
     *
     * @param Request $request
     *
     * @return null|Response
     * @throws \LogicException
     */
    public function cancelSuccessAction(string $route, Request $request): ?Response
    {
        if (!$this->isGranted('ROLE_ANIMATION')) {
            $this->createAccessDeniedException();
        }

        $router = $this->get('router');

        if (!$router->getRouteCollection()->get($route)) {//if route not exist, redirect to animation_index
            $route = 'animation_index';
        }

        $this->addFlash(
            'animation_unsubscribe_success',
            'animation.unregistration_success'
        );

        return $this->redirectToRoute(
            $route,
            $request->query->get('routeOptions') ?? array()
        );
    }

    /**
     * @return null|Agency
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \UnexpectedValueException
     * @throws AgencyMissingException
     */
    private function getAgency(): ?Agency
    {
        if (!$agency = $this->get('todotoday.core.domain_context')->getCurrentContext()->getAgency()) {
            throw $this->createNotFoundException('Agency is missing');
        }

        return $agency;
    }
}
