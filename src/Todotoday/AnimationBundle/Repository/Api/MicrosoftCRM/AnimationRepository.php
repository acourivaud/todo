<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 26/05/17
 * Time: 11:08
 */

namespace Todotoday\AnimationBundle\Repository\Api\MicrosoftCRM;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiRepository;
use Todotoday\AnimationBundle\Entity\Api\MicrosoftCRM\Animation;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class AnimationRepository
 * @package Todotoday\AnimationBundle\Repository\Api\MicrosoftCRM
 */
class AnimationRepository extends AbstractApiRepository
{
    /**
     * @param Agency $agency
     * @param string $codeAx
     *
     * @return null|Animation[]
     * @throws \InvalidArgumentException
     */
    public function getAnimationsAvailableForCategorie(Agency $agency, string $codeAx): ?array
    {
        $filter = $this->getFilterAvailable($agency);
        $filter .= ' and _aps_categoriedanimation_value eq ' . $codeAx;

        return $this
            ->createQueryBuilder()
            ->addCustomQuery(
                '$filter',
                $filter
            )
            ->addCustomQuery(
                '$orderby',
                'aps_dbutestim'
            )
            ->getRequest()
            ->execute();
    }

    /**
     * @param Agency $agency
     *
     * @return Animation[]
     * @throws \InvalidArgumentException
     */
    public function getAnimationsAvailableForAgency(Agency $agency): ?array
    {
        $filter = $this->getFilterAvailable($agency);

        return $this
            ->createQueryBuilder()
            ->addCustomQuery(
                '$filter',
                $filter
            )
            ->addCustomQuery(
                '$orderby',
                'aps_dbutestim'
            )
            ->getRequest()
            ->execute();
    }

    /**
     * @param Agency $agency
     * @param string $animationId
     *
     * @return null|Animation
     */
    public function getAnimationWithId(Agency $agency, string $animationId): ?Animation
    {
        $filter = $this->getFilterAvailable($agency);
        $filter .= ' and aps_animationid eq ' . $animationId;

        return $this
            ->createQueryBuilder()
            ->addCustomQuery(
                '$filter',
                $filter
            )
            ->addCustomQuery(
                '$top',
                '1'
            )
            ->getRequest()
            ->execute()[0];
    }

    /**
     * @param Agency $agency
     *
     * @return string
     */
    private function getFilterAvailable(Agency $agency): string
    {
        $now = (new \DateTime())->format('Y-m-d\TH:i:s\Z');

        $filter = '_aps_site_value eq ' . $agency->getCrmSite();
//        $filter .= ' and aps_fermeturedesinscriptions gt ' . $now;
        $filter .= ' and aps_ouverturedesinscriptions lt ' . $now;

        return $filter;
    }
}
