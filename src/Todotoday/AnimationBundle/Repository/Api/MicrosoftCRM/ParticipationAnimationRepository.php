<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 26/05/17
 * Time: 11:08
 */

namespace Todotoday\AnimationBundle\Repository\Api\MicrosoftCRM;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiRepository;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AnimationBundle\Entity\Api\MicrosoftCRM\Animation;
use Todotoday\AnimationBundle\Entity\Api\MicrosoftCRM\ParticipationAnimation;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class ParticipationAnimationRepository
 * @package Todotoday\AnimationBundle\Repository\Api\MicrosoftCRM
 */
class ParticipationAnimationRepository extends AbstractApiRepository
{
    /**
     * @param Adherent $adherent
     *
     * @return ParticipationAnimation[]
     * @throws \InvalidArgumentException
     */
    public function getParticipationAnimationForAdherent(Adherent $adherent): ?array
    {
        $filter = '_aps_adhrent_value eq ' . $adherent->getCrmIdAdherent();
        $filter .= ' and ' . $this->filterEnableParticipation();
        $select = '_aps_animation_participationuneanimaid_value, statecode, statuscode';

        return $this
            ->createQueryBuilder()
            ->addCustomQuery(
                '$filter',
                $filter
            )
            ->addCustomQuery(
                '$select',
                $select
            )
            ->getRequest()
            ->execute();
    }

    /**
     * @param Adherent  $adherent
     * @param Animation $animation
     *
     * @return null|ParticipationAnimation
     *
     */
    public function getParticipationAnimationForAdherentAndAnimation(
        Adherent $adherent,
        Animation $animation
    ): ?ParticipationAnimation {
        $filter = '_aps_adhrent_value eq ' . $adherent->getCrmIdAdherent();
        $filter .= ' and _aps_animation_participationuneanimaid_value eq ' . $animation->getApsAnimationid();
        $filter .= ' and ' . $this->filterEnableParticipation();

        return $this
                   ->createQueryBuilder()
                   ->addCustomQuery(
                       '$filter',
                       $filter
                   )
                   ->getRequest()
                   ->execute()[0];
    }

    /**
     * @param Adherent $adherent
     * @param string   $apsCommandeid
     *
     * @return null|ParticipationAnimation
     */
    public function getParticipationAnimationFromAdherentAndCommandeid(
        Adherent $adherent,
        string $apsCommandeid
    ): ?ParticipationAnimation {
        if (!$adherent->getCrmIdAdherent()) {
            return null;
        }

        $filter =
            '_aps_adhrent_value eq ' . $adherent->getCrmIdAdherent() .
            ' and aps_commandeid eq \'' . $apsCommandeid . '\'';
        $filter .= ' and ' . $this->filterEnableParticipation();

        return $this
                   ->createQueryBuilder()
                   ->addCustomQuery(
                       '$filter',
                       $filter
                   )
                   ->getRequest()
                   ->execute()[0];
    }

    /**
     * @return string
     */
    public function filterEnableParticipation(): string
    {
        return 'statecode ne ' . ParticipationAnimation::STATE_CODE_INACTIVE;
//        return '(statecode ne ' . ParticipationAnimation::STATE_CODE_INACTIVE . ' or statuscode ne '
//            . ParticipationAnimation::STATUS_CODE_INACTIVE . ')';
    }
}
