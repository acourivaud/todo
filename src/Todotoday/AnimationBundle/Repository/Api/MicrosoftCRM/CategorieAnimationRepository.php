<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 26/05/17
 * Time: 11:08
 */

namespace Todotoday\AnimationBundle\Repository\Api\MicrosoftCRM;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiRepository;
use Todotoday\AnimationBundle\Entity\Api\MicrosoftCRM\Animation;
use Todotoday\AnimationBundle\Entity\Api\MicrosoftCRM\CategorieAnimation;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class CategorieAnimationRepository
 * @package Todotoday\AnimationBundle\Repository\Api\MicrosoftCRM
 */
class CategorieAnimationRepository extends AbstractApiRepository
{
    /**
     * @param string $codeAx
     *
     * @return CategorieAnimation[]|null
     * @throws \InvalidArgumentException
     *
     */
    public function getCategorieAnimation(string $codeAx): ?array
    {
        $filter = 'aps_codeax eq \'' . $codeAx . '\'';

        return $this
            ->createQueryBuilder()
            ->addCustomQuery(
                '$filter',
                $filter
            )
            ->getRequest()
            ->execute();
    }
}
