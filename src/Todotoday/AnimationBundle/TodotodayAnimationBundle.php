<?php declare(strict_types=1);

namespace Todotoday\AnimationBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class TodotodayAnimationBundle
 * @package Todotoday\AnimationBundle
 */
class TodotodayAnimationBundle extends Bundle
{
}
