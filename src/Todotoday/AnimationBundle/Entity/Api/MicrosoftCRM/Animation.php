<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 26/05/17
 * Time: 11:05
 */

namespace Todotoday\AnimationBundle\Entity\Api\MicrosoftCRM;

use Actiane\ApiConnectorBundle\Lib\EntityProxyTrait;
use Actiane\ApiConnectorBundle\Lib\Mapping as APIConnector;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class Animation
 * @Serializer\ExclusionPolicy("all")
 * @APIConnector\Entity(path="aps_animations", repositoryId="todotoday.animation.repository.api.animation")
 * @package Todotoday\CatalogBundle\Entity\Api\MicrosoftCRM
 */
class Animation
{
    use EntityProxyTrait;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="aps_finestime", type="datetimez")
     * @Serializer\Expose()
     * @Serializer\Type("DateTime")
     */
    protected $apsFinestime;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="aps_dateetheuredujour", type="datetimez")
     * @Serializer\Expose()
     * @Serializer\Type("DateTime")
     */
    protected $apsDateetheuredujour;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_owningbusinessunit_value", type="string", guid="businessunitid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $owningbusinessunitValue;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="modifiedon", type="datetimez")
     * @Serializer\Expose()
     * @Serializer\Type("DateTime")
     */
    protected $modifiedon;

    /**
     * @var int
     *
     * @APIConnector\Column(name="statecode", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $statecode;

    /**
     * @var int
     *
     * @APIConnector\Column(name="statuscode", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $statuscode;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_nombredeparticipants", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $apsNombredeparticipants;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_createdby_value", type="string", guid="systemuserid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $createdbyValue;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @APIConnector\Column(name="aps_animationid", type="string", guid="id")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsAnimationid;

    /**
     * @var int
     *
     * @APIConnector\Column(name="timezoneruleversionnumber", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $timezoneruleversionnumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_ownerid_value", type="string", guid="ownerid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $owneridValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_animation1", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsAnimation1;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_aps_site_value", type="string", guid="siteid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsSiteValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_owninguser_value", type="string", guid="systemuserid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $owninguserValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_modifiedby_value", type="string", guid="systemuserid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $modifiedbyValue;

    /**
     * @var int
     *
     * @APIConnector\Column(name="versionnumber", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $versionnumber;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="createdon", type="datetimez")
     * @Serializer\Expose()
     * @Serializer\Type("DateTime")
     */
    protected $createdon;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="aps_dbutestim", type="datetimez")
     * @Serializer\Expose()
     * @Serializer\Type("DateTime")
     */
    protected $apsDbutestim;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="overriddencreatedon", type="datetimez")
     * @Serializer\Expose()
     * @Serializer\Type("Datetime")
     */
    protected $overriddencreatedon;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="aps_fermeturedesinscriptions", type="datetimez")
     * @Serializer\Expose()
     * @Serializer\Type("DateTime")
     */
    protected $apsFermeturedesinscriptions;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_modifiedonbehalfby_value", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $modifiedonbehalfbyValue;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_nbplaceprise", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $apsNbplaceprise;

    /**
     * @var int
     *
     * @APIConnector\Column(name="importsequencenumber", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $importsequencenumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_codeproduitax", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsCodeproduitax;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_owningteam_value", type="string", guid="teamid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $owningteamValue;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="aps_ouverturedesinscriptions", type="datetimez")
     * @Serializer\Expose()
     * @Serializer\Type("DateTime")
     */
    protected $apsOuverturedesinscriptions;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_createdonbehalfby_value", type="string", guid="systemuserid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $createdonbehalfbyValue;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_nbplacerestante", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $apsNbplacerestante;

    /**
     * @var int
     *
     * @APIConnector\Column(name="utcconversiontimezonecode", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $utcconversiontimezonecode;

    /**
     * @var float
     * @Serializer\Expose()
     * @Serializer\Type("float")
     */
    protected $price = 0;

    /**
     * @var string
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $currency;

    /**
     * @var bool
     * @Serializer\Expose()
     * @Serializer\Type("boolean")
     */
    protected $register = false;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_description", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsDescription;

    /**
     * @return \DateTime
     */
    public function getApsFinestime(): ?\DateTime
    {
        return $this->apsFinestime;
    }

    /**
     * @param \DateTime $apsFinestime
     *
     * @return Animation
     */
    public function setApsFinestime(?\DateTime $apsFinestime): Animation
    {
        return $this->setTrigger('apsFinestime', $apsFinestime);
    }

    /**
     * @return \DateTime
     */
    public function getApsDateetheuredujour(): ?\DateTime
    {
        return $this->apsDateetheuredujour;
    }

    /**
     * @param \DateTime $apsDateetheuredujour
     *
     * @return Animation
     */
    public function setApsDateetheuredujour(?\DateTime $apsDateetheuredujour): Animation
    {
        return $this->setTrigger('apsDateetheuredujour', $apsDateetheuredujour);
    }

    /**
     * @return string
     */
    public function getOwningbusinessunitValue(): ?string
    {
        return $this->owningbusinessunitValue;
    }

    /**
     * @param string $owningbusinessunitValue
     *
     * @return Animation
     */
    public function setOwningbusinessunitValue(?string $owningbusinessunitValue): Animation
    {
        return $this->setTrigger('owningbusinessunitValue', $owningbusinessunitValue);
    }

    /**
     * @return \DateTime
     */
    public function getModifiedon(): ?\DateTime
    {
        return $this->modifiedon;
    }

    /**
     * @param \DateTime $modifiedon
     *
     * @return Animation
     */
    public function setModifiedon(?\DateTime $modifiedon): Animation
    {
        return $this->setTrigger('modifiedon', $modifiedon);
    }

    /**
     * @return int
     */
    public function getStatecode(): ?int
    {
        return $this->statecode;
    }

    /**
     * @param int $statecode
     *
     * @return Animation
     */
    public function setStatecode(?int $statecode): Animation
    {
        return $this->setTrigger('statecode', $statecode);
    }

    /**
     * @return int
     */
    public function getStatuscode(): ?int
    {
        return $this->statuscode;
    }

    /**
     * @param int $statuscode
     *
     * @return Animation
     */
    public function setStatuscode(?int $statuscode): Animation
    {
        return $this->setTrigger('statuscode', $statuscode);
    }

    /**
     * @return int
     */
    public function getApsNombredeparticipants(): ?int
    {
        return $this->apsNombredeparticipants;
    }

    /**
     * @param int $apsNombredeparticipants
     *
     * @return Animation
     */
    public function setApsNombredeparticipants(?int $apsNombredeparticipants): Animation
    {
        return $this->setTrigger('apsNombredeparticipants', $apsNombredeparticipants);
    }

    /**
     * @return string
     */
    public function getCreatedbyValue(): ?string
    {
        return $this->createdbyValue;
    }

    /**
     * @param string $createdbyValue
     *
     * @return Animation
     */
    public function setCreatedbyValue(?string $createdbyValue): Animation
    {
        return $this->setTrigger('createdbyValue', $createdbyValue);
    }

    /**
     * @return string
     */
    public function getApsAnimationid(): ?string
    {
        return $this->apsAnimationid;
    }

    /**
     * @param string $apsAnimationid
     *
     * @return Animation
     */
    public function setApsAnimationid(?string $apsAnimationid): Animation
    {
        return $this->setTrigger('apsAnimationid', $apsAnimationid);
    }

    /**
     * @return int
     */
    public function getTimezoneruleversionnumber(): ?int
    {
        return $this->timezoneruleversionnumber;
    }

    /**
     * @param int $timezoneruleversionnumber
     *
     * @return Animation
     */
    public function setTimezoneruleversionnumber(?int $timezoneruleversionnumber): Animation
    {
        return $this->setTrigger('timezoneruleversionnumber', $timezoneruleversionnumber);
    }

    /**
     * @return string
     */
    public function getOwneridValue(): ?string
    {
        return $this->owneridValue;
    }

    /**
     * @param string $owneridValue
     *
     * @return Animation
     */
    public function setOwneridValue(?string $owneridValue): Animation
    {
        return $this->setTrigger('owneridValue', $owneridValue);
    }

    /**
     * @return string
     */
    public function getApsAnimation1(): ?string
    {
        return $this->apsAnimation1;
    }

    /**
     * @param string $apsAnimation1
     *
     * @return Animation
     */
    public function setApsAnimation1(?string $apsAnimation1): Animation
    {
        return $this->setTrigger('apsAnimation1', $apsAnimation1);
    }

    /**
     * @return string
     */
    public function getApsSiteValue(): ?string
    {
        return $this->apsSiteValue;
    }

    /**
     * @param string $apsSiteValue
     *
     * @return Animation
     */
    public function setApsSiteValue(?string $apsSiteValue): Animation
    {
        return $this->setTrigger('apsSiteValue', $apsSiteValue);
    }

    /**
     * @return string
     */
    public function getOwninguserValue(): ?string
    {
        return $this->owninguserValue;
    }

    /**
     * @param string $owninguserValue
     *
     * @return Animation
     */
    public function setOwninguserValue(?string $owninguserValue): Animation
    {
        return $this->setTrigger('owninguserValue', $owninguserValue);
    }

    /**
     * @return string
     */
    public function getModifiedbyValue(): ?string
    {
        return $this->modifiedbyValue;
    }

    /**
     * @param string $modifiedbyValue
     *
     * @return Animation
     */
    public function setModifiedbyValue(?string $modifiedbyValue): Animation
    {
        return $this->setTrigger('modifiedbyValue', $modifiedbyValue);
    }

    /**
     * @return int
     */
    public function getVersionnumber(): ?int
    {
        return $this->versionnumber;
    }

    /**
     * @param int $versionnumber
     *
     * @return Animation
     */
    public function setVersionnumber(?int $versionnumber): Animation
    {
        return $this->setTrigger('versionnumber', $versionnumber);
    }

    /**
     * @return \DateTime
     */
    public function getCreatedon(): ?\DateTime
    {
        return $this->createdon;
    }

    /**
     * @param \DateTime $createdon
     *
     * @return Animation
     */
    public function setCreatedon(?\DateTime $createdon): Animation
    {
        return $this->setTrigger('createdon', $createdon);
    }

    /**
     * @return \DateTime
     */
    public function getApsDbutestim(): ?\DateTime
    {
        return $this->apsDbutestim;
    }

    /**
     * @param \DateTime $apsDbutestim
     *
     * @return Animation
     */
    public function setApsDbutestim(?\DateTime $apsDbutestim): Animation
    {
        return $this->setTrigger('apsDbutestim', $apsDbutestim);
    }

    /**
     * @return \DateTime
     */
    public function getOverriddencreatedon(): ?\DateTime
    {
        return $this->overriddencreatedon;
    }

    /**
     * @param \DateTime $overriddencreatedon
     *
     * @return Animation
     */
    public function setOverriddencreatedon(?\DateTime $overriddencreatedon): Animation
    {
        return $this->setTrigger('overriddencreatedon', $overriddencreatedon);
    }

    /**
     * @return \DateTime
     */
    public function getApsFermeturedesinscriptions(): ?\DateTime
    {
        return $this->apsFermeturedesinscriptions;
    }

    /**
     * @param \DateTime $apsFermeturedesinscriptions
     *
     * @return Animation
     */
    public function setApsFermeturedesinscriptions(?\DateTime $apsFermeturedesinscriptions): Animation
    {
        return $this->setTrigger('apsFermeturedesinscriptions', $apsFermeturedesinscriptions);
    }

    /**
     * @return string
     */
    public function getModifiedonbehalfbyValue(): ?string
    {
        return $this->modifiedonbehalfbyValue;
    }

    /**
     * @param string $modifiedonbehalfbyValue
     *
     * @return Animation
     */
    public function setModifiedonbehalfbyValue(?string $modifiedonbehalfbyValue): Animation
    {
        return $this->setTrigger('modifiedonbehalfbyValue', $modifiedonbehalfbyValue);
    }

    /**
     * @return int
     */
    public function getApsNbplaceprise(): ?int
    {
        return $this->apsNbplaceprise;
    }

    /**
     * @param int $apsNbplaceprise
     *
     * @return Animation
     */
    public function setApsNbplaceprise(?int $apsNbplaceprise): Animation
    {
        return $this->setTrigger('apsNbplaceprise', $apsNbplaceprise);
    }

    /**
     * @return int
     */
    public function getImportsequencenumber(): ?int
    {
        return $this->importsequencenumber;
    }

    /**
     * @param int $importsequencenumber
     *
     * @return Animation
     */
    public function setImportsequencenumber(?int $importsequencenumber): Animation
    {
        return $this->setTrigger('importsequencenumber', $importsequencenumber);
    }

    /**
     * @return string
     */
    public function getApsCodeproduitax(): ?string
    {
        return $this->apsCodeproduitax;
    }

    /**
     * @param string $apsCodeproduitax
     *
     * @return Animation
     */
    public function setApsCodeproduitax(?string $apsCodeproduitax): Animation
    {
        return $this->setTrigger('apsCodeproduitax', $apsCodeproduitax);
    }

    /**
     * @return string
     */
    public function getOwningteamValue(): ?string
    {
        return $this->owningteamValue;
    }

    /**
     * @param string $owningteamValue
     *
     * @return Animation
     */
    public function setOwningteamValue(?string $owningteamValue): Animation
    {
        return $this->setTrigger('owningteamValue', $owningteamValue);
    }

    /**
     * @return \DateTime
     */
    public function getApsOuverturedesinscriptions(): ?\DateTime
    {
        return $this->apsOuverturedesinscriptions;
    }

    /**
     * @param \DateTime $apsOuverturedesinscriptions
     *
     * @return Animation
     */
    public function setApsOuverturedesinscriptions(?\DateTime $apsOuverturedesinscriptions): Animation
    {
        return $this->setTrigger('apsOuverturedesinscriptions', $apsOuverturedesinscriptions);
    }

    /**
     * @return string
     */
    public function getCreatedonbehalfbyValue(): ?string
    {
        return $this->createdonbehalfbyValue;
    }

    /**
     * @param string $createdonbehalfbyValue
     *
     * @return Animation
     */
    public function setCreatedonbehalfbyValue(?string $createdonbehalfbyValue): Animation
    {
        return $this->setTrigger('createdonbehalfbyValue', $createdonbehalfbyValue);
    }

    /**
     * @return int
     */
    public function getApsNbplacerestante(): ?int
    {
        return $this->apsNbplacerestante;
    }

    /**
     * @param int $apsNbplacerestante
     *
     * @return Animation
     */
    public function setApsNbplacerestante(?int $apsNbplacerestante): Animation
    {
        return $this->setTrigger('apsNbplacerestante', $apsNbplacerestante);
    }

    /**
     * @return int
     */
    public function getUtcconversiontimezonecode(): ?int
    {
        return $this->utcconversiontimezonecode;
    }

    /**
     * @param int $utcconversiontimezonecode
     *
     * @return Animation
     */
    public function setUtcconversiontimezonecode(?int $utcconversiontimezonecode): Animation
    {
        return $this->setTrigger('utcconversiontimezonecode', $utcconversiontimezonecode);
    }

    /**
     * @return float
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param float $price
     *
     * @return Animation
     */
    public function setPrice(?float $price): Animation
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     *
     * @return $this|Animation
     */
    public function setCurrency(?string $currency): Animation
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * @return bool
     */
    public function isRegister(): bool
    {
        return $this->register;
    }

    /**
     * @param bool $register
     *
     * @return Animation
     */
    public function setRegister(bool $register): Animation
    {
        $this->register = $register;

        return $this;
    }

    /**
     * @return string
     */
    public function getApsDescription(): ?string
    {
        return $this->apsDescription;
    }

    /**
     * @param string $apsDescription
     *
     * @return Animation
     */
    public function setApsDescription(?string $apsDescription): Animation
    {
        return $this->setTrigger('apsDescription', $apsDescription);
    }
}
