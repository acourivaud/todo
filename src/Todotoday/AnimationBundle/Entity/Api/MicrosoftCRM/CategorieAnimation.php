<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 26/05/17
 * Time: 15:07
 */

namespace Todotoday\AnimationBundle\Entity\Api\MicrosoftCRM;

use Actiane\ApiConnectorBundle\Lib\EntityProxyTrait;
use Actiane\ApiConnectorBundle\Lib\Mapping as APIConnector;
use JMS\Serializer\Annotation as Serializer;

/**
 * @SuppressWarnings(PHPMD)
 * Class CategorieAnimation
 * @Serializer\ExclusionPolicy("all")
 * @APIConnector\Entity(path="aps_catgoriedanimations",
 *     repositoryId="todotoday.animation.repository.api.categorie_animation")
 * @package Todotoday\CatalogBundle\Entity\Api\MicrosoftCRM
 */
class CategorieAnimation
{
    use EntityProxyTrait;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_codeax", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsCodeax;

    /**
     * @var string
     * @APIConnector\Id()
     * @APIConnector\Column(name="aps_catgoriedanimationid", type="string", guid="id")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsCatgoriedanimationid;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_owningbusinessunit_value", type="string", guid="businessunitid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $owningbusinessunitValue;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="modifiedon", type="datetimez")
     * @Serializer\Expose()
     * @Serializer\Type("DateTime")
     */
    protected $modifiedon;

    /**
     * @var int
     *
     * @APIConnector\Column(name="statecode", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $statecode;

    /**
     * @var int
     *
     * @APIConnector\Column(name="statuscode", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $statuscode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_createdby_value", type="string", guid="systemuserid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $createdbyValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_ownerid_value", type="string", guid="ownerid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $owneridValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_aps_animation_value", type="string", guid="aps_animationid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsAnimationValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_owninguser_value", type="string", guid="systemuserid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $owninguserValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_modifiedby_value", type="string", guid="systemuserid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $modifiedbyValue;

    /**
     * @var int
     *
     * @APIConnector\Column(name="versionnumber", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $versionnumber;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="createdon", type="datetimez")
     * @Serializer\Expose()
     * @Serializer\Type("DateTime")
     */
    protected $createdon;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_name", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsName;

    /**
     * @var int
     *
     * @APIConnector\Column(name="importsequencenumber", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $importsequencenumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_aps_animation_participationuneanimaid_value", type="string", guid="aps_animationid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsAnimationParticipationuneanimaidValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_owningteam_value", type="string", guid="teamid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $owningteamValue;

    /**
     * @var int
     *
     * @APIConnector\Column(name="timezoneruleversionnumber", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $timezoneruleversionnumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_createdonbehalfby_value", type="string", guid="systemuserid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $createdonbehalfbyValue;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="overriddencreatedon", type="datetimez")
     * @Serializer\Expose()
     * @Serializer\Type("DateTime")
     */
    protected $overriddencreatedon;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_modifiedonbehalfby_value", type="string", guid="systemuserid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $modifiedonbehalfbyValue;

    /**
     * @var int
     *
     * @APIConnector\Column(name="utcconversiontimezonecode", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $utcconversiontimezonecode;

    /**
     * @return string
     */
    public function getApsCodeax(): ?string
    {
        return $this->apsCodeax;
    }

    /**
     * @param string $apsCodeax
     *
     * @return CategorieAnimation
     */
    public function setApsCodeax(?string $apsCodeax): CategorieAnimation
    {
        return $this->setTrigger('apsCodeax', $apsCodeax);
    }

    /**
     * @return string
     */
    public function getApsCatgoriedanimationid(): ?string
    {
        return $this->apsCatgoriedanimationid;
    }

    /**
     * @param string $apsCatgoriedanimationid
     *
     * @return CategorieAnimation
     */
    public function setApsCatgoriedanimationid(?string $apsCatgoriedanimationid): CategorieAnimation
    {
        return $this->setTrigger('apsCatgoriedanimationid', $apsCatgoriedanimationid);
    }

    /**
     * @return string
     */
    public function getOwningbusinessunitValue(): ?string
    {
        return $this->owningbusinessunitValue;
    }

    /**
     * @param string $owningbusinessunitValue
     *
     * @return CategorieAnimation
     */
    public function setOwningbusinessunitValue(?string $owningbusinessunitValue): CategorieAnimation
    {
        return $this->setTrigger('owningbusinessunitValue', $owningbusinessunitValue);
    }

    /**
     * @return \DateTime
     */
    public function getModifiedon(): ?\DateTime
    {
        return $this->modifiedon;
    }

    /**
     * @param \DateTime $modifiedon
     *
     * @return CategorieAnimation
     */
    public function setModifiedon(?\DateTime $modifiedon): CategorieAnimation
    {
        return $this->setTrigger('modifiedon', $modifiedon);
    }

    /**
     * @return int
     */
    public function getStatecode(): ?int
    {
        return $this->statecode;
    }

    /**
     * @param int $statecode
     *
     * @return CategorieAnimation
     */
    public function setStatecode(?int $statecode): CategorieAnimation
    {
        return $this->setTrigger('statecode', $statecode);
    }

    /**
     * @return int
     */
    public function getStatuscode(): ?int
    {
        return $this->statuscode;
    }

    /**
     * @param int $statuscode
     *
     * @return CategorieAnimation
     */
    public function setStatuscode(?int $statuscode): CategorieAnimation
    {
        return $this->setTrigger('statuscode', $statuscode);
    }

    /**
     * @return string
     */
    public function getCreatedbyValue(): ?string
    {
        return $this->createdbyValue;
    }

    /**
     * @param string $createdbyValue
     *
     * @return CategorieAnimation
     */
    public function setCreatedbyValue(?string $createdbyValue): CategorieAnimation
    {
        return $this->setTrigger('createdbyValue', $createdbyValue);
    }

    /**
     * @return string
     */
    public function getOwneridValue(): ?string
    {
        return $this->owneridValue;
    }

    /**
     * @param string $owneridValue
     *
     * @return CategorieAnimation
     */
    public function setOwneridValue(?string $owneridValue): CategorieAnimation
    {
        return $this->setTrigger('owneridValue', $owneridValue);
    }

    /**
     * @return string
     */
    public function getApsAnimationValue(): ?string
    {
        return $this->apsAnimationValue;
    }

    /**
     * @param string $apsAnimationValue
     *
     * @return CategorieAnimation
     */
    public function setApsAnimationValue(?string $apsAnimationValue): CategorieAnimation
    {
        return $this->setTrigger('apsAnimationValue', $apsAnimationValue);
    }

    /**
     * @return string
     */
    public function getOwninguserValue(): ?string
    {
        return $this->owninguserValue;
    }

    /**
     * @param string $owninguserValue
     *
     * @return CategorieAnimation
     */
    public function setOwninguserValue(?string $owninguserValue): CategorieAnimation
    {
        return $this->setTrigger('owninguserValue', $owninguserValue);
    }

    /**
     * @return string
     */
    public function getModifiedbyValue(): ?string
    {
        return $this->modifiedbyValue;
    }

    /**
     * @param string $modifiedbyValue
     *
     * @return CategorieAnimation
     */
    public function setModifiedbyValue(?string $modifiedbyValue): CategorieAnimation
    {
        return $this->setTrigger('modifiedbyValue', $modifiedbyValue);
    }

    /**
     * @return int
     */
    public function getVersionnumber(): ?int
    {
        return $this->versionnumber;
    }

    /**
     * @param int $versionnumber
     *
     * @return CategorieAnimation
     */
    public function setVersionnumber(?int $versionnumber): CategorieAnimation
    {
        return $this->setTrigger('versionnumber', $versionnumber);
    }

    /**
     * @return \DateTime
     */
    public function getCreatedon(): ?\DateTime
    {
        return $this->createdon;
    }

    /**
     * @param \DateTime $createdon
     *
     * @return CategorieAnimation
     */
    public function setCreatedon(?\DateTime $createdon): CategorieAnimation
    {
        return $this->setTrigger('createdon', $createdon);
    }

    /**
     * @return string
     */
    public function getApsName(): ?string
    {
        return $this->apsName;
    }

    /**
     * @param string $apsName
     *
     * @return CategorieAnimation
     */
    public function setApsName(?string $apsName): CategorieAnimation
    {
        return $this->setTrigger('apsName', $apsName);
    }

    /**
     * @return int
     */
    public function getImportsequencenumber(): ?int
    {
        return $this->importsequencenumber;
    }

    /**
     * @param int $importsequencenumber
     *
     * @return CategorieAnimation
     */
    public function setImportsequencenumber(?int $importsequencenumber): CategorieAnimation
    {
        return $this->setTrigger('importsequencenumber', $importsequencenumber);
    }

    /**
     * @return string
     */
    public function getApsAnimationParticipationuneanimaidValue(): ?string
    {
        return $this->apsAnimationParticipationuneanimaidValue;
    }

    /**
     * @param string $apsAnimationParticipationuneanimaidValue
     *
     * @return CategorieAnimation
     */
    public function setApsAnimationParticipationuneanimaidValue(
        ?string $apsAnimationParticipationuneanimaidValue
    ): CategorieAnimation {
        return $this->setTrigger('apsAnimationParticipationuneanimaidValue', $apsAnimationParticipationuneanimaidValue);
    }

    /**
     * @return string
     */
    public function getOwningteamValue(): ?string
    {
        return $this->owningteamValue;
    }

    /**
     * @param string $owningteamValue
     *
     * @return CategorieAnimation
     */
    public function setOwningteamValue(?string $owningteamValue): CategorieAnimation
    {
        return $this->setTrigger('owningteamValue', $owningteamValue);
    }

    /**
     * @return int
     */
    public function getTimezoneruleversionnumber(): ?int
    {
        return $this->timezoneruleversionnumber;
    }

    /**
     * @param int $timezoneruleversionnumber
     *
     * @return CategorieAnimation
     */
    public function setTimezoneruleversionnumber(?int $timezoneruleversionnumber): CategorieAnimation
    {
        return $this->setTrigger('timezoneruleversionnumber', $timezoneruleversionnumber);
    }

    /**
     * @return string
     */
    public function getCreatedonbehalfbyValue(): ?string
    {
        return $this->createdonbehalfbyValue;
    }

    /**
     * @param string $createdonbehalfbyValue
     *
     * @return CategorieAnimation
     */
    public function setCreatedonbehalfbyValue(?string $createdonbehalfbyValue): CategorieAnimation
    {
        return $this->setTrigger('createdonbehalfbyValue', $createdonbehalfbyValue);
    }

    /**
     * @return \DateTime
     */
    public function getOverriddencreatedon(): ?\DateTime
    {
        return $this->overriddencreatedon;
    }

    /**
     * @param \DateTime $overriddencreatedon
     *
     * @return CategorieAnimation
     */
    public function setOverriddencreatedon(?\DateTime $overriddencreatedon): CategorieAnimation
    {
        return $this->setTrigger('overriddencreatedon', $overriddencreatedon);
    }

    /**
     * @return string
     */
    public function getModifiedonbehalfbyValue(): ?string
    {
        return $this->modifiedonbehalfbyValue;
    }

    /**
     * @param string $modifiedonbehalfbyValue
     *
     * @return CategorieAnimation
     */
    public function setModifiedonbehalfbyValue(?string $modifiedonbehalfbyValue): CategorieAnimation
    {
        return $this->setTrigger('modifiedonbehalfbyValue', $modifiedonbehalfbyValue);
    }

    /**
     * @return int
     */
    public function getUtcconversiontimezonecode(): ?int
    {
        return $this->utcconversiontimezonecode;
    }

    /**
     * @param int $utcconversiontimezonecode
     *
     * @return CategorieAnimation
     */
    public function setUtcconversiontimezonecode(?int $utcconversiontimezonecode): CategorieAnimation
    {
        return $this->setTrigger('utcconversiontimezonecode', $utcconversiontimezonecode);
    }
}
