<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 26/05/17
 * Time: 15:07
 */

namespace Todotoday\AnimationBundle\Entity\Api\MicrosoftCRM;

use Actiane\ApiConnectorBundle\Lib\EntityProxyTrait;
use Actiane\ApiConnectorBundle\Lib\Mapping as APIConnector;
use JMS\Serializer\Annotation as Serializer;

/**
 * @SuppressWarnings(PHPMD)
 * Class ParticipationAnimation
 * @Serializer\ExclusionPolicy("all")
 * @APIConnector\Entity(path="aps_participationuneanimations",
 *     repositoryId="todotoday.animation.repository.api.participation_animation")
 * @package Todotoday\CatalogBundle\Entity\Api\MicrosoftCRM
 */
class ParticipationAnimation
{
    use EntityProxyTrait;
    /**
     * STATE_CODE_INACTIVE
     */
    const STATE_CODE_INACTIVE = 1;
    /**
     * STATUS_CODE_INACTIVE
     */
    const STATUS_CODE_INACTIVE = 2;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_aps_adhrent_value", type="string", guid="aps_Adhrent")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsAdhrentValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_owningbusinessunit_value", type="string", guid="businessunitid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $owningbusinessunitValue;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="modifiedon", type="datetimez")
     * @Serializer\Expose()
     * @Serializer\Type("DateTime")
     */
    protected $modifiedon;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_participationuneanimationid", type="string", guid="id")
     * @APIConnector\Id()
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsParticipationuneanimationid;

    /**
     * @var int
     *
     * @APIConnector\Column(name="statecode", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $statecode;

    /**
     * @var int
     *
     * @APIConnector\Column(name="statuscode", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $statuscode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_createdby_value", type="string", guid="systemuserid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $createdbyValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_ownerid_value", type="string", guid="ownerid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $owneridValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_aps_animation_value", type="string", guid="aps_Animation")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsAnimationValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_owninguser_value", type="string", guid="systemuserid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $owninguserValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_modifiedby_value", type="string", guid="systemuserid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $modifiedbyValue;

    /**
     * @var int
     *
     * @APIConnector\Column(name="versionnumber", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $versionnumber;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="createdon", type="datetimez")
     * @Serializer\Expose()
     * @Serializer\Type("DateTime")
     */
    protected $createdon;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_name", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsName;

    /**
     * @var int
     *
     * @APIConnector\Column(name="importsequencenumber", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $importsequencenumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_aps_animation_participationuneanimaid_value", type="string",
     *                                                                           guid="aps_animation_participationuneanimaId")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsAnimationParticipationuneanimaidValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_owningteam_value", type="string", guid="teamid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $owningteamValue;

    /**
     * @var int
     *
     * @APIConnector\Column(name="timezoneruleversionnumber", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $timezoneruleversionnumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_createdonbehalfby_value", type="string", guid="systemuserid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $createdonbehalfbyValue;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="overriddencreatedon", type="datetimez")
     * @Serializer\Expose()
     * @Serializer\Type("DateTime")
     */
    protected $overriddencreatedon;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_modifiedonbehalfby_value", type="string", guid="systemuserid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $modifiedonbehalfbyValue;

    /**
     * @var int
     *
     * @APIConnector\Column(name="utcconversiontimezonecode", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $utcconversiontimezonecode;

    /**
     * @var string
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $salesOrderHeaderId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_commandeid", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsCommandeId;

    /**
     * @var Animation
     * @Serializer\Expose()
     */
    protected $animation;

    /**
     * @var bool
     * @Serializer\Expose()
     * @Serializer\Type("boolean")
     */
    protected $register = true;

    /**
     * @return Animation
     */
    public function getAnimation(): ?Animation
    {
        return $this->animation;
    }

    /**
     * @param Animation $animation
     *
     * @return ParticipationAnimation
     */
    public function setAnimation(Animation $animation): ParticipationAnimation
    {
        $this->animation = $animation;

        return $this;
    }

    /**
     * @return string|array
     */
    public function getApsAdhrentValue()
    {
        return $this->apsAdhrentValue;
    }

    /**
     * @param string $apsAdhrentValue
     *
     * @return ParticipationAnimation
     */
    public function setApsAdhrentValue(?string $apsAdhrentValue): ParticipationAnimation
    {
        return $this->setTrigger('apsAdhrentValue', $apsAdhrentValue);
    }

    /**
     * @return string|array
     */
    public function getApsAnimationParticipationuneanimaidValue()
    {
        return $this->apsAnimationParticipationuneanimaidValue;
    }

    /**
     * @param string $apsAnimationParticipationuneanimaidValue
     *
     * @return ParticipationAnimation
     */
    public function setApsAnimationParticipationuneanimaidValue(
        ?string $apsAnimationParticipationuneanimaidValue
    ): ParticipationAnimation {
        return $this->setTrigger('apsAnimationParticipationuneanimaidValue', $apsAnimationParticipationuneanimaidValue);
    }

    /**
     * @return string|array
     */
    public function getApsAnimationValue()
    {
        return $this->apsAnimationValue;
    }

    /**
     * @param string $apsAnimationValue
     *
     * @return ParticipationAnimation
     */
    public function setApsAnimationValue(?string $apsAnimationValue): ParticipationAnimation
    {
        return $this->setTrigger('apsAnimationValue', $apsAnimationValue);
    }

    /**
     * @return string
     */
    public function getApsCommandeId(): ?string
    {
        return $this->apsCommandeId;
    }

    /**
     * @param string|null $apsCommandeId
     *
     * @return ParticipationAnimation
     */
    public function setApsCommandeId(?string $apsCommandeId): ParticipationAnimation
    {
        return $this->setTrigger('apsCommandeId', $apsCommandeId);
    }

    /**
     * @return string
     */
    public function getApsName(): ?string
    {
        return $this->apsName;
    }

    /**
     * @param string $apsName
     *
     * @return ParticipationAnimation
     */
    public function setApsName(?string $apsName): ParticipationAnimation
    {
        return $this->setTrigger('apsName', $apsName);
    }

    /**
     * @return string
     */
    public function getApsParticipationuneanimationid(): ?string
    {
        return $this->apsParticipationuneanimationid;
    }

    /**
     * @param string $apsParticipationuneanimationid
     *
     * @return ParticipationAnimation
     */
    public function setApsParticipationuneanimationid(?string $apsParticipationuneanimationid): ParticipationAnimation
    {
        return $this->setTrigger('apsParticipationuneanimationid', $apsParticipationuneanimationid);
    }

    /**
     * @return string
     */
    public function getCreatedbyValue(): ?string
    {
        return $this->createdbyValue;
    }

    /**
     * @param string $createdbyValue
     *
     * @return ParticipationAnimation
     */
    public function setCreatedbyValue(?string $createdbyValue): ParticipationAnimation
    {
        return $this->setTrigger('createdbyValue', $createdbyValue);
    }

    /**
     * @return \DateTime
     */
    public function getCreatedon(): ?\DateTime
    {
        return $this->createdon;
    }

    /**
     * @param \DateTime $createdon
     *
     * @return ParticipationAnimation
     */
    public function setCreatedon(?\DateTime $createdon): ParticipationAnimation
    {
        return $this->setTrigger('createdon', $createdon);
    }

    /**
     * @return string
     */
    public function getCreatedonbehalfbyValue(): ?string
    {
        return $this->createdonbehalfbyValue;
    }

    /**
     * @param string $createdonbehalfbyValue
     *
     * @return ParticipationAnimation
     */
    public function setCreatedonbehalfbyValue(?string $createdonbehalfbyValue): ParticipationAnimation
    {
        return $this->setTrigger('createdonbehalfbyValue', $createdonbehalfbyValue);
    }

    /**
     * @return int
     */
    public function getImportsequencenumber(): ?int
    {
        return $this->importsequencenumber;
    }

    /**
     * @param int $importsequencenumber
     *
     * @return ParticipationAnimation
     */
    public function setImportsequencenumber(?int $importsequencenumber): ParticipationAnimation
    {
        return $this->setTrigger('importsequencenumber', $importsequencenumber);
    }

    /**
     * @return string
     */
    public function getModifiedbyValue(): ?string
    {
        return $this->modifiedbyValue;
    }

    /**
     * @param string $modifiedbyValue
     *
     * @return ParticipationAnimation
     */
    public function setModifiedbyValue(?string $modifiedbyValue): ParticipationAnimation
    {
        return $this->setTrigger('modifiedbyValue', $modifiedbyValue);
    }

    /**
     * @return \DateTime
     */
    public function getModifiedon(): ?\DateTime
    {
        return $this->modifiedon;
    }

    /**
     * @param \DateTime $modifiedon
     *
     * @return ParticipationAnimation
     */
    public function setModifiedon(?\DateTime $modifiedon): ParticipationAnimation
    {
        return $this->setTrigger('modifiedon', $modifiedon);
    }

    /**
     * @return string
     */
    public function getModifiedonbehalfbyValue(): ?string
    {
        return $this->modifiedonbehalfbyValue;
    }

    /**
     * @param string $modifiedonbehalfbyValue
     *
     * @return ParticipationAnimation
     */
    public function setModifiedonbehalfbyValue(?string $modifiedonbehalfbyValue): ParticipationAnimation
    {
        return $this->setTrigger('modifiedonbehalfbyValue', $modifiedonbehalfbyValue);
    }

    /**
     * @return \DateTime
     */
    public function getOverriddencreatedon(): ?\DateTime
    {
        return $this->overriddencreatedon;
    }

    /**
     * @param \DateTime $overriddencreatedon
     *
     * @return ParticipationAnimation
     */
    public function setOverriddencreatedon(?\DateTime $overriddencreatedon): ParticipationAnimation
    {
        return $this->setTrigger('overriddencreatedon', $overriddencreatedon);
    }

    /**
     * @return string|array
     */
    public function getOwneridValue()
    {
        return $this->owneridValue;
    }

    /**
     * @param string $owneridValue
     *
     * @return ParticipationAnimation
     */
    public function setOwneridValue(?string $owneridValue): ParticipationAnimation
    {
        return $this->setTrigger('owneridValue', $owneridValue);
    }

    /**
     * @return string
     */
    public function getOwningbusinessunitValue(): ?string
    {
        return $this->owningbusinessunitValue;
    }

    /**
     * @param string $owningbusinessunitValue
     *
     * @return ParticipationAnimation
     */
    public function setOwningbusinessunitValue(?string $owningbusinessunitValue): ParticipationAnimation
    {
        return $this->setTrigger('owningbusinessunitValue', $owningbusinessunitValue);
    }

    /**
     * @return string
     */
    public function getOwningteamValue(): ?string
    {
        return $this->owningteamValue;
    }

    /**
     * @param string $owningteamValue
     *
     * @return ParticipationAnimation
     */
    public function setOwningteamValue(?string $owningteamValue): ParticipationAnimation
    {
        return $this->setTrigger('owningteamValue', $owningteamValue);
    }

    /**
     * @return string
     */
    public function getOwninguserValue(): ?string
    {
        return $this->owninguserValue;
    }

    /**
     * @param string $owninguserValue
     *
     * @return ParticipationAnimation
     */
    public function setOwninguserValue(?string $owninguserValue): ParticipationAnimation
    {
        return $this->setTrigger('owninguserValue', $owninguserValue);
    }

    /**
     * @return string
     */
    public function getSalesOrderHeaderId(): ?string
    {
        return $this->salesOrderHeaderId;
    }

    /**
     * @param string $salesOrderHeaderId
     *
     * @return ParticipationAnimation
     */
    public function setSalesOrderHeaderId(string $salesOrderHeaderId): ParticipationAnimation
    {
        $this->salesOrderHeaderId = $salesOrderHeaderId;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatecode(): ?int
    {
        return $this->statecode;
    }

    /**
     * @param int $statecode
     *
     * @return ParticipationAnimation
     */
    public function setStatecode(?int $statecode): ParticipationAnimation
    {
        $this->setTrigger('statecode', $statecode);

        return $this->refreshStatus();
    }

    /**
     * @return int
     */
    public function getStatuscode(): ?int
    {
        return $this->statuscode;
    }

    /**
     * @param int $statuscode
     *
     * @return ParticipationAnimation
     */
    public function setStatuscode(?int $statuscode): ParticipationAnimation
    {
        $this->setTrigger('statuscode', $statuscode);

        return $this->refreshStatus();
    }

    /**
     * @return int
     */
    public function getTimezoneruleversionnumber(): ?int
    {
        return $this->timezoneruleversionnumber;
    }

    /**
     * @param int $timezoneruleversionnumber
     *
     * @return ParticipationAnimation
     */
    public function setTimezoneruleversionnumber(?int $timezoneruleversionnumber): ParticipationAnimation
    {
        return $this->setTrigger('timezoneruleversionnumber', $timezoneruleversionnumber);
    }

    /**
     * @return int
     */
    public function getUtcconversiontimezonecode(): ?int
    {
        return $this->utcconversiontimezonecode;
    }

    /**
     * @param int $utcconversiontimezonecode
     *
     * @return ParticipationAnimation
     */
    public function setUtcconversiontimezonecode(?int $utcconversiontimezonecode): ParticipationAnimation
    {
        return $this->setTrigger('utcconversiontimezonecode', $utcconversiontimezonecode);
    }

    /**
     * @return int
     */
    public function getVersionnumber(): ?int
    {
        return $this->versionnumber;
    }

    /**
     * @param int $versionnumber
     *
     * @return ParticipationAnimation
     */
    public function setVersionnumber(?int $versionnumber): ParticipationAnimation
    {
        return $this->setTrigger('versionnumber', $versionnumber);
    }

    /**
     * @return bool
     */
    public function isRegister(): ?bool
    {
        return $this->register;
    }

    /**
     * @param bool $register
     */
    public function setRegister(bool $register)
    {
        $this->register = $register;
    }

    /**
     * @param null|string $apsAdhrentValue
     *
     * @return ParticipationAnimation
     */
    public function setApsAdhrentValueGuid(?string $apsAdhrentValue): self
    {
        return $this->setTriggerGuid(
            'apsAdhrentValue',
            array(
                'target' => 'accounts',
                'value' => $apsAdhrentValue,
            )
        );
    }

    /**
     * @param null|string $apsAnimationParticipationuneanimaidValue
     *
     * @return ParticipationAnimation
     */
    public function setApsAnimationParticipationuneanimaidValueGuid(
        ?string $apsAnimationParticipationuneanimaidValue
    ): ParticipationAnimation {
        return $this->setTriggerGuid(
            'apsAnimationParticipationuneanimaidValue',
            array(
                'target' => 'aps_animations',
                'value' => $apsAnimationParticipationuneanimaidValue,
            )
        );
    }

    /**
     * @param null|string $apsAnimationId
     *
     * @return ParticipationAnimation
     */
    public function setApsAnimationValueGuid(?string $apsAnimationId): self
    {
        return $this->setTriggerGuid(
            'apsAnimationValue',
            array(
                'target' => 'aps_animations',
                'value' => $apsAnimationId,
            )
        );
    }

    /**
     * Method to call when persisting new value
     * Need to target which entity is bound
     *
     * @param string $owneridValue
     * @param string $target
     *
     * @return ParticipationAnimation
     */
    public function setOwnerIdValueGuid(string $owneridValue, string $target): self
    {
        return $this->setTriggerGuid(
            'owneridValue',
            array(
                'target' => $target,
                'value' => $owneridValue,
            )
        );
    }

    /**
     * @return ParticipationAnimation
     */
    private function refreshStatus(): ParticipationAnimation
    {
        if ($this->getStatecode() === $this::STATE_CODE_INACTIVE
//            && $this->getStatuscode() === $this::STATUS_CODE_INACTIVE
        ) {
            $this->setRegister(false);
        }

        return $this;
    }
}
