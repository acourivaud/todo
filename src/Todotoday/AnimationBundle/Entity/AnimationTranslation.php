<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 08/08/17
 * Time: 15:23
 */

namespace Todotoday\AnimationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Translatable\Entity\MappedSuperclass\AbstractPersonalTranslation;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class AnimationTranslation
 * @ORM\Entity()
 * @ORM\Table(schema="cms",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="lookup_unique_animation_translation_idx", columns={
 *         "locale", "object_id", "field"})}
 * )
 *
 * @package Todotoday\AnimationBundle\Entity
 * @Serializer\ExclusionPolicy("all")
 */
class AnimationTranslation extends AbstractPersonalTranslation
{
    /**
     * @ORM\ManyToOne(
     *     targetEntity="Todotoday\AnimationBundle\Entity\Animation",
     *     inversedBy="translations"
     * )
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $object;
}
