<?php declare(strict_types=1);

namespace Todotoday\AnimationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;
use Todotoday\MediaBundle\Entity\Media;

/**
 * Animation
 *
 * @ORM\Table(name="animation")
 * @ORM\Entity(repositoryClass="Todotoday\AnimationBundle\Repository\AnimationRepository")
 * @Gedmo\TranslationEntity(class="AnimationTranslation")
 * @Serializer\ExclusionPolicy("all")
 */
class Animation extends AbstractPersonalTranslatable implements TranslatableInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Expose()
     * @Serializer\Groups({"animation"})
     */
    private $id;

    /**
     * @var array
     */
    private $sessions;

    /**
     * @var int
     */
    private $nbSessions;

    /**
     * @var string
     *
     * @ORM\Column(name="AnimationCategory", type="string", length=255)
     * @Serializer\Expose()
     * @Serializer\SerializedName("aps_codeproduitax")
     * @Serializer\Groups({"animation"})
     *
     */
    private $animationCategory;

    /**
     * @var string
     * @ORM\Column(name="titre", type="string", length=100)
     * @Serializer\Expose()
     * @Gedmo\Translatable()
     * @Serializer\SerializedName("aps_animation1")
     * @Serializer\Groups({"animation"})
     */
    private $titre;

    /**
     * @var string
     * @Serializer\Expose()
     * @Serializer\Groups({"animation"})
     * @Gedmo\Translatable()
     * @ORM\Column(name="Content", type="text")
     */
    private $content;

    /**
     * @var Media
     * @Serializer\Expose()
     * @Serializer\Groups({"animation"})
     * @ORM\ManyToOne(targetEntity="Todotoday\MediaBundle\Entity\Media", cascade={"persist","remove","refresh"})
     */
    private $image;

    /**
     * @var AnimationTranslation[]
     * @Serializer\Exclude()
     * @Serializer\Groups({"translations"})
     * @ORM\OneToMany(
     *     targetEntity="Todotoday\AnimationBundle\Entity\AnimationTranslation",
     *     mappedBy="object",
     *     cascade={"persist","remove"}
     * )
     */
    protected $translations;

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set animationCategory
     *
     * @param string|null $animationCategory
     *
     * @return Animation
     */
    public function setAnimationCategory(?string $animationCategory): ?Animation
    {
        $this->animationCategory = $animationCategory;

        return $this;
    }

    /**
     * Get animationCategory
     *
     * @return string|null
     */
    public function getAnimationCategory(): ?string
    {
        return $this->animationCategory;
    }

    /**
     * Set content
     *
     * @param string|null $content
     *
     * @return Animation
     */
    public function setContent(?string $content): Animation
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @return Media
     */
    public function getImage(): ?Media
    {
        return $this->image;
    }

    /**
     * @param Media $image
     *
     * @return Animation
     */
    public function setImage(?Media $image): Animation
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return array
     */
    public function getSessions(): ?array
    {
        return $this->sessions;
    }

    /**
     * @return int|null
     */
    public function getNbSessions(): ?int
    {
        return count($this->sessions);
    }

    /**
     * @return string
     */
    public function getTitre(): ?string
    {
        return $this->titre;
    }

    /**
     * @param string $titre
     *
     * @return Animation
     */
    public function setTitre(?string $titre): Animation
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * @return null|string
     */
    public function __toString(): ?string
    {
        return (string) $this->getTitre();
    }

    /**
     * @param array $sessions
     *
     * @return Animation
     */
    public function setSessions(array $sessions): self
    {
        $this->sessions = $sessions;

        return $this;
    }

    /**
     * @param int $nbSessions
     *
     * @return Animation
     */
    public function setNbSessions(int $nbSessions): self
    {
        $this->nbSessions = $nbSessions;

        return $this;
    }
}
