<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 13/10/17
 * Time: 10:33
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\PluginBundle\Tests\Services
 *
 * @subpackage Todotoday\PluginBundle\Tests\Services
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\PluginBundle\Tests\Services;

use Liip\FunctionalTestBundle\Test\WebTestCase;
use Todotoday\PluginBundle\Entity\Partner\Globetipser\Tip;
use Todotoday\PluginBundle\Entity\Partner\Globetipser\User;

/**
 * Class GlobetipserConnectionTest
 */
class GlobetipserConnectionTest extends WebTestCase
{
    /**
     * @small
     */
    public function testGetPublicUser()
    {
        $service = $this->getContainer()->get('todotoday.plugin.connection.globetipser');
        $result = $service->sendResultToPlugin();
        $this->assertNotNull($result);
        foreach ($result as $tip) {
            $this->assertInstanceOf(Tip::class, $tip);
            $this->assertInstanceOf(User::class, $tip->getAuthor());
        }
    }
}
