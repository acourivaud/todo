<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 13/10/17
 * Time: 10:33
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\PluginBundle\Tests\Services
 *
 * @subpackage Todotoday\PluginBundle\Tests\Services
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\PluginBundle\Tests\Services;

use Liip\FunctionalTestBundle\Test\WebTestCase;
use Todotoday\PluginBundle\Entity\Partner\Collibris\Book;

/**
 * Class CollibrisConnectionTest
 */
class CollibrisConnectionTest extends WebTestCase
{
    /**
     * @small
     */
    public function testGetReviews()
    {
        $service = $this->getContainer()->get('todotoday.plugin.connection.collibris');
        $books = $service->sendResultToPlugin();
        $this->assertGreaterThan(0, count($books));
        foreach ($books as $book) {
            $this->assertInstanceOf(Book::class, $book);
            $this->assertNotNull($book->getAuthors());
            $this->assertNotNull($book->getTitle());
            $this->assertNotNull($book->getCover());
        }
    }
}
