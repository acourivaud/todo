<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 18/10/17
 * Time: 10:36
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\PluginBundle\Controlle\Admin
 *
 * @subpackage Todotoday\PluginBundle\Controlle\Admin
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\PluginBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController;

/**
 * Class PluginAdminController
 */
class PluginAdminController extends CRUDController
{
}
