<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 18/10/17
 * Time: 11:29
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\PluginBundle\Controller\Admin
 *
 * @subpackage Todotoday\PluginBundle\Controller\Admin
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\PluginBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController;

/**
 * Class LinkAgencyPluginAdminController
 */
class LinkAgencyPluginAdminController extends CRUDController
{
}
