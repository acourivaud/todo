<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 15/10/17
 * Time: 10:39
 */

namespace Todotoday\PluginBundle\Exceptions;

use Throwable;

/**
 * Class PartnerCurlException
 * @package Todotoday\PluginBundle\Exceptions
 */
class PartnerCurlException extends \Exception
{
    /**
     * PartnerCurlException constructor.
     *
     * @param string         $partner
     * @param string         $url
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct(string $partner, string $url, string $message, $code = 0, Throwable $previous = null)
    {
        $message = sprintf('Partner %s call %s failed with message %s', $partner, $url, $message);
        parent::__construct($message, $code, $previous);
    }
}
