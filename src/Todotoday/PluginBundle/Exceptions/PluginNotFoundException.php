<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 11/05/17
 * Time: 14:14
 */

namespace Todotoday\PluginBundle\Exceptions;

use Throwable;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\PluginBundle\Entity\Plugin;

/**
 * Class PluginNotFoundException
 * @package Todotoday\PluginBundle\Exceptions
 */
class PluginNotFoundException extends \Exception
{
    /**
     * PluginNotFoundException constructor.
     *
     * @param string         $pluginSlug
     * @param Agency         $agency
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct(
        string $pluginSlug = '',
        Agency $agency = null,
        $code = 0,
        Throwable $previous = null
    ) {
        $message = 'Plugin not found';
        $message .= $pluginSlug ? ' for the Plugin "' . $pluginSlug . '"' : '';
        $message .= $agency ? ' and for the Agency "' . $agency->getSlug() . '"' : '';
        parent::__construct($message, $code, $previous);
    }
}
