<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 19/10/17
 * Time: 15:17
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\PluginBundle\Exceptions
 *
 * @subpackage Todotoday\PluginBundle\Exceptions
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\PluginBundle\Exceptions;

use Throwable;

/**
 * Class PluginIncorrectException
 */
class PluginIncorrectException extends \Exception
{
    /**
     * PluginIncorrectException constructor.
     *
     * @param string         $slug
     * @param string         $category
     * @param string         $abstractClass
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct(
        string $slug,
        string $category,
        string $abstractClass,
        $code = 0,
        Throwable $previous = null
    ) {
        $message = sprintf(
            'The plugin %s of category %s is not extending the %s class',
            $slug,
            $category,
            $abstractClass
        );
        parent::__construct($message, $code, $previous);
    }
}
