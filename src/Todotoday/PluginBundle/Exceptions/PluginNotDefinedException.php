<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 19/10/17
 * Time: 15:20
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\PluginBundle\Exceptions
 *
 * @subpackage Todotoday\PluginBundle\Exceptions
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\PluginBundle\Exceptions;

use Throwable;

/**
 * Class PluginNotDefinedException
 */
class PluginNotDefinedException extends \Exception
{
    /**
     * PluginNotDefinedException constructor.
     *
     * @param string         $slug
     * @param string         $category
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct(string $slug, string $category, $code = 0, Throwable $previous = null)
    {
        $message = sprintf('The plugin "%s" is not defined for the category %s', $slug, $category);
        parent::__construct($message, $code, $previous);
    }
}
