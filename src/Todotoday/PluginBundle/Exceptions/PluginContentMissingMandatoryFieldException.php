<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 11/05/17
 * Time: 14:14
 */

namespace Todotoday\PluginBundle\Exceptions;

use Throwable;
use Todotoday\CMSBundle\Entity\Post;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\PluginBundle\Entity\Plugin;

/**
 * Class PostPluginMissingMandatoryFieldException
 * @package Todotoday\PluginBundle\Exceptions
 */
class PluginContentMissingMandatoryFieldException extends \Exception
{
    /**
     * PostPluginMissingMandatoryFieldException constructor.
     *
     * @param Plugin         $plugin
     * @param mixed          $content
     * @param string         $contentName
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct(
        Plugin $plugin,
        $content,
        string $contentName,
        $code = 0,
        Throwable $previous = null
    ) {
        $message = 'Plugin "' . $plugin->getCategory() . '.' . $plugin->getName() . '"';
        $message .= ' can\'t add ' . $contentName . ' "' . $content . '" because one mandatory field is empty.';
        parent::__construct($message, $code, $previous);
    }
}
