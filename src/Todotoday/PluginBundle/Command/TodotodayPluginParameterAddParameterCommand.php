<?php declare(strict_types=1);

namespace Todotoday\PluginBundle\Command;

use Actiane\ToolsBundle\Traits\OutputFormatLoggerTrait;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Todotoday\PluginBundle\Entity\Plugin;
use Todotoday\PluginBundle\Exceptions\PluginNotFoundException;

/**
 * Class TodotodayPluginParameterAddCommand
 */
class TodotodayPluginParameterAddParameterCommand extends ContainerAwareCommand
{
    /**
     * @var OutputInterface
     */
    private $output;

    use OutputFormatLoggerTrait;

    /**
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function configure(): void
    {
        $this
            ->setName('todotoday:plugin:parameter:add')
            ->setDescription('Add a parameter to a plugin')
            ->addArgument('slug', InputArgument::REQUIRED, 'Slug of the plugin (category.name)')
            ->addArgument('parameter', InputArgument::REQUIRED, 'Parameter label to add')
            ->addArgument('value', InputArgument::OPTIONAL, 'Parameter\'s value');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return void
     * @throws \Todotoday\PluginBundle\Exceptions\PluginNotDefinedException
     * @throws \Todotoday\PluginBundle\Exceptions\PluginIncorrectException
     * @throws \Todotoday\PluginBundle\Exceptions\PluginNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \LogicException
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $this->initFormat($output);
        $this->output = $output;

        $em = $this->getContainer()->get('doctrine');
        $pluginRepo = $em->getRepository('TodotodayPluginBundle:Plugin');
        /** @var Plugin $plugin */
        if (!$plugin = $pluginRepo->findOneBy(['slug' => $input->getArgument('slug')])) {
            throw new PluginNotFoundException($input->getArgument('slug'));
        }

        foreach ($plugin->getLinkAgencies() as $linkAgencyPlugin) {
            $params = $linkAgencyPlugin->getParameters();
//            $params[$input->getArgument('parameter')] = $input->getArgument('value');
            $linkAgencyPlugin->setParameters(
                array_merge($params, [$input->getArgument('parameter') => $input->getArgument('value')])
            );
        }
        $em->getManager()->flush();

        $message = sprintf(
            '<warning>Parameter </warning><notice>%s </notice><warning>added to plugin </warning>
<notice>%s</notice><warning> with value %s</warning>',
            $input->getArgument('parameter'),
            $input->getArgument('slug'),
            $input->getArgument('value')
        );
        $this->output->writeln($message);
    }
}
