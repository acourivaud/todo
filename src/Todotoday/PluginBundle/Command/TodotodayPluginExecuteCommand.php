<?php declare(strict_types=1);

namespace Todotoday\PluginBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class TodotodayPluginExecuteCommand
 * @package Todotoday\PluginBundle\Command
 */
class TodotodayPluginExecuteCommand extends ContainerAwareCommand
{
    /**
     *
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function configure(): void
    {
        $this
            ->setName('todotoday:plugin:execute')
            ->setDescription('Execute all the plugin tasks');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $pluginManager = $this->getContainer()->get('todotoday.plugin.plugin_manager');
        $pluginManager->launchCronPlugins();
    }
}
