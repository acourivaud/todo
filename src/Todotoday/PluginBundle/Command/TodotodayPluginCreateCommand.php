<?php declare(strict_types=1);

namespace Todotoday\PluginBundle\Command;

use Actiane\ToolsBundle\Traits\OutputFormatLoggerTrait;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Repository\AgencyRepository;
use Todotoday\PluginBundle\Entity\LinkAgencyPlugin;
use Todotoday\PluginBundle\Entity\Plugin;
use Todotoday\PluginBundle\Enum\PluginCategoryEnum;

/**
 * Class TodotodayPluginCreateCommand
 */
class TodotodayPluginCreateCommand extends ContainerAwareCommand
{
    use OutputFormatLoggerTrait;

    /**
     * @var OutputInterface
     */
    private $output;

    /**
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function configure(): void
    {
        $this
            ->setName('todotoday:plugin:create')
            ->setDescription('Create a plugin and link it to all agencies')
            ->addArgument('category', InputArgument::REQUIRED, 'Category (catalog, post, social) of the plugin')
            ->addArgument('name', InputArgument::REQUIRED, 'Name of the plugin');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     * @throws \Symfony\Component\Security\Acl\Exception\AclAlreadyExistsException
     * @throws \Symfony\Component\Security\Acl\Exception\InvalidDomainObjectException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \LogicException
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $this->initFormat($output);
        $this->output = $output;

        $aclProvider = $this->getContainer()->get('security.acl.provider');

        $args = $input->getArguments();
        $pluginSlug = PluginCategoryEnum::getNewInstance($args['category']) . '.' . $args['name'];

        $em = $this->getContainer()->get('doctrine')->getManager();
        $plugin = (new Plugin($pluginSlug))->setEnabled(false);

        /** @var AgencyRepository $agencyRepo */
        $agencyRepo = $em->getRepository(Agency::class);
        /** @var Agency[] $agencies */
        $agencies = $agencyRepo->findAll();
        foreach ($agencies as $agency) {
            $linkAgencyPlugin = (new LinkAgencyPlugin())->setEnabled(false)->setAgency($agency);
            $plugin->addLinkAgency($linkAgencyPlugin);
        }
        $em->persist($plugin);
        $aclProvider->createAcl(ObjectIdentity::fromDomainObject($plugin));
        foreach ($plugin->getLinkAgencies() as $linkAgencyPlugin) {
            $aclProvider->createAcl(ObjectIdentity::fromDomainObject($linkAgencyPlugin));
        }
        $em->flush();

        $message = sprintf(
            '<warning>Plugin </warning><notice>%s</notice>
<warning> created and linked for </warning>
<notice>%d</notice> agencies',
            $pluginSlug,
            count($agencies)
        );
        $this->output->writeln($message);
    }
}
