<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 15/10/17
 * Time: 11:19
 */

namespace Todotoday\PluginBundle\Entity\Partner\Globetipser;

/**
 * Class Tip
 * @package Todotoday\PluginBundle\Entity\Partner\Globetipser
 */
class Tip
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var bool
     */
    private $display;

    /**
     * @var User
     */
    private $author;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $category;

    /**
     * @var string
     */
    private $continent;

    /**
     * @var string
     */
    private $country;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $lat;

    /**
     * @var string
     */
    private $lon;

    /**
     * @var string
     */
    private $text;

    /**
     * @var string
     */
    private $cover;

    /**
     * @var int
     */
    private $v;

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string|null $id
     *
     * @return Tip
     */
    public function setId(?string $id): Tip
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function isDisplay(): ?bool
    {
        return $this->display;
    }

    /**
     * @param bool|null $display
     *
     * @return Tip
     */
    public function setDisplay(?bool $display): Tip
    {
        $this->display = $display;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getAuthor(): ?User
    {
        return $this->author;
    }

    /**
     * @param User|null $author
     *
     * @return Tip
     */
    public function setAuthor(?User $author): Tip
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     *
     * @return Tip
     */
    public function setName(?string $name): Tip
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCategory(): ?string
    {
        return $this->category;
    }

    /**
     * @param string|null $category
     *
     * @return Tip
     */
    public function setCategory(?string $category): Tip
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getContinent(): ?string
    {
        return $this->continent;
    }

    /**
     * @param string|null $continent
     *
     * @return Tip
     */
    public function setContinent(?string $continent): Tip
    {
        $this->continent = $continent;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string|null $country
     *
     * @return Tip
     */
    public function setCountry(?string $country): Tip
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string|null $city
     *
     * @return Tip
     */
    public function setCity(?string $city): Tip
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     *
     * @return Tip
     */
    public function setAddress(?string $address): Tip
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLat(): ?string
    {
        return $this->lat;
    }

    /**
     * @param string|null $lat
     *
     * @return Tip
     */
    public function setLat(?string $lat): Tip
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLon(): ?string
    {
        return $this->lon;
    }

    /**
     * @param string|null $lon
     *
     * @return Tip
     */
    public function setLon(?string $lon): Tip
    {
        $this->lon = $lon;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string|null $text
     *
     * @return Tip
     */
    public function setText(?string $text): Tip
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCover(): ?string
    {
        return $this->cover;
    }

    /**
     * @param string|null $cover
     *
     * @return Tip
     */
    public function setCover(?string $cover): Tip
    {
        $this->cover = $cover;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getV(): ?int
    {
        return $this->v;
    }

    /**
     * @param int|null $v
     *
     * @return Tip
     */
    public function setV(?int $v): Tip
    {
        $this->v = $v;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFullLocation(): ?string
    {
        return '(' . $this->getContinent() . ' / ' . $this->getCountry() . ' / ' . $this->getCity() . ')';
    }
}
