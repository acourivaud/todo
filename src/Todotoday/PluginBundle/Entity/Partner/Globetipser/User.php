<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 15/10/17
 * Time: 11:17
 */

namespace Todotoday\PluginBundle\Entity\Partner\Globetipser;

/**
 * Class User
 * @package Todotoday\PluginBundle\Entity\Partner\Globetipser
 */
/**
 * Class User
 * @package Todotoday\PluginBundle\Entity\Partner\Globetipser
 */
class User
{
    /**
     * @var string
     */
    private $fcbId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string|null
     */
    private $avatar;

    /**
     * @var int
     */
    private $v;

    /**
     * @var Tip[]
     */
    private $tips = [];

    /**
     * @return string
     */
    public function getFcbId(): string
    {
        return $this->fcbId;
    }

    /**
     * @param string $fcbId
     *
     * @return User
     */
    public function setFcbId(string $fcbId): User
    {
        $this->fcbId = $fcbId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     *
     * @return User
     */
    public function setName(?string $name): User
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    /**
     * @param string $avatar
     *
     * @return User
     */
    public function setAvatar(?string $avatar): User
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * @return int
     */
    public function getV(): int
    {
        return $this->v;
    }

    /**
     * @param int $v
     *
     * @return User
     */
    public function setV(int $v): User
    {
        $this->v = $v;

        return $this;
    }

    /**
     * @return Tip[]
     */
    public function getTips(): array
    {
        return $this->tips;
    }

    /**
     * @param Tip[] $tips
     *
     * @return User
     */
    public function setTips(array $tips): User
    {
        $this->tips = $tips;

        return $this;
    }

    /**
     * @param Tip $tip
     *
     * @return User
     */
    public function addTip(Tip $tip): User
    {
        $this->tips[] = $tip;

        return $this;
    }
}
