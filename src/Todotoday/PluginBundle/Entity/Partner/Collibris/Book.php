<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 15/10/17
 * Time: 10:53
 */

namespace Todotoday\PluginBundle\Entity\Partner\Collibris;

/**
 * Class Book
 * @package Todotoday\PluginBundle\Entity\Partner\Collibris
 */
class Book
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $authors;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $cover;

    /**
     * @var string
     */
    private $collibrisUrl;

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     *
     * @return Book
     */
    public function setTitle(?string $title): Book
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAuthors(): ?string
    {
        return $this->authors;
    }

    /**
     * @param string|null $authors
     *
     * @return Book
     */
    public function setAuthors(?string $authors): Book
    {
        $this->authors = $authors;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     *
     * @return Book
     */
    public function setDescription(?string $description): Book
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCover(): ?string
    {
        return $this->cover;
    }

    /**
     * @param string|null $cover
     *
     * @return Book
     */
    public function setCover(?string $cover): Book
    {
        $this->cover = $cover;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCollibrisUrl(): ?string
    {
        return $this->collibrisUrl;
    }

    /**
     * @param string|null $collibrisUrl
     *
     * @return Book
     */
    public function setCollibrisUrl(?string $collibrisUrl): Book
    {
        $this->collibrisUrl = $collibrisUrl;

        return $this;
    }
}
