<?php declare(strict_types=1);

namespace Todotoday\PluginBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Doctrine\ORM\PersistentCollection;
use Todotoday\CMSBundle\Entity\Post;
use Todotoday\PluginBundle\Enum\PluginCategoryEnum;
use Todotoday\SocialBundle\Entity\Tag;

/**
 * Class Plugin
 *
 * @ORM\Table(name="plugin", schema="plugin")
 * @ORM\Entity(repositoryClass="Todotoday\PluginBundle\Repository\PluginRepository")
 */
class Plugin
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, unique=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"Default", "Social"})
     */
    private $slug;

    /**
     * @var Collection|LinkAgencyPlugin[]
     *
     * @ORM\OneToMany(targetEntity="Todotoday\PluginBundle\Entity\LinkAgencyPlugin", mappedBy="plugin",
     *     cascade={"persist"})
     */
    private $linkAgencies;

    /**
     * @var Collection|Tag[]
     *
     * @ORM\ManyToMany(targetEntity="Todotoday\SocialBundle\Entity\Tag", inversedBy="plugins",
     *     cascade={"persist"})
     * @ORM\JoinTable(schema="plugin", name="link_plugin_tag")
     *
     * @Serializer\Expose()
     * @Serializer\Groups({"Default", "Social"})
     */
    private $socialTags;

    /**
     * @var Collection|Post[]
     *
     * @ORM\OneToMany(targetEntity="Todotoday\CMSBundle\Entity\Post", mappedBy="plugin")
     */
    private $posts;

    /**
     * @var bool
     *
     * @ORM\Column(name="enabled", type="boolean")
     */
    private $enabled;

    /**
     * Plugin constructor.
     *
     * @param string $slug
     */
    public function __construct(string $slug)
    {
        $this->slug = $slug;
        $this->linkAgencies = new ArrayCollection();
        $this->socialTags = new ArrayCollection();
        $this->posts = new ArrayCollection();
        $this->enabled = true;
    }

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return explode('.', $this->slug)[0];
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return explode('.', $this->slug)[1];
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @param null|string $slug
     *
     * @return Plugin
     */
    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Add linkAgency
     *
     * @param LinkAgencyPlugin $linkAgency
     *
     * @return Plugin
     */
    public function addLinkAgency(LinkAgencyPlugin $linkAgency): self
    {
        $this->linkAgencies[] = $linkAgency;
        $linkAgency->setPlugin($this);

        return $this;
    }

    /**
     * Get linkAgencies
     *
     * @return Collection|LinkAgencyPlugin[]
     */
    public function getLinkAgencies(): Collection
    {
        return $this->linkAgencies;
    }

    /**
     * @return Collection
     */
    public function getSocialTags(): Collection
    {
        return $this->socialTags;
    }

    /**
     * Remove linkAgency
     *
     * @param LinkAgencyPlugin $linkAgency
     *
     * @return Plugin
     */
    public function removeLinkAgency(LinkAgencyPlugin $linkAgency): self
    {
        $this->linkAgencies->removeElement($linkAgency);
        $linkAgency->setPlugin(null);

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnabled(): ?bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     *
     * @return Plugin
     */
    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->getSlug();
    }

    /**
     * @param Collection|Tag[] $socialTags
     *
     * @return Plugin
     */
    public function setSocialTags($socialTags): self
    {
        $this->socialTags = $socialTags;

        return $this;
    }

    /**
     * @param Tag $tag
     *
     * @return Plugin
     */
    public function addSocialTag(Tag $tag): self
    {
        $this->socialTags[] = $tag;
        $tag->addPlugin($this);

        return $this;
    }

    /**
     * @param Tag $tag
     *
     * @return Plugin
     */
    public function removeSocialTag(Tag $tag): self
    {
        $this->socialTags->removeElement($tag);
        $tag->removePlugin($this);

        return $this;
    }

    /**
     * @return Collection|Post[]
     */
    public function getPosts(): ?Collection
    {
        return $this->posts;
    }

    /**
     * @param Collection|Post[] $posts
     *
     * @return Plugin
     */
    public function setPosts($posts): self
    {
        $this->posts = $posts;

        return $this;
    }

    /**
     * @param Post $post
     *
     * @return Plugin
     */
    public function addPost(Post $post): self
    {
        $this->posts[] = $post;
        $post->setPlugin($this);

        return $this;
    }

    /**
     * @param Post $post
     *
     * @return Plugin
     */
    public function removePost(Post $post): self
    {
        $this->posts->removeElement($post);

        return $this;
    }
}
