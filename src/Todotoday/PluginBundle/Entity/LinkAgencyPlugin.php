<?php declare(strict_types=1);

namespace Todotoday\PluginBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class LinkAgencyPlugin
 *
 * @ORM\Table(name="link_agency_plugin", schema="plugin", uniqueConstraints={
 * @ORM\UniqueConstraint(name="link_index", columns={"agency_id", "plugin_id"})})
 * @ORM\Entity(repositoryClass="Todotoday\PluginBundle\Repository\LinkAgencyPluginRepository")
 */
class LinkAgencyPlugin
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="parameters", type="text")
     */
    private $parameters;

    /**
     * @var Agency
     *
     * @ORM\ManyToOne(targetEntity="Todotoday\CoreBundle\Entity\Agency", inversedBy="linkPlugins", cascade={"PERSIST"})
     */
    private $agency;

    /**
     * @var Plugin
     *
     * @ORM\ManyToOne(targetEntity="Todotoday\PluginBundle\Entity\Plugin", inversedBy="linkAgencies")
     */
    private $plugin;

    /**
     * @var bool
     *
     * @ORM\Column(name="enabled", type="boolean")
     */
    private $enabled;

    /**
     * LinkAgencyPlugin constructor.
     *
     */
    public function __construct()
    {
        $this->enabled = true;
        $this->parameters = '';
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set parameters
     *
     * @param string|array|null $parameters
     *
     * @return LinkAgencyPlugin
     */
    public function setParameters($parameters): self
    {
        if (!$parameters) {
            $this->parameters = '';

            return $this;
        }

        if (is_string($parameters)) {
            $this->parameters = $parameters;

            return $this;
        }

        $this->parameters = json_encode($parameters);

        return $this;
    }

    /**
     * Get parameters
     *
     * @return array
     */
    public function getParameters(): ?array
    {
        if (!$this->parameters) {
            return null;
        }

        return json_decode($this->parameters, true);
    }

    /**
     * @return Agency|null
     */
    public function getAgency(): ?Agency
    {
        return $this->agency;
    }

    /**
     * @param Agency $agency
     *
     * @return LinkAgencyPlugin
     */
    public function setAgency(Agency $agency): self
    {
        $this->agency = $agency;

        return $this;
    }

    /**
     * @return Plugin
     */
    public function getPlugin(): ?Plugin
    {
        return $this->plugin;
    }

    /**
     * @param Plugin|null $plugin
     *
     * @return LinkAgencyPlugin
     */
    public function setPlugin(?Plugin $plugin): self
    {
        $this->plugin = $plugin;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnabled(): ?bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     *
     * @return LinkAgencyPlugin
     */
    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->getAgency();
    }
}
