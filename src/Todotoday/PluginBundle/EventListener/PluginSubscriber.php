<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 19/10/17
 * Time: 13:19
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\PluginBundle\EventListener
 *
 * @subpackage Todotoday\PluginBundle\EventListener
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\PluginBundle\EventListener;

use Monolog\Logger;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Todotoday\PluginBundle\Entity\Plugin;
use Todotoday\PluginBundle\Enum\PluginCategoryEnum;
use Todotoday\PluginBundle\Event\PluginCronEvent;
use Todotoday\PluginBundle\Event\PostPluginEvent;
use Todotoday\PluginBundle\Event\LinkAgencyPluginEvent;
use Todotoday\PluginBundle\Event\SocialPluginEvent;
use Todotoday\PluginBundle\Services\PluginManager;

/**
 * Class PluginSubscriber
 */
class PluginSubscriber implements EventSubscriberInterface
{
    /**
     * @var PluginManager
     */
    private $pluginManager;

    /**
     * @var array
     */
    private $pluginsUsingCron;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * PluginSubscriber constructor.
     *
     * @param PluginManager $pluginManager
     * @param Logger        $logger
     *
     * @throws \Exception
     */
    public function __construct(PluginManager $pluginManager, Logger $logger)
    {
        $this->pluginManager = $pluginManager;
        $this->logger = $logger;
        $this->pluginsUsingCron = $pluginManager->getPluginCategoriesUsingCron();
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents(): array
    {
        return [
            LinkAgencyPluginEvent::ADD => 'addNewLinkAgencyPlugin',
            PostPluginEvent::ADD => 'addNewPostFromPlugin',
            PostPluginEvent::MISSING_MANDATORY_FIELD => 'addNewPostMissedFromPlugin',
            PostPluginEvent::ERROR => 'addNewPostErrorFromPlugin',
            SocialPluginEvent::ADD_SOCIAL_CONTENT => 'addNewSocialPluginContent',
            SocialPluginEvent::MISSING_MANDATORY_FIELD => 'addNewSocialPluginContentMissed',
            SocialPluginEvent::ADD_BOT_COMMENT => 'addNewBotComment',
            SocialPluginEvent::ERROR => 'addSocialError',
            PluginCronEvent::SUCCESS => 'pluginCronSuccess',
            PluginCronEvent::FAILED => 'pluginCronFailed',
            PluginCronEvent::FINISH => 'executeCronSummary',
        ];
    }

    /**
     * @param LinkAgencyPluginEvent $event
     *
     * @throws \Exception
     */
    public function addNewLinkAgencyPlugin(LinkAgencyPluginEvent $event)
    {
        $linkAgencyPlugin = $event->getLinkAgencyPlugin();
        $pluginService = $this->pluginManager->getServicePluginForCategoryAndSlug(
            $linkAgencyPlugin->getPlugin()->getCategory(),
            $linkAgencyPlugin->getPlugin()->getSlug()
        );

        $linkAgencyPlugin->setParameters($pluginService->getParameters());
    }

    /**
     * @param PluginCronEvent $pluginCronEvent
     * @SuppressWarnings(PHPMD)
     */
    public function executeCronSummary(PluginCronEvent $pluginCronEvent): void
    {
        /** @var array $plugins */
        foreach ($this->pluginsUsingCron as $category => $plugins) {
            foreach ($plugins as $pluginName => $plugin) {
                $pluginMessage = 'Plugin "' . $category . '.' . $pluginName . '"';
                $pluginHasOneContentFailed = false;

                foreach ($this->getContentNames($category) as $contentKey => $contentName) {
                    $states = ['info' => 'Added', 'error' => 'Failed', 'warning' => 'Missed'];

                    foreach ($states as $alert => $contentState) {
                        $contentQuantity = count($plugin[$contentKey . $contentState] ?? array());

                        if ($contentQuantity > 0) {
                            if (in_array($alert, ['error', 'critical'], true)) {
                                $pluginHasOneContentFailed = true;
                            }
                            $message = $pluginMessage . ' has ' . strtolower($contentState);
                            $message .= ' ' . $contentQuantity . ' ' . $contentName;
                            $this->logger->{'add' . ucfirst($alert)}($message);
                        }
                    }
                }

                if (!$pluginHasOneContentFailed && ($plugin['success'] ?? false)) {
                    $this->logger->addInfo($pluginMessage . ' succeed.');
                } elseif ($plugin['exception'] ?? false) {
                    $this->logger->addError($pluginMessage . ' failed with exception :' . $plugin['exception']);
                }
            }
        }
    }

    /**
     * @param PluginCronEvent $pluginCronEvent
     */
    public function pluginCronSuccess(PluginCronEvent $pluginCronEvent): void
    {
        $this->pluginsUsingCron[$pluginCronEvent->getCategory()][$pluginCronEvent->getPlugin()->getName()]['success'] =
            true;
    }

    /**
     * @param PluginCronEvent $pluginCronEvent
     */
    public function pluginCronFailed(PluginCronEvent $pluginCronEvent): void
    {
        $this->pluginsUsingCron[$pluginCronEvent->getCategory()][$pluginCronEvent->getPlugin()->getName(
        )]['exception'] =
            $pluginCronEvent->getException()->getMessage();
    }

    /**
     * @param PostPluginEvent $pluginEvent
     */
    public function addNewPostFromPlugin(PostPluginEvent $pluginEvent): void
    {
        $this->pluginsUsingCron[PluginCategoryEnum::POST_PLUGIN][$pluginEvent->getPlugin()->getName()]['postsAdded'][] =
            $pluginEvent->getPost();
    }

    /**
     * @param PostPluginEvent $pluginEvent
     */
    public function addNewPostMissedFromPlugin(PostPluginEvent $pluginEvent): void
    {
        $plugin = $pluginEvent->getPlugin();
        $post = $pluginEvent->getPost();
        $this->pluginsUsingCron[PluginCategoryEnum::POST_PLUGIN][$plugin->getName()]['postsMissed'][] = $post;
        $this->logger->addDebug($pluginEvent->getException()->getMessage());
    }

    /**
     * @param PostPluginEvent $pluginEvent
     */
    public function addNewPostErrorFromPlugin(PostPluginEvent $pluginEvent): void
    {
        $this->pluginsUsingCron[PluginCategoryEnum::POST_PLUGIN][$pluginEvent->getPlugin()->getName()]['postsFailed'][]
            = array(
            'post' => $pluginEvent->getPost(),
            'exception' => $pluginEvent->getException()->getMessage(),
        );

        $message = '"' . PluginCategoryEnum::POST_PLUGIN . '.' . $pluginEvent->getPlugin()->getName() . '"';
        $message .= 'failed with error ' . $pluginEvent->getException()->getMessage();
        $this->logger->addDebug($message);
    }

    /**
     * @param SocialPluginEvent $pluginEvent
     */
    public function addNewSocialPluginContent(SocialPluginEvent $pluginEvent): void
    {
        $this->pluginsUsingCron[PluginCategoryEnum::SOCIAL_PLUGIN][$pluginEvent->getPlugin()->getName(
        )]['socialPluginContentsAdded'][] = $pluginEvent->getSocialPluginContent();
    }

    /**
     * @param SocialPluginEvent $pluginEvent
     */
    public function addNewSocialPluginContentMissed(SocialPluginEvent $pluginEvent): void
    {
        $plugin = $pluginEvent->getPlugin();
        $socialPluginContent = $pluginEvent->getSocialPluginContent();
        $this->pluginsUsingCron[PluginCategoryEnum::SOCIAL_PLUGIN][$plugin->getName()]['socialPluginContentsMissed'][] =
            $socialPluginContent;
        $this->logger->addDebug($pluginEvent->getException()->getMessage());
    }

    /**
     * @param SocialPluginEvent $pluginEvent
     */
    public function addNewBotComment(SocialPluginEvent $pluginEvent): void
    {
        $this->pluginsUsingCron[PluginCategoryEnum::SOCIAL_PLUGIN][$pluginEvent->getPlugin()->getName(
        )]['botCommentsAdded'][] = $pluginEvent->getBotComment();
    }

    /**
     * @param SocialPluginEvent $pluginEvent
     */
    public function addSocialError(SocialPluginEvent $pluginEvent): void
    {
        $socialContentName = $pluginEvent->getSocialPluginContent() ? 'socialPluginContents' : 'botComments';
        $this->pluginsUsingCron[PluginCategoryEnum::SOCIAL_PLUGIN][$pluginEvent->getPlugin()->getName(
        )][$socialContentName . 'Failed'][] = array(
            'content' => $pluginEvent->getSocialPluginContent() ?? $pluginEvent->getBotComment(),
            'exception' => $pluginEvent->getException(),
        );
        $message = '"' . PluginCategoryEnum::SOCIAL_PLUGIN . '.' . $pluginEvent->getPlugin()->getName() . '"';
        $message .= 'failed with error ' . $socialContentName . ' : ' . $pluginEvent->getException()->getMessage();
        $this->logger->addDebug($message);
    }

    /**
     * @param string $category
     *
     * @return array
     */
    private function getContentNames(string $category): array
    {
        if ($category === PluginCategoryEnum::POST_PLUGIN) {
            return array('posts' => 'post(s)');
        }
        if ($category === PluginCategoryEnum::SOCIAL_PLUGIN) {
            return array(
                'socialPluginContents' => 'social plugin content(s)',
                'botComments' => 'comment(s)',
            );
        }

        return array();
    }
}
