<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 19/10/17
 * Time: 13:00
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\PluginBundle\EventListener
 *
 * @subpackage Todotoday\PluginBundle\EventListener
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\PluginBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Todotoday\PluginBundle\Entity\LinkAgencyPlugin;
use Todotoday\PluginBundle\Event\LinkAgencyPluginEvent;

/**
 * Class LinkAgencyPluginSubscriber
 */
class LinkAgencyPluginDoctrineSubscriber implements EventSubscriber
{
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * LinkAgencyPluginDoctrineSubscriber constructor.
     *
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return array
     */
    public function getSubscribedEvents(): array
    {
        return [
            'prePersist',
        ];
    }

    /**
     * @param LifecycleEventArgs $args
     *
     * @throws \Exception
     */
    public function prePersist(LifecycleEventArgs $args): void
    {
        if (!$args->getObject() instanceof LinkAgencyPlugin) {
            return;
        }
        /** @var LinkAgencyPlugin $linkAgencyPlugin */
        $linkAgencyPlugin = $args->getEntity();
        $event = new LinkAgencyPluginEvent($linkAgencyPlugin);
        $this->dispatcher->dispatch($event::ADD, $event);
    }
}
