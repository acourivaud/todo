<?php declare(strict_types=1);

namespace Todotoday\PluginBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class TodotodayPluginExtension extends Extension
{
    /**
     * @var ContainerBuilder
     */
    private $container;

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $this->container = $container;

        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('mappers.yml');
        $loader->load('services.yml');
        $loader->load('connections.yml');
        $loader->load('admin.yml');
        $loader->load('plugins_catalog.yml');
        $loader->load('plugins_post.yml');
        $loader->load('plugins_social.yml');

        $this->processConnections($config['connections']);
    }

    /**
     * @param array $connections
     */
    private function processConnections(array $connections)
    {
        $definitions = [];
        foreach ($connections as $key => $connection) {
            $definition = (new Definition(
                $connection['class'],
                [$key, $connection]
            ))
                ->setFactory(
                    [new Reference('todotoday.plugin.connection.partner_manager'), 'constructPartnerConnection']
                );
            $definitions['todotoday.plugin.connection.' . $key] = $definition;
        }

        $this->container->addDefinitions($definitions);
    }
}
