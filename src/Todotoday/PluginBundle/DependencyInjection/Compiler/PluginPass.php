<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 11/04/2017
 * Time: 17:39
 */

namespace Todotoday\PluginBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Todotoday\PluginBundle\Enum\PluginCategoryEnum;

/**
 * Class PaymentMethodPass
 *
 * @category   Todo-Todev
 * @package    Actiane\PaymentBundle
 * @subpackage Actiane\PaymentBundle\DependencyInjection\Compiler
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class PluginPass implements CompilerPassInterface
{
    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     *
     * @throws \Exception
     */
    public function process(ContainerBuilder $container): void
    {
        /** @var array[] $paymentMethodsDefinitions */
        $pluginsTagged = $container->findTaggedServiceIds('todotoday.plugin');
        $pluginManagerDefinition = $container->getDefinition('todotoday.plugin.plugin_manager');

        foreach ($pluginsTagged as $id => $tags) {
            $category = $tags[0]['category'];

            $pluginManagerDefinition->addMethodCall(
                'addPlugin',
                array(
                    new Reference($id),
                    $category,
                )
            );
        }
    }
}
