<?php declare(strict_types = 1);

namespace Todotoday\PluginBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Todotoday\PluginBundle\DependencyInjection\Compiler\PluginPass;

class TodotodayPluginBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new PluginPass());
    }
}
