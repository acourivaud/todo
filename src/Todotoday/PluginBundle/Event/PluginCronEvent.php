<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 19/10/17
 * Time: 13:13
 *
 * @category   Todo-Todev
 * @package    Todotoday\PluginBundle\Event
 * @subpackage Todotoday\PluginBundle\Event
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\PluginBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Todotoday\PluginBundle\Entity\Plugin;

/**
 * Class PluginCronEvent
 */
class PluginCronEvent extends Event
{
    public const SUCCESS = 'plugin.cron.success';
    public const FAILED = 'plugin.cron.failed';
    public const FINISH = 'plugin.cron.finish';

    /**
     * @var Plugin
     */
    private $plugin;

    /**
     * @var string
     */
    private $category;

    /**
     * @var \Exception
     */
    private $exception;

    /**
     * PluginCronEvent constructor.
     *
     * @param Plugin          $plugin
     * @param string          $category
     * @param null|\Exception $exception
     */
    public function __construct(Plugin $plugin, string $category, ?\Exception $exception = null)
    {
        $this->plugin = $plugin;
        $this->category = $category;
        $this->exception = $exception;
    }

    /**
     * @return Plugin
     */
    public function getPlugin(): Plugin
    {
        return $this->plugin;
    }

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return $this->category;
    }

    /**
     * @return \Exception
     */
    public function getException(): ?\Exception
    {
        return $this->exception;
    }
}
