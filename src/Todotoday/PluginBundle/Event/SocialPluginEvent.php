<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 19/10/17
 * Time: 13:13
 *
 * @category   Todo-Todev
 * @package    Todotoday\PluginBundle\Event
 * @subpackage Todotoday\PluginBundle\Event
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\PluginBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Todotoday\PluginBundle\Entity\Plugin;
use Todotoday\SocialBundle\Entity\BotComment;
use Todotoday\SocialBundle\Entity\SocialPluginContent;

/**
 * Class SocialPluginEvent
 */
class SocialPluginEvent extends Event
{
    public const ADD_SOCIAL_CONTENT = 'social.plugin.add_social_content';
    public const ADD_BOT_COMMENT = 'social.plugin.add_bot_comment';
    public const MISSING_MANDATORY_FIELD = 'social.plugin.missing_mandatory_field';
    public const ERROR = 'social.plugin.error';

    /**
     * @var Plugin
     */
    private $plugin;

    /**
     * @var SocialPluginContent
     */
    private $socialPluginContent;

    /**
     * @var array
     */
    private $botComment;

    /**
     * @var \Exception
     */
    private $exception;

    /**
     * SocialPluginEvent constructor.
     *
     * @param Plugin                   $plugin
     * @param null|SocialPluginContent $socialPluginContent
     * @param null|BotComment|null     $botComment
     * @param null|\Exception          $exception
     */
    public function __construct(
        Plugin $plugin,
        ?SocialPluginContent $socialPluginContent,
        ?BotComment $botComment = null,
        ?\Exception $exception = null
    ) {
        $this->plugin = $plugin;
        $this->socialPluginContent = $socialPluginContent;
        $this->botComment = $botComment;
        $this->exception = $exception;
    }

    /**
     * @return Plugin
     */
    public function getPlugin(): Plugin
    {
        return $this->plugin;
    }

    /**
     * @return SocialPluginContent
     */
    public function getSocialPluginContent(): ?SocialPluginContent
    {
        return $this->socialPluginContent;
    }

    /**
     * @return null|BotComment
     */
    public function getBotComment(): ?BotComment
    {
        return $this->botComment;
    }

    /**
     * @return \Exception
     */
    public function getException(): ?\Exception
    {
        return $this->exception;
    }
}
