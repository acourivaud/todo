<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 19/10/17
 * Time: 13:13
 *
 * @category   Todo-Todev
 * @package    Todotoday\PluginBundle\Event
 * @subpackage Todotoday\PluginBundle\Event
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\PluginBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Todotoday\CMSBundle\Entity\Post;
use Todotoday\PluginBundle\Entity\Plugin;

/**
 * Class PostPluginEvent
 */
class PostPluginEvent extends Event
{
    public const ADD = 'post.plugin.add';
    public const MISSING_MANDATORY_FIELD = 'post.plugin.missing_mandatory_field';
    public const ERROR = 'post.plugin.error';

    /**
     * @var Plugin
     */
    private $plugin;

    /**
     * @var array
     */
    private $post;

    /**
     * @var \Exception
     */
    private $exception;

    /**
     * PostPluginEvent constructor.
     *
     * @param Plugin          $plugin
     * @param Post            $post
     * @param null|\Exception $exception
     */
    public function __construct(Plugin $plugin, Post $post, ?\Exception $exception = null)
    {
        $this->plugin = $plugin;
        $this->post = $post;
        $this->exception = $exception;
    }

    /**
     * @return Plugin
     */
    public function getPlugin(): Plugin
    {
        return $this->plugin;
    }

    /**
     * @return Post
     */
    public function getPost(): Post
    {
        return $this->post;
    }

    /**
     * @return \Exception
     */
    public function getException(): ?\Exception
    {
        return $this->exception;
    }
}
