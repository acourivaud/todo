<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 19/10/17
 * Time: 13:13
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\PluginBundle\Event
 *
 * @subpackage Todotoday\PluginBundle\Event
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\PluginBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Todotoday\PluginBundle\Entity\LinkAgencyPlugin;

/**
 * Class LinkAgencyPluginEvent
 */
class LinkAgencyPluginEvent extends Event
{
    public const ADD = 'link_agency_plugin.add';

    /**
     * @var LinkAgencyPlugin
     */
    private $linkAgencyPlugin;

    /**
     * LinkAgencyPluginEvent constructor.
     *
     * @param LinkAgencyPlugin $linkAgencyPlugin
     */
    public function __construct(LinkAgencyPlugin $linkAgencyPlugin)
    {
        $this->linkAgencyPlugin = $linkAgencyPlugin;
    }

    /**
     * @return LinkAgencyPlugin
     */
    public function getLinkAgencyPlugin(): LinkAgencyPlugin
    {
        return $this->linkAgencyPlugin;
    }
}
