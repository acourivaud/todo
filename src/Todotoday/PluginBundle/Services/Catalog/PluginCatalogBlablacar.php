<?php declare(strict_types=1);

namespace Todotoday\PluginBundle\Services\Catalog;

use Todotoday\CatalogBundle\Entity\Api\Microsoft\RetailCatalog;
use Todotoday\PluginBundle\Entity\Plugin;
use Todotoday\PluginBundle\Services\PluginCatalog;

/**
 * Class PluginCatalog
 *
 * @category   Todo-Todev
 * @package    Todotoday\PluginBundle\Services\Catalog
 * @subpackage Todotoday\PluginBundle\DependencyInjection\Compiler
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class PluginCatalogBlablacar extends PluginCatalog
{
    /**
     * @return string
     */
    public function getPluginCatalogName(): string
    {
        return 'blablacar';
    }

    /**
     * Do getFrenchLokalizeKey
     *
     * @return string
     */
    public function getPluginNameKey(): string
    {
        return 'plugin.catalog.blablacar.plugin_name';
    }

    /**
     * @return array
     */
    public function getParameters(): array
    {
        return array(
            'catalogFamilies' => '5637144578',
            'itemPositions' => '',
        );
    }

    /**
     * @return string
     */
    protected function getItemNameKey(): string
    {
        return 'plugin.catalog.blablacar.item_name';
    }

    /**
     * @param RetailCatalog $retailCatalog
     *
     */
    protected function modifyRetailCatalog(RetailCatalog $retailCatalog): void
    {
        // TODO: Implement modifyRetailCatalog() method.
    }

    /**
     * @return array
     */
    protected function getViewWithParameters(): array
    {
        return array(
            'view' => 'blablacar.html.twig',
            'parameters' => array(),
        );
    }

    /**
     * The name of the mdi icon that will show in the search dropdown of the catalog
     * If there is no picto_url for the plugin, the mdi icon will be use as picto for the plugin family
     *
     * @return string
     */
    protected function getMdiIcon(): string
    {
        return 'car-connected';
    }
}
