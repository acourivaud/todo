<?php declare(strict_types=1);

namespace Todotoday\PluginBundle\Services;

use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\PluginBundle\Entity\LinkAgencyPlugin;
use Todotoday\PluginBundle\Entity\Plugin;
use Todotoday\PluginBundle\Enum\PluginCategoryEnum;
use Todotoday\PluginBundle\Event\PluginCronEvent;
use Todotoday\PluginBundle\Exceptions\PluginIncorrectException;
use Todotoday\PluginBundle\Exceptions\PluginNotDefinedException;
use Todotoday\PluginBundle\Exceptions\PluginNotFoundException;
use Todotoday\PluginBundle\Interfaces\PluginInterface;
use Todotoday\PluginBundle\Repository\PluginRepository;

/**
 * Class PluginManager
 *
 * @SuppressWarnings(PHPMD.UnusedPrivateMethod)
 * @package    Todotoday\PluginBundle\Services\Catalog
 * @subpackage Todotoday\PluginBundle\DependencyInjection\Compiler
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class PluginManager
{
    /**
     * PARAMETERS_DELIMITER
     */
    public const PARAMETERS_DELIMITER = ';;';

    /**
     * @var PluginInterface[]
     */
    private $plugins;

    /**
     * @var array
     */
    private $pluginCategoriesAvailable;

    /**
     * @var array
     */
    private $pluginCategoriesUsingCron;

    /**
     * @var PluginRepository
     */
    private $linkAgencyPluginRepository;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * PluginManager constructor.
     *
     * @param EntityManager            $entityManager
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EntityManager $entityManager, EventDispatcherInterface $dispatcher)
    {
        $this->plugins = array();
        $this->pluginCategoriesAvailable = PluginCategoryEnum::getValues();
        foreach ($this->pluginCategoriesAvailable as $category) {
            $this->plugins[$category] = array();
        }
        $this->pluginCategoriesUsingCron = array(
            PluginCategoryEnum::POST_PLUGIN,
            PluginCategoryEnum::SOCIAL_PLUGIN,
        );
        $this->linkAgencyPluginRepository =
            $entityManager->getRepository('TodotodayPluginBundle:LinkAgencyPlugin');
        $this->entityManager = $entityManager;
        $this->dispatcher = $dispatcher;
    }

    /**
     * Do addPlugin
     *
     * @param PluginInterface $plugin
     * @param string          $category
     *
     * @return PluginManager
     * @throws \Exception
     *
     */
    public function addPlugin(PluginInterface $plugin, string $category): self
    {
        $this->isCategoryValid($category);
        $this->addPluginToCategory($plugin, $category);

        return $this;
    }

    /**
     * @param Agency $agency
     * @param string $pluginCategory
     * @param string $keyToReturn
     * @param array  $values
     *
     * @return array|null
     * @throws \Exception
     */
    public function launchPlugins(
        Agency $agency,
        string $pluginCategory,
        string $keyToReturn = '',
        array $values = array()
    ): ?array {
        $servicePlugins = $this->getAllServicePluginsForAgencyAndCategory($agency, $pluginCategory);
        $methodName = 'launch' . ucfirst($pluginCategory) . 'Plugins';

        if (method_exists($this, $methodName)) {
            return ($keyToReturn === '')
                ? $this->$methodName($servicePlugins, $values)
                : $this->$methodName($servicePlugins, $values)[$keyToReturn];
        }

        return null;
    }

    /**
     * @param array $values
     *
     * @return bool
     * @throws \Exception
     */
    public function launchCronPlugins(array $values = array()): bool
    {
        $isLaunchSucceed = true;
        /** @var PluginRepository $pluginRepository */
        $pluginRepository = $this->entityManager->getRepository('TodotodayPluginBundle:Plugin');

        foreach ($this->pluginCategoriesUsingCron as $pluginCategory) {
            $plugins = $this->getAllServicePluginsForCategory($pluginCategory);

            /** @var PluginInterface $pluginService */
            foreach ($plugins as $pluginService) {
                /** @var Plugin $plugin */
                $plugin = $pluginRepository->findOneBy(
                    array('slug' => $pluginService->getSlug(), 'enabled' => true)
                );

                if (!$plugin) {
                    continue;
                }

                try {
                    $pluginService->executeCron($plugin);
                    $event = new PluginCronEvent($plugin, $pluginCategory);
                    $this->dispatcher->dispatch(PluginCronEvent::SUCCESS, $event);
                } catch (\Exception $exception) {
                    $event = new PluginCronEvent($plugin, $pluginCategory, $exception);
                    $this->dispatcher->dispatch(PluginCronEvent::FAILED, $event);
                }
            }
        }

        $finishEvent = new PluginCronEvent(new Plugin(''), '');
        $this->dispatcher->dispatch(PluginCronEvent::FINISH, $finishEvent);

        return $isLaunchSucceed;
    }

    /**
     * @param Agency $agency
     * @param string $pluginCategory
     * @param string $pluginSlug
     * @param array  $values
     *
     * @return array|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Todotoday\PluginBundle\Exceptions\PluginNotDefinedException
     * @throws \Todotoday\PluginBundle\Exceptions\PluginNotFoundException
     * @throws \BadMethodCallException
     */
    public function getPluginView(
        Agency $agency,
        string $pluginCategory,
        string $pluginSlug,
        array $values = array()
    ): ?array {
        $servicePlugin = $this->getServicePluginForAgencyAndSlug($agency, $pluginCategory, $pluginSlug);

        if (!method_exists($servicePlugin, 'getView')) {
            throw new \BadMethodCallException(
                'The method "getView" does not exist for the service ' . get_class($servicePlugin) . '.'
            );
        }

        return $servicePlugin->getView();
    }

    /**
     * @param string $pluginCategory
     * @param string $pluginSlug
     *
     * @return null|string
     * @throws \Todotoday\PluginBundle\Exceptions\PluginNotDefinedException
     * @throws \Todotoday\PluginBundle\Exceptions\PluginIncorrectException
     * @throws \Exception
     * @throws \BadMethodCallException
     */
    public function getPluginImage(string $pluginCategory, string $pluginSlug): ?string
    {
        $servicePlugin = $this->getServicePluginForCategoryAndSlug($pluginCategory, $pluginSlug);

        if (!method_exists($servicePlugin, 'getImage')) {
            throw new \BadMethodCallException(
                'The method "getImage" does not exist for the service ' . get_class($servicePlugin) . '.'
            );
        }

        return $servicePlugin->getImage();
    }

    /**
     * @param string     $category
     * @param string     $pluginSlug
     * @param array|null $parameters
     *
     * @return PluginInterface
     * @throws \Todotoday\PluginBundle\Exceptions\PluginNotDefinedException
     * @throws \Todotoday\PluginBundle\Exceptions\PluginIncorrectException
     */
    public function getServicePluginForCategoryAndSlug(
        string $category,
        string $pluginSlug,
        ?array $parameters = array()
    ): PluginInterface {
        $this->isSlugValid($category, $pluginSlug);
        /** @var PluginInterface $pluginService */
        $pluginService = $this->plugins[$category][$pluginSlug];
        $pluginService->setParameters($parameters);

        $abstractClass = __NAMESPACE__ . '\\Plugin' . ucfirst($category);
        if (!is_subclass_of($pluginService, $abstractClass)) {
            throw new PluginIncorrectException($pluginSlug, $category, $abstractClass);
        }

        return $pluginService;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getPluginCategoriesUsingCron(): array
    {
        $categoriesUsingCron = array();

        foreach ($this->pluginCategoriesUsingCron as $category) {
            $categoriesUsingCron[$category] = array();
        }

        return $categoriesUsingCron;
    }

    /**
     * @param array $catalogPlugins
     * @param array $values
     *
     * @return array
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function launchCatalogPlugins(array $catalogPlugins, array $values): array
    {
        $retailsCatalog = array();

        /** @var PluginCatalog $plugin */
        foreach ($catalogPlugins as $plugin) {
            foreach ($plugin->getRetailsCatalog() as $retailCatalog) {
                $retailsCatalog[] = $retailCatalog;
            }
        }

        return array(
            'retailCatalog' => $retailsCatalog,
        );
    }

    /**
     * @param Agency $agency
     * @param string $category
     * @param string $slug
     *
     * @return PluginInterface
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Todotoday\PluginBundle\Exceptions\PluginNotDefinedException
     * @throws \Exception
     * @throws PluginNotFoundException
     */
    private function getServicePluginForAgencyAndSlug(Agency $agency, string $category, string $slug): PluginInterface
    {
        $this->isSlugValid($category, $slug);
        $linkAgencyPlugin =
            $this->linkAgencyPluginRepository->getLinkAgencyPluginByAgencyAndSlug(
                $agency,
                $category,
                $slug
            );

        if (!$linkAgencyPlugin) {
            throw new PluginNotFoundException($slug, $agency);
        }

        return $this->getServicePluginForCategoryAndLink($category, $linkAgencyPlugin);
    }

    /**
     * @param Agency $agency
     * @param string $pluginCategory
     *
     * @return array
     * @throws \Exception
     */
    private function getAllServicePluginsForAgencyAndCategory(Agency $agency, string $pluginCategory): array
    {
        $this->isCategoryValid($pluginCategory);
        $servicePlugins = array();
        $linkAgencyPlugins =
            $this->linkAgencyPluginRepository->getLinkAgencyPluginsByAgencyAndCategory($agency, $pluginCategory);

        /** @var LinkAgencyPlugin $linkAgencyPlugin */
        foreach ($linkAgencyPlugins as $linkAgencyPlugin) {
            $servicePlugins[] = $this->getServicePluginForCategoryAndLink($pluginCategory, $linkAgencyPlugin);
        }

        return $servicePlugins;
    }

    /**
     * @param string $pluginCategory
     *
     * @return array
     * @throws \Exception
     */
    private function getAllServicePluginsForCategory(string $pluginCategory): array
    {
        $this->isCategoryValid($pluginCategory);

        return $this->plugins[$pluginCategory];
    }

    /**
     * @param string           $category
     * @param LinkAgencyPlugin $linkAgencyPlugin
     *
     * @return PluginInterface
     * @throws \Exception
     */
    private function getServicePluginForCategoryAndLink(
        string $category,
        LinkAgencyPlugin $linkAgencyPlugin
    ): PluginInterface {
        return $this->getServicePluginForCategoryAndSlug(
            $category,
            $linkAgencyPlugin->getPlugin()->getSlug(),
            $linkAgencyPlugin->getParameters()
        );
    }

    /**
     * @param PluginInterface $plugin
     * @param string          $category
     *
     * @throws \Exception
     */
    private function addPluginToCategory(PluginInterface $plugin, string $category): void
    {
        if (isset($this->plugins[$category][$plugin->getSlug()])) {
            throw new \Exception('The plugin "' . $plugin->getSlug() . '" is already defined !"');
        }
        $this->plugins[$category][$plugin->getSlug()] = $plugin;
    }

    /**
     * @param string $category
     * @param string $slug
     *
     * @throws \Todotoday\PluginBundle\Exceptions\PluginNotDefinedException
     */
    private function isSlugValid(string $category, string $slug): void
    {
        $this->isCategoryValid($category);
        if (!isset($this->plugins[$category][$slug])) {
            throw new PluginNotDefinedException($slug, $category);
        }
    }

    /**
     * @param string $category
     *
     * @throws \Exception
     */
    private function isCategoryValid(string $category): void
    {
        if (!in_array($category, $this->pluginCategoriesAvailable, true)) {
            throw new \Exception(
                'The category "' . $category . '" in not present in the PluginCategoryEnum.' .
                'Valid categories are : ' . implode(', ', $this->pluginCategoriesAvailable)
            );
        }
    }
}
