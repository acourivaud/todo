<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 13/10/17
 * Time: 10:31
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\PluginBundle\Services\Connections
 *
 * @subpackage Todotoday\PluginBundle\Services\Connections
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\PluginBundle\Services\Connections;

/**
 * Class CollibrisConnection
 */
use Todotoday\PluginBundle\Entity\Partner\Collibris\Book;

/**
 * Class CollibrisConnection
 * @package Todotoday\PluginBundle\Services\Connections
 */
class CollibrisConnection extends AbstractPartnerConnection
{
    /**
     * @var Book[]
     */
    private $books = [];

    /**
     * Method to implement to send data to plugin manager
     *
     * @param null|string $locale
     *
     * @return array
     * @throws \Todotoday\PluginBundle\Exceptions\PartnerCurlException
     */
    public function sendResultToPlugin(?string $locale = null): array
    {
        return $this->getBooks();
    }

    /**
     * @return Book[]
     * @throws \Todotoday\PluginBundle\Exceptions\PartnerCurlException
     */
    private function getBooks(): array
    {
        $this->books = array();
        $results = json_decode($this->getTopStats(), true);
        foreach ((array) $results as $result) {
            $book = new Book();
            $book->setTitle($result['title'])
                ->setAuthors($result['authors'])
                ->setDescription($result['description'])
                ->setCover($result['cover'])
                ->setCollibrisUrl($result['collibris_url']);
            $this->books[] = $book;
        }

        return $this->books;
    }

    /**
     * @return string
     * @throws \Todotoday\PluginBundle\Exceptions\PartnerCurlException
     */
    private function getTopStats(): string
    {
        return $this->execCurl('statistics/top');
    }
}
