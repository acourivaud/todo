<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 13/10/17
 * Time: 16:33
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\PluginBundle\Services\Connections
 *
 * @subpackage Todotoday\PluginBundle\Services\Connections
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\PluginBundle\Services\Connections;

/**
 * Class GlobetipserConnection
 */
/**
 * Class GlobetipserConnection
 *
 * @package Todotoday\PluginBundle\Services\Connections
 */
use Todotoday\PluginBundle\Entity\Partner\Globetipser\Tip;
use Todotoday\PluginBundle\Entity\Partner\Globetipser\User;

/**
 * Class GlobetipserConnection
 *
 * @package Todotoday\PluginBundle\Services\Connections
 */
class GlobetipserConnection extends AbstractPartnerConnection
{
    /**
     * @var User[]
     */

    private $users = [];

    /**
     * @var Tip[]
     */
    private $tips = [];

    /**
     * Method to implement to send data to plugin manager
     *
     * @param null|string $locale
     *
     * @return array
     * @throws \Todotoday\PluginBundle\Exceptions\PartnerCurlException
     */
    public function sendResultToPlugin(?string $locale = null): array
    {
        $this->tips = [];
        $this->users = [];
        $this->retrievePublicUser();

        return $this->tips;
    }

    /**
     * @return GlobetipserConnection
     * @throws \Todotoday\PluginBundle\Exceptions\PartnerCurlException
     */
    private function retrievePublicUser(): self
    {
        $results = json_decode($this->execCurl('users/field/public/true'), true);
        foreach ((array) $results as $result) {
            // on ne récupère que les tips de l'user Globe Tipser
            if ($result['fcb_id'] !== '121709508602462') {
                continue;
            }
            $this->getTips($result['tips']);
            $user = new User();
            $user->setFcbId($result['fcb_id'])
                ->setName($result['name'])
                ->setAvatar($result['avatar'])
                ->setV($result['__v']);
            $this->bindTipToUser($user, $result['tips']);
            $this->users[] = $user;
        }

        return $this;
    }

    /**
     * @param string $tipId
     *
     * @throws \Todotoday\PluginBundle\Exceptions\PartnerCurlException
     */
    private function retrieveTip(string $tipId)
    {
        $result = json_decode($this->execCurl('tips/' . $tipId), true);

        $cover = empty($result['cover']) ? null : $this->getBaseUrlWithoutApi() . rawurlencode($result['cover']);

        $tip = new Tip();
        $tip->setId(@$result['_id'])
            ->setDisplay(@$result['display'])
            ->setName(@$result['name'])
            ->setCategory(@$result['category'])
            ->setContinent(@$result['continent'])
            ->setCountry(@$result['country'])
            ->setCity(@$result['city'])
            ->setAddress(@$result['address'])
            ->setLat(@$result['lat'])
            ->setLon(@$result['lon'])
            ->setText(@$result['text'])
            ->setCover($cover)
            ->setV(@$result['__v']);

        $this->tips[$tip->getId()] = $tip;
    }

    /**
     * @return null|string
     */
    private function getBaseUrlWithoutApi(): ?string
    {
        return explode('api/', $this->baseUrl)[0];
    }

    /**
     * @param array $tips
     *
     * @return GlobetipserConnection
     * @throws \Todotoday\PluginBundle\Exceptions\PartnerCurlException
     */
    private function getTips(array $tips): self
    {
        foreach ($tips as $tip) {
            $this->retrieveTip($tip);
        }

        return $this;
    }

    /**
     * @param string $tipId
     *
     * @return null|Tip
     */
    private function getTip(string $tipId): ?Tip
    {
        if (array_key_exists($tipId, $this->tips)) {
            return $this->tips[$tipId];
        }

        return null;
    }

    /**
     * @param User  $user
     * @param array $tipIds
     *
     * @return $this
     */
    private function bindTipToUser(User $user, array $tipIds)
    {
        foreach ($tipIds as $tipId) {
            if ($tip = $this->getTip($tipId)) {
                $user->addTip($tip);
                $tip->setAuthor($user);
            }
        }

        return $this;
    }
}
