<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 13/10/17
 * Time: 14:00
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\PluginBundle\Services\Connections
 *
 * @subpackage Todotoday\PluginBundle\Services\Connections
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\PluginBundle\Services\Connections;

use Psr\Log\LoggerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PartnerApiConnection
 */
class PartnerApiConnection
{
    /**
     * @var OptionsResolver
     */
    private $optionResolver;

    /**
     * @var array
     */
    private $partnerConnections;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * PartnerApiConnection constructor.
     *
     * @param LoggerInterface $logger
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->partnerConnections = [];
        $this->optionResolver = new OptionsResolver();
        $this->configureSettings($this->optionResolver);
        $this->logger = $logger;
    }

    /**
     * @param string $key
     * @param array  $conf
     *
     * @return mixed
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     */
    public function constructPartnerConnection(string $key, array $conf)
    {
        $options = $this->optionResolver->resolve($conf);
        $partnerConnectionClass = $options['class'];
        $partnerConnection = new $partnerConnectionClass($conf, $key, $this->logger);
        $this->partnerConnections[$key] = $partnerConnection;

        return $partnerConnection;
    }

    /**
     * @param string $key
     *
     * @return mixed
     */
    public function getConnection(string $key)
    {
        if (!array_key_exists($key, $this->partnerConnections)) {
            $this->logger->error(sprintf('No connection found for key %s', $key));
        }

        return $this->partnerConnections[$key];
    }

    /**
     * @param OptionsResolver $optionsResolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     */
    private function configureSettings(OptionsResolver $optionsResolver): void
    {
        $optionsResolver->setRequired(
            [
                'class',
                'base_url',
            ]
        )
            ->setAllowedTypes('class', 'string')
            ->setAllowedTypes('base_url', 'string');
    }
}
