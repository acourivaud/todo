<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 13/10/17
 * Time: 10:41
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\PluginBundle\Services\Connections
 *
 * @subpackage Todotoday\PluginBundle\Services\Connections
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\PluginBundle\Services\Connections;

use Actiane\ApiConnectorBundle\Traits\LoggedCurlInfoTrait;
use Psr\Log\LoggerInterface;
use Todotoday\PluginBundle\Exceptions\PartnerCurlException;

/**
 * Class AbstractPartnerConnection
 */
abstract class AbstractPartnerConnection
{
    use LoggedCurlInfoTrait;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var string
     */
    protected $baseUrl;

    /**
     * @var string
     */
    private $loggerRef;

    /**
     * @var string
     */
    private $key;

    /**
     * AbstractPartnerConnection constructor.
     *
     * @param array           $conf
     * @param string          $key
     * @param LoggerInterface $logger
     */
    public function __construct(array $conf, string $key, LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->baseUrl = $conf['base_url'];
        $this->key = $key;
    }

    /**
     * Method to implement to send data to plugin manager
     *
     * @param null|string $locale
     *
     * @return array
     */
    abstract public function sendResultToPlugin(?string $locale = null): array;

    /**
     * @param string $uri
     *
     * @return string
     * @throws \Todotoday\PluginBundle\Exceptions\PartnerCurlException
     */
    protected function execCurl(string $uri): string
    {
        $ch = $this->initCurl();
        $url = $this->baseUrl . $uri;

        curl_setopt($ch, CURLOPT_URL, $url);
        $output = curl_exec($ch);
        $info = curl_getinfo($ch);

        if (intdiv($info['http_code'], 100) !== 2) {
            throw new PartnerCurlException($this->key, $url, curl_error($ch));
        }
        $this->loggedCurlInfo($info, $output);
        curl_close($ch);

        return $output;
    }

    /**
     * @return resource
     */
    private function initCurl()
    {
        $this->loggerRef = sprintf('(%s) (%s)', uniqid(), $this->key);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_TIMEOUT, 45);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        return $ch;
    }
}
