<?php declare(strict_types=1);

namespace Todotoday\PluginBundle\Services\Post\Mappers;

use Todotoday\CMSBundle\Entity\Post;
use Todotoday\CMSBundle\Enum\PostStateEnum;
use Todotoday\MediaBundle\Repository\MediaRepository;
use Todotoday\PluginBundle\Entity\Plugin;
use Todotoday\PluginBundle\Services\ImageCurlDownload;

/**
 * Created by PhpStorm.
 * User: riwan
 * Date: 13/10/17
 * Time: 10:25
 */
abstract class AbstractPluginPostMapper
{
    /**
     * @var ImageCurlDownload
     */
    protected $imageCurlDownload;

    /**
     * AbstractPluginPostMapper constructor.
     *
     * @param ImageCurlDownload $imageCurlDownload
     */
    public function __construct(ImageCurlDownload $imageCurlDownload)
    {
        $this->imageCurlDownload = $imageCurlDownload;
    }

    /**
     * The input is an array of array from API
     *
     * @param array|null    $posts
     * @param Plugin        $plugin
     * @param PostStateEnum $postState
     * @param string        $locale
     *
     * @return array
     */
    public function formatArrayFromApi(
        array $posts,
        Plugin $plugin,
        PostStateEnum $postState,
        string $locale = 'fr'
    ): array {
        $postsFormatted = array();
        $postsToFormat = $this->formatArray($posts, $plugin);

        foreach ($postsToFormat as $post) {
            if (!($post instanceof Post)) {
                continue;
            }
            $this->fillEmptyPostValues($post);
            $post->setState($postState);
            $post->setLocale($locale);

            $postsFormatted[] = $post;
        }

        return $postsFormatted;
    }

    /**
     * The input is the API (Collibris, etc...)
     * The output is a array of Post.
     *
     * @param array|null $rawPosts
     * @param Plugin     $plugin
     *
     * @return array
     */
    abstract protected function formatArray(?array $rawPosts, Plugin $plugin): array;

    /**
     * @param null|string $url
     *
     * @return null|string
     */
    protected function getEndOfUrl(?string $url): ?string
    {
        if (!$url) {
            return null;
        }
        $urlExploded = explode('/', $url);

        return end($urlExploded);
    }

    /**
     * @param Post $post
     *
     * @return Post
     */
    private function fillEmptyPostValues(Post $post): Post
    {
        if (!$post->getSubtitle()) {
            $post->setSubtitle('');
        }

        return $post;
    }
}
