<?php declare(strict_types=1);

namespace Todotoday\PluginBundle\Services\Post\Mappers;

use Todotoday\CMSBundle\Entity\Post;
use Todotoday\PluginBundle\Entity\Partner\Collibris\Book;
use Todotoday\PluginBundle\Entity\Plugin;
use Todotoday\PluginBundle\Services\ImageCurlDownload;

/**
 * Created by PhpStorm.
 * User: riwan
 * Date: 13/10/17
 * Time: 10:25
 */
class PluginPostCollibrisMapper extends AbstractPluginPostMapper
{
    /**
     * @param array|null $rawPosts
     * @param Plugin     $plugin
     *
     * @return array
     * @throws \Exception
     */
    public function formatArray(?array $rawPosts, Plugin $plugin): array
    {
        $posts = array();

        /** @var Book $rawPost */
        foreach ($rawPosts as $rawPost) {
            $collibrisUrl = $rawPost->getCollibrisUrl();

            $post = new Post();
            $post->setTitle($rawPost->getTitle())
                ->setContent($rawPost->getDescription())
                ->setPluginPostId($this->getEndOfUrl($collibrisUrl))
                ->setPluginPostUrl($collibrisUrl);

            $imageCoverUrl = $rawPost->getCover();
            $media = $this->imageCurlDownload->getMediaUrl(
                $plugin->getName(),
                $this->getEndOfUrl($imageCoverUrl),
                $imageCoverUrl
            );

            if ($media) {
                $post->setMedia($media);
            }
            $posts[] = $post;
        }

        return $posts;
    }
}
