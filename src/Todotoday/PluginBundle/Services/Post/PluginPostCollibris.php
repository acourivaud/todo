<?php declare(strict_types=1);

namespace Todotoday\PluginBundle\Services\Post;

use Todotoday\PluginBundle\Services\Connections\AbstractPartnerConnection;
use Todotoday\PluginBundle\Services\PluginPost;
use Todotoday\CMSBundle\Enum\PostStateEnum;
use Todotoday\PluginBundle\Services\Post\Mappers\AbstractPluginPostMapper;

/**
 * Created by PhpStorm.
 * User: riwan
 * Date: 12/10/17
 * Time: 17:32
 */

class PluginPostCollibris extends PluginPost
{
    /**
     * Do getFrenchLokalizeKey
     *
     * @return string
     */
    public function getPluginNameKey(): string
    {
        return 'plugin.post.collibris.plugin_name';
    }

    /**
     * The name of the plugin without the "catalog.".
     * Example: 'Blablacar'
     *
     * @return string
     */
    protected function getPluginPostName(): string
    {
        return 'collibris';
    }

    /**
     * @return PostStateEnum
     */
    protected function getPostState(): PostStateEnum
    {
        return PostStateEnum::getNewInstance(PostStateEnum::DRAFT);
    }

    /**
     * @return array
     */
    protected function getLocales(): array
    {
        return array('fr');
    }
}
