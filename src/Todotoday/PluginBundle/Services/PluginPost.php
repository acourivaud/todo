<?php declare(strict_types=1);

namespace Todotoday\PluginBundle\Services;

use Carbon\Carbon;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Model\MutableAclProviderInterface;
use Todotoday\CMSBundle\Entity\LinkPostAgency;
use Todotoday\CMSBundle\Entity\Post;
use Todotoday\CMSBundle\Enum\LinkPostAgencyStateEnum;
use Todotoday\CMSBundle\Enum\PostStateEnum;
use Todotoday\CMSBundle\Repository\PostRepository;
use Todotoday\PluginBundle\Entity\LinkAgencyPlugin;
use Todotoday\PluginBundle\Entity\Plugin;
use Todotoday\PluginBundle\Enum\PluginCategoryEnum;
use Todotoday\PluginBundle\Event\PostPluginEvent;
use Todotoday\PluginBundle\Exceptions\PluginContentMissingMandatoryFieldException;
use Todotoday\PluginBundle\Exceptions\PostPluginMissingMandatoryFieldException;
use Todotoday\PluginBundle\Interfaces\PluginInterface;
use Todotoday\PluginBundle\Services\Connections\AbstractPartnerConnection;
use Todotoday\PluginBundle\Services\Post\Mappers\AbstractPluginPostMapper;

/**
 * Class PluginPost
 * @package Todotoday\PluginBundle\Services
 */
abstract class PluginPost implements PluginInterface
{
    /**
     * CATEGORY
     */
    public const CATEGORY = PluginCategoryEnum::POST_PLUGIN;
    /**
     * FIRST_LANGUAGE
     */
    public const FIRST_LANGUAGE = 0;

    /**
     * @var AbstractPluginPostMapper
     */
    protected $postMapper;

    /**
     * @var AbstractPartnerConnection
     */
    protected $partnerConnection;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var MutableAclProviderInterface
     */
    private $aclProvider;

    /**
     * @var \Doctrine\ORM\EntityRepository|\Todotoday\PluginBundle\Repository\LinkAgencyPluginRepository
     */
    private $linkAgencyPluginRepo;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * PluginPost constructor.
     *
     * @param AbstractPluginPostMapper    $postMapper
     * @param AbstractPartnerConnection   $partnerConnection
     * @param EntityManager               $entityManager
     * @param MutableAclProviderInterface $aclProvider
     * @param EventDispatcherInterface    $dispatcher
     */
    public function __construct(
        AbstractPluginPostMapper $postMapper,
        AbstractPartnerConnection $partnerConnection,
        EntityManager $entityManager,
        MutableAclProviderInterface $aclProvider,
        EventDispatcherInterface $dispatcher
    ) {
        $this->postMapper = $postMapper;
        $this->partnerConnection = $partnerConnection;
        $this->entityManager = $entityManager;
        $this->aclProvider = $aclProvider;
        $this->linkAgencyPluginRepo = $entityManager->getRepository('TodotodayPluginBundle:LinkAgencyPlugin');
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param Plugin $plugin
     *
     * @throws \Symfony\Component\Security\Acl\Exception\InvalidDomainObjectException
     * @throws \Symfony\Component\Security\Acl\Exception\AclAlreadyExistsException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     * @throws \UnexpectedValueException
     */
    public function executeCron(Plugin $plugin): void
    {
        $linkAgencyPlugins = $this->linkAgencyPluginRepo->getLinkAgencyPluginsByCategoryAndSlug(
            $plugin->getCategory(),
            $plugin->getName()
        );

        $postsInserted = $this->insertPostsForPlugin($plugin);
        $this->publishPostsForAgencies($linkAgencyPlugins, $postsInserted);
    }

    /**
     * Do getPlugin
     *
     * @return Plugin
     */
    public function getPlugin(): Plugin
    {
        return new Plugin(
            $this->getSlug()
        );
    }

    /**
     * Do getSlug
     *
     * @return string
     */
    public function getSlug(): string
    {
        return $this->getCategory()->get() . '.' . $this->getPluginPostName();
    }

    /**
     * Do getCategory
     *
     * @return PluginCategoryEnum
     */
    public function getCategory(): PluginCategoryEnum
    {
        return PluginCategoryEnum::getNewInstance(self::CATEGORY);
    }

    /**
     * Do getParameters
     *
     * @return array
     */
    public function getParameters(): array
    {
        return array();
    }

    /**
     * Do setParameters
     *
     * @param array|null $parameters
     *
     * @return PluginInterface
     */
    public function setParameters(?array $parameters): PluginInterface
    {
        return $this;
    }

    /**
     * The name of the plugin without the "post.".
     * Example: 'Collibris'
     *
     * @return string
     */
    abstract protected function getPluginPostName(): string;

    /**
     * @return PostStateEnum
     */
    abstract protected function getPostState(): PostStateEnum;

    /**
     * @return array
     */
    abstract protected function getLocales(): array;

    /**
     * @param Plugin $plugin
     *
     * @return array
     */
    private function getPostsByLocale(Plugin $plugin): array
    {
        $posts = array();

        foreach ($this->getLocales() as $locale) {
            $postsFromApi = $this->partnerConnection->sendResultToPlugin($locale);
            $posts[] = $this->postMapper->formatArrayFromApi(
                $postsFromApi,
                $plugin,
                $this->getPostState(),
                $locale
            );
        }

        return $posts;
    }

    /**
     * @param Plugin $plugin
     *
     * @return array
     * @throws \Symfony\Component\Security\Acl\Exception\InvalidDomainObjectException
     * @throws \Symfony\Component\Security\Acl\Exception\AclAlreadyExistsException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function insertPostsForPlugin(Plugin $plugin): array
    {
        $postsAdded = array();
        /** @var PostRepository $postRepository */
        $postRepository = $this->entityManager->getRepository('TodotodayCMSBundle:Post');
        $postsByLocale = $this->getPostsByLocale($plugin);

        /** @var array $posts */
        foreach ($postsByLocale as $localeIndex => $posts) {
            /** @var Post $post */
            foreach ($posts as $postIndex => $post) {
                if ($localeIndex === self::FIRST_LANGUAGE && !$postRepository->postPluginExist($post, $plugin)) {
                    try {
                        $this->isPostValid($plugin, $post);
                        $plugin->addPost($post);
                        $this->entityManager->persist($post);
                        $this->aclProvider->createAcl(ObjectIdentity::fromDomainObject($post));
                        $this->entityManager->flush();
                        $event = new PostPluginEvent($plugin, $post);
                        $this->dispatcher->dispatch(PostPluginEvent::ADD, $event);
                        $postsAdded[] = $post;
                    } catch (PluginContentMissingMandatoryFieldException $exception) {
                        $event = new PostPluginEvent($plugin, $post, $exception);
                        $this->dispatcher->dispatch(PostPluginEvent::MISSING_MANDATORY_FIELD, $event);
                    } catch (\Exception $exception) {
                        $plugin->removePost($post);
                        $event = new PostPluginEvent($plugin, $post, $exception);
                        $this->dispatcher->dispatch(PostPluginEvent::ERROR, $event);
                    }
                } elseif ($localeIndex > self::FIRST_LANGUAGE && $postRepository->postPluginExist($post, $plugin)) {
                    /** @var Post $postFirstLocale */
                    $postFirstLocale = $postsByLocale[self::FIRST_LANGUAGE][$postIndex];

                    $postFirstLocale->setPluginPostId($post->getPluginPostId())
                        ->setTitle($post->getTitle())
                        ->setSubtitle($post->getSubtitle())
                        ->setContent($post->getContent())
                        ->setLocale($post->getLocale());

                    $this->entityManager->flush();
                }
            }
        }

        return $postsAdded;
    }

    /**
     * @param array $linkAgencyPlugins
     * @param array $posts
     *
     * @return array
     * @throws \Symfony\Component\Security\Acl\Exception\InvalidDomainObjectException
     * @throws \Symfony\Component\Security\Acl\Exception\AclAlreadyExistsException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     * @throws \Exception
     */
    private function publishPostsForAgencies(?array $linkAgencyPlugins, array $posts): array
    {
        $linkPostAgencyInserted = array();

        foreach ($posts as $post) {
            /** @var LinkAgencyPlugin $linkAgencyPlugin */
            foreach ($linkAgencyPlugins as $linkAgencyPlugin) {
                $publicationDate = Carbon::now();//todo: publicationDate must be calculated with the parameters !
                $publicationDate->addDays(7)
                    ->startOfWeek()
                    ->hour(6)->minute(0)->second(0);
                $postAgency = new LinkPostAgency();
                $postAgency->setAgency($linkAgencyPlugin->getAgency())
                    ->setPost($post)
                    ->setPublicationDate($publicationDate)
                    ->setState(LinkPostAgencyStateEnum::getNewInstance(LinkPostAgencyStateEnum::UNPUBLISHED));
                $this->entityManager->persist($postAgency);
                $this->aclProvider->createAcl(ObjectIdentity::fromDomainObject($postAgency));
                $linkPostAgencyInserted[] = $postAgency;
            }
        }
        $this->entityManager->flush();

        return $linkPostAgencyInserted;
    }

    /**
     * @param Plugin $plugin
     * @param Post   $post
     *
     * @return bool
     * @throws PluginContentMissingMandatoryFieldException
     */
    private function isPostValid(Plugin $plugin, Post $post): bool
    {
        if ($post->getMedia() && $post->getContent() && $post->getTitle() && $post->getPluginPostId()
            && $post->getPluginPostUrl()) {
            return true;
        }

        throw new PluginContentMissingMandatoryFieldException($plugin, $post, 'post');
    }
}
