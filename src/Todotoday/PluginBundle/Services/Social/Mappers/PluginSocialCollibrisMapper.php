<?php declare(strict_types=1);

namespace Todotoday\PluginBundle\Services\Social\Mappers;

use Todotoday\MediaBundle\Repository\MediaRepository;
use Todotoday\PluginBundle\Entity\Partner\Collibris\Book;
use Todotoday\SocialBundle\Entity\SocialPluginContent;

/**
 * Created by PhpStorm.
 * User: riwan
 * Date: 13/10/17
 * Time: 10:25
 */
class PluginSocialCollibrisMapper extends AbstractPluginSocialMapper
{
    /**
     * @param array|null $rawSocialContents
     * @param string     $pluginName
     *
     * @return array
     * @throws \Exception
     */
    public function formatArray(?array $rawSocialContents, string $pluginName): array
    {
        $contents = array();

        /** @var Book $rawSocialContent */
        foreach ($rawSocialContents as $rawSocialContent) {
            $collibrisUrl = $rawSocialContent->getCollibrisUrl();

            $socialPluginContent = new SocialPluginContent();
            $socialPluginContent->setTitle($rawSocialContent->getTitle())
                ->setPluginContentId($this->getEndOfUrl($collibrisUrl))
                ->setContent($rawSocialContent->getDescription())
                ->setPluginUrl($collibrisUrl);

            $imageCoverUrl = $rawSocialContent->getCover();
            $media = $this->imageCurlDownload->getMediaUrl(
                $pluginName,
                $this->getEndOfUrl($imageCoverUrl),
                $imageCoverUrl
            );

            if ($media) {
                $socialPluginContent->setMedia($media);
            }
            $contents[] = $socialPluginContent;
        }

        return $contents;
    }
}
