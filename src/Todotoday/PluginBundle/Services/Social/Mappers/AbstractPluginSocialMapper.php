<?php declare(strict_types=1);

namespace Todotoday\PluginBundle\Services\Social\Mappers;

use Todotoday\MediaBundle\Repository\MediaRepository;
use Todotoday\PluginBundle\Entity\Plugin;
use Todotoday\PluginBundle\Services\ImageCurlDownload;
use Todotoday\SocialBundle\Entity\SocialPluginContent;

/**
 * Created by PhpStorm.
 * User: riwan
 * Date: 13/10/17
 * Time: 10:25
 */
abstract class AbstractPluginSocialMapper
{
    /**
     * @var ImageCurlDownload
     */
    protected $imageCurlDownload;

    /**
     * AbstractPluginSocialMapper constructor.
     *
     * @param ImageCurlDownload $imageCurlDownload
     */
    public function __construct(ImageCurlDownload $imageCurlDownload)
    {
        $this->imageCurlDownload = $imageCurlDownload;
    }

    /**
     * The input is an array of array from API
     *
     * @param array|null $socialPluginContents
     * @param Plugin     $plugin
     *
     * @return array
     */
    public function formatArrayFromApi(array $socialPluginContents, Plugin $plugin): array
    {
        $contentsFormatted = array();
        $contentsToFormat = $this->formatArray($socialPluginContents, $plugin->getName());

        foreach ($contentsToFormat as $content) {
            if (!($content instanceof SocialPluginContent)) {
                continue;
            }
            $this->fillEmptyContentValues($content);
            $content->setPlugin($plugin);

            $contentsFormatted[] = $content;
        }

        return $contentsFormatted;
    }

    /**
     * The input is the API (Collibris, etc...)
     * The output is a array of SocialPluginContent.
     *
     * @param array|null $rawSocialContents
     * @param string     $pluginName
     *
     * @return array
     */
    abstract protected function formatArray(?array $rawSocialContents, string $pluginName): array;

    /**
     * @param null|string $url
     *
     * @return null|string
     */
    protected function getEndOfUrl(?string $url): ?string// todo: créer un Trait
    {
        if (!$url) {
            return null;
        }
        $urlExploded = explode('/', $url);

        return end($urlExploded);
    }

    /**
     * @param SocialPluginContent $content
     *
     * @return SocialPluginContent
     */
    private function fillEmptyContentValues(SocialPluginContent $content): SocialPluginContent
    {
        return $content;
    }
}
