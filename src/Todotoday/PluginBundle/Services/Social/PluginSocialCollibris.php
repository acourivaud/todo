<?php declare(strict_types=1);

namespace Todotoday\PluginBundle\Services\Social;

use Todotoday\PluginBundle\Services\PluginSocial;

/**
 * Created by PhpStorm.
 * User: riwan
 * Date: 12/10/17
 * Time: 17:32
 */

class PluginSocialCollibris extends PluginSocial
{
    /**
     * Do getFrenchLokalizeKey
     *
     * @return string
     */
    public function getPluginNameKey(): string
    {
        // TODO: Implement getPluginNameKey() method.
    }

    /**
     * The name of the plugin without the "social.".
     * Example: 'Collibris'
     *
     * @return string
     */
    protected function getPluginSocialName(): string
    {
        return 'collibris';
    }
}
