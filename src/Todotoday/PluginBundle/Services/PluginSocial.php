<?php declare(strict_types=1);

namespace Todotoday\PluginBundle\Services;

use Carbon\Carbon;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Model\MutableAclProviderInterface;
use Todotoday\PluginBundle\Entity\Plugin;
use Todotoday\PluginBundle\Enum\PluginCategoryEnum;
use Todotoday\PluginBundle\Enum\PluginDayEnum;
use Todotoday\PluginBundle\Enum\PluginFrequencyEnum;
use Todotoday\PluginBundle\Event\SocialPluginEvent;
use Todotoday\PluginBundle\Exceptions\PluginContentMissingMandatoryFieldException;
use Todotoday\PluginBundle\Interfaces\PluginInterface;
use Todotoday\PluginBundle\Services\Connections\AbstractPartnerConnection;
use Todotoday\PluginBundle\Services\Social\Mappers\AbstractPluginSocialMapper;
use Todotoday\SocialBundle\Entity\BotComment;
use Todotoday\SocialBundle\Entity\SocialGroup;
use Todotoday\SocialBundle\Entity\SocialPluginContent;
use Todotoday\SocialBundle\Repository\SocialGroupRepository;
use Todotoday\SocialBundle\Repository\SocialPluginContentRepository;

/**
 * Class PluginSocial
 *
 * @package Todotoday\PluginBundle\Services
 */
abstract class PluginSocial implements PluginInterface
{
    public const CATEGORY = PluginCategoryEnum::SOCIAL_PLUGIN;

    /**
     * @var string
     *
     * Frequency of launch for the Cron
     */
    protected $cronFrequency = PluginFrequencyEnum::EVERY_WEEK;

    /**
     * @var int
     *
     * Day when the Cron is launch
     */
    protected $dayToLaunchCron = PluginDayEnum::MONDAY;

    /**
     * @var AbstractPluginSocialMapper
     */
    protected $socialMapper;

    /**
     * @var AbstractPartnerConnection
     */
    protected $partnerConnection;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var MutableAclProviderInterface
     */
    private $aclProvider;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * PluginSocial constructor.
     *
     * @param AbstractPluginSocialMapper  $socialMapper
     * @param AbstractPartnerConnection   $partnerConnection
     * @param EntityManager               $entityManager
     * @param MutableAclProviderInterface $aclProvider
     * @param EventDispatcherInterface    $dispatcher
     */
    public function __construct(
        AbstractPluginSocialMapper $socialMapper,
        AbstractPartnerConnection $partnerConnection,
        EntityManager $entityManager,
        MutableAclProviderInterface $aclProvider,
        EventDispatcherInterface $dispatcher
    ) {
        $this->socialMapper = $socialMapper;
        $this->partnerConnection = $partnerConnection;
        $this->entityManager = $entityManager;
        $this->aclProvider = $aclProvider;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param Plugin $plugin
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Symfony\Component\Security\Acl\Exception\AclAlreadyExistsException
     * @throws \Symfony\Component\Security\Acl\Exception\InvalidDomainObjectException
     * @throws \Todotoday\PluginBundle\Exceptions\PluginNotFoundException
     */
    public function executeCron(Plugin $plugin): void
    {
        $socialContentsInserted = $this->insertContents($plugin);
        $this->publishComments($plugin, $socialContentsInserted);
    }

    /**
     * Do getPlugin
     *
     * @return Plugin
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getPlugin(): Plugin
    {
        return new Plugin(
            $this->getSlug()
        );
    }

    /**
     * Do getSlug
     *
     * @return string
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getSlug(): string
    {
        return $this->getCategory()->get() . '.' . $this->getPluginSocialName();
    }

    /**
     * Do getCategory
     *
     * @return PluginCategoryEnum
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getCategory(): PluginCategoryEnum
    {
        return PluginCategoryEnum::getNewInstance(self::CATEGORY);
    }

    /**
     * Do getParameters
     *
     * @return array
     */
    public function getParameters(): array
    {
        return array(
            'cronFrequency' => $this->cronFrequency,
            'dayToLaunchCron' => $this->dayToLaunchCron,
        );
    }

    /**
     * Do setParameters
     *
     * @param array|null $parameters
     *
     * @return PluginInterface
     */
    public function setParameters(?array $parameters): PluginInterface
    {
        $this->cronFrequency = $parameters['cronFrequency'] ?? '';
        $this->dayToLaunchCron = $parameters['dayToLaunchCron'] ?? '';

        return $this;
    }

    /**
     * The name of the plugin without the "social.".
     * Example: 'Collibris'
     *
     * @return string
     */
    abstract protected function getPluginSocialName(): string;

    /**
     * @param null|Plugin $plugin
     *
     * @return array
     */
    private function getSocialPluginContents(Plugin $plugin): array
    {
        $contentsFromApi = $this->partnerConnection->sendResultToPlugin();
        $socialPluginContents = $this->socialMapper->formatArrayFromApi($contentsFromApi, $plugin);

        return $socialPluginContents;
    }

    /**
     * @param Plugin $plugin
     *
     * @return array
     * @throws \Symfony\Component\Security\Acl\Exception\InvalidDomainObjectException
     * @throws \Symfony\Component\Security\Acl\Exception\AclAlreadyExistsException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Todotoday\PluginBundle\Exceptions\PluginNotFoundException
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function insertContents(Plugin $plugin): array
    {
        $contentsAdded = array();

        /** @var SocialPluginContentRepository $socialPluginContentRepo */
        $socialPluginContentRepo =
            $this->entityManager->getRepository('TodotodaySocialBundle:SocialPluginContent');
        $contents = $this->getSocialPluginContents($plugin);

        /** @var SocialPluginContent $content */
        foreach ($contents as $content) {
            if (!$socialPluginContentRepo->socialPluginContentExist($content)) {
                try {
                    $this->isSocialValid($plugin, $content);
                    $this->entityManager->persist($content);
                    $this->entityManager->flush();
                    $event = new SocialPluginEvent($plugin, $content);
                    $this->dispatcher->dispatch(SocialPluginEvent::ADD_SOCIAL_CONTENT, $event);
                    $contentsAdded[] = $content;
                } catch (PluginContentMissingMandatoryFieldException $exception) {
                    $event = new SocialPluginEvent($plugin, $content, null, $exception);
                    $this->dispatcher->dispatch(SocialPluginEvent::MISSING_MANDATORY_FIELD, $event);
                } catch (\Exception $exception) {
                    $event = new SocialPluginEvent($plugin, $content, null, $exception);
                    $this->dispatcher->dispatch(SocialPluginEvent::ERROR, $event);
                }
            }
        }

        return $contentsAdded;
    }

    /**
     * @param Plugin $plugin
     * @param array  $socialPluginContents
     *
     * @return array
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Symfony\Component\Security\Acl\Exception\AclAlreadyExistsException
     * @throws \Symfony\Component\Security\Acl\Exception\InvalidDomainObjectException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function publishComments(Plugin $plugin, array $socialPluginContents): array
    {
        $botCommentsInserted = array();
        /** @var SocialGroupRepository $socialGroupRepo */
        $socialGroupRepo = $this->entityManager->getRepository('TodotodaySocialBundle:SocialGroup');
        $socialGroups = $socialGroupRepo->findByTags($plugin->getSocialTags());

        /** @var SocialPluginContent $content */
        foreach ($socialPluginContents as $content) {
            /** @var SocialGroup $socialGroup */
            foreach ($socialGroups as $socialGroup) {
                $publicationDate = $this->getPublicationDate();

                $botComment = new BotComment();
                $botComment->setContent($content->getContent())
                    ->setSocialGroup($socialGroup)
                    ->setPlugin($plugin)
                    ->setUrl($content->getPluginUrl())
                    ->setMedia($content->getMedia())
                    ->setPublicationDate($publicationDate);

                $this->entityManager->persist($botComment);
                $acl = $this->aclProvider->createAcl(ObjectIdentity::fromDomainObject($botComment));
                $this->aclProvider->updateAcl($acl);
                $event = new SocialPluginEvent($plugin, null, $botComment);
                $this->dispatcher->dispatch(SocialPluginEvent::ADD_BOT_COMMENT, $event);
                $botCommentsInserted[] = $botComment;
            }
        }

        $this->entityManager->flush();

        return $botCommentsInserted;
    }

    /**
     * @return \DateTime
     */
    private function getPublicationDate(): \DateTime
    {
        $publicationDate = Carbon::now();

        if ($this->cronFrequency === PluginFrequencyEnum::EVERY_WEEK) {
            $publicationDate->addDays(7)
                ->startOfWeek()
                ->addDays($this->dayToLaunchCron);
        }

        $publicationDate->hour(6)->minute(0)->second(0);
    }

    /**
     * @param Plugin              $plugin
     * @param SocialPluginContent $content
     *
     * @return bool
     * @throws PluginContentMissingMandatoryFieldException
     */
    private function isSocialValid(Plugin $plugin, SocialPluginContent $content): bool
    {
        if ($content->getMedia() && $content->getContent() && $content->getTitle() && $content->getPluginContentId()
            && $content->getPluginUrl()) {
            return true;
        }

        throw new PluginContentMissingMandatoryFieldException($plugin, $content, 'social plugin content');
    }
}
