<?php declare(strict_types=1);

namespace Todotoday\PluginBundle\Services;

use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Filesystem\Filesystem;
use Todotoday\MediaBundle\Entity\Media;

/**
 * Class ImageCurlDownload
 *
 * @SuppressWarnings(PHPMD.UnusedPrivateMethod)
 * @category   Todo-Todev
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class ImageCurlDownload
{
    /**
     * @var Slugify
     */
    private $slugify;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var \Doctrine\ORM\EntityRepository|\Todotoday\MediaBundle\Repository\MediaRepository
     */
    private $mediaRepository;

    /**
     * ImageCurlDownload constructor.
     *
     * @param Slugify       $slugify
     * @param Filesystem    $filesystem
     * @param EntityManager $entityManager
     */
    public function __construct(Slugify $slugify, Filesystem $filesystem, EntityManager $entityManager)
    {
        $this->slugify = $slugify;
        $this->filesystem = $filesystem;
        $this->entityManager = $entityManager;
        $this->mediaRepository = $entityManager->getRepository('TodotodayMediaBundle:Media');
    }

    /**
     * @param string $fileName
     *
     * @return string
     */
    public function getSlug(string $fileName): string
    {
        return $this->slugify->slugify($fileName, '.');
    }

    /**
     * @param string      $pluginName
     * @param null|string $mediaName
     * @param null|string $mediaUrl
     *
     * @return null|Media
     * @throws \Exception
     */
    public function getMediaUrl(string $pluginName, ?string $mediaName, ?string $mediaUrl): ?Media
    {
        if ($mediaUrl) {
            $fileName = $pluginName . '.' . $mediaName;
            if (strpos($fileName, ';')) {// to avoid "file.jpeg;charset=ut8"
                $fileName = substr($fileName, 0, strpos($fileName, ';'));
            }
            $fileName = $this->getSlug($fileName);
            if (!($media = $this->mediaRepository->findOneBy(array('name' => $fileName)))) {
                $media = $this->getMedia($mediaUrl, $fileName);
            }

            return $media;
        }

        return null;
    }

    /**
     * @param string $url
     * @param string $fileName
     *
     * @return Media
     * @throws \Exception
     */
    private function getMedia(string $url, string $fileName): Media
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'User-Agent: curl/7.39.0');
        curl_setopt($curl, CURLOPT_HEADER, false);
        $data = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        if ($httpcode === 200) {
            $name = $this->getSlug($fileName);
            $this->filesystem->dumpFile(sys_get_temp_dir() . '/' . $name, $data);
            unset($data);

            if (@exif_imagetype(sys_get_temp_dir() . '/' . $name)) {
                $media = new Media();

                $category = $this->entityManager->getRepository('TodotodayClassificationBundle:Category')
                    ->findOneBy(
                        array(
                            'slug' => 'agency',
                        )
                    );
                $media->setContext('default');
                $media->setCategory($category);
                $media->setName($name);
                $media->setProviderName('sonata.media.provider.image');
                $media->setBinaryContent(sys_get_temp_dir() . '/' . $name);

                return $media;
            } else {
                throw new \Exception('File corrupted : ' . exif_imagetype(sys_get_temp_dir()));
            }
        } else {
            throw new \Exception('HTTP ERROR ' . $httpcode . ' => ' . $url);
        }
    }
}
