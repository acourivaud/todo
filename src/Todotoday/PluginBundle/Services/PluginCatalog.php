<?php declare(strict_types=1);

namespace Todotoday\PluginBundle\Services;

use Symfony\Component\Translation\TranslatorInterface;
use Todotoday\CatalogBundle\Entity\Api\Microsoft\RetailCatalog;
use Todotoday\CatalogBundle\Services\CatalogManager;
use Todotoday\PluginBundle\Entity\Plugin;
use Todotoday\PluginBundle\Interfaces\PluginImageInterface;
use Todotoday\PluginBundle\Interfaces\PluginInterface;
use Todotoday\PluginBundle\Enum\PluginCategoryEnum;
use Todotoday\PluginBundle\Interfaces\PluginViewInterface;

/**
 * Class PluginCatalog
 *
 * @category   Todo-Todev
 * @package    Todotoday\PluginBundle\Services
 * @subpackage Todotoday\PluginBundle\DependencyInjection\Compiler
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
abstract class PluginCatalog implements PluginInterface, PluginViewInterface, PluginImageInterface
{
    /**
     * CATEGORY
     */
    public const CATEGORY = PluginCategoryEnum::CATALOG_PLUGIN;
    /**
     * BEGINNING_LABEL
     * This label is use to place the plugin's item in the end of the family
     */
    public const BEGINNING_LABEL = '9999;;9999';
    /**
     * MIDDLE_LABEL
     */
    public const MIDDLE_LABEL = ' - ;;';

    /**
     * @var string
     *
     * Familie(s) of catalog separated by separator where Plugin must be shown
     */
    protected $catalogFamilies;

    /**
     * @var string
     *
     * Position where item's Plugin must appear.
     * It's an string separated by separator in we need array
     * We can have multiple positions in case of multiple
     * families
     */
    protected $itemPositions;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * PluginCatalog constructor.
     *
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @return array
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     */
    public function getRetailsCatalog(): array
    {
        $retailsCatalog = array();

        foreach (explode(PluginManager::PARAMETERS_DELIMITER, $this->getCatalogFamilies()) as $key => $families) {
            $itemPosition = (int) (explode(PluginManager::PARAMETERS_DELIMITER, $this->itemPositions)[$key] ?? 0);
            $retailCatalog = new RetailCatalog();
            $retailCatalog->setItemId($this->getSlug() . '.' . $key);// The key is just to avoid bugs in catalog search
            // (same Id for multiple catalog families)
            $retailCatalog->setProductType(self::getProductType());
            $retailCatalog->setItemModelGroup($this->getItemModelGroup());
            $retailCatalog->setNameTrans($this->translator->trans($this->getItemNameKey(), array(), 'todotoday'));
            $retailCatalog->setFamilyMdiIcon($this->getMdiIcon());
            $retailCatalog->setFamilyPluginPosition($itemPosition);

            if ($families) {
                $hierarchy = '';
                $familiesExplode = explode(CatalogManager::HIERARCHY_DELIMITER, $families);

                foreach ($familiesExplode as $familyNumber => $family) {
                    $hierarchy .= $this::BEGINNING_LABEL;
                    $isLastFamily = (count($familiesExplode) - 1) === $familyNumber;

                    $hierarchy .= $this::MIDDLE_LABEL . $family;

                    if (!$isLastFamily) {
                        $hierarchy .= CatalogManager::HIERARCHY_DELIMITER;
                    }
                }
                $retailCatalog->setHierarchy($hierarchy);
            }
            $this->modifyRetailCatalog($retailCatalog);//the code specific to the service (Ex: PluginCatalogBlablacar)

            $retailsCatalog[] = $retailCatalog;
        }

        return $retailsCatalog;
    }

    /**
     * @return string
     */
    public static function getProductType(): string
    {
        return 'plugin_' . self::CATEGORY;
    }

    /**
     * @return Plugin
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getPlugin(): Plugin
    {
        return new Plugin(
            $this->getSlug()
        );
    }

    /**
     * @return array
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getView(): array
    {
        $childrenView = $this->getViewWithParameters();
        $view =
            !empty($childrenView['view']) ? '::plugin/' . $this->getCategory()->get() . '/' . $childrenView['view']
                : null;
        $parameters = $childrenView['parameters'] ?? null;

        return array(
            'view' => $view,
            'parameters' => $parameters,
        );
    }

    /**
     * @return null|string
     */
    public function getImage(): ?string
    {
        return '/plugin/' . self::CATEGORY . '/' . $this->getPluginCatalogName() . '.png';
    }

    /**
     * @return string
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getSlug(): string
    {
        return $this->getCategory()->get() . '.' . $this->getPluginCatalogName();
    }

    /**
     * @return PluginCategoryEnum
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getCategory(): PluginCategoryEnum
    {
        return PluginCategoryEnum::getNewInstance(self::CATEGORY);
    }

    /**
     * @return array
     */
    public function getParameters(): array
    {
        return array(
            'catalogFamilies' => $this->catalogFamilies,
            'itemPositions' => $this->itemPositions,
        );
    }

    /**
     * @param array $parameters
     *
     * @return PluginInterface
     */
    public function setParameters(?array $parameters): PluginInterface
    {
        $this->catalogFamilies = isset($parameters['catalogFamilies']) ?
            str_replace('+', '||', $parameters['catalogFamilies']) :
            '';
        $this->itemPositions = $parameters['itemPositions'] ?? '';

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCatalogFamilies(): ?string
    {
        return $this->catalogFamilies;
    }

    /**
     * @param string $catalogFamilies
     */
    public function setCatalogFamilies(string $catalogFamilies)
    {
        $this->catalogFamilies = $catalogFamilies;
    }

    /**
     * @return string|null
     */
    public function getItemPositions(): ?string
    {
        return $this->itemPositions;
    }

    /**
     * @param string $itemPositions
     */
    public function setItemPositions(string $itemPositions)
    {
        $this->itemPositions = $itemPositions;
    }

    /**
     * @param Plugin $plugin
     */
    public function executeCron(Plugin $plugin): void
    {
        // TODO: Implement executeCron() method.
    }

    /**
     * The name of the plugin without the "catalog.".
     * Example: 'Blablacar'
     *
     * @return string
     */
    abstract protected function getPluginCatalogName(): string;

    /**
     * The lokalize key for the item name
     *
     * @return string
     */
    abstract protected function getItemNameKey(): string;

    /**
     * @param RetailCatalog $retailCatalog
     *
     */
    abstract protected function modifyRetailCatalog(RetailCatalog $retailCatalog): void;

    /**
     * @return array
     */
    abstract protected function getViewWithParameters(): array;

    /**
     * The name of the mdi icon that will show in the search dropdown of the catalog
     * If there is no picto_url for the plugin, the mdi icon will be use as picto for the plugin family
     *
     * @return string
     */
    abstract protected function getMdiIcon(): string;

    /**
     * @return string
     */
    private function getItemModelGroup(): string
    {
        return $this->getPluginCatalogName();
    }
}
