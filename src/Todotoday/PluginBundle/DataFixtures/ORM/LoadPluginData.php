<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 23/10/17
 * Time: 10:29
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\PluginBundle\DataFixtures
 *
 * @subpackage Todotoday\PluginBundle\DataFixtures
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\PluginBundle\DataFixtures;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyLinkedMicrosoft;
use Todotoday\PluginBundle\Entity\LinkAgencyPlugin;
use Todotoday\PluginBundle\Entity\Plugin;
use Todotoday\PluginBundle\Enum\PluginCategoryEnum;

/**
 * Class LoadPluginData
 * @SuppressWarnings(PHPMD.UnusedLocalVariable)
 */
class LoadPluginData extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * @var array
     */
    public static $plugins = [
        'blablacar' => [
            [
                'type' => PluginCategoryEnum::CATALOG_PLUGIN,
                'enabled' => true,
            ],
        ],
        'collibris' => [
            [
                'type' => PluginCategoryEnum::POST_PLUGIN,
                'enabled' => true,
            ],
            [
                'type' => PluginCategoryEnum::SOCIAL_PLUGIN,
                'enabled' => true,
            ],
        ],
    ];

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return [LoadAgencyLinkedMicrosoft::class];
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        foreach (self::$plugins as $name => $plugin) {
            foreach ($plugin as $value) {
                $pluginToInsert = new Plugin($value['type'] . '.' . $name);
                $pluginToInsert->setEnabled($value['enabled']);

                foreach (LoadAgencyLinkedMicrosoft::getAgenciesLinkedSlug() as $agencySlug => $infoAgency) {
                    $agency = $this->getReference('agency_' . $agencySlug);
                    $linkAgencyPlugin = new LinkAgencyPlugin();
                    $linkAgencyPlugin
                        ->setAgency($agency)
                        ->setEnabled(false);
                    $pluginToInsert->addLinkAgency($linkAgencyPlugin);
                }
                $manager->persist($pluginToInsert);
            }
        }
        $manager->flush();
    }
}
