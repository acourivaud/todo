<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 01/05/17
 * Time: 18:39
 */

namespace Todotoday\PluginBundle\Enum;

use Actiane\ToolsBundle\Enum\AbstractEnum;

/**
 * Class PluginDayEnum
 * @package Todotoday\PluginBundle\Enum
 */
class PluginDayEnum extends AbstractEnum
{
    public const MONDAY = 0;
    public const TUESDAY = 1;
    public const WEDNESDAY = 2;
    public const THURSDAY = 3;
    public const FRIDAY = 4;
    public const SATURDAY = 5;
    public const SUNDAY = 6;
}
