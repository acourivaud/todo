<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 01/05/17
 * Time: 18:39
 */

namespace Todotoday\PluginBundle\Enum;

use Actiane\ToolsBundle\Enum\AbstractEnum;

/**
 * Class PluginCategoryEnum
 * @package Todotoday\PluginBundle\Enum
 */
class PluginCategoryEnum extends AbstractEnum
{
    //ATTENTION : choisir des valeurs en minuscule pour que le PluginManager->launchPlugins() fonctionne
    public const CATALOG_PLUGIN = 'catalog';
    public const POST_PLUGIN = 'post';
    public const SOCIAL_PLUGIN = 'social';
}
