<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 01/05/17
 * Time: 18:39
 */

namespace Todotoday\PluginBundle\Enum;

use Actiane\ToolsBundle\Enum\AbstractEnum;

/**
 * Class PluginSocialFrequencyEnum
 * @package Todotoday\PluginBundle\Enum
 */
class PluginFrequencyEnum extends AbstractEnum
{
    public const EVERY_DAY = 'every.day';
    public const EVERY_WEEK = 'every.week';
    public const EVERY_MONTH = 'every.month';
}
