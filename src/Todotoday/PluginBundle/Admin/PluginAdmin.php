<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 18/10/17
 * Time: 10:30
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\PluginBundle\Admin
 *
 * @subpackage Todotoday\PluginBundle\Admin
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\PluginBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Sonata\AdminBundle\Route\RouteCollection;
use Todotoday\PluginBundle\Entity\Plugin;
use Todotoday\PluginBundle\Enum\PluginCategoryEnum;

/**
 * Class PluginAdmin
 */
class PluginAdmin extends AbstractAdmin
{
    /**
     * Whether or not to persist the filters in the session.
     *
     * @var bool
     */
    protected $persistFilters = true;

    /**
     * @var array
     */
    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'category',
    );

    /**
     * @param ListMapper $list
     *
     * @throws \RuntimeException
     */
    public function configureListFields(ListMapper $list): void
    {
        $list->add('id')
            ->add('category')
            ->add('name')
            ->add('linkAgencies')
            ->add('socialTags')
            ->add(
                'enabled',
                null,
                array(
                    'editable' => true,
                )
            )
            ->add(
                '_action',
                null,
                [
                    'actions' => [
                        'edit' => [],
                    ],
                ]
            );
    }

    /**
     * @param mixed $object
     */
    public function postUpdate($object)
    {
        /** @var Plugin $object */
        foreach ($object->getLinkAgencies() as $linkAgencyPlugin) {
            $this->createObjectSecurity($linkAgencyPlugin);
        }
    }

    /**
     * @param FormMapper $form
     */
    protected function configureFormFields(FormMapper $form)
    {
        /** @var Plugin $plugin */
        $plugin = $this->getSubject();
        if ($plugin->getCategory() === PluginCategoryEnum::SOCIAL_PLUGIN) {
            $form->add('socialTags');
        } else {
            $form
                ->add(
                    'linkAgencies',
                    CollectionType::class,
                    array('by_reference' => false),
                    array(
                        'edit' => 'inline',
                        'inline' => 'table',
                        'sortable' => 'position',
                    )
                );
        }
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
    }
}
