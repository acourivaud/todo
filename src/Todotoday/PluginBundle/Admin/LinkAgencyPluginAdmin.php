<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 18/10/17
 * Time: 11:25
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\PluginBundle\Admin
 *
 * @subpackage Todotoday\PluginBundle\Admin
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\PluginBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\CoreBundle\Form\Type\ImmutableArrayType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Todotoday\PluginBundle\Entity\LinkAgencyPlugin;

/**
 * Class LinkAgencyPluginAdmin
 *
 * @SuppressWarnings(PHPMD.UnusedLocalVariable)
 */
class LinkAgencyPluginAdmin extends AbstractAdmin
{
    /**
     * Whether or not to persist the filters in the session.
     *
     * @var bool
     */
    protected $persistFilters = true;

    /**
     * @param ListMapper $list
     *
     * @throws \RuntimeException
     */
    protected function configureListFields(ListMapper $list)
    {
        $list->add('agency')
            ->add('plugin')
            ->add(
                'enabled',
                null,
                array(
                    'editable' => true,
                )
            )
            ->add('_action', null, ['actions' => ['edit' => []]]);
    }

    /**
     * @param FormMapper $form
     */
    protected function configureFormFields(FormMapper $form)
    {
        $form->add(
            'agency',
            ModelListType::class,
            [
                'btn_add' => false,
                'btn_delete' => false,
            ]
        )
            ->add('enabled');

        /** @var LinkAgencyPlugin $linkAgencyPlugin */
        if ($this->isGranted('ROLE_COMMUNITY_MANAGER')
            && ($linkAgencyPlugin = $this->getSubject())
            && $this->isCurrentRoute('edit')
        ) {
            $formOptions = [];
            foreach ((array) $linkAgencyPlugin->getParameters() as $paramName => $value) {
                $formOptions[] = [$paramName, TextType::class, ['required' => false]];
            }

            $form->add(
                'parameters',
                ImmutableArrayType::class,
                ['keys' => $formOptions]
            );
        }
    }

    /**
     * @param DatagridMapper $filter
     *
     * @throws \RuntimeException
     */
    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter->add('agency')
            ->add('plugin')
            ->add('enabled')
        ;
    }
}
