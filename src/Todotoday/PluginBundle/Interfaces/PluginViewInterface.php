<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 10/04/2017
 * Time: 14:27
 */

namespace Todotoday\PluginBundle\Interfaces;

/**
 * Interface PluginViewInterface
 *
 * @category   Todo-Todev
 * @package    Todotoday\PluginBundle
 * @subpackage Todotoday\PluginBundle\Interfaces
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
interface PluginViewInterface
{
    /**
     * @return array
     */
    public function getView(): array;
}
