<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 10/04/2017
 * Time: 14:27
 */

namespace Todotoday\PluginBundle\Interfaces;

/**
 * Interface PluginImageInterface
 *
 * @category   Todo-Todev
 * @package    Todotoday\PluginBundle
 * @subpackage Todotoday\PluginBundle\Interfaces
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
interface PluginImageInterface
{
    /**
     * @return null|string
     */
    public function getImage(): ?string;
}
