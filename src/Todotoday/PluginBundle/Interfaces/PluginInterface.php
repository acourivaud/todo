<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 10/04/2017
 * Time: 14:27
 */

namespace Todotoday\PluginBundle\Interfaces;

use Todotoday\PluginBundle\Enum\PluginCategoryEnum;
use Todotoday\PluginBundle\Entity\Plugin;

/**
 * Interface PluginInterface
 *
 * @category   Todo-Todev
 * @package    Todotoday\PluginBundle
 * @subpackage Todotoday\PluginBundle\Interfaces
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
interface PluginInterface
{
    /**
     * Do getPlugin
     *
     * @return Plugin
     */
    public function getPlugin(): Plugin;

    /**
     * Do getSlug
     *
     * @return string
     */
    public function getSlug(): string;

    /**
     * Do getFrenchLokalizeKey
     *
     * @return string
     */
    public function getPluginNameKey(): string;

    /**
     * Do getCategory
     *
     * @return PluginCategoryEnum
     */
    public function getCategory(): PluginCategoryEnum;

    /**
     * Do getParameters
     *
     * @return array
     */
    public function getParameters(): array;

    /**
     * Do setParameters
     *
     * @param array|null $parameters
     *
     * @return PluginInterface
     */
    public function setParameters(?array $parameters): self;

    /**
     * Do execute Cron
     *
     * @param Plugin $plugin
     */
    public function executeCron(Plugin $plugin): void;
}
