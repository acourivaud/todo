<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 15/05/17
 * Time: 16:07
 */

namespace Todotoday\InvoiceBundle\Repository\Api\Microsoft;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiRepository;
use Actiane\ApiConnectorBundle\Lib\Request;
use Carbon\Carbon;
use Todotoday\AccountBundle\Entity\Adherent;

/**
 * Class CustomerInvoicesRepository
 * @package Todotoday\InvoiceBundle\Repository\Api\Microsoft
 */
class CustomerInvoicesRepository extends AbstractApiRepository
{
    /**
     * @param Adherent $adherent
     *
     * @return array|null
     */
    public function findCustomerInvoicesByAdherent(Adherent $adherent): ?array
    {
        $filter = 'CustomerAccount eq \'' . $adherent->getCustomerAccount() . '\'';

        return $this->getRequest($filter)->execute();
    }

    /**
     * @param Adherent  $adherent
     * @param \Datetime $datetime
     *
     * @return array|null
     */
    public function findCustomerInvoicesByAdherentAndMonth(Adherent $adherent, \Datetime $datetime): ?array
    {
        $dateFormat = 'Y-m-d\TH:i:s\Z';
        $carbon = Carbon::instance($datetime);
        $firstDayOfMonth = $carbon->startOfMonth()->format($dateFormat);
        $lastDayOfMonth = $carbon->lastOfMonth()->format($dateFormat);

        $filter = 'CustomerAccount eq \'' . $adherent->getCustomerAccount() . '\' and InvoiceDate ge ' .
            $firstDayOfMonth . ' and InvoiceDate le ' . $lastDayOfMonth;

        return $this->getRequest($filter)->execute();
    }

    /**
     * @param string $filter
     *
     * @return Request
     * @throws \InvalidArgumentException
     */
    private function getRequest(string $filter): Request
    {
        return $this
            ->createQueryBuilder()
            ->addCustomQuery(
                '$filter',
                $filter
            )
            ->addCustomQuery(
                '$orderby',
                'InvoiceId desc'
            )
            ->getRequest();
    }
}
