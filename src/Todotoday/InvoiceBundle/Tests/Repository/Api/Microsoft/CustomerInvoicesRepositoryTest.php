<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 15/05/17
 * Time: 16:11
 */

namespace Todotoday\InvoiceBundle\Tests\Repository\Api\Microsoft;

use Actiane\ApiConnectorBundle\Tests\Repository\Api\AbstractRepositoryTestCase;

class CustomerInvoicesRepositoryTest extends AbstractRepositoryTestCase
{
    /**
     * Do getRepositoryId
     *
     * @return string
     */
    protected function getRepositoryId(): string
    {
        return 'todotoday.invoice.repository.api.customer_invoice';
    }
}
