<?php declare(strict_types=1);

namespace Todotoday\InvoiceBundle\Tests\Services;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAccountData;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyData;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\InvoiceBundle\Services\CustomerInvoicesManager;

/**
 * Class CustomerInvoicesManagerTest
 * @package Todotoday\InvoiceBundle\Tests\Services
 */
class CustomerInvoicesManagerTest extends WebTestCase
{
    /**
     * @small
     */
    public function testGetAllMonthsInvoicesByAdherent(): void
    {
        $adherent = new Adherent();
        $adherent->setCustomerAccount('0000001');

        $demo = self::getFRR()->getReference('agency_demo0');

        $invoiceManager = $this->getContainer()->get('todotoday.invoice.invoice_manager');
        if ($invoicesByMonth = $invoiceManager->getAllMonthsInvoicesByAdherent($adherent, $demo)) {
            static::assertNotNull($invoicesByMonth);
        } else {
            self::assertNull($invoicesByMonth);
        }
    }

//    /**
//     * @small
//     */
//    public function testGetPdfFileContentByAdherentAndMonth(): void
//    {
//        $adherent = new Adherent();
//        $adherent->setCustomerAccount('0000001');
//        $agency = new Agency();
//        $agency->setCurrency('EUR');
//
//        $monthYear = (new \Datetime())
//            ->setDate(2017, 2, 1)
//            ->format(CustomerInvoicesManager::INVOICE_DATE_FORMAT);
//
//        $invoiceManager = $this->getContainer()->get('todotoday.invoice.invoice_manager');
//        /*$pdfFileContent = $invoiceManager->getPdfFileContentByAdherentAndMonth($adherent, $agency, $monthYear);
//
//        $this->assertNotEmpty($pdfFileContent);*/
//    }

    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAgencyData::class,
            LoadAccountData::class,
        );
    }
}
