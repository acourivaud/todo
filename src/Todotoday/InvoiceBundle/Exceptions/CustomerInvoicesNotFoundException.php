<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 11/05/17
 * Time: 14:14
 */

namespace Todotoday\InvoiceBundle\Exceptions;

use Throwable;

/**
 * Class CustomerInvoicesInvalidDateException
 * @package Todotoday\InvoiceBundle\Exceptions
 */
class CustomerInvoicesNotFoundException extends \Exception
{
    /**
     * CustomerInvoicesNotFoundException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = 'Customer Invoices not found', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
