<?php declare(strict_types=1);

namespace Todotoday\InvoiceBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class TodotodayInvoiceBundle
 * @package Todotoday\InvoiceBundle
 */
class TodotodayInvoiceBundle extends Bundle
{
}
