<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 06/03/17
 * Time: 11:56
 */

namespace Todotoday\InvoiceBundle\Controller\Api;

use FOS\RestBundle\Controller\Annotations as FosRest;
use FOS\RestBundle\Request\ParamFetcher;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Todotoday\CoreBundle\Controller\Api\AbstractApiController;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Exceptions\AgencyMissingException;

/**
 * Class InvoiceApiController
 * @package Todotoday\InvoiceBundle\Controller\Api
 * @FosRest\NamePrefix("todotoday.invoice.api.invoice.")
 * @FosRest\Prefix(value="/invoice")
 */
class InvoiceApiController extends AbstractApiController
{
    /**
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Retrieve invoice for current user",
     *     description="Retrieve invoice for current user",
     *     output={
     *           "class"="Todotoday\InvoiceBundle\Entity\Api\Microsoft\CustomerInvoices",
     *           "groups"={"Default"}
     *     },
     *     views={"Invoice"},
     *     section="Invoice"
     * )
     *
     * @FosRest\Route("", options={"expose"=true})
     * @FosRest\QueryParam(
     *     name="fields",
     *     requirements="((Default),?)*",
     *     default="",
     *     description="Fields you need"
     * )
     *
     * @param ParamFetcher $paramFetcher
     *
     * @param Request      $request
     *
     * @return Response
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingException
     * @throws \LogicException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \UnexpectedValueException
     * @throws \InvalidArgumentException
     */
    public function getAllAction(ParamFetcher $paramFetcher, Request $request): Response
    {
        if (!$this->isGranted('VIEW', $this)) {
            throw $this->createAccessDeniedException();
        }

        $agency = $this->getAgency();
        $adherent = $this->getUser();

        $invoiceManager = $this->get('todotoday.invoice.invoice_manager');
        $invoices = $invoiceManager->getAllMonthsInvoicesByAdherent($adherent, $agency);

        $view = $this->view($invoices);

        return $this->handleView($view);
    }

    /**
     * Get the formType for the current entity class link to the controller
     *
     * @return string
     */
    protected function getFormType(): string
    {
        return '';
    }

    /**
     * Get the serivce name from custum repository's entity
     *
     * @return string
     */
    protected function getServiceRepository(): string
    {
        return 'todotoday.invoice.repository.api.customer_invoice';
    }

    /**
     * @return null|Agency
     * @throws \UnexpectedValueException
     * @throws AgencyMissingException
     */
    private function getAgency(): ?Agency
    {
        if (!$agency = $this->get('todotoday.core.domain_context.api')->getAgency()) {
            throw new AgencyMissingException('Agency is missing');
        }

        return $agency;
    }
}
