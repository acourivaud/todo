<?php declare(strict_types=1);

namespace Todotoday\InvoiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class InvoiceController
 *
 * @package Todotoday\InvoiceBundle\Controller
 * @Route("/invoice")
 */
class InvoiceController extends Controller
{
    /**
     * @Route("/", name="invoice_index", options={"expose"=true})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     *
     */
    public function indexAction(Request $request): Response
    {
        if (!$this->isGranted('ROLE_INVOICE')) {
            throw $this->createAccessDeniedException();
        }

        $agency = $this->getAgency();

//        $locale = $request->getLocale();
        /** @var Adherent $adherent */
        $adherent = $this->getUser();

        $invoiceManager = $this->get('todotoday.invoice.invoice_manager');
        $months = $invoiceManager->getAllMonthsInvoicesByAdherent($adherent, $agency);

        return $this->render(
            'invoice/invoice.html.twig',
            array(
                'currency' => $agency->getCurrency(),
                'months' => $months,
            )
        );
    }

    /**
     * @Route("/{monthYear}", name="invoice_month", options={"expose"=true})
     *
     * @param Request $request
     * @param string  $monthYear
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Lexik\Bundle\JWTAuthenticationBundle\Exception\InvalidTokenException
     * @throws \Lexik\Bundle\JWTAuthenticationBundle\Exception\ExpiredTokenException
     * @throws \Twig_Error
     * @throws \RuntimeException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     * @throws \Todotoday\InvoiceBundle\Exceptions\CustomerInvoicesNotFoundException
     */
    public function specificMonthAction(Request $request, string $monthYear): Response
    {
        $adherent = null;

        if (!$this->isGranted('ROLE_INVOICE')) {
            if ($jwt = $request->query->get('jwt')) {
                $request->headers->add(['Authorization' => 'Bearer ' . $jwt]);
            }
            $jwtAuthentificator = $this->get('lexik_jwt_authentication.jwt_token_authenticator');
            $jwtManager = $this->get('lexik_jwt_authentication.jwt_manager');
            $preAuthToken = $jwtAuthentificator->getCredentials($request);

            if ($preAuthToken) {
                $usernameFromToken = $jwtManager->decode($preAuthToken)['username'];
                $adherentRepo = $this->getDoctrine()->getManager()->getRepository('TodotodayAccountBundle:Adherent');
                $adherent = $adherentRepo->findOneBy(array('username' => $usernameFromToken));
            }

            if (!$adherent) {
                throw $this->createAccessDeniedException();
            }
        }

        $agency = $this->getAgency();

        if (!$adherent) {// If the adherent coming from the website (and not from Appli Noosphere)
            /** @var Adherent $adherent */
            $adherent = $this->getUser();
        }

        $invoiceManager = $this->get('todotoday.invoice.invoice_manager');
        $fileName = $invoiceManager->getFileName($adherent, $agency, $monthYear);

        return new Response(
            $invoiceManager->getPdfFileContentByAdherentAndMonth($adherent, $agency, $monthYear),
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename="' . $fileName . '"',
            )
        );
    }

    /**
     * Do getAgency
     *
     * @return Agency
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \UnexpectedValueException
     * @throws NotFoundHttpException
     */
    protected function getAgency(): Agency
    {
        if (!$agency = $this->get('todotoday.core.domain_context')->getCurrentContext()->getAgency()) {
            throw $this->createNotFoundException();
        }

        return $agency;
    }
}
