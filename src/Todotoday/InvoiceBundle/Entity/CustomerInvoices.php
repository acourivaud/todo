<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 15/05/17
 * Time: 15:49
 */

namespace Todotoday\InvoiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * CustomerInvoices
 *
 * @ORM\Table(name="customer_invoices", schema="invoice")
 * @ORM\Entity(repositoryClass="Todotoday\InvoiceBundle\Repository\CustomerInvoicesRepository")
 * @Serializer\ExclusionPolicy("all")
 */
class CustomerInvoices
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @Serializer\Expose()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     * @Serializer\Expose()
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="company_reg_com_fr", type="string", length=255, nullable=true)
     * @Serializer\Expose()
     */
    protected $companyRegComFr;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     * @Serializer\Expose()
     */
    protected $address;

    /**
     * @var string
     *
     * @ORM\Column(name="primary_contact_fax", type="string", length=255, nullable=true)
     * @Serializer\Expose()
     */
    protected $primaryContactFax;

    /**
     * @var string
     *
     * @ORM\Column(name="customer_account", type="string", length=255, nullable=true)
     * @Serializer\Expose()
     */
    protected $customerAccount;

    /**
     * @var integer
     *
     * @ORM\Column(name="line_tax", type="integer", nullable=true)
     * @Serializer\Expose()
     */
    protected $lineTax;

    /**
     * @var integer
     *
     * @ORM\Column(name="total_invoice_amount", type="integer", nullable=true)
     * @Serializer\Expose()
     */
    protected $totalInvoiceAmount;

    /**
     * @var string
     *
     * @ORM\Column(name="legal_form_fr", type="string", length=255, nullable=true)
     * @Serializer\Expose()
     */
    protected $legalFormFR;

    /**
     * @var string
     *
     * @ORM\Column(name="currency_code", type="string", length=255, nullable=true)
     * @Serializer\Expose()
     */
    protected $currencyCode;

    /**
     * @var string
     *
     * @ORM\Column(name="adherent_num", type="string", length=255, nullable=true)
     * @Serializer\Expose()
     */
    protected $adherentNum;

    /**
     * @var string
     *
     * @ORM\Column(name="vat_nm", type="string", length=255, nullable=true)
     * @Serializer\Expose()
     */
    protected $vatNm;

    /**
     * @var integer
     *
     * @ORM\Column(name="line_amount", type="integer", nullable=true)
     * @Serializer\Expose()
     */
    protected $lineAmount;

    /**
     * @var string
     *
     * @ORM\Column(name="co_reg_num", type="string", length=255, nullable=true)
     * @Serializer\Expose()
     */
    protected $coRegNum;

    /**
     * @var integer
     *
     * @ORM\Column(name="sales_price", type="integer", nullable=true)
     * @Serializer\Expose()
     */
    protected $salesPrice;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="valid_to", type="datetimetz", nullable=true)
     * @Serializer\Expose()
     */
    protected $validTo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="due_date", type="datetimetz", nullable=true)
     * @Serializer\Expose()
     */
    protected $dueDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="total_tax_amount", type="integer", nullable=true)
     * @Serializer\Expose()
     */
    protected $totalTaxAmount;

    /**
     * @var string
     *
     * @ORM\Column(name="item_id", type="string", length=255, nullable=true)
     * @Serializer\Expose()
     */
    protected $itemId;

    /**
     * @var string
     *
     * @ORM\Column(name="primary_contact_phone", type="string", length=255, nullable=true)
     * @Serializer\Expose()
     */
    protected $primaryContactPhone;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="valid_from", type="datetimetz", nullable=true)
     * @Serializer\Expose()
     */
    protected $validFrom;

    /**
     * @var string
     *
     * @ORM\Column(name="invoice_id", type="string", length=255, nullable=true)
     * @Serializer\Expose()
     */
    protected $invoiceId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="invoice_date", type="datetimetz", nullable=true)
     * @Serializer\Expose()
     */
    protected $invoiceDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="line_qty", type="integer", nullable=true)
     * @Serializer\Expose()
     */
    protected $lineQty;

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return CustomerInvoices
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getCompanyRegComFr(): ?string
    {
        return $this->companyRegComFr;
    }

    /**
     * @param string $companyRegComFr
     *
     * @return CustomerInvoices
     */
    public function setCompanyRegComFr(string $companyRegComFr): self
    {
        $this->companyRegComFr = $companyRegComFr;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string $address
     *
     * @return CustomerInvoices
     */
    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return string
     */
    public function getPrimaryContactFax(): ?string
    {
        return $this->primaryContactFax;
    }

    /**
     * @param string $primaryContactFax
     *
     * @return CustomerInvoices
     */
    public function setPrimaryContactFax(string $primaryContactFax): self
    {
        $this->primaryContactFax = $primaryContactFax;

        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerAccount(): ?string
    {
        return $this->customerAccount;
    }

    /**
     * @param string $customerAccount
     *
     * @return CustomerInvoices
     */
    public function setCustomerAccount(string $customerAccount): self
    {
        $this->customerAccount = $customerAccount;

        return $this;
    }

    /**
     * @return int
     */
    public function getLineTax(): ?int
    {
        return $this->lineTax;
    }

    /**
     * @param int $lineTax
     *
     * @return CustomerInvoices
     */
    public function setLineTax(int $lineTax): self
    {
        $this->lineTax = $lineTax;

        return $this;
    }

    /**
     * @return int
     */
    public function getTotalInvoiceAmount(): ?int
    {
        return $this->totalInvoiceAmount;
    }

    /**
     * @param int $totalInvoiceAmount
     *
     * @return CustomerInvoices
     */
    public function setTotalInvoiceAmount(int $totalInvoiceAmount): self
    {
        $this->totalInvoiceAmount = $totalInvoiceAmount;

        return $this;
    }

    /**
     * @return string
     */
    public function getLegalFormFR(): ?string
    {
        return $this->legalFormFR;
    }

    /**
     * @param string $legalFormFR
     *
     * @return CustomerInvoices
     */
    public function setLegalFormFR(string $legalFormFR): self
    {
        $this->legalFormFR = $legalFormFR;

        return $this;
    }

    /**
     * @return string
     */
    public function getCurrencyCode(): ?string
    {
        return $this->currencyCode;
    }

    /**
     * @param string $currencyCode
     *
     * @return CustomerInvoices
     */
    public function setCurrencyCode(string $currencyCode): self
    {
        $this->currencyCode = $currencyCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getAdherentNum(): ?string
    {
        return $this->adherentNum;
    }

    /**
     * @param string $adherentNum
     *
     * @return CustomerInvoices
     */
    public function setAdherentNum(string $adherentNum): self
    {
        $this->adherentNum = $adherentNum;

        return $this;
    }

    /**
     * @return string
     */
    public function getVatNm(): ?string
    {
        return $this->vatNm;
    }

    /**
     * @param string $vatNm
     *
     * @return CustomerInvoices
     */
    public function setVatNm(string $vatNm): self
    {
        $this->vatNm = $vatNm;

        return $this;
    }

    /**
     * @return int
     */
    public function getLineAmount(): ?int
    {
        return $this->lineAmount;
    }

    /**
     * @param int $lineAmount
     *
     * @return CustomerInvoices
     */
    public function setLineAmount(int $lineAmount): self
    {
        $this->lineAmount = $lineAmount;

        return $this;
    }

    /**
     * @return string
     */
    public function getCoRegNum(): ?string
    {
        return $this->coRegNum;
    }

    /**
     * @param string $coRegNum
     *
     * @return CustomerInvoices
     */
    public function setCoRegNum(string $coRegNum): self
    {
        $this->coRegNum = $coRegNum;

        return $this;
    }

    /**
     * @return int
     */
    public function getSalesPrice(): ?int
    {
        return $this->salesPrice;
    }

    /**
     * @param int $salesPrice
     *
     * @return CustomerInvoices
     */
    public function setSalesPrice(int $salesPrice): self
    {
        $this->salesPrice = $salesPrice;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getValidTo(): ?\DateTime
    {
        return $this->validTo;
    }

    /**
     * @param \DateTime $validTo
     *
     * @return CustomerInvoices
     */
    public function setValidTo(\DateTime $validTo): self
    {
        $this->validTo = $validTo;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDueDate(): ?\DateTime
    {
        return $this->dueDate;
    }

    /**
     * @param \DateTime $dueDate
     *
     * @return CustomerInvoices
     */
    public function setDueDate(\DateTime $dueDate): self
    {
        $this->dueDate = $dueDate;

        return $this;
    }

    /**
     * @return int
     */
    public function getTotalTaxAmount(): ?int
    {
        return $this->totalTaxAmount;
    }

    /**
     * @param int $totalTaxAmount
     *
     * @return CustomerInvoices
     */
    public function setTotalTaxAmount(int $totalTaxAmount): self
    {
        $this->totalTaxAmount = $totalTaxAmount;

        return $this;
    }

    /**
     * @return string
     */
    public function getItemId(): ?string
    {
        return $this->itemId;
    }

    /**
     * @param string $itemId
     *
     * @return CustomerInvoices
     */
    public function setItemId(string $itemId): self
    {
        $this->itemId = $itemId;

        return $this;
    }

    /**
     * @return string
     */
    public function getPrimaryContactPhone(): ?string
    {
        return $this->primaryContactPhone;
    }

    /**
     * @param string $primaryContactPhone
     *
     * @return CustomerInvoices
     */
    public function setPrimaryContactPhone(string $primaryContactPhone): self
    {
        $this->primaryContactPhone = $primaryContactPhone;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getValidFrom(): ?\DateTime
    {
        return $this->validFrom;
    }

    /**
     * @param \DateTime $validFrom
     *
     * @return CustomerInvoices
     */
    public function setValidFrom(\DateTime $validFrom): self
    {
        $this->validFrom = $validFrom;

        return $this;
    }

    /**
     * @return string
     */
    public function getInvoiceId(): ?string
    {
        return $this->invoiceId;
    }

    /**
     * @param string $invoiceId
     *
     * @return CustomerInvoices
     */
    public function setInvoiceId(string $invoiceId): self
    {
        $this->invoiceId = $invoiceId;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getInvoiceDate(): ?\DateTime
    {
        return $this->invoiceDate;
    }

    /**
     * @param \DateTime $invoiceDate
     *
     * @return CustomerInvoices
     */
    public function setInvoiceDate(\DateTime $invoiceDate): self
    {
        $this->invoiceDate = $invoiceDate;

        return $this;
    }

    /**
     * @return int
     */
    public function getLineQty(): ?int
    {
        return $this->lineQty;
    }

    /**
     * @param int $lineQty
     *
     * @return CustomerInvoices
     */
    public function setLineQty(int $lineQty): self
    {
        $this->lineQty = $lineQty;

        return $this;
    }
}
