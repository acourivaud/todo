<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 15/05/17
 * Time: 15:49
 */

namespace Todotoday\InvoiceBundle\Entity\Api\Microsoft;

use Actiane\ApiConnectorBundle\Lib\EntityProxyTrait;
use Actiane\ApiConnectorBundle\Lib\Mapping as APIConnector;
use JMS\Serializer\Annotation as Serializer;

/**
 * @SuppressWarnings(PHPMD)
 * @APIConnector\Entity(path="CustomerInvoices", repositoryId="todotoday.invoice.repository.api.customer_invoice")
 * Class CustomerInvoices
 * @package Todotoday\InvoiceBundle\Entity\Api\Microsoft
 * @Serializer\ExclusionPolicy("all")
 */
class CustomerInvoices
{
    use EntityProxyTrait;

    /**
     * @var float
     *
     * @APIConnector\Column(name="LineTax", type="float")
     * @Serializer\Expose()
     * @Serializer\Type("float")
     */
    protected $lineTax;

    /**
     * @var string
     *
     * @APIConnector\Column(name="Address", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $address;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ItemId", type="string")
     * @APIConnector\Id()
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $itemId;

    /**
     * @var float
     *
     * @APIConnector\Column(name="TotalTaxAmount", type="float")
     * @Serializer\Expose()
     * @Serializer\Type("float")
     */
    protected $totalTaxAmount;

    /**
     * @var string
     *
     * @APIConnector\Column(name="PrimaryContactPhone", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $primaryContactPhone;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CoRegNum", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $coRegNum;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="InvoiceDate", type="datetime")
     * @Serializer\Expose()
     * @Serializer\Type("DateTime")
     */
    protected $invoiceDate;

    /**
     * @var string
     *
     * @APIConnector\Column(name="LegalFormFR", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $legalFormFr;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="ValidFrom", type="datetimez")
     * @Serializer\Expose()
     * @Serializer\Type("DateTime")
     */
    protected $validFrom;

    /**
     * @var float
     *
     * @APIConnector\Column(name="LineQty", type="float")
     * @APIConnector\Id()
     * @Serializer\Expose()
     * @Serializer\Type("float")
     */
    protected $lineQty;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CurrencyCode", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $currencyCode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="VATNm", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $vatNm;

    /**
     * @var string
     *
     * @APIConnector\Column(name="AdherentNum", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $adherentNum;

    /**
     * @var string
     *
     * @APIConnector\Column(name="InvoiceId", type="string")
     * @APIConnector\Id()
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $invoiceId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CustomerAccount", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $customerAccount;

    /**
     * @var string
     *
     * @APIConnector\Column(name="PrimaryContactFax", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $primaryContactFax;

    /**
     * @var float
     *
     * @APIConnector\Column(name="SalesPrice", type="float")
     * @Serializer\Expose()
     * @Serializer\Type("float")
     */
    protected $salesPrice;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="DueDate", type="datetimez")
     * @Serializer\Expose()
     * @Serializer\Type("DateTime")
     */
    protected $dueDate;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="ValidTo", type="datetimez")
     * @Serializer\Expose()
     * @Serializer\Type("DateTime")
     */
    protected $validTo;

    /**
     * @var string
     *
     * @APIConnector\Column(name="Name", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $name;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CompanyRegComFR", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $companyRegComFr;

    /**
     * @var float
     *
     * @APIConnector\Column(name="LineAmount", type="float")
     * @Serializer\Expose()
     * @Serializer\Type("float")
     */
    protected $lineAmount;

    /**
     * @var float
     *
     * @APIConnector\Column(name="TotalInvoiceAmount", type="float")
     * @Serializer\Expose()
     * @Serializer\Type("float")
     */
    protected $totalInvoiceAmount;

    /**
     * @return float
     */
    public function getLineTax(): ?float
    {
        return $this->lineTax;
    }

    /**
     * @param float $lineTax
     *
     * @return CustomerInvoices
     */
    public function setLineTax(?float $lineTax): CustomerInvoices
    {
        return $this->setTrigger('lineTax', $lineTax);
    }

    /**
     * @return string
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string $address
     *
     * @return CustomerInvoices
     */
    public function setAddress(?string $address): CustomerInvoices
    {
        return $this->setTrigger('address', $address);
    }

    /**
     * @return string
     */
    public function getItemId(): ?string
    {
        return $this->itemId;
    }

    /**
     * @param string $itemId
     *
     * @return CustomerInvoices
     */
    public function setItemId(?string $itemId): CustomerInvoices
    {
        return $this->setTrigger('itemId', $itemId);
    }

    /**
     * @return float
     */
    public function getTotalTaxAmount(): ?float
    {
        return $this->totalTaxAmount;
    }

    /**
     * @param float $totalTaxAmount
     *
     * @return CustomerInvoices
     */
    public function setTotalTaxAmount(?float $totalTaxAmount): CustomerInvoices
    {
        return $this->setTrigger('totalTaxAmount', $totalTaxAmount);
    }

    /**
     * @return string
     */
    public function getPrimaryContactPhone(): ?string
    {
        return $this->primaryContactPhone;
    }

    /**
     * @param string $primaryContactPhone
     *
     * @return CustomerInvoices
     */
    public function setPrimaryContactPhone(?string $primaryContactPhone): CustomerInvoices
    {
        return $this->setTrigger('primaryContactPhone', $primaryContactPhone);
    }

    /**
     * @return string
     */
    public function getCoRegNum(): ?string
    {
        return $this->coRegNum;
    }

    /**
     * @param string $coRegNum
     *
     * @return CustomerInvoices
     */
    public function setCoRegNum(?string $coRegNum): CustomerInvoices
    {
        return $this->setTrigger('coRegNum', $coRegNum);
    }

    /**
     * @return \DateTime
     */
    public function getInvoiceDate(): ?\DateTime
    {
        return $this->invoiceDate;
    }

    /**
     * @param \DateTime $invoiceDate
     *
     * @return CustomerInvoices
     */
    public function setInvoiceDate(?\DateTime $invoiceDate): CustomerInvoices
    {
        return $this->setTrigger('invoiceDate', $invoiceDate);
    }

    /**
     * @return string
     */
    public function getLegalFormFr(): ?string
    {
        return $this->legalFormFr;
    }

    /**
     * @param string $legalFormFr
     *
     * @return CustomerInvoices
     */
    public function setLegalFormFr(?string $legalFormFr): CustomerInvoices
    {
        return $this->setTrigger('legalFormFr', $legalFormFr);
    }

    /**
     * @return \DateTime
     */
    public function getValidFrom(): ?\DateTime
    {
        return $this->validFrom;
    }

    /**
     * @param \DateTime $validFrom
     *
     * @return CustomerInvoices
     */
    public function setValidFrom(?\DateTime $validFrom): CustomerInvoices
    {
        return $this->setTrigger('validFrom', $validFrom);
    }

    /**
     * @return float
     */
    public function getLineQty(): ?float
    {
        return $this->lineQty;
    }

    /**
     * @param float $lineQty
     *
     * @return CustomerInvoices
     */
    public function setLineQty(?float $lineQty): CustomerInvoices
    {
        return $this->setTrigger('lineQty', $lineQty);
    }

    /**
     * @return string
     */
    public function getCurrencyCode(): ?string
    {
        return $this->currencyCode;
    }

    /**
     * @param string $currencyCode
     *
     * @return CustomerInvoices
     */
    public function setCurrencyCode(?string $currencyCode): CustomerInvoices
    {
        return $this->setTrigger('currencyCode', $currencyCode);
    }

    /**
     * @return string
     */
    public function getVatNm(): ?string
    {
        return $this->vatNm;
    }

    /**
     * @param string $vatNm
     *
     * @return CustomerInvoices
     */
    public function setVatNm(?string $vatNm): CustomerInvoices
    {
        return $this->setTrigger('vatNm', $vatNm);
    }

    /**
     * @return string
     */
    public function getAdherentNum(): ?string
    {
        return $this->adherentNum;
    }

    /**
     * @param string $adherentNum
     *
     * @return CustomerInvoices
     */
    public function setAdherentNum(?string $adherentNum): CustomerInvoices
    {
        return $this->setTrigger('adherentNum', $adherentNum);
    }

    /**
     * @return string
     */
    public function getInvoiceId(): ?string
    {
        return $this->invoiceId;
    }

    /**
     * @param string $invoiceId
     *
     * @return CustomerInvoices
     */
    public function setInvoiceId(?string $invoiceId): CustomerInvoices
    {
        return $this->setTrigger('invoiceId', $invoiceId);
    }

    /**
     * @return string
     */
    public function getCustomerAccount(): ?string
    {
        return $this->customerAccount;
    }

    /**
     * @param string $customerAccount
     *
     * @return CustomerInvoices
     */
    public function setCustomerAccount(?string $customerAccount): CustomerInvoices
    {
        return $this->setTrigger('customerAccount', $customerAccount);
    }

    /**
     * @return string
     */
    public function getPrimaryContactFax(): ?string
    {
        return $this->primaryContactFax;
    }

    /**
     * @param string $primaryContactFax
     *
     * @return CustomerInvoices
     */
    public function setPrimaryContactFax(?string $primaryContactFax): CustomerInvoices
    {
        return $this->setTrigger('primaryContactFax', $primaryContactFax);
    }

    /**
     * @return float
     */
    public function getSalesPrice(): ?float
    {
        return $this->salesPrice;
    }

    /**
     * @param float $salesPrice
     *
     * @return CustomerInvoices
     */
    public function setSalesPrice(?float $salesPrice): CustomerInvoices
    {
        return $this->setTrigger('salesPrice', $salesPrice);
    }

    /**
     * @return \DateTime
     */
    public function getDueDate(): ?\DateTime
    {
        return $this->dueDate;
    }

    /**
     * @param \DateTime $dueDate
     *
     * @return CustomerInvoices
     */
    public function setDueDate(?\DateTime $dueDate): CustomerInvoices
    {
        return $this->setTrigger('dueDate', $dueDate);
    }

    /**
     * @return \DateTime
     */
    public function getValidTo(): ?\DateTime
    {
        return $this->validTo;
    }

    /**
     * @param \DateTime $validTo
     *
     * @return CustomerInvoices
     */
    public function setValidTo(?\DateTime $validTo): CustomerInvoices
    {
        return $this->setTrigger('validTo', $validTo);
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return CustomerInvoices
     */
    public function setName(?string $name): CustomerInvoices
    {
        return $this->setTrigger('name', $name);
    }

    /**
     * @return string
     */
    public function getCompanyRegComFr(): ?string
    {
        return $this->companyRegComFr;
    }

    /**
     * @param string $companyRegComFr
     *
     * @return CustomerInvoices
     */
    public function setCompanyRegComFr(?string $companyRegComFr): CustomerInvoices
    {
        return $this->setTrigger('companyRegComFr', $companyRegComFr);
    }

    /**
     * @return float
     */
    public function getLineAmount(): ?float
    {
        return $this->lineAmount;
    }

    /**
     * @param float $lineAmount
     *
     * @return CustomerInvoices
     */
    public function setLineAmount(?float $lineAmount): CustomerInvoices
    {
        return $this->setTrigger('lineAmount', $lineAmount);
    }

    /**
     * @return float
     */
    public function getTotalInvoiceAmount(): ?float
    {
        return $this->totalInvoiceAmount;
    }

    /**
     * @param float $totalInvoiceAmount
     *
     * @return CustomerInvoices
     */
    public function setTotalInvoiceAmount(?float $totalInvoiceAmount): CustomerInvoices
    {
        return $this->setTrigger('totalInvoiceAmount', $totalInvoiceAmount);
    }
}
