<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 13/01/2017
 * Time: 19:17
 */

namespace Todotoday\InvoiceBundle\Services;

use Actiane\PaymentBundle\Enum\PaymentTypeEnum;
use Knp\Bundle\SnappyBundle\Snappy\LoggableGenerator;
use Knp\Snappy\Pdf;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Services\UrlGenerator;
use Todotoday\InvoiceBundle\Entity\Api\Microsoft\CustomerInvoices;
use Todotoday\InvoiceBundle\Exceptions\CustomerInvoicesNotFoundException;
use Todotoday\InvoiceBundle\Repository\Api\Microsoft\CustomerInvoicesRepository;

/**
 * Class OperationStatementManager
 *
 * @package Todotoday\InvoiceBundle\Services
 */
class CustomerInvoicesManager
{
    /**
     * ORDER_ASC
     */
    const ORDER_ASC = 'asc';
    /**
     * ORDER_DESC
     */
    const ORDER_DESC = 'desc';
    /**
     * INVOICE_DATE
     */
    const INVOICE_DATE = 'invoice_date';
    /**
     * INVOICE_DATE_FORMAT
     */
    const INVOICE_DATE_FORMAT = 'Y-m';
    /**
     * TOTAL_INVOICE_AMOUNT
     */
    const TOTAL_INVOICE_AMOUNT = 'total_invoice_amount';
    /**
     * INVOICE_MONTH_URL
     */
    const INVOICE_MONTH_URL = 'invoice_month_url';

    /**
     * @var TwigEngine
     */
    private $twigEngine;

    /**
     * @var LoggableGenerator
     */
    private $loggableGenerator;

    /**
     * @var CustomerInvoicesRepository
     */
    private $apiRepository;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var UrlGenerator
     */
    private $urlGenerator;

    /**
     * CustomerInvoicesManager constructor.
     *
     * @param TwigEngine                 $twigEngine
     * @param Pdf                        $loggableGenerator
     * @param CustomerInvoicesRepository $apiRepository
     * @param Router                     $router
     * @param UrlGenerator               $urlGenerator
     *
     */
    public function __construct(
        TwigEngine $twigEngine,
        Pdf $loggableGenerator,
        CustomerInvoicesRepository $apiRepository,
        Router $router,
        UrlGenerator $urlGenerator
    ) {
        $this->twigEngine = $twigEngine;
        $this->loggableGenerator = $loggableGenerator;
        $this->apiRepository = $apiRepository;
        $this->router = $router;
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * @param Adherent $adherent
     * @param Agency   $agency
     * @param string   $order
     *
     * @return array|null
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     */
    public function getAllMonthsInvoicesByAdherent(
        Adherent $adherent,
        Agency $agency,
        string $order = CustomerInvoicesManager::ORDER_DESC
    ): ?array {
        $invoices = $this->getAllInvoicesByAdherent($adherent);

        $invoicesByMonth = $this->getInvoicesMonthArray($invoices, $agency);
        $invoicesByMonthOrdered = $this->orderArrayByKey($invoicesByMonth, $order);

        return $invoicesByMonthOrdered;
    }

    /**
     * @param Adherent $adherent
     * @param Agency   $agency
     * @param string   $monthYear
     *
     * @return string
     * @throws \Twig_Error
     * @throws \RuntimeException
     * @throws \Todotoday\InvoiceBundle\Exceptions\CustomerInvoicesInvalidDateException
     * @throws \Todotoday\InvoiceBundle\Exceptions\CustomerInvoicesNotFoundException
     */
    public function getPdfFileContentByAdherentAndMonth(Adherent $adherent, Agency $agency, string $monthYear): string
    {
        $datetime = new \DateTime($monthYear);
        $invoices = $this->getInvoiceByAdherentAndMonth($adherent, $datetime);

        return $this->loggableGenerator->getOutputFromHtml(
            $this->getTwigInvoiceTemplate($adherent, $agency, $invoices),
            $this->getInvoiceTemplateOptions()
        );
    }

    /**
     * @param Adherent $adherent
     * @param Agency   $agency
     * @param string   $monthYear
     *
     * @return string
     */
    public function getFileName(Adherent $adherent, Agency $agency, string $monthYear): string
    {
        return 'invoice-' . $agency->getSlug() . '-' . $adherent->getCustomerAccount() . '-' . $monthYear . '.pdf';
    }

    /**
     * @param Adherent  $adherent
     * @param \Datetime $datetime
     *
     * @return array|null
     * @throws CustomerInvoicesNotFoundException
     */
    public function getInvoiceByAdherentAndMonth(Adherent $adherent, \Datetime $datetime): ?array
    {
        $invoices = $this->apiRepository->findCustomerInvoicesByAdherentAndMonth($adherent, $datetime);

        if (!$invoices) {
            throw new CustomerInvoicesNotFoundException();
        }

        return $invoices;
    }

    /**
     * @param Adherent $adherent
     *
     * @return array|null
     */
    public function getAllInvoicesByAdherent(Adherent $adherent): ?array
    {
        return $this->apiRepository->findCustomerInvoicesByAdherent($adherent);
    }

    /**
     * Get all invoices classified by month for an Adherent
     *
     * @param array  $invoices
     * @param Agency $agency
     *
     * @return array|null
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     */
    private function getInvoicesMonthArray(?array $invoices, Agency $agency): ?array
    {
        if (!$invoices) {
            return null;
        }

        $invoicesByMonth = array();

        /** @var CustomerInvoices $invoice */
        foreach ($invoices as $invoice) {
            $invoiceDate = $invoice->getInvoiceDate();
            $invoiceMonthYear = $invoiceDate->format(static::INVOICE_DATE_FORMAT);

            if (!isset($invoicesByMonth[$invoiceMonthYear])) {
                $route = $this->router->generate(
                    'invoice_month',
                    array(
                        'monthYear' => $invoiceMonthYear,
                    )
                );
                $absoluteUrl = $this->urlGenerator->generateAbsoluteUrl($agency->getSlug(), $route);

                $invoicesByMonth[$invoiceMonthYear] = array(
                    static::INVOICE_DATE => $invoiceDate,
                    static::INVOICE_MONTH_URL => $absoluteUrl,
                    static::TOTAL_INVOICE_AMOUNT => 0,
                );
            }
            $invoicesByMonth[$invoiceMonthYear][static::TOTAL_INVOICE_AMOUNT] += $invoice->getLineAmount();
        }

        return $invoicesByMonth;
    }

    /**
     * @param array $invoices
     *
     * @return array
     */
    private function getHtAndVatByRates(array $invoices): array
    {
        $htAndVatByRates = array();

        /** @var CustomerInvoices $invoice */
        foreach ($invoices as $invoice) {
            if ($invoice->getLineTax() <= 0 || $invoice->getLineAmount() <= 0) {
                continue;
            }

            $rate = $invoice->getLineTax() / $invoice->getLineAmount() * 100;

            if (!isset($htAndVatByRates[$rate])) {
                $htAndVatByRates[$rate] = array(
                    'ht' => 0,
                    'vat' => 0,
                );
            }
            $htAndVatByRates[$rate]['ht'] += $invoice->getLineAmount();
            $htAndVatByRates[$rate]['vat'] += $invoice->getLineTax();
        }

        return $htAndVatByRates;
    }

    /**
     * @param array  $array
     * @param string $order
     *
     * @return array|null
     */
    private function orderArrayByKey(?array $array, string $order): ?array
    {
        if (!$array) {
            return null;
        }

        $arrayOrdered = $array;

        if ($order === static::ORDER_ASC) {
            ksort($arrayOrdered);
        } elseif ($order === static::ORDER_DESC) {
            krsort($arrayOrdered);
        }

        return $arrayOrdered;
    }

    /**
     * @param Adherent $adherent
     * @param Agency   $agency
     * @param array    $invoices
     *
     * @return string
     * @throws \RuntimeException
     */
    private function getTwigInvoiceTemplate(Adherent $adherent, Agency $agency, array $invoices): string
    {
        /** @var CustomerInvoices $firstInvoice */
        $firstInvoice = $invoices[0];

        return $this->twigEngine->render(
            '@TodotodayInvoice/Pdf/invoice.html.twig',
            array(
                'adherent' => $adherent,
                'payment_is_paypal' => $adherent->getPaymentTypeSelectedString() === PaymentTypeEnum::PAYPAL,
                'currency' => $agency->getCurrency(),
                'agency_data_area_id' => strtolower($agency->getDataAreaId()),
                'invoices' => $invoices,
                'refinvoice' => $firstInvoice,
                'total_invoice_amount' => $firstInvoice->getTotalInvoiceAmount(),
                'htAndVatByRates' => $this->getHtAndVatByRates($invoices),
            )
        );
    }

    /**
     * @return array
     * @throws \Twig_Error
     * @throws \RuntimeException
     */
    private function getInvoiceTemplateOptions(): array
    {
        return array(
            'header-html' => $this->getInvoiceHeader(),
            'footer-html' => $this->getInvoiceFooter(),
        );
    }

    /**
     * @return string
     * @throws \Twig_Error
     * @throws \RuntimeException
     */
    private function getInvoiceHeader(): string
    {
        return $this->twigEngine->render('TodotodayPaymentBundle:Pdf/footer:sepa_footer.html.twig');
    }

    /**
     * @return string
     * @throws \Twig_Error
     * @throws \RuntimeException
     */
    private function getInvoiceFooter(): string
    {
        return $this->twigEngine->render('TodotodayPaymentBundle:Pdf/footer:sepa_footer.html.twig');
    }
}
