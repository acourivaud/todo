<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 03/04/17
 * Time: 15:33
 */

namespace Todotoday\CatalogBundle\Exceptions;

use Actiane\ToolsBundle\Exceptions\PublishedMessageExceptionInterface;

/**
 * Class ProductNotFoundException
 * @package Todotoday\CatalogBundle\Exceptions
 */
class ProductNotFoundException extends \Exception implements PublishedMessageExceptionInterface
{
    /**
     * @return mixed
     */
    public function getMessages()
    {
        parent::getMessage();
    }

    /**
     * @return int
     */
    public function getCustomCode(): int
    {
        return 400;
    }
}
