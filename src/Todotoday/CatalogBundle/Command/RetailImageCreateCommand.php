<?php declare(strict_types=1);

namespace Todotoday\CatalogBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class RetailImageCreateCommand
 * @package Todotoday\CatalogBundle\Command
 */
class RetailImageCreateCommand extends ContainerAwareCommand
{

    const DEFAULT_BATCH_SIZE = 10;

    /**
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function configure(): void
    {
        $this
            ->setName('todotoday:catalog:retailimage')
            ->setDescription('Retrieve product images for catalog')
            ->addOption(
                'batch',
                'b',
                InputOption::VALUE_OPTIONAL,
                'The batch size to retrieve images (default : ' . self::DEFAULT_BATCH_SIZE . ')'
            )
            ->addOption(
                'force',
                'f',
                InputOption::VALUE_NONE,
                'Force retrieve all images regardless of the schedule configuration'
            );
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return void
     * @throws \InvalidArgumentException
     * @throws \LogicException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $batchOption = (int) $input->getOption('batch');
        $batchSize = $batchOption ? $batchOption : self::DEFAULT_BATCH_SIZE;
        $offset = 0;
        $totalImages = 0;
        $forceOption = $input->getOption('force') ? true : false;
        $imageManager = $this->getContainer()->get('todotoday_catalog.image_manager');

        if (!$imageManager->isTimeToImport() && !$forceOption) {
            $currentHour = date('H:00:00');
            $output->writeln('The current hour ' . $currentHour . ' is not the time to import images (see configuration)');

            return;
        }

        $path       = $imageManager->getCatalogProductsImagesServerPath();
        $tmpPath    = $imageManager->getCatalogProductsImagesTmpServerPath();
        $filesystem = new Filesystem();
        try {
            if (!$filesystem->exists($tmpPath)) {
                $filesystem->mkdir($tmpPath);
            }
        } catch (IOExceptionInterface $exception) {
            echo "An error occurred while creating your directory at " . $exception->getPath();
        }

        while (!empty($images = $imageManager->createAllCatalogProductsImages($batchSize, $offset))) {
            foreach ($images as $image) {
                $output->writeln($image . ' created');
                $totalImages++;
            }
            $output->writeln(count($images) . ' images created');
            $offset += $batchSize;
        }

        $output->writeln('Total : ' . $totalImages . ' images created in temp folder');

        $filesystem->rename($tmpPath, $path, true);

        $output->writeln('Rename temp folder (' . $tmpPath . ') to real folder (' . $path . ')');
    }
}
