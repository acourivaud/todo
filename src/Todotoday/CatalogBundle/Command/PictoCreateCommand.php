<?php declare(strict_types=1);

namespace Todotoday\CatalogBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class PictoCreateCommand
 * @package Todotoday\CatalogBundle\Command
 */
class PictoCreateCommand extends ContainerAwareCommand
{
    /**
     *
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function configure(): void
    {
        $this
            ->setName('picto:create')
            ->setDescription('Create picto for catalog');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return void
     * @throws \InvalidArgumentException
     * @throws \LogicException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $images = $this->getContainer()->get('todotoday_catalog.image_manager')->createAllCatalogCategoriesImages();
        foreach ($images as $image) {
            $output->writeln($image . ' created');
        }

        $output->writeln(count($images) . ' images created');
    }
}
