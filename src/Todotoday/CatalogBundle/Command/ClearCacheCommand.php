<?php declare(strict_types=1);

namespace Todotoday\CatalogBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class ClearCacheCommand
 * @package Todotoday\CatalogBundle\Command
 */
class ClearCacheCommand extends ContainerAwareCommand
{
    /**
     *
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function configure(): void
    {
        $this
            ->setName('todotoday:catalog:clearcache')
            ->setDescription('Clear cache of catalog');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return void
     * @throws \InvalidArgumentException
     * @throws \LogicException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $currentHour = date('H:00:00');
        $catalogManager = $this->getContainer()->get('todotoday_catalog.catalog_manager');
        $agencies = $this->getContainer()->get('doctrine.orm.entity_manager')->getRepository(Agency::class)
            ->getEnabledAgencies();

        if ($agencies) {
            foreach ($agencies as $agency) {
                if ($languages = $agency->getLanguages()) {
                    foreach ($languages as $language) {
                        if ($catalogManager->clearCacheCatalog(
                            $agency,
                            $language
                        )
                        ) {
                            $output->writeln(
                                $agency->getName()
                                . ' - '
                                . $language
                                . ' : cache cleared at '
                                . $currentHour
                            );
                        }
                    }
                }
            }
        }
    }
}
