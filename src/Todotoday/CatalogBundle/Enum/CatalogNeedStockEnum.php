<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 01/05/17
 * Time: 18:37
 */

namespace Todotoday\CatalogBundle\Enum;

use Actiane\ToolsBundle\Enum\AbstractEnum;

/**
 * Class CatalogNeedStockEnum
 * @package Todotoday\CatalogBundle\Enum
 */
class CatalogNeedStockEnum extends AbstractEnum
{
    const PMP = 'PMP';
    const PMP1 = 'PMP1';
    const PMP2 = 'PMP2';
    const PMP3 = 'PMP3';
}
