<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 01/05/17
 * Time: 18:37
 */

namespace Todotoday\CatalogBundle\Enum;

use Actiane\ToolsBundle\Enum\AbstractEnum;

/**
 * Class CatalogQuotationEnum
 * @package Todotoday\CatalogBundle\Enum
 */
class CatalogQuotationEnum extends AbstractEnum
{
    const DEVIS = 'devis';
}
