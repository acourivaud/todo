<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 01/05/17
 * Time: 18:39
 */

namespace Todotoday\CatalogBundle\Enum;

use Actiane\ToolsBundle\Enum\AbstractEnum;

/**
 * Class CatalogNeedBookingEnum
 * @package Todotoday\CatalogBundle\Enum
 */
class CatalogNeedBookingEnum extends AbstractEnum
{
    const SERVICERDV = 'ServiceRDV';
    const SERVICERDVUCFIRST = 'ServiceRdv';
    const SERVICE_UPPER_RDV = 'Service_RDV';
    const SERVICE_LOWER_RDV = 'Service_rdv';
    const SERVICE_UCFIRST_RDV = 'Service_Rdv';
}
