<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 01/05/17
 * Time: 18:23
 */

namespace Todotoday\CatalogBundle\Enum;

use Actiane\ToolsBundle\Enum\AbstractEnum;

/**
 * Class CatalogNeedDepositoryEnum
 * @package Todotoday\CatalogBundle\Enum
 */
class CatalogNeedDepositoryEnum extends AbstractEnum
{
    public const SERVICE = 'ServiceStk';
}
