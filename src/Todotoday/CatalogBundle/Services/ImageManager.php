<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 06/03/17
 * Time: 14:04
 */

namespace Todotoday\CatalogBundle\Services;

use Predis\Client;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Todotoday\CatalogBundle\Entity\Api\Microsoft\DocCategory;
use Todotoday\CatalogBundle\Repository\Api\Microsoft\DocCategoryRepository;
use Todotoday\CatalogBundle\Entity\Api\Microsoft\RetailImage;
use Todotoday\CatalogBundle\Repository\Api\Microsoft\RetailImageRepository;
use Todotoday\CoreBundle\Services\UrlGenerator;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class ImageManager
 *
 * @package Todotoday\CatalogBundle\Services
 */
class ImageManager
{
    public const PREFIX_MEDIA_URL = 'media';

    /**
     * CATALOG_PICTOS_FOLDER
     */
    public const CATALOG_PICTOS_FOLDER = '/images/catalog/pictos';

    /**
     * CATALOG_PRODUCTS_FOLDER
     */
    public const CATALOG_PRODUCTS_FOLDER = '/images/catalog/products';

    /**
     * CATALOG_PRODUCTS_TMP_FOLDER
     */
    public const CATALOG_PRODUCTS_TMP_FOLDER = '/images/catalog/tmp';

    /**
     * CATALOG_PICTOS_REDIS_PREFIX
     */
    public const CATALOG_PICTOS_REDIS_PREFIX = 'catalog_picto_';


    /**
     * @var string
     */
    private $webRoot;

    /**
     * @var DocCategoryRepository
     */
    private $docCategoryRepo;

    /**
     * @var Client
     */
    private $redis;

    /**
     * @var UrlGenerator
     */
    private $urlGenerator;

    /**
     * @var array
     */
    private $imgRetailImportRules;

    /**
     * @var RetailImageRepository
     */
    private $retailImageRepository;

    /**
     * ImageManager constructor.
     *
     * @param string                $rootDir
     * @param DocCategoryRepository $docCategoryRepo
     * @param Client                $redis
     * @param UrlGenerator          $urlGenerator
     * @param RetailImageRepository $retailImageRepository
     */
    public function __construct(
        string $rootDir,
        DocCategoryRepository $docCategoryRepo,
        Client $redis,
        UrlGenerator $urlGenerator,
        RetailImageRepository $retailImageRepository
    ) {
        $this->webRoot = dirname($rootDir) . '/web';
        $this->docCategoryRepo = $docCategoryRepo;
        $this->redis = $redis;
        $this->urlGenerator = $urlGenerator;
        $this->retailImageRepository = $retailImageRepository;
    }

    /**
     * @return string
     */
    public function getCatalogCategoriesImagesServerPath(): string
    {
        return $this->webRoot . static::CATALOG_PICTOS_FOLDER;
    }

    /**
     * @return string
     */
    public function getCatalogCategoriesImagesAbsoluteUrl(): string
    {
        return $this->urlGenerator->generateAbsoluteUrl(static::PREFIX_MEDIA_URL, static::CATALOG_PICTOS_FOLDER);
    }

    /**
     * @return string[]
     * @throws \InvalidArgumentException
     */
    public function createAllCatalogCategoriesImages(): ?array
    {
        $path = $this->getCatalogCategoriesImagesServerPath();
        $absoluteUrl = $this->getCatalogCategoriesImagesAbsoluteUrl();

        $imageCreated = [];
        /** @var DocCategory $catalogCategory */
        if ($images = $this->docCategoryRepo->findAll()) {
            foreach ($images as $catalogCategory) {
                // fix on reçoit une categorie vide dans le WS Microsoft
                if ($catalogCategory->getCategory()) {
                    $fileName = $catalogCategory->getCategory() . '.' . $catalogCategory->getFileType();

                    $this->cacheCatalogCategoryInRedis(
                        $catalogCategory->getCategory(),
                        $absoluteUrl . '/' . $fileName . '?t=' . $catalogCategory->getTimestamp()->getTimestamp()
                    );
                    $imageCreated[] = $this->createImageWithBinaries(
                        $path,
                        $fileName,
                        $catalogCategory->getFileContents()
                    );
                }
            }
        }

        return $imageCreated;
    }

    /**
     * @param string $limit
     * @param string $offset
     * @param string $date
     *
     * @return string[]
     * @throws \InvalidArgumentException
     */
    public function manualCreateAllCatalogCategoriesImages(
        string $limit = null,
        string $offset = null,
        string $date = null
    ): ?array {

        $path = $this->getCatalogCategoriesImagesServerPath();
        $absoluteUrl = $this->getCatalogCategoriesImagesAbsoluteUrl();

        $imageCreated = [];

        /** @var DocCategory $catalogCategory */
        if ($images = $this->docCategoryRepo->findAllByDate($limit, $offset, $date)) {
            foreach ($images as $catalogCategory) {
                // fix on reçoit une categorie vide dans le WS Microsoft
                if ($catalogCategory->getCategory()) {
                    $fileName = $catalogCategory->getCategory() . '.' . $catalogCategory->getFileType();

                    $this->cacheCatalogCategoryInRedis(
                        $catalogCategory->getCategory(),
                        $absoluteUrl . '/' . $fileName . '?t=' . $catalogCategory->getTimestamp()->getTimestamp()
                    );
                    $imageCreated[] = $this->createImageWithBinaries(
                        $path,
                        $fileName,
                        $catalogCategory->getFileContents()
                    );
                }
            }
        }

        return $imageCreated;
    }

    /**
     * @param string|null $category
     * @param string      $absoluteUrl
     */
    public function cacheCatalogCategoryInRedis(?string $category = null, string $absoluteUrl): void
    {
        $redisKey = static::CATALOG_PICTOS_REDIS_PREFIX . $category;
        $redisValue = $absoluteUrl;
        $this->redis->set($redisKey, $redisValue);
    }

    /**
     * @param string $path
     * @param string $fileName
     * @param string $binaries
     *
     * @return string
     */
    public function createImageWithBinaries(string $path, string $fileName, string $binaries): string
    {
        $filePath = $path . '/' . $fileName;
        file_put_contents($filePath, base64_decode($binaries));

        return $filePath;
    }

    /**
     * @return string
     */
    public function getCatalogProductsImagesServerPath(): string
    {
        return $this->webRoot . static::CATALOG_PRODUCTS_FOLDER;
    }

    /**
     * @return string
     */
    public function getCatalogProductsImagesTmpServerPath(): string
    {
        return $this->webRoot . static::CATALOG_PRODUCTS_TMP_FOLDER;
    }

    /**
     * @return string
     */
    public function getCatalogProductsImagesAbsoluteUrl(): string
    {
        return $this->urlGenerator->generateAbsoluteUrl(static::PREFIX_MEDIA_URL, static::CATALOG_PRODUCTS_FOLDER);
    }

    /**
     * @param int|null $limit
     * @param int|null $offset
     *
     * @return string[]
     */
    public function createAllCatalogProductsImages(int $limit = null, int $offset = null): ?array
    {
        $tmpPath    = $this->getCatalogProductsImagesTmpServerPath();
        $filesystem = new Filesystem();

        try {
            if (!$filesystem->exists($tmpPath)) {
                $filesystem->mkdir($tmpPath);
            }
        } catch (IOExceptionInterface $exception) {
            echo "An error occurred while creating your directory at " . $exception->getPath();
        }

        $imageCreated = [];
        $tdtdEntityFolderCreated = [];
        /** @var RetailImage $retailImage */
        if ($images = $this->retailImageRepository->findAll($limit, $offset)) {
            foreach ($images as $retailImage) {
                $tdtdEntityFolder = $tmpPath . '/' . $retailImage->getDataAreaId();
                if (!in_array($tdtdEntityFolder, $tdtdEntityFolderCreated)) {
                    $filesystem->mkdir($tdtdEntityFolder);
                }
                $fileName = $retailImage->getItemId() . '.jpg';
                $imageCreated[] = $this->createImageWithBinaries(
                    $tdtdEntityFolder,
                    $fileName,
                    $retailImage->getFileContents()
                );
            }
        }

        return $imageCreated;
    }

    /**
     * @param array $imgRetailImportRules
     *
     * @return ImageManager
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     */
    public function setImgRetailImportRules(array $imgRetailImportRules): self
    {
        $resolver = new OptionsResolver();
        $this->configureOptionsRules($resolver);
        foreach ($imgRetailImportRules as $rulesName => $rule) {
            $this->imgRetailImportRules[$rulesName] = $resolver->resolve($rule);
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isTimeToImport(): bool
    {
        if ($this->imgRetailImportRules) {
            $ruleKey     = 'default';
            $currentHour = date('H:00:00');

            if ($this->imgRetailImportRules[$ruleKey]['firstExecution'] == $currentHour
                || $this->imgRetailImportRules[$ruleKey]['secondExecution'] == $currentHour) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     */
    private function configureOptionsRules(OptionsResolver $resolver): void
    {
        $resolver->setRequired(
            [
                'firstExecution',
                'secondExecution',
            ]
        )
            ->setAllowedTypes('firstExecution', 'string')
            ->setAllowedTypes('secondExecution', 'string');
    }
}
