<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 06/03/17
 * Time: 14:04
 */

namespace Todotoday\CatalogBundle\Services;

use Actiane\ToolsBundle\Enum\LoggerEnum;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;
use Monolog\Logger;
use Predis\Client;
use Predis\Collection\Iterator\Keyspace;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Todotoday\CatalogBundle\Entity\Api\Microsoft\RetailCatalog;
use Todotoday\CatalogBundle\Exceptions\ProductNotFoundException;
use Todotoday\CatalogBundle\Repository\Api\Microsoft\RetailCatalogRepository;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\PluginBundle\Enum\PluginCategoryEnum;
use Todotoday\PluginBundle\Services\PluginManager;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class CatalogManager
 *
 * @package Todotoday\CatalogBundle\Services
 */
class CatalogManager
{
    /**
     * FAMILY_DELIMITER
     */
    public const FAMILY_DELIMITER = ';;';
    /**
     * FAMILY_ID
     */
    public const FAMILY_ID = '_family_id';
    /**
     * FAMILY_TO_ESCAPE
     */
    public const FAMILY_TO_ESCAPE = array(
        'en' => ['Family', 'Familly'],
        'fr' => ['Famille'],
    );
    /**
     * HIERARCHY_DELIMITER
     */
    public const HIERARCHY_DELIMITER = '||';
    /**
     * MAIN_FAMILIES
     */
    public const MAIN_FAMILIES = 'catalog_main_families';
    /**
     * ORDER_ID
     */
    public const ORDER_ID = '_order_id';
    /**
     * PICTOS_URL
     */
    public const PICTOS_URL = '_picto_url';
    /**
     * PICTO_DEFAULT
     */
    public const PICTO_DEFAULT = '';
    /**
     * IS_PLUGIN_FAMILY
     */
    public const IS_PLUGIN_FAMILY = '_is_plugin_family';
    /**
     * PLUGIN_SLUG
     */
    public const PLUGIN_SLUG = '_plugin_slug';
    /**
     * PLUGIN_SEARCH_MDI_ICON
     */
    public const PLUGIN_MDI_ICON = '_plugin_mdi_icon';
    /**
     * FAMILY_DELIMITER
     */
    public const PREFIX_DELIMITER = ' - ';
    /**
     * PRODUCT_FOLDER
     */
    public const PRODUCT_FOLDER = '_products';
    /**
     * NORMAL_CATALOG
     */
    public const NORMAL_CATALOG = 'catalog';
    /**
     * NON_ADHERENT_CATALOG
     */
    public const NON_ADHERENT_CATALOG = 'non_adherent_catalog';

    /**
     * @var RetailCatalogRepository
     */
    private $repo;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var AdapterInterface
     */
    private $cache;

    /**
     * @var Client Database Catalog
     */
    private $redis;

    /**
     * @var Client Database Framework
     */
    private $redisCacheCatalog;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var PluginManager
     */
    private $pluginManager;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var array
     */
    private $clearCacheRules;

    /**
     * @var ImageManager
     */
    private $imageManager;

    /**
     * CatalogManager constructor.
     *
     * @param RetailCatalogRepository $repo
     * @param Serializer              $serializer
     * @param AdapterInterface        $cache
     * @param Client                  $redis
     * @param Client                  $redisCacheCatalog
     * @param Logger                  $logger
     * @param PluginManager           $pluginManager
     * @param TranslatorInterface     $translator
     * @param ImageManager            $imageManager
     */
    public function __construct(
        RetailCatalogRepository $repo,
        Serializer $serializer,
        AdapterInterface $cache,
        Client $redis,
        Client $redisCacheCatalog,
        Logger $logger,
        PluginManager $pluginManager,
        TranslatorInterface $translator,
        ImageManager $imageManager
    ) {
        $this->repo = $repo;
        $this->serializer = $serializer;
        $this->cache = $cache;
        $this->redis = $redis;
        $this->redisCacheCatalog = $redisCacheCatalog;
        $this->logger = $logger;
        $this->pluginManager = $pluginManager;
        $this->translator = $translator;
        $this->imageManager = $imageManager;
    }

    /**
     * @param Agency        $agency
     * @param RetailCatalog $product
     * @param string        $lang
     *
     * @return null|RetailCatalog
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \InvalidArgumentException
     */
    public function cacheProduct(Agency $agency, RetailCatalog $product, string $lang = 'fr'): ?RetailCatalog
    {
        $productCached = $this->cache->getItem($this->getKey($agency, 'product_' . $product->getItemId(), $lang));

        if (!$productCached->isHit()) {
            $productCached->set($this->serializer->serialize($product, 'json'))
                ->expiresAfter(3600);
            $this->cache->save($productCached);
        }

        return $this->serializer->deserialize($productCached->get(), RetailCatalog::class, 'json');
    }

    /**
     * @param array  $products
     * @param string $lang
     *
     * @return array
     */
    public function cleanAndOrderCatalog(array $products, string $lang = 'fr'): array
    {
        /** @var RetailCatalog[] $productsOrdered */
        $productsOrdered = array();

        /** @var RetailCatalog $product */
        foreach ($products as $product) {
            try {
                if ($this->isProductValid($product)) {
                    $mainFamily = $this->getFamilyFromHierarchy($product, 0);
                    if (!$mainFamily) {
                        continue;
                    } elseif ($this->isSkipFirstFamily($mainFamily['name'], $lang)) {
                        $this->removeFirstLevelHierarchy($product);

                        if (!$mainFamily) {
                            continue;
                        }
                    }
                    $productsOrdered[] = $product;
                }
            } catch (\Exception $e) {
                $message = '[cleanAndOrderCatalog] - The product ' . $product->getNameTrans() . ' failed. ' . $e;
                $this->logger->log(LoggerEnum::CRITICAL, $message);
                continue;
            }
        }

        usort(
            $productsOrdered,
            function (RetailCatalog $product1, RetailCatalog $product2) {
                return strcmp($product1->getHierachyAndName(), $product2->getHierachyAndName());
            }
        );

        return $productsOrdered;
    }

    /**
     * @param Agency $agency
     * @param string $lang
     * @param string $type
     *
     * @return null|RetailCatalog[]
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \InvalidArgumentException
     */
    public function getAllProducts(
        Agency $agency,
        string $lang = 'fr',
        string $type = CatalogManager::NORMAL_CATALOG
    ): ?array {
        if (!\in_array($type, array(static::NORMAL_CATALOG, static::NON_ADHERENT_CATALOG))) {
            return null;
        }

        if (($type === static::NORMAL_CATALOG)
            && (!$result = $this->repo->findCatalogByAgencyAndLang($agency->getSlug(), $lang, $agency->isDigital()))
        ) {
            return null;
        } elseif (($type === static::NON_ADHERENT_CATALOG)
            && (!$result =
                $this->repo->findCatalogNonAdherentByAgencyAndLang($agency->getSlug(), $lang, $agency->isDigital()))
        ) {
            return null;
        }

        // remove duplicate content from catalog
        $result = array_unique($result);
        $imgProductFolder = $this->imageManager->getCatalogProductsImagesServerPath();
        $filesystem = new Filesystem();

        foreach ($result as $key => $product) {
            $this->cacheProduct($agency, $product, $lang);
            $tdtdEntity = strtolower(substr($product->getStore(), 0, 3));
            $imgFullPath = $imgProductFolder . '/' . $tdtdEntity . '/' . $product->getItemId() . '.jpg';
            /**
             * @todo  update getCatalogProductsImagesAbsoluteUrl to get URL of the img
             */
            $imgUrl = $this->imageManager->getCatalogProductsImagesAbsoluteUrl() . '/' . $tdtdEntity . '/' . $product->getItemId() . '.jpg';
            if ($filesystem->exists($imgFullPath)) {
                $product->setPicture($imgUrl);
            }
            $result[$key] = $product;
        }

        return $result;
    }

    /**
     * @param Agency $agency
     * @param string $lang
     * @param string $type
     *
     * @return array|null
     * @throws \Exception
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function getCatalog(
        Agency $agency,
        string $lang = 'fr',
        string $type = CatalogManager::NORMAL_CATALOG
    ): ?array {
        $catalogCached = $this->cache->getItem($this->getKey($agency, $type, $lang));
        if (!$catalogCached->isHit()) {
            if (!$result = $this->getAllProducts($agency, $lang, $type)) {
                return null;
            }
            $resultOrdered = $this->cleanAndOrderCatalog($result, $lang);
            $catalogCached->set($this->serializer->serialize($resultOrdered, 'json'));
            $this->cache->save($catalogCached);
            $this->setRedisMainFamilies($resultOrdered, $agency, $lang, $type);
        }

        $result = $this->serializer->deserialize($catalogCached->get(), 'array<' . RetailCatalog::class . '>', 'json');

        if (!$this->redis->exists($this->getKey($agency, $this->getMainFamiliesType($type), $lang))) {
            $resultOrdered = $this->cleanAndOrderCatalog($result, $lang);
            $this->setRedisMainFamilies($resultOrdered, $agency, $lang, $type);
        }

        $retailsCatalogFromPlugins = $this->pluginManager->launchPlugins(
            $agency,
            PluginCategoryEnum::CATALOG_PLUGIN,
            'retailCatalog'
        );

        return $this->getHierarchyTreeCatalog(array_merge($result, $retailsCatalogFromPlugins), $lang);
    }

    /**
     * @param RetailCatalog $product
     * @param int           $hierarchyLevel
     *
     * @return array
     */
    public function getFamilyFromHierarchy(RetailCatalog $product, int $hierarchyLevel): ?array
    {
        $rawFamily = explode(static::HIERARCHY_DELIMITER, $product->getHierarchy())[$hierarchyLevel];

        return $this->getFamilyNameAndId($rawFamily);
    }

    /**
     * @param Agency $agency
     * @param string $lang
     * @param string $type
     * @param int    $maxFamilies
     *
     * @return array|null
     * @throws \Exception
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function getMainFamiliesCatalog(
        Agency $agency,
        string $lang = 'fr',
        string $type = CatalogManager::NORMAL_CATALOG,
        int $maxFamilies = 0
    ): ?array {
        $type = $this->getMainFamiliesType($type);
        if (!$this->getCatalog($agency, $lang, $type)
            && !$this->redis->exists($this->getKey($agency, $type, $lang))
        ) {
            return null;
        }
        $mainFamilies = $this->serializer->deserialize(
            $this->redis->get($this->getKey($agency, $type, $lang)),
            'array',
            'json'
        );
        if ($maxFamilies > 0) {
            $mainFamilies = \array_slice($mainFamilies, 0, $maxFamilies);
        }

        return $mainFamilies;
    }

    /**
     * @param string      $familyId
     * @param null|string $pluginSlug
     *
     * @return null|string
     */
    public function getPictoUrl(string $familyId, ?string $pluginSlug = null): ?string
    {
        if (!$pluginSlug) {
            $redisKey = ImageManager::CATALOG_PICTOS_REDIS_PREFIX . $familyId;

            return $this->redis->exists($redisKey) ? $this->redis->get($redisKey) : null;
        }

        return null;
//            return $this->pluginManager->getPluginImage(PluginCategoryEnum::CATALOG_PLUGIN, $pluginSlug);
    }

    /**
     * @param Agency $agency
     * @param string $productId
     * @param string $lang
     *
     * @return RetailCatalog|bool
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Todotoday\CatalogBundle\Exceptions\ProductNotFoundException
     * @throws \InvalidArgumentException
     */
    public function getProduct(Agency $agency, string $productId, string $lang = 'fr')
    {
        $productCached = $this->cache->getItem($this->getKey($agency, 'product_' . $productId, $lang));

        if (!$productCached->isHit()) {
            if (!$result = $this->repo->findProduct($agency->getSlug(), $lang, $productId)) {
                return false;
            }

            $productCached->set($this->serializer->serialize($result, 'json'));
            $this->cache->save($productCached);
        }

        return $this->serializer->deserialize($productCached->get(), RetailCatalog::class, 'json');
    }

    /**
     * @param RetailCatalog $product
     *
     * @return string
     */
    public function getProductFamilyName(RetailCatalog $product): string
    {
        $productFamilyName = '';
        $productFamily = $this->getProductFamilyNameAndId($product);

        if ($productFamily) {
            $productFamilyName = $productFamily['name'];
        }

        return $productFamilyName;
    }

    /**
     * @param RetailCatalog $product
     *
     * @return array|null
     */
    public function getProductFamilyNameAndId(RetailCatalog $product): ?array
    {
        if ($this->isProductValid($product)) {
            $families = explode(static::HIERARCHY_DELIMITER, $product->getHierarchy());
            $numberOfFamilies = \count($families);

            if ($numberOfFamilies >= 1) {
                return $this->getFamilyNameAndId($families[$numberOfFamilies - 1]);
            }
        }

        return null;
    }

    /**
     * @param RetailCatalog $product
     *
     * @return CatalogManager
     */
    public function removeFirstLevelHierarchy(RetailCatalog $product): self
    {
        $newHierarchyArray = explode(static::HIERARCHY_DELIMITER, $product->getHierarchy(), 2);
        $newHierarchy = '';

        if (isset($newHierarchyArray[1]) && $newHierarchyArray[1]) {
            $newHierarchy = $newHierarchyArray[1];
        }

        $product->setHierarchy($newHierarchy);

        return $this;
    }

    /**
     * @param bool $isNonAdherent
     *
     * @return string
     */
    public function getTypeWithRole(bool $isNonAdherent = false): string
    {
        return $isNonAdherent ? static::NON_ADHERENT_CATALOG : static::NORMAL_CATALOG;
    }

    /**
     *
     * @param null|Agency $agency
     * @param string      $lang
     *
     * @return CatalogManager
     */
    public function resetCache(?Agency $agency, ?string $lang): self
    {
        $this->logger->info(
            sprintf(
                'Reseting catalog cache for %s and lang %s',
                $agency ?: 'all agencies',
                $lang
            )
        );
        if ($agency) {
            $searchKeys = $this->getKey($agency, null, $lang);
            $this->logger->info('Searching keys ' . $searchKeys);
            foreach (new Keyspace($this->redisCacheCatalog, $searchKeys) as $key) {
                $this->redisCacheCatalog->del($key);
                $this->logger->info(sprintf('Deleting key %s', $key));
            }
        } else {
            $this->redisCacheCatalog->flushdb();
            $this->logger->info('Clear all catalog cache');
        }

        return $this;
    }

    /**
     * @param array $clearCacheRules
     *
     * @return CatalogManager
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     */
    public function setClearCacheRules(array $clearCacheRules): self
    {
        $resolver = new OptionsResolver();
        $this->configureOptionsRules($resolver);
        foreach ($clearCacheRules as $rulesName => $rule) {
            $this->clearCacheRules[$rulesName] = $resolver->resolve($rule);
        }

        return $this;
    }

    /**
     * @param Agency $agency
     * @param string $lang
     *
     * @return bool
     * @throws \Exception
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function clearCacheCatalog(
        Agency $agency,
        string $lang = 'fr'
    ): bool {

        if ($this->clearCacheRules) {
            $ruleKey = 'default';
            $currentHour = date('H:00:00');

            if ($this->clearCacheRules[$ruleKey]['firstExecution'] == $currentHour
                || $this->clearCacheRules[$ruleKey]['secondExecution'] == $currentHour) {
                    $this->resetCache($agency, $lang);

                    return true;
            }
        }

        return false;
    }

    /**
     * @param string $rawFamily
     *
     * @return array|null
     */
    private function getFamilyNameAndId(string $rawFamily): ?array
    {
        $familyParts = explode(static::FAMILY_DELIMITER, $rawFamily);

        if (\count($familyParts) === 3) {
            return array(
                'order_id' => $familyParts[0],
                'name' => $this->removePrefix($familyParts[1])['suffix'],
                'id' => $familyParts[2],
            );
        } elseif (\count($familyParts) === 2) {
            return array(
                'order_id' => 'empty_order_id',
                'name' => $this->removePrefix($familyParts[0])['suffix'],
                'id' => $familyParts[1],
            );
        }

        return null;
    }

    /**
     * @param array  $catalog
     * @param string $lang
     *
     * @return array
     */
    private function getHierarchyTreeCatalog(array $catalog, string $lang): array
    {
        $mainFamily = $this->translator->trans('catalog.products_services', array(), 'todotoday');

        $catalogTree = array(
            $mainFamily => array(),
        );

        /** @var RetailCatalog $product */
        foreach ($catalog as $product) {
            try {
                if ($this->isProductValid($product)) {
                    $productFamilies = explode(static::HIERARCHY_DELIMITER, $product->getHierarchy());

                    $this->setTree(
                        $catalogTree[$mainFamily],
                        $productFamilies,
                        $product,
                        $lang
                    );
                }
            } catch (\Exception $e) {
                $message = '[getHierarchyTreeCatalog] - The product ' . $product->getNameTrans() . ' failed. ' . $e;
                $this->logger->log(LoggerEnum::CRITICAL, $message);
                continue;
            }
        }

        return $catalogTree;
    }

    /**
     * Generate key for redis storage
     *
     * @param Agency      $agency
     * @param string|null $type
     * @param string|null $language
     *
     * @return string
     */
    private function getKey(Agency $agency, ?string $type = '*', ?string $language = '*'): string
    {
        $type = $type ?: '*';

        return $type . '_' . $agency->getSlug() . '_' . $language;
    }

    /**
     * @param array  $catalog
     * @param string $lang
     *
     * @return array
     */
    private function getMainFamiliesWithCatalog(array $catalog, string $lang): array
    {
        $mainFamilies = array();

        foreach ($catalog as $product) {
            try {
                if ($this->isProductValid($product)) {
                    $mainFamily = $this->getFamilyFromHierarchy($product, 0);
                    if (!$mainFamily) {
                        continue;
                    }

                    if (!isset($mainFamilies[$mainFamily['name']])) {
                        $mainFamilyName = $mainFamily['name'];
                        $familyId = $mainFamily['id'];
                        $familyPictoUrl = $this->getPictoUrl($familyId);

                        $mainFamilies[$mainFamilyName] = array(
                            'name' => $mainFamilyName,
                            static::FAMILY_ID => $familyId,
                            static::PICTOS_URL => $familyPictoUrl,
                        );
                    }
                }
            } catch (\Exception $e) {
                $message = '[getMainFamiliesWithCatalog] - The product ' . $product->getNameTrans() . ' failed. ' . $e;
                $this->logger->log(LoggerEnum::CRITICAL, $message);
                continue;
            }
        }

        return $mainFamilies;
    }

    /**
     * @param RetailCatalog $product
     *
     * @return bool
     */
    private function isProductValid(RetailCatalog $product): bool
    {
        return null !== $product->getHierarchy() && null !== $product->getNameTrans();
    }

    /**
     * Skip the family if the family is the main family and is equals to "Family/Famille'
     *
     * @param string $familyName
     * @param string $lang
     *
     * @return bool
     */
    private function isSkipFirstFamily(string $familyName, string $lang): bool
    {
        return \in_array($familyName, static::FAMILY_TO_ESCAPE[$lang], true);
    }

    /**
     * @param string $string
     *
     * @return array
     */
    private function removePrefix(string $string): array
    {
        //Si la position du premier caractère du délimiteur est supérieur à $maxDelimiterPosition
        //alors on ne le supprime pas le délimiteur.
        //Ex1: Famille - Enfant (on ne supprime pas le délimiteur)
        //Ex2: 1 - Famille Enfant (on supprime le délimiteur)
        $maxDelimiterPosition = 5;

        $delimiterPosition = strpos($string, static::PREFIX_DELIMITER);
        if ($delimiterPosition && $delimiterPosition <= $maxDelimiterPosition) {
            return array(
                'prefix' => substr($string, 0, $delimiterPosition),
                'suffix' => substr($string, $delimiterPosition + strlen(static::PREFIX_DELIMITER)),
            );
        }

        return array(
            'prefix' => '',
            'suffix' => $string,
        );
    }

    /**
     * @param array  $result
     * @param Agency $agency
     * @param string $lang
     * @param string $type
     */
    private function setRedisMainFamilies(
        array $result,
        Agency $agency,
        string $lang = 'fr',
        string $type = CatalogManager::NORMAL_CATALOG
    ): void {
        $this->redis->set(
            $this->getKey($agency, $this->getMainFamiliesType($type), $lang),
            $this->serializer->serialize(
                $this->getMainFamiliesWithCatalog($result, $lang),
                'json',
                (new
                SerializationContext())->setSerializeNull(true)
            )
        );
    }

    /**
     * @param string $type
     *
     * @return string
     */
    private function getMainFamiliesType(string $type = CatalogManager::NORMAL_CATALOG): string
    {
        $type = ($type === static::NORMAL_CATALOG) ? '' : '_non_adherent';

        return static::MAIN_FAMILIES . $type;
    }

    /**
     * @param array         $catalogTree
     * @param array         $productFamilies
     * @param RetailCatalog $product
     * @param string        $lang
     * @param int           $position
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    private function setTree(
        array &$catalogTree,
        array $productFamilies,
        RetailCatalog $product,
        string $lang,
        int $position = 0
    ): void {
        $familiesNumber = \count($productFamilies);
        $family = $this->getFamilyNameAndId($productFamilies[$position]);
        $isPluginProduct = $product->isPluginProduct();

        if (!$family) {
            return;
        }

        $familyName = $family['name'];
        if ($isPluginProduct && !empty($family['id'])) {
            $familyName = $this->getFamilyNameFromTree($catalogTree, $family['id']) ?? $product->getNameTrans();
        }

        $isLastChildFamily = ($position === ($familiesNumber - 1));

        if (!isset($catalogTree[$familyName])) {
            if ($isPluginProduct) {
                return;
            }
            $familyId = $family['id'];

            $catalogTree[$familyName] = $this->getFamilyArray(
                $familyId,
                $family['order_id'],
                $product
            );
        }

        if ($isLastChildFamily) {
            $product->setFamillyId($family['id']);
            if ($isPluginProduct) {
                $familyInformations = $this->getFamilyArray(
                    $product->getItemId(),
                    $family['order_id'],
                    $product
                );

                $minIndex = \count($familyInformations);
                $maxIndex = \count($catalogTree[$familyName]);
                $familyPluginPosition = $product->getFamilyPluginPosition();

                if (($familyPluginPosition <= 0) || ($familyPluginPosition + $minIndex > $maxIndex)) {
                    $catalogTree[$familyName][$product->getNameTrans()] = $familyInformations;
                } else {
                    $position = $minIndex + $familyPluginPosition - 1;
                    $catalogTree[$familyName] =
                        \array_slice($catalogTree[$familyName], 0, $position, true) +
                        array($product->getNameTrans() => $familyInformations) +
                        \array_slice($catalogTree[$familyName], $position, null, true);
                }
                $catalogTree[$familyName][$product->getNameTrans()][self::PRODUCT_FOLDER][] = $product;
            } else {
                $catalogTree[$familyName][self::PRODUCT_FOLDER][] = $product;
            }
        } else {
            $this->setTree($catalogTree[$familyName], $productFamilies, $product, $lang, ++$position);
        }
    }

    /**
     * @param string        $familyId
     * @param string        $orderId
     * @param RetailCatalog $product
     *
     * @return array
     */
    private function getFamilyArray(
        string $familyId,
        ?string $orderId,
        RetailCatalog $product
    ): array {
        $isPluginFamily = $product->isPluginProduct();
        $pluginSlug = $isPluginFamily ? $product->getItemModelGroup() : null;
        $pictosUrl = $this->getPictoUrl($familyId, $pluginSlug);

        return array(
            static::FAMILY_ID => $familyId,
            static::ORDER_ID => $orderId,
            static::PICTOS_URL => $pictosUrl,
            static::IS_PLUGIN_FAMILY => $isPluginFamily,
            static::PLUGIN_SLUG => $pluginSlug,
            static::PLUGIN_MDI_ICON => $isPluginFamily ? $product->getFamilyMdiIcon() : null,
        );
    }

    /**
     * @param array  $catalogTree
     * @param string $familyId
     *
     * @return null|string
     */
    private function getFamilyNameFromTree(array $catalogTree, string $familyId): ?string
    {
        foreach ($catalogTree as $familyName => $family) {
            if (\is_array($family) && $family[static::FAMILY_ID] === $familyId) {
                return $familyName;
            }
        }

        return null;
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     */
    private function configureOptionsRules(OptionsResolver $resolver): void
    {
        $resolver->setRequired(
            [
                'firstExecution',
                'secondExecution',
            ]
        )
        ->setAllowedTypes('firstExecution', 'string')
        ->setAllowedTypes('secondExecution', 'string');
    }
}
