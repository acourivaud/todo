<?php declare(strict_types=1);

/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 01/03/2017
 * Time: 12:10
 */

namespace Todotoday\CatalogBundle\Repository\Api\Microsoft;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiRepository;

/**
 * Class DocCategoryRepository
 *
 * @category   Todo-Todev
 * @package    Todotoday\CatalogBundle
 * @subpackage Todotoday\CatalogBundle\Repository\Api\Microsoft
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class DocCategoryRepository extends AbstractApiRepository
{
    /**
     * Do findAllByDate
     *
     * @param int    $limit
     * @param int    $offset
     * @param string $date
     *
     * @return array|\object[]
     * @throws \InvalidArgumentException
     */
    public function findAllByDate(int $limit = null, int $offset = null, string $date = null)
    {
        return $this
            ->createQueryBuilder()
            ->setLimit($limit)
            ->setOffset($offset)
            ->addCustomQuery(
                '$filter',
                'LastModification gt ' . $date
            )
            ->getRequest()
            ->execute();
    }
}
