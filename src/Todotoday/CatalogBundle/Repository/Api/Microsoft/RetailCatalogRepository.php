<?php declare(strict_types=1);

/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 01/03/2017
 * Time: 12:10
 */

namespace Todotoday\CatalogBundle\Repository\Api\Microsoft;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiRepository;
use Actiane\ApiConnectorBundle\Lib\Request;
use Todotoday\CatalogBundle\Entity\Api\Microsoft\RetailCatalog;

/**
 * Class RetailCatalogRepository
 *
 * @category   Todo-Todev
 * @package    Todotoday\CatalogBundle
 * @subpackage Todotoday\CatalogBundle\Repository\Api\Microsoft
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class RetailCatalogRepository extends AbstractApiRepository
{
    /**
     * @param string $agencySlug
     * @param string $language
     *
     * @param bool   $isDigital
     *
     * @return RetailCatalog[]|null
     * @throws \InvalidArgumentException
     */
    public function findCatalogByAgencyAndLang(string $agencySlug, string $language, bool $isDigital = false): ?array
    {
        $filter = $this->getFilter($agencySlug, $language, $isDigital);
        $filter .= ' and ItemModelGroup ne \'NA_PMP\' and ItemModelGroup ne \'NA_Service\'';

        return $this->getRequest($filter)->execute();
    }

    /**
     * @param string $agencySlug
     * @param string $language
     *
     * @param bool   $isDigital
     *
     * @return RetailCatalog[]|null
     * @throws \InvalidArgumentException
     */
    public function findCatalogNonAdherentByAgencyAndLang(
        string $agencySlug,
        string $language,
        bool $isDigital = false
    ): ?array {
        $filter = $this->getFilter($agencySlug, $language, $isDigital);
        $filter .= ' and (ItemModelGroup eq \'NA_PMP\' or ItemModelGroup eq \'NA_Service\')';

        return $this->getRequest($filter)->execute();
    }

    /**
     * @param string $agencySlug
     * @param string $language
     * @param string $productId
     *
     * @return RetailCatalog|null
     * @throws \InvalidArgumentException
     */
    public function findProduct(string $agencySlug, string $language, string $productId): ?RetailCatalog
    {
        $filter = $this->getFilter($agencySlug, $language) . ' and ItemId eq \'' . $productId . '\'';

        return ($this->getRequest($filter)->execute()) ? $this->getRequest($filter)->execute()[0] : null;
    }

    /**
     * Return one product (usefull for testing case)
     *
     * @param string $agencySlug
     * @param string $language
     *
     * @return RetailCatalog[]
     * @throws \InvalidArgumentException
     */
    public function getFirstProduct(string $agencySlug, string $language): ?array
    {
        $filter = $this->getFilter($agencySlug, $language);

        // TODO le premier item est en erreur (item model group etc.)
        return $this
            ->createQueryBuilder()
            ->addCustomQuery(
                '$filter',
                $filter
            )
            ->setLimit(5)
            ->getRequest()
            ->execute();
    }

    /**
     * @param string $agencySlug
     * @param string $language
     *
     * @return RetailCatalog[]
     * @throws \InvalidArgumentException
     */
    public function getFirstRdv(string $agencySlug, string $language = 'fr'): ?array
    {
        $filter = $this->getFilter($agencySlug, $language);
        $filter .= ' and ItemModelGroup eq \'ServiceRDV\'';

        return $this
            ->createQueryBuilder()
            ->addCustomQuery(
                '$filter',
                $filter
            )
            ->setLimit(1)
            ->getRequest()
            ->execute();
    }

    /**
     * @param string $agencySlug
     * @param string $language
     *
     * @return RetailCatalog[]
     * @throws \InvalidArgumentException
     */
    public function getFirstService(string $agencySlug, string $language = 'fr'): ?array
    {
        $filter = $this->getFilter($agencySlug, $language);
        $filter .= ' and ItemModelGroup eq \'PMP\'';

        return $this
            ->createQueryBuilder()
            ->addCustomQuery(
                '$filter',
                $filter
            )
            ->setLimit(1)
            ->getRequest()
            ->execute();
    }

    /**
     * Return all Service RDV available for agency
     *
     * @param string $agencySlug
     * @param string $language
     *
     * @return RetailCatalog[]|null
     * @throws \InvalidArgumentException
     */
    public function getServiceRdv(string $agencySlug, string $language = 'fr'): ?array
    {
        $filter = $this->getFilter($agencySlug, $language);
        $filter .= ' and crm_id_service ne \'\' and ItemModelGroup eq \'ServiceRDV\'';

        return $this
            ->createQueryBuilder()
            ->addCustomQuery('$filter', $filter)
            ->getRequest()
            ->execute();
    }

    /**
     * Return all Service RDV available for agency
     *
     * @param string $agencySlug
     * @param string $language
     *
     * @return RetailCatalog[]|null
     * @throws \InvalidArgumentException
     */
    public function getServiceRdvWithNoCrmIdService(string $agencySlug, string $language = 'fr'): ?array
    {
        $filter = $this->getFilter($agencySlug, $language);
        $filter .= ' and crm_id_service eq \'\' and ItemModelGroup eq \'ServiceRDV\'';

        return $this
            ->createQueryBuilder()
            ->addCustomQuery('$filter', $filter)
            ->getRequest()
            ->execute();
    }

    /**
     * @param string $agencySlug
     * @param string $language
     * @param bool   $isDigital
     *
     * @return string
     */
    private function getFilter(string $agencySlug, string $language, bool $isDigital = false): string
    {
        if ($language === 'en') {
            $language = 'en-US';
        }

        $filterDigit = $isDigital ? ' and Vendors ne \'\'' : '';

        return sprintf('IdActiane eq \'%s\' and LanguageTrans eq \'%s\' %s', $agencySlug, $language, $filterDigit);
    }

    /**
     * @param string $filter
     *
     * @return Request
     * @throws \InvalidArgumentException
     */
    private function getRequest(string $filter): Request
    {
        return $this
            ->createQueryBuilder()
            ->addCustomQuery(
                '$filter',
                $filter
            )
            ->addCustomQuery(
                '$orderby',
                'Hierarchy,NameTrans'
            )
            ->getRequest();
    }
}
