<?php declare(strict_types=1);

/**
 * PHP version 7
 *
 * Created by
 * User: Pix
 * Date: 04/02/2021
 * Time: 15:00
 */

namespace Todotoday\CatalogBundle\Repository\Api\Microsoft;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiRepository;

/**
 * Class RetailImageRepository
 *
 * @category   Todo-Todev
 * @package    Todotoday\CatalogBundle
 * @subpackage Todotoday\CatalogBundle\Repository\Api\Microsoft
 * @author     Pix Digital <technique@pix-digital.com>
 */
class RetailImageRepository extends AbstractApiRepository
{

    /**
     * Do findAll
     *
     * @param int $limit
     * @param int $offset
     *
     * @return array|\object[]
     * @throws \InvalidArgumentException
     */
    public function findAll(int $limit = null, int $offset = null)
    {
        return $this
            ->createQueryBuilder()
            ->setLimit($limit)
            ->setOffset($offset)
            ->getRequest()
            ->addUrlParam('cross-company', 'true')
            ->execute();
    }
}
