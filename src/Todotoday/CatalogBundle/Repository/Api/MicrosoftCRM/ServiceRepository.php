<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 13/04/17
 * Time: 12:36
 */

namespace Todotoday\CatalogBundle\Repository\Api\MicrosoftCRM;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiRepository;

/**
 * Class ServiceRepository
 * @package Todotoday\CatalogBundle\Repository\Api\MicrosoftCRM
 */
class ServiceRepository extends AbstractApiRepository
{
}
