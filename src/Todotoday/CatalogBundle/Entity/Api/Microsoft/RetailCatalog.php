<?php declare(strict_types=1);

/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 01/03/2017
 * Time: 11:46
 */

namespace Todotoday\CatalogBundle\Entity\Api\Microsoft;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiConnection;
use Actiane\ApiConnectorBundle\Lib\EntityProxyTrait;
use Actiane\ApiConnectorBundle\Lib\Mapping as APIConnector;
use JMS\Serializer\Annotation as Serializer;
use Todotoday\CatalogBundle\Enum\CatalogNeedBookingEnum;
use Todotoday\CatalogBundle\Enum\CatalogNeedDepositoryEnum;
use Todotoday\CatalogBundle\Enum\CatalogNeedStockEnum;
use Todotoday\CatalogBundle\Enum\CatalogQuotationEnum;
use Todotoday\PluginBundle\Services\PluginCatalog;

/**
 * Class RetailCatalog
 *
 * @SuppressWarnings(PHPMD)
 * @APIConnector\Entity(path="TDTDRetailCatalogs", repositoryId="todotoday.catalog.repository.api.retail_catalogs")
 *
 * @category   Todo-Todev
 * @package    Todotoday\CatalogBundle
 * @subpackage Todotoday\CatalogBundle\Entity\Api\Microsoft
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 * @Serializer\ExclusionPolicy("all")
 */
class RetailCatalog
{
    use EntityProxyTrait;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @APIConnector\Column(name="ItemId", type="string", doctrineAttribute="product")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $itemId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ProductType", type="string", doctrineAttribute="productType")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $productType;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DescriptionTrans", type="string", doctrineAttribute="productDescription")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $descriptionTrans;

    /**
     * @var string
     *
     * @APIConnector\Column(name="CategoryName", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $categoryName;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @APIConnector\Column(name="Assortment", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $assortment;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ParentCategory", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $parentCategory;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @APIConnector\Column(name="Store", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $store;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @APIConnector\Column(name="LanguageTrans", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $languageTrans;

    /**
     * @var string
     *
     * @APIConnector\Column(name="IdActiane", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $idActiane;

    /**
     * @var string
     *
     * @APIConnector\Column(name="Status", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $status;

    /**
     * @var string
     *
     * @APIConnector\Column(name="NameTrans", type="string", doctrineAttribute="productName")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $nameTrans;

    /**
     * @var string
     *
     * @APIConnector\Column(name="MainCategory", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $mainCategory;

    /**
     * @var string
     * @APIConnector\Column(name="Hierarchy", type="string", doctrineAttribute="hierarchy")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $hierarchy;

    /**
     * @var int
     * @APIConnector\Column(name="Taxe", type="int")
     * @Serializer\Type("int")
     * @Serializer\Expose()
     */
    protected $taxe;

    /**
     * @var float
     * @APIConnector\Column(name="Amount", type="float", doctrineAttribute="productPrice")
     * @Serializer\Type("float")
     * @Serializer\Expose()
     */
    protected $amount;

    /**
     * @var float
     * @APIConnector\Column(name="AmountHT", type="float", doctrineAttribute="productPriceExclTax")
     * @Serializer\Type("float")
     * @Serializer\Expose()
     */
    protected $amountExclTax;

    /**
     * @var int
     * @APIConnector\Column(name="Stock", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $stock;

    /**
     * @var string
     *
     * @APIConnector\Column(name="ItemModelGroup", type="string", doctrineAttribute="itemModelGroup")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $itemModelGroup;

    /**
     * @var string
     *
     * @APIConnector\Column(name="crm_id_service", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $crmIdService;

    /**
     * @var int
     *
     * @APIConnector\Column(name="ServiceLength", type="int")
     * @Serializer\Type("int")
     * @Serializer\Expose()
     */
    protected $serviceLength;

    /**
     * @var string
     *
     * @APIConnector\Column(name="Vendors", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $vendors;

    /**
     * @var string
     *
     * @APIConnector\Column(name="IsAnimation", type="string")
     */
    protected $isAnimation;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     * @Serializer\Expose()
     */
    protected $depositoryNeeded = false;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     * @Serializer\Expose()
     */
    protected $stockNeeded = false;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     * @Serializer\Expose()
     */
    protected $bookable = false;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     * @Serializer\Expose()
     */
    protected $animation = false;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $famillyId;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     * @Serializer\Expose()
     */
    protected $quotation = false;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $familyMdiIcon = '';

    /**
     * @var int
     * @Serializer\Type("int")
     * @Serializer\Expose()
     */
    protected $familyPluginPosition = 0;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $picture = '';

    /**
     * @return string
     */
    public function getPicture(): string
    {
        return $this->picture;
    }

    /**
     * @param string $picture
     *
     * @return RetailCatalog
     */
    public function setPicture(string $picture): RetailCatalog
    {
        return $this->setTrigger('picture', $picture);
    }

    /**
     * @return float
     */
    public function getAmount(): ?float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     *
     * @return RetailCatalog
     */
    public function setAmount(float $amount): RetailCatalog
    {
        return $this->setTrigger('amount', $amount);
    }

    /**
     * @return float
     */
    public function getAmountExclTax(): ?float
    {
        return $this->amountExclTax;
    }

    /**
     * @param float $amountExclTax
     *
     * @return RetailCatalog
     */
    public function setAmountExclTax(float $amountExclTax): RetailCatalog
    {
        return $this->setTrigger('amountExclTax', $amountExclTax);
    }

    /**
     * @return AbstractApiConnection
     */
    public function getApiConnection(): ?AbstractApiConnection
    {
        return $this->apiConnection;
    }

    /**
     * Get assortment
     *
     * @return string
     */
    public function getAssortment(): ?string
    {
        return $this->assortment;
    }

    /**
     * Set assortment
     *
     * @param string $assortment
     *
     * @return RetailCatalog
     */
    public function setAssortment(?string $assortment = null): RetailCatalog
    {
        return $this->setTrigger('assortment', $assortment);
    }

    /**
     * Get categoryName
     *
     * @return string
     */
    public function getCategoryName(): ?string
    {
        return $this->categoryName;
    }

    /**
     * Set categoryName
     *
     * @param string $categoryName
     *
     * @return RetailCatalog
     */
    public function setCategoryName(?string $categoryName = null): RetailCatalog
    {
        return $this->setTrigger('categoryName', $categoryName);
    }

    /**
     * @return string
     */
    public function getCrmIdService(): ?string
    {
        return $this->crmIdService;
    }

    /**
     * @param string $crmIdService
     *
     * @return RetailCatalog
     */
    public function setCrmIdService(?string $crmIdService): RetailCatalog
    {
        return $this->setTrigger('crmIdService', $crmIdService);
    }

    /**
     * Get descriptionTrans
     *
     * @return string
     */
    public function getDescriptionTrans(): ?string
    {
        return $this->descriptionTrans;
    }

    /**
     * Set descriptionTrans
     *
     * @param string $descriptionTrans
     *
     * @return RetailCatalog
     */
    public function setDescriptionTrans(?string $descriptionTrans = null): RetailCatalog
    {
        return $this->setTrigger('descriptionTrans', $descriptionTrans);
    }

    /**
     * @return string
     */
    public function getHierarchy(): ?string
    {
        return $this->hierarchy;
    }

    /**
     * @param string $hierarchy
     *
     * @return RetailCatalog
     */
    public function setHierarchy(?string $hierarchy): RetailCatalog
    {
        return $this->setTrigger('hierarchy', $hierarchy);
    }

    /**
     * Get idActiane
     *
     * @return string
     */
    public function getIdActiane(): ?string
    {
        return $this->idActiane;
    }

    /**
     * Set idActiane
     *
     * @param string $idActiane
     *
     * @return RetailCatalog
     */
    public function setIdActiane(?string $idActiane = null): RetailCatalog
    {
        return $this->setTrigger('idActiane', $idActiane);
    }

    /**
     * Get itemId
     *
     * @return string
     */
    public function getItemId(): ?string
    {
        return $this->itemId;
    }

    /**
     * Set itemId
     *
     * @param string $itemId
     *
     * @return RetailCatalog
     */
    public function setItemId(?string $itemId = null): RetailCatalog
    {
        return $this->setTrigger('itemId', $itemId);
    }

    /**
     * @return string
     */
    public function getItemModelGroup(): ?string
    {
        return $this->itemModelGroup;
    }

    /**
     * @param string $itemModelGroup
     *
     * @return RetailCatalog
     */
    public function setItemModelGroup(string $itemModelGroup): RetailCatalog
    {
        $this->depositoryNeeded = CatalogNeedDepositoryEnum::has($itemModelGroup);
        $this->bookable = CatalogNeedBookingEnum::has($itemModelGroup);
        $this->stockNeeded = CatalogNeedStockEnum::has($itemModelGroup);
        $this->quotation = CatalogQuotationEnum::has(strtolower($itemModelGroup));

        return $this->setTrigger('itemModelGroup', $itemModelGroup);
    }

    /**
     * Get languageTrans
     *
     * @return string
     */
    public function getLanguageTrans(): ?string
    {
        if ($this->languageTrans === 'en-US') {
            return 'en';
        }

        return $this->languageTrans;
    }

    /**
     * Set languageTrans
     *
     * @param string $languageTrans
     *
     * @return RetailCatalog
     */
    public function setLanguageTrans(?string $languageTrans = null): RetailCatalog
    {
        if ($languageTrans === 'en') {
            $languageTrans = 'en-US';
        }

        return $this->setTrigger('languageTrans', $languageTrans);
    }

    /**
     * Get mainCategory
     *
     * @return string
     */
    public function getMainCategory(): ?string
    {
        return $this->mainCategory;
    }

    /**
     * Set mainCategory
     *
     * @param string $mainCategory
     *
     * @return RetailCatalog
     */
    public function setMainCategory(?string $mainCategory = null): RetailCatalog
    {
        return $this->setTrigger('mainCategory', $mainCategory);
    }

    /**
     * Get nameTrans
     *
     * @return string
     */
    public function getNameTrans(): ?string
    {
        return $this->nameTrans;
    }

    /**
     * Set nameTrans
     *
     * @param string $nameTrans
     *
     * @return RetailCatalog
     */
    public function setNameTrans(?string $nameTrans = null): RetailCatalog
    {
        return $this->setTrigger('nameTrans', $nameTrans);
    }

    /**
     * Get parentCategory
     *
     * @return string
     */
    public function getParentCategory(): ?string
    {
        return $this->parentCategory;
    }

    /**
     * Set parentCategory
     *
     * @param string $parentCategory
     *
     * @return RetailCatalog
     */
    public function setParentCategory(?string $parentCategory = null): RetailCatalog
    {
        return $this->setTrigger('parentCategory', $parentCategory);
    }

    /**
     * Get productType
     *
     * @return string
     */
    public function getProductType(): ?string
    {
        return $this->productType;
    }

    /**
     * Set productType
     *
     * @param string $productType
     *
     * @return RetailCatalog
     */
    public function setProductType(?string $productType = null): RetailCatalog
    {
        return $this->setTrigger('productType', $productType);
    }

    /**
     * @return int
     */
    public function getServiceLength(): ?int
    {
        return $this->serviceLength;
    }

    /**
     * @param int $serviceLength
     *
     * @return RetailCatalog
     */
    public function setServiceLength(?int $serviceLength): RetailCatalog
    {
        return $this->setTrigger('serviceLength', $serviceLength);
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return RetailCatalog
     */
    public function setStatus(?string $status = null): RetailCatalog
    {
        return $this->setTrigger('status', $status);
    }

    /**
     * @return int
     */
    public function getStock(): ?int
    {
        return $this->stock;
    }

    /**
     * @param int $stock
     *
     * @return RetailCatalog
     */
    public function setStock(int $stock): RetailCatalog
    {
        return $this->setTrigger('stock', $stock);
    }

    /**
     * Get store
     *
     * @return string
     */
    public function getStore(): ?string
    {
        return $this->store;
    }

    /**
     * Set store
     *
     * @param string $store
     *
     * @return RetailCatalog
     */
    public function setStore(?string $store = null): RetailCatalog
    {
        return $this->setTrigger('store', $store);
    }

    /**
     * @return int
     */
    public function getTaxe(): ?int
    {
        return $this->taxe;
    }

    /**
     * @param int $taxe
     *
     * @return RetailCatalog
     */
    public function setTaxe(int $taxe): RetailCatalog
    {
        return $this->setTrigger('taxe', $taxe);
    }

    /**
     * @return string
     */
    public function getIsAnimation(): ?string
    {
        return $this->isAnimation;
    }

    /**
     * @param string $isAnimation
     *
     * @return RetailCatalog
     */
    public function setIsAnimation(?string $isAnimation): RetailCatalog
    {
        if ($isAnimation === 'Yes') {
            $this->setAnimation(true);
        }

        return $this->setTrigger('isAnimation', $isAnimation);
    }

    /**
     * @return bool
     */
    public function isBookable(): bool
    {
        return $this->bookable;
    }

    /**
     * @param bool $bookable
     *
     * @return RetailCatalog
     */
    public function setBookable(bool $bookable): RetailCatalog
    {
        $this->bookable = $bookable;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDepositoryNeeded(): bool
    {
        return $this->depositoryNeeded;
    }

    /**
     * @param bool $depositoryNeeded
     *
     * @return RetailCatalog
     */
    public function setDepositoryNeeded(bool $depositoryNeeded): RetailCatalog
    {
        $this->depositoryNeeded = $depositoryNeeded;

        return $this;
    }

    /**
     * @return bool
     */
    public function isStockNeeded(): bool
    {
        return $this->stockNeeded;
    }

    /**
     * @param bool $stockNeeded
     *
     * @return RetailCatalog
     */
    public function setStockNeeded(bool $stockNeeded): RetailCatalog
    {
        $this->stockNeeded = $stockNeeded;

        return $this;
    }

    /**
     * @param AbstractApiConnection $apiConnection
     *
     * @return RetailCatalog
     */
    public function setApiConnection(?AbstractApiConnection $apiConnection): RetailCatalog
    {
        $this->apiConnection = $apiConnection;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAnimation(): bool
    {
        return $this->animation;
    }

    /**
     * @param bool $animation
     *
     * @return RetailCatalog
     */
    public function setAnimation(bool $animation): RetailCatalog
    {
        $this->animation = $animation;

        return $this;
    }

    /**
     * @return string
     */
    public function getFamillyId(): ?string
    {
        return $this->famillyId;
    }

    /**
     * @param string $famillyId
     *
     * @return RetailCatalog
     */
    public function setFamillyId(string $famillyId): RetailCatalog
    {
        $this->famillyId = $famillyId;

        return $this;
    }

    /**
     * @return bool
     */
    public function isQuotation(): bool
    {
        return $this->quotation;
    }

    /**
     * @param bool $quotation
     *
     * @return RetailCatalog
     */
    public function setQuotation(bool $quotation): self
    {
        $this->quotation = $quotation;

        return $this;
    }

    /**
     * @return string
     */
    public function getHierachyAndName(): string
    {
        return $this->hierarchy . $this->getNameTrans();
    }

    /**
     * @return string
     */
    public function getFamilyMdiIcon(): ?string
    {
        return $this->familyMdiIcon;
    }

    /**
     * @param null|string $familyMdiIcon
     *
     * @return RetailCatalog
     *
     */
    public function setFamilyMdiIcon(?string $familyMdiIcon): self
    {
        $this->familyMdiIcon = $familyMdiIcon;

        return $this;
    }

    /**
     * @return bool
     */
    public function isPluginProduct(): bool
    {
        return $this->getProductType() === PluginCatalog::getProductType();
    }

    /**
     * @return int
     */
    public function getFamilyPluginPosition(): ?int
    {
        return $this->familyPluginPosition;
    }

    /**
     * @param int|null $familyPluginPosition
     *
     * @return RetailCatalog
     */
    public function setFamilyPluginPosition(?int $familyPluginPosition): self
    {
        $this->familyPluginPosition = $familyPluginPosition;

        return $this;
    }

    /**
     * In order to remove duplicates content from catalog
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->languageTrans . '_' . $this->getItemId();
    }

    /**
     * @return null|string
     */
    public function getVendors(): ?string
    {
        return $this->vendors;
    }

    /**
     * @param null|string $vendors
     *
     * @return RetailCatalog
     */
    public function setVendors(?string $vendors): self
    {
        return $this->setTrigger('vendors', $vendors);
    }
}
