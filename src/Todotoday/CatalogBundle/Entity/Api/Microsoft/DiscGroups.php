<?php declare(strict_types = 1);

/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 08/03/2017
 * Time: 20:13
 */

namespace Todotoday\CatalogBundle\Entity\Api\Microsoft;

use Actiane\ApiConnectorBundle\Lib\EntityProxyTrait;
use Actiane\ApiConnectorBundle\Lib\Mapping as APIConnector;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class DiscGroups
 *
 * @SuppressWarnings(PHPMD)
 * @Serializer\ExclusionPolicy("all")
 * @APIConnector\Entity(path="DiscGroups", repositoryId="todotoday.catalog.repository.api.disc_groups")
 *
 * @category   Todo-Todev
 * @package    Todotoday\CatalogBundle
 * @subpackage Todotoday\CatalogBundle\Entity\Api\Microsoft
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class DiscGroups
{
    use EntityProxyTrait;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Id()
     * @APIConnector\Column(name="Module", type="string", mType="Microsoft.Dynamics.DataEntities.ModuleInventCustVend")
     */
    protected $module;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="Name", type="string")
     */
    protected $name;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Id()
     * @APIConnector\Column(name="Store", type="string")
     */
    protected $store;

    /**
     * @var int
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="RetailPricingPriorityNumber", type="int")
     */
    protected $retailPricingPriorityNumber;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="LegalEntity", type="string")
     */
    protected $legalEntity;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Id()
     * @APIConnector\Column(name="GroupId", type="string")
     */
    protected $groupId;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Id()
     * @APIConnector\Column(name="dataAreaId", type="string")
     */
    protected $dataAreaId;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="IdActiane", type="string")
     */
    protected $idActiane;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="MCRPriceDiscGroupType", type="string")
     */
    protected $mCRPriceDiscGroupType;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Id()
     * @APIConnector\Column(name="ItemId", type="string")
     */
    protected $itemId;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="RetailCheckSalesPriceStatus", type="string")
     */
    protected $retailCheckSalesPriceStatus;

    /**
     * @var float
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="Price", type="float")
     */
    protected $price;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Id()
     * @APIConnector\Column(name="Type", type="string", mType="Microsoft.Dynamics.DataEntities.PriceGroupType")
     */
    protected $type;

    /**
     * Get dataAreaId
     *
     * @return string
     */
    public function getDataAreaId(): ?string
    {
        return $this->dataAreaId;
    }

    /**
     * Set dataAreaId
     *
     * @param string $dataAreaId
     *
     * @return DiscGroups
     */
    public function setDataAreaId(?string $dataAreaId): DiscGroups
    {
        return $this->setTrigger('dataAreaId', $dataAreaId);
    }

    /**
     * Get groupId
     *
     * @return string
     */
    public function getGroupId(): ?string
    {
        return $this->groupId;
    }

    /**
     * Set groupId
     *
     * @param string $groupId
     *
     * @return DiscGroups
     */
    public function setGroupId(?string $groupId): DiscGroups
    {
        return $this->setTrigger('groupId', $groupId);
    }

    /**
     * Get idActiane
     *
     * @return string
     */
    public function getIdActiane(): ?string
    {
        return $this->idActiane;
    }

    /**
     * Set idActiane
     *
     * @param string $idActiane
     *
     * @return DiscGroups
     */
    public function setIdActiane(?string $idActiane): DiscGroups
    {
        return $this->setTrigger('idActiane', $idActiane);
    }

    /**
     * Get itemId
     *
     * @return string
     */
    public function getItemId(): ?string
    {
        return $this->itemId;
    }

    /**
     * Set itemId
     *
     * @param string $itemId
     *
     * @return DiscGroups
     */
    public function setItemId(?string $itemId): DiscGroups
    {
        return $this->setTrigger('itemId', $itemId);
    }

    /**
     * Get legalEntity
     *
     * @return string
     */
    public function getLegalEntity(): ?string
    {
        return $this->legalEntity;
    }

    /**
     * Set legalEntity
     *
     * @param string $legalEntity
     *
     * @return DiscGroups
     */
    public function setLegalEntity(?string $legalEntity): DiscGroups
    {
        return $this->setTrigger('legalEntity', $legalEntity);
    }

    /**
     * Get mCRPriceDiscGroupType
     *
     * @return string
     */
    public function getMCRPriceDiscGroupType(): ?string
    {
        return $this->mCRPriceDiscGroupType;
    }

    /**
     * Set mCRPriceDiscGroupType
     *
     * @param string $mCRPriceDiscGroupType
     *
     * @return DiscGroups
     */
    public function setMCRPriceDiscGroupType(?string $mCRPriceDiscGroupType): DiscGroups
    {
        return $this->setTrigger('mCRPriceDiscGroupType', $mCRPriceDiscGroupType);
    }

    /**
     * Get module
     *
     * @return string
     */
    public function getModule(): ?string
    {
        return $this->module;
    }

    /**
     * Set module
     *
     * @param string $module
     *
     * @return DiscGroups
     */
    public function setModule(?string $module): DiscGroups
    {
        return $this->setTrigger('module', $module);
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return DiscGroups
     */
    public function setName(?string $name): DiscGroups
    {
        return $this->setTrigger('name', $name);
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return DiscGroups
     */
    public function setPrice(float $price): DiscGroups
    {
        return $this->setTrigger('price', $price);
    }

    /**
     * Get retailCheckSalesPriceStatus
     *
     * @return string
     */
    public function getRetailCheckSalesPriceStatus(): ?string
    {
        return $this->retailCheckSalesPriceStatus;
    }

    /**
     * Set retailCheckSalesPriceStatus
     *
     * @param string $retailCheckSalesPriceStatus
     *
     * @return DiscGroups
     */
    public function setRetailCheckSalesPriceStatus(?string $retailCheckSalesPriceStatus): DiscGroups
    {
        return $this->setTrigger('retailCheckSalesPriceStatus', $retailCheckSalesPriceStatus);
    }

    /**
     * Get retailPricingPriorityNumber
     *
     * @return int
     */
    public function getRetailPricingPriorityNumber(): int
    {
        return $this->retailPricingPriorityNumber;
    }

    /**
     * Set retailPricingPriorityNumber
     *
     * @param int $retailPricingPriorityNumber
     *
     * @return DiscGroups
     */
    public function setRetailPricingPriorityNumber(int $retailPricingPriorityNumber): DiscGroups
    {
        return $this->setTrigger('retailPricingPriorityNumber', $retailPricingPriorityNumber);
    }

    /**
     * Get store
     *
     * @return string
     */
    public function getStore(): ?string
    {
        return $this->store;
    }

    /**
     * Set store
     *
     * @param string $store
     *
     * @return DiscGroups
     */
    public function setStore(?string $store): DiscGroups
    {
        return $this->setTrigger('store', $store);
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return DiscGroups
     */
    public function setType(?string $type): DiscGroups
    {
        return $this->setTrigger('type', $type);
    }
}
