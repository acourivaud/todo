<?php declare(strict_types=1);

/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 01/03/2017
 * Time: 11:46
 */

namespace Todotoday\CatalogBundle\Entity\Api\Microsoft;

use Actiane\ApiConnectorBundle\Lib\EntityProxyTrait;
use Actiane\ApiConnectorBundle\Lib\Mapping as APIConnector;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class DocCategory
 *
 * @SuppressWarnings(PHPMD)
 * @APIConnector\Entity(path="DocCategories", repositoryId="todotoday.catalog.repository.api.doc_categories")
 *
 * @category   Todo-Todev
 * @package    Todotoday\CatalogBundle
 * @subpackage Todotoday\CatalogBundle\Entity\Api\Microsoft
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 * @Serializer\ExclusionPolicy("all")
 */
class DocCategory
{
    use EntityProxyTrait;

    /**
     * @var string
     *
     * @APIConnector\Column(name="LastModification", type="datetimez")
     * @Serializer\Type("DateTime")
     * @Serializer\Expose()
     */
    protected $timestamp;

    /**
     * @var string
     *
     * @APIConnector\Column(name="Name", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $name;

    /**
     * @var string
     *
     * @APIConnector\Column(name="Category", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $category;

    /**
     * @var string
     *
     * @APIConnector\Column(name="FileType", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $fileType;

    /**
     * @var string
     *
     * @APIConnector\Column(name="Restriction", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $restriction;

    /**
     * @var string
     *
     * @APIConnector\Column(name="RefRecId", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $refRecId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="FileName", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $fileName;

    /**
     * @var string
     *
     * @APIConnector\Column(name="RefTableId", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $refTableId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="TypeId", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $typeId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DefaultAttachment", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $defaultAttachment;

    /**
     * @var string
     *
     * @APIConnector\Column(name="Notes", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $notes;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @APIConnector\Column(name="dataAreaId", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $dataAreaId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="RefCompanyId", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $refCompanyId;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @APIConnector\Column(name="DocumentId", type="string", guid="id")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $documentId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="FileContents", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $fileContents;

    /**
     * @return \DateTime
     */
    public function getTimestamp(): ?\DateTime
    {
        return $this->timestamp;
    }

    /**
     * @param \DateTime $timestamp
     *
     * @return DocCategory
     */
    public function setTimestamp(?\DateTime $timestamp)
    {
        return $this->setTrigger('timestamp', $timestamp);
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return DocCategory
     */
    public function setName(string $name): DocCategory
    {
        return $this->setTrigger('name', $name);
    }

    /**
     * @return string
     */
    public function getCategory(): ?string
    {
        return $this->category;
    }

    /**
     * @param string $category
     *
     * @return DocCategory
     */
    public function setCategory(string $category): DocCategory
    {
        return $this->setTrigger('category', $category);
    }

    /**
     * @return string
     */
    public function getFileType(): ?string
    {
        return $this->fileType;
    }

    /**
     * @param string $fileType
     *
     * @return DocCategory
     */
    public function setFileType(string $fileType): DocCategory
    {
        return $this->setTrigger('fileType', strtolower($fileType));
    }

    /**
     * @return string
     */
    public function getRestriction(): ?string
    {
        return $this->restriction;
    }

    /**
     * @param string $restriction
     *
     * @return DocCategory
     */
    public function setRestriction(string $restriction): DocCategory
    {
        return $this->setTrigger('restriction', $restriction);
    }

    /**
     * @return string
     */
    public function getRefRecId(): ?string
    {
        return $this->refRecId;
    }

    /**
     * @param string $refRecId
     *
     * @return DocCategory
     */
    public function setRefRecId(string $refRecId): DocCategory
    {
        return $this->setTrigger('refRecId', $refRecId);
    }

    /**
     * @return string
     */
    public function getFileName(): ?string
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     *
     * @return DocCategory
     */
    public function setFileName(string $fileName): DocCategory
    {
        return $this->setTrigger('fileName', $fileName);
    }

    /**
     * @return string
     */
    public function getRefTableId(): ?string
    {
        return $this->refTableId;
    }

    /**
     * @param string $refTableId
     *
     * @return DocCategory
     */
    public function setRefTableId(string $refTableId): DocCategory
    {
        return $this->setTrigger('refTableId', $refTableId);
    }

    /**
     * @return string
     */
    public function getTypeId(): ?string
    {
        return $this->typeId;
    }

    /**
     * @param string $typeId
     *
     * @return DocCategory
     */
    public function setTypeId(string $typeId): DocCategory
    {
        return $this->setTrigger('typeId', $typeId);
    }

    /**
     * @return string
     */
    public function getDefaultAttachment(): ?string
    {
        return $this->defaultAttachment;
    }

    /**
     * @param string $defaultAttachment
     *
     * @return DocCategory
     */
    public function setDefaultAttachment(string $defaultAttachment): DocCategory
    {
        return $this->setTrigger('defaultAttachment', $defaultAttachment);
    }

    /**
     * @return string
     */
    public function getNotes(): ?string
    {
        return $this->notes;
    }

    /**
     * @param string $notes
     *
     * @return DocCategory
     */
    public function setNotes(string $notes): DocCategory
    {
        return $this->setTrigger('notes', $notes);
    }

    /**
     * @return string
     */
    public function getDataAreaId(): ?string
    {
        return $this->dataAreaId;
    }

    /**
     * @param string $dataAreaId
     *
     * @return DocCategory
     */
    public function setDataAreaId(string $dataAreaId): DocCategory
    {
        return $this->setTrigger('dataAreaId', $dataAreaId);
    }

    /**
     * @return string
     */
    public function getRefCompanyId(): ?string
    {
        return $this->refCompanyId;
    }

    /**
     * @param string $refCompanyId
     *
     * @return DocCategory
     */
    public function setRefCompanyId(string $refCompanyId): DocCategory
    {
        return $this->setTrigger('refCompanyId', $refCompanyId);
    }

    /**
     * @return string
     */
    public function getDocumentId(): ?string
    {
        return $this->documentId;
    }

    /**
     * @param string $documentId
     *
     * @return DocCategory
     */
    public function setDocumentId(string $documentId): DocCategory
    {
        return $this->setTrigger('documentId', $documentId);
    }

    /**
     * @return string
     */
    public function getFileContents(): ?string
    {
        return $this->fileContents;
    }

    /**
     * @param string $fileContents
     *
     * @return DocCategory
     */
    public function setFileContents(string $fileContents): DocCategory
    {
        return $this->setTrigger('fileContents', $fileContents);
    }
}
