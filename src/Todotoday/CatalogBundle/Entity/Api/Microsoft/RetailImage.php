<?php declare(strict_types=1);

/**
 * PHP version 7
 *
 * Created by
 * User: Pix
 * Date: 04/02/2021
 * Time: 15:00
 */

namespace Todotoday\CatalogBundle\Entity\Api\Microsoft;

use Actiane\ApiConnectorBundle\Lib\EntityProxyTrait;
use Actiane\ApiConnectorBundle\Lib\Mapping as APIConnector;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class RetailImage
 *
 * @SuppressWarnings(PHPMD)
 * @APIConnector\Entity(path="InventTableRetailImages", repositoryId="todotoday.catalog.repository.api.retail_image")
 *
 * @category   Todo-Todev
 * @package    Todotoday\CatalogBundle
 * @subpackage Todotoday\CatalogBundle\Entity\Api\Microsoft
 * @author     Pix Digital <technique@pix-digital.com>
 * @Serializer\ExclusionPolicy("all")
 */
class RetailImage
{
    use EntityProxyTrait;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DocuRefModifiedDateTime", type="datetimez")
     * @Serializer\Type("DateTime")
     * @Serializer\Expose()
     */
    protected $imageModifiedDateTime;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @APIConnector\Column(name="dataAreaId", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $dataAreaId;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @APIConnector\Column(name="ItemId", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $itemId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="DocuRefRecId", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $docuRefRecId;

    /**
     * @var string
     *
     * @APIConnector\Column(name="FileContents", type="string")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $fileContents;

    /**
     * @return \DateTime
     */
    public function getImageModifiedDateTime(): ?\DateTime
    {
        return $this->imageModifiedDateTime;
    }

    /**
     * @param \DateTime $imageModifiedDateTime
     *
     * @return RetailImage
     */
    public function setImageModifiedDateTime(?\DateTime $imageModifiedDateTime)
    {
        return $this->setTrigger('imageModifiedDateTime', $imageModifiedDateTime);
    }

    /**
     * @return string
     */
    public function getDataAreaId(): ?string
    {
        return $this->dataAreaId;
    }

    /**
     * @param string $dataAreaId
     *
     * @return RetailImage
     */
    public function setDataAreaId(string $dataAreaId): RetailImage
    {
        return $this->setTrigger('dataAreaId', $dataAreaId);
    }

    /**
     * @return string
     */
    public function getDocuRefRecId(): ?string
    {
        return $this->docuRefRecId;
    }

    /**
     * @param string $docuRefRecId
     *
     * @return RetailImage
     */
    public function setDocuRefRecId(string $docuRefRecId): RetailImage
    {
        return $this->setTrigger('docuRefRecId', $docuRefRecId);
    }

    /**
     * @return string
     */
    public function getItemId(): ?string
    {
        return $this->itemId;
    }

    /**
     * @param string $itemId
     *
     * @return RetailImage
     */
    public function setItemId(string $itemId): RetailImage
    {
        return $this->setTrigger('itemId', $itemId);
    }

    /**
     * @return string
     */
    public function getFileContents(): ?string
    {
        return $this->fileContents;
    }

    /**
     * @param string $fileContents
     *
     * @return RetailImage
     */
    public function setFileContents(string $fileContents): RetailImage
    {
        return $this->setTrigger('fileContents', $fileContents);
    }
}
