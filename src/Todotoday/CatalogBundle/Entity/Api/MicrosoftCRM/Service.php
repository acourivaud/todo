<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 13/04/17
 * Time: 11:18
 */

namespace Todotoday\CatalogBundle\Entity\Api\MicrosoftCRM;

use Actiane\ApiConnectorBundle\Lib\EntityProxyTrait;
use Actiane\ApiConnectorBundle\Lib\Mapping as APIConnector;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class Service
 * @SuppressWarnings(PHPMD)
 * @Serializer\ExclusionPolicy("all")
 * @APIConnector\Entity(path="services", repositoryId="todotoday.catalog.repository.api.service")
 * @package Todotoday\CatalogBundle\Entity\Api\MicrosoftCRM
 */
class Service
{
    use EntityProxyTrait;

    /**
     * @var string
     *
     * @APIConnector\Column(name="showresources", type="string")
     * @Serializer\Expose()
     */
    protected $showresources;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_organizationid_value", type="string")
     * @Serializer\Expose()
     */
    protected $organizationidValue;

    /**
     * @var int
     *
     * @APIConnector\Column(name="duration", type="int")
     * @Serializer\Expose()
     */
    protected $duration;

    /**
     * @var string
     *
     * @APIConnector\Column(name="isschedulable", type="string")
     * @Serializer\Expose()
     */
    protected $isschedulable;

    /**
     * @var int
     *
     * @APIConnector\Column(name="initialstatuscode", type="int")
     * @Serializer\Expose()
     */
    protected $initialstatuscode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_createdby_value", type="string")
     * @Serializer\Expose()
     */
    protected $createdbyValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_strategyid_value", type="string")
     * @Serializer\Expose()
     */
    protected $strategyidValue;

    /**
     * @var int
     *
     * @APIConnector\Column(name="anchoroffset", type="int")
     * @Serializer\Expose()
     */
    protected $anchoroffset;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="createdon", type="datetimez")
     * @Serializer\Expose()
     */
    protected $createdon;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_modifiedby_value", type="string")
     * @Serializer\Expose()
     */
    protected $modifiedbyValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="isvisible", type="string")
     * @Serializer\Expose()
     */
    protected $isvisible;

    /**
     * @var int
     *
     * @APIConnector\Column(name="versionnumber", type="int")
     * @Serializer\Expose()
     */
    protected $versionnumber;

    /**
     * @var string
     *
     * @APIConnector\Id()
     * @APIConnector\Column(name="serviceid", type="string")
     * @Serializer\Expose()
     */
    protected $serviceid;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="modifiedon", type="datetimez")
     * @Serializer\Expose()
     */
    protected $modifiedon;

    /**
     * @var string
     *
     * @APIConnector\Column(name="name", type="string")
     * @Serializer\Expose()
     */
    protected $name;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_resourcespecid_value", type="string")
     * @Serializer\Expose()
     */
    protected $resourcespecidValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="granularity", type="string")
     * @Serializer\Expose()
     */
    protected $granularity;

    /**
     * @var string
     *
     * @APIConnector\Column(name="calendarid", type="string")
     * @Serializer\Expose()
     */
    protected $calendarid;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_createdonbehalfby_value", type="string")
     * @Serializer\Expose()
     */
    protected $createdonbehalfbyValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="overriddencreatedon", type="string")
     * @Serializer\Expose()
     */
    protected $overriddencreatedon;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_modifiedonbehalfby_value", type="string")
     * @Serializer\Expose()
     */
    protected $modifiedonbehalfbyValue;

    /**
     * @var int
     *
     * @APIConnector\Column(name="importsequencenumber", type="int")
     * @Serializer\Expose()
     */
    protected $importsequencenumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="description", type="string")
     * @Serializer\Expose()
     */
    protected $description;

    /**
     * @return string
     */
    public function getShowresources(): string
    {
        return $this->showresources;
    }

    /**
     * @param string $showresources
     *
     * @return Service
     */
    public function setShowresources(string $showresources): Service
    {
        return $this->setTrigger('showresources', $showresources);
    }

    /**
     * @return string
     */
    public function getOrganizationidValue(): string
    {
        return $this->organizationidValue;
    }

    /**
     * @param string $organizationidValue
     *
     * @return Service
     */
    public function setOrganizationidValue(string $organizationidValue): Service
    {
        return $this->setTrigger('organizationidValue', $organizationidValue);
    }

    /**
     * @return int
     */
    public function getDuration(): int
    {
        return $this->duration;
    }

    /**
     * @param int $duration
     *
     * @return Service
     */
    public function setDuration(int $duration): Service
    {
        return $this->setTrigger('duration', $duration);
    }

    /**
     * @return string
     */
    public function getIsschedulable(): string
    {
        return $this->isschedulable;
    }

    /**
     * @param string $isschedulable
     *
     * @return Service
     */
    public function setIsschedulable(string $isschedulable): Service
    {
        return $this->setTrigger('isschedulable', $isschedulable);
    }

    /**
     * @return int
     */
    public function getInitialstatuscode(): int
    {
        return $this->initialstatuscode;
    }

    /**
     * @param int $initialstatuscode
     *
     * @return Service
     */
    public function setInitialstatuscode(int $initialstatuscode): Service
    {
        return $this->setTrigger('initialstatuscode', $initialstatuscode);
    }

    /**
     * @return string
     */
    public function getCreatedbyValue(): string
    {
        return $this->createdbyValue;
    }

    /**
     * @param string $createdbyValue
     *
     * @return Service
     */
    public function setCreatedbyValue(string $createdbyValue): Service
    {
        return $this->setTrigger('createdbyValue', $createdbyValue);
    }

    /**
     * @return string
     */
    public function getStrategyidValue(): string
    {
        return $this->strategyidValue;
    }

    /**
     * @param string $strategyidValue
     *
     * @return Service
     */
    public function setStrategyidValue(string $strategyidValue): Service
    {
        return $this->setTrigger('strategyidValue', $strategyidValue);
    }

    /**
     * @return int
     */
    public function getAnchoroffset(): int
    {
        return $this->anchoroffset;
    }

    /**
     * @param int $anchoroffset
     *
     * @return Service
     */
    public function setAnchoroffset(int $anchoroffset): Service
    {
        return $this->setTrigger('anchoroffset', $anchoroffset);
    }

    /**
     * @return \DateTime
     */
    public function getCreatedon(): \DateTime
    {
        return $this->createdon;
    }

    /**
     * @param \DateTime $createdon
     *
     * @return Service
     */
    public function setCreatedon(\DateTime $createdon): Service
    {
        return $this->setTrigger('createdon', $createdon);
    }

    /**
     * @return string
     */
    public function getModifiedbyValue(): string
    {
        return $this->modifiedbyValue;
    }

    /**
     * @param string $modifiedbyValue
     *
     * @return Service
     */
    public function setModifiedbyValue(string $modifiedbyValue): Service
    {
        return $this->setTrigger('modifiedbyValue', $modifiedbyValue);
    }

    /**
     * @return string
     */
    public function getIsvisible(): string
    {
        return $this->isvisible;
    }

    /**
     * @param string $isvisible
     *
     * @return Service
     */
    public function setIsvisible(string $isvisible): Service
    {
        return $this->setTrigger('isvisible', $isvisible);
    }

    /**
     * @return int
     */
    public function getVersionnumber(): int
    {
        return $this->versionnumber;
    }

    /**
     * @param int $versionnumber
     *
     * @return Service
     */
    public function setVersionnumber(int $versionnumber): Service
    {
        return $this->setTrigger('versionnumber', $versionnumber);
    }

    /**
     * @return string
     */
    public function getServiceid(): string
    {
        return $this->serviceid;
    }

    /**
     * @param string $serviceid
     *
     * @return Service
     */
    public function setServiceid(string $serviceid): Service
    {
        return $this->setTrigger('serviceid', $serviceid);
    }

    /**
     * @return \DateTime
     */
    public function getModifiedon(): \DateTime
    {
        return $this->modifiedon;
    }

    /**
     * @param \DateTime $modifiedon
     *
     * @return Service
     */
    public function setModifiedon(\DateTime $modifiedon): Service
    {
        return $this->setTrigger('modifiedon', $modifiedon);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Service
     */
    public function setName(string $name): Service
    {
        return $this->setTrigger('name', $name);
    }

    /**
     * @return string
     */
    public function getResourcespecidValue(): string
    {
        return $this->resourcespecidValue;
    }

    /**
     * @param string $resourcespecidValue
     *
     * @return Service
     */
    public function setResourcespecidValue(string $resourcespecidValue): Service
    {
        return $this->setTrigger('resourcespecidValue', $resourcespecidValue);
    }

    /**
     * @return string
     */
    public function getGranularity(): string
    {
        return $this->granularity;
    }

    /**
     * @param string $granularity
     *
     * @return Service
     */
    public function setGranularity(string $granularity): Service
    {
        return $this->setTrigger('granularity', $granularity);
    }

    /**
     * @return string
     */
    public function getCalendarid(): string
    {
        return $this->calendarid;
    }

    /**
     * @param string $calendarid
     *
     * @return Service
     */
    public function setCalendarid(string $calendarid): Service
    {
        return $this->setTrigger('calendarid', $calendarid);
    }

    /**
     * @return string
     */
    public function getCreatedonbehalfbyValue(): string
    {
        return $this->createdonbehalfbyValue;
    }

    /**
     * @param string $createdonbehalfbyValue
     *
     * @return Service
     */
    public function setCreatedonbehalfbyValue(string $createdonbehalfbyValue): Service
    {
        return $this->setTrigger('createdonbehalfbyValue', $createdonbehalfbyValue);
    }

    /**
     * @return string
     */
    public function getOverriddencreatedon(): string
    {
        return $this->overriddencreatedon;
    }

    /**
     * @param string $overriddencreatedon
     *
     * @return Service
     */
    public function setOverriddencreatedon(string $overriddencreatedon): Service
    {
        return $this->setTrigger('overriddencreatedon', $overriddencreatedon);
    }

    /**
     * @return string
     */
    public function getModifiedonbehalfbyValue(): string
    {
        return $this->modifiedonbehalfbyValue;
    }

    /**
     * @param string $modifiedonbehalfbyValue
     *
     * @return Service
     */
    public function setModifiedonbehalfbyValue(string $modifiedonbehalfbyValue): Service
    {
        return $this->setTrigger('modifiedonbehalfbyValue', $modifiedonbehalfbyValue);
    }

    /**
     * @return int
     */
    public function getImportsequencenumber(): int
    {
        return $this->importsequencenumber;
    }

    /**
     * @param int $importsequencenumber
     *
     * @return Service
     */
    public function setImportsequencenumber(int $importsequencenumber): Service
    {
        return $this->setTrigger('importsequencenumber', $importsequencenumber);
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return Service
     */
    public function setDescription(string $description): Service
    {
        return $this->setTrigger('description', $description);
    }
}
