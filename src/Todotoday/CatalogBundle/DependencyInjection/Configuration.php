<?php declare(strict_types = 1);

namespace Todotoday\CatalogBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('todotoday_catalog');

        $rootNode->append($this->getClearCacheRules());
        $rootNode->append($this->getImgRetailImportRules());

        return $treeBuilder;
    }

    /**
     * @return \Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition|\Symfony\Component\Config\Definition\Builder\NodeDefinition
     */
    protected function getClearCacheRules()
    {
        $treeBuilder = new TreeBuilder();
        $node = $treeBuilder->root('clearcache_rules');

        /** @var ArrayNodeDefinition|NodeDefinition $clearcacheRuleNode */
        $clearcacheRuleNode = $node
            ->requiresAtLeastOneElement()
            ->useAttributeAsKey('name')
            ->prototype('array');

        $clearcacheRuleNode
            ->children()
            ->scalarNode('firstExecution')->end()
            ->scalarNode('secondExecution')->end()
            ->end();

        return $node;
    }

    /**
     * @return \Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition|\Symfony\Component\Config\Definition\Builder\NodeDefinition
     */
    protected function getImgRetailImportRules()
    {
        $treeBuilder = new TreeBuilder();
        $node = $treeBuilder->root('imgretail_import_rules');

        /** @var ArrayNodeDefinition|NodeDefinition $clearcacheRuleNode */
        $clearcacheRuleNode = $node
            ->requiresAtLeastOneElement()
            ->useAttributeAsKey('name')
            ->prototype('array');

        $clearcacheRuleNode
            ->children()
            ->scalarNode('firstExecution')->end()
            ->scalarNode('secondExecution')->end()
            ->end();

        return $node;
    }
}
