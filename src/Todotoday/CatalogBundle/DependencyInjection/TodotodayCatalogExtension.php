<?php declare(strict_types = 1);

namespace Todotoday\CatalogBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class TodotodayCatalogExtension extends Extension
{
    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('admin.yml');
        $loader->load('services.yml');
        $loader->load('repository_api_microsoft.yml');

        $catalogManager = $container->getDefinition('todotoday_catalog.catalog_manager');
        $catalogManager->addMethodCall('setClearCacheRules', array($config['clearcache_rules']));

        $imageManager = $container->getDefinition('todotoday_catalog.image_manager');
        $imageManager->addMethodCall('setImgRetailImportRules', array($config['imgretail_import_rules']));
    }
}
