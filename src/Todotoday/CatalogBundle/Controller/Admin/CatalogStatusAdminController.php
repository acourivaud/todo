<?php declare(strict_types=1);

namespace Todotoday\CatalogBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Repository\AgencyRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Todotoday\AccountBundle\Entity\Account;
use DateTime;
use Exception;

/**
 * Class CatalogStatusAdminController
 * @package Todotoday\CatalogBundle\Controller\Admin
 */
class CatalogStatusAdminController extends CRUDController
{

    /**
     *
     * @return Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \LogicException
     */
    public function listAction(): Response
    {
        if (!$this->isGranted('VIEW', $this)) {
            throw $this->createAccessDeniedException('Fail');
        }

        return $this->renderWithExtraParams('TodotodayCatalogBundle:Admin/Catalog/Status:list.html.twig');
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \LogicException
     */
    public function refreshImagesAction(Request $request): RedirectResponse
    {
        if (!$request->isMethod('post')) {
            throw $this->createNotFoundException();
        }

        $catalogImageManager = $this->get('todotoday_catalog.image_manager');
        $translator = $this->get('translator');

        try {
            $date = $request->get('date');
            //$images = $catalogImageManager->createAllCatalogCategoriesImages();
            $images = $catalogImageManager->manualCreateAllCatalogCategoriesImages(null, null, $date);

            if ($images) {
                $this->addFlash('success', $translator->trans('catalog.status.refresh_success', [
                    '%countImages%' => count($images),
                ], 'todotoday'));
            } else {
                $this->addFlash('warning', $translator->trans('catalog.status.refresh_null', [], 'todotoday'));
            }
        } catch (Exception $exception) {
            $this->addFlash('error', $translator->trans('catalog.status.refresh_error', [], 'todotoday'));
        }

        return new RedirectResponse(
            $this->admin->generateUrl('list')
        );
    }
}
