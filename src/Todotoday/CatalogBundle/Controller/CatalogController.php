<?php declare(strict_types=1);

namespace Todotoday\CatalogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\PluginBundle\Enum\PluginCategoryEnum;

/**
 * Class CatalogController
 * @package Todotoday\CatalogBundle\Controller
 */
class CatalogController extends Controller
{
    /**
     * @Route("/catalog/plugin/{pluginSlug}", options={"expose"=true}, name="todotoday_catalog_plugin")
     *
     * @param string $pluginSlug
     *
     * @return Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Todotoday\PluginBundle\Exceptions\PluginNotDefinedException
     * @throws \Todotoday\PluginBundle\Exceptions\PluginNotFoundException
     * @throws \UnexpectedValueException
     * @throws \BadMethodCallException
     * @throws \InvalidArgumentException
     */
    public function pluginAction(string $pluginSlug): Response
    {
        $agency = $this->getAgency();
        $pluginManager = $this->get('todotoday.plugin.plugin_manager');
        $view = $pluginManager->getPluginView(
            $agency,
            PluginCategoryEnum::CATALOG_PLUGIN,
            PluginCategoryEnum::CATALOG_PLUGIN . '.' . $pluginSlug
        );

        return $this->render(
            $view['view'],
            $view['parameters']
        );
    }

    /**
     * @Route("/catalog/{hierarchyFamily}", options={"expose"=true}, name="todotoday_catalog_index")
     *
     * @param null $hierarchyFamily
     *
     * @return Response
     */
    public function indexAction($hierarchyFamily = null): Response
    {
        return $this->render('TodotodayCatalogBundle:Default:index.html.twig');
    }

    /**
     * @return Agency
     * @throws \UnexpectedValueException
     * @throws \InvalidArgumentException
     */
    private function getAgency(): Agency
    {
        if (!$agency = $this->get('todotoday.core.domain_context.agency')->getAgency()) {
            throw new \InvalidArgumentException('Agency is missing');
        }

        return $agency;
    }
}
