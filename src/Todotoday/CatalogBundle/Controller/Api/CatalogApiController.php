<?php

declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 06/03/17
 * Time: 11:56
 */

namespace Todotoday\CatalogBundle\Controller\Api;

use FOS\RestBundle\Controller\Annotations as FosRest;
use FOS\RestBundle\Request\ParamFetcher;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Todotoday\CatalogBundle\Entity\Api\Microsoft\RetailCatalog;
use Todotoday\CoreBundle\Controller\Api\AbstractApiController;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Exceptions\AgencyMissingException;

/**
 * Class CatalogApiController
 *
 * @package Todotoday\CatalogBundle\Controller\Api
 * @FosRest\NamePrefix("todotoday.catalog.api.catalog.")
 * @FosRest\Prefix(value="/catalog")
 */
class CatalogApiController extends AbstractApiController
{
    /**
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Retrieve catalog for current agency",
     *     description="Retrieve catalog for current agency",
     *     output={
     *           "class"="Todotoday\CatalogBundle\Entity\Api\Microsoft\RetailCatalog",
     *           "groups"={"Default"}
     *     },
     *     views={"Catalog"},
     *     section="Catalog"
     * )
     *
     * @FosRest\Route("", options={"expose"=true})
     * @FosRest\QueryParam(
     *     name="fields",
     *     requirements="((Default),?)*",
     *     default="",
     *     description="Fields you need"
     * )
     *
     * @param ParamFetcher $paramFetcher
     *
     * @param Request      $request
     *
     * @return Response
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \LogicException
     * @throws \UnexpectedValueException
     * @throws AgencyMissingException
     * @throws \Exception
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     */
    public function getAllAction(ParamFetcher $paramFetcher, Request $request): Response
    {
        if (!$this->isGranted('VIEW', $this)) {
            throw $this->createAccessDeniedException();
        }

        $locale = $request->getLocale();

        $catalogManager = $this->get('todotoday_catalog.catalog_manager');
        $catalogType = $catalogManager->getTypeWithRole($this->isGranted('ROLE_NOT_ADHERENT', $this));
        /** @var RetailCatalog $retailCatalog */
        $retailCatalog = $catalogManager->getCatalog($this->getAgency(), $locale, $catalogType);
        // agency_data_area will be used by catalog front Vue app to display price excluding taxes
        $retailCatalog['agency_data_area'] = $this->getAgency()->getDataAreaId();

        $view = $this->view($retailCatalog);

        $groups = $this->getDefaultGroups();
        if ($fields = explode(',', $paramFetcher->get('fields'))) {
            $groups = array_merge($groups, $fields);
        }
        $view->getContext()->addGroups($groups);
        //dump($retailCatalog);die();
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Get product",
     *     description="Get Product",
     *     output={
     *           "class"="Todotoday\CatalogBundle\Entity\Api\Microsoft\RetailCatalog",
     *           "groups"={"Default"}
     *     },
     *     views={"Catalog"},
     *     section="Catalog"
     * )
     *
     * @FosRest\Route("/product/{productId}", requirements={"productId" = "\w+"})
     * @FosRest\QueryParam(
     *     name="fields",
     *     requirements="((Default),?)*",
     *     default="",
     *     description="Fields you need"
     * )
     *
     * @param string       $productId
     * @param Request      $request
     * @param ParamFetcher $paramFetcher
     *
     * @return Response
     * @throws \LogicException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \UnexpectedValueException
     * @throws AgencyMissingException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Todotoday\CatalogBundle\Exceptions\ProductNotFoundException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     */
    public function getProductAction(string $productId, Request $request, ParamFetcher $paramFetcher): Response
    {
        if (!$this->isGranted('VIEW', $this)) {
            throw $this->createAccessDeniedException();
        }

        $locale = $request->getLocale();
        $manager = $this->get('todotoday_catalog.catalog_manager');
        $result = $manager->getProduct($this->getAgency(), $productId, $locale);

        $view = $this->view($result);
        $groups = $this->getDefaultGroups();
        if ($fields = explode(',', $paramFetcher->get('fields'))) {
            $groups = array_merge($groups, $fields);
        }
        $view->getContext()->addGroups($groups);
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * @FosRest\Route("/cache-reset",options={"expose"=true})
     *
     * @param Request $request
     *
     * @return Response
     * @throws \InvalidArgumentException
     * @throws \LogicException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \UnexpectedValueException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     */
    public function postResetCatalogCacheAction(Request $request): Response
    {
        if (!$this->isGranted('CREATE', $this)) {
            throw $this->createAccessDeniedException();
        }

        try {
            $agency = $this->getAgency();
        } catch (AgencyMissingException $e) {
            $agency = null;
        }

        $manager = $this->get('todotoday_catalog.catalog_manager');

        $lang = $request->get('Language') ?: null;

        $manager->resetCache($agency, $lang);

        return new Response();
    }

    /**
     * Get the formType for the current entity class link to the controller
     *
     * @return string
     */
    protected function getFormType(): string
    {
        return '';
    }

    /**
     * Get the serivce name from custum repository's entity
     *
     * @return string
     */
    protected function getServiceRepository(): string
    {
        return 'todotoday.catalog.repository.api.retail_catalog';
    }

    /**
     * @return null|Agency
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \UnexpectedValueException
     * @throws AgencyMissingException
     */
    private function getAgency(): ?Agency
    {
        if (!$agency = $this->get('todotoday.core.domain_context.api')->getAgency()) {
            throw new AgencyMissingException('Agency is missing');
        }

        return $agency;
    }
}
