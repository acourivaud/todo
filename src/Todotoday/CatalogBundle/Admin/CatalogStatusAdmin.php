<?php declare(strict_types=1);

namespace Todotoday\CatalogBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Route\RouteCollection;

/**
 * Class CatalogStatusAdmin
 *
 * @package Todotoday\CatalogBundle\Admin
 */
final class CatalogStatusAdmin extends AbstractAdmin
{
    protected $baseRoutePattern = 'todotoday/catalog/status';
    protected $baseRouteName = 'admin_todotoday_catalog_status';

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('delete')
            ->remove('create')
            ->remove('batch')
            ->remove('show')
            ->remove('export')
            ->remove('acl')
            ->remove('edit')
            ->add('refresh_images');
    }
}
