<?php declare(strict_types=1);

/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 01/03/2017
 * Time: 12:40
 */

namespace Todotoday\CatalogBundle\Tests\Repository\Api\MicrosoftCRM;

use Actiane\ApiConnectorBundle\Tests\Repository\Api\AbstractRepositoryTestCase;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use Todotoday\CatalogBundle\Repository\Api\MicrosoftCRM\ServiceRepository;

/**
 * Class ServiceRepositoryTest
 *
 * @category   Todo-Todev
 * @package    Todotoday\CatalogBundle
 * @subpackage Todotoday\CatalogBundle\Tests\Repository\Api\Microsoft
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class ServiceRepositoryTest extends WebTestCase
{
    /**
     * @small
     */
    public function testConnection()
    {
        /** @var ServiceRepository $repo */
        $repo = $this->getContainer()->get($this->getRepositoryId());
        $result = $repo->findAll(5);
        $this->assertNotNull($result);
    }

    /**
     * Do getRepositoryId
     *
     * @return string
     */
    protected function getRepositoryId(): string
    {
        return 'todotoday.catalog.repository.api.service';
    }
}
