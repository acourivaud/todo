<?php declare(strict_types=1);

/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 01/03/2017
 * Time: 12:40
 */

namespace Todotoday\CatalogBundle\Tests\Repository\Api\Microsoft;

use Actiane\ApiConnectorBundle\Tests\Repository\Api\AbstractRepositoryTestCase;
use Todotoday\CatalogBundle\Entity\Api\Microsoft\RetailCatalog;
use Todotoday\CatalogBundle\Repository\Api\Microsoft\RetailCatalogRepository;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class RetailCatalogsRepositoryTest
 *
 * @category   Todo-Todev
 * @package    Todotoday\CatalogBundle
 * @subpackage Todotoday\CatalogBundle\Tests\Repository\Api\Microsoft
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class RetailCatalogsRepositoryTest extends AbstractRepositoryTestCase
{
    /**
     * @small
     */
    public function testGetFirstService(): void
    {
        /** @var RetailCatalogRepository $repo */
        $repo = $this->getContainer()->get($this->getRepositoryId());
        /** @var RetailCatalog[] $result */
        static::assertNotNull($repo->getServiceRdv('carrefour-massy')[0]->getCrmIdService());
    }

    /**
     * @small
     * @throws \Doctrine\Common\DataFixtures\OutOfBoundsException
     */
    public function testGetCatalogFromAgencyAndLang(): void
    {
        /** @var RetailCatalogRepository $repo */
        $repo = $this->getContainer()->get($this->getRepositoryId());
        /** @var RetailCatalog[] $result */
        /** @var Agency $agency */
        $result = $repo->findCatalogByAgencyAndLang('carrefour-massy', 'fr');
        static::assertEquals('carrefour-massy', $result[0]->getIdActiane());
        static::assertEquals('fr', $result[0]->getLanguageTrans());
        static::assertNotNull($result);
    }

    /**
     * Do getRepositoryId
     *
     * @return string
     */
    protected function getRepositoryId(): string
    {
        return 'todotoday.catalog.repository.api.retail_catalog';
    }
}
