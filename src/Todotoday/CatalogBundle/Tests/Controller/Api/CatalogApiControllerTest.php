<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 06/03/17
 * Time: 13:17
 */

namespace Todotoday\CatalogBundle\Tests\Repository\Api;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAccountData;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAdherentLinkedMicrosoftData;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyData;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class CatalogApiControllerTest
 *
 * @package Todotoday\CatalogBundle\Tests\Repository\Api
 */
class CatalogApiControllerTest extends WebTestCase
{
    /**
     * @small
     * @throws \Doctrine\Common\DataFixtures\OutOfBoundsException
     * @throws \Doctrine\Common\DataFixtures\OutOfBoundsException
     */
    public function testFailedGetUnknownProduct(): void
    {
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent_linked_arevalta');
        /** @var Agency $agency */
        $agency = self::getFRR()->getReference('agency_arevalta');
        $this->loginAs($adherent, 'api');
        $url = $this->getUrl(
            'todotoday.catalog.api.catalog.get_product',
            array('productId' => '1337')
        );
        $client = $this->makeClient();
        $client->request('GET', $url, array(), array(), array('HTTP_AGENCY' => $agency->getSlug()));
        $response = json_decode($client->getResponse()->getContent());
        $this->assertStatusCode(400, $client);
        $this->assertEquals('Product not found', $response->error->message);
    }

    /**
     * @small
     * @throws \Doctrine\Common\DataFixtures\OutOfBoundsException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Todotoday\CatalogBundle\Exceptions\ProductNotFoundException
     */
    public function testGetProductSuccess(): void
    {
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent_linked_arevalta');
        /** @var Agency $agency */
        $agency = self::getFRR()->getReference('agency_arevalta');
        $this->loginAs($adherent, 'api');
        $catalog = $this->getContainer()->get('todotoday_catalog.catalog_manager')->getAllProducts($agency, 'fr');
        $url = $this->getUrl(
            'todotoday.catalog.api.catalog.get_product',
            array('productId' => $catalog[0]->getItemId())
        );
        $client = $this->makeClient();
        $client->request('GET', $url, array(), array(), array('HTTP_AGENCY' => $agency->getSlug()));
        $response = json_decode($client->getResponse()->getContent(), true);
        $this->assertNotNull($response);
        $this->assertEquals($agency->getSlug(), $response['id_actiane']);
        $this->assertEquals($catalog[0]->getItemId(), $response['item_id']);
    }

    /**
     * @dataProvider agencyProvider
     *
     * Test success for get Catalog but empty
     *
     * @small
     *
     * @param string $agencySlug
     * @param string $adherentSlug
     *
     * @throws \Doctrine\Common\DataFixtures\OutOfBoundsException
     */
    public function testGetCatalogEmpty(string $agencySlug, string $adherentSlug): void
    {
        /** @var Agency $agency */
        $agency = self::getFRR()->getReference('agency_' . $agencySlug);
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference($adherentSlug);
        $this->loginAs($adherent, 'api');
        $url = $this->getUrl('todotoday.catalog.api.catalog.get_all');
        $client = $this->makeClient();
        $client->request('GET', $url, array(), array(), array('HTTP_AGENCY' => $agency->getSlug()));
        $this->assertStatusCode(204, $client);
    }

    /**
     * Test success for get Catalog
     *
     * @small
     * @throws \Doctrine\Common\DataFixtures\OutOfBoundsException
     */
    public function testGetCatalogNotEmpty(): void
    {
        /** @var Agency $agency */
        $agency = self::getFRR()->getReference('agency_arevalta');
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent_linked_arevalta');
        $this->loginAs($adherent, 'api');
        $url = $this->getUrl('todotoday.catalog.api.catalog.get_all');
        $client = $this->makeClient();
        $client->request('GET', $url, array(), array(), array('HTTP_AGENCY' => $agency->getSlug()));
        $response = json_decode($client->getResponse()->getContent());
        $this->assertStatusCode(200, $client);
        $this->assertNotNull($response);
    }

    /**
     * @dataProvider agencyProvider
     *
     * Test success for get Catalog
     *
     * @small
     *
     * @param string $agencySlug
     * @param string $adherentSlug
     *
     * @throws \Doctrine\Common\DataFixtures\OutOfBoundsException
     */
    public function testGetCatalogDeniedWithoutAgency(string $agencySlug = null, string $adherentSlug): void
    {
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference($adherentSlug);
        $this->loginAs($adherent, 'api');
        $url = $this->getUrl('todotoday.catalog.api.catalog.get_all');
        $client = $this->makeClient();
        $client->request('GET', $url);
        $response = json_decode($client->getResponse()->getContent());
        $this->assertStatusCode(500, $client);
        $this->assertEquals('Agency is missing', $response->error->message);
    }

    /**
     * Test success for get Catalog
     *
     * @small
     *
     * @throws \Doctrine\Common\DataFixtures\OutOfBoundsException
     */
    public function testGetProductDeniedWithoutAgency(): void
    {
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent_linked_arevalta');
        $this->loginAs($adherent, 'api');
        $url = $this->getUrl('todotoday.catalog.api.catalog.get_product', array('productId' => '1337'));
        $client = $this->makeClient();
        $client->request('GET', $url);
        $response = json_decode($client->getResponse()->getContent());
        $this->assertStatusCode(500, $client);
        $this->assertEquals('Agency is missing', $response->error->message);
    }

    /**
     * @dataProvider partnerProvider
     * @small
     *
     * @param string $slug
     *
     * @throws \Doctrine\Common\DataFixtures\OutOfBoundsException
     */
    public function testGetCatalogDeniedForPartner(string $slug): void
    {
        /** @var Agency $agency */
        $agency = self::getFRR()->getReference('agency_demo0');
        /** @var Adherent $adherent */
        $account = self::getFRR()->getReference($slug);
        $this->loginAs($account, 'api');
        $url = $this->getUrl('todotoday.catalog.api.catalog.get_all');
        $client = $this->makeClient();
        $client->request('GET', $url, array(), array(), array('HTTP_AGENCY' => $agency->getSlug()));
        $this->assertStatusCode(403, $client);
    }

    /**
     * @dataProvider partnerProvider
     * @small
     *
     * @param string $slug
     *
     * @throws \Doctrine\Common\DataFixtures\OutOfBoundsException
     */
    public function testGetProductDeniedForPartner(string $slug): void
    {
        /** @var Agency $agency */
        $agency = self::getFRR()->getReference('agency_demo0');
        /** @var Adherent $adherent */
        $account = self::getFRR()->getReference($slug);
        $this->loginAs($account, 'api');
        $url = $this->getUrl('todotoday.catalog.api.catalog.get_product', array('productId' => '1337'));
        $client = $this->makeClient();
        $client->request('GET', $url, array(), array(), array('HTTP_AGENCY' => $agency->getSlug()));
        $this->assertStatusCode(403, $client);
    }

    /**
     * @param string      $slugAccount
     * @param null|string $slugAgency
     * @param int         $expectedCode
     *
     * @dataProvider catalogResetCacheProvider
     *
     * @throws \Doctrine\Common\DataFixtures\OutOfBoundsException
     */
    public function testResetCatalogCacheFailForAdherent(
        ?string $slugAccount,
        ?string $slugAgency,
        int $expectedCode
    ): void {

        /** @var Account $account */
        if ($slugAccount) {
            $account = self::getFRR()->getReference($slugAccount);
            $this->loginAs($account, 'api');
        }

        $agency = self::getFRR()->getReference('agency_carrefour-massy');

        $client = $this->makeClient();
        $url = $this->getUrl('todotoday.catalog.api.catalog.post_reset_catalog_cache');
        $client->request('POST', $url, [], [], array('HTTP_AGENCY' => $agency->getSlug(), 'HTTP_LANGUAGE' => 'fr'));
        $this->assertStatusCode($expectedCode, $client);
    }

    /**
     * @return array
     */
    public function catalogResetCacheProvider(): array
    {
        return [
//            'anonymous' => [null, null, 401],
//            'adherent0' => ['adherent0', 'agency0', 403],
//            'concierge0' => ['concierge0', 'agency0', 403],
//            'noosphere' => ['noosphere', null, 403],
//            'microsoft' => ['microsoft', null, 403],
            'Community Manager' => ['cm', null, 200],
        ];
    }

    /**
     * @return array
     */
    public function agencyProvider(): array
    {
        $agenciesSlug = LoadAgencyData::getAgenciesSlug();
        $adherent = LoadAccountData::getAdherentsSlugs();
        $provider = [];
        foreach ($agenciesSlug as $key => $agency) {
            $provider[$agency] = array($agency, $adherent[$key]);
        }

        return $provider;
    }

    /**
     * Partners slugs provider
     *
     * @return array
     */
    public function partnerProvider(): array
    {
        $provider = array();
        $partners = LoadAccountData::getPartnersSlugs();
        foreach ($partners as $slug) {
            $provider[$slug] = array($slug);
        }

        return $provider;
    }

    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAccountData::class,
            LoadAgencyData::class,
            LoadAdherentLinkedMicrosoftData::class,
        );
    }
}
