<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 06/03/17
 * Time: 14:10
 */

namespace Todotoday\CatalogBundle\Services;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAdherentLinkedMicrosoftData;
use Todotoday\CatalogBundle\Entity\Api\Microsoft\RetailCatalog;
use Todotoday\CatalogBundle\Exceptions\ProductNotFoundException;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyData;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyLinkedMicrosoft;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class CatalogManagerTest
 * @package Todotoday\CatalogBundle\Services
 */
class CatalogManagerTest extends WebTestCase
{
    /**
     * @var CatalogManager
     */
    private $manager;

    /**
     * @small
     */
    public function testGetAllSuccess(): void
    {
        /** @var Agency $agency */
        $agency = self::getFRR()->getReference('agency_arevalta');
        $result = $this->manager->getCatalog($agency);
        $this->assertNotNull($result);
    }

    /**
     * @small
     */
    public function testGetProduct(): void
    {
        /** @var Agency $agency */
        $agency = self::getFRR()->getReference('agency_arevalta');
        $lang = 'fr';
        $products = $this->manager->getAllProducts($agency, $lang);
        $result = $this->manager->getProduct($agency, $products[0]->getItemId(), $lang);
        $this->assertNotNull($result);
        $this->assertEquals($products[0]->getItemId(), $result->getItemId());
    }

    /**
     * @small
     */
    public function testGetProductNull(): void
    {
        $this->expectException(ProductNotFoundException::class);
        /** @var Agency $agency */
        $agency = self::getFRR()->getReference('agency_arevalta');
        $lang = 'fr';
        $this->manager->getProduct($agency, 'NE DOIS PAS EXISTER LOL', $lang);
    }

    /**
     * @small
     */
    public function testGetAllNull(): void
    {
        /** @var Agency $agency */
        $agency = self::getFRR()->getReference('agency_demo0');
        $result = $this->manager->getCatalog($agency);
        $this->assertNull($result);
    }

    /**
     * setup
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->manager = $this->getContainer()->get('todotoday_catalog.catalog_manager');
    }

    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAgencyData::class,
            LoadAgencyLinkedMicrosoft::class,
            LoadAdherentLinkedMicrosoftData::class,
        );
    }
}
