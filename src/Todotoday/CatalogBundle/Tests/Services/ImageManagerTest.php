<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 06/03/17
 * Time: 14:10
 */

namespace Todotoday\CatalogBundle\Services;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Todotoday\CatalogBundle\Entity\Api\Microsoft\DocCategory;

/**
 * Class ImageManagerTest
 *
 * @package Todotoday\CatalogBundle\Services
 */
class ImageManagerTest extends WebTestCase
{
    /**
     * @var ImageManager
     */
    private $manager;

    /**
     * @group        ignore
     * @dataProvider imageProvider
     * @small
     *
     * @param string|null $docId
     * @param string      $fileType
     * @param string      $binaries
     * @param \DateTime   $timestamp
     */
    public function testCreateAllCatalogCategoriesImages(
        ?string $docId = null,
        string $fileType,
        string $binaries,
        \DateTime $timestamp
    ): void {
        $path = $this->manager->getCatalogCategoriesImagesServerPath() . '/test';
        $absoluteUrl = $this->manager->getCatalogCategoriesImagesAbsoluteUrl();

        if (!is_dir($path)) {
            mkdir($path);
        }
        $fileName = $docId . '.' . $fileType;

        $redis = $this->getContainer()->get('snc_redis.catalog');
        $this->manager->cacheCatalogCategoryInRedis(
            $docId,
            $absoluteUrl . '/' . $fileName . '?t=' . $timestamp->getTimestamp()
        );
        $this->assertSame($redis->exists(ImageManager::CATALOG_PICTOS_REDIS_PREFIX . $docId), 1);
        $redis->del(array(ImageManager::CATALOG_PICTOS_REDIS_PREFIX . $docId));

        $this->manager->createImageWithBinaries(
            $path,
            $fileName,
            $binaries
        );
        $this->assertFileExists($path . '/' . $fileName);

        unlink($path . '/' . $fileName);
    }

    /**
     * setup
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->manager = $this->getContainer()->get('todotoday_catalog.image_manager');
    }

    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return [];
    }

    /**
     * @return array
     */
    public function imageProvider(): array
    {
        $repo = $this->getContainer()->get('todotoday.catalog.repository.api.doc_category');
        $docs = array();

        /** @var DocCategory $catalogCategory */
        foreach ($repo->findAll() as $catalogCategory) {
            $docs['Category:' . $catalogCategory->getCategory()] = array(
                $catalogCategory->getCategory(),
                $catalogCategory->getFileType(),
                $catalogCategory->getFileContents(),
                $catalogCategory->getTimestamp(),
            );
        }

        return $docs;
    }
}
