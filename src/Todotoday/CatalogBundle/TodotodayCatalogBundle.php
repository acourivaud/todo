<?php declare(strict_types = 1);

namespace Todotoday\CatalogBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class TodotodayCatalogBundle extends Bundle
{
}
