<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 08/02/2017
 * Time: 16:03
 */

namespace Todotoday\ClassificationBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Todotoday\ClassificationBundle\Entity\Category;
use Todotoday\ClassificationBundle\Entity\Context;

/**
 * Class LoadCategoryData
 *
 * @package    Todotoday\ClassificationBundle
 * @subpackage Todotoday\ClassificationBundle\DataFixtures\ORM
 */
class LoadCategoryData extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getDependencies(): array
    {
        return array(
            LoadContextData::class,
        );
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        /** @var Context $defaultContext */
        $defaultContext = $this->getReference('default_context');

        $category = (new Category())
            ->setDescription('Lorem ipsum')
            ->setPosition(0)
            ->setEnabled(true)
            ->setName('Actualités')
            ->setContext($defaultContext);
        $manager->persist($category);
        $manager->flush();
        $this->setReference('actualites', $category);

        $category = (new Category())
            ->setDescription('Lorem ipsum')
            ->setPosition(0)
            ->setEnabled(true)
            ->setName('Bons Plans')
            ->setContext($defaultContext);
        $manager->persist($category);
        $manager->flush();
        $this->setReference('bons-plans', $category);

        $category = (new Category())
            ->setDescription('Lorem ipsum')
            ->setPosition(0)
            ->setEnabled(true)
            ->setName('programme')
            ->setContext($defaultContext);
        $manager->persist($category);
        $manager->flush();
        $this->setReference('programme', $category);

        $category = (new Category())
            ->setDescription('Lorem ipsum')
            ->setPosition(0)
            ->setEnabled(true)
            ->setName('I Want More')
            ->setContext($defaultContext);
        $manager->persist($category);
        $manager->flush();
        $this->setReference('i-want-more', $category);

        $category = (new Category())
            ->setDescription('Lorem ipsum')
            ->setPosition(0)
            ->setEnabled(true)
            ->setName('Agency')
            ->setContext($defaultContext);
        $manager->persist($category);
        $manager->flush();
        $this->setReference('agency_category', $category);

        $category = (new Category())
            ->setDescription('Social Context default category')
            ->setPosition(0)
            ->setEnabled(true)
            ->setName('Social Default')
            ->setContext($this->getReference('social_context'));
        $manager->persist($category);
        $manager->flush();
        $this->setReference('social_category', $category);

        $category = (new Category())
            ->setDescription('File Context default category')
            ->setPosition(0)
            ->setEnabled(true)
            ->setName('File Default')
            ->setContext($this->getReference('file_context'));
        $manager->persist($category);
        $manager->flush();
        $this->setReference('file_category', $category);
    }
}
