<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 06/03/2017
 * Time: 11:29
 */

namespace Todotoday\ClassificationBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Todotoday\ClassificationBundle\Entity\Context;

/**
 * Class LoadContextData
 *
 * @category   Todo-Todev
 * @package    Todotoday\ClassificationBundle
 * @subpackage Todotoday\ClassificationBundle\DataFixtures\ORM
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class LoadContextData extends AbstractFixture
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $defaultContext = (new Context())
            ->setId(Context::DEFAULT_CONTEXT)
            ->setName(Context::DEFAULT_CONTEXT)
            ->setEnabled(true);

        $socialContext = (new Context())
            ->setId('social')
            ->setName('social')
            ->setEnabled(true);

        $fileContext = (new Context())
            ->setId('file')
            ->setName('file')
            ->setEnabled(true);

        $manager->persist($defaultContext);
        $manager->persist($socialContext);
        $manager->persist($fileContext);
        $manager->flush();

        $this->setReference('default_context', $defaultContext);
        $this->setReference('social_context', $socialContext);
        $this->setReference('file_context', $fileContext);
    }
}
