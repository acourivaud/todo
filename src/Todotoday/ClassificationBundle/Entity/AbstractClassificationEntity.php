<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 05/02/17
 * Time: 17:11
 */

namespace Todotoday\ClassificationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Sonata\ClassificationBundle\Model\ContextInterface;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class AbstractClassificationEntity
 *
 * @package Todotoday\ClassificationBundle\Entity
 * @ORM\MappedSuperclass()
 */
abstract class AbstractClassificationEntity
{
    /**
     * @var int $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\XmlAttributeMap()
     * @Serializer\Expose()
     * @Serializer\SerializedName("id")
     * @Serializer\Type("integer")
     * @Serializer\Since("1.0")
     * @Serializer\Groups({"sonata_api_read", "sonata_api_write", "sonata_search"})
     */
    protected $id;

    /**
     * @var string
     * @Gedmo\Slug(fields={"name"}, updatable=false)
     * @ORM\Column(name="slug", type="string", length=24)
     */
    protected $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=24)
     * @Serializer\Expose()
     */
    protected $name;

    /**
     * @var bool
     *
     * @ORM\Column(name="enabled", type="boolean")
     */
    protected $enabled;

    /**
     * @var ContextInterface
     *
     * @ORM\ManyToOne(targetEntity="Todotoday\ClassificationBundle\Entity\Context")
     * @ORM\JoinColumn(name="context", referencedColumnName="id")
     */
    protected $context;

    /**
     * @return ContextInterface
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * @param ContextInterface $context
     *
     * @return AbstractClassificationEntity
     */
    public function setContext(ContextInterface $context): self
    {
        $this->context = $context;

        return $this;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return AbstractClassificationEntity
     */
    public function setName($name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return AbstractClassificationEntity
     */
    public function setSlug($slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Is enabled
     *
     * @return bool
     */
    public function isEnabled(): ?bool
    {
        return $this->enabled;
    }

    /**
     * Get enabled.
     *
     * @return bool $enabled
     */
    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    /**
     * Set enabled.
     *
     * @param bool $enabled
     *
     * @return AbstractClassificationEntity
     */
    public function setEnabled($enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }
}
