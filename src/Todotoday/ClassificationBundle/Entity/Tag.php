<?php declare(strict_types = 1);
/**
 * PHP version 7
 */

namespace Todotoday\ClassificationBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Sonata\ClassificationBundle\Model\TagInterface;
use Todotoday\CMSBundle\Entity\Post;

/**
 * Category
 *
 * @author Alexandre Vinet <alexandre.vinet@actiane.com>
 *
 * @ORM\Table(name="tag", schema="classification")
 * @ORM\Entity()
 *
 * @Serializer\ExclusionPolicy(value="all")
 * @Serializer\XmlRoot(name="_tag")
 */
class Tag extends AbstractClassificationEntity implements TagInterface
{
    /**
     * ##########################################################################################################
     * ############################################ RELATIONS ###################################################
     * ##########################################################################################################
     */

    /**
     * @ORM\ManyToMany(targetEntity="Todotoday\CMSBundle\Entity\Post", mappedBy="tags")
     */
    protected $posts;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;

    /**
     * #########################################################################################################
     * ############################################ METHODES ###################################################
     * #########################################################################################################
     */

    /**
     * Tag constructor.
     */
    public function __construct()
    {
        $this->posts = new ArrayCollection();
    }

    /**
     * Add post
     *
     * @param Post $post
     *
     * @return Tag
     */
    public function addPost(Post $post): self
    {
        $this->posts[] = $post;
        $post->addTag($this);

        return $this;
    }

    /**
     * Get created_at.
     *
     * @return \DateTime $createdAt
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set created_at.
     *
     * @param \DateTime $createdAt
     *
     * @return $this
     */
    public function setCreatedAt(\DateTime $createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get posts
     *
     * @return Collection|Post[]
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    /**
     * Get updated_at.
     *
     * @return \DateTime $updatedAt
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set updated_at.
     *
     * @param \DateTime $updatedAt
     *
     * @return Tag
     */
    public function setUpdatedAt(\DateTime $updatedAt = null): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Remove post
     *
     * @param Post $post
     *
     * @return Tag
     */
    public function removePost(Post $post): self
    {
        $this->posts->removeElement($post);
        $post->removeTag($this);

        return $this;
    }
}
