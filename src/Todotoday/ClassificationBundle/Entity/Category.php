<?php declare(strict_types = 1);
/**
 * PHP version 7
 */

namespace Todotoday\ClassificationBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use JMS\Serializer\Annotation as Serializer;
use Sonata\ClassificationBundle\Model\CategoryInterface;
use Sonata\MediaBundle\Model\MediaInterface;
use Todotoday\CMSBundle\Entity\Post;
use Todotoday\MediaBundle\Entity\Media;

/**
 * Category
 *
 * @author Alexandre Vinet <alexandre.vinet@actiane.com>
 *
 * @ORM\Table(name="category", schema="classification")
 * @ORM\Entity()
 *
 * @Serializer\ExclusionPolicy(value="all")
 * @Serializer\XmlRoot(name="_category")
 */
class Category extends AbstractClassificationEntity implements CategoryInterface
{
    /**
     * ##########################################################################################################
     * ############################################ RELATIONS ###################################################
     * ##########################################################################################################
     */

    /**
     * @var Collection|Post[]
     *
     * @ORM\ManyToMany(targetEntity="Todotoday\CMSBundle\Entity\Post", mappedBy="categories")
     */
    protected $posts;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */
    private $position;

    /**
     * @var Collection|Category[]
     *
     * @ORM\OneToMany(targetEntity="Todotoday\ClassificationBundle\Entity\Category", mappedBy="parent")
     */
    private $children;

    /**
     * @var Category
     *
     * @ORM\ManyToOne(targetEntity="Todotoday\ClassificationBundle\Entity\Category",inversedBy="children")
     */
    private $parent;

    /**
     * @var Media
     *
     * @ORM\ManyToOne(targetEntity="Todotoday\MediaBundle\Entity\Media")
     */
    private $media;

    /**
     * #########################################################################################################
     * ############################################ METHODES ###################################################
     * #########################################################################################################
     */

    /**
     * Tag constructor.
     */
    public function __construct()
    {
        $this->posts = new ArrayCollection();
        $this->children = new ArrayCollection();
    }

    /**
     * Do __toString
     *
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->getName();
    }

    /**
     * {@inheritdoc}
     *
     * @return Category
     */
    public function addChild(CategoryInterface $child, $nested = false): self
    {
        $this->children[] = $child;

        if ($this->getContext()) {
            $child->setContext($this->getContext());
        }

        if (!$nested) {
            $child->setParent($this, true);
        } else {
            $child->setParent($this);
        }

        return $this;
    }

    /**
     * @param Category $child
     *
     * @deprecated only used by the AdminHelper
     * @return Category
     */
    public function addChildren(Category $child): self
    {
        $this->addChild($child, true);

        return $this;
    }

    /**
     * Add post
     *
     * @param Post $post
     *
     * @return Category
     */
    public function addPost(Post $post): self
    {
        $this->posts[] = $post;

        return $this;
    }

    /**
     * Do disableChildrenLazyLoading
     *
     * @return Category
     */
    public function disableChildrenLazyLoading(): self
    {
        if ($this->children instanceof PersistentCollection) {
            $this->children->setInitialized(true);
        }

        return $this;
    }

    /**
     * Get Children.
     *
     * @return Collection|Category[] $children
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    /**
     * @param Collection|Category[] $children
     *
     * @return Category
     * @throws \InvalidArgumentException
     */
    public function setChildren($children): self
    {
        if ($children instanceof Collection) {
            $this->children = $children;
        } elseif (is_array($children)) {
            foreach ($children as $category) {
                $this->addChild($category);
            }
        } else {
            throw new \InvalidArgumentException('$children must be an instance of Collection or an array.');
        }

        return $this;
    }

    /**
     * Get description.
     *
     * @return string $description
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Category
     */
    public function setDescription($description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return MediaInterface
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * @param MediaInterface $media
     *
     * @return Category
     */
    public function setMedia(MediaInterface $media = null): self
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get Parent.
     *
     * @return Category $parent
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set Parent.
     *
     * @param CategoryInterface $parent
     * @param bool              $nested
     *
     * @return Category
     */
    public function setParent(CategoryInterface $parent = null, $nested = false): self
    {
        $this->parent = $parent;

        if (!$nested && $parent) {
            $parent->addChild($this, true);
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }

    /**
     * @param int $position
     *
     * @return Category
     */
    public function setPosition($position): self
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get posts
     *
     * @return Collection|Post[]
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    /**
     * Return true if category has children.
     *
     * @return bool
     */
    public function hasChildren(): bool
    {
        return count($this->children) > 0;
    }

    /**
     * Do removeChild
     *
     * @param Category $category
     *
     * @return Category
     */
    public function removeChild(Category $category): self
    {
        $this->children->remove($category);
        $category->setParent(null);

        return $this;
    }

    /**
     * Remove post
     *
     * @param Post $post
     *
     * @return Category
     */
    public function removePost(Post $post): self
    {
        $this->posts->removeElement($post);

        return $this;
    }
}
