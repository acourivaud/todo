<?php
/**
 * PHP version 7
 */
declare(strict_types = 1);

namespace Todotoday\ClassificationBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * TodotodayClassificationBundle
 *
 * @author Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class TodotodayClassificationBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'SonataClassificationBundle';
    }
}
