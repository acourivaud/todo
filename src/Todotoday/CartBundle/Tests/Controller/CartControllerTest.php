<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 06/04/17
 * Time: 09:54
 */

namespace Todotoday\CartBundle\Tests\Controller;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAccountData;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyData;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class CartControllerTest
 * @package Todotoday\CartBundle\Tests\Controller
 */
class CartControllerTest extends WebTestCase
{
    /**
     * @dataProvider adherentProvider
     * @small
     *
     * @param string $adherentSlug
     */
    public function testCartAdherent(string $adherentSlug)
    {
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference($adherentSlug);

        /** @var Agency $agency */
        $agency = $adherent->getLinkAgencies()->first()->getAgency();

        $adherent = $this->getDoctrine()->getRepository('TodotodayAccountBundle:Adherent')->find($adherent->getId());
        $this->loginAs($adherent, 'main');
        $url = 'http://' . $agency->getSlug() . '.' . 'todotoday.lol';
        $url .= $this->getUrl('todotoday_cart_cart_index');
        $client = $this->makeClient();
        $client->request('GET', $url);
        $this->assertStatusCode(200, $client);
    }

    /**
     * @small
     */
    public function testNotLogged()
    {
        /** @var Agency $agency */
        $agency = self::getFRR()->getReference('agency_demo0');

        $domain = $this->getContainer()->getParameter('domain');
        $url = 'http://' . $agency->getSlug() . '.' . $domain . '';
        $url .= '' . $this->getUrl('todotoday_cart_cart_index');

        $client = $this->makeClient();
        $client->request('GET', $url);
        $this->assertStatusCode(302, $client);
    }

    /**
     * @return array
     */
    public function adherentProvider()
    {
        $provider = array();
        foreach (LoadAccountData::getAdherentsSlugs() as $adherentsSlug) {
            $provider[$adherentsSlug] = array($adherentsSlug);
        }

        return $provider;
    }

    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAgencyData::class,
            LoadAccountData::class,
        );
    }
}
