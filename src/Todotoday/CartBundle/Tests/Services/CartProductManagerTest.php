<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 05/02/17
 * Time: 11:08
 */

namespace Todotoday\CartBundle\Tests\Services;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAccountData;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Entity\Concierge;
use Todotoday\CartBundle\DataFixtures\ORM\LoadCartProductData;
use Todotoday\CartBundle\Entity\CartProduct;
use Todotoday\CartBundle\Exception\NotAdherentCartBundleException;
use Todotoday\CartBundle\Exception\ProductNotFoundCartBundleException;
use Todotoday\CartBundle\Services\CartProductManager;
use Todotoday\CatalogBundle\Entity\Api\Microsoft\RetailCatalog;
use Todotoday\CatalogBundle\Exceptions\ProductNotFoundException;
use Todotoday\CatalogBundle\Services\CatalogManager;
use Todotoday\CheckoutBundle\Enum\CheckoutNeedDepositoryEnum;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyData;
use Todotoday\CoreBundle\Entity\Agency;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CartProductManagerTest
 * @package Todotoday\CartBundle\Tests\Services
 */
class CartProductManagerTest extends WebTestCase
{
    /**
     * @var CartProductManager
     */
    private $manager;

    /**
     * @var Agency
     */
    private $agency;

    /**
     * Expect exception when when not an adherent perform operations on carts
     *
     * @dataProvider providerMethod
     *
     * @small
     *
     * @param       $method
     * @param array $params
     */
    public function testOperationOnCartForNotAnAdherent($method, array $params = [])
    {
        $this->expectException(NotAdherentCartBundleException::class);
        $user = new Concierge();
        $this->tokenAs($user, 'main');

        $agency = new Agency();
        $cartProduct = new CartProduct();
        $cartProduct->setProduct('1');
        $cartProduct->setQuantity(1);

        $parameters = array(
            'agency' => $agency,
            'product' => 1,
            'cartProduct' => $cartProduct,
        );

        call_user_func_array(
            array(
                $this->manager,
                $method,
            ),
            array_intersect_key($parameters, $params)
        );
    }

//    TODO Faire les tests sur les adherents qui font des manip sur un panier d'une conciergerie dont il ne font pas
// partie

    /**
     * Test assert exception when removing product that dont belongs to cart
     *
     * @small
     */
    public function testRemoveProductThatDontBelongsToCart()
    {
        $this->expectException(ProductNotFoundCartBundleException::class);
        $adherent = new Adherent();
        $agency = new Agency();
        $this->tokenAs($adherent, 'main');
        $this->manager->removeProduct($agency, '999');
    }

    /**
     * Test assert exception when updating product that dont belongs to cart
     *
     * @small
     */
    public function testUpdateProductThatDontBelongsToCart()
    {
        $this->expectException(ProductNotFoundCartBundleException::class);
        $adherent = new Adherent();
        $agency = new Agency();
        $this->tokenAs($adherent, 'main');
        $cartProduct = new CartProduct();
        $cartProduct->setProduct('999')->setQuantity(1);
        $this->manager->updateProduct($agency, $cartProduct);
    }

    /**
     * Test get cart product
     *
     * @small
     */
    public function testGetCartProduct()
    {
        $this->getMockedCatalog();
        /** @var Adherent $adherent0 */
        $adherent0 = self::getFRR()->getReference('adherent0');
        /** @var Agency $agency */
        $agency = $adherent0->getLinkAgencies()->first()->getAgency();
        /** @var CartProduct $cartProductsBefore */
        $cartProductsBefore = self::getFRR()->getReference('cartProduct_1_demo0_adherent0');

        $this->tokenAs($adherent0, 'main');

        /** @var CartProduct $cartProduct */
        $cartProduct = $this->manager->getCartProduct($agency, '1');

        $this->assertEquals(
            $cartProduct->getAdherent()->getId(),
            $cartProductsBefore->getAdherent()->getId()
        );
        $this->assertEquals(
            $cartProduct->getAgency()->getId(),
            $cartProductsBefore->getAgency()->getId()
        );
        $this->assertEquals($cartProduct->getProduct(), $cartProductsBefore->getProduct());
        $this->assertEquals($cartProduct->getQuantity(), $cartProductsBefore->getQuantity());
    }
//

    /**
     * Test get cart
     * @small
     */
    public function testGetCart()
    {
        $this->getMockedCatalog();
        /** @var Adherent $adherent0 */
        $adherent0 = self::getFRR()->getReference('adherent0');
        /** @var Agency $agency */
        $agency = $adherent0->getLinkAgencies()->first()->getAgency();

        /** @var CartProduct[] $cartProductsBefore */
        $cartProductsBefore = array(
            self::getFRR()->getReference('cartProduct_1_demo0_adherent0'),
            self::getFRR()->getReference('cartProduct_2_demo0_adherent0'),
            self::getFRR()->getReference('cartProduct_3_demo0_adherent0'),
        );
        $this->tokenAs($adherent0, 'main');
        /** @var CartProduct[] $cartProduct */
        $cartProduct = $this->manager->getCart($agency);
        $i = 0;
        foreach ($cartProductsBefore as $cartProductBefore) {
            $this->assertEquals(
                $cartProduct[$i]->getAdherent()->getId(),
                $cartProductBefore->getAdherent()->getId()
            );
            $this->assertEquals(
                $cartProduct[$i]->getAgency()->getId(),
                $cartProductBefore->getAgency()->getId()
            );
            $this->assertEquals($cartProduct[$i]->getProduct(), $cartProductBefore->getProduct());
            $this->assertEquals($cartProduct[$i]->getQuantity(), $cartProductBefore->getQuantity());
            $i++;
        }
    }

    /**
     * Test add product
     *
     * @runInSeparateProcess
     * @small
     */
    public function testAddProduct()
    {
        $this->getMockedCatalog();
        /** @var Adherent $adherent0 */
        $adherent0 = self::getFRR()->getReference('adherent0');
        /** @var Agency $agency */
        $agency = $adherent0->getLinkAgencies()->first()->getAgency();

        $this->tokenAs($adherent0, 'main');

        /** @var CartProduct[] $cartProductBefore */
        $cartProductBefore = array(
            self::getFRR()->getReference('cartProduct_1_demo0_adherent0'),
            self::getFRR()->getReference('cartProduct_2_demo0_adherent0'),
            self::getFRR()->getReference('cartProduct_3_demo0_adherent0'),
        );

        $cartProduct = new CartProduct();
        $cartProduct->setAgency($this->agency);
        $cartProduct->setProduct('1');
        $cartProduct->setQuantity(10);

        /** @var CartProduct[] $cartProductAfterCheck1 */
        $cartProductAfterCheck1 = $this->manager->addProduct($this->agency, $cartProduct);
        $cartProductAfterCheck1 = array_filter(
            $cartProductAfterCheck1,
            function ($val) {
                /** @var CartProduct $val */

                return $val->getProduct() === '1';
            }
        );

        $this->assertEquals(15, $cartProductAfterCheck1[0]->getQuantity());

        $this->checkAssert($agency, $cartProduct, $cartProductBefore);
    }

    /**
     * Test add product
     * @small
     */
    public function testAddUnknownProduct()
    {
        $this->expectException(ProductNotFoundException::class);
        $this->expectExceptionMessage('Product not found');

        /** @var Adherent $adherent0 */
        $adherent0 = self::getFRR()->getReference('adherent0');
        $this->tokenAs($adherent0, 'main');

        $cartProduct = new CartProduct();
        $cartProduct->setAgency($this->agency);
        $cartProduct->setProduct('PRODUCT THAT DONT EXIST');
        $cartProduct->setQuantity(10);

        $this->manager->addProduct($this->agency, $cartProduct);
    }

    /**
     * Test update product
     *
     * @small
     */
    public function testUpdateProduct()
    {
        $this->getMockedCatalog();

        /** @var Adherent $adherent0 */
        $adherent0 = self::getFRR()->getReference('adherent0');
        /** @var Agency $agency */
        $agency = $adherent0->getLinkAgencies()->first()->getAgency();
        $this->tokenAs($adherent0, 'main');

        /** @var CartProduct[] $cartProductBefore */
        $cartProductBefore = array(
            self::getFRR()->getReference('cartProduct_1_demo0_adherent0'),
            self::getFRR()->getReference('cartProduct_2_demo0_adherent0'),
            self::getFRR()->getReference('cartProduct_3_demo0_adherent0'),
        );

        $cartProduct = new CartProduct();
        $cartProduct->setAgency($this->agency);
        $cartProduct->setAdherent($adherent0);
        $cartProduct->setProduct('1');
        $cartProduct->setQuantity(10);

        /*
         * On teste d'abord que la nouvelle quantité du produit est bien celle modifié
         */

        /** @var CartProduct[] $cartProductAfterCheck1 */
        $cartProductAfterCheck1 = $this->manager->updateProduct($agency, $cartProduct);
        $cartProductAfterCheck1 = array_filter(
            $cartProductAfterCheck1,
            function ($val) {
                /** @var CartProduct $val */

                return $val->getProduct() === '1';
            }
        );

        $this->assertEquals(10, $cartProductAfterCheck1[0]->getQuantity());

        $this->checkAssert($agency, $cartProduct, $cartProductBefore);
    }

    /**
     * Test removing one specific product
     * Null expected, expect that other product still the same
     * @runInSeparateProcess
     * @small
     */
    public function testRemoveProduct()
    {
        $this->getMockedCatalog();

        /** @var Adherent $adherent0 */
        $adherent0 = self::getFRR()->getReference('adherent0');
        /** @var Agency $agency */
        $agency = $adherent0->getLinkAgencies()->first()->getAgency();
        $this->tokenAs($adherent0, 'main');

        /** @var CartProduct $cartProductBefore */
        $cartProductBefore = self::getFRR()->getReference('cartProduct_2_demo0_adherent0');

        $this->manager->removeProduct($agency, '1');
        $cartProductAfter1 = $this->manager->getCartProduct($agency, '1');
        $cartProductAfter2 = $this->manager->getCartProduct($agency, '2');

        // testing if null returned after remove product
        // testing if null returned when getting product that was removed
        $this->assertNull($cartProductAfter1);
        // finally testing if other products are still there
        $this->assertEquals($cartProductAfter2->getProduct(), $cartProductBefore->getProduct());
        $this->assertEquals($cartProductAfter2->getQuantity(), $cartProductBefore->getQuantity());
        $this->assertEquals(
            $cartProductAfter2->getAdherent()->getId(),
            $cartProductBefore->getAdherent()->getId()
        );
    }

    /**
     * Test empty cart
     *
     * @small
     */
    public function testEmptyCart()
    {
        /** @var Adherent $adherent12 */
        $adherent12 = self::getFRR()->getReference('adherent12');
        /** @var Agency $agency */
        $agency = $adherent12->getLinkAgencies()->first()->getAgency();
        $this->tokenAs($adherent12, 'main');
        $result = $this->manager->emptyCart($agency);

        // testing that null is returned after emptying cart
        $this->assertNull($result);
        // testing that there is no more product in cart
        $this->assertEmpty($this->manager->getCart($agency));
    }

    /**
     * @small
     * @depends testEmptyCart
     */
    public function testNotEmptyCartForMultiAgencies()
    {
        // finally testing that adherent still got product in other agency

        /** @var CartProduct $cartProductBefore */
        $cartProductBefore = self::getFRR()->getReference('cartProduct_2_demo1_adherent12');
        /** @var Agency $agency1 */
        $agency1 = self::getFRR()->getReference('agency_demo1');
        $requestStack = $this->getContainer()->get('request_stack');
        $domain = $this->getContainer()->getParameter('domain');
        $requestStack->push(Request::create('http://' . $agency1->getSlug() . '.' . $domain));
        /** @var Adherent $adherent12 */
        $adherent12 = self::getFRR()->getReference('adherent12');
        $this->tokenAs($adherent12, 'main');

        $cartProductAfter = $this->manager->getCart($agency1);

        $this->assertEquals($cartProductBefore->getProduct(), $cartProductAfter[0]->getProduct());
        $this->assertEquals($cartProductBefore->getQuantity(), $cartProductAfter[0]->getQuantity());
        $this->assertEquals(
            $cartProductBefore->getAdherent()->getId(),
            $cartProductAfter[0]->getAdherent()->getId()
        );
        $this->assertEquals(
            $cartProductBefore->getAgency()->getId(),
            $cartProductAfter[0]->getAgency()->getId()
        );
    }

    /**
     * Provider method for cart product
     * Return expected as array (
     *  Method to call,
     *  arugments[type => value (could be anything)
     * )
     *
     * @return array
     */
    public function providerMethod()
    {
        return array(
            'getCart' => array(
                'getCart',
                array(
                    'agency' => 0,
                ),
            ),
            'getCartProduct' => array(
                'getCartProduct',
                array(
                    'agency' => 0,
                    'product' => 0,
                ),
            ),
            'addProduct' => array(
                'addProduct',
                array('cartProduct' => 0, 'agency' => 0),
            ),
            'emptyCart' => array(
                'emptyCart',
                array('agency' => 0),
            ),
            'removeProduct' => array(
                'removeProduct',
                array(
                    'agency' => 0,
                    'product' => 0,
                ),
            ),
            'updateProduct' => array(
                'updateProduct',
                array(
                    'agency' => 0,
                    'cartProduct' => 0,
                ),
            ),
        );
    }

    /**
     * Provider Adherent
     *
     * @return array
     */
    public function providerAdherent()
    {
        $accountsSlug = LoadAccountData::getAdherentsSlugs();
        $provider = [];

        foreach ($accountsSlug as $accountSlug) {
            $provider[] = array($accountSlug);
        }

        return $provider;
    }

    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAccountData::class,
            LoadAgencyData::class,
            LoadCartProductData::class,
        );
    }

    /**
     * Setup method
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->manager = $this->getContainer()->get('todotoday_cart_product.manager');
        $this->agency = self::getFRR()->getReference('agency_demo0');

        $requestStack = $this->getContainer()->get('request_stack');
        $domain = $this->getContainer()->getParameter('domain');
        $requestStack->push(
            Request::create('http://' . $this->agency->getSlug() . '.' . $domain)
        );
    }

    /**
     * @runInSeparateProcess
     * @small
     */
    public function testIsNeeededDepositoryTrue()
    {
        $this->getMockedCatalog('ServiceStk');
        /** @var Adherent $adherent0 */
        $adherent0 = self::getFRR()->getReference('adherent0');
        /** @var Agency $agency */
        $agency = $adherent0->getLinkAgencies()->first()->getAgency();
        $this->tokenAs($adherent0, 'main');

        $cartProduct = (new CartProduct())
            ->setProduct('osef')
            ->setQuantity(1)
            ->setProductPrice((float) 25)
            ->setAdherent($adherent0)
            ->setAgency($agency);

        $this->manager->addProduct($agency, $cartProduct);
        $this->assertTrue($this->manager->isCartNeedDepository($agency));
    }

    /**
     * @runInSeparateProcess
     * @small
     */
    public function testIsNeeededDepositoryFalse()
    {
        $this->getMockedCatalog('osef');
        /** @var Adherent $adherent0 */
        $adherent0 = self::getFRR()->getReference('adherent0');
        /** @var Agency $agency */
        $agency = $adherent0->getLinkAgencies()->first()->getAgency();
        $this->tokenAs($adherent0, 'main');

        $cartProduct = (new CartProduct())
            ->setProduct('tesazdazdazdt')
            ->setQuantity(1)
            ->setProductPrice((float) 25)
            ->setAdherent($adherent0)
            ->setAgency($agency);

        $this->manager->addProduct($agency, $cartProduct);
        $this->assertFalse($this->manager->isCartNeedDepository($agency));
    }

    /**
     * @param Agency      $agency
     * @param CartProduct $cartProduct
     * @param array       $cartProductBefore
     */
    private function checkAssert(Agency $agency, CartProduct $cartProduct, array $cartProductBefore)
    {
        // On test ensuite que les autres produits n'ont pas été modifié

        /** @var CartProduct[] $cartProductsAfterCheck2 */
        $cartProductsAfterCheck2 = $this->manager->getCart($agency);

        $i = 1;
        foreach ($cartProductsAfterCheck2 as $cartProductsfterCheck2) {
            // on exclue du test le produit 1 qui a déjà été testé
            if ($cartProduct->getProduct() === '1') {
                continue;
            }
            $this->assertEquals($cartProductBefore[$i]->getQuantity(), $cartProductsfterCheck2->getQuantity());
            $this->assertEquals($cartProductBefore[$i]->getProduct(), $cartProductsfterCheck2->getProduct());
            $this->assertEquals(
                $cartProductBefore[$i]->getAgency()->getId(),
                $cartProductsfterCheck2->getAgency()->getId()
            );
            $this->assertEquals(
                $cartProductBefore[$i]->getAdherent()->getId(),
                $cartProductsfterCheck2->getAdherent()->getId()
            );
        }
    }

    /**
     * Mocked catalog to bypass verification from actual catalog
     *
     * @param string $itemModelGroup
     */
    private function getMockedCatalog(string $itemModelGroup = 'osef')
    {
        $mockedRetailCatalog = new RetailCatalog();
        $mockedRetailCatalog->setHierarchy('hierarchy')
            ->setProductType('item')
            ->setAmount(20)
            ->setItemModelGroup($itemModelGroup)
            ->setDescriptionTrans('description')
            ->setNameTrans('produit');

        $mockedCatalogManager = $this->getMockBuilder(CatalogManager::class)->disableOriginalConstructor()->getMock();
        $mockedCatalogManager->method('getProduct')->will($this->returnValue($mockedRetailCatalog));

        $reflection = new \ReflectionClass($this->manager);
        $reflectionCatalog = $reflection->getProperty('catalogManager');
        $reflectionCatalog->setAccessible(true);
        $reflectionCatalog->setValue($this->manager, $mockedCatalogManager);
    }
}
