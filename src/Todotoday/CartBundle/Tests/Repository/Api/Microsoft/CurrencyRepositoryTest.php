<?php declare(strict_types = 1);

/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 01/03/2017
 * Time: 12:40
 */

namespace Todotoday\CartBundle\Tests\Repository\Api\Microsoft;

use Actiane\ApiConnectorBundle\Tests\Repository\Api\AbstractRepositoryTestCase;

/**
 * Class CurrencyRepositoryTest
 *
 * @category   Todo-Todev
 * @package    Todotoday\CartBundle
 * @subpackage Todotoday\CartBundle\Tests\Repository\Api\Microsoft
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class CurrencyRepositoryTest extends AbstractRepositoryTestCase
{
    /**
     * Do getRepositoryId
     *
     * @return string
     */
    protected function getRepositoryId(): string
    {
        return 'todotoday.cart.repository.api.currency';
    }
}
