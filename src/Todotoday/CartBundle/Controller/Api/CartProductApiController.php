<?php declare(strict_types=1);
/**
 * PHP version 7
 */

namespace Todotoday\CartBundle\Controller\Api;

use FOS\RestBundle\Controller\Annotations as FosRest;
use FOS\RestBundle\Request\ParamFetcher;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Todotoday\CartBundle\Form\Api\CartProductCollectionType;
use Todotoday\CartBundle\Form\Api\CartProductType;
use Todotoday\CoreBundle\Controller\Api\AbstractApiController;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Traits\Api\ApiControllerDeleteTrait;
use Todotoday\CoreBundle\Traits\Api\ApiControllerGetTrait;
use Todotoday\CoreBundle\Traits\Api\ApiControllerPostTrait;
use Todotoday\CoreBundle\Traits\Api\ApiControllerPutTrait;

/**
 * Class CartProductApiController
 *
 * @package Todotoday\CartBundle\Controller\Api
 * @FosRest\Prefix("/cart/products")
 * @FosRest\NamePrefix("todotoday.cart.api.cart_product.")
 */
class CartProductApiController extends AbstractApiController
{
    use ApiControllerDeleteTrait, ApiControllerGetTrait, ApiControllerPutTrait, ApiControllerPostTrait;

    /**
     * Empty cart for current adherent
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Operation on product carts.",
     *     description="Empty cart for current adherent",
     *     views={"default","cart-product"},
     *     section="cart/products"
     * )
     *
     * @FosRest\QueryParam(
     *     name="fields",
     *     requirements="((agency),(adherent),?)*",
     *     default="",
     *     description="Fields you need"
     * )
     *
     * @FosRest\Route("/me")
     *
     * @return Response
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \Todotoday\CartBundle\Exception\NotAdherentCartBundleException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \UnexpectedValueException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     *
     */
    public function deleteAllMeAction(): Response
    {
        if (!$this->isGranted('CREATE', $this)) {
            throw $this->createAccessDeniedException();
        }
        $this->getCartManager()->emptyCart($this->getAgency());
        $view = $this->view();
        $groups = ['Default'];
        $view->getContext()->addGroups($groups)->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * Delete specific product in current cart's adherent
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Operation on product carts.",
     *     description="Delete specific product in current cart's adherent",
     *     views={"default","cart-product"},
     *     section="cart/products"
     * )
     *
     * @FosRest\QueryParam(
     *     name="fields",
     *     requirements="((agency),(adherent),?)*",
     *     default="",
     *     description="Fields you need"
     * )
     *
     * @FosRest\Route("/me/{productId}", requirements={"id" = "\d+"})
     *
     * @param string $productId
     *
     * @return Response
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \Todotoday\CartBundle\Exception\ProductNotFoundCartBundleException
     * @throws \Todotoday\CartBundle\Exception\NotAdherentCartBundleException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \UnexpectedValueException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     *
     */
    public function deleteMeAction(string $productId): Response
    {
        if (!$this->isGranted('CREATE', $this)) {
            throw $this->createAccessDeniedException();
        }

        $data = $this->getCartManager()->removeProduct($this->getAgency(), $productId);

        $view = $this->view($data);
        $groups = ['Default'];
        $view->getContext()->addGroups($groups)->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * Retrieves product and quantity for specific cart product's id
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Operation on product carts.",
     *     description="Retrieves product and quantity for specific cart product's id",
     *     output={
     *           "class"="Todotoday\CartBundle\Entity\CartProduct",
     *           "groups"={"Default","agency","adherent"}
     *     },
     *     views={"default","cart-product"},
     *     section="cart/products"
     * )
     *
     * @FosRest\QueryParam(
     *     name="fields",
     *     requirements="((agency|adherent),?)*",
     *     default="",
     *     description="Fields you need"
     * )
     *
     * @FosRest\Route("/{id}", requirements={"id" = "\d+"})
     *
     * @param ParamFetcher $paramFetcher
     * @param int          $id
     *
     * @return Response
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \LogicException
     */
    public function getAction(ParamFetcher $paramFetcher, int $id): Response
    {
        return $this->getRoute($paramFetcher, $id);
    }

    /**
     * Retrieves all carts
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Operation on product carts.",
     *     description="Retrieves all carts",
     *     output={
     *           "class"="array<Todotoday\CartBundle\Entity\CartProduct> as CartProduct",
     *           "groups"={"Default"}
     *     },
     *     views={"default","cart-product"},
     *     section="cart/products"
     * )
     *
     * @FosRest\QueryParam(
     *     name="fields",
     *     requirements="((agency|adherent),?)*",
     *     default="",
     *     description="Fields you need"
     * )
     *
     * @FosRest\Route("")
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return Response
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \LogicException
     */
    public function getAllAction(ParamFetcher $paramFetcher): Response
    {
        return $this->getAllRoute($paramFetcher);
    }

    /**
     * Retrieves all the product in cart's current adherent
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Operation on product carts.",
     *     description="Retrieves all the product in cart's current adherent",
     *     output={
     *           "class"="array<Todotoday\CartBundle\Entity\CartProduct> as CartProduct",
     *           "groups"={"Default","agency","adherent"}
     *     },
     *     views={"default","cart-product"},
     *     section="cart/products"
     * )
     *
     * @FosRest\QueryParam(
     *     name="fields",
     *     requirements="((agency|adherent),?)*",
     *     default="",
     *     description="Fields you need"
     * )
     *
     * @FosRest\Route("/me")
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return Response
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \UnexpectedValueException
     * @throws \Todotoday\CartBundle\Exception\NotAdherentCartBundleException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \LogicException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function getMeAllAction(ParamFetcher $paramFetcher): Response
    {
        if (!$this->isGranted('CREATE', $this)) {
            throw $this->createAccessDeniedException();
        }

        $agency = $this->getAgency();
        $view = $this->view(
            $this->get('todotoday_cart_product.manager')->getCart($agency)
        );

        $groups = ['Default'];
        if ($fields = explode(',', $paramFetcher->get('fields'))) {
            $groups = array_merge($groups, $fields);
        }
        $view->getContext()->addGroups($groups);
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * Post a new product in current adherent's cart
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Operation on product carts.",
     *     description="Post a new product in current adherent's cart",
     *     output={
     *           "class"="Todotoday\CartBundle\Entity\CartProduct",
     *           "groups"={"Default","agency","adherent"}
     *     },
     *     input={
     *          "class" = "Todotoday\CartBundle\Form\Api\CartProductType"},
     *     views={"default","cart-product"},
     *     section="cart/products"
     * )
     *
     * @FosRest\Route("/me")
     *
     *
     * @param Request $request
     *
     * @return object|\Todotoday\CoreBundle\Entity\Agency
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Todotoday\CatalogBundle\Exceptions\ProductNotFoundException
     * @throws \Todotoday\CartBundle\Exception\NotAdherentCartBundleException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \UnexpectedValueException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    public function postMeAction(Request $request)
    {
        if (!$this->isGranted('CREATE', $this)) {
            throw $this->createAccessDeniedException();
        }

        $agency = $this->getAgency();
        $formType = $this->getFormType();
        $form = $this->createForm(
            $formType
        );

        $form->handleRequest($request);
        $data = $form;

        if ($form->isSubmitted() && $form->isValid()) {
            $cartProduct = $this->getCartManager()
                ->addProduct($agency, $form->getData());
            $data = $cartProduct;
        }

        $view = $this->view($data);
        $groups = $this->getDefaultGroups();
        $view->getContext()->addGroups($groups);
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * Post multiple new products in current adherent's cart
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Operation on product carts.",
     *     description="Post multiple new products in current adherent's cart",
     *     output={
     *           "class"="array<Todotoday\CartBundle\Entity\CartProduct> as CartProduct",
     *           "groups"={"Default","agency","adherent"}
     *     },
     *     input={
     *          "class" = "Todotoday\CartBundle\Form\Api\CartProductCollectionType"},
     *     views={"default","cart-product"},
     *     section="cart/products"
     * )
     *
     * @FosRest\Route("/me/multiple")
     *
     * @param Request $request
     *
     * @return object|\Todotoday\CoreBundle\Entity\Agency
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Todotoday\CatalogBundle\Exceptions\ProductNotFoundException
     * @throws \Todotoday\CartBundle\Exception\NotAdherentCartBundleException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \UnexpectedValueException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    public function postMultipleMeAction(Request $request)
    {
        if (!$this->isGranted('CREATE', $this)) {
            throw $this->createAccessDeniedException();
        }

        $agency = $this->getAgency();
        $formType = CartProductCollectionType::class;
        $form = $this->createForm(
            $formType
        );

        $form->handleRequest($request);
        $data = $form;

        if ($form->isSubmitted() && $form->isValid()) {
            if ($request->headers->get('clear') && $request->headers->get('clear') == true) {
                $this->getCartManager()->emptyCart($agency);
            }

            foreach ($form->getData()['cartProducts'] as $cartProduct) {
                $this->getCartManager()->addProduct($agency, $cartProduct);
            }
            $data = $this->getCartManager()->getCart($agency);
        }

        $view = $this->view($data);
        $groups = $this->getDefaultGroups();
        $view->getContext()->addGroups($groups);
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * Update quantity for specific product in cart's current adherent
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Operation on product carts.",
     *     description="Update quantity for specific product in cart's current adherent",
     *     output={
     *           "class"="Todotoday\CartBundle\Entity\CartProduct",
     *           "groups"={"Default","agency","adherent"}
     *     },
     *     input={
     *           "class" = "Todotoday\CartBundle\Form\Api\CartProductType"},
     *     views={"default","cart-product"},
     *     section="cart/products"
     * )
     *
     * @FosRest\QueryParam(
     *     name="fields",
     *     requirements="((agency),(adherent),?)*",
     *     default="",
     *     description="Fields you need"
     * )
     *
     * @FosRest\Route("/me")
     *
     * @param Request $request
     *
     * @return Response
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \Todotoday\CartBundle\Exception\ProductNotFoundCartBundleException
     * @throws \Todotoday\CartBundle\Exception\NotAdherentCartBundleException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \UnexpectedValueException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    public function putMeAction(Request $request): Response
    {
        if (!$this->isGranted('CREATE', $this)) {
            throw $this->createAccessDeniedException();
        }

        $agency = $this->getAgency();
        $formType = $this->getFormType();
        $form = $this->createForm(
            $formType,
            null,
            array(
                'method' => 'PUT',
            )
        );

        $form->handleRequest($request);
        $data = $form;

        if ($form->isSubmitted() && $form->isValid()) {
            $cartProduct = $this->getCartManager()
                ->updateProduct($agency, $form->getData());

            $data = $cartProduct;
        }

        $view = $this->view($data);
        $groups = $this->getDefaultGroups();
        $view->getContext()->addGroups($groups);
        $view->getContext()->setSerializeNull(true);

        return $this->handleView($view);
    }

    /**
     * Get the formType for the current entity class link to the controller
     *
     * @return string
     */
    protected function getFormType(): string
    {
        return CartProductType::class;
    }

    /**
     * Get the serivce name from custum repository's entity
     *
     * @return string
     */
    protected function getServiceRepository(): string
    {
        return 'todotoday_cart.repository.cart_product';
    }

    /**
     * @return Agency
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \UnexpectedValueException
     * @throws \InvalidArgumentException
     */
    private function getAgency(): Agency
    {
        if (!$agency = $this->get('todotoday.core.domain_context.api')->getAgency()) {
            throw new \InvalidArgumentException('Agency is missing');
        }

        return $agency;
    }

    /**
     * @return object|\Todotoday\CartBundle\Services\CartProductManager
     */
    private function getCartManager()
    {
        return $this->get('todotoday_cart_product.manager');
    }
}
