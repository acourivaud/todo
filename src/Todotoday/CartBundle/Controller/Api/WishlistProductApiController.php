<?php declare(strict_types = 1);
/**
 * PHP version 7
 */

namespace Todotoday\CartBundle\Controller\Api;

use FOS\RestBundle\Controller\Annotations as FosRest;

/**
 * Class WishlistProductApiController
 *
 * @package Todotoday\CartBundle\Controller\Api
 * @FosRest\Prefix("/wishlists")
 * @FosRest\NamePrefix("todotoday.cart.api.wishlist")
 */
class WishlistProductApiController
{
//    /**
//     * Get the formType for the current entity class link to the controller
//     *
//     * @return string
//     */
//    protected function getFormType(): string
//    {
//        // TODO: Implement getFormType() method.
//    }
//
//    /**
//     * Get the serivce name from custum repository's entity
//     *
//     * @return string
//     */
//    protected function getServiceRepository(): string
//    {
//        // TODO: Implement getServiceRepository() method.
//    }
}
