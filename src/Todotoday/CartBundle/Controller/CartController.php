<?php declare(strict_types = 1);

namespace Todotoday\CartBundle\Controller;

use Actiane\PaymentBundle\Enum\PaymentTypeEnum;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Enum\PaymentStatusEnum;
use Todotoday\CoreBundle\Entity\Agency;
use InvalidArgumentException;

/**
 * Class IndexController
 * @package Todotoday\CartBundle\Controller
 * @Route("/cart")
 */
class CartController extends Controller
{
    /**
     * @Route("/")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Todotoday\CartBundle\Exception\NotAdherentCartBundleException
     * @throws \InvalidArgumentException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \LogicException
     */
    public function indexAction(): Response
    {
        if (!$this->isGranted('ROLE_CART')) {
            throw $this->createAccessDeniedException();
        }
        $agency = $this->getAgency();
        $cartManager = $this->get('todotoday_cart_product.manager');

        /** @var Account $adherent */
        $adherent = $this->getUser();

        $allowCheckout = $adherent instanceof Adherent && $adherent->getPaymentTypeSelectedString()
            && $adherent->getPaymentMethodStatusString() === PaymentStatusEnum::VALID;

        $isCoAdherent = ($adherent instanceof Adherent) && ($adherent->isCoAdherent());

        return $this->render(
            ':cart:cart.html.twig',
            array(
                'products' => $cartManager->getCart($agency),
                'isEmpty' => $cartManager->isEmpty($agency),
                'deposit' => $cartManager->isCartNeedDepository($agency),
                'delivery' => true,
                'total' => $cartManager->cartTotalPrice($agency),
                'allowCheckout' => $allowCheckout,
                'isCoAdherent' => $isCoAdherent,
                'displayPriceExclTaxes' => $agency->getDisplayPriceExclTaxes(),
            )
        );
    }

    /**
     * @Route("/empty-cart")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \Todotoday\CartBundle\Exception\NotAdherentCartBundleException
     * @throws \InvalidArgumentException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \LogicException
     */
    public function emptyCartAction(): RedirectResponse
    {
        if (!$this->isGranted('ROLE_CART')) {
            throw $this->createAccessDeniedException();
        }
        $this->get('todotoday_cart_product.manager')->emptyCart($this->getAgency());

        $this->addFlash('notice', 'cart.flash_empty');

        return $this->redirectToRoute('todotoday_cart_cart_index');
    }

    /**
     * @return Agency
     * @throws \UnexpectedValueException
     * @throws \InvalidArgumentException
     */
    private function getAgency(): Agency
    {
        if (!$agency = $this->get('todotoday.core.domain_context.agency')->getAgency()) {
            throw new InvalidArgumentException('Agency is missing');
        }

        return $agency;
    }
}
