<?php declare(strict_types = 1);

namespace Todotoday\CartBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class TodotodayCartBundle extends Bundle
{
}
