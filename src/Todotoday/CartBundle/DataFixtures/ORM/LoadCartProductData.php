<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 01/02/17
 * Time: 13:39
 */

namespace Todotoday\CartBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAccountData;
use Todotoday\CartBundle\Entity\CartProduct;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyData;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class LoadCartProductData
 *
 * @package Todotoday\CartBundle\DataFixtures\ORM
 */
class LoadCartProductData extends AbstractFixture implements DependentFixtureInterface, ContainerAwareInterface
{
    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return array(
            LoadAgencyData::class,
            LoadAccountData::class,
        );
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        if ($this->kernel->getEnvironment() === 'prod') {
            return;
        }
        $agenciesSlug = LoadAgencyData::getAgenciesSlug();
        $adherentSlug = LoadAccountData::getAdherentsSlugs();
        $cartProduct = new CartProduct();

        foreach ($agenciesSlug as $key => $agencySlug) {
            /** @var Agency $agency */
            $agency = $this->getReference('agency_' . $agencySlug);
            $cartProduct1 = clone $cartProduct;
            $cartProduct1
                ->setAgency($agency)
                ->setAdherent($this->getReference($adherentSlug[$key]))
                ->setProduct('1')
                ->setCurrency($agency->getCurrency())
                ->setQuantity(5);

            $cartProduct2 = clone $cartProduct;
            $cartProduct2
                ->setAgency($this->getReference('agency_' . $agencySlug))
                ->setAdherent($this->getReference($adherentSlug[$key]))
                ->setProduct('2')
                ->setCurrency($agency->getCurrency())
                ->setQuantity(10);

            $cartProduct3 = clone $cartProduct;
            $cartProduct3
                ->setAgency($this->getReference('agency_' . $agencySlug))
                ->setAdherent($this->getReference($adherentSlug[$key]))
                ->setProduct('3')
                ->setCurrency($agency->getCurrency())
                ->setQuantity(1);

            $manager->persist($cartProduct1);
            $manager->persist($cartProduct2);
            $manager->persist($cartProduct3);

            $this->setReference('cartProduct_1_' . $agencySlug . '_' . $adherentSlug[$key], $cartProduct1);
            $this->setReference('cartProduct_2_' . $agencySlug . '_' . $adherentSlug[$key], $cartProduct2);
            $this->setReference('cartProduct_3_' . $agencySlug . '_' . $adherentSlug[$key], $cartProduct3);
        }

        $cartProduct12 = clone $cartProduct;

        $cartProduct12
            ->setAdherent($this->getReference('adherent12'))
            ->setAgency($this->getReference('agency_' . $agenciesSlug[0]))
            ->setProduct('1')
            ->setCurrency($agency->getCurrency())
            ->setQuantity(10);
        $manager->persist($cartProduct12);
        $this->setReference('cartProduct_1_' . $agenciesSlug[0] . '_adherent12', $cartProduct12);

        $cartProduct
            ->setAdherent($this->getReference('adherent12'))
            ->setAgency($this->getReference('agency_' . $agenciesSlug[1]))
            ->setProduct('2')
            ->setCurrency($agency->getCurrency())
            ->setQuantity(5);
        $manager->persist($cartProduct);
        $this->setReference('cartProduct_2_' . $agenciesSlug[1] . '_adherent12', $cartProduct);

        $manager->flush();
    }

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     *
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->kernel = $container->get('kernel');
    }
}
