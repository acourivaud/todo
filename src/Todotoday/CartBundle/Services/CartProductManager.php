<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 31/01/17
 * Time: 16:54
 */

namespace Todotoday\CartBundle\Services;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CartBundle\Entity\CartProduct;
use Todotoday\CartBundle\Exception\NotAdherentCartBundleException;
use Todotoday\CartBundle\Exception\ProductNotFoundCartBundleException;
use Todotoday\CartBundle\Repository\CartProductRepository;
use Todotoday\CatalogBundle\Exceptions\ProductNotFoundException;
use Todotoday\CatalogBundle\Services\CatalogManager;
use Todotoday\CheckoutBundle\Entity\Supplier;
use Symfony\Component\HttpFoundation\RequestStack;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class CartProductManager
 *
 * @package Todotoday\CartBundle\Services
 */
class CartProductManager
{
    /**
     * @var TokenStorageInterface
     */
    private $token;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var CartProductRepository
     */
    private $repository;

    /**
     * @var CatalogManager
     */
    private $catalogManager;

    /**
     * @var RequestStack
     */
    private $request;

    /**
     * CartProductManager constructor.
     *
     * @param TokenStorageInterface  $token
     * @param EntityManagerInterface $entityManager
     * @param CartProductRepository  $repository
     * @param CatalogManager         $catalogManager
     * @param RequestStack           $request
     */
    public function __construct(
        TokenStorageInterface $token,
        EntityManagerInterface $entityManager,
        CartProductRepository $repository,
        CatalogManager $catalogManager,
        RequestStack $request
    ) {
        $this->token = $token;
        $this->em = $entityManager;
        $this->repository = $repository;
        $this->catalogManager = $catalogManager;
        $this->request = $request;
    }

    /**
     * @param Agency      $agency
     * @param CartProduct $formCartProduct
     *
     * @return null|object|CartProduct
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Todotoday\CatalogBundle\Exceptions\ProductNotFoundException
     * @throws \InvalidArgumentException
     * @throws \LogicException
     * @throws \UnexpectedValueException
     * @throws \Todotoday\CartBundle\Exception\NotAdherentCartBundleException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     */
    public function addProduct(Agency $agency, CartProduct $formCartProduct)
    {
        if ($cartProduct = $this->getCartProduct($agency, $formCartProduct->getProduct())) {
            $cartProduct->setQuantity($cartProduct->getQuantity() + $formCartProduct->getQuantity());
        } else {
            $cartProduct = $formCartProduct;
        }
        $this->isProductFound($agency, $formCartProduct->getProduct());
        $cartProduct
            ->setAdherent($this->token->getToken()->getUser())
            ->setAgency($agency)
            ->setCurrency($agency->getCurrency());
        $this->em->persist($cartProduct);
        $this->em->flush();

        return $this->getCart($agency);
    }

    /**
     *  Empty cart for current adherent and agency
     *
     * @param Agency $agency
     *
     * @return null
     * @throws \InvalidArgumentException
     * @throws \LogicException
     * @throws \Todotoday\CartBundle\Exception\NotAdherentCartBundleException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \UnexpectedValueException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function emptyCart(Agency $agency)
    {
        if ($cartProducts = $this->getCart($agency)) {
            foreach ($cartProducts as $cartProduct) {
                $this->em->remove($cartProduct);
            }
            $this->em->flush();
        }

        return null;
    }

    /**
     * @param Agency $agency
     *
     * @return CartProduct[]|null
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \InvalidArgumentException
     * @throws \LogicException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws NotAdherentCartBundleException
     */
    public function getCart(Agency $agency): ?array
    {
        if (!$this->token->getToken()->getUser() instanceof Adherent) {
            throw new NotAdherentCartBundleException('Cart must belongs to an adherent');
        }

        /** @var CartProduct[] $cartProducts */
        $cartProducts = $this->repository->findBy(
            array(
                'adherent' => $this->token->getToken()->getUser(),
            ),
            array(
                'product' => 'ASC',
            )
        );
        /** @var CartProduct[] $result */
        $result = [];
        foreach ($cartProducts as $cartProduct) {
            if ($product = $this->hydrateProductDetail($agency, $cartProduct, $this->token->getToken()->getUser())) {
                $result[] = $product;
            }
            //dump($cartProduct);
        }
        //die();
        return $result;
    }

    /**
     * Get an instance of cartproduct for selected product, current adherent and agency
     *
     * @param Agency $agency
     * @param string $product
     *
     * @return null|object|CartProduct
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \InvalidArgumentException
     * @throws \Todotoday\CartBundle\Exception\NotAdherentCartBundleException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \LogicException
     */
    public function getCartProduct(Agency $agency, string $product): ?CartProduct
    {
        if (!$this->token->getToken()->getUser() instanceof Adherent) {
            throw new NotAdherentCartBundleException('Cart must belongs to an adherent');
        }

        /** @var CartProduct $product */
        if (!$product = $this->repository->findOneBy(
            array(
                'product' => $product,
                'adherent' => $this->token->getToken()->getUser(),
            )
        )
        ) {
            return null;
        }

        return $this->hydrateProductDetail(
            $agency,
            $product,
            $this->token->getToken()->getUser()
        );
    }

    /**
     * @param Agency $agency
     * @param string $product
     *
     * @return CartProduct[]|null
     * @throws \InvalidArgumentException
     * @throws \LogicException
     * @throws \Todotoday\CartBundle\Exception\NotAdherentCartBundleException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \Todotoday\CartBundle\Exception\ProductNotFoundCartBundleException
     * @throws \UnexpectedValueException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function removeProduct(Agency $agency, string $product)
    {
        if (!$cartProduct = $this->getCartProduct($agency, $product)) {
            throw new ProductNotFoundCartBundleException('Product not found in cart');
        }
        $this->em->remove($cartProduct);
        $this->em->flush();

        return $this->getCart($agency);
    }

    /**
     * Method to update product's quantity
     *
     * @param Agency      $agency
     * @param CartProduct $formCartProduct
     *
     * @return null|object|CartProduct
     * @throws \InvalidArgumentException
     * @throws \LogicException
     * @throws \UnexpectedValueException
     * @throws \Todotoday\CartBundle\Exception\NotAdherentCartBundleException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws ProductNotFoundCartBundleException
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function updateProduct(Agency $agency, CartProduct $formCartProduct)
    {
        if (!$cartProduct = $this->getCartProduct($agency, $formCartProduct->getProduct())) {
            throw new ProductNotFoundCartBundleException('Product not found in cart');
        }
        $cartProduct->setQuantity($formCartProduct->getQuantity());
        $this->em->persist($cartProduct);
        $this->em->flush();

        return $this->getCart($agency);
    }

    /**
     * Return true if at least one product in cart need depository
     *
     * @param Agency $agency
     *
     * @return bool
     * @throws \UnexpectedValueException
     * @throws \Todotoday\CartBundle\Exception\NotAdherentCartBundleException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function isCartNeedDepository(Agency $agency): bool
    {
        $cartProducts = $this->getCart($agency);
        foreach ($cartProducts as $cartProduct) {
            if ($cartProduct->isDepositoryNeeded()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param Agency $agency
     *
     * @return float
     * @throws \UnexpectedValueException
     * @throws \Todotoday\CartBundle\Exception\NotAdherentCartBundleException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function cartTotalPrice(Agency $agency): float
    {
        $cartProducts = $this->getCart($agency);

        return $this->getTotalPriceCartProducts($cartProducts);
    }

    /**
     * @param CartProduct[] $cartProducts
     *
     * @return float
     */
    public function getTotalPriceCartProducts(array $cartProducts): float
    {
        $total = 0;

        foreach ($cartProducts as $cartProduct) {
            $total += $cartProduct->getProductPrice() * $cartProduct->getQuantity();
        }

        return $total;
    }

    /**
     * @param Agency $agency
     *
     * @return float
     * @throws \UnexpectedValueException
     * @throws \Todotoday\CartBundle\Exception\NotAdherentCartBundleException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function cartTotalPriceExclTax(Agency $agency): float
    {
        $cartProducts = $this->getCart($agency);

        return $this->getTotalPriceExclTaxCartProducts($cartProducts);
    }

    /**
     * @param CartProduct[] $cartProducts
     *
     * @return float
     */
    public function getTotalPriceExclTaxCartProducts(array $cartProducts): float
    {
        $total = 0;

        foreach ($cartProducts as $cartProduct) {
            $total += $cartProduct->getProductPriceExclTax() * $cartProduct->getQuantity();
        }

        return $total;
    }

    /**
     * @param Agency $agency
     *
     * @return bool
     * @throws \UnexpectedValueException
     * @throws \Todotoday\CartBundle\Exception\NotAdherentCartBundleException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function isEmpty(Agency $agency): bool
    {
        $cartProducts = $this->getCart($agency);
        if ($cartProducts) {
            return false;
        }

        return true;
    }

    /**
     * @param Agency $agency
     *
     * @return Supplier[]
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function getAllSuppliers(Agency $agency): array
    {
        $cartProducts = $this->getCart($agency);
        /** @var Supplier[] $suppliers */
        $suppliers = array();

        foreach ($cartProducts as $cartProduct) {
            $supplierAlreadyExist = false;

            if (!$cartProduct->getVendors()) {
                continue;
            }

            $supplierName = $cartProduct->getVendors();

            /** @var Supplier $actualSupplier */
            foreach ($suppliers as $key => $actualSupplier) {
                if ($actualSupplier->getSupplierName() === $supplierName) {
                    $suppliers[$key]->addCartProduct($cartProduct);
                    $supplierAlreadyExist = true;
                    continue;
                }
            }

            if ($supplierAlreadyExist) {
                continue;
            }

            $supplier = new Supplier();
            $supplier
                ->setSupplierName($supplierName)
                ->addCartProduct($cartProduct);

            $suppliers[] = $supplier;
        }

        return $suppliers;
    }

    /**
     * Hydrate cartProduct with information from catalog
     *
     * @param Agency      $agency
     * @param CartProduct $cartProduct
     *
     * @param Adherent    $adherent
     *
     * @return CartProduct|bool
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \InvalidArgumentException
     */
    private function hydrateProductDetail(Agency $agency, CartProduct $cartProduct, Adherent $adherent)
    {
        $adherent->getLanguage() ? $locale = $adherent->getLanguage() : $locale = $agency->getLanguagesString();

        try {
            if ($productFromCatalog = $this->catalogManager->getProduct(
                $agency,
                $cartProduct->getProduct(),
                $this->request->getCurrentRequest()->getLocale()
            )
            ) {
                $cartProduct
                    ->setProductDescription($productFromCatalog->getDescriptionTrans())
                    ->setProductPrice($productFromCatalog->getAmount())
                    ->setProductPriceExclTax($productFromCatalog->getAmountExclTax())
                    ->setProductName($productFromCatalog->getNameTrans())
                    ->setHierarchy($productFromCatalog->getHierarchy())
                    ->setProductType($productFromCatalog->getProductType())
                    ->setDepositoryNeeded($productFromCatalog->isDepositoryNeeded())
                    ->setStockNeeded($productFromCatalog->isStockNeeded())
                    ->setBookable($productFromCatalog->isBookable())
                    ->setItemModelGroup($productFromCatalog->getItemModelGroup())
                    ->setVendors($productFromCatalog->getVendors());

                return $cartProduct;
            } else {
                return false;
            }
        } catch (ProductNotFoundException $e) {
            throw new ProductNotFoundCartBundleException('Product in the cart not found.');
        }

        return $cartProduct;
    }

    /**
     * Will return an exception if product not found
     *
     * @param Agency $agency
     * @param string $productId
     *
     * @throws \InvalidArgumentException
     * @throws \Todotoday\CatalogBundle\Exceptions\ProductNotFoundException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    private function isProductFound(Agency $agency, string $productId)
    {
        $this->catalogManager->getProduct($agency, $productId);
    }
}
