<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 31/01/17
 * Time: 16:54
 */

namespace Todotoday\CartBundle\Services;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CartBundle\Entity\Wishlist;
use Todotoday\CartBundle\Entity\WishlistProduct;
use Todotoday\CartBundle\Exception\NotAdherentCartBundleException;
use Todotoday\CartBundle\Exception\ProductNotFoundCartBundleException;
use Todotoday\CartBundle\Repository\WishlistProductRepository;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class CartProductManager
 *
 * @package Todotoday\CartBundle\Services
 */
class WishlistProductManager
{
    /**
     * @var TokenStorageInterface
     */
    private $token;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var CartProductRepository
     */
    private $repository;

    /**
     * CartProductManager constructor.
     *
     * @param TokenStorageInterface     $token
     * @param EntityManagerInterface    $entityManager
     * @param WishlistProductRepository $wishlistProductRepository
     */
    public function __construct(
        TokenStorageInterface $token,
        EntityManagerInterface $entityManager,
        WishlistProductRepository $wishlistProductRepository
    ) {
        $this->token = $token;
        $this->em = $entityManager;
        $this->repository = $wishlistProductRepository;
    }

    /**
     * @param WishlistProduct $formWishlistProduct
     * @param Agency          $agency
     *
     * @return null|object|WishlistProduct
     * @throws \LogicException
     * @throws \UnexpectedValueException
     * @throws \Todotoday\CartBundle\Exception\NotAdherentCartBundleException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     *
     */
    public function addProduct(WishlistProduct $formWishlistProduct, Agency $agency) : WishlistProduct
    {
        if ($wishlistProduct = $this->getWishlistProduct(
            $formWishlistProduct->getWishlist(),
            $formWishlistProduct->getProduct()
        )
        ) {
            $wishlistProduct->setQuantity($wishlistProduct->getQuantity() + $formWishlistProduct->getQuantity());
        } else {
            $wishlistProduct = $formWishlistProduct;
        }
        $wishlistProduct->setAdherent($this->token->getToken()->getUser());
        $wishlistProduct->getWishlist()->setAgency($agency);
        $this->em->persist($wishlistProduct);
        $this->em->flush();

        return $wishlistProduct;
    }

    /**
     *  Empty cart for current adherent and agency
     *
     * @param Wishlist $wishlist
     */
    public function emptyWishlist(Wishlist $wishlist)
    {
//        if ($wishlistProduct = $this->getCart()) {
//            foreach ($cartProducts as $cartProduct) {
//                $this->em->remove($cartProduct);
//            }
//            $this->em->flush();
//        }
    }

    /**
     * @param WishlistProduct $wishlistProduct
     *
     * @return null|CartProduct[]
     */
    public function getWishlists(WishlistProduct $wishlistProduct)
    {
//        if (!$this->token->getToken()->getUser() instanceof Adherent) {
//            throw new NotAdherentCartBundleException();
//        }
//
//        return $this->repository->findBy(
//            array(
//
//                'adherent' => $this->token->getToken()->getUser(),
//            )
//        );
    }

    /**
     * Get an instance of cartproduct for selected product, current adhrent and agency
     *
     * @param Wishlist $wishlist
     * @param int      $product
     *
     * @return null|object|WishlistProduct
     * @throws \Todotoday\CartBundle\Exception\NotAdherentCartBundleException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \LogicException
     * @internal param Wishlist $wishlit
     */
    public function getWishlistProduct(Wishlist $wishlist, int $product): ?WishlistProduct
    {
        if (!$this->token->getToken()->getUser() instanceof Adherent) {
            throw new NotAdherentCartBundleException();
        }

        return $this->repository->findOneBy(
            array(
                'wishlist' => $wishlist,
                'product' => $product,
                'adherent' => $this->token->getToken()->getUser(),
            )
        );
    }

    /**
     * @param Wishlist $wishlist
     * @param int      $product
     *
     * @return null
     * @throws \UnexpectedValueException
     * @throws \Todotoday\CartBundle\Exception\NotAdherentCartBundleException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \LogicException
     * @throws \Todotoday\CartBundle\Exception\ProductNotFoundCartBundleException
     */
    public function removeProduct(Wishlist $wishlist, int $product)
    {
        if (!$wishtlistProduct = $this->getWishlistProduct(
            $wishlist,
            $product
        )
        ) {
            throw new ProductNotFoundCartBundleException();
        }
        $this->em->remove($wishtlistProduct);
        $this->em->flush();

        return null;
    }

    /**
     * Method to update product's quantity
     *
     * @param Wishlist        $wishlist
     * @param WishlistProduct $formWishlistProduct
     *
     * @return null|object|WishlistProduct
     * @throws \UnexpectedValueException
     * @throws \Todotoday\CartBundle\Exception\NotAdherentCartBundleException
     * @throws \Symfony\Component\Filesystem\Exception\FileNotFoundException
     * @throws \LogicException
     * @throws \Todotoday\CartBundle\Exception\ProductNotFoundCartBundleException
     */
    public function updateProduct(Wishlist $wishlist, WishlistProduct $formWishlistProduct): WishlistProduct
    {
        if (!$wishtlistProduct = $this->getWishlistProduct(
            $wishlist,
            $formWishlistProduct->getProduct()
        )
        ) {
            throw new ProductNotFoundCartBundleException();
        }
        $wishtlistProduct->setQuantity($formWishlistProduct->getQuantity());
        $this->em->persist($wishtlistProduct);
        $this->em->flush();

        return $wishtlistProduct;
    }
}
