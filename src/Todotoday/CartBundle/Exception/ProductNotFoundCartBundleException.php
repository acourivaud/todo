<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 05/02/17
 * Time: 15:11
 */

namespace Todotoday\CartBundle\Exception;

use Actiane\ToolsBundle\Exceptions\PublishedMessageExceptionInterface;
use Exception;

/**
 * Class ProductNotFoundCartBundleException
 *
 * @package Todotoday\CartBundle\Exception
 */
class ProductNotFoundCartBundleException extends \InvalidArgumentException implements PublishedMessageExceptionInterface
{
    /**
     * @return mixed
     */
    public function getMessages()
    {
        parent::getMessage();
    }

    /**
     * @return int
     */
    public function getCustomCode(): int
    {
        return 400;
    }
}
