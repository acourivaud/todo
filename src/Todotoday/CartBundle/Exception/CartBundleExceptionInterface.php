<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 05/02/17
 * Time: 15:07
 */

namespace Todotoday\CartBundle\Exception;

/**
 * Interface CartBundleExceptionInterface
 *
 * @package Todotoday\CartBundle\Exception
 */
interface CartBundleExceptionInterface
{
}
