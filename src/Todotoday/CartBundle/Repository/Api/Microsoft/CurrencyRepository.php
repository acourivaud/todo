<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 27/02/2017
 * Time: 16:53
 */

namespace Todotoday\CartBundle\Repository\Api\Microsoft;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiRepository;

/**
 * Class CurrencyRepository
 *
 * @category   Todo-Todev
 * @package    Todotoday\CartBundle
 * @subpackage Todotoday\CartBundle\Repository\Api\Microsoft
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class CurrencyRepository extends AbstractApiRepository
{
}
