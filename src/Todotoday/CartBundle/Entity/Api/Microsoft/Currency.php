<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 27/02/2017
 * Time: 15:49
 */

namespace Todotoday\CartBundle\Entity\Api\Microsoft;

use Actiane\ApiConnectorBundle\Lib\EntityProxyTrait;
use Actiane\ApiConnectorBundle\Lib\Mapping as APIConnector;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class Currency
 *
 * @Serializer\ExclusionPolicy("all")
 * @APIConnector\Entity(path="Currencies", repositoryId="todotoday.cart.repository.api.currency")
 *
 * @category   Todo-Todev
 * @package    Todotoday\CartBundle
 * @subpackage Todotoday\CartBundle\Entity\Api\Microsoft
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class Currency
{
    use EntityProxyTrait;

    /**
     * @var int
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="GeneralRoundingRule", type="int")
     */
    protected $generalRoundingRule;

    /**
     * @var int
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="RoundingRuleSalesOrders", type="int")
     */
    protected $roundingRuleSalesOrders;

    /**
     * @var int
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="RoundingRulePrices", type="int")
     */
    protected $roundingRulePrices;

    /**
     * @var int
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="RoundingRuleFixedAssetDepreciation", type="int")
     */
    protected $roundingRuleAssetDepreciation;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="ReferenceCurrencyForTriangulation", type="string")
     */
    protected $referenceCurrencyTriangulation;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="RoundingMethodPurchaseOrders", type="string")
     */
    protected $roundingMethodPurchaseOrders;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="RoundingMethodPrices", type="string")
     */
    protected $roundingMethodPrices;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="Name", type="string")
     */
    protected $name;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="RoundingMethodSalesOrders", type="string")
     */
    protected $roundingMethodSalesOrders;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="Symbol", type="string")
     */
    protected $symbol;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Id()
     * @APIConnector\Column(name="CurrencyCode", type="string")
     */
    protected $currencyCode;

    /**
     * @var int
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="RoundingRulePurchaseOrders", type="int")
     */
    protected $roundingRulePurchaseOrders;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="RoundingMethodFixedAssetDepreciation", type="string")
     */
    protected $roundingMethodAssetDepress;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @APIConnector\Column(name="CurrencyGender", type="string")
     */
    protected $currencyGender;

    /**
     * Get currencyCode
     *
     * @return string
     */
    public function getCurrencyCode(): ?string
    {
        return $this->currencyCode;
    }

    /**
     * Set currencyCode
     *
     * @param string $currencyCode
     *
     * @return Currency
     */
    public function setCurrencyCode(string $currencyCode): Currency
    {
        return $this->setTrigger('currencyCode', $currencyCode);
    }

    /**
     * Get currencyGender
     *
     * @return string
     */
    public function getCurrencyGender(): ?string
    {
        return $this->currencyGender;
    }

    /**
     * Set currencyGender
     *
     * @param string $currencyGender
     *
     * @return Currency
     */
    public function setCurrencyGender(string $currencyGender): Currency
    {
        return $this->setTrigger('currencyGender', $currencyGender);
    }

    /**
     * Get generalRoundingRule
     *
     * @return int
     */
    public function getGeneralRoundingRule(): ?int
    {
        return $this->generalRoundingRule;
    }

    /**
     * Set generalRoundingRule
     *
     * @param int $generalRoundingRule
     *
     * @return Currency
     */
    public function setGeneralRoundingRule(int $generalRoundingRule): Currency
    {
        return $this->setTrigger('generalRoundingRule', $generalRoundingRule);
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Currency
     */
    public function setName(string $name): Currency
    {
        return $this->setTrigger('name', $name);
    }

    /**
     * Get referenceCurrencyTriangulation
     *
     * @return string
     */
    public function getReferenceCurrencyTriangulation(): ?string
    {
        return $this->referenceCurrencyTriangulation;
    }

    /**
     * Set referenceCurrencyTriangulation
     *
     * @param string $referenceCurrencyTriangulation
     *
     * @return Currency
     */
    public function setReferenceCurrencyTriangulation(string $referenceCurrencyTriangulation): Currency
    {
        return $this->setTrigger('referenceCurrencyTriangulation', $referenceCurrencyTriangulation);
    }

    /**
     * Get roundingMethodAssetDepress
     *
     * @return string
     */
    public function getRoundingMethodAssetDepress(): ?string
    {
        return $this->roundingMethodAssetDepress;
    }

    /**
     * Set roundingMethodAssetDepress
     *
     * @param string $roundingMethodAssetDepress
     *
     * @return Currency
     */
    public function setRoundingMethodAssetDepress(string $roundingMethodAssetDepress): Currency
    {
        return $this->setTrigger('roundingMethodAssetDepress', $roundingMethodAssetDepress);
    }

    /**
     * Get roundingMethodPrices
     *
     * @return string
     */
    public function getRoundingMethodPrices(): ?string
    {
        return $this->roundingMethodPrices;
    }

    /**
     * Set roundingMethodPrices
     *
     * @param string $roundingMethodPrices
     *
     * @return Currency
     */
    public function setRoundingMethodPrices(string $roundingMethodPrices): Currency
    {
        return $this->setTrigger('roundingMethodPrices', $roundingMethodPrices);
    }

    /**
     * Get roundingMethodPurchaseOrders
     *
     * @return string
     */
    public function getRoundingMethodPurchaseOrders(): ?string
    {
        return $this->roundingMethodPurchaseOrders;
    }

    /**
     * Set roundingMethodPurchaseOrders
     *
     * @param string $roundingMethodPurchaseOrders
     *
     * @return Currency
     */
    public function setRoundingMethodPurchaseOrders(string $roundingMethodPurchaseOrders): Currency
    {
        return $this->setTrigger('roundingMethodPurchaseOrders', $roundingMethodPurchaseOrders);
    }

    /**
     * Get roundingMethodSalesOrders
     *
     * @return string
     */
    public function getRoundingMethodSalesOrders(): ?string
    {
        return $this->roundingMethodSalesOrders;
    }

    /**
     * Set roundingMethodSalesOrders
     *
     * @param string $roundingMethodSalesOrders
     *
     * @return Currency
     */
    public function setRoundingMethodSalesOrders(string $roundingMethodSalesOrders): Currency
    {
        return $this->setTrigger('roundingMethodSalesOrders', $roundingMethodSalesOrders);
    }

    /**
     * Get roundingRuleAssetDepreciation
     *
     * @return int
     */
    public function getRoundingRuleAssetDepreciation(): ?int
    {
        return $this->roundingRuleAssetDepreciation;
    }

    /**
     * Set roundingRuleAssetDepreciation
     *
     * @param int $roundingRuleAssetDepreciation
     *
     * @return Currency
     */
    public function setRoundingRuleAssetDepreciation(int $roundingRuleAssetDepreciation): Currency
    {
        return $this->setTrigger('roundingRuleAssetDepreciation', $roundingRuleAssetDepreciation);
    }

    /**
     * Get roundingRulePrices
     *
     * @return int
     */
    public function getRoundingRulePrices(): ?int
    {
        return $this->roundingRulePrices;
    }

    /**
     * Set roundingRulePrices
     *
     * @param int $roundingRulePrices
     *
     * @return Currency
     */
    public function setRoundingRulePrices(int $roundingRulePrices): Currency
    {
        return $this->setTrigger('roundingRulePrices', $roundingRulePrices);
    }

    /**
     * Get roundingRulePurchaseOrders
     *
     * @return int
     */
    public function getRoundingRulePurchaseOrders(): ?int
    {
        return $this->roundingRulePurchaseOrders;
    }

    /**
     * Set roundingRulePurchaseOrders
     *
     * @param int $roundingRulePurchaseOrders
     *
     * @return Currency
     */
    public function setRoundingRulePurchaseOrders(int $roundingRulePurchaseOrders): Currency
    {
        return $this->setTrigger('roundingRulePurchaseOrders', $roundingRulePurchaseOrders);
    }

    /**
     * Get roundingRuleSalesOrders
     *
     * @return int
     */
    public function getRoundingRuleSalesOrders(): ?int
    {
        return $this->roundingRuleSalesOrders;
    }

    /**
     * Set roundingRuleSalesOrders
     *
     * @param int $roundingRuleSalesOrders
     *
     * @return Currency
     */
    public function setRoundingRuleSalesOrders(int $roundingRuleSalesOrders): Currency
    {
        return $this->setTrigger('roundingRuleSalesOrders', $roundingRuleSalesOrders);
    }

    /**
     * Get symbol
     *
     * @return string
     */
    public function getSymbol(): ?string
    {
        return $this->symbol;
    }

    /**
     * Set symbol
     *
     * @param string $symbol
     *
     * @return Currency
     */
    public function setSymbol(string $symbol): Currency
    {
        return $this->setTrigger('symbol', $symbol);
    }
}
