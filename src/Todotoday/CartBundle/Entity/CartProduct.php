<?php declare(strict_types=1);

namespace Todotoday\CartBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * CartProduct
 *
 * @ORM\Table(schema="cart", name="cart_product")
 * @ORM\Entity(repositoryClass="Todotoday\CartBundle\Repository\CartProductRepository")
 * @Serializer\ExclusionPolicy("all")
 * @Serializer\AccessorOrder("custom", custom = {"comment", "adherent" ,"agency"})
 */
class CartProduct extends AbstractCartProductType
{
    /**
     * @var Adherent
     *
     * @ORM\ManyToOne(targetEntity="Todotoday\AccountBundle\Entity\Adherent", inversedBy="cartProducts")
     * @Serializer\Expose()
     * @Serializer\Groups("adherent")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $adherent;

    /**
     * @var Agency
     *
     * @ORM\ManyToOne(targetEntity="Todotoday\CoreBundle\Entity\Agency", inversedBy="cartProducts")
     *
     * @Serializer\Expose()
     * @Serializer\Groups("agency")
     */
    protected $agency;

    /**
     * @var string
     * @Serializer\Expose()
     * @Serializer\Groups("adherent")
     */
    protected $comment;

    /**
     * @var null?string
     *
     * @ORM\Column(name="delivery", type="string", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups("adherent")
     */
    protected $delivery;

    /**
     * Get adherent
     *
     * @return Adherent
     */
    public function getAdherent(): Adherent
    {
        return $this->adherent;
    }

    /**
     * Set adherent
     *
     * @param Adherent $adherent
     *
     * @return CartProduct
     */
    public function setAdherent(Adherent $adherent = null): self
    {
        $this->adherent = $adherent;

        return $this;
    }

    /**
     * Get agency
     *
     * @return Agency
     */
    public function getAgency(): Agency
    {
        return $this->agency;
    }

    /**
     * Set agency
     *
     * @param Agency $agency
     *
     * @return CartProduct
     */
    public function setAgency(Agency $agency = null): self
    {
        $this->agency = $agency;

        return $this;
    }

    /**
     * @Serializer\VirtualProperty()
     * @Serializer\SerializedName("agency_slug")
     * @return null|string
     */
    public function getAgencySlug(): ?string
    {
        return $this->agency->getSlug();
    }

    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     *
     * @return CartProduct
     */
    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get delivery
     *
     * @return null|string
     */
    public function getDelivery(): ?string
    {
        return $this->delivery;
    }

    /**
     * Set delivery
     *
     * @param null|string $delivery
     *
     * @return CartProduct
     */
    public function setDelivery(?string $delivery): self
    {
        $this->delivery = $delivery;

        return $this;
    }
}
