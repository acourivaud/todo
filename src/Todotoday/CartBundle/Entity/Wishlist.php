<?php declare(strict_types = 1);

namespace Todotoday\CartBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Wishlist
 *
 * @ORM\Table(name="wishlist", schema="cart")
 * @ORM\Entity(repositoryClass="Todotoday\CartBundle\Repository\WishlistRepository")
 */
class Wishlist
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64)
     */
    private $name;

    /**
     * @var Adherent
     *
     * @ORM\ManyToOne(targetEntity="Todotoday\AccountBundle\Entity\Adherent", inversedBy="wishlists")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $adherent;

    /**
     * @var Agency
     *
     * @ORM\ManyToOne(targetEntity="Todotoday\CoreBundle\Entity\Agency", inversedBy="wishlists")
     */
    private $agency;

    /**
     * @var WishlistProduct
     *
     * @ORM\OneToMany(targetEntity="Todotoday\CartBundle\Entity\WishlistProduct", mappedBy="wishlist")
     */
    private $wishlistProducts;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime")
     */
    private $updatedAt;

    /**
     * Wishlist constructor.
     */
    public function __construct()
    {
        $this->wishlistProducts = new ArrayCollection();
    }

    /**
     * Add wishlistProduct
     *
     * @param WishlistProduct $wishlistProduct
     *
     * @return Wishlist
     */
    public function addWishlistProduct(WishlistProduct $wishlistProduct)
    {
        $this->wishlistProducts[] = $wishlistProduct;

        return $this;
    }

    /**
     * Get adherent
     *
     * @return \Todotoday\AccountBundle\Entity\Adherent
     */
    public function getAdherent(): Adherent
    {
        return $this->adherent;
    }

    /**
     * Set adherent
     *
     * @param \Todotoday\AccountBundle\Entity\Adherent $adherent
     *
     * @return Wishlist
     */
    public function setAdherent(Adherent $adherent = null): self
    {
        $this->adherent = $adherent;

        return $this;
    }

    /**
     * Get agency
     *
     * @return \Todotoday\CoreBundle\Entity\Agency
     */
    public function getAgency(): Agency
    {
        return $this->agency;
    }

    /**
     * Set agency
     *
     * @param \Todotoday\CoreBundle\Entity\Agency $agency
     *
     * @return Wishlist
     */
    public function setAgency(Agency $agency = null): self
    {
        $this->agency = $agency;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt(): \Datetime
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Wishlist
     */
    public function setCreatedAt($createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Wishlist
     */
    public function setName($name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Wishlist
     */
    public function setUpdatedAt($updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get wishlistProducts
     *
     * @return \Doctrine\Common\Collections\Collection|WishlistProduct[]
     */
    public function getWishlistProducts()
    {
        return $this->wishlistProducts;
    }

    /**
     * Remove wishlistProduct
     *
     * @param WishlistProduct $wishlistProduct
     *
     * @return Wishlist
     */
    public function removeWishlistProduct(WishlistProduct $wishlistProduct): self
    {
        $this->wishlistProducts->removeElement($wishlistProduct);

        return $this;
    }
}
