<?php declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 31/01/17
 * Time: 11:27
 */

namespace Todotoday\CartBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use DateTime;

/**
 * Class AbstractCartProductType
 *
 * @package Todotoday\CartBundle\Entity
 * @ORM\MappedSuperclass
 * @Serializer\ExclusionPolicy("all")
 */
abstract class AbstractCartProductType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="product", type="string")
     * @Serializer\Expose()
     * @Serializer\SerializedName("item_id")
     */
    protected $product;

    /**
     * @var int
     *
     * @ORM\Column(name="quantity", type="integer")
     * @Assert\NotBlank()
     * @Assert\Range(
     *     min="1"
     * )
     * @Serializer\Expose()
     */
    protected $quantity;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="addedAt", type="datetime")
     * @Serializer\Expose()
     */
    protected $addedAt;

    /**
     * @var string
     * @Serializer\Expose()
     * @Serializer\SerializedName("name_trans")
     */
    protected $productName;

    /**
     * @var float
     * @Serializer\Expose()
     * @Serializer\SerializedName("amount")
     */
    protected $productPrice;

    /**
     * @var float
     * @Serializer\Expose()
     * @Serializer\SerializedName("amount_excl_tax")
     */
    protected $productPriceExclTax;

    /**
     * @var string
     * @Serializer\Expose()
     * @Serializer\SerializedName("description_trans")
     */
    protected $productDescription;

    /**
     * @var string
     * @Serializer\Expose()
     */
    protected $hierarchy;

    /**
     * @var string
     * @Serializer\Expose()
     */
    protected $productType;

    /**
     * @var string
     * @Serializer\Expose()
     * @Serializer\SerializedName("item_model_group")
     */
    protected $itemModelGroup;

    /**
     * @var string
     * @ORM\Column(name="currency", type="string", nullable=false)
     * @Serializer\Expose()
     */
    protected $currency;

    /**
     * @var bool
     * @Serializer\Expose()
     */
    protected $depositoryNeeded;

    /**
     * @var bool
     * @Serializer\Expose()
     */
    protected $stockNeeded;

    /**
     * @var bool
     * @Serializer\Expose()
     */
    protected $bookable;

    /**
     * @var string
     * @Serializer\Expose()
     */
    protected $vendors;

    /**
     * AbstractCartProductType constructor.
     */
    public function __construct()
    {
        $this->addedAt = new DateTime();
        $this->depositoryNeeded = false;
        $this->stockNeeded = false;
        $this->bookable = false;
        $this->productPrice = 0.;
    }

    /**
     * Get addedAt
     *
     * @return \DateTime
     */
    public function getAddedAt(): \DateTime
    {
        return $this->addedAt;
    }

    /**
     * Set addedAt
     *
     * @param \DateTime $addedAt
     *
     * @return AbstractCartProductType
     */
    public function setAddedAt($addedAt): self
    {
        $this->addedAt = $addedAt;

        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     *
     * @return AbstractCartProductType
     */
    public function setCurrency(?string $currency): AbstractCartProductType
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getHierarchy(): ?string
    {
        return $this->hierarchy;
    }

    /**
     * @param string $hierarchy
     *
     * @return AbstractCartProductType
     */
    public function setHierarchy(?string $hierarchy): AbstractCartProductType
    {
        $this->hierarchy = $hierarchy;

        return $this;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getItemModelGroup(): ?string
    {
        return $this->itemModelGroup;
    }

    /**
     * @param string $itemModelGroup
     *
     * @return AbstractCartProductType
     */
    public function setItemModelGroup(?string $itemModelGroup): AbstractCartProductType
    {
        $this->itemModelGroup = $itemModelGroup;

        return $this;
    }

    /**
     * Get product
     *
     * @return string
     */
    public function getProduct(): ?string
    {
        return $this->product;
    }

    /**
     * Set product
     *
     * @param string $product
     *
     * @return AbstractCartProductType
     */
    public function setProduct(?string $product): self
    {
        $this->product = $product;

        return $this;
    }

    /**
     * @return string
     */
    public function getProductDescription(): ?string
    {
        return $this->productDescription;
    }

    /**
     * @param string $productDescription
     *
     * @return AbstractCartProductType
     */
    public function setProductDescription(?string $productDescription): AbstractCartProductType
    {
        $this->productDescription = $productDescription;

        return $this;
    }

    /**
     * @return string
     */
    public function getProductName(): ?string
    {
        return $this->productName;
    }

    /**
     * @param string $productName
     *
     * @return AbstractCartProductType
     */
    public function setProductName(?string $productName): AbstractCartProductType
    {
        $this->productName = $productName;

        return $this;
    }

    /**
     * @return float
     */
    public function getProductPrice(): float
    {
        return (float) $this->productPrice;
    }

    /**
     * @param float $productPrice
     *
     * @return AbstractCartProductType
     */
    public function setProductPrice(?float $productPrice): AbstractCartProductType
    {
        $this->productPrice = (float) $productPrice;

        return $this;
    }

    /**
     * @return float
     */
    public function getProductPriceExclTax(): float
    {
        return (float) $this->productPriceExclTax;
    }

    /**
     * @param float $productPriceExclTax
     *
     * @return AbstractCartProductType
     */
    public function setProductPriceExclTax(?float $productPriceExclTax): AbstractCartProductType
    {
        $this->productPriceExclTax = (float) $productPriceExclTax;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getProductType(): ?string
    {
        return $this->productType;
    }

    /**
     * @param string $productType
     *
     * @return AbstractCartProductType
     */
    public function setProductType(?string $productType): AbstractCartProductType
    {
        $this->productType = $productType;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return int
     */
    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     *
     * @return AbstractCartProductType
     */
    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @return bool
     */
    public function isBookable(): bool
    {
        return (bool) $this->bookable;
    }

    /**
     * @param bool $bookable
     *
     * @return AbstractCartProductType
     */
    public function setBookable(bool $bookable): AbstractCartProductType
    {
        $this->bookable = $bookable;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDepositoryNeeded(): bool
    {
        return (bool) $this->depositoryNeeded;
    }

    /**
     * @param bool $depositoryNeeded
     *
     * @return AbstractCartProductType
     */
    public function setDepositoryNeeded(bool $depositoryNeeded): AbstractCartProductType
    {
        $this->depositoryNeeded = $depositoryNeeded;

        return $this;
    }

    /**
     * @return bool
     */
    public function isStockNeeded(): bool
    {
        return (bool) $this->stockNeeded;
    }

    /**
     * @param bool $stockNeeded
     *
     * @return AbstractCartProductType
     */
    public function setStockNeeded(bool $stockNeeded): AbstractCartProductType
    {
        $this->stockNeeded = $stockNeeded;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getVendors(): ?string
    {
        return $this->vendors;
    }

    /**
     * @param null|string $vendors
     *
     * @return AbstractCartProductType
     */
    public function setVendors(?string $vendors): self
    {
        $this->vendors = $vendors;

        return $this;
    }
}
