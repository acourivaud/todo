<?php declare(strict_types = 1);

namespace Todotoday\CartBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * WishListProduct
 *
 * @ORM\Table(schema="cart", name="wishlist_product")
 * @ORM\Entity(repositoryClass="Todotoday\CartBundle\Repository\WishlistProductRepository")
 * @Serializer\ExclusionPolicy("all")
 */
class WishlistProduct extends AbstractCartProductType
{
    /**
     * fields : name
     * Wishlist name
     *
     * @var string
     *
     * @ORM\Column(type="string")
     * @Serializer\Groups({"Wishlist"})
     * @Serializer\Expose()
     */
    private $name;

    /**
     * @var Wishlist
     *
     * @ORM\ManyToOne(targetEntity="Todotoday\CartBundle\Entity\Wishlist", inversedBy="wishlistProducts",
     *     cascade={"PERSIST"})
     */
    private $wishlist;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return WishlistProduct
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get wishlist
     *
     * @return \Todotoday\CartBundle\Entity\Wishlist
     */
    public function getWishlist(): Wishlist
    {
        return $this->wishlist;
    }

    /**
     * Set wishlist
     *
     * @param \Todotoday\CartBundle\Entity\Wishlist $wishlist
     *
     * @return WishlistProduct
     */
    public function setWishlist(Wishlist $wishlist = null): self
    {
        $this->wishlist = $wishlist;

        return $this;
    }
}
