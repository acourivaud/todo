<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 18/06/17
 * Time: 17:33
 */

namespace Todotoday\PaymentBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Todotoday\AccountBundle\Entity\Account;

class ChangePaymentTypeEvent extends Event
{
    const NAME = 'account.change_payment';

    /**
     * @var Account
     */
    private $account;

    /**
     * ChangePaymentTypeEvent constructor.
     *
     * @param Account $account
     */
    public function __construct(Account $account)
    {
        $this->account = $account;
    }

    /**
     * @return Account
     */
    public function getAccount(): Account
    {
        return $this->account;
    }
}
