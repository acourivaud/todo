<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 23/06/17
 * Time: 06:25
 */

namespace Todotoday\PaymentBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Todotoday\AccountBundle\Entity\Account;

/**
 * Class ChangePaymentToDirectValidEvent
 * @package Todotoday\PaymentBundle\Event
 */
class ChangePaymentToDirectValidEvent extends Event
{
    const NAME = 'account.change_payment_direct_valid';

    /**
     * @var Account
     */
    private $account;

    /**
     * ChangePaymentToDirectValidEvent constructor.
     *
     * @param Account $account
     */
    public function __construct(Account $account)
    {
        $this->account = $account;
    }

    /**
     * @return Account
     */
    public function getAccount(): ?Account
    {
        return $this->account;
    }
}
