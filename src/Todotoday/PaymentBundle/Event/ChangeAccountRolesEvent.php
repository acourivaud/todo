<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 20/07/17
 * Time: 10:22
 */

namespace Todotoday\PaymentBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Todotoday\AccountBundle\Entity\Account;

/**
 * Class ChangeAccountRolesEvent
 * @package Todotoday\PaymentBundle\Event
 */
class ChangeAccountRolesEvent extends Event
{
    const NAME = 'account.change.roles';

    /**
     * @var Account
     */
    private $account;

    /**
     * ChangeAccountRolesEvent constructor.
     *
     * @param Account $account
     */
    public function __construct(Account $account)
    {
        $this->account = $account;
    }

    /**
     * @return Account
     */
    public function getAccount(): ?Account
    {
        return $this->account;
    }
}
