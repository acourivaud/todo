<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 17/06/17
 * Time: 18:15
 */

namespace Todotoday\PaymentBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Todotoday\AccountBundle\Entity\Account;

/**
 * Class SepaWaitingForSignatureEvent
 * @package Todotoday\PaymentBundle\Event
 */
class SepaWaitingForSignatureEvent extends Event
{
    const NAME = 'account.sepa.waiting_signature';

    /**
     * @var Account
     */
    protected $account;

    /**
     * SepaWaitingForSignature constructor.
     *
     * @param Account $account
     */
    public function __construct(Account $account)
    {
        $this->account = $account;
    }

    /**
     * @return Account
     */
    public function getAccount(): Account
    {
        return $this->account;
    }
}
