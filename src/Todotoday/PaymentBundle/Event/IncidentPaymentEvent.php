<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 18/06/17
 * Time: 16:39
 */

namespace Todotoday\PaymentBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Todotoday\AccountBundle\Entity\Account;

/**
 * Class IncidentPaymentEvent
 * @package Todotoday\PaymentBundle\Event
 */
class IncidentPaymentEvent extends Event
{
    const NAME = 'account.incident_payment';

    /**
     * @var Account
     */
    private $account;

    /**
     * IncidentPaymentEvent constructor.
     *
     * @param Account $account
     */
    public function __construct(Account $account)
    {
        $this->account = $account;
    }

    /**
     * @return Account
     */
    public function getAccount(): Account
    {
        return $this->account;
    }
}
