<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 18/06/17
 * Time: 13:42
 */

namespace Todotoday\PaymentBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Todotoday\AccountBundle\Entity\Account;

/**
 * Class NoPaymentMethodEvent
 * @package Todotoday\PaymentBundle\Event
 */
class NoPaymentMethodEvent extends Event
{
    const NAME = 'account.no_payment';

    /**
     * @var Account
     */
    private $account;

    /**
     * ValidPaymentMethod constructor.
     *
     * @param Account $account
     */
    public function __construct(Account $account)
    {
        $this->account = $account;
    }

    /**
     * @return Account
     */
    public function getAccount(): Account
    {
        return $this->account;
    }
}
