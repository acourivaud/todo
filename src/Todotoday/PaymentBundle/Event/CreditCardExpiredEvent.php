<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 23/06/17
 * Time: 06:36
 */

namespace Todotoday\PaymentBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Todotoday\AccountBundle\Entity\Account;

/**
 * Class CreditCardExpiredEvent
 * @package Todotoday\PaymentBundle\Event
 */
class CreditCardExpiredEvent extends Event
{
    const NAME = 'account.credit_card_expired';

    /**
     * @var Account
     */
    private $account;

    /**
     * CreditCardExpiredEvent constructor.
     *
     * @param Account $account
     */
    public function __construct(Account $account)
    {
        $this->account = $account;
    }

    /**
     * @return Account
     */
    public function getAccount(): ?Account
    {
        return $this->account;
    }
}
