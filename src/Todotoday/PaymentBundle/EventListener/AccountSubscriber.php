<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 17/06/17
 * Time: 17:58
 */

namespace Todotoday\PaymentBundle\EventListener;

use Actiane\PaymentBundle\Enum\PaymentTypeEnum;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Inflector\Inflector;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\AccountBundle\Enum\PaymentStatusEnum;
use Todotoday\PaymentBundle\Event\ChangeAccountRolesEvent;
use Todotoday\PaymentBundle\Event\ChangePaymentToDirectValidEvent;
use Todotoday\PaymentBundle\Event\ChangePaymentTypeEvent;
use Todotoday\PaymentBundle\Event\CreditCardExpiredEvent;
use Todotoday\PaymentBundle\Event\IncidentPaymentEvent;
use Todotoday\PaymentBundle\Event\SepaWaitingForSignatureEvent;
use Todotoday\PaymentBundle\Event\ValidPaymentMethodEvent;
use Todotoday\PaymentBundle\Exception\TrackedMethodFieldNotFoundException;

/**
 * Class AccountSubscriber
 *
 * @package Todotoday\PaymentBundle\EventListener
 */
class AccountSubscriber implements EventSubscriber
{
    /**
     * @var array
     */
    protected $changedField =
        array(
            'paymentTypeSelected',
            'paymentMethodStatus',
            'roles',
        );

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * @var array[]
     */
    protected $callBacks;

    /**
     * AccountSubscriber constructor.
     *
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
        $this->callBacks = array();
    }

    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return array
     */
    public function getSubscribedEvents(): array
    {
        return array(
            'onFlush',
            'postFlush',
            'prePersist',
        );
    }

    /**
     * Only use if adherent is created throw api
     * We create an exception with ROLE_MIGRATION. It is important to delete that role after adherent set its pwd
     *
     * @param LifecycleEventArgs $args
     *
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     */
    public function prePersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getEntity();
        if (!$entity instanceof Account || $entity->hasRole('ROLE_MIGRATION')) {
            return;
        }
    }

    /**
     * @param OnFlushEventArgs $args
     *
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     * @throws \Todotoday\PaymentBundle\Exception\TrackedMethodFieldNotFoundException
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     */
    public function onFlush(OnFlushEventArgs $args): void
    {
        $entityManager = $args->getEntityManager();
        $unitOfWork = $entityManager->getUnitOfWork();
        $updatedEntities = $unitOfWork->getScheduledEntityUpdates();

        foreach ($updatedEntities as $updatedEntity) {
            if ($updatedEntity instanceof Account) {
                $changeset = $unitOfWork->getEntityChangeSet($updatedEntity);
                if (!is_array($changeset)) {
                    return;
                }

                foreach ($this->changedField as $field) {
                    if (array_key_exists($field, $changeset)) {
                        $method = 'track' . ucfirst($field);
                        if (!method_exists($this, $method)) {
                            throw new TrackedMethodFieldNotFoundException($field);
                        }
                        $this->callBacks[$field] = array(array($this, $method), array($changeset, $updatedEntity));
                    }
                }
            }
        }
    }

    /**
     * Do postFlush
     *
     * @return void
     */
    public function postFlush(): void
    {
        if ($this->callBacks) {
            $callBacks = $this->callBacks;
            $this->callBacks = array();
            foreach ($callBacks as $callBack) {
                call_user_func_array($callBack[0], $callBack[1]);
            }
        }
    }

    /**
     * @param array   $changeset
     * @param Account $account
     *
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     */
    protected function trackPaymentMethodStatus(array $changeset, Account $account): void
    {
        /** @var PaymentStatusEnum[] $changes */
        $changes = $changeset['paymentMethodStatus'];
        $nullEnum = PaymentStatusEnum::getNewInstance(PaymentStatusEnum::__DEFAULT);
        $previousValueForField = $changes[0] ?? $nullEnum;
        $newValueForField = $changes[1] ?? $nullEnum;

        if ($previousValueForField->get() !== $newValueForField->get()) {
            $method = 'changePaymentStatusTo' . ucfirst(Inflector::camelize($newValueForField->get()));
            if (method_exists($this, $method)) {
                $this->$method($account);
            }
        }
    }

    /**
     * @param array   $changeset
     * @param Account $account
     *
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     */
    protected function trackPaymentTypeSelected(array $changeset, Account $account): void
    {
        /** @var PaymentTypeEnum[] $changes */
        $changes = $changeset['paymentTypeSelected'];
        $nullEnum = PaymentTypeEnum::getNewInstance(PaymentTypeEnum::__DEFAULT);
        $previousValueForField = $changes[0] ?? $nullEnum;
        $newValueForField = $changes[1] ?? $nullEnum;

        if ($previousValueForField->get() !== $newValueForField->get()) {
            $event = new ChangePaymentTypeEvent($account);
            $this->dispatcher->dispatch($event::NAME, $event);

            if (!is_null($newValueForField->get())) {
                $method = 'changePaymentTo' . ucfirst(Inflector::camelize($newValueForField->get()));
                if (method_exists($this, $method)) {
                    $this->$method($account);
                }
            }
        }
    }

    /**
     * @param array   $changeset
     * @param Account $account
     */
    protected function trackRoles(array $changeset, Account $account): void
    {
        [$previousValueForField, $newValueForField] = $changeset['roles'];
        if ($previousValueForField !== $newValueForField) {
            $event = new ChangeAccountRolesEvent($account);
            $this->dispatcher->dispatch($event::NAME, $event);
        }
    }

    /**
     * @param Account $account
     */
    protected function changePaymentToSepa(Account $account): void
    {
        $event = new SepaWaitingForSignatureEvent($account);
        $this->dispatcher->dispatch($event::NAME, $event);
    }

    /**
     * @param Account $account
     */
    protected function changePaymentToStripe(Account $account): void
    {
        $this->changePaymentTopostfin($account);
    }

    /**
     * @param Account $account
     */
    protected function changePaymentToStripeDir(Account $account): void
    {
        $this->changePaymentTopostfin($account);
    }

    /**
     * @param Account $account
     */
    protected function changePaymentToVirement(Account $account): void
    {
        $this->changePaymentTopostfin($account);
    }

    /**
     * @param Account $account
     */
    protected function changePaymentToCheque(Account $account): void
    {
        $this->changePaymentTopostfin($account);
    }

    /**
     * @param Account $account
     */
    protected function changePaymentTopostfin(Account $account): void
    {
        $event = new ChangePaymentToDirectValidEvent($account);
        $this->dispatcher->dispatch($event::NAME, $event);
    }

    /**
     * @param Account $account
     */
    protected function changePaymentToPaypal(Account $account): void
    {
        $event = new ChangePaymentToDirectValidEvent($account);
        $this->dispatcher->dispatch($event::NAME, $event);
    }

    /**
     * @param Account $account
     */
    protected function changePaymentStatusToValid(Account $account): void
    {
        $event = new ValidPaymentMethodEvent($account);
        $this->dispatcher->dispatch($event::NAME, $event);
    }

    /**
     * @param Account $account
     */
    protected function changePaymentStatusToPaymentIncident(Account $account): void
    {
        $event = new IncidentPaymentEvent($account);
        $this->dispatcher->dispatch($event::NAME, $event);
    }

    /**
     * @param Account $account
     */
    protected function changePaymentStatusToCreditCardExpired(Account $account): void
    {
        $event = new CreditCardExpiredEvent($account);
        $this->dispatcher->dispatch($event::NAME, $event);
    }
}
