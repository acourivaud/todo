<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 20/07/17
 * Time: 10:27
 */

namespace Todotoday\PaymentBundle\EventListener;

use Actiane\AlertBundle\Enum\AlertStatusEnum;
use Actiane\AlertBundle\Services\AlertManager;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\Router;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\PaymentBundle\Event\ChangeAccountRolesEvent;

/**
 * Class AccountRoleSubscriber
 * @package Todotoday\PaymentBundle\EventListener
 */
class AccountRoleSubscriber implements EventSubscriberInterface
{
    public const ALERT_NAME = 'role';

    public static $rolesNeeded = [
        'ROLE_ADHERENT',
        'ROLE_NOT_ADHERENT',
        'ROLE_CO_ADHERENT',
    ];

    /**
     * @var AlertManager
     */
    private $alertManager;

    /**
     * @var ObjectManager
     */
    private $manager;

    /**
     * @var Router
     */
    private $router;

    /**
     * AccountRoleSubscriber constructor.
     *
     * @param ObjectManager $manager
     * @param AlertManager  $alertManager
     * @param Router        $router
     */
    public function __construct(ObjectManager $manager, AlertManager $alertManager, Router $router)
    {
        $this->alertManager = $alertManager;
        $this->manager = $manager;
        $this->router = $router;
    }

    /**
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents(): array
    {
        return [
            ChangeAccountRolesEvent::NAME => 'changeRole',
        ];
    }

    /**
     * @param ChangeAccountRolesEvent $event
     *
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     */
    public function changeRole(ChangeAccountRolesEvent $event): void
    {
        $account = $event->getAccount();
        if (!$account instanceof Adherent) {
            return;
        }
        $roles = $account->getRoles();

        foreach ($roles as $role) {
            if (in_array($role, self::$rolesNeeded, true)) {
                $this->alertManager->delete($account, self::ALERT_NAME);

                return;
            }
        }

        $alert = $this->alertManager->findOrCreate($account, self::ALERT_NAME);
        $alert->setUrl($this->router->generate('todotoday_account_registration_mandatoryworkflow'))
            ->setLinkLabel('account.alert_registration_incomplete_url')
            ->setMessage('account.alert_registration_incomplete')
            ->setStatus(AlertStatusEnum::getNewInstance(AlertStatusEnum::ERROR))
            ->setMessageDomain('todotoday');

        $this->manager->persist($alert);
        $this->manager->flush();
    }
}
