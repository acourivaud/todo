<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 17/06/17
 * Time: 18:27
 */

namespace Todotoday\PaymentBundle\EventListener;

use Actiane\AlertBundle\Enum\AlertStatusEnum;
use Actiane\AlertBundle\Services\AlertManager;
use Actiane\ApiConnectorBundle\Services\Connections\Microsoft\MicrosoftDynamicsConnection;
use Actiane\PaymentBundle\Enum\PaymentTypeEnum;
use Doctrine\ORM\EntityManagerInterface;
use Predis\Client;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\Router;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Entity\Api\Microsoft\Account as AccountMicrosoft;
use Todotoday\AccountBundle\Enum\PaymentStatusEnum;
use Todotoday\AccountBundle\Repository\Api\Microsoft\AccountRepository;
use Todotoday\AccountBundle\Services\AdherentManager;
use Todotoday\PaymentBundle\Event\ChangeAccountRolesEvent;
use Todotoday\PaymentBundle\Event\ChangePaymentToDirectValidEvent;
use Todotoday\PaymentBundle\Event\ChangePaymentTypeEvent;
use Todotoday\PaymentBundle\Event\CreditCardExpiredEvent;
use Todotoday\PaymentBundle\Event\IncidentPaymentEvent;
use Todotoday\PaymentBundle\Event\NoPaymentMethodEvent;
use Todotoday\PaymentBundle\Event\SepaWaitingForSignatureEvent;
use Todotoday\PaymentBundle\Event\ValidPaymentMethodEvent;

/**
 * Class PaymentSubscriber
 * @package Todotoday\PaymentBundle\EventListener
 */
class PaymentSubscriber implements EventSubscriberInterface
{
    public const ALERT_NAME = 'payment_method';

    /**
     * @var AlertManager
     */
    private $alertManager;

    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var MicrosoftDynamicsConnection
     */
    private $dynamicsConnection;

    /**
     * @var Client
     */
    private $redis;

    /**
     * @var string
     */
    private $baseUrlAcronis;

    /**
     * PaymentSubscriber constructor.
     *
     * @param AlertManager                $alertManager
     * @param EntityManagerInterface      $manager
     * @param Router                      $router
     * @param MicrosoftDynamicsConnection $dynamicsConnection
     * @param Client                      $redis
     * @param string                      $baseUrlAcronis
     */
    public function __construct(
        AlertManager $alertManager,
        EntityManagerInterface $manager,
        Router $router,
        MicrosoftDynamicsConnection $dynamicsConnection,
        Client $redis,
        string $baseUrlAcronis
    ) {
        $this->alertManager = $alertManager;
        $this->manager = $manager;
        $this->router = $router;
        $this->dynamicsConnection = $dynamicsConnection;
        $this->redis = $redis;
        $this->baseUrlAcronis = $baseUrlAcronis;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents(): array
    {
        return array(
            SepaWaitingForSignatureEvent::NAME => 'waitingForSignature',
            ValidPaymentMethodEvent::NAME => 'validPaymentMethod',
            IncidentPaymentEvent::NAME => 'incidentPayment',
            ChangePaymentTypeEvent::NAME => 'changePayment',
            ChangePaymentToDirectValidEvent::NAME => 'changePaymentToDirectValid',
            CreditCardExpiredEvent::NAME => 'creditCardExpired',
            NoPaymentMethodEvent::NAME => 'noPaymentMethod',
        );
    }

    /**
     * @param ChangePaymentTypeEvent $event
     *
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \UnexpectedValueException
     */
    public function changePayment(ChangePaymentTypeEvent $event): void
    {
        $account = $event->getAccount();

        if (!$this->redis->exists(AdherentManager::LOCK_MULTIPLE_PATCH . $account->getUsername())) {
            $payment = $account->getPaymentTypeSelected() ? $account->getPaymentTypeSelected()->get() : null;

            /** @var AccountRepository $accountRepoMicrosoft */
            $accountRepoMicrosoft = $this->dynamicsConnection->getRepository(AccountMicrosoft::class);
            if ($accountMicrosoft = $accountRepoMicrosoft->findByDoctrineAccount($account)) {
                $accountMicrosoft->setPaymentMethod($payment);
                $this->dynamicsConnection->persist($accountMicrosoft)->flush();
            }
        }
    }

    /**
     * @param IncidentPaymentEvent $event
     *
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     */
    public function incidentPayment(IncidentPaymentEvent $event): void
    {
        $account = $event->getAccount();
        $notification = $this->alertManager->findOrCreate($account, self::ALERT_NAME);
        $notification->setMessageDomain('todotoday')
            ->setStatus(AlertStatusEnum::getNewInstance(AlertStatusEnum::ERROR))
            ->setMessage('account.alert_' . PaymentStatusEnum::PAYMENT_INCIDENT)
            ->setUrl($this->router->generate('account.profile_payment'))
            ->setLinkLabel('account.alert.update_payment_information')
            ->setTargetUrl(null);
        $this->manager->persist($notification);
        $account->setPaymentTypeSelected(PaymentTypeEnum::getNewInstance(PaymentTypeEnum::__DEFAULT))
            ->setPaymentMethodStatus(PaymentStatusEnum::getNewInstance(PaymentStatusEnum::PAYMENT_INCIDENT));

        $this->manager->flush();
    }

    /**
     * @param SepaWaitingForSignatureEvent $event
     *
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     */
    public function waitingForSignature(SepaWaitingForSignatureEvent $event): void
    {
        /** @var Account|Adherent $account */
        $account = $event->getAccount();
        $urlSignee = '';
        $messageToSign = '';
//        if ($acronisId = $account->getAcronisId()) {
//            $messageToSign = 'account.alert_sepa_waiting_for_signature_url';
//            $urlSignee = $this->baseUrlAcronis . $acronisId;
//        }
        $notification =
            $this->alertManager->findOrCreate($account, self::ALERT_NAME);
        $notification->setMessageDomain('todotoday')
            ->setStatus(AlertStatusEnum::getNewInstance(AlertStatusEnum::WARNING))
            ->setMessage('account.alert_' . PaymentStatusEnum::SEPA_WAITING_FOR_SIGNATURE)
            ->setUrl($urlSignee)
            ->setLinkLabel($messageToSign)
            ->setTargetUrl('_blank');

        $this->manager->persist($notification);

        $account->setPaymentMethodStatus(
            PaymentStatusEnum::getNewInstance(PaymentStatusEnum::SEPA_WAITING_FOR_SIGNATURE)
        );

        $this->manager->flush();
    }

    /**
     * @param ChangePaymentToDirectValidEvent $event
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     */
    public function changePaymentToDirectValid(ChangePaymentToDirectValidEvent $event): void
    {
        $account = $event->getAccount();
        $account->setPaymentMethodStatus(PaymentStatusEnum::getNewInstance(PaymentStatusEnum::VALID));
        $this->deleteAlert($account);
        $this->manager->flush();
    }

    /**
     * @param ValidPaymentMethodEvent $event
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function validPaymentMethod(ValidPaymentMethodEvent $event): void
    {
        $this->deleteAlert($event->getAccount());
    }

    /**
     * @param CreditCardExpiredEvent $event
     *
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     */
    public function creditCardExpired(CreditCardExpiredEvent $event): void
    {
        $account = $event->getAccount();
        $notification =
            $this->alertManager->findOrCreate($account, self::ALERT_NAME);
        $notification->setMessageDomain('todotoday')
            ->setStatus(AlertStatusEnum::getNewInstance(AlertStatusEnum::ERROR))
            ->setMessage('account.alert_' . PaymentStatusEnum::CREDIT_CARD_EXPIRED)
            ->setUrl($this->router->generate('account.profile_payment'))
            ->setLinkLabel('account.alert.update_payment_information')
            ->setTargetUrl(null);

        $this->manager->persist($notification);

        $account->setPaymentMethodStatus(
            PaymentStatusEnum::getNewInstance(PaymentStatusEnum::CREDIT_CARD_EXPIRED)
        );

        $this->manager->flush();
    }

    /**
     * @param NoPaymentMethodEvent $event
     *
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     */
    public function noPaymentMethod(NoPaymentMethodEvent $event): void
    {
        $account = $event->getAccount();
        $account->setPaymentMethodStatus(PaymentStatusEnum::getNewInstance(PaymentStatusEnum::NO_PAYMENT_METHOD))
            ->setPaymentTypeSelected(null);
        $notification = $this->alertManager->findOrCreate($account, self::ALERT_NAME);
        $notification->setMessageDomain('todotoday')
            ->setStatus(AlertStatusEnum::getNewInstance(AlertStatusEnum::WARNING))
            ->setMessage('account.alert_' . PaymentStatusEnum::NO_PAYMENT_METHOD)
            ->setUrl($this->router->generate('account.profile_payment'))
            ->setLinkLabel('account.alert.update_payment_information')
            ->setTargetUrl(null);

        $this->manager->persist($notification);
        $this->manager->flush();
    }

    /**
     * @param Account $account
     *
     * @throws \Doctrine\ORM\ORMException
     */
    private function deleteAlert(Account $account): void
    {
        $this->alertManager->delete($account, self::ALERT_NAME);
    }
}
