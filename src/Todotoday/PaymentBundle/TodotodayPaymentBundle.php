<?php declare(strict_types = 1);

namespace Todotoday\PaymentBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class TodotodayPaymentBundle extends Bundle
{
}
