<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 13/01/2017
 * Time: 19:17
 */

namespace Todotoday\PaymentBundle\Services;

use Actiane\ApiConnectorBundle\Traits\LoggedCurlInfoTrait;
use Exception;
use Psr\Log\LoggerInterface;

/**
 * Class ESignatureManager
 *
 * @package Todotoday\PaymentBundle\Services
 */
class ESignatureManager
{
    use LoggedCurlInfoTrait;

    /**
     * @var string
     */
    private $baseUrl;

    /**
     * @var string
     */
    private $emailDepot;

    /**
     * @var array
     */
    private $headers;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var string
     */
    private $key = 'Acronis';

    /**
     * @var string
     */
    private $loggerRef;

    /**
     * @var string
     */
    private $lastHttpMethodUsed = 'GET';

    /**
     * ESignatureManager constructor.
     *
     * @param LoggerInterface $logger
     * @param string          $token
     * @param string          $baseUrl
     * @param string          $emailDepot
     */
    public function __construct(LoggerInterface $logger, string $token, string $baseUrl, string $emailDepot)
    {
        $this->baseUrl = $baseUrl;
        $this->emailDepot = $emailDepot;
        $this->headers = array('Content-Type: application/json', 'Authorization: Bearer ' . $token);
        $this->logger = $logger;
    }

    /**
     * @param string $fileUrl
     * @param string $fileName
     * @param string $fileHash
     *
     * @return array|null
     */
    public function createAcronisDocIdWithFileUrl(string $fileUrl, string $fileName, string $fileHash): ?array
    {
        $errorMessage = 'Veuillez réessayer afin de recevoir le docId du fichier à signer.';

        try {
            $url = $this->baseUrl . '/esign/docs';

            $head = array_change_key_case(get_headers($fileUrl, 1));
            $fileSize = (int) $head['content-length'];
//            $fileHash = hash_file('sha256', $fileUrl);

            $datas = json_encode(
                array(
                    'file' => array(
                        'link' => $fileUrl,
                        'name' => $fileName,
                        'size' => $fileSize,
                        'hash' => $fileHash,
                    ),
                    'signee' => array(
                        'email' => $this->emailDepot,
                    ),
                    'exclude_me' => true,
                )
            );

            $response = $this->sendCurl($url, $datas, true);
            $docID = $response['doc'];

            return array(
                'success' => true,
                'docId' => $docID,
            );
        } catch (Exception $e) {
            $message = $e->getMessage() . ' ' . $errorMessage;

            return array(
                'success' => false,
                'message' => $message,
            );
        }
    }

    /**
     * @param string $string
     *
     * @return bool
     */
    public function isJson(string $string): bool
    {
        json_decode($string);

        return (json_last_error() === JSON_ERROR_NONE);
    }

    /**
     * @param string $url
     * @param null   $datas
     * @param bool   $isPost
     *
     * @return mixed
     * @throws Exception
     */
    public function sendCurl(string $url, $datas = null, bool $isPost = false)
    {
        $this->loggerRef = sprintf('(%s) (%s)', uniqid(), $this->key);
        $curl = curl_init($url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $datas);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $this->headers);
        if ($isPost) {
            $this->lastHttpMethodUsed = 'POST';
            curl_setopt($curl, CURLOPT_POST, true);
        }

        $curlResponse = curl_exec($curl);
        $info = curl_getinfo($curl);
        if ($curlResponse === false) {
            curl_close($curl);
            throw new Exception('Error occured during curl exec. Additional info: ' . var_export($info, true));
        }

        $this->loggedCurlInfo($info, $curlResponse);

        curl_close($curl);

        if (!$this->isJson($curlResponse)) {
            throw new Exception('The response is not a valid JSON.');
        }

        $response = json_decode($curlResponse, true);
        if (isset($response->response->status) && $response->response->status == 'ERROR') {
            throw new Exception('Error occured: ' . $response->response->errormessage);
        }

        return $response;
    }

    /**
     * @param string      $docId
     * @param string      $email
     *
     * @param null|string $lang
     *
     * @return array|null
     */
    public function sendEmailToESignWithDocId(string $docId, string $email, ?string $lang = 'fr'): ?array
    {
        $errorMessage = 'Veuillez réessayer afin d\'envoyer le fichier à signer par email.';

        $url = $this->baseUrl . '/esign/docs/' . $docId . '/invite';
        $datas = json_encode(
            array(
                'signees' => array(
                    array('email' => $email),
                ),
                'lang' => $lang,
            )
        );

        try {
            $response = $this->sendCurl($url, $datas, true);

            if ($response['status'] !== 'ok') {
                throw new Exception('Une erreur s\'est produite.');
            }

            return array(
                'success' => true,
            );
        } catch (Exception $e) {
            $message = $e->getMessage() . ' ' . $errorMessage;

            return array(
                'success' => false,
                'message' => $message,
            );
        }
    }
}
