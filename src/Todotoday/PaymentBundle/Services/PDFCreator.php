<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 13/01/2017
 * Time: 19:17
 */

namespace Todotoday\PaymentBundle\Services;

use Cocur\Slugify\Slugify;
use Knp\Bundle\SnappyBundle\Snappy\LoggableGenerator;
use Knp\Snappy\Pdf;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Entity\Api\Microsoft\CustomerBankAccount;

/**
 * Class PDFCreator
 *
 * @package Todotoday\PaymentBundle\Services
 */
class PDFCreator
{
    /**
     * @var TwigEngine
     */
    private $twigEngine;

    /**
     * @var Pdf
     */
    private $loggableGenerator;

    /**
     * @var Slugify
     */
    private $slugify;

    /**
     * @var BankAccountManager
     */
    private $bankAccountManager;

    /**
     * PDFCreator constructor.
     *
     * @param TwigEngine         $twigEngine
     * @param Pdf                $loggableGenerator
     * @param Slugify            $slugify
     * @param BankAccountManager $bankAccountManager
     */
    public function __construct(
        TwigEngine $twigEngine,
        Pdf $loggableGenerator,
        Slugify $slugify,
        BankAccountManager $bankAccountManager
    ) {
        $this->twigEngine = $twigEngine;
        $this->loggableGenerator = $loggableGenerator;
        $this->slugify = $slugify;
        $this->bankAccountManager = $bankAccountManager;
    }

    /**
     * @param string              $agencyName
     * @param Adherent            $adherent
     * @param CustomerBankAccount $customerBankAccount
     *
     * @return string
     * @throws \Twig_Error
     * @throws \RuntimeException
     */
    public function createSepaBinaryFile(
        string $agencyName,
        Adherent $adherent,
        CustomerBankAccount $customerBankAccount
    ): string {
        return $this->loggableGenerator->getOutputFromHtml(
            $this->getTwigSepaTemplate(
                $agencyName,
                $adherent,
                $customerBankAccount
            ),
            $this->getSepaFooter()
        );
    }

    /**
     * @param string              $agencyName
     * @param Adherent            $adherent
     * @param CustomerBankAccount $customerBankAccount
     *
     * @return string
     * @throws \Twig_Error
     * @throws \RuntimeException
     */
    public function createSepaPdf(
        string $agencyName,
        Adherent $adherent,
        CustomerBankAccount $customerBankAccount
    ): string {
        do {
            $fileLinkSlugified = sys_get_temp_dir() . '/' . $this->getSepaFileName($agencyName, $adherent);
        } while (file_exists($fileLinkSlugified));
        $this->loggableGenerator->generateFromHtml(
            $this->getTwigSepaTemplate(
                $agencyName,
                $adherent,
                $customerBankAccount
            ),
            $fileLinkSlugified,
            $this->getSepaFooter()
        );

        return $fileLinkSlugified;
    }

    /**
     * @param string   $agency
     * @param Adherent $adherent
     *
     * @return string
     *
     */
    public function getSepaFileName(string $agency, Adherent $adherent): string
    {
        $timestamp = (new \DateTime())->getTimestamp();
        $agencyLower = strtolower($agency);
        $fileName = 'sepa-' . $agencyLower . '-' . $adherent->getLastName() . '-' . $adherent->getFirstName() . '-'
            . $timestamp;

        return $this->slugify->slugify($fileName) . '.pdf';
    }

    /**
     * @return array
     * @throws \Twig_Error
     * @throws \RuntimeException
     */
    public function getSepaFooter(): array
    {
        return array(
            'footer-html' => $this->twigEngine->render('TodotodayPaymentBundle:Pdf/footer:sepa_footer.html.twig'),
        );
    }

    /**
     * @param string              $agencyName
     * @param Adherent            $adherent
     * @param CustomerBankAccount $customerBankAccount
     *
     * @return string
     * @throws \Twig_Error
     * @throws \RuntimeException
     */
    public function getTwigSepaTemplate(
        string $agencyName,
        Adherent $adherent,
        CustomerBankAccount $customerBankAccount
    ): string {
        $address = '';
        if ($adherent->getContactPrincipal()) {
            $address =
                $adherent->getContactPrincipal()->getPrimaryAddressStreet() . ' ' . $adherent->getContactPrincipal()
                    ->getPrimaryAddressZipCode() . ' ' . $adherent->getContactPrincipal()->getPrimaryAddressCity();
        }

        return $this->twigEngine->render(
            'TodotodayPaymentBundle:Pdf:sepa.html.twig',
            array(
                'agency' => ucwords($agencyName),
                'user' => array(
                    'rum' => $adherent->getCustomerAccount(),
                    'lastname' => $adherent->getLastName(),
                    'surname' => $adherent->getFirstName(),
                    'address' => $address,
                ),
                'customerBankAccount' => array(
                    'iban' => $customerBankAccount->getIban(),
                    'swiftCode' => $customerBankAccount->getSwiftCode(),
                    'countryCode' => $this->bankAccountManager->getCountryCode($customerBankAccount),
                    'keyCode' => $this->bankAccountManager->getKeyCode($customerBankAccount),
                ),
            )
        );
    }
}
