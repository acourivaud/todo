<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 13/01/2017
 * Time: 19:17
 */

namespace Todotoday\PaymentBundle\Services;

use Actiane\ApiConnectorBundle\Services\Connections\Microsoft\MicrosoftDynamicsConnection;
use Actiane\ApiConnectorBundle\Traits\LoggedCurlInfoTrait;
use Exception;
use Psr\Log\LoggerInterface;
use stdClass;
use Todotoday\PaymentBundle\Exception\OneDriveException;

/**
 * Class OneDriveManager
 *
 * @package Todotoday\PaymentBundle\Services
 */
class OneDriveManager
{
    use LoggedCurlInfoTrait;
    /**
     * @var string
     */
    public const BASE_PATH = '/v1.0/me/drive/root';

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var MicrosoftDynamicsConnection
     */
    private $microsoftDynamicsConnection;

    /**
     * @var string
     */
    private $loggerRef;

    /**
     * @var string
     */
    private $lastHttpMethodUsed = 'GET';

    /**
     * @var string
     */
    private $key = 'OneDrive';

    /**
     * @var string
     */
    private $baseFolder;

    /**
     * OneDriveManager constructor.
     *
     * @param MicrosoftDynamicsConnection $microsoftDynamicsConnection
     * @param LoggerInterface             $logger
     * @param String                      $env
     */
    public function __construct(
        MicrosoftDynamicsConnection $microsoftDynamicsConnection,
        LoggerInterface $logger,
        String $env
    ) {
        $this->microsoftDynamicsConnection = $microsoftDynamicsConnection;
        $this->logger = $logger;
        $this->baseFolder = 'prod' === $env ? 'sepa' : 'test';
    }

    /**
     * @param string $path
     * @param string $binaries
     * @param string $contentType
     *
     * @return array
     * @throws Exception
     */
    public function uploadFileWithBinaries(string $path, string $binaries, $contentType = 'application/json'): array
    {
        $response = array(
            'success' => false,
            'filePath' => $path,
        );

        $url = $this->getPath($path) . '/content';
        $curl = $this->sendPutCurl($url, $binaries, $contentType);
        $curlResponse = $curl['response'];
        $httpCode = $curl['http_code'];

        $response['success'] = !isset($curlResponse['error']) && (intdiv($httpCode, 100) === 2);

        return $response;
    }

    /**
     * @param string $agencyName
     * @param string $fileName
     * @param string $binaries
     *
     * @return array
     * @throws Exception
     */
    public function uploadFileWithBinariesForSepa(string $agencyName, string $fileName, string $binaries): array
    {
        $path = sprintf(
            '/%s/%s/%s',
            $this->baseFolder,
            strtolower($agencyName),
            $fileName
        );

        return $this->uploadFileWithBinaries($path, $binaries, 'application/pdf');
    }

    /**
     * @param string $folderName
     *
     * @return bool
     * @throws Exception
     */
    public function createFolderInSepaFolder(string $folderName): bool
    {
        return $this->verifyAndCreateFolder($folderName, $this->baseFolder);
    }

    /**
     * @param string $folderName
     * @param string $basePath
     *
     * @return bool
     * @throws Exception
     */
    public function verifyAndCreateFolder(string $folderName, string $basePath = ''): bool
    {
        if (!$this->isItemExist($basePath . '/' . $folderName)) {
            return $this->createFolder($folderName, $basePath);
        }

        return false;
    }

    /**
     * @param string $folderName
     *
     * @param string $basePath
     *
     * @return bool
     * @throws Exception
     */
    public function createFolder(string $folderName, string $basePath = ''): bool
    {
        $url = $this->getPath($basePath) . '/children';
        $datas = array(
            'name' => strtolower($folderName),
            'folder' => new stdClass(),
        );
        $curl = $this->sendPostCurl($url, $datas);
        $response = $curl['response'];
        $httpCode = $curl['http_code'];

        return !isset($response['error']) && (intdiv($httpCode, 100) === 2);
    }

    /**
     * @param string $filePath
     *
     * @return array
     * @throws Exception
     */
    public function createSharingLink(string $filePath): array
    {
        $response = array(
            'success' => false,
            'sharingLink' => '',
        );

        $url = $this->getPath($filePath) . '/createLink';
        $datas = array(
            'type' => 'view',
            'scope' => 'anonymous',
        );
        $curl = $this->sendPostCurl($url, $datas);
        $curlResponse = $curl['response'];
        $httpCode = $curl['http_code'];

        if (!isset($curlResponse['error']) && (intdiv($httpCode, 100) === 2)) {
            $response['success'] = true;
            $response['sharingLink'] = $curlResponse['link']['webUrl'];
        }

        return $response;
    }

    /**
     * @param string $path
     *
     * @return bool
     * @throws Exception
     */
    public function isItemExist(string $path): bool
    {
        $url = $this->getPath(strtolower($path));
        $curl = $this->sendCurl($url);

        $response = $curl['response'];
        $httpCode = $curl['http_code'];

        return !isset($response['error']) && (intdiv($httpCode, 100) === 2);
    }

    /**
     * @param string $path
     *
     * @return array
     * @throws Exception
     */
    public function deleteItem(string $path): array
    {
        $url = $this->getPath($path);
        $curl = $this->sendCurl($url, 'DELETE');

        $httpCode = $curl['http_code'];

        $curl['success'] = !isset($curlResponse['error']) && (intdiv($httpCode, 100) === 2);

        return $curl;
    }

    /**
     * @param string       $relativeUrl
     * @param string       $httpMethod
     * @param string|array $postfields
     * @param string       $contentType
     *
     * @return array
     * @throws OneDriveException
     */
    public function sendCurl(
        string $relativeUrl,
        string $httpMethod = 'GET',
        $postfields = '',
        string $contentType = 'application/json'
    ): array {

        $customRequest = ['PUT', 'DELETE'];

        $url = $this->microsoftDynamicsConnection->getBaseUrl() . $relativeUrl;
        $accessToken = $this->microsoftDynamicsConnection->connect()->getAccessToken();
        $this->loggerRef = sprintf('(%s) (%s)', uniqid(), $this->key);
        $curl = curl_init($url);

        curl_setopt($curl, CURLOPT_TIMEOUT, 45);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        if (($httpMethod === 'POST') || ($httpMethod === 'PUT')) {
            if ($contentType === 'application/json') {
                $postfields = json_encode($postfields);
            }
            curl_setopt($curl, CURLOPT_POSTFIELDS, $postfields);
        }
        if (\in_array($httpMethod, $customRequest, true)) {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $httpMethod);
        }
        curl_setopt(
            $curl,
            CURLOPT_HTTPHEADER,
            array(
                'Authorization: Bearer ' . $accessToken,
                'Content-Type: ' . $contentType,
            )
        );

        $curlResponse = curl_exec($curl);
        $info = curl_getinfo($curl);

        if ($curlResponse === false) {
            curl_close($curl);
            throw new OneDriveException('Error occured during curl exec. Additional info: ' . var_export($info, true));
        }

        $this->loggedCurlInfo($info, $curlResponse);

        curl_close($curl);

        $response = json_decode($curlResponse, true);
        if (isset($response->response->status) && $response->response->status == 'ERROR') {
            throw new OneDriveException('Error occured: ' . $response->response->errormessage);
        }

        return array(
            'response' => $response,
            'http_code' => $info['http_code'],
        );
    }

    /**
     * @param string         $relativeUrl
     * @param string | array $datas
     * @param string         $contentType
     *
     * @return array
     * @throws Exception
     */
    public function sendPostCurl(string $relativeUrl, $datas, $contentType = 'application/json'): array
    {
        $this->lastHttpMethodUsed = 'POST';

        return $this->sendCurl($relativeUrl, 'POST', $datas, $contentType);
    }

    /**
     * @param string $relativeUrl
     * @param string $datas
     * @param string $contentType
     *
     * @return array
     * @throws Exception
     */
    public function sendPutCurl(
        string $relativeUrl,
        string $datas = '',
        string $contentType = 'application/json'
    ): array {
        $this->lastHttpMethodUsed = 'PUT';

        return $this->sendCurl($relativeUrl, 'PUT', $datas, $contentType);
    }

    /**
     * @param string $filePath
     *
     * @return string
     */
    private function getPath(string $filePath = ''): string
    {
        if ('' !== $filePath && '/' !== $filePath[0]) {
            $filePath = '/' . $filePath;
        }
        $filePath = '' === $filePath ? '' : ':' . $filePath . ':';

        return self::BASE_PATH . $filePath;
    }
}
