<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 13/01/2017
 * Time: 19:17
 */

namespace Todotoday\PaymentBundle\Services;

use Actiane\ApiConnectorBundle\Services\Connections\Microsoft\MicrosoftDynamicsConnection;
use Todotoday\AccountBundle\Entity\Api\Microsoft\CustomerBankAccount;
use Todotoday\AccountBundle\Entity\Api\Microsoft\DirectDebitMandate;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class BankAccountManager
 *
 * @package Todotoday\PaymentBundle\Services
 */
class BankAccountManager
{
    /**
     * @var MicrosoftDynamicsConnection
     */
    private $apiConnection;

    /**
     * BankAccountManager constructor.
     *
     * @param MicrosoftDynamicsConnection $apiConnection
     */
    public function __construct(MicrosoftDynamicsConnection $apiConnection)
    {
        $this->apiConnection = $apiConnection;
    }

    /**
     * @param CustomerBankAccount $customerBankAccount
     *
     * @return CustomerBankAccount
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \InvalidArgumentException
     */
    public function createCustomerBankAccount(CustomerBankAccount $customerBankAccount): CustomerBankAccount
    {
        $this->apiConnection->persist($customerBankAccount);
        $this->apiConnection->flush();

        return $customerBankAccount;
    }

    /**
     * @param CustomerBankAccount $customerBankAccount
     * @param string              $docId
     * @param Agency              $agency
     *
     * @return DirectDebitMandate
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \InvalidArgumentException
     */
    public function createDirectDebitMandate(
        CustomerBankAccount $customerBankAccount,
        string $docId,
        Agency $agency
    ): DirectDebitMandate {
        $directDebitMandate = new DirectDebitMandate();
        $directDebitMandate->setIdAcronis($docId);
        $this->apiConnection->persist($directDebitMandate);

        $directDebitMandate
            ->setCustomerBankAccount($customerBankAccount->getBankAccountID())
            ->setCustomerAccount($customerBankAccount->getCustomerAccountNumber())
            ->setDataAreaId($agency->getDataAreaId());
        //todo: supprimer cette ligne dès que possible !
        $creditorBankAccount = ($agency->getDataAreaId() === 'ERA') ? 'CE1' : 'CIC';
        $directDebitMandate->setCreditorBankAccount($creditorBankAccount);

        $this->apiConnection->flush();

        return $directDebitMandate;
    }

    /**
     * @param CustomerBankAccount $customerBankAccount
     *
     * @return string
     */
    public function getKeyCode(CustomerBankAccount $customerBankAccount): string
    {
        return $this->getSubstringOfIban($customerBankAccount, 2, 2);
    }

    /**
     * @param CustomerBankAccount $customerBankAccount
     *
     * @return string
     */
    public function getCountryCode(CustomerBankAccount $customerBankAccount): string
    {
        return $this->getSubstringOfIban($customerBankAccount, 0, 2);
    }

    /**
     * @param CustomerBankAccount $customerBankAccount
     * @param int                 $firstCharacter
     * @param int                 $numberOfCharacters
     *
     * @return string
     */
    private function getSubstringOfIban(
        CustomerBankAccount $customerBankAccount,
        int $firstCharacter,
        int $numberOfCharacters
    ): string {
        $iban = $customerBankAccount->getIban();

        if ($iban && (strlen($iban) >= ($firstCharacter + $numberOfCharacters))) {
            return strtoupper(substr($iban, $firstCharacter, $numberOfCharacters));
        }

        return '';
    }
}
