<?php declare(strict_types=1);

/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 12/04/2017
 * Time: 19:32
 */

namespace Todotoday\PaymentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Todotoday\AccountBundle\Entity\Account;

/**
 * Class CustomerStripeAccount
 *
 * @ORM\Entity()
 * @ORM\Table()
 * @category   Todo-Todev
 * @package    Todotoday\PaymentBundle
 * @subpackage Todotoday\PaymentBundle\Entity
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class CustomerStripeAccount
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="stripe_customer_id", type="string", nullable=true)
     * TODO: To implement to Microsoft API
     */
    protected $stripeCustomerId;

    /**
     * @var string
     *
     * @ORM\Column(name="stripe_card_id", type="string", nullable=true)
     * TODO: To implement to Microsoft API
     */
    protected $stripeCardId;

    /**
     * @var string
     *
     * @ORM\Column(name="last_stripe_card_token", type="string", nullable=true)
     * TODO: To implement to Microsoft API
     */
    protected $lastStripeCardToken;

    /**
     * @var string
     *
     * TODO: To implement to Microsoft API
     */
    protected $newStripeCardToken;

    /**
     * @var Account
     *
     * @ORM\OneToOne(targetEntity="Todotoday\AccountBundle\Entity\Account", inversedBy="customerStripeAccount")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $account;

    /**
     * Get account
     *
     * @return Account
     */
    public function getAccount(): ?Account
    {
        return $this->account;
    }

    /**
     * Set account
     *
     * @param Account $account
     *
     * @return CustomerStripeAccount
     */
    public function setAccount(?Account $account): self
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get id
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Get lastStripeCardToken
     *
     * @return string
     */
    public function getLastStripeCardToken(): ?string
    {
        return $this->lastStripeCardToken;
    }

    /**
     * Set lastStripeCardToken
     *
     * @param string $lastStripeCardToken
     *
     * @return CustomerStripeAccount
     */
    public function setLastStripeCardToken(?string $lastStripeCardToken): self
    {
        $this->lastStripeCardToken = $lastStripeCardToken;

        return $this;
    }

    /**
     * Get newStripeCardToken
     *
     * @return string
     */
    public function getNewStripeCardToken(): ?string
    {
        return $this->newStripeCardToken;
    }

    /**
     * Set newStripeCardToken
     *
     * @param string $newStripeCardToken
     *
     * @return CustomerStripeAccount
     */
    public function setNewStripeCardToken(?string $newStripeCardToken): self
    {
        $this->newStripeCardToken = $newStripeCardToken;

        return $this;
    }

    /**
     * Get stripeCardId
     *
     * @return string
     */
    public function getStripeCardId(): ?string
    {
        return $this->stripeCardId;
    }

    /**
     * Set stripeCardId
     *
     * @param string $stripeCardId
     *
     * @return CustomerStripeAccount
     */
    public function setStripeCardId(?string $stripeCardId): self
    {
        $this->stripeCardId = $stripeCardId;

        return $this;
    }

    /**
     * Get stripeCustomerId
     *
     * @return string
     */
    public function getStripeCustomerId(): ?string
    {
        return $this->stripeCustomerId;
    }

    /**
     * Set stripeCustomerId
     *
     * @param string $stripeCustomerId
     *
     * @return CustomerStripeAccount
     */
    public function setStripeCustomerId(?string $stripeCustomerId): self
    {
        $this->stripeCustomerId = $stripeCustomerId;

        return $this;
    }
}
