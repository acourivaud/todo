<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 23/03/2017
 * Time: 09:12
 */

namespace Todotoday\PaymentBundle\Form;

use Actiane\ToolsBundle\Services\AssetManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\Exception\AccessException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Bic;
use Symfony\Component\Validator\Constraints\Iban;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Todotoday\AccountBundle\Entity\Api\Microsoft\CustomerBankAccount;

/**
 * Class AccountSEPAInfoType
 *
 * @package    Todotoday\PaymentBundle
 * @subpackage Todotoday\PaymentBundle\Form
 */
class AccountSEPAInfoType extends AbstractType
{
    /**
     * @var string
     */
    private $env;

    /**
     * AccountSEPAInfoType constructor.
     *
     * @param AssetManager $assetManager
     * @param string       $env
     */
    public function __construct(AssetManager $assetManager, string $env)
    {
        $this->env = $env;

        $assetManager->addJavascript('build/accountSEPAInfoType.js');
    }

    /**
     * {@inheritdoc}
     * @throws \Symfony\Component\Validator\Exception\ConstraintDefinitionException
     * @throws \Symfony\Component\Validator\Exception\InvalidOptionsException
     * @throws \Symfony\Component\Validator\Exception\MissingOptionsException
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($this->env === 'dev') {
            $iban = 'FR1420041010050500013M02606';
            $swift = 'OKOYFIHH';
        } else {
            $iban = null;
            $swift = null;
        }
        $builder
            ->add(
                'bankName',
                TextType::class,
                array(
                    'label' => 'account.bank_name',
                    'translation_domain' => 'todotoday',
                    'constraints' => new NotNull(
                        array(
                            'groups' => 'sepa',
                        )
                    ),
                )
            )
//            ->add(
//                'bankAccountNumber',
//                TextType::class,
//                array(
//                    'label' => 'account.bank_account_number',
//                    'translation_domain' => 'todotoday',
//                )
//            )
            ->add(
                'iban',
                TextType::class,
                array(
                    'data' => $iban,
                    'label' => 'iban',
                    'translation_domain' => 'todotoday',
                    'constraints' => new Iban(array('groups' => 'sepa')),
                )
            )
            ->add(
                'swiftCode',
                TextType::class,
                array(
                    'data' => $swift,
                    'label' => 'account.swift_code',
                    'translation_domain' => 'todotoday',
                    'constraints' => new Bic(array('groups' => 'sepa')),
                )
            );
    }

    /**
     * Do buildView
     *
     * @param FormView      $view
     * @param FormInterface $form
     * @param array         $options
     *
     * @return void
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['as_component'] = $options['as_component'];
    }

    /**
     * {@inheritdoc}
     * @throws AccessException
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => CustomerBankAccount::class,
                'as_component' => false,
                'label' => 'account.bank_account',
                'translation_domain' => 'todotoday',
            )
        );
    }
}
