<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 13/12/17
 * Time: 11:44
 *
 * @category   Todo-Todev
 *
 * @package    Todotoday\PaymentBundle\Exception
 *
 * @subpackage Todotoday\PaymentBundle\Exception
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Todotoday\PaymentBundle\Exception;

use Throwable;

/**
 * Class OneDriveException
 */
class OneDriveException extends \Exception
{
    /**
     * OneDriveException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message = '', int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
