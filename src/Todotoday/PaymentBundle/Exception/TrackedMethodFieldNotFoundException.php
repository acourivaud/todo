<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 18/06/17
 * Time: 12:25
 */

namespace Todotoday\PaymentBundle\Exception;

use Throwable;

/**
 * Class TrackedMethodFieldNotFoundException
 * @package Todotoday\PaymentBundle\Exception
 */
class TrackedMethodFieldNotFoundException extends \Exception
{
    /**
     * TrackedMethodFieldNotFoundException constructor.
     *
     * @param string         $field
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct(
        string $field,
        $message = 'Tracked method for field %s does not exist',
        $code = 0,
        Throwable $previous = null
    ) {
        $message = sprintf($message, $field);
        parent::__construct($message, $code, $previous);
    }
}
