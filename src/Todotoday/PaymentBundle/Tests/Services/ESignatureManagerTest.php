<?php declare(strict_types=1);

namespace Todotoday\PaymentBundle\Tests\Services;

use Liip\FunctionalTestBundle\Test\WebTestCase;

/**
 * Class ESignatureManagerTest
 *
 * @package Todotoday\PaymentBundle\Tests\Services
 */
class ESignatureManagerTest extends WebTestCase
{
    /**
     * @small
     */
    public function testSendEmailAcronis()
    {
        $esignManager = $this->getContainer()->get('todotoday.payment.esign_manager');
        $docId = $this->testCreateAcronisFileLink();
        $sendEmailResponse = $esignManager->sendEmailToESignWithDocId($docId, 'technique-tdtd@actiane.com');
        $this->assertTrue($sendEmailResponse['success']);
    }

    /**
     * @small
     * @return string
     */
    public function testCreateAcronisFileLink(): string
    {
        $fileName = 'pdf-test.pdf';
        $fileUrl = 'http://www.orimi.com/' . $fileName;
        $hashFile = hash_file('sha256', __DIR__ . '/Resources/' . $fileName);
        $esignManager = $this->getContainer()->get('todotoday.payment.esign_manager');
        $docIdResponse = $esignManager->createAcronisDocIdWithFileUrl($fileUrl, $fileName, $hashFile);
        $this->assertTrue($docIdResponse['success']);
        $this->assertNotNull($docIdResponse['docId']);

        return $docIdResponse['docId'];
    }
}
