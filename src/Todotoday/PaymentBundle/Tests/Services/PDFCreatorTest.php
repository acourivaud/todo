<?php declare(strict_types=1);

namespace Todotoday\PaymentBundle\Tests\Services;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAccountData;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Entity\Api\Microsoft\CustomerBankAccount;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyData;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class PDFCreatorTest
 * @package Todotoday\PaymentBundle\Tests\Services
 */
class PDFCreatorTest extends WebTestCase
{
    /**
     * @group        ignore
     * @small
     */
    public function testcreateSepaPdf(): void
    {
        // TODO test à corriger puis enlever le group ignore
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent0');
        /** @var Agency $agency */
        $agency = self::getFRR()->getReference('agency_demo0');

        $pdfCreatorManager = $this->getContainer()->get('todotoday.payment.pdf_creator');

        //to get the context and get image from absolute url
        $context = $this->getContainer()->get('router')->getContext();
        $context->setHost('www.todotoday.local');

        $customerBankAccount = new CustomerBankAccount();

        $file = $pdfCreatorManager->createSepaPdf($agency->getName(), $adherent, $customerBankAccount);
        $this->assertNotNull($file);
        $fs = $this->getContainer()->get('filesystem');
        $this->assertTrue($fs->exists($file));
        if ($fs->exists($file)) {
            $fs->remove($file);
        }
    }

    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAccountData::class,
            LoadAgencyData::class,
        );
    }
}
