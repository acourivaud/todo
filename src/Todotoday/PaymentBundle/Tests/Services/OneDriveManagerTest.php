<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 18/04/17
 * Time: 21:29
 */

namespace Todotoday\PaymentBundle\Tests\Services;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Todotoday\PaymentBundle\Services\OneDriveManager;

/**
 * Class OneDriveManagerTest
 *
 * @package Todotoday\PaymentBundle\Tests\Services
 */
class OneDriveManagerTest extends WebTestCase
{
    /**
     * @var OneDriveManager
     */
    private $manager;

    /**
     * @small
     * @throws \Exception
     */
    public function testIsFolderExist(): void
    {
        $this->assertTrue($this->manager->isItemExist('test'));
    }

    /**
     * @small
     *
     * @throws \Exception
     */
    public function testUPloadFileWithBinary(): void
    {
        $result = $this->manager->uploadFileWithBinaries('/test/test.pdf', 'test');
        $this->assertTrue($result['success']);
        $this->assertEquals('/test/test.pdf', $result['filePath']);
    }

    /**
     * @small
     *
     * @throws \Exception
     */
    public function testFileExist(): void
    {
        $result = $this->manager->isItemExist('test/test.pdf');
        $this->assertTrue($result);
    }

    /**
     * @throws \Exception
     */
    public function testDeleteItem(): void
    {
        $result = $this->manager->deleteItem('test/test.pdf');
        $this->assertTrue($result['success']);
    }

    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array();
    }

    /**
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->manager = $this->getContainer()->get('todotoday.payment.onedrive_manager');
        $this->manager->verifyAndCreateFolder('test');
    }
}
