<?php declare(strict_types = 1);

namespace Todotoday\PaymentBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     * @throws \RuntimeException
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('todotoday_payment');

        $rootNode
            ->children()
                ->arrayNode('acronis')
                    ->children()
                        ->scalarNode('token')->end()
                        ->scalarNode('base_url')->end()
                        ->scalarNode('email_depot')->end()
                        ->scalarNode('signee_base_url')->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
