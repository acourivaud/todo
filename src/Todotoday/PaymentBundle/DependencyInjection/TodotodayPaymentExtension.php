<?php declare(strict_types=1);

namespace Todotoday\PaymentBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class TodotodayPaymentExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');
        $loader->load('form.yml');

        $esignManager = $container->getDefinition('todotoday.payment.esign_manager');
        $esignManager->addArgument($config['acronis']['token'])
            ->addArgument($config['acronis']['base_url'])
            ->addArgument($config['acronis']['email_depot']);

        $paymentSubscriber = $container->getDefinition('todotoday.payment.subscriber');
        $paymentSubscriber->addArgument($config['acronis']['signee_base_url']);
    }
}
