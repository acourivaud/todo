<?php
/**
 * PHP version 7
 */
declare(strict_types = 1);

namespace Todotoday\MediaBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * TodotodayMediaBundle
 *
 * @author Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class TodotodayMediaBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'SonataMediaBundle';
    }
}
