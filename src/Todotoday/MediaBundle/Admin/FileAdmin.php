<?php declare(strict_types=1);

namespace Todotoday\MediaBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Doctrine\ORM\QueryBuilder;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Todotoday\CMSBundle\Entity\LinkFileAgency;
use Todotoday\CoreBundle\Admin\MultiAgencyAdminAbstract;
use Todotoday\MediaBundle\Entity\File;

class FileAdmin extends MultiAgencyAdminAbstract
{
    /**
     * Do getNewInstance
     *
     * @return File
     * @throws \RuntimeException
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getNewInstance(): File
    {
        /** @var File $file */
        $file = parent::getNewInstance();

        if (!$file->getId()
            && $file->getLinks()->isEmpty()
            && $agency = $this->domainContextManager->getCurrentContext()->getAgency()
        ) {
            $file->addLink(
                $link = (new LinkFileAgency())
                    ->setFile($file)
                    ->setVisibility(true)
                    ->setAgency($agency)
            );
        }

        if ($this->isGranted('ROLE_COMMUNITY_MANAGER')
            && $agencies = $this->getAgencyByStrategy(
                $this->getRequest()->get('agencies_strategy')
            )
        ) {
            foreach ($agencies as $agency) {
                $file->addLink(
                    $link = (new LinkFileAgency())
                        ->setFile($file)
                        ->setVisibility(true)
                        ->setAgency($agency)
                );
            }
        }

        return $file;
    }

    /**
     * {@inheritdoc}
     * @throws \UnexpectedValueException
     */
    public function createQuery($context = 'list')
    {
        /** @var ProxyQueryInterface|QueryBuilder $query */
        $query = parent::createQuery($context);

        if ($agency = $this->domainContextManager->getCurrentContext()->getAgency()) {
            $query
                ->join($query->getRootAliases()[0] . '.links', 'link_file_agency')
                ->andWhere('link_file_agency.agency = :agency')
                ->setParameter('agency', $agency);
        }

        return $query;
    }

    /**
     * @param DatagridMapper $datagridMapper
     *
     * @throws \RuntimeException
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('media', null, ['label' => 'linked_file'])
            ->add('links.visibility', null, ['label' => 'Visibility'])
            ->add('links.agency', null, ['label' => 'Agencies']);
    }

    /**
     * @param ListMapper $listMapper
     *
     * @throws \RuntimeException
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        if (($tl = $this->getRequest()->get('tl')) && $tl !== 'fr') {
            $listMapper
                ->add(
                    'media' . ucfirst($tl),
                    null,
                    ['label' => 'linked_file']
                );
        } elseif (($loc = $this->getRequest()->getLocale()) !== 'fr') {
            $listMapper
                ->add(
                    'media' . ucfirst($loc),
                    null,
                    ['label' => 'linked_file']
                );
        } else {
            $listMapper
                ->add(
                    'media',
                    null,
                    ['label' => 'linked_file']
                );
        }
        $listMapper
            ->add('title')
            ->add('description')
            ->add(
                '_action',
                null,
                array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    ),
                )
            );
    }

    /**
     * @param FormMapper $formMapper
     *
     * @throws \RuntimeException
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $field = 'media';
        if (($tl = $this->getRequest()->get('tl')) && $tl !== 'fr') {
            $field .= ucfirst($tl);
        } elseif (($loc = $this->getRequest()->getLocale()) !== 'fr') {
            $field .= ucfirst($loc);
        }
        $formMapper
            ->add(
                $field,
                ModelListType::class,
                array('btn_delete' => false),
                array('link_parameters' => array('context' => 'file'))
            )
            ->add(
                'title',
                null,
                array(
                    'required' => true,
                )
            )
            ->add(
                'description',
                null,
                array(
                    'required' => true,
                )
            );

        if ($this->isGranted('ROLE_COMMUNITY_MANAGER')) {
            $formMapper
                ->add(
                    'links',
                    CollectionType::class,
                    array(
                        'by_reference' => false,
                        'label' => 'Agencies',
                    ),
                    array(
                        'edit' => 'inline',
                        'inline' => 'table',
                        'sortable' => 'position',
                    )
                );
        };
    }

    /**
     * @param ShowMapper $showMapper
     *
     * @throws \RuntimeException
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('title')
            ->add('media')
            ->add('description');
    }
}
