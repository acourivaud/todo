<?php declare(strict_types=1);

namespace Todotoday\MediaBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CorrectMediaAclCommand extends ContainerAwareCommand
{
    /**
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function configure()
    {
        $this
            ->setName('todotoday:correct:acl:media')
            ->setDescription('Add correct VIEW and EDIT Media ACL for ROLE_COMMUNITY_MANAGER using acl:set command')
            ->addArgument(
                'context',
                InputArgument::REQUIRED,
                'Specify the media context'
            )
            ->addArgument(
                'role',
                InputArgument::OPTIONAL,
                'The role to correct'
            )
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     * @throws \LogicException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $context = $input->hasArgument('context') ? $input->getArgument('context') : false;
        $role = $input->hasArgument('role') ? $input->getArgument('role') : false;
        if ($context && $role) {
            $doctrine = $this->getContainer()->get('doctrine');
            $mediaRepo = $doctrine->getRepository('TodotodayMediaBundle:Media');
            /** @var Media[] $medias */
            $medias = $mediaRepo->findAllByContext($context);

            if (count($medias) > 0) {
                $output->writeln("The following Medias' ACL are corrected : ");
                foreach ($medias as $media) {
                    $command = 'php bin/console acl:set';
                    $params = ' --class-scope --role=' . $role;
                    $arg = ' VIEW EDIT Todotoday/MediaBundle/Entity/Media:' . $media['id'];
                    $arg .= ' --env=prod';
                    exec($command . $params . $arg);
                    $output->writeln($media['id'] . ' ' . $media['name']);
                }
            }

            $output->writeln('End.');
        } else {
            $output->writeln('You need to specify the media context');
        }
    }
}
