<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 01/10/17
 * Time: 11:21
 */

namespace Todotoday\MediaBundle\Traits;

use JMS\Serializer\Annotation as Serializer;

/**
 * Trait FormatedFilesTrait
 *
 * @package Todotoday\MediaBundle\Traits
 */
trait FormatedFilesTrait
{
    /**
     * @Serializer\VirtualProperty()
     * @Serializer\SerializedName("medias")
     *
     * @return array
     */
    public function getFormatedFiles(): array
    {
        return [
            'fr' => $this->getMedia(),
            'en' => $this->getMediaEn(),
            'de' => $this->getMediaDe(),
        ];
    }
}
