<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 16/06/2017
 * Time: 15:28
 */

namespace Todotoday\MediaBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Todotoday\ClassificationBundle\DataFixtures\ORM\LoadContextData;
use Todotoday\ClassificationBundle\Entity\Category;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyLinkedMicrosoft;
use Todotoday\MediaBundle\Entity\Media;

/**
 * Class LoadMediaData
 *
 * @category   Todo-Todev
 * @package    Todotoday\MediaBundle
 * @subpackage Todotoday\MediaBundle\DataFixtures\ORM
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class LoadMediaData extends AbstractFixture implements DependentFixtureInterface, ContainerAwareInterface
{
    /**
     * @var KernelInterface
     */
    protected $kernel;

    /**
     * {@inheritdoc}
     */
    public function getDependencies(): array
    {
        return array(
            LoadContextData::class,
        );
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     *
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function load(ObjectManager $manager): void
    {
        $keys = array_keys(LoadAgencyLinkedMicrosoft::getAgenciesLinkedSlug($this->kernel->getEnvironment()));

        $background = $this->kernel->locateResource(
            '@TodotodayCoreBundle/Resources/public/img/background/tdtd-cabriole.jpg'
        );

        $image = new Media();
        /**
 * @var Category $category
*/
        $category = $this->getReference('agency_category');
        $image->setContext($this->getReference('default_context')->getId());
        $image->setCategory($category);
        $image->setProviderName('sonata.media.provider.image');
        $image->setBinaryContent((string) $background);
        $image->setName('cabriole');

        $manager->persist($image);
        $manager->flush();
        $this->setReference('img-cabriole', $image);

        foreach ($keys as $key) {
            $this->createLogoHeader($manager, $key);
        }
    }

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     *
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     */
    public function setContainer(ContainerInterface $container = null): void
    {
        if ($container) {
            $this->kernel = $container->get('kernel');
        }
    }

    /**
     * Do createLogoHeader
     *
     * @param ObjectManager $manager
     * @param string        $slug
     *
     * @return void
     * @throws \RuntimeException
     * @throws \InvalidArgumentException
     */
    protected function createLogoHeader(ObjectManager $manager, string $slug): void
    {
        $background = $this->kernel->locateResource(
            '@TodotodayCoreBundle/Resources/public/img/header/' . $slug . '.png'
        );

        $image = new Media();
        /**
 * @var Category $category
*/
        $category = $this->getReference('agency_category');
        $image->setContext($this->getReference('default_context')->getId());
        $image->setCategory($category);
        $image->setProviderName('sonata.media.provider.image');
        $image->setBinaryContent($background);
        $image->setName('logo_header_agency_' . $slug);

        $manager->persist($image);
        $manager->flush();
        $this->setReference('img-header-' . $slug, $image);
    }
}
