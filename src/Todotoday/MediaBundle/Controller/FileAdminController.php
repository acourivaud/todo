<?php declare(strict_types=1);

namespace Todotoday\MediaBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\Response;

class FileAdminController extends CRUDController
{
    /**
     * {@inheritdoc}
     * @throws \LogicException
     */
    public function createAction(): Response
    {
        if (!$this->isGranted('ROLE_COMMUNITY_MANAGER') || $this->getRequest()->get('agencies_strategy')) {
            return parent::createAction();
        }

        return $this->render(
            '@TodotodayCMS/Admin/Post/select_option.html.twig',
            array(
                'action' => 'create',
                'options' => array(
                    'empty',
                    'all',
                    'us',
                    'fr',
                    'ch',
                ),
            )
        );
    }
}
