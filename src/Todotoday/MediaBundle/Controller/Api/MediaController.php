<?php
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 11/01/2017
 * Time: 16:58
 *
 * @category   Todo-Todev
 * @package    Todotoday\MediaBundle
 * @subpackage Todotoday\MediaBundle\Controller\Api
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
declare(strict_types = 1);

namespace Todotoday\MediaBundle\Controller\Api;

use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View as FOSRestView;
use JMS\Serializer\SerializationContext;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sonata\DatagridBundle\Pager\PagerInterface;
use Sonata\MediaBundle\Controller\Api\MediaController as BaseMediaController;
use Sonata\MediaBundle\Model\MediaInterface;
use Sonata\MediaBundle\Provider\MediaProviderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class MediaController
 *
 * @category   Todo-Todev
 * @package    Todotoday\MediaBundle
 * @subpackage Todotoday\MediaBundle\Controller\Api
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class MediaController extends BaseMediaController
{
    /**
     * Deletes a medium.
     *
     * @ApiDoc(
     *  requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="medium identifier"}
     *  },
     *  statusCodes={
     *      200="Returned when medium is successfully deleted",
     *      400="Returned when an error has occurred while deleting the medium",
     *      404="Returned when unable to find medium"
     *  },
     *  views={"media"},
     *  section="Media"
     * )
     *
     * @param int $id A medium identifier
     *
     * @return View
     *
     * @throws NotFoundHttpException
     */
    public function deleteMediumAction($id)
    {
        return parent::deleteMediumAction($id);
    }

    /**
     * Retrieves the list of medias (paginated).
     *
     * @ApiDoc(
     *  resource=true,
     *  output={"class"="Sonata\DatagridBundle\Pager\PagerInterface", "groups"={"sonata_api_read"}},
     *  views={"media"},
     *  section="Media"
     * )
     *
     * @FOSRest\QueryParam(name="page", requirements="\d+", default="1", description="Page for media list pagination")
     * @FOSRest\QueryParam(name="count", requirements="\d+", default="10", description="Number of medias by page")
     * @FOSRest\QueryParam(name="enabled", requirements="0|1", nullable=true, strict=true, description="Enabled/Disabled
     * medias filter")
     *
     * @FOSRest\View(serializerGroups={"sonata_api_read"}, serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return PagerInterface
     */
    public function getMediaAction(ParamFetcherInterface $paramFetcher)
    {
        return parent::getMediaAction($paramFetcher);
    }

    /**
     * Retrieves a specific media.
     *
     * @ApiDoc(
     *  requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="media id"}
     *  },
     *  output={"class"="Sonata\MediaBundle\Model\Media", "groups"={"sonata_api_read"}},
     *  statusCodes={
     *      200="Returned when successful",
     *      404="Returned when media is not found"
     *  },
     *  views={"media"},
     *  section="Media"
     * )
     *
     * @FOSRest\View(serializerGroups={"sonata_api_read"}, serializerEnableMaxDepthChecks=true)
     *
     * @param int $id
     *
     * @return MediaInterface
     */
    public function getMediumAction($id)
    {
        return parent::getMediumAction($id);
    }

    /**
     * Returns media binary content for each format.
     *
     * @ApiDoc(
     *  requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="media id"},
     *      {"name"="format", "dataType"="string", "description"="media format"}
     *  },
     *  statusCodes={
     *      200="Returned when successful",
     *      404="Returned when media is not found"
     *  },
     *  views={"media"},
     *  section="Media"
     * )
     *
     * @param int     $id      The media id
     * @param string  $format  The format
     * @param Request $request Object request
     *
     * @return Response
     */
    public function getMediumBinaryAction($id, $format, Request $request)
    {
        return parent::getMediumBinaryAction($id, $format, $request);
    }

    /**
     * Returns media urls for each format.
     *
     * @ApiDoc(
     *  requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="media id"}
     *  },
     *  statusCodes={
     *      200="Returned when successful",
     *      404="Returned when media is not found"
     *  },
     *  views={"media"},
     *  section="Media"
     * )
     *
     * @param int $id
     *
     * @return array
     */
    public function getMediumFormatsAction($id)
    {
        return parent::getMediumFormatsAction($id);
    }

    /**
     * Adds a medium of given provider
     * If you need to upload a file (depends on the provider) you will need to
     * do so by sending content as a multipart/form-data HTTP Request
     * See documentation for more details.
     *
     * @ApiDoc(
     *  resource=true,
     *  input={"class"="sonata_media_api_form_media", "name"="", "groups"={"sonata_api_write"}},
     *  output={"class"="Sonata\MediaBundle\Model\Media", "groups"={"sonata_api_read"}},
     *  statusCodes={
     *      200="Returned when successful",
     *      400="Returned when an error has occurred while medium creation",
     *      404="Returned when unable to find medium"
     *  },
     *  views={"media"},
     *  section="Provider"
     * )
     *
     * @FOSRest\Route(requirements={"provider"="[A-Za-z0-9.]*"})
     *
     * @param string  $provider A media provider
     * @param Request $request  A Symfony request
     *
     * @return MediaInterface
     *
     * @throws NotFoundHttpException
     */
    public function postProviderMediumAction($provider, Request $request)
    {
        return parent::postProviderMediumAction($provider, $request);
    }

    /**
     * Updates a medium
     * If you need to upload a file (depends on the provider) you will need to
     * do so by sending content as a multipart/form-data HTTP Request
     * See documentation for more details.
     *
     * @ApiDoc(
     *  requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="medium identifier"}
     *  },
     *  input={"class"="sonata_media_api_form_media", "name"="", "groups"={"sonata_api_write"}},
     *  output={"class"="Sonata\MediaBundle\Model\Media", "groups"={"sonata_api_read"}},
     *  statusCodes={
     *      200="Returned when successful",
     *      400="Returned when an error has occurred while medium update",
     *      404="Returned when unable to find medium"
     *  },
     *  views={"media"},
     *  section="Media"
     * )
     *
     * @param int     $id      A Medium identifier
     * @param Request $request A Symfony request
     *
     * @return MediaInterface
     *
     * @throws NotFoundHttpException
     */
    public function putMediumAction($id, Request $request)
    {
        return parent::putMediumAction($id, $request);
    }

    /**
     * Set Binary content for a specific media.
     *
     * @ApiDoc(
     *  input={"class"="Sonata\MediaBundle\Model\Media", "groups"={"sonata_api_write"}},
     *  output={"class"="Sonata\MediaBundle\Model\Media", "groups"={"sonata_api_read"}},
     *  statusCodes={
     *      200="Returned when successful",
     *      404="Returned when media is not found"
     *  },
     *  views={"media"},
     *  section="Media"
     * )
     *
     * @View(serializerGroups={"sonata_api_read"}, serializerEnableMaxDepthChecks=true)
     *
     * @param int     $id
     * @param Request $request A Symfony request
     *
     * @return MediaInterface
     *
     * @throws NotFoundHttpException
     */
    public function putMediumBinaryContentAction($id, Request $request)
    {
        return parent::putMediumBinaryContentAction($id, $request);
    }

    /**
     * Write a medium, this method is used by both POST and PUT action methods.
     *
     * @param Request                $request
     * @param MediaInterface         $media
     * @param MediaProviderInterface $provider
     *
     * @return FOSRestView|FormInterface
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     */
    protected function handleWriteMedium(Request $request, MediaInterface $media, MediaProviderInterface $provider)
    {
        $form = $this->formFactory->createNamed(
            null,
            'sonata_media_api_form_media',
            $media,
            array(
                'provider_name' => $provider->getName(),
                'csrf_protection' => false,
            )
        );

        $requestFields = $request->request->all();

        unset($requestFields['provider'], $requestFields['_format']);

        $form->submit($requestFields);

        if ($form->isValid()) {
            $media = $form->getData();
            $this->mediaManager->save($media);

            $view = FOSRestView::create($media);

            // BC for FOSRestBundle < 2.0
            if (method_exists($view, 'setSerializationContext')) {
                $serializationContext = SerializationContext::create();
                $serializationContext->setGroups(array('sonata_api_read'));
                $serializationContext->enableMaxDepthChecks();
                $view->setSerializationContext($serializationContext);
            } else {
                $context = new Context();
                $context->setGroups(array('sonata_api_read'));
                $context->disableMaxDepth();
                $view->setContext($context);
            }

            return $view;
        }

        return $form;
    }
}
