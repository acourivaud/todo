<?php
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 12/01/2017
 * Time: 08:53
 *
 * @category   Todo-Todev
 * @package    Todotoday\MediaBundle
 * @subpackage Todotoday\MediaBundle\Controller\Api
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
declare(strict_types = 1);

namespace Todotoday\MediaBundle\Controller\Api;

use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sonata\DatagridBundle\Pager\PagerInterface;
use Sonata\MediaBundle\Controller\Api\GalleryController as BaseGalleryController;
use Sonata\MediaBundle\Model\GalleryHasMediaInterface;
use Sonata\MediaBundle\Model\GalleryInterface;
use Sonata\MediaBundle\Model\MediaInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class GalleryController
 *
 * @category   Todo-Todev
 * @package    Todotoday\MediaBundle
 * @subpackage Todotoday\MediaBundle\Controller\Api
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class GalleryController extends BaseGalleryController
{
    /**
     * Deletes a gallery.
     *
     * @ApiDoc(
     *  requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="gallery identifier"}
     *  },
     *  statusCodes={
     *      200="Returned when gallery is successfully deleted",
     *      400="Returned when an error has occurred while gallery deletion",
     *      404="Returned when unable to find gallery"
     *  },
     *  views={"media"},
     *  section="Gallery"
     * )
     *
     * @param int $id A Gallery identifier
     *
     * @return View
     *
     * @throws NotFoundHttpException
     */
    public function deleteGalleryAction($id)
    {
        return parent::deleteGalleryAction($id);
    }

    /**
     * Deletes a media association to a gallery.
     *
     * @ApiDoc(
     *  requirements={
     *      {"name"="galleryId", "dataType"="integer", "requirement"="\d+", "description"="gallery identifier"},
     *      {"name"="mediaId", "dataType"="integer", "requirement"="\d+", "description"="media identifier"}
     *  },
     *  statusCodes={
     *      200="Returned when media is successfully deleted from gallery",
     *      400="Returned when an error has occurred while media deletion of gallery",
     *      404="Returned when unable to find gallery or media"
     *  },
     *  views={"media"},
     *  section="Gallery"
     * )
     *
     * @param int $galleryId A gallery identifier
     * @param int $mediaId   A media identifier
     *
     * @return View
     *
     * @throws NotFoundHttpException
     */
    public function deleteGalleryMediaGalleryhasmediaAction($galleryId, $mediaId)
    {
        return parent::deleteGalleryMediaGalleryhasmediaAction(
            $galleryId,
            $mediaId
        );
    }

    /**
     * Retrieves the list of galleries (paginated).
     *
     * @ApiDoc(
     *  resource=true,
     *  output={"class"="Sonata\DatagridBundle\Pager\PagerInterface", "groups"={"sonata_api_read"}},
     *  views={"media"},
     *  section="Gallery"
     * )
     *
     * @FOSRest\QueryParam(name="page", requirements="\d+", default="1", description="Page for gallery list pagination")
     * @FOSRest\QueryParam(name="count", requirements="\d+", default="10", description="Number of galleries by page")
     * @FOSRest\QueryParam(
     *     name="enabled",
     *     requirements="0|1",
     *     nullable=true,
     *     strict=true,
     *     description="Enabled/Disabled galleries filter"
     * )
     *
     * @View(serializerGroups={"sonata_api_read"}, serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return PagerInterface
     */
    public function getGalleriesAction(ParamFetcherInterface $paramFetcher)
    {
        return parent::getGalleriesAction($paramFetcher);
    }

    /**
     * Retrieves a specific gallery.
     *
     * @ApiDoc(
     *  requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="gallery id"}
     *  },
     *  output={"class"="sonata_media_api_form_gallery", "groups"={"sonata_api_read"}},
     *  statusCodes={
     *      200="Returned when successful",
     *      404="Returned when gallery is not found"
     *  },
     *  views={"media"},
     *  section="Gallery"
     * )
     *
     * @View(serializerGroups={"sonata_api_read"}, serializerEnableMaxDepthChecks=true)
     *
     * @param int $id
     *
     * @return GalleryInterface
     */
    public function getGalleryAction($id)
    {
        return parent::getGalleryAction($id);
    }

    /**
     * Retrieves the galleryhasmedias of specified gallery.
     *
     * @ApiDoc(
     *  requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="gallery id"}
     *  },
     *  output={"class"="Sonata\MediaBundle\Model\GalleryHasMedia", "groups"={"sonata_api_read"}},
     *  statusCodes={
     *      200="Returned when successful",
     *      404="Returned when gallery is not found"
     *  },
     *  views={"media"},
     *  section="Gallery"
     * )
     *
     * @View(serializerGroups={"sonata_api_read"}, serializerEnableMaxDepthChecks=true)
     *
     * @param int $id
     *
     * @return GalleryHasMediaInterface[]
     */
    public function getGalleryGalleryhasmediasAction($id)
    {
        return parent::getGalleryGalleryhasmediasAction($id);
    }

    /**
     * Retrieves the medias of specified gallery.
     *
     * @ApiDoc(
     *  requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="gallery id"}
     *  },
     *  output={"class"="Sonata\MediaBundle\Model\Media", "groups"={"sonata_api_read"}},
     *  statusCodes={
     *      200="Returned when successful",
     *      404="Returned when gallery is not found"
     *  },
     *  views={"media"},
     *  section="Gallery"
     * )
     *
     * @View(serializerGroups={"sonata_api_read"}, serializerEnableMaxDepthChecks=true)
     *
     * @param int $id
     *
     * @return MediaInterface[]
     */
    public function getGalleryMediasAction($id)
    {
        return parent::getGalleryMediasAction($id);
    }

    /**
     * Adds a gallery.
     *
     * @ApiDoc(
     *  input={"class"="sonata_media_api_form_gallery", "name"="", "groups"={"sonata_api_write"}},
     *  output={"class"="sonata_media_api_form_gallery", "groups"={"sonata_api_read"}},
     *  statusCodes={
     *      200="Returned when successful",
     *      400="Returned when an error has occurred while gallery creation",
     *  },
     *  views={"media"},
     *  section="Gallery"
     * )
     *
     * @param Request $request A Symfony request
     *
     * @return GalleryInterface
     *
     * @throws NotFoundHttpException
     */
    public function postGalleryAction(Request $request)
    {
        return parent::postGalleryAction($request);
    }

    /**
     * Adds a media to a gallery.
     *
     * @ApiDoc(
     *  requirements={
     *      {"name"="galleryId", "dataType"="integer", "requirement"="\d+", "description"="gallery identifier"},
     *      {"name"="mediaId", "dataType"="integer", "requirement"="\d+", "description"="media identifier"}
     *  },
     *  input={"class"="sonata_media_api_form_gallery_has_media", "name"="", "groups"={"sonata_api_write"}},
     *  output={"class"="sonata_media_api_form_gallery", "groups"={"sonata_api_read"}},
     *  statusCodes={
     *      200="Returned when successful",
     *      400="Returned when an error has occurred while gallery/media attachment",
     *  },
     *  views={"media"},
     *  section="Gallery"
     * )
     *
     * @param int     $galleryId A gallery identifier
     * @param int     $mediaId   A media identifier
     * @param Request $request   A Symfony request
     *
     * @return GalleryInterface
     *
     * @throws NotFoundHttpException
     */
    public function postGalleryMediaGalleryhasmediaAction($galleryId, $mediaId, Request $request)
    {
        return parent::postGalleryMediaGalleryhasmediaAction(
            $galleryId,
            $mediaId,
            $request
        );
    }

    /**
     * Updates a gallery.
     *
     * @ApiDoc(
     *  requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="gallery identifier"}
     *  },
     *  input={"class"="sonata_media_api_form_gallery", "name"="", "groups"={"sonata_api_write"}},
     *  output={"class"="sonata_media_api_form_gallery", "groups"={"sonata_api_read"}},
     *  statusCodes={
     *      200="Returned when successful",
     *      400="Returned when an error has occurred while gallery creation",
     *      404="Returned when unable to find gallery"
     *  },
     *  views={"media"},
     *  section="Gallery"
     * )
     *
     * @param int     $id      User id
     * @param Request $request A Symfony request
     *
     * @return GalleryInterface
     *
     * @throws NotFoundHttpException
     */
    public function putGalleryAction($id, Request $request)
    {
        return parent::putGalleryAction($id, $request);
    }

    /**
     * Updates a media to a gallery.
     *
     * @ApiDoc(
     *  requirements={
     *      {"name"="galleryId", "dataType"="integer", "requirement"="\d+", "description"="gallery identifier"},
     *      {"name"="mediaId", "dataType"="integer", "requirement"="\d+", "description"="media identifier"}
     *  },
     *  input={"class"="sonata_media_api_form_gallery_has_media", "name"="", "groups"={"sonata_api_write"}},
     *  output={"class"="sonata_media_api_form_gallery", "groups"={"sonata_api_read"}},
     *  statusCodes={
     *      200="Returned when successful",
     *      404="Returned when an error if media cannot be found in gallery",
     *  },
     *  views={"media"},
     *  section="Gallery"
     * )
     *
     * @param int     $galleryId A gallery identifier
     * @param int     $mediaId   A media identifier
     * @param Request $request   A Symfony request
     *
     * @return GalleryInterface
     *
     * @throws NotFoundHttpException
     */
    public function putGalleryMediaGalleryhasmediaAction($galleryId, $mediaId, Request $request)
    {
        return parent::putGalleryMediaGalleryhasmediaAction(
            $galleryId,
            $mediaId,
            $request
        );
    }
}
