<?php
/**
 * PHP version 7
 */
declare(strict_types = 1);

namespace Todotoday\MediaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Sonata\MediaBundle\Entity\BaseMedia;

/**
 * Media
 *
 * @author Alexandre Vinet <alexandre.vinet@actiane.com>
 *
 * @ORM\Table(name="media", schema="media")
 * @ORM\Entity(repositoryClass="Todotoday\MediaBundle\Repository\MediaRepository")
 *
 * @Serializer\ExclusionPolicy(value="all")
 * @Serializer\XmlRoot(name="_media")
 */
class Media extends BaseMedia
{
    /**
     * @var int $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\XmlAttributeMap()
     * @Serializer\Expose()
     * @Serializer\SerializedName("id")
     * @Serializer\Type("integer")
     * @Serializer\Since("1.0")
     * @Serializer\Groups({"Default", "sonata_api_read", "sonata_api_write", "sonata_search"})
     */
    protected $id;

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }
}
