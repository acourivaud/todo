<?php declare(strict_types=1);

namespace Todotoday\MediaBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslatable;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;
use Todotoday\CMSBundle\Entity\LinkFileAgency;
use Todotoday\MediaBundle\Traits\FormatedFilesTrait;

/**
 * @ORM\Table(name="file", schema="media")
 * @ORM\Entity(repositoryClass="Todotoday\MediaBundle\Repository\FileRepository")
 *
 * @Serializer\ExclusionPolicy("all")
 * @Serializer\XmlRoot(name="_file")
 * @Gedmo\TranslationEntity(class="Todotoday\MediaBundle\Entity\FileTranslation")
 *
 */
class File extends AbstractPersonalTranslatable implements TranslatableInterface
{
    use FormatedFilesTrait;

    /**
     * @var int $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\XmlAttributeMap()
     * @Serializer\Expose()
     * @Serializer\SerializedName("id")
     * @Serializer\Type("integer")
     * @Serializer\Since("1.0")
     * @Serializer\Groups({"Default", "sonata_api_read", "sonata_api_write", "sonata_search"})
     */
    protected $id;

    /**
     * @var
     *
     * @ORM\Column(type="string", name="title", nullable=true)
     * @Gedmo\Translatable()
     * @Serializer\Expose()
     */
    protected $title;

    /**
     * @var Media
     *
     * @ORM\ManyToOne(targetEntity="Todotoday\MediaBundle\Entity\Media", cascade={"persist", "remove", "refresh"})
     */
    protected $media;

    /**
     * @var Media
     *
     * @ORM\ManyToOne(targetEntity="Todotoday\MediaBundle\Entity\Media", cascade={"persist", "remove", "refresh"})
     */
    protected $mediaEn;

    /**
     * @var Media
     *
     * @ORM\ManyToOne(targetEntity="Todotoday\MediaBundle\Entity\Media", cascade={"persist", "remove", "refresh"})
     */
    protected $mediaDe;

    /**
     * @ORM\Column(type="string", name="description", nullable=true)
     * @Gedmo\Translatable()
     * @Serializer\Expose()
     */
    protected $description;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Todotoday\CMSBundle\Entity\LinkFileAgency",
     *     mappedBy="file",
     *     cascade={"persist", "remove", "refresh"})
     */
    protected $links;

    /**
     * @var Collection|FileTranslation[]
     *
     * @ORM\OneToMany(
     *     targetEntity="Todotoday\MediaBundle\Entity\FileTranslation",
     *     mappedBy="object",
     *     cascade={"persist","remove"}
     *     )
     * @Serializer\SerializedName("test")
     * @Serializer\Exclude(if="true")
     */
    protected $translations;

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getMedia() ? $this->getMedia()->getName() : '';
    }

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->links = new ArrayCollection();
    }

    /**
     * Set media
     *
     * @param Media $media
     *
     * @return File
     */
    public function setMedia(Media $media = null): File
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get media
     *
     * @return Media|null
     */
    public function getMedia(): ?Media
    {
        return $this->media;
    }

    /**
     * Add link
     *
     * @param LinkFileAgency $link
     *
     * @return File
     */
    public function addLink(LinkFileAgency $link): self
    {
        $this->links[] = $link;
        $link->setFile($this);

        return $this;
    }

    /**
     * @return Media
     */
    public function getMediaDe(): ?Media
    {
        return $this->mediaDe;
    }

    /**
     * @return Media
     */
    public function getMediaEn(): ?Media
    {
        return $this->mediaEn;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Remove link
     *
     * @param LinkFileAgency $link
     *
     * @return $this
     */
    public function removeLink(LinkFileAgency $link): self
    {
        $this->links->removeElement($link);
        $link->setFile(null);

        return $this;
    }

    /**
     * Get links
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLinks()
    {
        return $this->links;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     *
     * @return File
     */
    public function setDescription(?string $description = null): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @param Media $mediaDe
     *
     * @return File
     */
    public function setMediaDe(Media $mediaDe): File
    {
        $this->mediaDe = $mediaDe;

        return $this;
    }

    /**
     * @param Media $mediaEn
     *
     * @return File
     */
    public function setMediaEn(Media $mediaEn): File
    {
        $this->mediaEn = $mediaEn;

        return $this;
    }

    /**
     * @param mixed $title
     *
     * @return File
     */
    public function setTitle($title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @param string $locale
     *
     * @return null|Media
     */
    public function getMediaWithLocale(string $locale = 'fr'): ?Media
    {
        if (strtolower($locale) === 'fr') {
            return $this->getMedia();
        } elseif (method_exists($this, 'getMedia' . ucfirst($locale))) {
            return $this->{'getMedia' . ucfirst($locale)}();
        }

        return null;
    }
}
