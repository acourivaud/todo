<?php declare(strict_types = 1);
/**
 * PHP version 7
 */

namespace Todotoday\MediaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Sonata\MediaBundle\Entity\BaseGalleryHasMedia;

/**
 * GalleryHasMedia
 *
 * @author Alexandre Vinet <alexandre.vinet@actiane.com>
 *
 * @ORM\Table(name="gallery_media", schema="media")
 * @ORM\Entity()
 *
 * @Serializer\ExclusionPolicy(value="all")
 * @Serializer\XmlRoot(name="_gallery_has_media")
 */
class GalleryHasMedia extends BaseGalleryHasMedia
{
    /**
     * @var int $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\XmlAttributeMap()
     * @Serializer\Expose()
     * @Serializer\SerializedName("id")
     * @Serializer\Type("integer")
     * @Serializer\Since("1.0")
     * @Serializer\Groups({"Default", "sonata_api_read", "sonata_api_write", "sonata_search"})
     */
    protected $id;

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }
}
