<?php declare(strict_types=1);

namespace Todotoday\MediaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslation;

/**
 * Class FileTranslation
 *
 * @ORM\Entity()
 * @ORM\Table(
 *     schema="cms",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="lookup_unique_file_translation_idx", columns={
 *         "locale", "object_id", "field"
 * })})
 */
class FileTranslation extends AbstractPersonalTranslation
{
    /**
     * @var File
     *
     * @ORM\ManyToOne(targetEntity="Todotoday\MediaBundle\Entity\File", inversedBy="translations")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $object;
}
