<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 11/05/17
 * Time: 12:07
 */

namespace Todotoday\NotificationBundle\Repository\Api\MicrosoftCRM;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiRepository;
use Actiane\ApiConnectorBundle\Lib\Request;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\AccountBundle\Exceptions\AdherentCRMIdMissingException;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Exceptions\AgencyMissingCrmServiceClientMissingException;
use Todotoday\CoreBundle\Exceptions\AgencyMissingCrmTeamMissingException;
use Todotoday\NotificationBundle\Entity\Api\MicrosoftCRM\Notification;

/**
 * Class NotificationRepository
 *
 * @package Todotoday\NotificationBundle\Repository\Api\MicrosoftCRM
 */
class NotificationRepository extends AbstractApiRepository
{
    /**
     * @param Agency   $agency
     * @param Adherent $adherent
     *
     * @return Notification[]|null
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingCrmTeamMissingException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingCrmServiceClientMissingException
     * @throws \Todotoday\AccountBundle\Exceptions\AdherentCRMIdMissingException
     * @throws \InvalidArgumentException
     */
    public function getNotificationNotReadByAdherent(Agency $agency, Adherent $adherent): ?array
    {
        $filter = $this->getFilter($agency, $adherent);
        $filter .= ' and aps_read eq 100000000';

        return $this->getRequest($filter)->execute();
    }

    /**
     * @param Agency   $agency
     * @param Adherent $adherent
     *
     * @return array|null|Notification[]
     * @throws \InvalidArgumentException
     * @throws AdherentCRMIdMissingException
     * @throws AgencyMissingCrmServiceClientMissingException
     * @throws AgencyMissingCrmTeamMissingException
     */
    public function getNotificationByAdherent(Agency $agency, Adherent $adherent): ?array
    {
        $filter = $this->getFilter($agency, $adherent);
        $request = $this->getRequest($filter);

        return $request->execute();
    }

    /**
     * @param Agency   $agency
     * @param Adherent $adherent
     *
     * @return string
     * @throws AdherentCRMIdMissingException
     * @throws AgencyMissingCrmServiceClientMissingException
     * @throws AgencyMissingCrmTeamMissingException
     */
    private function getFilter(Agency $agency, Adherent $adherent): string
    {
        if (!$agencyServiceClient = $agency->getCrmServiceClient()) {
            throw new AgencyMissingCrmServiceClientMissingException();
        }

        if (!$agencyTeam = $agency->getCrmTeam()) {
            throw new AgencyMissingCrmTeamMissingException();
        }

        if (!$adherentCrmId = $adherent->getCrmIdAdherent()) {
            throw new AdherentCRMIdMissingException();
        }

        $filter = '_aps_clientadherent_value eq ' . $adherentCrmId;
        $filter .= ' and (_owningteam_value eq ' . $agencyServiceClient;
        $filter .= ' or _owningteam_value eq ' . $agencyTeam . ' )';

        return $filter;
    }

    /**
     * @param string $filter
     *
     * @return Request
     * @throws \InvalidArgumentException
     */
    private function getRequest(string $filter): Request
    {
        return $this
            ->createQueryBuilder()
            ->addCustomQuery(
                '$filter',
                $filter
            )
            ->addCustomQuery(
                '$orderby',
                'createdon desc'
            )
            ->getRequest();
    }
}
