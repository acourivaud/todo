<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 11/05/17
 * Time: 13:37
 */

namespace Todotoday\NotificationBundle\Controller\Api;

use FOS\RestBundle\Controller\Annotations as FosRest;
use Symfony\Component\HttpFoundation\Response;
use Todotoday\CoreBundle\Controller\Api\AbstractApiController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\CoreBundle\Exceptions\AgencyMissingException;

/**
 * Class NotificationApiController
 *
 * @package Todotoday\NotificationBundle\Controller\Api
 * @FosRest\Prefix("/notification")
 * @FosRest\NamePrefix(value="todotoday.notification.api.notification.")
 */
class NotificationApiController extends AbstractApiController
{
    /**
     *
     * Get single agency by id
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Get current notification's adherent",
     *     description="Get single agency by id",
     *     output={
     *           "class"="Todotoday\Notification\Entity\Api\MicrosoftCRM\Notification",
     *           "groups"={"Default"}
     *     },
     *     views={"default"},
     *     section="Notification"
     * )
     *
     * @FosRest\Route("/me")
     *
     * {@inheritdoc}
     * @throws \LogicException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \UnexpectedValueException
     * @throws \InvalidArgumentException
     * @throws \Todotoday\AccountBundle\Exceptions\AdherentCRMIdMissingException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingCrmServiceClientMissingException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingCrmTeamMissingException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     */
    public function getMeAction(): Response
    {
        if (!$this->isGranted('VIEW', $this)) {
            throw $this->createAccessDeniedException();
        }

        $adherent = $this->getUser();
        $agency = $this->getAgency();
        $repo = $this->get('todotoday.notification.repository.api.notification');
        $notifications = $repo->getNotificationByAdherent($agency, $adherent);
        $view = $this->view($notifications);

        return $this->handleView($view);
    }

    /**
     * @param string $apsNotificationId
     *
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Mark notification as read",
     *     description="Update status to read",
     *     output={
     *           "class"="Todotoday\Notification\Entity\Api\MicrosoftCRM\Notification",
     *           "groups"={"Default"}
     *     },
     *     views={"default"},
     *     section="Notification"
     * )
     *
     * @FosRest\Route("/{apsNotificationId}/read", options = { "expose" = true })
     *
     * {@inheritdoc}
     * @return Response
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingCrmTeamMissingException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingCrmServiceClientMissingException
     * @throws \Todotoday\AccountBundle\Exceptions\AdherentCRMIdMissingException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \LogicException
     * @throws \UnexpectedValueException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    public function putReadAction(string $apsNotificationId): Response
    {
        if (!$this->isGranted('VIEW', $this)) {
            throw $this->createAccessDeniedException();
        }

        $adherent = $this->getUser();
        $agency = $this->getAgency();

        $manager = $this->get('todotoday.notification.notification_manager');
        $notification = $manager->markNotificationAsRead($agency, $adherent, $apsNotificationId);
        $view = $this->view($notification);

        return $this->handleView($view);
    }

    /**
     * @ApiDoc(
     *     resource=true,
     *     resourceDescription="Mark all notification adherent as read",
     *     description="Update status to read for all notification",
     *     output={
     *           "class"="array<Todotoday\Notification\Entity\Api\MicrosoftCRM\Notification>",
     *           "groups"={"Default"}
     *     },
     *     views={"default"},
     *     section="Notification"
     * )
     *
     * @FosRest\Route("/readAll", options = { "expose" = true })
     *
     * {@inheritdoc}
     * @return Response
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingCrmTeamMissingException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingCrmServiceClientMissingException
     * @throws \Todotoday\AccountBundle\Exceptions\AdherentCRMIdMissingException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \LogicException
     * @throws \UnexpectedValueException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    public function postReadAllAction(): Response
    {
        if (!$this->isGranted('VIEW', $this)) {
            throw $this->createAccessDeniedException();
        }

        $adherent = $this->getUser();
        $agency = $this->getAgency();

        $manager = $this->get('todotoday.notification.notification_manager');
        $notification = $manager->markAllNotificationAsRead($agency, $adherent);
        $view = $this->view($notification);

        return $this->handleView($view);
    }

    /**
     * Get the formType for the current entity class link to the controller
     *
     * @return string
     */
    protected function getFormType(): string
    {
        return '';
    }

    /**
     * Get the serivce name from custum repository's entity
     *
     * @return string
     */
    protected function getServiceRepository(): string
    {
        return 'todotoday.notification.repository.api.notification';
    }

    /**
     * @return null|Agency
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyWithoutFrontException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyFrontDisabledException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingException
     * @throws \UnexpectedValueException
     */
    private function getAgency(): ?Agency
    {
        if (!$agency = $this->get('todotoday.core.domain_context.api')->getAgency()) {
            throw new AgencyMissingException('Agency is missing');
        }

        return $agency;
    }
}
