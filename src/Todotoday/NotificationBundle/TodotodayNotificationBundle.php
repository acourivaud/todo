<?php declare(strict_types=1);

namespace Todotoday\NotificationBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class TodotodayNotificationBundle
 * @package Todotoday\NotificationBundle
 */
class TodotodayNotificationBundle extends Bundle
{
}
