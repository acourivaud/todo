<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 14/06/2017
 * Time: 17:54
 */

namespace Todotoday\NotificationBundle\Services;

use Actiane\AlertBundle\Entity\Alert;
use Actiane\ApiConnectorBundle\Services\Connections\Microsoft\MicrosoftDynamicsConnection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Translation\DataCollectorTranslator;
use Symfony\Component\Translation\TranslatorInterface;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CoreBundle\Entity\Agency;
use Todotoday\NotificationBundle\Entity\Api\MicrosoftCRM\Notification;
use Todotoday\NotificationBundle\Repository\Api\MicrosoftCRM\NotificationRepository;

/**
 * Class NotificationManager
 *
 * @category   Todo-Todev
 * @package    Todotoday\NotificationBundle
 * @subpackage Todotoday\NotificationBundle\Services
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class NotificationManager
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var MicrosoftDynamicsConnection
     */
    private $connection;

    /**
     * NotificationManager constructor.
     *
     * @param EntityManager               $entityManager
     * @param TranslatorInterface         $translator
     * @param MicrosoftDynamicsConnection $connection
     */
    public function __construct(
        EntityManager $entityManager,
        TranslatorInterface $translator,
        MicrosoftDynamicsConnection $connection
    ) {
        $this->entityManager = $entityManager;
        $this->translator = $translator;
        $this->connection = $connection;
    }

    /**
     * Do sendAlertNotification
     *
     * @param Alert  $alert
     * @param Agency $agency
     *
     * @return NotificationManager
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    public function sendAlertNotification(Alert $alert, Agency $agency): self
    {
        $notification = (new Notification())
            ->setApsMessageen(
                $this->translator->trans(
                    $alert->getMessage(),
                    array(),
                    $alert->getMessageDomain(),
                    'en'
                )
            )
            ->setApsMessagefr(
                $this->translator->trans(
                    $alert->getMessage(),
                    array(),
                    $alert->getMessageDomain(),
                    'fr'
                )
            )
            ->setApsClientadherentValue($alert->getAccount()->getCrmIdAdherent())
            ->setOwningteamValue($agency->getCrmTeam() ?? $agency->getCrmServiceClient());
        $this->connection->persist($notification)->flush();

        return $this;
    }

    /**
     * Do sendAlertNotification
     *
     * @return NotificationManager
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     */
    public function sendAlertsNotifications(): self
    {
        $repo = $this->entityManager->getRepository(Alert::class);

        /** @var Alert[] $alerts */
        $alerts = $repo->findAll();

        foreach ($alerts as $alert) {
            foreach ($alert->getAccount()->getLinkAgencies() as $linkAccountAgency) {
                $this->sendAlertNotification($alert, $linkAccountAgency->getAgency());
            }
        }

        return $this;
    }

    /**
     * @param Agency   $agency
     * @param Adherent $adherent
     * @param string   $notificationId
     *
     * @return Notification
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingCrmTeamMissingException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingCrmServiceClientMissingException
     * @throws \Todotoday\AccountBundle\Exceptions\AdherentCRMIdMissingException
     *
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     */
    public function markNotificationAsRead(Agency $agency, Adherent $adherent, string $notificationId): ?Notification
    {
        $repo = $this->connection->getRepository(Notification::class);

        /** @var Notification $notification */
        if (!$notification = $repo->find(array('apsNotificationid' => $notificationId))) {
            return null;
        }

        $notification->setApsRead(100000001);
        $this->connection->flush();

        return $notification;
    }

    /**
     * @param Agency   $agency
     * @param Adherent $adherent
     *
     * @return array|null
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingCrmTeamMissingException
     * @throws \Todotoday\CoreBundle\Exceptions\AgencyMissingCrmServiceClientMissingException
     * @throws \Todotoday\AccountBundle\Exceptions\AdherentCRMIdMissingException
     * @throws \InvalidArgumentException
     */
    public function markAllNotificationAsRead(Agency $agency, Adherent $adherent): ?array
    {
        /** @var NotificationRepository $repo */
        $repo = $this->connection->getRepository(Notification::class);

        if ($notifications = $repo->getNotificationByAdherent($agency, $adherent)) {
            foreach ($notifications as &$notification) {
                if (!$notification->isRead()) {
                    $notification = $this->markNotificationAsRead(
                        $agency,
                        $adherent,
                        $notification->getApsNotificationid()
                    );
                }
            }
        }

        return $notifications;
    }
}
