<?php declare(strict_types=1);

namespace Todotoday\NotificationBundle\Repository\Api\MicrosoftCRM;

use Actiane\ApiConnectorBundle\Tests\Repository\Api\AbstractRepositoryTestCase;

/**
 * Class NotificationRepositoryTest
 * @package Totoday\NotificationBundle\Repository\Api\MicrosoftCRM
 */
class NotificationRepositoryTest extends AbstractRepositoryTestCase
{
    /**
     * Do getRepositoryId
     *
     * @return string
     */
    protected function getRepositoryId(): string
    {
        return 'todotoday.notification.repository.api.notification';
    }
}
