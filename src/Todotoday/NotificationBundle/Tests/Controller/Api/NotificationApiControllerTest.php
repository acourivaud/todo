<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 11/05/17
 * Time: 14:21
 */

namespace Todotoday\NotificationBundle\Tests\Controller\Api;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Actiane\UnitFonctionalTestBundle\Traits\AccessDeniedForNotAdherentTestableTrait;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAccountData;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAdherentLinkedMicrosoftData;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyLinkedMicrosoft;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class NotificationApiControllerTest
 * @package Todotoday\NotificationBundle\Tests\Controller\Api
 */
class NotificationApiControllerTest extends WebTestCase
{
    use AccessDeniedForNotAdherentTestableTrait;

    /**
     * @group ignore
     * @small
     */
    public function testGetNotificationAdherentSuccess(): void
    {
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference('adherent_linked_arevalta');
        /** @var Agency $agency */
        $agency = self::getFRR()->getReference('agency_arevalta');

        $url = $this->getUrl('todotoday.notification.api.notification.get_me');
        $this->loginAs($adherent, 'api');
        $client = $this->makeClient();
        $client->request(
            'GET',
            $url,
            [],
            [],
            [
                'HTTP_AGENCY' => $agency->getSlug(),
            ]
        );
        $response = json_decode($client->getResponse()->getContent(), true);
        static::assertNotNull($response);
    }

    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAccountData::class,
            LoadAgencyLinkedMicrosoft::class,
            LoadAdherentLinkedMicrosoftData::class,
        );
    }
}
