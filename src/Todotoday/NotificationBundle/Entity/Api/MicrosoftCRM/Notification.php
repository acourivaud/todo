<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 11/05/17
 * Time: 11:26
 */

namespace Todotoday\NotificationBundle\Entity\Api\MicrosoftCRM;

use Actiane\ApiConnectorBundle\Lib\EntityProxyTrait;
use Actiane\ApiConnectorBundle\Lib\Mapping as APIConnector;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class Service
 * @Serializer\ExclusionPolicy("all")
 * @APIConnector\Entity(path="aps_notifications", repositoryId="todotoday.notification.repository.api.notification")
 *
 * @package Todotoday\CatalogBundle\Entity\Api\MicrosoftCRM
 */
class Notification
{
    use EntityProxyTrait;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_motifdelanotification", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsMotifdelanotification;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_messageen", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsMessageen;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_notificationid", type="string", guid="id")
     * @APIConnector\Id()
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsNotificationid;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_owningbusinessunit_value", type="string", guid="businessunitid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $owningbusinessunitValue;

    /**
     * @var int
     *
     * @APIConnector\Column(name="statecode", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $statecode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_messagefr", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsMessagefr;

    /**
     * @var int
     *
     * @APIConnector\Column(name="statuscode", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $statuscode;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_createdby_value", type="string", guid="systemuserid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $createdbyValue;

    /**
     * @var int
     *
     * @APIConnector\Column(name="timezoneruleversionnumber", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $timezoneruleversionnumber;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_ownerid_value", type="string", guid="ownerid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $owneridValue;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="modifiedon", type="datetimez")
     * @Serializer\Expose()
     * @Serializer\Type("DateTime")
     */
    protected $modifiedon;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_read", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $apsRead;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_modifiedby_value", type="string", guid="systemuserid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $modifiedbyValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_owninguser_value", type="string", guid="systemuserid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $owninguserValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_aps_clientadherent_value", type="string", guid="aps_ClientAdherent")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsClientadherentValue;

    /**
     * @var float
     *
     * @APIConnector\Column(name="versionnumber", type="float")
     * @Serializer\Expose()
     * @Serializer\Type("float")
     */
    protected $versionnumber;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="createdon", type="datetimez")
     * @Serializer\Expose()
     * @Serializer\Type("DateTime")
     */
    protected $createdon;

    /**
     * @var int
     *
     * @APIConnector\Column(name="aps_statutdelanotif", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $apsStatutdelanotif;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_name", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsName;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="aps_datedenvoidelanotification", type="datetimez")
     * @Serializer\Expose()
     * @Serializer\Type("Datetime")
     */
    protected $apsDatedenvoidelanotification;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_createdonbehalfby_value", type="string", guid="systemuserid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $createdonbehalfbyValue;

    /**
     * @var bool
     *
     * @APIConnector\Column(name="aps_doittreenvoyeeparonesignal", type="bool")
     * @Serializer\Expose()
     * @Serializer\Type("boolean")
     */
    protected $apsDoittreenvoyeeparonesignal;

    /**
     * @var bool
     *
     * @APIConnector\Column(name="aps_envoiparmail", type="bool")
     * @Serializer\Expose()
     * @Serializer\Type("boolean")
     */
    protected $apsEnvoiparmail;

    /**
     * @var string
     *
     * @APIConnector\Column(name="aps_playerid", type="string")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $apsPlayerid;

    /**
     * @var int
     *
     * @APIConnector\Column(name="importsequencenumber", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $importsequencenumber;

    /**
     * @var \DateTime
     *
     * @APIConnector\Column(name="overriddencreatedon", type="datetimez")
     * @Serializer\Expose()
     * @Serializer\Type("DateTime")
     */
    protected $overriddencreatedon;

    /**
     * @var int
     *
     * @APIConnector\Column(name="utcconversiontimezonecode", type="int")
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $utcconversiontimezonecode;

    /**
     * @var bool
     *
     * @APIConnector\Column(name="aps_envoiparsms", type="bool")
     * @Serializer\Expose()
     * @Serializer\Type("boolean")
     */
    protected $apsEnvoiparsms;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_owningteam_value", type="string", guid="teamid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $owningteamValue;

    /**
     * @var string
     *
     * @APIConnector\Column(name="_modifiedonbehalfby_value", type="string", guid="systemuserid")
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $modifiedonbehalfbyValue;

    /**
     * @return string
     */
    public function getApsMotifdelanotification(): ?string
    {
        return $this->apsMotifdelanotification;
    }

    /**
     * @param string $apsMotifdelanotification
     *
     * @return Notification
     */
    public function setApsMotifdelanotification(?string $apsMotifdelanotification): Notification
    {
        return $this->setTrigger('apsMotifdelanotification', $apsMotifdelanotification);
    }

    /**
     * @return string
     */
    public function getApsMessageen(): ?string
    {
        return $this->apsMessageen;
    }

    /**
     * @param string $apsMessageen
     *
     * @return Notification
     */
    public function setApsMessageen(?string $apsMessageen): Notification
    {
        return $this->setTrigger('apsMessageen', $apsMessageen);
    }

    /**
     * @return string
     */
    public function getApsNotificationid(): ?string
    {
        return $this->apsNotificationid;
    }

    /**
     * @param string $apsNotificationid
     *
     * @return Notification
     */
    public function setApsNotificationid(?string $apsNotificationid): Notification
    {
        return $this->setTrigger('apsNotificationid', $apsNotificationid);
    }

    /**
     * @return string
     */
    public function getOwningbusinessunitValue(): ?string
    {
        return $this->owningbusinessunitValue;
    }

    /**
     * @param string $owningbusinessunitValue
     *
     * @return Notification
     */
    public function setOwningbusinessunitValue(?string $owningbusinessunitValue): Notification
    {
        return $this->setTrigger('owningbusinessunitValue', $owningbusinessunitValue);
    }

    /**
     * @return int
     */
    public function getStatecode(): ?int
    {
        return $this->statecode;
    }

    /**
     * @param int $statecode
     *
     * @return Notification
     */
    public function setStatecode(?int $statecode): Notification
    {
        return $this->setTrigger('statecode', $statecode);
    }

    /**
     * @return string
     */
    public function getApsMessagefr(): ?string
    {
        return $this->apsMessagefr;
    }

    /**
     * @param string $apsMessagefr
     *
     * @return Notification
     */
    public function setApsMessagefr(?string $apsMessagefr): Notification
    {
        return $this->setTrigger('apsMessagefr', $apsMessagefr);
    }

    /**
     * @return int
     */
    public function getStatuscode(): ?int
    {
        return $this->statuscode;
    }

    /**
     * @param int $statuscode
     *
     * @return Notification
     */
    public function setStatuscode(?int $statuscode): Notification
    {
        return $this->setTrigger('statuscode', $statuscode);
    }

    /**
     * @return string
     */
    public function getCreatedbyValue(): ?string
    {
        return $this->createdbyValue;
    }

    /**
     * @param string $createdbyValue
     *
     * @return Notification
     */
    public function setCreatedbyValue(?string $createdbyValue): Notification
    {
        return $this->setTrigger('createdbyValue', $createdbyValue);
    }

    /**
     * @return int
     */
    public function getTimezoneruleversionnumber(): ?int
    {
        return $this->timezoneruleversionnumber;
    }

    /**
     * @param int $timezoneruleversionnumber
     *
     * @return Notification
     */
    public function setTimezoneruleversionnumber(?int $timezoneruleversionnumber): Notification
    {
        return $this->setTrigger('timezoneruleversionnumber', $timezoneruleversionnumber);
    }

    /**
     * @return string|array
     */
    public function getOwneridValue()
    {
        return $this->owneridValue;
    }

    /**
     * @param string $owneridValue
     *
     * @return Notification
     */
    public function setOwneridValue(?string $owneridValue): Notification
    {
        return $this->setTrigger('owneridValue', $owneridValue);
    }

    /**
     * @param null|string $owneridValue
     *
     * @return Notification
     */
    public function setOwnerIdValueGuid(?string $owneridValue): Notification
    {
        return $this->setTriggerGuid(
            'owneridValue',
            array(
                'target' => 'teams',
                'value' => $owneridValue,
            )
        );
    }

    /**
     * @return \DateTime
     */
    public function getModifiedon(): ?\DateTime
    {
        return $this->modifiedon;
    }

    /**
     * @param \DateTime $modifiedon
     *
     * @return Notification
     */
    public function setModifiedon(?\DateTime $modifiedon): Notification
    {
        return $this->setTrigger('modifiedon', $modifiedon);
    }

    /**
     * @return int
     */
    public function getApsRead(): ?int
    {
        return $this->apsRead;
    }

    /**
     * @param int $apsRead
     *
     * @return Notification
     */
    public function setApsRead(?int $apsRead): Notification
    {
        return $this->setTrigger('apsRead', $apsRead);
    }

    /**
     * @return string
     */
    public function getModifiedbyValue(): ?string
    {
        return $this->modifiedbyValue;
    }

    /**
     * @param string $modifiedbyValue
     *
     * @return Notification
     */
    public function setModifiedbyValue(?string $modifiedbyValue): Notification
    {
        return $this->setTrigger('modifiedbyValue', $modifiedbyValue);
    }

    /**
     * @return string
     */
    public function getOwninguserValue(): ?string
    {
        return $this->owninguserValue;
    }

    /**
     * @param string $owninguserValue
     *
     * @return Notification
     */
    public function setOwninguserValue(?string $owninguserValue): Notification
    {
        return $this->setTrigger('owninguserValue', $owninguserValue);
    }

    /**
     * @return string|array
     */
    public function getApsClientadherentValue()
    {
        return $this->apsClientadherentValue;
    }

    /**
     * @param string $apsClientadherentValue
     *
     * @return Notification
     */
    public function setApsClientadherentValue(?string $apsClientadherentValue): Notification
    {
        return $this->setTrigger('apsClientadherentValue', $apsClientadherentValue);
    }

    /**
     * @param null|string $apsClientadherentValue
     *
     * @return Notification
     */
    public function setApsClientadherentValueGuid(?string $apsClientadherentValue): Notification
    {
        return $this->setTriggerGuid(
            'apsClientadherentValue',
            [
                'target' => 'accounts',
                'value' => $apsClientadherentValue,
            ]
        );
    }

    /**
     * @return float
     */
    public function getVersionnumber(): ?float
    {
        return $this->versionnumber;
    }

    /**
     * @param float $versionnumber
     *
     * @return Notification
     */
    public function setVersionnumber(?float $versionnumber): Notification
    {
        return $this->setTrigger('versionnumber', $versionnumber);
    }

    /**
     * @return \DateTime
     */
    public function getCreatedon(): ?\DateTime
    {
        return $this->createdon;
    }

    /**
     * @param \DateTime $createdon
     *
     * @return Notification
     */
    public function setCreatedon(?\DateTime $createdon): Notification
    {
        return $this->setTrigger('createdon', $createdon);
    }

    /**
     * @return int
     */
    public function getApsStatutdelanotif(): ?int
    {
        return $this->apsStatutdelanotif;
    }

    /**
     * @param int $apsStatutdelanotif
     *
     * @return Notification
     */
    public function setApsStatutdelanotif(?int $apsStatutdelanotif): Notification
    {
        return $this->setTrigger('apsStatutdelanotif', $apsStatutdelanotif);
    }

    /**
     * @return string
     */
    public function getApsName(): ?string
    {
        return $this->apsName;
    }

    /**
     * @param string $apsName
     *
     * @return Notification
     */
    public function setApsName(?string $apsName): Notification
    {
        return $this->setTrigger('apsName', $apsName);
    }

    /**
     * @return \DateTime
     */
    public function getApsDatedenvoidelanotification(): ?\DateTime
    {
        return $this->apsDatedenvoidelanotification;
    }

    /**
     * @param \DateTime $apsDatedenvoidelanotification
     *
     * @return Notification
     */
    public function setApsDatedenvoidelanotification(?\DateTime $apsDatedenvoidelanotification): Notification
    {
        return $this->setTrigger('apsDatedenvoidelanotification', $apsDatedenvoidelanotification);
    }

    /**
     * @return string
     */
    public function getCreatedonbehalfbyValue(): ?string
    {
        return $this->createdonbehalfbyValue;
    }

    /**
     * @param string $createdonbehalfbyValue
     *
     * @return Notification
     */
    public function setCreatedonbehalfbyValue(?string $createdonbehalfbyValue): Notification
    {
        return $this->setTrigger('createdonbehalfbyValue', $createdonbehalfbyValue);
    }

    /**
     * @return bool
     */
    public function isApsDoittreenvoyeeparonesignal(): ?bool
    {
        return $this->apsDoittreenvoyeeparonesignal;
    }

    /**
     * @param bool $apsDoittreenvoyeeparonesignal
     *
     * @return Notification
     */
    public function setApsDoittreenvoyeeparonesignal(?bool $apsDoittreenvoyeeparonesignal): Notification
    {
        return $this->setTrigger('apsDoittreenvoyeeparonesignal', $apsDoittreenvoyeeparonesignal);
    }

    /**
     * @return bool
     */
    public function isApsEnvoiparmail(): ?bool
    {
        return $this->apsEnvoiparmail;
    }

    /**
     * @param bool $apsEnvoiparmail
     *
     * @return Notification
     */
    public function setApsEnvoiparmail(?bool $apsEnvoiparmail): Notification
    {
        return $this->setTrigger('apsEnvoiparmail', $apsEnvoiparmail);
    }

    /**
     * @return string
     */
    public function getApsPlayerid(): ?string
    {
        return $this->apsPlayerid;
    }

    /**
     * @param string $apsPlayerid
     *
     * @return Notification
     */
    public function setApsPlayerid(?string $apsPlayerid): Notification
    {
        return $this->setTrigger('apsPlayerid', $apsPlayerid);
    }

    /**
     * @return int
     */
    public function getImportsequencenumber(): ?int
    {
        return $this->importsequencenumber;
    }

    /**
     * @param int $importsequencenumber
     *
     * @return Notification
     */
    public function setImportsequencenumber(?int $importsequencenumber): Notification
    {
        return $this->setTrigger('importsequencenumber', $importsequencenumber);
    }

    /**
     * @return \DateTime
     */
    public function getOverriddencreatedon(): ?\DateTime
    {
        return $this->overriddencreatedon;
    }

    /**
     * @param \DateTime $overriddencreatedon
     *
     * @return Notification
     */
    public function setOverriddencreatedon(?\DateTime $overriddencreatedon): Notification
    {
        return $this->setTrigger('overriddencreatedon', $overriddencreatedon);
    }

    /**
     * @return int
     */
    public function getUtcconversiontimezonecode(): ?int
    {
        return $this->utcconversiontimezonecode;
    }

    /**
     * @param int $utcconversiontimezonecode
     *
     * @return Notification
     */
    public function setUtcconversiontimezonecode(?int $utcconversiontimezonecode): Notification
    {
        return $this->setTrigger('utcconversiontimezonecode', $utcconversiontimezonecode);
    }

    /**
     * @return bool
     */
    public function isApsEnvoiparsms(): ?bool
    {
        return $this->apsEnvoiparsms;
    }

    /**
     * @param bool $apsEnvoiparsms
     *
     * @return Notification
     */
    public function setApsEnvoiparsms(?bool $apsEnvoiparsms): Notification
    {
        return $this->setTrigger('apsEnvoiparsms', $apsEnvoiparsms);
    }

    /**
     * @return string
     */
    public function getOwningteamValue(): ?string
    {
        return $this->owningteamValue;
    }

    /**
     * @param string $owningteamValue
     *
     * @return Notification
     */
    public function setOwningteamValue(?string $owningteamValue): Notification
    {
        return $this->setTrigger('owningteamValue', $owningteamValue);
    }

    /**
     * @return string
     */
    public function getModifiedonbehalfbyValue(): ?string
    {
        return $this->modifiedonbehalfbyValue;
    }

    /**
     * @param string $modifiedonbehalfbyValue
     *
     * @return Notification
     */
    public function setModifiedonbehalfbyValue(?string $modifiedonbehalfbyValue): Notification
    {
        return $this->setTrigger('modifiedonbehalfbyValue', $modifiedonbehalfbyValue);
    }

    /**
     * @Serializer\VirtualProperty()
     * @Serializer\Type("boolean")
     * @Serializer\Expose()
     * @return bool
     */
    public function isRead(): bool
    {
        return $this->apsRead === 100000001;
    }
}
