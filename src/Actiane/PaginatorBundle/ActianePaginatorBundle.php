<?php
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 26/09/2016
 * Time: 09:00
 *
 * @category   Actiane - Library
 * @package    Actiane\PaginatorBundle
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
declare(strict_types = 1);

namespace Actiane\PaginatorBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class ActianePaginatorBundle
 *
 * @category   Actiane - Library
 * @package    Actiane\PaginatorBundle
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class ActianePaginatorBundle extends Bundle
{
}
