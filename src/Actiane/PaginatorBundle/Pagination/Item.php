<?php
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 26/09/2016
 * Time: 10:13
 *
 * @category   Actiane - Library
 * @package    Actiane\PaginatorBundle
 * @subpackage Actiane\PaginatorBundle\Pagination
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
declare(strict_types = 1);

namespace Actiane\PaginatorBundle\Pagination;

/**
 * Class Item
 *
 * @category   Actiane - Library
 * @package    Actiane\PaginatorBundle
 * @subpackage Actiane\PaginatorBundle\Pagination
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class Item
{
    /**
     * Class name with namespace.
     *
     * @var string
     */
    protected $className;

    /**
     * Method name to call back
     *
     * @var string
     */
    protected $methodRepo;

    /**
     * @var array
     */
    protected $callbackOptions;

    /**
     * The results of the callback.
     *
     * @var array|mixed[]
     */
    protected $results;

    /**
     * The result of the callback {$methodRepo . 'Count'}
     * The purpose is to get the total result to know how many page we have.
     *
     * @var int
     */
    protected $nbIterationTotal;

    /**
     * The current page
     *
     * @var int
     */
    protected $page;

    /**
     * Nb page
     *
     * @var int
     */
    protected $nbPage;

    /**
     * Nb page to display
     *
     * @var int
     */
    protected $nbSideDisplayPage;

    /**
     * Number of result per page.
     *
     * @var int
     */
    protected $limit;

    /**
     * The field name in the request to get the current page.
     *
     * @var string
     */
    protected $pageField;

    /**
     * All parameters for generate the route.
     *
     * @var array
     */
    protected $routeParams;

    /**
     * The route to generate. By default it's the current route.
     *
     * @var string
     */
    protected $route;

    /**
     * Item constructor.
     *
     * @param string $entityClass
     * @param string $methodRepo
     * @param array  $options
     */
    public function __construct(string $entityClass, string $methodRepo, array &$options)
    {
        $this->className = $entityClass;
        $this->methodRepo = $methodRepo;
        $this->page = $options['page'];
        $this->limit = $options['limit'];
        $this->callbackOptions = $options['callback_options'];
        $this->nbSideDisplayPage = $options['nb_side_display_page'];
        $this->pageField = $options['page_field'];
        $this->routeParams = $options['route_params'];
        $this->route = $options['route'];
    }

    /**
     * Get callbackOptions
     *
     * @return array
     */
    public function getCallbackOptions(): array
    {
        return $this->callbackOptions;
    }

    /**
     * Set callbackOptions
     *
     * @param array $callbackOptions
     *
     * @return Item
     */
    public function setCallbackOptions(array $callbackOptions): self
    {
        $this->callbackOptions = $callbackOptions;

        return $this;
    }

    /**
     * Get className
     *
     * @return string
     */
    public function getClassName(): string
    {
        return $this->className;
    }

    /**
     * Set className
     *
     * @param string $className
     *
     * @return Item
     */
    public function setClassName(string $className): self
    {
        $this->className = $className;

        return $this;
    }

    /**
     * Do getFirstDisplayPage
     *
     * @return int
     */
    public function getFirstDisplayPage(): int
    {
        $first = $this->getPage() - $this->getNbSideDisplayPage();
//        $last = $this->getPage() + $this->getNbSideDisplayPage();

//        if ($last > $this->getNbPage()) {
//            $first -= ($last - $this->getNbPage());
//        }
        if ($first < 1) {
            $first = 1;
        }

        return $first;
    }

    /**
     * Do getFirstDisplayPage
     *
     * @return int
     */
    public function getLastDisplayPage(): int
    {
//        $first = $this->getPage() - $this->getNbSideDisplayPage();
        $last = $this->getPage() + $this->getNbSideDisplayPage();

//        if ($first < 1) {
//            $last += ($first - 1) * -1;
//        }
        if ($last > $this->getNbPage()) {
            $last = $this->getNbPage();
        }

        return $last;
    }

    /**
     * Get limit
     *
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * Set limit
     *
     * @param int $limit
     *
     * @return Item
     */
    public function setLimit(int $limit): self
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * Get methodRepo
     *
     * @return string
     */
    public function getMethodRepo(): string
    {
        return $this->methodRepo;
    }

    /**
     * Set methodRepo
     *
     * @param string $methodRepo
     *
     * @return Item
     */
    public function setMethodRepo(string $methodRepo): self
    {
        $this->methodRepo = $methodRepo;

        return $this;
    }

    /**
     * Get nbDisplayPage
     *
     * @return int
     */
    public function getNbDisplayPage(): int
    {
        return ($this->nbSideDisplayPage * 2) + 1;
    }

    /**
     * Get nbIterationTotal
     *
     * @return int
     */
    public function getNbIterationTotal(): int
    {
        return $this->nbIterationTotal;
    }

    /**
     * Set nbIterationTotal
     *
     * @param int $nbIterationTotal
     *
     * @return Item
     */
    public function setNbIterationTotal(int $nbIterationTotal): self
    {
        $this->nbIterationTotal = $nbIterationTotal;

        return $this;
    }

    /**
     * Get nbPage
     *
     * @return int
     */
    public function getNbPage(): int
    {
        return $this->nbPage;
    }

    /**
     * Set nbPage
     *
     * @param int $nbPage
     *
     * @return Item
     */
    public function setNbPage(int $nbPage): self
    {
        $this->nbPage = $nbPage;

        return $this;
    }

    /**
     * Get nbSideDisplayPage
     *
     * @return int
     */
    public function getNbSideDisplayPage(): int
    {
        return $this->nbSideDisplayPage;
    }

    /**
     * Set nbSideDisplayPage
     *
     * @param int $nbSideDisplayPage
     *
     * @return Item
     */
    public function setNbSideDisplayPage(int $nbSideDisplayPage): self
    {
        $this->nbSideDisplayPage = $nbSideDisplayPage;

        return $this;
    }

    /**
     * Get page
     *
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * Set page
     *
     * @param int $page
     *
     * @return Item
     */
    public function setPage(int $page): self
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get pageField
     *
     * @return string
     */
    public function getPageField(): string
    {
        return $this->pageField;
    }

    /**
     * Set pageField
     *
     * @param string $pageField
     *
     * @return Item
     */
    public function setPageField(string $pageField): self
    {
        $this->pageField = $pageField;

        return $this;
    }

    /**
     * Get results
     *
     * @return array|mixed[]
     */
    public function getResults(): array
    {
        return $this->results;
    }

    /**
     * Set results
     *
     * @param array|mixed[] $results
     *
     * @return Item
     */
    public function setResults(array $results): self
    {
        $this->results = $results;

        return $this;
    }

    /**
     * Get route
     *
     * @return string
     */
    public function getRoute(): string
    {
        return $this->route;
    }

    /**
     * Set route
     *
     * @param string $route
     *
     * @return Item
     */
    public function setRoute(string $route): self
    {
        $this->route = $route;

        return $this;
    }

    /**
     * Get routeParams
     *
     * @return array
     */
    public function getRouteParams(): array
    {
        return $this->routeParams;
    }

    /**
     * Set routeParams
     *
     * @param array $routeParams
     *
     * @return Item
     */
    public function setRouteParams(array $routeParams): self
    {
        $this->routeParams = $routeParams;

        return $this;
    }
}
