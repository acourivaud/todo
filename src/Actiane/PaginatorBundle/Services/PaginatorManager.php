<?php
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 26/09/2016
 * Time: 09:00
 *
 * @category   Actiane - Library
 * @package    Actiane\PaginatorBundle
 * @subpackage Actiane\PaginatorBundle\Services
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
declare(strict_types = 1);

namespace Actiane\PaginatorBundle\Services;

use Actiane\PaginatorBundle\Pagination\Item;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class PaginatorManager
 *
 * @category   Actiane - Library
 * @package    Actiane\PaginatorBundle
 * @subpackage Actiane\PaginatorBundle\Services
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class PaginatorManager
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var array|Item[]
     */
    protected $itemContainer;

    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * PaginatorManager constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param RequestStack           $requestStack
     */
    public function __construct(EntityManagerInterface $entityManager, RequestStack $requestStack)
    {
        $this->entityManager = $entityManager;
        $this->itemContainer = array();
        $this->requestStack = $requestStack;
    }

    /**
     * Do generate
     *
     * @param string $entityClass
     * @param string $methodRepo
     * @param array  $options
     *
     * @return Item
     */
    public function generate(string $entityClass, string $methodRepo, array $options = array()): Item
    {
        $this->resolveOptions($options);
        $item = new Item($entityClass, $methodRepo, $options);
        $this->process($item);

        return $item;
    }

    /**
     * Do process
     *
     * @param Item $item
     *
     * @return void
     */
    public function process(Item $item)
    {
        $repo = $this->entityManager->getRepository($item->getClassName());
        $method = $item->getMethodRepo();
        $methodCount = $method . 'Count';

        $options = array_merge(
            array(
                'limit' => $item->getLimit(),
                'offset' => ($item->getPage() - 1) * $item->getLimit(),
            ),
            $item->getCallbackOptions()
        );

        $item->setResults(call_user_func_array(array($repo, $method), $options));
        $item->setNbIterationTotal(call_user_func_array(array($repo, $methodCount), $item->getCallbackOptions()));
        $item->setNbPage((int) ceil($item->getNbIterationTotal() / $item->getLimit()));
    }

    /**
     * Do resolveOptions
     *
     * @param array $options
     *
     * @return void
     */
    protected function resolveOptions(array &$options)
    {
        $options = array_merge(
            array(
                'page_field' => 'page',
                'limit' => 10,
                'nb_side_display_page' => 2,
                'callback_options' => array(),
                'route' => $this->requestStack->getCurrentRequest()->get('_route'),
                'route_params' => array_merge(
                    $this->requestStack->getCurrentRequest()->get('_route_params'),
                    $this->requestStack->getCurrentRequest()->query->all()
                ),
            ),
            $options
        );

        $options['page'] = (int) $this->requestStack->getCurrentRequest()->get($options['page_field'], 1) ?: 1;
    }
}
