<?php
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 26/09/2016
 * Time: 17:48
 *
 * @category   Actiane - Library
 * @package    Actiane\PaginatorBundle
 * @subpackage Actiane\PaginatorBundle\Services\Twgit
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
declare(strict_types = 1);

namespace Actiane\PaginatorBundle\Services\Twgit;

use Actiane\PaginatorBundle\Pagination\Item;
use Twig_Environment;

/**
 * Class RenderExtension
 *
 * @category   Actiane - Library
 * @package    Actiane\PaginatorBundle
 * @subpackage Actiane\PaginatorBundle\Services\Twgit
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class RenderExtension extends \Twig_Extension
{
    /**
     * Do getFunctions
     *
     * @return array|\Twig_SimpleFunction[]
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction(
                'actiane_paginator_render_pagination',
                array($this, 'renderPagination'),
                array(
                    'is_safe' => array(
                        'html',
                    ),
                    'needs_environment' => true,
                )
            ),
        );
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'actiane_pagnitor_render';
    }

    /**
     * Do renderPagination
     *
     * @param Twig_Environment $env
     * @param Item             $item
     *
     * @return string
     * @throws \InvalidArgumentException
     * @throws \Twig_Error_Syntax
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Loader
     */
    public function renderPagination(Twig_Environment $env, Item $item): string
    {
        return $env->render(
            'ActianePaginatorBundle:Pagination:paginate.html.twig',
            array(
                'item' => $item,
            )
        );
    }
}
