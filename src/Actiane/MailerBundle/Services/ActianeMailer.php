<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 09/02/17
 * Time: 13:54
 */

namespace Actiane\MailerBundle\Services;

use Actiane\MailerBundle\Interfaces\IcsPartInterface;
use Actiane\MailerBundle\Interfaces\MailSenderInterface;
use Actiane\ToolsBundle\Enum\LoggerEnum;
use Actiane\ToolsBundle\Traits\LoggerTrait;
use Monolog\Logger;

/**
 * Class ActianeMailer
 *
 * @package Actiane\MailerBundle\Services
 */
class ActianeMailer
{
    use LoggerTrait;

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var MailSenderInterface
     */
    private $sender;

    /**
     * @var \Swift_Message
     */
    private $message;

    /**
     * @var string|array
     */
    private $addressTo;

    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * ActianeMailer constructor.
     *
     * @param \Swift_Mailer     $mailer
     * @param Logger            $logger
     * @param \Twig_Environment $twig
     */
    public function __construct(\Swift_Mailer $mailer, Logger $logger, \Twig_Environment $twig)
    {
        $this->mailer = $mailer;
        $this->logger = $logger;
        $this->twig = $twig;
    }

    /**
     * @param IcsPartInterface $event
     *
     * @throws \Psr\Log\InvalidArgumentException
     */
    public function addIcsPart(IcsPartInterface $event): void
    {
        $now = new \DateTime();
        $organizer = '[' . $this->sender->getCorporate() . '] ' . $this->sender->getName();
        $filename = $this->sender->getCorporate() . '-' . $this->sender->getName();
        $filename .= '-' . $event->getEventName() . '.ics';
        $attendee = '';
        $sampleAttendee = 'ATTENDEE;CN="%1$s";RSVP=TRUE:mailto:%1$s';
        if (is_array($this->addressTo)) {
            foreach ($this->addressTo as $address) {
                $attendee .= sprintf($sampleAttendee, $address) . PHP_EOL;
            }
        } else {
            $attendee = sprintf($sampleAttendee, $this->addressTo) . PHP_EOL;
        }

        $icsContent = 'BEGIN:VCALENDAR' . PHP_EOL;
        $icsContent .= 'PRODID:-//Microsoft Corporation//Outlook 16.0 MIMEDIR//EN' . PHP_EOL;
        $icsContent .= 'VERSION:2.0' . PHP_EOL;
        $icsContent .= 'METHOD:REQUEST' . PHP_EOL;
        $icsContent .= 'CALSCALE:GREGORIAN' . PHP_EOL;
        $icsContent .= 'BEGIN:VEVENT' . PHP_EOL;
        $icsContent .= $attendee . PHP_EOL;
        $icsContent .= 'DTSTART:' . $event->getEventStart()->format('Ymd\THis') . PHP_EOL;
        $icsContent .= 'DTEND:' . $event->getEventEnd()->format('Ymd\THis') . PHP_EOL;
        $icsContent .= 'DTSTAMP:' . $now->format('Ymd\THis') . PHP_EOL;
        $icsContent .= 'ORGANIZER;CN=' . $this->sender->getName() . ':mailto:' . $this->sender->getEmail() . PHP_EOL;
        $icsContent .= 'UID:' . uniqid('tdtd', true) . '@' . $this->sender->getName() . PHP_EOL;
        $icsContent .= 'CLASS:PUBLIC' . PHP_EOL;
        $icsContent .= 'DESCRIPTION:' . $organizer . ' requested Phone/Video Meeting Request' . PHP_EOL;
        $icsContent .= 'LOCATION:' . $event->getLocation() . PHP_EOL;
        $icsContent .= 'PRIORITY:5' . PHP_EOL;
        $icsContent .= 'SEQUENCE:0' . PHP_EOL;
        $icsContent .= 'SUMMARY:Meeting has been scheduled by ' . $organizer . PHP_EOL;
        $icsContent .= 'TRANSP:OPAQUE' . PHP_EOL;
        $icsContent .= 'BEGIN:VALARM' . PHP_EOL;
        $icsContent .= 'TRIGGER:-PT15M' . PHP_EOL;
        $icsContent .= 'ACTION:DISPLAY' . PHP_EOL;
        $icsContent .= 'DESCRIPTION:Reminder' . PHP_EOL;
        $icsContent .= 'END:VALARM' . PHP_EOL;
        $icsContent .= 'END:VEVENT' . PHP_EOL;
        $icsContent .= 'END:VCALENDAR' . PHP_EOL;

        $ics = (new \Swift_Attachment())
            ->setFilename($filename)
            ->setDisposition('inline; filename=' . $filename)
            ->setBody($icsContent)
            ->setContentType('text/calendar; method=REQUEST; charset=utf-8');

        $this->message->setBody($icsContent, 'multipart/alternative MIME', 'utf-8');
        $this->message->attach($ics);

        $this->log(
            LoggerEnum::DEBUG,
            'Create email with ICS for ' . serialize($this->addressTo)
        );
    }

    /**
     * @param string|array $addressTo
     * @param string       $subjet
     *
     * @return ActianeMailer
     * @throws \Psr\Log\InvalidArgumentException
     */
    public function createMail($addressTo, string $subjet): self
    {
        $this->addressTo = $addressTo;
        $mail = new \Swift_Message();
        $mail
            ->setFrom(
                'no-reply@conciergerie-todotoday.com',
                'TO DO TODAY'
            )
            ->setSender(
                'no-reply@conciergerie-todotoday.com',
                'TO DO TODAY'
            )
            ->setTo($this->addressTo)
            ->setSubject('TO DO TODAY - ' . $subjet);
        $this->message = $mail;

        $this->log(
            LoggerEnum::DEBUG,
            'Create email for ' . serialize($addressTo)
        );

        return $this;
    }

    /**
     * @return \Swift_Mailer
     */
    public function getMailer(): \Swift_Mailer
    {
        return $this->mailer;
    }

    /**
     * @return \Swift_Message
     */
    public function getMessage(): \Swift_Message
    {
        return $this->message;
    }

    /**
     * @param string     $template
     * @param array|null $variables
     *
     * @return string
     * @throws \Twig_Error_Syntax
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Loader
     */
    public function renderTemplate(string $template, ?array $variables = []): string
    {
        return $this->twig->render($template, $variables);
    }

    /**
     * @return int
     * @throws \Psr\Log\InvalidArgumentException
     */
    public function send(): int
    {
        $this->log(
            LoggerEnum::DEBUG,
            'Send email for ' . serialize($this->addressTo)
        );

        return $this->mailer->send($this->message);
    }

    /**
     * @param MailSenderInterface $sender
     *
     * @return ActianeMailer
     */
    public function setSenderCorporate(MailSenderInterface $sender): self
    {
        $this->sender = $sender;

        return $this;
    }
}
