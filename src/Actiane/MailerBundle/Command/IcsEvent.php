<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 12/02/17
 * Time: 17:57
 */

namespace Actiane\MailerBundle\Command;

use Actiane\MailerBundle\Interfaces\IcsPartInterface;

class IcsEvent implements IcsPartInterface
{

    /**
     * @return \DateTime
     */
    public function getEventStart(): \DateTime
    {
        $event = new \DateTime('2017-12-25');
        $event->setTime(12, 00);

        return $event;
    }

    /**
     * @return \Datetime
     */
    public function getEventEnd(): \Datetime
    {
        $event = new \DateTime('2017-12-25');
        $event->setTime(15, 00);

        return $event;
    }

    /**
     * @return string
     */
    public function getEventName(): string
    {
        return 'test';
    }

    /**
     * @return string
     */
    public function getEventDescription(): string
    {
        return 'ceci est un test';
    }

    /**
     * @return string
     */
    public function getLocation(): string
    {
        return 'paris';
    }
}
