<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 12/02/17
 * Time: 17:20
 */

namespace Actiane\MailerBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\Request;
use Todotoday\CoreBundle\Entity\Agency;

/**
 * Class SendMailCommand
 * @package Actiane\MailerBundle\Command
 */
class SendMailCommand extends ContainerAwareCommand
{
    /**
     * Configure method
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function configure()
    {
        $this
            ->setName('actiane:mailer:send')
            ->setDescription('Send email from actianer mailer')
            ->addArgument('to', InputArgument::REQUIRED, 'address to send email to')
            ->addArgument('from', InputArgument::REQUIRED, 'slug agency to send mail from');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \LogicException
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$agency = $this->getContainer()->get('doctrine')->getRepository(Agency::class)->findOneBy(
            array(
                'slug' => $input->getArgument('from'),
            )
        )
        ) {
            return $output->writeln('No agency found for ' . $input->getArgument('from'));
        };
        $mailer = $this->getContainer()->get('actiane.mailer');
        $requestStack = $this->getContainer()->get('request_stack');
        $requestStack->push(Request::create('http://' . $agency->getSlug() . '.todotoday.local'));
        $mailer->createMail($input->getArgument('to'), 'ceci est un test');
        $mailer->getMessage()->setBody('blablabla');
        $output->writeln($mailer->send() . ' mail sent');
    }
}
