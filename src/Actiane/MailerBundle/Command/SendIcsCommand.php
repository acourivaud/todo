<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 12/02/17
 * Time: 17:20
 */

namespace Actiane\MailerBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class SendMailCommand
 * @package Actiane\MailerBundle\Command
 */
class SendIcsCommand extends ContainerAwareCommand
{
    /**
     * Configure method
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function configure()
    {
        $this
            ->setName('actiane:ics:send')
            ->setDescription('Send email from actianer mailer with ICS')
            ->addArgument('to', InputArgument::REQUIRED, 'address to send email to');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \LogicException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $agency = $this->getContainer()->get('doctrine')->getRepository('TodotodayCoreBundle:Agency')->find(1);
        $mailer = $this->getContainer()->get('actiane.mailer');

        $event = new IcsEvent();

        $requestStack = $this->getContainer()->get('request_stack');
        $requestStack->push(Request::create('http://' . $agency->getSlug() . '.todotoday.local'));
        $mail = $mailer->createMail($input->getArgument('to'), 'ceci est un test');
        $mail->addIcsPart($event);
        $output->writeln($mailer->send($mail) . ' mail sent');
    }
}
