<?php declare(strict_types = 1);
namespace Actiane\MailerBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ActianeMailerBundle extends Bundle
{
}
