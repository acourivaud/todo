<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 12/02/17
 * Time: 08:56
 */

namespace Actiane\MailerBundle\Interfaces;

/**
 * Interface IcsPartInterface
 * @package Actiane\MailerBundle\Interfaces
 */
interface IcsPartInterface
{

    /**
     * @return \DateTime
     */
    public function getEventStart(): \DateTime;

    /**
     * @return \Datetime
     */
    public function getEventEnd(): \Datetime;

    /**
     * @return string
     */
    public function getEventName(): string;

    /**
     * @return string
     */
    public function getEventDescription(): string;

    /**
     * @return string
     */
    public function getLocation(): string;
}
