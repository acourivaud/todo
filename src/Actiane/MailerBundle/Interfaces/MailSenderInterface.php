<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 09/02/17
 * Time: 17:46
 */

namespace Actiane\MailerBundle\Interfaces;

/**
 * Interface MailSenderInterface
 * @package Actiane\MailerBundle\Interfaces
 */
interface MailSenderInterface
{
    /**
     * @return string
     */
    public function getName(): ?string;

    /**
     * @return string
     */
    public function getEmail(): ?string;

    /**
     * @return string
     */
    public function getCorporate(): ?string;
}
