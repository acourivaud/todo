<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 10/11/17
 * Time: 10:58
 *
 * @category   Todo-Todev
 *
 * @package    Actiane\ToolsBundle\Constraints
 *
 * @subpackage Actiane\ToolsBundle\Constraints
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Actiane\ToolsBundle\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class DateInterval
 *
 * @Annotation
 * @Target({"CLASS", "ANNOTATION"})
 */
class DateInterval extends Constraint
{
    /**
     * @var
     */
    public $startDate;

    /**
     * @var
     */
    public $endDate;

    /**
     * @return string
     */
    public function validatedBy(): string
    {
        return get_class($this) . 'Validator';
    }

    /**
     * @return string
     */
    public function getTargets(): string
    {
        return self::CLASS_CONSTRAINT;
    }

    /**
     * @return array
     */
    public function getRequiredOptions(): array
    {
        return ['startDate', 'endDate'];
    }
}
