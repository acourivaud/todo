<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 10/11/17
 * Time: 11:08
 *
 * @category   Todo-Todev
 *
 * @package    Actiane\ToolsBundle\Constraints
 *
 * @subpackage Actiane\ToolsBundle\Constraints
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Actiane\ToolsBundle\Constraints;

use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class DateIntervalValidator extends ConstraintValidator
{
    /**
     * Checks if the passed value is valid.
     *
     * @param mixed                   $entity     The entity that should be validated
     * @param Constraint|DateInterval $constraint The constraint for the validation
     *
     * @throws \Symfony\Component\PropertyAccess\Exception\AccessException
     * @throws \Symfony\Component\PropertyAccess\Exception\InvalidArgumentException
     * @throws \Symfony\Component\PropertyAccess\Exception\UnexpectedTypeException
     */
    public function validate($entity, Constraint $constraint): void
    {
        $startDatePath = $constraint->startDate;
        $endDatePath = $constraint->endDate;

        $accessor = PropertyAccess::createPropertyAccessor();
        $dateStart = $accessor->getValue($entity, $startDatePath);
        $dateEnd = $accessor->getValue($entity, $endDatePath);

        if (!$dateStart instanceof \DateTime && !$dateEnd instanceof \DateTime) {
            return;
        }

        $executionContext = $this->context;
        if ($dateStart > $dateEnd) {
            $executionContext->buildViolation('validation.startDate_greater_than_endDate')
                ->atPath($endDatePath)
                ->addViolation();
        }
    }
}
