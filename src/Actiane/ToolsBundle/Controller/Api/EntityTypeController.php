<?php
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 29/11/2016
 * Time: 14:12
 *
 * @category   Actiane - TOOLS
 * @package    Actiane\ToolsBundle
 * @subpackage Actiane\ToolsBundle\Controller\Api
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
declare(strict_types = 1);

namespace Actiane\ToolsBundle\Controller\Api;

use Actiane\ToolsBundle\Interfaces\FormApiTypeInterface;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\Controller\Annotations\Prefix;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Exception\AlreadySubmittedException;
use Symfony\Component\Form\Exception\LogicException;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\Exception as SFOException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class EntityTypeController
 *
 * @NamePrefix("actiane.tools.api.entity_type.")
 * @Prefix("/entity")
 *
 * @category   Actiane - TOOLS
 * @package    Actiane\ToolsBundle
 * @subpackage Actiane\ToolsBundle\Controller\Api
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class EntityTypeController extends FOSRestController
{
    /**
     * Do editAction
     *
     * @param Request $request
     * @param string  $formId
     * @param int     $id
     *
     * @return Response
     * @throws \Exception
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     * @throws AccessDeniedException
     * @throws SFOException\UndefinedOptionsException
     * @throws SFOException\OptionDefinitionException
     * @throws SFOException\NoSuchOptionException
     * @throws SFOException\MissingOptionsException
     * @throws SFOException\InvalidOptionsException
     * @throws SFOException\AccessException
     * @throws \RuntimeException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    public function editTypeEntityAction(Request $request, string $formId, int $id)
    {
        $forms = $this->editForm($request, $formId, $id);
        $forms['mainForm'] = $forms['mainForm']->createView();

        $view = $this->view($forms)
            ->setTemplate('@ActianeTools/form/form.html.twig');

        return $this->handleView($view);
    }

    /**
     * Do createAction
     *
     * @param Request $request
     * @param string  $formId
     *
     * @return Response
     * @throws \Exception
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     * @throws \RuntimeException
     * @throws SFOException\UndefinedOptionsException
     * @throws SFOException\OptionDefinitionException
     * @throws SFOException\NoSuchOptionException
     * @throws SFOException\MissingOptionsException
     * @throws SFOException\InvalidOptionsException
     * @throws SFOException\AccessException
     * @throws \LogicException
     * @throws AccessDeniedException
     * @throws \InvalidArgumentException
     */
    public function newTypeEntityAction(Request $request, string $formId)
    {
        $forms = $this->newForm($request, $formId);
        $forms['mainForm'] = $forms['mainForm']->createView();
        $view = $this->view($forms)
            ->setTemplate('@ActianeTools/form/form.html.twig');

        return $this->handleView($view);
    }

    /**
     * Do createAction
     *
     * @param Request $request
     * @param string  $formId
     *
     * @return Response
     * @throws \Exception
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     * @throws \RuntimeException
     * @throws AccessDeniedException
     * @throws SFOException\UndefinedOptionsException
     * @throws SFOException\OptionDefinitionException
     * @throws SFOException\NoSuchOptionException
     * @throws SFOException\MissingOptionsException
     * @throws SFOException\InvalidOptionsException
     * @throws SFOException\AccessException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    public function postTypeEntityAction(Request $request, string $formId)
    {
        $forms = $this->newForm($request, $formId);
        /** @var FormInterface $form */
        $form = $forms['mainForm'];
        $em = $this->getDoctrine()->getManager();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $em->persist($data);
            $em->flush();

            return $this->handleView($this->view($data)->setFormat('json'));
        }

        $forms['mainForm'] = $form->createView();

        return $this->handleView($this->view($forms)->setTemplate('@ActianeTools/form/form.html.twig'));
    }

    /**
     * Do editAction
     *
     * @param Request $request
     * @param string  $formId
     * @param int     $id
     *
     * @return Response
     * @throws \Exception
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     * @throws \RuntimeException
     * @throws SFOException\UndefinedOptionsException
     * @throws SFOException\OptionDefinitionException
     * @throws SFOException\NoSuchOptionException
     * @throws SFOException\MissingOptionsException
     * @throws SFOException\InvalidOptionsException
     * @throws SFOException\AccessException
     * @throws AccessDeniedException
     * @throws \LogicException
     * @throws LogicException
     * @throws AlreadySubmittedException
     * @throws \InvalidArgumentException
     */
    public function putTypeEntityAction(Request $request, string $formId, int $id)
    {
        $forms = $this->editForm($request, $formId, $id);
        $form = $forms['mainForm'];

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $data = $form->getData();
            $em->persist($data);
            $em->flush();

            return $this->handleView($this->view($data)->setFormat('json'));
        }
        $forms['mainForm'] = $form->createView();

        return $this->handleView($this->view($forms)->setTemplate('@ActianeTools/form/form.html.twig'));
    }

    /**
     * Do editForm
     *
     * @param Request $request
     * @param string  $formId
     * @param int     $id
     *
     * @return array
     * @throws \Exception
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     * @throws SFOException\UndefinedOptionsException
     * @throws SFOException\OptionDefinitionException
     * @throws SFOException\NoSuchOptionException
     * @throws SFOException\MissingOptionsException
     * @throws SFOException\InvalidOptionsException
     * @throws SFOException\AccessException
     * @throws AccessDeniedException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    protected function editForm(Request $request, string &$formId, int &$id): array
    {
        $formType = $this->get($formId);
        $reflect = new \ReflectionClass($formType);
        if (!$reflect->implementsInterface(FormApiTypeInterface::class)) {
            throw $this->createAccessDeniedException(
                'You have to implement the interface "FormApiTypeInterface" in your form'
            );
        }
        $entityTypeManager = $this->get('actiane.tools.services.form_api_type_manager');

        $array = $entityTypeManager->resolveOptions($formType->getFormApiConf());
        $class = $array['data_class'];
        $roles = $array['roles'];
        $imports = $array['imports'];

        if (!$roles || !array_key_exists('EDIT', $roles) || !$this->isGranted($roles['EDIT'])) {
            throw $this->createAccessDeniedException();
        }

        $options = array(
            'action' => $this->generateUrl(
                'actiane.tools.api.entity_type.put_type_entity',
                array(
                    'formId' => $formId,
                    'id' => $id,
                    '_format' => 'html',
                ),
                UrlGeneratorInterface::ABSOLUTE_URL
            ),
            'method' => 'put',
            'csrf_protection' => false,
        );
        $entity = $this->getDoctrine()->getManager()->find($class, $id);
        $subForms =
            $entityTypeManager->processImports($request, $entity = ($entity ?? new $class()), $imports, $options);

        $mainForm = $this->createForm(
            $formType,
            $entity ?? new $class(),
            $options
        );

        return array(
            'subForms' => $subForms,
            'mainForm' => $mainForm,
        );
    }

    /**
     * Do newForm
     *
     * @param Request $request
     * @param string  $formId
     *
     * @return array
     * @throws \Exception
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     * @throws \RuntimeException
     * @throws SFOException\UndefinedOptionsException
     * @throws SFOException\OptionDefinitionException
     * @throws SFOException\NoSuchOptionException
     * @throws SFOException\MissingOptionsException
     * @throws SFOException\InvalidOptionsException
     * @throws SFOException\AccessException
     * @throws AccessDeniedException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    protected function newForm(Request $request, string &$formId): array
    {
        /** @var AbstractType|FormApiTypeInterface $formType */
        $formType = $this->get($formId);
        $reflect = new \ReflectionClass($formType);
        if (!$reflect->implementsInterface(FormApiTypeInterface::class)) {
            throw $this->createAccessDeniedException(
                'You have to implement the interface "FormApiTypeInterface" in your form'
            );
        }
        $entityTypeManager = $this->get('actiane.tools.services.form_api_type_manager');

        $array = $entityTypeManager->resolveOptions($formType->getFormApiConf());
        $class = $array['data_class'];
        $roles = $array['roles'];
        $imports = $array['imports'];
        if (!$roles || !array_key_exists('NEW', $roles) || !$this->isGranted($roles['NEW'])) {
            throw $this->createAccessDeniedException();
        }

        $data = null;
        if ($discr = $request->get('discr')) {
            /** @var ClassMetadataInfo $meta */
            $meta = $this->getDoctrine()->getManager()->getClassMetadata($class);
            $map = $meta->discriminatorMap;
            if ($map && array_key_exists($discr, $map)) {
                $class = $map[$discr];
                $data = new $class();
            }
        }

        $options = array(
            'action' => $this->generateUrl(
                'actiane.tools.api.entity_type.post_type_entity',
                array(
                    'formId' => $formId,
                    'discr' => $request->get('discr'),
                    '_format' => 'html',
                ),
                UrlGeneratorInterface::ABSOLUTE_URL
            ),
            'csrf_protection' => false,
        );

        $subForms = $entityTypeManager->processImports($request, $data = ($data ?? new $class()), $imports, $options);

        $mainForm = $this->createForm(
            $formType,
            $data,
            $options
        );

        return array(
            'subForms' => $subForms,
            'mainForm' => $mainForm,
        );
    }
}
