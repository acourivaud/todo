<?php declare(strict_types=1);

namespace Actiane\ToolsBundle\Command;

use Sonata\AdminBundle\Util\ObjectAclManipulatorInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ActianeAclGenerateObjectAclCommand
 */
class ActianeAclGenerateObjectAclCommand extends ContainerAwareCommand
{
    /**
     *
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function configure(): void
    {
        $this
            ->setName('actiane:acl:generate-object-acl')
            ->setDescription('Generate ACL for given admin class')
            ->addArgument('class', InputArgument::REQUIRED, 'Admin class to generate ACLs');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws \Sonata\AdminBundle\Exception\ModelManagerException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \LogicException
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $pool = $this->getContainer()->get('sonata.admin.pool');
        $admin = $pool->getInstance($input->getArgument('class'));

        $manipulatorId = sprintf('sonata.admin.manipulator.acl.object.%s', $admin->getManagerType());

        if (!$this->getContainer()->has($manipulatorId)) {
            $output->writeln(
                'Admin class is using a manager type that has no manipulator implemented : <info>ignoring</info>'
            );

            return;
        }

        $manipulator = $this->getContainer()->get($manipulatorId);
        if (!$manipulator instanceof ObjectAclManipulatorInterface) {
            $output->writeln(
                sprintf(
                    'The interface "ObjectAclManipulatorInterface" is not implemented for %s: <info>ignoring</info>',
                    \get_class($manipulator)
                )
            );

            return;
        }

        $manipulator->batchConfigureAcls($output, $admin);
    }
}
