$(function () {
    $('button[id$=_add_entity_button]').on('click', function (event) {
        let $button = $(event.currentTarget);
        let options = {'formId': $button.data('widget-id'), '_format': 'html'};
        let discr = $button.data('widget-discr');
        if (discr) {
            options.discr = discr;
        }
        let route = Routing.generate($button.data('src'), options);
        let for_form = $button.data('for');
        $.ajax({
            url: route,
            xhrFields: {
                withCredentials: true
            }
        })
            .done(function (data) {
                let $modal = $('#form_generic_modal');
                $modal.find('.modal-body').html(data);
                initDateTimePicker();
                let $button_send = $('#form_generic_modal_button_send');
                $button_send.data('src', route);
                $button_send.data('for', for_form);
                initSubformEvent($modal);
                $modal.modal();
            })
        ;
    });
    $('button[id$=_edit_entity_button]').on('click', function (event) {
        let $button = $(event.currentTarget);
        let $form = $('#' + $button.data('for'));
        let route = Routing.generate($button.data('src'), {
            'formId': $button.data('widget-id'),
            'id': $form.val(),
            '_format': 'html'
        });

        $.ajax({
            url: route,
            xhrFields: {
                withCredentials: true
            }
        })
            .done(function (data) {
                let $modal = $('#form_generic_modal');
                $modal.find('.modal-body').html(data);
                initDateTimePicker();
                let $button_send = $('#form_generic_modal_button_send');
                $button_send.data('src', route);
                $button_send.data('for', null);
                initSubformEvent($modal);
                $modal.modal();
            })
        ;
    });
    $('button[id$=_delete_entity_button]').on('click', function (event) {
        let $button = $(event.currentTarget);
        let route = Routing.generate($button.data('src'), {'formId': $button.data('widget-id'), '_format': 'html'});
        $.ajax({
            url: route,
            xhrFields: {
                withCredentials: true
            }
        })
            .done(function (data) {
                let $modal = $('#form_generic_modal');
                $modal.find('.modal-body').html(data);
                initDateTimePicker();
                $('#form_generic_modal_button_send').data('src', route);
                $modal.modal();
            })
        ;
    });
    $('#form_generic_modal_button_send').on('click', function (event) {
        let $button = $(event.currentTarget);
        let $modal = $button.closest('.modal');
        let $form = $modal.find('form');
        let action = $form.attr('action');

        if (action === undefined || action === null) {
            action = $button.data('src');
        }

        $.ajax({
            url: action,
            type: $form.attr('method'),
            data: $form.serialize(),
            success: function (data) {
                $modal = $('#form_generic_modal');
                let for_form = $button.data('for');

                if (for_form != null) {
                    $form = $('#' + for_form);
                    $form.append(new Option(data.to_string, data.id));
                    $form.val(data.id).trigger('change');
                }

                $modal.modal('hide');
            },
            error: function (data) {
                $modal = $('#form_generic_modal');
                $modal.find('.modal-body').html(data.responseText);
                initDateTimePicker();
                initSubformEvent($modal);
                $modal.modal();
            },
            xhrFields: {
                withCredentials: true
            }
        })
        ;
    });
    function initSubformEvent($modal) {
        $modal.find('form').on('submit', function (e) {
            e.preventDefault();

            let $form = $(event.currentTarget);
            let action = $form.attr('action');

            $.ajax({
                url: action,
                type: $form.attr('method'),
                data: $form.serialize(),
                complete: function (data) {
                    console.log(data);
                    $modal = $('#form_generic_modal');
                    $modal.find('.modal-body').html(data.responseText);
                    initDateTimePicker();
                    initSubformEvent($modal);
                    $modal.modal();
                },
                xhrFields: {
                    withCredentials: true
                }
            })
            ;
        })
    }
});