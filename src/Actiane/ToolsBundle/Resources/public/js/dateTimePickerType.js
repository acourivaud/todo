import Vue from 'vue'
import FlatDatePickr from 'vue-flatpickr'
import translation from '../../../../../../vue-src/filters/translation'

const forms = document.querySelectorAll('.datetimepicker_form')

forms.forEach(el => {
  new Vue({
    el: '#' + el.getAttribute('id'),
    components: {flatpickr: FlatDatePickr},
    delimiters: ['#{', '}'],
    data () {
      return {
        dateSelected: null,
        flatpickrOptions: {
          enableTime: false,
          defaultDate: '',
          time_24hr: true,
          dateFormat: 'Y-m-d',
          altInput: true,
          altFormat: $t('booking.desired_date_format'),
          locale: window.flatpickrLocale
        }
      }
    },
    computed: {
      translatePlaceHolder () {
        return $t('rdv.title')
      },
    },
    methods: {
      clear () {
        this.dateSelected = null
        this.$el.querySelector('.flatpickr-input.form-control.input').value = ''
      }
    },
    beforeMount () {
      const needTime = this.$el.attributes['time'].value === 'true'
      this.$set(this.flatpickrOptions, 'enableTime', needTime)
      this.$set(this.flatpickrOptions, 'altFormat', needTime ? $t('booking.desired_date_hour_format') : $t('booking.desired_date_format'))
      this.$set(this.flatpickrOptions, 'defaultDate', this.$el.attributes['date'].value)
      if (needTime) {
        this.$set(this.flatpickrOptions, 'dateFormat', this.flatpickrOptions.dateFormat + ' H:i')
      }
    },
  })
})