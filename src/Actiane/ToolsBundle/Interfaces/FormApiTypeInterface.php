<?php
declare(strict_types = 1);

/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 30/11/2016
 * Time: 14:28
 *
 * @category   Actiane - Tools
 * @package    Actiane\ToolsBundle\Interfaces
 * @subpackage Actiane\ToolsBundle\Interfaces
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */

namespace Actiane\ToolsBundle\Interfaces;

/**
 * Interface FormApiTypeInterface
 *
 * @category   Actiane - Tools
 * @package    Actiane\ToolsBundle\Interfaces
 * @subpackage Actiane\ToolsBundle\Interfaces
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
interface FormApiTypeInterface
{
    /**
     * Return an array withe the form conf
     *
     * @return array
     */
    public function getFormApiConf(): array;
}
