<?php
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 03/01/2017
 * Time: 18:25
 *
 * @category   LaTribune - CMS
 * @package    Actiane\ToolsBundle
 * @subpackage Actiane\ToolsBundle\Interfaces
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
declare(strict_types = 1);

namespace Actiane\ToolsBundle\Interfaces;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Interface ImportFormApiTypeInterface
 *
 * @category   LaTribune - CMS
 * @package    Actiane\ToolsBundle
 * @subpackage Actiane\ToolsBundle\Interfaces
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
interface ImportFormApiTypeInterface
{
    /**
     * Return an array withe the form conf
     *
     * @param Request $request
     * @param object  $entity
     * @param array   $options
     *
     * @return FormInterface
     */
    public function process(Request $request, $entity, array $options): FormInterface;
}
