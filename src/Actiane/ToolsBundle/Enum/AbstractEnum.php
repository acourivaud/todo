<?php
/**
 * Created by PhpStorm.
 * PHP version 7
 * User: alexandre_vinet
 * Date: 21/04/2016
 * Time: 14:05
 */
declare(strict_types=1);

namespace Actiane\ToolsBundle\Enum;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Platforms\PostgreSqlPlatform;
use Doctrine\DBAL\Platforms\SqlitePlatform;
use Doctrine\DBAL\Platforms\SQLServerPlatform;
use Doctrine\DBAL\Types\Type;

/**
 * Class AbstractEnum
 *
 * @package LaTribune\NewsletterBundle\Enum
 */
abstract class AbstractEnum extends Type
{
    /**
     * @var mixed
     */
    const __DEFAULT = null;

    /**
     * @var \ReflectionClass[]
     */
    protected static $reflects;

    /**
     * @var array[]
     */
    protected static $arrays;

    /**
     * @var mixed
     */
    protected $value;

    /**
     * Get readable choices for the ENUM field.
     *
     * @static
     *
     * @return array Values for the ENUM field
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     */
    public static function getChoices(): array
    {
        $choices = array();
        foreach (static::toArray() as $value) {
            $choices[$value] = static::getNewInstance($value);
        }

        return $choices;
    }

    /**
     * Do getCurrentConstants
     *
     * @return string[]
     */
    public static function getCurrentConstants(): array
    {
        $const = static::getReflect(static::class)->getConstants();
        $keys = array_keys(static::getReflect(AbstractEnum::class)->getConstants());
        foreach ($keys as $key) {
            unset($const[$key]);
        }

        return $const;
    }

    /**
     * Do getNewInstance
     *
     * @param string $value
     *
     * @return AbstractEnum
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     */
    public static function getNewInstance(string $value = null): AbstractEnum
    {
        if (!$value) {
            $value = static::__DEFAULT;
        }

        $defaultInstance = static::getType((new \ReflectionClass(static::class))->getShortName());
        /** @var AbstractEnum $instance */
        $instance = clone $defaultInstance;
        $instance->set($value);

        return $instance;
    }

    /**
     * Get value in readable format.
     *
     * @param string $value ENUM value
     *
     * @static
     *
     * @return string|null $value Value in readable format
     *
     * @throws \InvalidArgumentException
     */
    public static function getReadableValue($value): ?string
    {
        if (!static::has($value)) {
            throw new \InvalidArgumentException(
                sprintf('Invalid value "%s" for ENUM type "%s".', $value, get_called_class())
            );
        }

        return $value;
    }

    /**
     * Get array of ENUM Values, where ENUM values are keys and their readable versions are values.
     *
     * @static
     *
     * @return array Array of values with readable format
     */
    public static function getReadableValues(): array
    {
        return static::toArray();
    }

    /**
     * Do getReflect
     *
     * @param string $class
     *
     * @return \ReflectionClass
     */
    public static function getReflect(string $class): \ReflectionClass
    {
        if (!static::$reflects || !array_key_exists($class, static::$reflects)) {
            static::$reflects[$class] = new \ReflectionClass($class);
        }

        return static::$reflects[$class];
    }

    /**
     * Get values for the ENUM field.
     *
     * @static
     *
     * @return array Values for the ENUM field
     */
    public static function getValues(): array
    {
        return static::toArray();
    }

    /**
     * @param mixed $value
     *
     * @return bool
     */
    public static function has($value): bool
    {
        return in_array($value, static::getReflect(static::class)->getConstants(), true);
    }

    /**
     * Check if some string value exists in the array of ENUM values.
     *
     * @param string $value ENUM value
     *
     * @static
     *
     * @return bool
     */
    public static function isValueExist($value): bool
    {
        return static::has($value);
    }

    /**
     * @return array
     */
    public static function toArray(): array
    {
        if (!static::$arrays || !array_key_exists(static::class, static::$arrays)) {
            $array = array();

            foreach (static::getCurrentConstants() as $key => $constant) {
                if ($key !== '__DEFAULT') {
                    $array[$key] = $constant;
                }
            }

            static::$arrays[static::class] = $array;
        }

        return static::$arrays[static::class];
    }

    /**
     * Return random value
     *
     * @return string
     */
    public static function randomEnum(): string
    {
        return static::toArray()[array_rand(static::toArray())];
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->get();
    }

    /**
     * {@inheritdoc}
     * @throws \InvalidArgumentException
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return null;
        }

        if (!$value instanceof AbstractEnum) {
            throw new \InvalidArgumentException(sprintf('Invalid value "%s" for ENUM "%s".', $value, $this->getName()));
        }

        return $value->get();
    }

    /**
     * {@inheritdoc}
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value !== null) {
            return static::getNewInstance($value);
        }

        return $value;
    }

    /**
     * Get the value
     *
     * @return mixed
     */
    public function get()
    {
        return $this->value;
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return (new \ReflectionClass(get_class($this)))->getShortName();
    }

    /**
     * {@inheritdoc}
     */
    public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        $values = implode(
            ', ',
            array_map(
                function ($value) {
                    return "'{$value}'";
                },
                static::getValues()
            )
        );

        if ($platform instanceof SqlitePlatform) {
            if (!$fieldDeclaration['notnull']) {
                $values .= ', NULL';
            }

            return sprintf('TEXT CHECK(%s IN (%s))', $fieldDeclaration['name'], $values);
        }

        if ($platform instanceof PostgreSqlPlatform || $platform instanceof SQLServerPlatform) {
            if (!$fieldDeclaration['notnull']) {
                $values .= ', NULL';
            }

            return sprintf('VARCHAR(255) CHECK(%s IN (%s))', $fieldDeclaration['name'], $values);
        }

        return sprintf('ENUM(%s)', $values);
    }

    /**
     * {@inheritdoc}
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * @param mixed $value
     *
     * @return AbstractEnum
     * @throws \UnexpectedValueException
     */
    public function set($value): self
    {
        if (!$value) {
            $value = static::__DEFAULT;
        } elseif (!static::has($value)) {
            throw new \UnexpectedValueException(
                sprintf(
                    'Invalid value "%s" for ENUM type "%s".',
                    $value,
                    get_called_class()
                )
            );
        }

        $this->value = $value;

        return $this;
    }
}
