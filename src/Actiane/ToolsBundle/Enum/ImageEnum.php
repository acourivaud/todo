<?php


namespace Actiane\ToolsBundle\Enum;


class ImageEnum extends AbstractEnum
{
    public const THUMBNAIL_INSET = 'inset';
    public const THUMBNAIL_OUTBOUND = 'outbound';
}
