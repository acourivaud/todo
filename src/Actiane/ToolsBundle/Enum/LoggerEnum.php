<?php
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 09/01/2017
 * Time: 16:27
 *
 * @category   LaTribune - CMS
 * @package    Actiane\ToolsBundle
 * @subpackage Actiane\ToolsBundle\Enum
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
declare(strict_types = 1);

namespace Actiane\ToolsBundle\Enum;

/**
 * Class LoggerEnum
 *
 * @category   LaTribune - CMS
 * @package    Actiane\ToolsBundle
 * @subpackage Actiane\ToolsBundle\Enum
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class LoggerEnum extends AbstractEnum
{
    const ALERT = 'alert';
    const CRITICAL = 'critical';
    const DEBUG = 'debug';
    const EMERGENCY = 'emergency';
    const ERROR = 'error';
    const INFO = 'info';
    const NOTICE = 'notice';
    const WARNING = 'warning';
    const __DEFAULT = self::DEBUG;
}
