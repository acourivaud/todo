<?php
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 09/01/2017
 * Time: 15:55
 *
 * @category   LaTribune - CMS
 * @package    Actiane\ToolsBundle
 * @subpackage Actiane\ToolsBundle\Traits
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
declare(strict_types = 1);

namespace Actiane\ToolsBundle\Traits;

use Actiane\ToolsBundle\Enum\LoggerEnum;
use Monolog\Logger;
use Psr\Log\InvalidArgumentException;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Trait LoggerTrait
 *
 * @category   LaTribune - CMS
 * @package    Actiane\ToolsBundle
 * @subpackage Actiane\ToolsBundle\Traits
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
trait LoggerTrait
{
    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var OutputInterface
     */
    protected $output;

    /**
     * @var string
     */
    protected $prefix;

    /**
     * Get logger
     *
     * @return Logger
     */
    public function getLogger(): Logger
    {
        return $this->logger;
    }

    /**
     * Set logger
     *
     * @param Logger $logger
     *
     * @return LoggerTrait
     */
    public function setLogger(Logger $logger): self
    {
        $this->logger = $logger;

        return $this;
    }

    /**
     * Get output
     *
     * @return OutputInterface
     */
    public function getOutput(): OutputInterface
    {
        return $this->output;
    }

    /**
     * Set output
     *
     * @param OutputInterface $output
     *
     * @return LoggerTrait
     */
    public function setOutput(OutputInterface $output = null): self
    {
        $this->output = $output;

        return $this;
    }

    /**
     * Get prefix
     *
     * @return string
     */
    public function getPrefix(): string
    {
        if (!$this->prefix) {
            $reflect = new \ReflectionClass($this);
            $this->prefix = $reflect->getShortName();
        }

        return $this->prefix;
    }

    /**
     * Set prefix
     *
     * @param string $prefix
     *
     * @return LoggerTrait
     */
    public function setPrefix(string $prefix = null): self
    {
        $this->prefix = $prefix;

        return $this;
    }

    /**
     * Do log
     *
     * @param string $status
     * @param string $message
     * @param string $exceptionClass
     *
     * @return LoggerTrait
     * @throws InvalidArgumentException
     */
    protected function log(string $status, string $message, string $exceptionClass = null): self
    {
        if (!LoggerEnum::has($status)) {
            $this->logger->emerg('Log status ' . $status . ' doesn\'t existe ! Fix it ASAP !');
            $this->logger->emerg($message);
            throw new InvalidArgumentException('Status ' . $status . ' doesn\'t existe');
        }

        $this->logger->$status($message);

        if ($this->output) {
            $str = '<' . $status . '>[' . strtoupper($status) . ']';
            if ($this->getPrefix()) {
                $str .= '[' . $this->getPrefix() . ']';
            }
            $this->output->writeln($str . $message . '</' . $status . '>');
        }

        if ($exceptionClass) {
            throw new $exceptionClass($message);
        }

        return $this;
    }
}
