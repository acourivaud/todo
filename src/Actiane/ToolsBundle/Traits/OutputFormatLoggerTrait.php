<?php
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 09/01/2017
 * Time: 16:04
 *
 * @category   LaTribune - CMS
 * @package    Actiane\ToolsBundle
 * @subpackage Actiane\ToolsBundle\Traits
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
declare(strict_types = 1);

namespace Actiane\ToolsBundle\Traits;

use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Trait OutputFormatLoggerTrait
 *
 * @category   LaTribune - CMS
 * @package    Actiane\ToolsBundle
 * @subpackage Actiane\ToolsBundle\Traits
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
trait OutputFormatLoggerTrait
{
    /**
     * Do initFormater
     *
     * @param OutputInterface $output
     *
     * @return OutputFormatLoggerTrait
     */
    protected function initFormat(OutputInterface $output): self
    {
        $debug = new OutputFormatterStyle('magenta');
        $info = new OutputFormatterStyle('blue');
        $notice = new OutputFormatterStyle('cyan');
        $warning = new OutputFormatterStyle('yellow');
        $error = new OutputFormatterStyle('red', null, array('bold', 'blink'));
        $critical = new OutputFormatterStyle('red', 'yellow', array('bold', 'blink'));
        $alert = new OutputFormatterStyle('red', 'black', array('bold', 'blink'));
        $emergency = new OutputFormatterStyle('black', 'red', array('bold', 'blink'));

        $formatter = $output->getFormatter();
        $formatter->setStyle('debug', $debug);
        $formatter->setStyle('info', $info);
        $formatter->setStyle('notice', $notice);
        $formatter->setStyle('warning', $warning);
        $formatter->setStyle('error', $error);
        $formatter->setStyle('critical', $critical);
        $formatter->setStyle('alert', $alert);
        $formatter->setStyle('emergency', $emergency);

        return $this;
    }
}
