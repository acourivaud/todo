<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 18/03/17
 * Time: 09:50
 */

namespace Actiane\ToolsBundle\Exceptions;

/**
 * Interface PublishedMessageExceptionInterface
 * @package Actiane\ToolsBundle\Exceptions
 */
interface PublishedMessageExceptionInterface
{
    /**
     * @return mixed
     */
    public function getMessages();

    /**
     * @return int
     */
    public function getCustomCode(): int;
}
