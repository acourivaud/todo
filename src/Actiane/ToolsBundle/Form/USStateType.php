<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 08/06/17
 * Time: 09:33
 */

namespace Actiane\ToolsBundle\Form;

use Actiane\ToolsBundle\Enum\USStateEnum;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class USStateType
 * @package Actiane\ToolsBundle\Form
 */
class USStateType extends AbstractType
{
    /**
     * @param OptionsResolver $resolver
     *
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            array(
                'choices' => array_flip(USStateEnum::toArray()),
                'expanded' => false,
                'multiple' => false,
            )
        );
    }

    /**
     * @return string
     */
    public function getParent(): string
    {
        return ChoiceType::class;
    }
}
