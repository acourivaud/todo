<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 13/12/17
 * Time: 17:55
 *
 * @category   Todo-Todev
 *
 * @package    Actiane\ToolsBundle\Form
 *
 * @subpackage Actiane\ToolsBundle\Form
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Actiane\ToolsBundle\Form;

use Actiane\ToolsBundle\Services\AssetManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DateTimePickerType
 */
class DateTimePickerType extends AbstractType
{
    /**
     * @var AssetManager
     */
    private $assetManager;

    /**
     * DateTimePickerType constructor.
     *
     * @param AssetManager $assetManager
     */
    public function __construct(AssetManager $assetManager)
    {
        $this->assetManager = $assetManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $this->assetManager->addJavascript('build/dateTimePickerType.js');
    }

    /**
     * @param FormView      $view
     * @param FormInterface $form
     * @param array         $options
     */
    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        $view->vars['time'] = $options['time'];
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefault('time', false)->setAllowedTypes('time', 'boolean');
    }

    /**
     * @return string
     */
    public function getParent(): string
    {
        return TextType::class;
    }
}
