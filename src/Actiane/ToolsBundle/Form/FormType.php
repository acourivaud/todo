<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 28/04/2017
 * Time: 11:53
 */

namespace Actiane\ToolsBundle\Form;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\FormType as BaseFormType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class FormType
 *
 * @category   Todo-Todev
 * @package    Actiane\ToolsBundle\Form
 * @subpackage Actiane\ToolsBundle\Form
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class FormType extends AbstractTypeExtension
{
    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        $view->vars['is_vue_template'] = $options['is_vue_template'];
    }

    /**
     * {@inheritdoc}
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefault('is_vue_template', false);
    }

    /**
     * {@inheritdoc}
     */
    public function getExtendedType(): string
    {
        return BaseFormType::class;
    }
}
