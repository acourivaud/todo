<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 16/01/18
 * Time: 16:56
 *
 * @category   tdtd
 *
 * @package    Actiane\ToolsBundle\Form
 *
 * @subpackage Actiane\ToolsBundle\Form
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Actiane\ToolsBundle\Form;

use Actiane\ToolsBundle\Services\AssetManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Regex;

/**
 * Class TimePickerType
 */
class TimePickerType extends AbstractType
{
    /**
     * @var AssetManager
     */
    private $assetManager;

    /**
     * TimePickerType constructor.
     *
     * @param AssetManager $assetManager
     */
    public function __construct(AssetManager $assetManager)
    {
        $this->assetManager = $assetManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @throws \Symfony\Component\Validator\Exception\MissingOptionsException
     * @throws \Symfony\Component\Validator\Exception\InvalidOptionsException
     * @throws \Symfony\Component\Validator\Exception\ConstraintDefinitionException
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $this->assetManager->addJavascript('build/timePickerType.js');
    }

    /**
     * @return string
     */
    public function getParent(): string
    {
        return TextType::class;
    }
}
