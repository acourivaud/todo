<?php
declare(strict_types = 1);

/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 25/11/2016
 * Time: 11:05
 *
 * @category   Actiane - TOOLS
 * @package    Actiane\ToolsBundle
 * @subpackage Actiane\ToolsBundle\Form\Type\Custom
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */

namespace Actiane\ToolsBundle\Form;

use Actiane\ToolsBundle\Interfaces\FormApiTypeInterface;
use Actiane\ToolsBundle\Services\AssetManager;
use Actiane\ToolsBundle\Services\FormApiTypeManager;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Bridge\Doctrine\Form\Type\EntityType as BaseEntityType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\Exception\AccessException;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class EntityType
 *
 * @category   Actiane - TOOLS
 * @package    Actiane\ToolsBundle
 * @subpackage Actiane\ToolsBundle\Form\Type\Custom
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class EntityType extends BaseEntityType
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    protected $formApiTypeManager;

    /**
     * EntityType constructor.
     *
     * @param ManagerRegistry    $registry
     * @param AssetManager       $assetExtension
     * @param ContainerInterface $container
     * @param FormApiTypeManager $formApiTypeManager
     */
    public function __construct(
        ManagerRegistry $registry,
        AssetManager $assetExtension,
        ContainerInterface $container,
        FormApiTypeManager $formApiTypeManager
    ) {
        parent::__construct($registry);

        $this->container = $container;
        $this->formApiTypeManager = $formApiTypeManager;

        $assetExtension
            ->addJavascript('/bundles/actianetools/js/entity_custom.js')
            ->addBlock('@ActianeTools/form/block/form_generic_modal.html.twig');
    }

    /**
     * {@inheritdoc}
     * @throws ServiceCircularReferenceException
     * @throws ServiceNotFoundException
     * @throws \LogicException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view, $form, $options);

        $view->vars['buttons'] = $options['buttons'];
        $view->vars['routes'] = $options['routes'];
        $view->vars['forms_id'] = $options['forms_id'];
        $view->vars['form_edit_id'] = $options['form_edit_id'];

        /** @var FormApiTypeInterface $formType */
        $formType = $this->container->get($options['form_edit_id']);
        $reflect = new \ReflectionClass($formType);
        if (!$reflect->implementsInterface(FormApiTypeInterface::class)) {
            throw new \LogicException(
                'You have to implement the interface "FormApiTypeInterface" in your ' . $reflect->getName()
            );
        }
        $formApiOptions = $this->formApiTypeManager->resolveOptions($formType->getFormApiConf());
        $view->vars['roles'] = $formApiOptions['roles'];
    }

    /**
     * {@inheritdoc}
     * @throws AccessException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults(
            array(
                'buttons' => array(
                    'allow' => array(
                        'add' => true,
                        'edit' => true,
                        'delete' => true,
                    ),
                    'icon' => array(
                        'add' => 'icon-plus icon-white',
                        'edit' => 'icon-pencil icon-white',
                        'delete' => 'icon-trash icon-white',
                    ),
                    'class' => array(
                        'add' => 'btn btn-success btn-small tooltiped',
                        'edit' => 'btn btn-primary btn-small tooltiped',
                        'delete' => 'btn btn-danger btn-small tooltiped',
                    ),
                    'tooltip' => array(
                        'add' => 'Ajouter',
                        'edit' => 'Editer',
                        'delete' => 'Suprimer',
                    ),
                ),
                'routes' => array(
                    'add' => 'actiane.tools.api.entity_type.new_type_entity',
                    'edit' => 'actiane.tools.api.entity_type.edit_type_entity',
                    'delete' => 'actiane.tools.api.entity_type.edit_type_entity',
                ),
            )
        );

        $resolver->setRequired('forms_id');
        $resolver->setRequired('form_edit_id');

        $resolver->setAllowedTypes('forms_id', 'array');
        $resolver->setAllowedTypes('form_edit_id', 'string');
        $resolver->setAllowedTypes('buttons', 'array');
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'actiane_tools_entity';
    }
}
