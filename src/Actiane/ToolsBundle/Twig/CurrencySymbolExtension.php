<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: riwan
 * Date: 12/06/17
 * Time: 12:11
 */

namespace Actiane\ToolsBundle\Twig;

use Symfony\Component\Intl\Intl;

class CurrencySymbolExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFunctions(): array
    {
        return array(
            'currency_symbol' => new \Twig_SimpleFunction('currency_symbol', array($this, 'currencySymbol')),
        );
    }

    /**
     * @param string $currencyShortName
     *
     * @return null|string
     */
    public function currencySymbol(string $currencyShortName): ?string
    {
        return Intl::getCurrencyBundle()->getCurrencySymbol($currencyShortName);
    }
}
