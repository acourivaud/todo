<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 28/04/2017
 * Time: 08:42
 */

namespace Actiane\ToolsBundle\Twig;

use Symfony\Component\OptionsResolver\Exception as ORException;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class VueExtension
 *
 * @category   Todo-Todev
 * @package    Actiane\ToolsBundle\Twig
 * @subpackage Actiane\ToolsBundle\Twig
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class VueExtension extends \Twig_Extension
{
    /**
     *{@inheritdoc}
     */
    public function getFilters(): array
    {
        return array(
            new \Twig_SimpleFilter(
                'prototype_to_vue',
                array($this, 'prototypeToVue'),
                array(
                    'is_safe' => array('html'),
                )
            ),
        );
    }

    /**
     * Do prototypeToVue
     *
     * @param string $str
     * @param array  $options
     *
     * @return string
     * @throws ORException\UndefinedOptionsException
     * @throws ORException\OptionDefinitionException
     * @throws ORException\NoSuchOptionException
     * @throws ORException\MissingOptionsException
     * @throws ORException\InvalidOptionsException
     * @throws ORException\AccessException
     */
    public function prototypeToVue(string $str, array $options = array()): string
    {
        $optionsResolver = new OptionsResolver();
        $this->prototypeToVueConfigureOptions($optionsResolver);
        $options = $optionsResolver->resolve($options);

        $str = str_replace(
            array(
                $options['prototype_name_label_source'],
                $options['prototype_name_source'],
                $options['v_model_source'],
            ),
            array(
                $options['prototype_name_label_target'],
                $options['prototype_name_target'],
                $options['v_model_target'],
            ),
            $str
        );

        return $str;
    }

    /**
     * Do prototypeToVueConfigureOptions
     *
     * @param OptionsResolver $optionsResolver
     *
     * @return VueExtension
     * @throws ORException\AccessException
     */
    protected function prototypeToVueConfigureOptions(OptionsResolver $optionsResolver): self
    {
        $optionsResolver->setDefaults(
            array(
                'v_model_source' => 'v-model="',
                'v_model_target' => 'v-model="item.',
                'prototype_name_source' => '__name__',
                'prototype_name_target' => '\' + key + \'',
                'prototype_name_label_source' => '__name__label__',
                'prototype_name_label_target' => '\' + key + \'',
            )
        );

        return $this;
    }
}
