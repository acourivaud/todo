<?php
declare(strict_types = 1);

/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 25/11/2016
 * Time: 14:47
 *
 * @category   Actiane - TOOLS
 * @package    Actiane\ToolsBundle
 * @subpackage Actiane\ToolsBundle\Twig
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */

namespace Actiane\ToolsBundle\Twig;

use Actiane\ToolsBundle\Services\AssetManager;

/**
 * Class AssetExtension
 *
 * @category   Actiane - TOOLS
 * @package    Actiane\ToolsBundle
 * @subpackage Actiane\ToolsBundle\Twig
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class AssetExtension extends \Twig_Extension
{
    /**
     * @var string[]
     */
    protected $assetManager;

    /**
     * AssetExtension constructor.
     *
     * @param AssetManager $assetManager
     */
    public function __construct(AssetManager $assetManager)
    {
        $this->assetManager = $assetManager;
    }

    /**
     * Do addBlockTemplate
     *
     * @param string $blockName
     */
    public function addBlockTemplate(string $blockName): void
    {
        $this->assetManager->addBlockTemplate($blockName);
    }

    /**
     * Do addJavascript
     *
     * @param string $path
     */
    public function addCss(string $path): void
    {
        $this->assetManager->addCss($path);
    }

    /**
     * Do addExtra
     *
     * @param string $extra
     */
    public function addExtra(string $extra): void
    {
        $this->assetManager->addExtra($extra);
    }

    /**
     * Do addJavascript
     *
     * @param string $path
     */
    public function addJavascript(string $path): void
    {
        $this->assetManager->addJavascript($path);
    }

    /**
     * Do addTemplate
     *
     * @param string $key
     * @param string $blockName
     */
    public function addTemplate(string $key, string $blockName): void
    {
        $this->assetManager->addTemplate($key, $blockName);
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions(): array
    {
        return array(
            new \Twig_SimpleFunction('add_javascript', array($this, 'addJavascript')),
            new \Twig_SimpleFunction('add_css', array($this, 'addCss')),
            new \Twig_SimpleFunction('add_block', array($this, 'addBlock')),
            new \Twig_SimpleFunction('add_template', array($this, 'addTemplate')),
            new \Twig_SimpleFunction('add_block_template', array($this, 'addBlockTemplate')),
            new \Twig_SimpleFunction('add_extra', array($this, 'addExtra')),
            new \Twig_SimpleFunction(
                'render_javascript',
                array($this, 'renderJavascript'),
                array(
                    'needs_environment' => true,
                    'is_safe' => array('html'),
                )
            ),
            new \Twig_SimpleFunction(
                'render_css',
                array($this, 'renderCss'),
                array(
                    'needs_environment' => true,
                    'is_safe' => array('html'),
                )
            ),
            new \Twig_SimpleFunction(
                'render_block',
                array($this, 'renderBlock'),
                array(
                    'needs_environment' => true,
                    'is_safe' => array('html'),
                )
            ),
            new \Twig_SimpleFunction(
                'render_template',
                array($this, 'renderTemplate'),
                array(
                    'needs_environment' => true,
                    'is_safe' => array('html'),
                )
            ),
            new \Twig_SimpleFunction(
                'render_block_template',
                array($this, 'renderBlockTemplate'),
                array(
                    'needs_environment' => true,
                    'is_safe' => array('html'),
                )
            ),
            new \Twig_SimpleFunction(
                'render_extra',
                array($this, 'renderExtra'),
                array(
                    'needs_environment' => true,
                    'is_safe' => array('html'),
                )
            ),
        );
    }

    /**
     * Do renderBlock
     *
     * @param \Twig_Environment $env
     *
     * @return string
     * @throws \Twig_Error_Syntax
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Loader
     */
    public function renderBlock(\Twig_Environment $env): string
    {
        return $env->render(
            '@ActianeTools/AssetsExtension/block.html.twig',
            array('blocks' => $this->assetManager->getBlocks())
        );
    }

    /**
     * Do renderBlockTemplate
     *
     * @param \Twig_Environment $env
     *
     * @return string
     * @throws \Twig_Error_Syntax
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Loader
     */
    public function renderBlockTemplate(\Twig_Environment $env): string
    {
        return $env->render(
            '@ActianeTools/AssetsExtension/block_template.html.twig',
            array('block_templates' => $this->assetManager->getBlockTemplates())
        );
    }

    /**
     * Do renderCss
     *
     * @param \Twig_Environment $env
     *
     * @return string
     * @throws \Twig_Error_Syntax
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Loader
     */
    public function renderCss(\Twig_Environment $env): string
    {
        return $env->render(
            '@ActianeTools/AssetsExtension/css.html.twig',
            array('csses' => $this->assetManager->getCss())
        );
    }

    /**
     * Do renderExtra
     *
     * @param \Twig_Environment $env
     *
     * @return string
     * @throws \Twig_Error_Syntax
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Loader
     */
    public function renderExtra(\Twig_Environment $env): string
    {
        return $env->render(
            '@ActianeTools/AssetsExtension/extras.html.twig',
            array('extras' => $this->assetManager->getExtras())
        );
    }

    /**
     * Do renderJavascript
     *
     * @param \Twig_Environment $env
     *
     * @return string
     * @throws \Twig_Error_Syntax
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Loader
     */
    public function renderJavascript(\Twig_Environment $env): string
    {
        return $env->render(
            '@ActianeTools/AssetsExtension/javascript.html.twig',
            array('javascripts' => $this->assetManager->getJavascript())
        );
    }

    /**
     * Do renderTemplate
     *
     * @param \Twig_Environment $env
     *
     * @return string
     * @throws \Twig_Error_Syntax
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Loader
     */
    public function renderTemplate(\Twig_Environment $env): string
    {
        return $env->render(
            '@ActianeTools/AssetsExtension/template.html.twig',
            array('templates' => $this->assetManager->getTemplates())
        );
    }
}
