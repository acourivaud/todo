<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 01/09/17
 * Time: 17:14
 */

namespace Actiane\ToolsBundle\Twig;

/**
 * Class JsonDecodeExtension
 *
 * @package Actiane\ToolsBundle\Twig
 */
class JsonDecodeExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFunctions(): array
    {
        return [
            new \Twig_SimpleFunction('json_decode', [$this, 'jsonDecode']),
        ];
    }

    /**
     * @param string $json
     *
     * @return array
     */
    public function jsonDecode(string $json): array
    {
        return json_decode($json, true);
    }
}
