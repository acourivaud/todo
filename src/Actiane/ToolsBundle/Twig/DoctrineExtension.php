<?php
declare(strict_types = 1);

/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 16/11/2016
 * Time: 16:13
 *
 * @category   Actiane - TOOLS
 * @package    Actiane\ToolsBundle
 * @subpackage Actiane\ToolsBundle\Twig
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */

namespace Actiane\ToolsBundle\Twig;

use Doctrine\DBAL\Exception\InvalidArgumentException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadataInfo;

/**
 * Class DoctrineExtension
 *
 * @category   Actiane - TOOLS
 * @package    Actiane\ToolsBundle
 * @subpackage Actiane\ToolsBundle\Twig
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class DoctrineExtension extends \Twig_Extension
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * DoctrineExtension constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Do getEntityDiscriminator
     *
     * @param object $entity
     *
     * @return string
     * @throws InvalidArgumentException
     */
    public function getEntityDiscriminator($entity)
    {
        $reflect = new \ReflectionObject($entity);
        /** @var ClassMetadataInfo $meta */
        $meta = $this->entityManager->getClassMetadata($reflect->getName());

        if (!$discriminator = array_search($reflect->getName(), $meta->discriminatorMap, true)) {
            throw new InvalidArgumentException();
        }

        return $discriminator;
    }

    /**
     * Do getFunctions
     *
     * @return \Twig_SimpleFunction[]
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('get_entity_discriminator', array($this, 'getEntityDiscriminator')),
        );
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'actiane.tools.twig.doctrine_extention';
    }
}
