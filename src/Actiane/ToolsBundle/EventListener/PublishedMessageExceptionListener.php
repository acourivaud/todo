<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 18/03/17
 * Time: 10:04
 */

namespace Actiane\ToolsBundle\EventListener;

use Actiane\ToolsBundle\Exceptions\PublishedMessageExceptionInterface;
use Actiane\ToolsBundle\Exceptions\UserInputExceptionInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

/**
 * Class PublishedMessageExceptionListener
 * @package Actiane\ToolsBundle\EventListener
 */
class PublishedMessageExceptionListener
{
    /**
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        if (!$exception instanceof PublishedMessageExceptionInterface) {
            return;
        }

        $code = $exception->getCustomCode();

        $responseData = [
            'error' => [
                'code' => $code,
                'message' => $exception->getMessage(),
            ],
        ];

        $event->setResponse(new JsonResponse($responseData, $code));
    }
}
