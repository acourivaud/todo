<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 26/10/17
 * Time: 11:52
 *
 * @category   Todo-Todev
 *
 * @package    Actiane\ToolsBundle\Services
 *
 * @subpackage Actiane\ToolsBundle\Services
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Actiane\ToolsBundle\Services;

use Actiane\ToolsBundle\Enum\ImageEnum;
use Gaufrette\File;
use Imagine\Filter\Basic\Autorotate;
use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Imagine\Image\ImagineInterface;
use InvalidArgumentException;
use Sonata\MediaBundle\Metadata\MetadataBuilderInterface;
use Sonata\MediaBundle\Model\MediaInterface;
use Sonata\MediaBundle\Resizer\ResizerInterface;

/**
 * Class SonataMediaFixedResizer
 */
class SonataMediaFixedResizer implements ResizerInterface
{
    /**
     * @var ImagineInterface
     */
    private $adapter;

    /**
     * @var string
     */
    private $mode;

    /**
     * @var MetadataBuilderInterface
     */
    private $metadata;

    /**
     * SonataMediaFixedResizer constructor.
     *
     * @param ImagineInterface         $adapter
     * @param string                   $mode
     * @param MetadataBuilderInterface $metadata
     */
    public function __construct(ImagineInterface $adapter, string $mode, MetadataBuilderInterface $metadata)
    {
        $this->adapter = $adapter;
        $this->mode = $mode;
        $this->metadata = $metadata;
    }

    /**
     * @param MediaInterface $media
     * @param File           $in
     * @param File           $out
     * @param string         $format
     * @param array          $settings
     *
     * @throws \Gaufrette\Exception\FileNotFound
     * @throws \RuntimeException
     * @throws \Imagine\Exception\RuntimeException
     * @throws \Imagine\Exception\InvalidArgumentException
     */
    public function resize(MediaInterface $media, File $in, File $out, $format, array $settings): void
    {
        if (!(isset($settings['width']) && $settings['width'])) {
            throw new \RuntimeException(
                sprintf(
                    'Width parameter is missing in context "%s" for provider "%s"',
                    $media->getContext(),
                    $media->getProviderName()
                )
            );
        }

        $image = $this->adapter->load($in->getContent());

        $image = $image->thumbnail($this->getBox($media, $settings), $this->mode);
        $autorotate = new Autorotate();
        $image = $autorotate->apply($image);

        $content = $image->get($format, array('quality' => $settings['quality']));

        $out->setContent($content, $this->metadata->get($media, $out->getName()));
    }

    /**
     * @param MediaInterface $media
     * @param array          $settings
     *
     * @return Box
     * @throws \Imagine\Exception\InvalidArgumentException
     * @throws \RuntimeException
     * @throws InvalidArgumentException
     */
    public function getBox(MediaInterface $media, array $settings): Box
    {
        $size = $media->getBox();
        $hasWidth = isset($settings['width']) && $settings['width'];
        $hasHeight = isset($settings['height']) && $settings['height'];

        if (!$hasWidth && !$hasHeight) {
            throw new \RuntimeException(
                sprintf(
                    'Width/Height parameter is missing
                     in context "%s" for provider "%s". Please add at least one parameter.',
                    $media->getContext(),
                    $media->getProviderName()
                )
            );
        }

        if ($hasWidth && $hasHeight) {
            return new Box($settings['width'], $settings['height']);
        }

        if (!$hasHeight) {
            $settings['height'] = (int) ($settings['width'] * $size->getHeight() / $size->getWidth());
        }

        if (!$hasWidth) {
            $settings['width'] = (int) ($settings['height'] * $size->getWidth() / $size->getHeight());
        }

        return $this->computeBox($media, $settings);
    }

    /**
     * @throws InvalidArgumentException
     *
     * @param MediaInterface $media
     * @param array          $settings
     *
     * @return Box
     */
    private function computeBox(MediaInterface $media, array $settings): Box
    {
        if ($this->mode !== ImageEnum::THUMBNAIL_INSET && $this->mode !== ImageEnum::THUMBNAIL_OUTBOUND) {
            throw new InvalidArgumentException('Invalid mode specified');
        }

        $size = $media->getBox();

        $ratios = [
            $settings['width'] / $size->getWidth(),
            $settings['height'] / $size->getHeight(),
        ];

        if ($this->mode === ImageEnum::THUMBNAIL_INSET) {
            $ratio = min($ratios);
        } else {
            $ratio = max($ratios);
        }

        return $size->scale($ratio);
    }
}
