<?php
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 04/01/2017
 * Time: 11:22
 *
 * @category   LaTribune - CMS
 * @package    Actiane\ToolsBundle
 * @subpackage Actiane\ToolsBundle\Services
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
declare(strict_types = 1);

namespace Actiane\ToolsBundle\Services;

use Actiane\ToolsBundle\Interfaces\ImportFormApiTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\Exception as ORException;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class FormApiTypeManager
 *
 * @category   LaTribune - CMS
 * @package    Actiane\ToolsBundle
 * @subpackage Actiane\ToolsBundle\Services
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class FormApiTypeManager
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * FormApiTypeManager constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Do processImports
     *
     * @param Request $request
     * @param object  $data
     * @param array   $imports
     * @param array   $options
     *
     * @return array|FormInterface[]
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Exception
     * @throws \RuntimeException
     */
    public function processImports(Request $request, $data, array &$imports, array &$options = array()): array
    {
        $forms = array();

        foreach ($imports as $import) {
            /** @var ImportFormApiTypeInterface $service */
            if (!(($service = $this->container->get($import)) instanceof ImportFormApiTypeInterface)) {
                throw new \RuntimeException(
                    'You have to implement ImportFormApiTypeInterface in service ' . $import,
                    500
                );
            }
            $forms[] = $service->process($request, $data, $options)->createView();
        }

        return $forms;
    }

    /**
     * Do resolveOptions
     *
     * @param array $options
     *
     * @return array
     * @throws ORException\UndefinedOptionsException
     * @throws ORException\OptionDefinitionException
     * @throws ORException\NoSuchOptionException
     * @throws ORException\MissingOptionsException
     * @throws ORException\InvalidOptionsException
     * @throws ORException\AccessException
     */
    public function resolveOptions(array $options): array
    {
        $optionsResolver = new OptionsResolver();
        $this->configureOptions($optionsResolver);

        return $optionsResolver->resolve($options);
    }

    /**
     * Do configureOptions
     *
     * @param OptionsResolver $optionsResolver
     *
     * @return FormApiTypeManager
     * @throws ORException\UndefinedOptionsException
     * @throws ORException\AccessException
     */
    protected function configureOptions(OptionsResolver $optionsResolver): self
    {
        $optionsResolver->setRequired('data_class')
            ->setAllowedTypes('data_class', 'string')
            ->setRequired('roles')
            ->setAllowedTypes('roles', 'array')
            ->setDefault('imports', array())
            ->setAllowedTypes('imports', 'array');

        return $this;
    }
}
