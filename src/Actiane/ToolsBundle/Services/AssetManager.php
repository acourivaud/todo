<?php
declare(strict_types = 1);

/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 25/11/2016
 * Time: 18:28
 *
 * @category   Actiane - TOOLS
 * @package    Actiane\ToolsBundle\Services
 * @subpackage Actiane\ToolsBundle\Services
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */

namespace Actiane\ToolsBundle\Services;

/**
 * Class AssetManager
 *
 * @category   Actiane - TOOLS
 * @package    Actiane\ToolsBundle\Services
 * @subpackage Actiane\ToolsBundle\Services
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class AssetManager
{
    /**
     * @var array|string[]
     */
    protected $javascripts;

    /**
     * @var array|string[]
     */
    protected $css;

    /**
     * @var array|string[]
     */
    protected $templates;

    /**
     * @var array|string[]
     */
    protected $blocks;

    /**
     * @var string[]
     */
    protected $blockTemplates;

    /**
     * @var string[]
     */
    protected $extra;

    /**
     * AssetExtension constructor.
     */
    public function __construct()
    {
        $this->javascripts = array();
        $this->css = array();
        $this->templates = array();
        $this->blocks = array();
        $this->blockTemplates = array();
        $this->extras = array();
    }

    /**
     * Do addBlock
     *
     * @param string $blockName
     *
     * @return AssetManager
     */
    public function addBlock(string $blockName): self
    {
        if (!array_key_exists($blockName, $this->templates)) {
            $this->blocks[$blockName] = $blockName;
        }

        return $this;
    }

    /**
     * Do addBlockTemplate
     *
     * @param string $blockName
     *
     * @return AssetManager
     */
    public function addBlockTemplate(string $blockName): self
    {
        if (!array_key_exists($blockName, $this->blockTemplates)) {
            $this->blockTemplates[$blockName] = $blockName;
        }

        return $this;
    }

    /**
     * Do addJavascript
     *
     * @param string $path
     *
     * @return AssetManager
     */
    public function addCss(string $path): self
    {
        if (!array_key_exists($path, $this->css)) {
            $this->css[$path] = $path;
        }

        return $this;
    }

    /**
     * Do addExtra
     *
     * @param string $extra
     *
     * @return AssetManager
     */
    public function addExtra(string $extra): self
    {
        if (!in_array($extra, $this->extras, true)) {
            $this->extras[] = $extra;
        }

        return $this;
    }

    /**
     * Do addJavascript
     *
     * @param string $path
     *
     * @return AssetManager
     */
    public function addJavascript(string $path): self
    {
        if (!array_key_exists($path, $this->javascripts)) {
            $this->javascripts[$path] = $path;
        }

        return $this;
    }

    /**
     * Do addTemplate
     *
     * @param string $key
     * @param string $blockName
     *
     * @return AssetManager
     */
    public function addTemplate(string $key, string $blockName): self
    {
        if (!array_key_exists($key, $this->templates)) {
            $this->templates[$key] = $blockName;
        }

        return $this;
    }

    /**
     * Do getBlockTemplates
     *
     * @return string[]
     */
    public function getBlockTemplates(): array
    {
        return $this->blockTemplates;
    }

    /**
     * Do getBlocks
     *
     * @return array|string[]
     */
    public function getBlocks(): array
    {
        return $this->blocks;
    }

    /**
     * Do getCss
     *
     * @return array|string[]
     */
    public function getCss(): array
    {
        return $this->css;
    }

    /**
     * Do getExtras
     *
     * @return array
     */
    public function getExtras(): array
    {
        return $this->extras;
    }

    /**
     * Do getJavascript
     *
     * @return array|string[]
     */
    public function getJavascript(): array
    {
        return $this->javascripts;
    }

    /**
     * Do getTemplates
     *
     * @return array|string[]
     */
    public function getTemplates(): array
    {
        return $this->templates;
    }
}
