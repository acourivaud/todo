<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 06/03/17
 * Time: 22:58
 */

namespace Actiane\ToolsBundle\Serializer;

use Actiane\ToolsBundle\Enum\AbstractEnum;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\JsonSerializationVisitor;

/**
 * Class MediaHandler
 * @package Actiane\ToolsBundle\Serializer
 */
class EnumHandler implements SubscribingHandlerInterface
{
    /**
     * Return format:
     *
     *      array(
     *          array(
     *              'direction' => GraphNavigator::DIRECTION_SERIALIZATION,
     *              'format' => 'json',
     *              'type' => 'DateTime',
     *              'method' => 'serializeDateTimeToJson',
     *          ),
     *      )
     *
     * The direction and method keys can be omitted.
     *
     * @return array
     */
    public static function getSubscribingMethods()
    {
        return array(
            array(
                'direction' => GraphNavigator::DIRECTION_SERIALIZATION,
                'format' => 'json',
                'type' => AbstractEnum::class,
                'method' => 'serializerEnumToJson',
            ),
        );
    }

    /**
     * @param JsonSerializationVisitor $visitor
     * @param AbstractEnum             $enum
     *
     * @return string
     *
     */
    public function serializerEnumToJson(
        JsonSerializationVisitor $visitor,
        AbstractEnum $enum
    ) {
        return $enum->get();
    }
}
