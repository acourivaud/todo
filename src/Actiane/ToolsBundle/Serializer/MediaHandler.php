<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 06/03/17
 * Time: 22:58
 */

namespace Actiane\ToolsBundle\Serializer;

use JMS\Serializer\GraphNavigator;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\JsonSerializationVisitor;
use Sonata\MediaBundle\Model\MediaInterface;
use Sonata\MediaBundle\Provider\ImageProvider;
use Sonata\MediaBundle\Provider\Pool;
use Symfony\Component\HttpFoundation\RequestStack;
use Todotoday\CatalogBundle\Services\ImageManager;
use Todotoday\MediaBundle\Entity\Media;

/**
 * Class MediaHandler
 *
 * @package Actiane\ToolsBundle\Serializer
 */
class MediaHandler implements SubscribingHandlerInterface
{
    /**
     * @var Pool
     */
    private $pool;

    /**
     * @var string
     */
    private $domain;

    /**
     * @var RequestStack
     */
    private $request;

    /**
     * MediaHandler constructor.
     *
     * @param Pool         $pool
     * @param RequestStack $request
     * @param string       $domain
     */
    public function __construct(Pool $pool, RequestStack $request, string $domain)
    {
        $this->pool = $pool;
        $this->request = $request;
        $this->domain = $domain;
    }

    /**
     * Return format:
     *
     *      array(
     *          array(
     *              'direction' => GraphNavigator::DIRECTION_SERIALIZATION,
     *              'format' => 'json',
     *              'type' => 'DateTime',
     *              'method' => 'serializeDateTimeToJson',
     *          ),
     *      )
     *
     * The direction and method keys can be omitted.
     *
     * @return array
     */
    public static function getSubscribingMethods(): array
    {
        return array(
            array(
                'direction' => GraphNavigator::DIRECTION_SERIALIZATION,
                'format' => 'json',
                'type' => Media::class,
                'method' => 'serializerImageToJson',
            ),
        );
    }

    /**
     * @param JsonSerializationVisitor $visitor
     * @param MediaInterface           $media
     *
     * @return array
     * @throws \RuntimeException
     */
    public function serializerImageToJson(
        JsonSerializationVisitor $visitor,
        MediaInterface $media
    ): array {

        $formats = array('reference');
        $formats = array_merge($formats, array_keys($this->pool->getFormatNamesByContext($media->getContext())));
        $provider = $this->pool->getProvider($media->getProviderName());

        $properties = array();

        if (!$provider instanceof ImageProvider) {
            $properties['metadata'] = $media->getProviderMetadata();
        }

        foreach ($formats as $format) {
            $port = $this->request->getCurrentRequest()->getPort();
            $protocol = $this->request->getCurrentRequest()->isSecure() ? 'https://' : 'http://';
            $url = $provider->generatePublicUrl($media, $format);
            $properties[$format]['relative_url'] = $url;
            $properties[$format]['absolute_url'] =
                $protocol . ImageManager::PREFIX_MEDIA_URL . '.' . $this->domain . ':' . $port .
                $url;
            $properties[$format]['properties'] = $provider->getHelperProperties($media, $format);
            $properties[$format]['properties']['video'] = $media->getProviderReference();
        }

        return $properties;
    }
}
