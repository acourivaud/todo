<?php
declare(strict_types = 1);

/**
 * PHP version 7
 */

namespace Actiane\ToolsBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class ActianeToolsBundle
 *
 * @package    Actiane\ToolsBundle
 */
class ActianeToolsBundle extends Bundle
{
}
