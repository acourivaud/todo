<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 14/06/2017
 * Time: 16:46
 */

namespace Actiane\AlertBundle\Enum;

use Actiane\ToolsBundle\Enum\AbstractEnum;

/**
 * Class AlertStatusEnum
 *
 * @category   Todo-Todev
 * @package    Actiane\AlertBundle
 * @subpackage Actiane\AlertBundle\Enum
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class AlertStatusEnum extends AbstractEnum
{
    const ERROR = 'error';
    const INFO = 'info';
    const SUCCESS = 'success';
    const WARNING = 'warning';
    const __DEFAULT = self::WARNING;
}
