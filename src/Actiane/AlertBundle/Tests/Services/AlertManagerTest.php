<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 12/06/2017
 * Time: 12:39
 */

namespace Actiane\AlertBundle\Tests\Services;

use Actiane\AlertBundle\DataFixtures\ORM\LoadAlertData;
use Actiane\AlertBundle\Services\AlertManager;
use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Todotoday\AccountBundle\Entity\Account;

/**
 * Class AlertManagerTest
 *
 * @category   Todo-Todev
 * @package    Actiane\AlertBundle
 * @subpackage Actiane\AlertBundle\Tests\Services
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class AlertManagerTest extends WebTestCase
{
    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAlertData::class,
        );
    }

    /**
     * @small
     */
    public function testDelete(): void
    {
        /** @var AlertManager $alertManager */
        $alertManager = $this->get('actiane.alert.alert_manager');

        /** @var Account $account */
        $account = static::getFRR()->getReference('adherent_linked_arevalta');

        $alertManager->delete($account, 'payment_method_other');

        static::assertNull($alertManager->get($account, 'payment_method_other'));
    }

    /**
     * @small
     */
    public function testFindOrCreate(): void
    {
        /** @var AlertManager $alertManager */
        $alertManager = $this->get('actiane.alert.alert_manager');

        $alert =
            $alertManager->findOrCreate(
                static::getFRR()->getReference('adherent_linked_arevalta'),
                'payment_method_other'
            );
        $this->get('doctrine.orm.entity_manager')->flush();

        static::assertNotNull($alert);
    }

    /**
     * @small
     */
    public function testGet(): void
    {
        /** @var AlertManager $alertManager */
        $alertManager = $this->get('actiane.alert.alert_manager');

        $alert = $alertManager->get(static::getFRR()->getReference('adherent_linked_arevalta'), 'payment_method');

        static::assertNotNull($alert);
    }

    /**
     * @small
     */
    public function testGetAll(): void
    {
        /** @var AlertManager $alertManager */
        $alertManager = $this->get('actiane.alert.alert_manager');

        static::assertNotEmpty(
            $alertManager->getAll(static::getFRR()->getReference('adherent_linked_arevalta'))
        );
    }
}
