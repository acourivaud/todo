<?php declare(strict_types=1);

namespace Actiane\AlertBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class ActianeAlertBundle
 *
 * @package    Actiane\AlertBundle
 */
class ActianeAlertBundle extends Bundle
{
}
