<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 15/06/2017
 * Time: 11:49
 */

namespace Actiane\AlertBundle\Twig;

use Actiane\AlertBundle\Services\AlertManager;

/**
 * Class AlertExtension
 *
 * @category   Todo-Todev
 * @package    Actiane\AlertBundle
 * @subpackage Actiane\AlertBundle\Twig
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class AlertExtension extends \Twig_Extension
{
    /**
     * @var AlertManager
     */
    private $alertManager;

    /**
     * AlertExtension constructor.
     *
     * @param AlertManager $alertManager
     */
    public function __construct(AlertManager $alertManager)
    {
        $this->alertManager = $alertManager;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions(): array
    {
        return array(
            new \Twig_SimpleFunction('get_alerts', array($this->alertManager, 'getAll')),
        );
    }
}
