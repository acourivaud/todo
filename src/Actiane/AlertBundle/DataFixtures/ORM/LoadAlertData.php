<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 12/06/2017
 * Time: 12:41
 */

namespace Actiane\AlertBundle\DataFixtures\ORM;

use Actiane\AlertBundle\Entity\Alert;
use Actiane\AlertBundle\Enum\AlertStatusEnum;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Todotoday\AccountBundle\DataFixtures\ORM\LoadAdherentLinkedMicrosoftData;

/**
 * Class LoadAlertData
 *
 * @category   Todo-Todev
 * @package    Actiane\AlertBundle
 * @subpackage Actiane\AlertBundle\DataFixtures\ORM
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class LoadAlertData extends AbstractFixture implements DependentFixtureInterface, ContainerAwareInterface
{
    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return array(
            LoadAdherentLinkedMicrosoftData::class,
        );
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     *
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     */
    public function load(ObjectManager $manager): void
    {
        if ($this->kernel->getEnvironment() === 'prod') {
            return;
        }
        $alert = (new Alert())
            ->setMessage('payment_method_invalid')
            ->setMessageDomain('todotoday')
            ->setUrl('/profile/payment')
            ->setStatus(AlertStatusEnum::getNewInstance(AlertStatusEnum::ERROR))
            ->setType('payment_method')
            ->setAccount($this->getReference('adherent_linked_arevalta'));

        $manager->persist($alert);
        $manager->flush();
    }

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->kernel = $container->get('kernel');
    }
}
