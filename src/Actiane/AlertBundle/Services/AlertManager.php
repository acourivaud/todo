<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 11/06/2017
 * Time: 17:21
 */

namespace Actiane\AlertBundle\Services;

use Actiane\AlertBundle\Entity\Alert;
use Doctrine\ORM\EntityManagerInterface;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\PaymentBundle\EventListener\AccountRoleSubscriber;

/**
 * Class AlertManager
 *
 * @category   Todo-Todev
 * @package    Actiane\AlertBundle
 * @subpackage Actiane\AlertBundle\Services
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class AlertManager
{
    /**
     * @var EntityManagerInterface
     */
    private $objectManager;

    /**
     * AlertManager constructor.
     *
     * @param EntityManagerInterface $objectManager
     */
    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * Do delete
     *
     * @param Account $account
     * @param string  $type
     *
     * @return AlertManager
     * @throws \Doctrine\ORM\ORMException
     */
    public function delete(Account $account, string $type): self
    {
        if ($alert = $this->objectManager->find(Alert::class, array('account' => $account, 'type' => $type))) {
            $this->objectManager->remove($alert);
            $this->objectManager->flush();
        }

        return $this;
    }

    /**
     * Do findOrCreate
     *
     * @param Account $account
     * @param string  $type
     *
     * @return Alert
     * @throws \UnexpectedValueException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function findOrCreate(Account $account, string $type): Alert
    {
        /** @var Alert $notification */
        if (!$notification = $this->get($account, $type)) {
            $notification = new Alert();
            $notification->setAccount($account);
            $notification->setType($type);
        }

        return $notification;
    }

    /**
     * Do get
     *
     * @param Account $account
     * @param string  $type
     *
     * @return Alert
     */
    public function get(Account $account, string $type): ?Alert
    {
        $repo = $this->objectManager->getRepository(Alert::class);

        return $repo->find(array('type' => $type, 'account' => $account));
    }

    /**
     * Do getAll
     *
     * @param Account $account
     *
     * @return Alert[]|null
     * @throws \UnexpectedValueException
     */
    public function getAll(Account $account): ?array
    {
        $repo = $this->objectManager->getRepository(Alert::class);
        if (!$account->hasRole('ROLE_ADHERENT')) {
            return $repo->findBy(
                array(
                    'account' => $account,
                    'type' => AccountRoleSubscriber::ALERT_NAME,
                )
            );
        }

        return $repo->findBy(array('account' => $account));
    }
}
