<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 11/06/2017
 * Time: 17:02
 */

namespace Actiane\AlertBundle\Entity;

use Actiane\AlertBundle\Enum\AlertStatusEnum;
use Actiane\ToolsBundle\Enum\AbstractEnum;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Todotoday\AccountBundle\Entity\Adherent;

/**
 * Class Notification
 *
 * @ORM\Entity()
 *
 * @category   Todo-Todev
 * @package    Actiane\AlertBundle
 * @subpackage Actiane\AlertBundle\Entity
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class Alert
{
    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", nullable=false, unique=false)
     * @ORM\Id()
     */
    protected $type;

    /**
     * @var Adherent
     *
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="Todotoday\AccountBundle\Entity\Adherent", inversedBy="alerts")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $account;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="string", length=255, nullable=false)
     */
    protected $message;

    /**
     * @var string
     *
     * @ORM\Column(name="message_domain", type="string", length=255, nullable=false)
     */
    protected $messageDomain;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", nullable=true, length=255)
     */
    protected $url;

    /**
     * @ORM\Column(type="string", name="link_label", nullable=true, length=100)
     */
    protected $linkLabel;

    /**
     * @ORM\Column(name="target_url", type="string", nullable=true)
     */
    protected $targetUrl;

    /**
     * @var AlertStatusEnum
     *
     * @ORM\Column(name="status", type="AlertStatusEnum", nullable=false)
     */
    protected $status;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_send_at", type="datetime", nullable=true)
     */
    protected $lastSendAt;

    /**
     * Alert constructor.
     * @throws \Doctrine\DBAL\DBALException
     * @throws \UnexpectedValueException
     */
    public function __construct()
    {
        $this->status = AlertStatusEnum::getNewInstance();
    }

    /**
     * Get account
     *
     * @return Adherent
     */
    public function getAccount(): Adherent
    {
        return $this->account;
    }

    /**
     * Set account
     *
     * @param Adherent $account
     *
     * @return Alert
     */
    public function setAccount(?Adherent $account): self
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Alert
     */
    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get lastSendAt
     *
     * @return \DateTime
     */
    public function getLastSendAt(): \DateTime
    {
        return $this->lastSendAt;
    }

    /**
     * Set lastSendAt
     *
     * @param \DateTime $lastSendAt
     *
     * @return Alert
     */
    public function setLastSendAt(\DateTime $lastSendAt): self
    {
        $this->lastSendAt = $lastSendAt;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return Alert
     */
    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get messageDomain
     *
     * @return string
     */
    public function getMessageDomain(): string
    {
        return $this->messageDomain;
    }

    /**
     * Set messageDomain
     *
     * @param string $messageDomain
     *
     * @return Alert
     */
    public function setMessageDomain(string $messageDomain): self
    {
        $this->messageDomain = $messageDomain;

        return $this;
    }

    /**
     * Get status
     *
     * @return AlertStatusEnum
     */
    public function getStatus(): AlertStatusEnum
    {
        return $this->status;
    }

    /**
     * Set status
     *
     * @param AbstractEnum|AlertStatusEnum $status
     *
     * @return Alert
     */
    public function setStatus(AlertStatusEnum $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Alert
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get url
     *
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * Set url
     *
     * @param string|null $url
     *
     * @return Alert
     */
    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLinkLabel(): ?string
    {
        return $this->linkLabel;
    }

    /**
     * @param string|null $linkLabel
     *
     * @return Alert
     */
    public function setLinkLabel(?string $linkLabel): self
    {
        $this->linkLabel = $linkLabel;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTargetUrl(): ?string
    {
        return $this->targetUrl;
    }

    /**
     * @param string|null $targetUrl
     *
     * @return Alert
     */
    public function setTargetUrl(?string $targetUrl): self
    {
        $this->targetUrl = $targetUrl;

        return $this;
    }
}
