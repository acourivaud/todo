<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 19/06/2017
 * Time: 15:35
 */

namespace Actiane\PaymentBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 *
 * @category   Todo-Todev
 * @package    Actiane\PaymentBundle\DependencyInjection
 * @subpackage Actiane\PaymentBundle\DependencyInjection
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     * @throws \RuntimeException
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('actiane_payment');

        $rootNode->append($this->getConfigStripe());

        return $treeBuilder;
    }

    /**
     * Do getConfigStripe
     *
     * @return NodeDefinition
     * @throws \RuntimeException
     */
    public function getConfigStripe(): NodeDefinition
    {
        $treeBuilder = new TreeBuilder();
        $node = $treeBuilder->root('stripe');

        $node->append($this->getConfigStripeConnections());

        return $node;
    }

    /**
     * Do getConfigStripeConnections
     *
     * @return NodeDefinition
     * @throws \RuntimeException
     */
    public function getConfigStripeConnections(): NodeDefinition
    {
        $treeBuilder = new TreeBuilder();
        $node = $treeBuilder->root('connections');

        /** @var ArrayNodeDefinition|NodeDefinition $connectionNode */
        $connectionNode = $node
            ->requiresAtLeastOneElement()
            ->useAttributeAsKey('name')
            ->prototype('array');

        $connectionNode
            ->children()
            ->scalarNode('secret')->end()
            ->scalarNode('public')->end()
            ->scalarNode('tags')->end()
            ->end();

        return $node;
    }
}
