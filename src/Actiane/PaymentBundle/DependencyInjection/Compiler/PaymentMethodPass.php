<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 11/04/2017
 * Time: 17:39
 */

namespace Actiane\PaymentBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class PaymentMethodPass
 *
 * @category   Todo-Todev
 * @package    Actiane\PaymentBundle
 * @subpackage Actiane\PaymentBundle\DependencyInjection\Compiler
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class PaymentMethodPass implements CompilerPassInterface
{
    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     *
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\InvalidArgumentException
     */
    public function process(ContainerBuilder $container): void
    {
        /** @var array[] $paymentMethodsDefinitions */
        $paymentMethodsDefinitions = $container->findTaggedServiceIds('actiane.payment.method');
        $paymentMethodManagerDefinition = $container->getDefinition('actiane.payment.method_manager');

        foreach ($paymentMethodsDefinitions as $id => $tags) {
            foreach ($tags as $tag) {
                $aliases = $tag['aliases'] ?? '';

                $paymentMethodManagerDefinition->addMethodCall(
                    'addPaymentMethod',
                    array(
                        new Reference($id),
                        explode(',', $aliases),
                    )
                );
            }
        }
    }
}
