<?php declare(strict_types=1);

/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 14/04/2017
 * Time: 00:15
 */

namespace Actiane\PaymentBundle\Services\PaymentMethods;

use Actiane\PaymentBundle\Interfaces\PaymentMethodInterface;
use Symfony\Component\Filesystem\Filesystem;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CoreBundle\Services\DomainContextManager;
use Todotoday\PaymentBundle\Form\AccountSEPAInfoType;
use Todotoday\PaymentBundle\Services\BankAccountManager;
use Todotoday\PaymentBundle\Services\ESignatureManager;
use Todotoday\PaymentBundle\Services\OneDriveManager;
use Todotoday\PaymentBundle\Services\PDFCreator;

/**
 * Class Sepa
 *
 * @category   Todo-Todev
 * @package    Actiane\PaymentBundle\Services
 * @subpackage Actiane\PaymentBundle\Services\PaymentMethods
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class Sepa implements PaymentMethodInterface
{
    /**
     * @var OneDriveManager
     */
    private $oneDriveManager;

    /**
     * @var BankAccountManager
     */
    private $bankAccountManager;

    /**
     * @var PDFCreator
     */
    private $pdfCreatorManager;

    /**
     * @var DomainContextManager
     */
    private $domainContextManager;

    /**
     * @var ESignatureManager
     */
    private $esignManager;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * Sepa constructor.
     *
     * @param OneDriveManager      $oneDriveManager
     * @param BankAccountManager   $bankAccountManager
     * @param PDFCreator           $pdfCreatorManager
     * @param DomainContextManager $domainContextManager
     * @param ESignatureManager    $esignManager
     * @param Filesystem           $filesystem
     */
    public function __construct(
        OneDriveManager $oneDriveManager,
        BankAccountManager $bankAccountManager,
        PDFCreator $pdfCreatorManager,
        DomainContextManager $domainContextManager,
        ESignatureManager $esignManager,
        Filesystem $filesystem
    ) {
        $this->oneDriveManager = $oneDriveManager;
        $this->bankAccountManager = $bankAccountManager;
        $this->pdfCreatorManager = $pdfCreatorManager;
        $this->domainContextManager = $domainContextManager;
        $this->esignManager = $esignManager;
        $this->filesystem = $filesystem;
    }

    /**
     * {@inheritdoc}
     */
    public function getFormType(): string
    {
        return AccountSEPAInfoType::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return 'sepa';
    }

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function handleInformation(Adherent $account): PaymentMethodInterface
    {
        if (($customerBankAccount = $account->getCustomerBankAccount())
            && ($paymentType = $account->getPaymentTypeSelected())
            && $paymentType->get() === $this->getName()
        ) {
            $customerBankAccount->setCustomerAccountNumber($account->getCustomerAccount())
                ->setDataAreaId($account->getDataAreaId());
            $this->bankAccountManager->createCustomerBankAccount($customerBankAccount);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function hasValidInformation($entity): bool
    {
        return false;
    }

    /**
     * {@inheritdoc}
     * @throws \Exception
     * @throws \RuntimeException
     * @throws \Twig_Error
     * @throws \UnexpectedValueException
     * @throws \InvalidArgumentException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\Filesystem\Exception\IOException
     */
    public function validateInformation(Adherent $account): PaymentMethodInterface
    {
        if (($customerBankAccount = $account->getCustomerBankAccount())
            && ($paymentType = $account->getPaymentTypeSelected())
            && $paymentType->get() === $this->getName()
        ) {
            $agency = $this->domainContextManager
                ->getCurrentContext()
                ->getAgency();
            $agencySlug = $agency->getSlug();

            // Quand il était possible de faire un hash_file depuis l'url du one drive,
            // on n'avait pas besoin de créer physiquement le fichier PDF
//            $pdfSepaBinaries =
//                $this->pdfCreatorManager->createSepaBinaryFile($agencySlug, $account, $customerBankAccount);

            $pdf = $this->pdfCreatorManager->createSepaPdf($agencySlug, $account, $customerBankAccount);
            $pdfSepaBinaries = file_get_contents($pdf);

            $fileName = $this->pdfCreatorManager->getSepaFileName($agencySlug, $account);
            $response = $this->oneDriveManager->uploadFileWithBinariesForSepa($agencySlug, $fileName, $pdfSepaBinaries);

            $filePath = $response['filePath'];
            $sharingLinkResponse = $this->oneDriveManager->createSharingLink($filePath);

            if ($sharingLinkResponse['success']) {
                $sharingLink = $sharingLinkResponse['sharingLink'];
                $hash = hash_file('sha256', $pdf);
                $docIdResponse = $this->esignManager->createAcronisDocIdWithFileUrl($sharingLink, $fileName, $hash);

                if (!$docIdResponse['success']) {
                    throw new \Exception($docIdResponse['message']);
                }
                $docId = $docIdResponse['docId'];
                $account->setAcronisId($docId);
                $this->bankAccountManager->createDirectDebitMandate($customerBankAccount, $docId, $agency);
                $sendEmailReponse = $this->esignManager->sendEmailToESignWithDocId(
                    $docId,
                    $account->getEmail(),
                    $account->getLanguage()
                );

                if (!$sendEmailReponse['success']) {
                    throw new \Exception($sendEmailReponse['message']);
                }

                if ($this->filesystem->exists($pdf)) {
                    $this->filesystem->remove($pdf);
                }
            }
        }

        return $this;
    }

    /**
     * Do init
     *
     * @param string $configName
     *
     * @return PaymentMethodInterface
     */
    public function init(string $configName): PaymentMethodInterface
    {
        return $this;
    }
}
