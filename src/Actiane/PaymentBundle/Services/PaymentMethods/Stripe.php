<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 10/04/2017
 * Time: 15:35
 */

namespace Actiane\PaymentBundle\Services\PaymentMethods;

use Actiane\PaymentBundle\Form\StripeType;
use Actiane\PaymentBundle\Interfaces\PaymentMethodInterface;
use Psr\Log\InvalidArgumentException;
use Stripe\Charge;
use Stripe\Customer;
use Todotoday\AccountBundle\Entity\Account;
use Todotoday\AccountBundle\Entity\Adherent;
use Todotoday\CheckoutBundle\Entity\Checkout;

/**
 * Class Stripe
 *
 * @category   Todo-Todev
 * @package    Actiane\PaymentBundle
 * @subpackage Actiane\PaymentBundle\Services\PaymentMethods
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class Stripe implements PaymentMethodInterface
{
    /**
     * @var array
     */
    protected $stripeConfig;

    /**
     * @var string
     */
    protected $configName;

    /**
     * Do init
     *
     * @param null|string $configName
     *
     * @return PaymentMethodInterface
     * @throws \Psr\Log\InvalidArgumentException
     */
    public function init(string $configName): PaymentMethodInterface
    {
        if (isset($this->stripeConfig[$configName])) {
            $this->configName = $configName;
        } else {
            foreach ($this->stripeConfig as $key => $config) {
                if (strpos($config['tags'], $configName) !== false) {
                    $this->configName = $key;
                    $configName = null;
                }
            }

            if ($configName) {
                throw new InvalidArgumentException('The stripe config ' . $configName . ' does not exist');
            }
        }

        \Stripe\Stripe::setApiKey($this->stripeConfig[$this->configName]['secret']);

        return $this;
    }

    /**
     * Do isInit
     *
     * @return bool
     */
    public function isInit(): bool
    {
        return (bool) $this->configName;
    }

    /**
     * Do checkIfInit
     *
     * @return Stripe
     * @throws \Psr\Log\InvalidArgumentException
     */
    public function checkIfInit(): self
    {
        if (!$this->isInit()) {
            throw new InvalidArgumentException('Stripe have to be init !');
        }

        return $this;
    }

    /**
     * Do chargeWithCardToken
     *
     * @param Checkout $checkout
     * @param string   $token
     *
     * @return Stripe
     * @throws \Psr\Log\InvalidArgumentException
     */
    public function chargeWithCardToken(Checkout $checkout, string $token): self
    {
        $this->checkIfInit();
        $charge = Charge::create(
            array(
                'amount' => (int) ($checkout->getTotalAmount() * 100.),
                'currency' => $checkout->getAgency()->getCurrency(),
                'source' => $token,
            )
        );

        $charge->save();

        $checkout->setStripeId($charge->id);

        return $this;
    }

    /**
     * Do getCustomer
     *
     * @param Account $account
     *
     * @return Customer
     * @throws \Psr\Log\InvalidArgumentException
     */
    public function getCustomer(Account $account): Customer
    {
        $this->checkIfInit();
        if (($customerStripeAccount = $account->getCustomerStripeAccount())
            && $customerStripId = $customerStripeAccount->getStripeCustomerId()
        ) {
            return Customer::retrieve($customerStripId);
        }
        $customer = Customer::create(
            array(
                'description' => 'Customer for ' . $account->getEmail(),
                'email' => $account->getEmail(),
            )
        );
        $customerStripeAccount->setStripeCustomerId($customer->id);

        return $customer;
    }

    /**
     * {@inheritdoc}
     */
    public function getFormType(): string
    {
        return StripeType::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return 'stripe';
    }

    /**
     * Get stripeConfig
     *
     * @return array
     */
    public function getStripeConfig(): array
    {
        return $this->stripeConfig;
    }

    /**
     * {@inheritdoc}
     * @throws \Psr\Log\InvalidArgumentException
     */
    public function handleInformation(Adherent $account): PaymentMethodInterface
    {
        if (($customerStripeAccount = $account->getCustomerStripeAccount())
            && ($stripeCardToken = $customerStripeAccount->getNewStripeCardToken())
            && $customerStripeAccount->getLastStripeCardToken() !== $stripeCardToken
        ) {
            $customer = $this->getCustomer($account);
            $card = $customer->sources->create(array('source' => $stripeCardToken));
            // @codingStandardsIgnoreStart
            $customer->default_source = $card->id;
            // @codingStandardsIgnoreEnd
            $customer->save();
            $customerStripeAccount->setStripeCardId($card->id);
            $customerStripeAccount->setLastStripeCardToken($stripeCardToken);
            $customerStripeAccount->setNewStripeCardToken(null);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function hasValidInformation($entity): bool
    {
        return false;
    }

    /**
     * Set stripeConfig
     *
     * @param array $stripeConfig
     *
     * @return Stripe
     */
    public function setStripeConfig(array $stripeConfig): self
    {
        $this->stripeConfig = $stripeConfig;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function validateInformation(Adherent $account): PaymentMethodInterface
    {
        return $this;
    }

    /**
     * Do getConfigByTag
     *
     * @param string $configTag
     *
     * @return array|null
     */
    public function getConfigByTag(string $configTag): ?array
    {
        foreach ($this->stripeConfig as $config) {
            if (strpos($config['tags'], $configTag) !== false) {
                return $config;
            }
        }

        return null;
    }
}
