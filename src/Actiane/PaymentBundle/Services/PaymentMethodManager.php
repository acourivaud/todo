<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 10/04/2017
 * Time: 14:22
 */

namespace Actiane\PaymentBundle\Services;

use Actiane\PaymentBundle\Interfaces\PaymentMethodInterface;
use Todotoday\AccountBundle\Entity\Adherent;

/**
 * Class PaymentMethodManager
 *
 * @category   Todo-Todev
 * @package    Actiane\PaymentBundle
 * @subpackage Actiane\PaymentBundle\Services
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class PaymentMethodManager
{
    /**
     * @var PaymentMethodInterface[]
     */
    protected $paymentMethods;

    /**
     * Do addPaymentMethod
     *
     * @param PaymentMethodInterface $paymentMethod
     * @param array                  $aliases
     *
     * @return PaymentMethodManager
     */
    public function addPaymentMethod(PaymentMethodInterface $paymentMethod, array $aliases = array()): self
    {
        $this->paymentMethods[$paymentMethod->getName()] = $paymentMethod;
        foreach (array_filter($aliases) as $alias) {
            $this->paymentMethods[$alias] = $paymentMethod;
        }

        return $this;
    }

    /**
     * Do getAuthorizedPaymentMethods
     *
     * @param string[] $authorizedScopes
     *
     * @return PaymentMethodInterface[]
     */
    public function getAuthorizedPaymentMethods(array $authorizedScopes): array
    {
        $paymentMethods = array();

        foreach ($this->paymentMethods as $paymentMethod) {
            if (in_array($paymentMethod->getName(), $authorizedScopes, true)) {
                $paymentMethods[] = $paymentMethod;
            }
        }

        return $paymentMethods;
    }

    /**
     * Get paymentMethods
     *
     * @param string $alias
     *
     * @return PaymentMethodInterface
     */
    public function getPaymentMethod(string $alias): PaymentMethodInterface
    {
        return $this->paymentMethods[$alias];
    }

    /**
     * Get paymentMethods
     *
     * @return PaymentMethodInterface[]
     */
    public function getPaymentMethods(): array
    {
        return $this->paymentMethods;
    }

    /**
     * Set paymentMethods
     *
     * @param PaymentMethodInterface[] $paymentMethods
     *
     * @return PaymentMethodManager
     */
    public function setPaymentMethods(array $paymentMethods): self
    {
        $this->paymentMethods = $paymentMethods;

        return $this;
    }

    /**
     * Do hasAtLeastOneValidPaymentInformation
     *
     * @param object $entity
     *
     * @return bool
     */
    public function hasAtLeastOneValidPaymentInformation($entity): bool
    {
        foreach ($this->paymentMethods as $paymentMethod) {
            if ($paymentMethod->hasValidInformation($entity)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Do updatePaymentInformation
     *
     * @param Adherent $account
     * @param array    $authorizedScopes
     *
     * @return PaymentMethodManager
     */
    public function updatePaymentInformation(Adherent $account, array $authorizedScopes): self
    {
        foreach ($this->getAuthorizedPaymentMethods($authorizedScopes) as $authorizedPaymentMethod) {
            $authorizedPaymentMethod->handleInformation($account);
        }

        return $this;
    }

    /**
     * Do updatePaymentInformation
     *
     * @param string $configName
     * @param array  $authorizedScopes
     *
     * @return PaymentMethodManager
     */
    public function initPaymentInformation(string $configName, array $authorizedScopes): self
    {
        foreach ($this->getAuthorizedPaymentMethods($authorizedScopes) as $authorizedPaymentMethod) {
            $authorizedPaymentMethod->init($configName);
        }

        return $this;
    }

    /**
     * Do validatePaymentInformation
     *
     * @param Adherent $account
     * @param array    $authorizedScopes
     *
     * @return PaymentMethodManager
     */
    public function validatePaymentInformation(Adherent $account, array $authorizedScopes): self
    {
        foreach ($this->getAuthorizedPaymentMethods($authorizedScopes) as $authorizedPaymentMethod) {
            $authorizedPaymentMethod->validateInformation($account);
        }

        return $this;
    }
}
