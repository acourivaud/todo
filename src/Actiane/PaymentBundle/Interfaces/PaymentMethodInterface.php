<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 10/04/2017
 * Time: 14:27
 */

namespace Actiane\PaymentBundle\Interfaces;

use Todotoday\AccountBundle\Entity\Adherent;

/**
 * Interface PaymentMethodInterface
 *
 * @category   Todo-Todev
 * @package    Actiane\PaymentBundle
 * @subpackage Actiane\PaymentBundle\Interfaces
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
interface PaymentMethodInterface
{
    /**
     * Do getFormType
     *
     * @return string
     */
    public function getFormType(): string;

    /**
     * Do getName
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Do handleInformation
     *
     * @param Adherent $account
     *
     * @return PaymentMethodInterface
     */
    public function handleInformation(Adherent $account): self;

    /**
     * Valid informations to know if it's usable
     *
     * @param object $entity
     *
     * @return bool
     */
    public function hasValidInformation($entity): bool;

    /**
     * Do validateInformation
     *
     * @param Adherent $account
     *
     * @return PaymentMethodInterface
     */
    public function validateInformation(Adherent $account): self;

    /**
     * Do init
     *
     * @param string $configName
     *
     * @return PaymentMethodInterface
     */
    public function init(string $configName): self;
}
