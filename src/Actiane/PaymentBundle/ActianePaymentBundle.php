<?php declare(strict_types=1);

namespace Actiane\PaymentBundle;

use Actiane\PaymentBundle\DependencyInjection\Compiler\PaymentMethodPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class ActianePaymentBundle
 *
 * @package    Actiane\PaymentBundle
 */
class ActianePaymentBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new PaymentMethodPass());
    }
}
