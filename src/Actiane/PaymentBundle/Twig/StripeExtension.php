<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 19/06/2017
 * Time: 17:04
 */

namespace Actiane\PaymentBundle\Twig;

use Actiane\PaymentBundle\Services\PaymentMethods\Stripe;
use Todotoday\CoreBundle\Services\DomainContextManager;

/**
 * Class StripeExtension
 *
 * @category   Todo-Todev
 * @package    Actiane\PaymentBundle\Twig
 * @subpackage Actiane\PaymentBundle\Twig
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class StripeExtension extends \Twig_Extension
{
    /**
     * @var DomainContextManager
     */
    private $domainContextManager;

    /**
     * @var Stripe
     */
    private $stripe;

    /**
     * StripeExtension constructor.
     *
     * @param DomainContextManager $domainContextManager
     * @param Stripe               $stripe
     */
    public function __construct(DomainContextManager $domainContextManager, Stripe $stripe)
    {
        $this->domainContextManager = $domainContextManager;
        $this->stripe = $stripe;
    }

    /**
     * Do getFunctions
     *
     * @return array
     */
    public function getFunctions(): array
    {
        return array(
            new \Twig_SimpleFunction('get_publishable_key', array($this, 'getPublishableKey')),
        );
    }

    /**
     * Do getPublishableKey
     *
     * @return string
     * @throws \UnexpectedValueException
     */
    public function getPublishableKey(): string
    {
        $dataAreaId = $this->domainContextManager->getCurrentContext()->getAgency()->getDataAreaId();

        return $this->stripe->getConfigByTag($dataAreaId)['public'];
    }
}
