<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 10/04/2017
 * Time: 14:52
 */

namespace Actiane\PaymentBundle\Form;

use Actiane\PaymentBundle\Enum\PaymentTypeEnum;
use Actiane\ToolsBundle\Services\AssetManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;
use Todotoday\PaymentBundle\Entity\CustomerStripeAccount;

/**
 * Class StripeType
 *
 * @category   Todo-Todev
 * @package    Actiane\PaymentBundle
 * @subpackage Actiane\PaymentBundle\Form
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class StripeType extends AbstractType
{
    /**
     * @var AssetManager
     */
    private $assetManager;

    /**
     * StripeType constructor.
     *
     * @param AssetManager $assetManager
     */
    public function __construct(AssetManager $assetManager)
    {
        $this->assetManager = $assetManager;
        $this->assetManager->addJavascript('https://js.stripe.com/v2/');
//        $assetManager->addJavascript('/bundles/actianepayment/js/stripe.js');
        $this->assetManager->addJavascript('build/bundles/stripe.js');
    }

    /**
     * {@inheritdoc}
     * @throws \Symfony\Component\Validator\Exception\ConstraintDefinitionException
     * @throws \Symfony\Component\Validator\Exception\InvalidOptionsException
     * @throws \Symfony\Component\Validator\Exception\MissingOptionsException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \Symfony\Component\Form\Exception\LogicException
     * @throws \Symfony\Component\Form\Exception\UnexpectedTypeException
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $flow = $options['flow'];

        $formOptions = array(
            'attr' => array(
                'v-model' => 'value', //value is the stripe_token
            ),
            'constraints' => array(
                new NotNull(
                    array(
                        'groups' => array(
                            'stripe',
                            'flow_' . $flow['formFlowName'] . '_step' . $flow['step'],
                        ),
                        'message' => 'account.error_stripe_cart_not_added',
                    )
                ),
            ),
        );

        $builder->add(
            'newStripeCardToken',
            HiddenType::class,
            $formOptions
        );

        if (!$options['as_component']) {
            $this->assetManager->addJavascript('build//bundles/stripe_new_vue.js');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        $view->vars['as_component'] = $options['as_component'];
        $view->vars['fields_required'] = $options['fields_required'];
        $view->vars['stripe_class'] = $options['stripe_class'];
        $view->vars['card_save_message_success'] = $options['card_save_message_success'];
    }

    /**
     * {@inheritdoc}
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            array(
                'data_class' => CustomerStripeAccount::class,
                'as_component' => false,
                'fields_required' => false,
                'flow' => array(
                    'enable' => false,
                    'formFlowName' => '',
                    'step' => 0,
                ),
                'stripe_class' => '',
                'card_save_message_success' => 'stripe.card_save_success',
            )
        );
    }
}
