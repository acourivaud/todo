<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 11/04/2017
 * Time: 20:48
 */

namespace Actiane\PaymentBundle\Tests\Services;

use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;
use Todotoday\CoreBundle\DataFixtures\ORM\LoadAgencyData;

/**
 * Class PaymentMethodManagerTest
 *
 * @category   Todo-Todev
 * @package    Actiane\PaymentBundle\Tests\Services
 * @subpackage Actiane\PaymentBundle\Tests\Services
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class PaymentMethodManagerTest extends WebTestCase
{
    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    public static function getFixtures(): array
    {
        return array(
            LoadAgencyData::class,
        );
    }

    /**
     * Do testGetAuthorizedPaymentMethods
     *
     * @return void
     */
    public function testGetAuthorizedPaymentMethods()
    {
        $paymentMethodManager = $this->getContainer()->get('actiane.payment.method_manager');

        static::assertNotEmpty($paymentMethodManager->getAuthorizedPaymentMethods(array('stripe')));
    }

    /**
     * Do testGetPaymentMethods
     *
     * @return void
     */
    public function testGetPaymentMethods()
    {
        $paymentMethodManager = $this->getContainer()->get('actiane.payment.method_manager');

        static::assertNotEmpty($paymentMethodManager->getPaymentMethods());
    }
}
