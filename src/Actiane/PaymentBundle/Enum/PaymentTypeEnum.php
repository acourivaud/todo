<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 18/04/2017
 * Time: 16:28
 */

namespace Actiane\PaymentBundle\Enum;

use Actiane\ToolsBundle\Enum\AbstractEnum;

/**
 * Class PaymentTypeEnum
 *
 * @category   Todo-Todev
 * @package    Actiane\PaymentBundle\Enum
 * @subpackage Actiane\PaymentBundle\Enum
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class PaymentTypeEnum extends AbstractEnum
{
    const SEPA = 'sepa';
    const STRIPE = 'stripe';
    const STRIPE_DIRECT = 'stripe_dir';
    const PAYPAL = 'paypal';
    const POSTFINANCE = 'postfin';
    const CHEQUE = 'cheque';
    const VIREMENT = 'virement';
    const __DEFAULT = null;
}
