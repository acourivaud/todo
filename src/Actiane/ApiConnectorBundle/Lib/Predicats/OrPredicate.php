<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 17/02/2017
 * Time: 16:33
 */

namespace Actiane\ApiConnectorBundle\Lib\Predicats;

use Actiane\ApiConnectorBundle\Lib\AbstractPredicate;

/**
 * Class OrPredicate
 *
 * @category   Todo-Todev
 * @package    Actiane\ApiConnectorBundle
 * @subpackage Actiane\ApiConnectorBundle\Lib\Predicats
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class OrPredicate extends AbstractPredicate
{
    // TODO: gogo implement
}
