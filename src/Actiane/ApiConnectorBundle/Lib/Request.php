<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 17/02/2017
 * Time: 17:25
 */

namespace Actiane\ApiConnectorBundle\Lib;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiConnection;
use Actiane\ApiConnectorBundle\Abstracts\AbstractApiRepository;

/**
 * Class Request
 *
 * @category   Todo-Todev
 * @package    Actiane\ApiConnectorBundle\Lib
 * @subpackage Actiane\ApiConnectorBundle\Lib
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class Request
{
    /**
     * @var string
     */
    protected $urlSuffix;

    /**
     * @var string[]
     */
    protected $urlParams;

    /**
     * @var string[]
     */
    protected $headers;

    /**
     * @var string[]
     */
    protected $postFields;

    /**
     * @var AbstractApiConnection
     */
    protected $apiConnection;

    /**
     * @var AbstractApiRepository
     */
    protected $entityRepo;

    /**
     * Request constructor.
     *
     * @param AbstractApiConnection $apiConnection
     * @param AbstractApiRepository $entityRepo
     *
     * @throws \InvalidArgumentException
     */
    public function __construct(AbstractApiConnection $apiConnection, AbstractApiRepository $entityRepo = null)
    {
        $this->apiConnection = $apiConnection;
        $this->entityRepo = $entityRepo;
        $this->urlParams = array();
        if ($entityRepo) {
            $this->urlSuffix = $entityRepo->getMapping()->getPath();
        }
    }

    /**
     * Do addHeader
     *
     * @param string $key
     * @param string $value
     *
     * @return Request
     */
    public function addHeader(string $key, string $value): self
    {
        $this->headers[$key] = $value;

        return $this;
    }

    /**
     * Do addPostField
     *
     * @param string $key
     * @param string $value
     *
     * @return Request
     */
    public function addPostField(string $key, string $value): self
    {
        $this->postFields[$key] = $value;

        return $this;
    }

    /**
     * Do addUrlParam
     *
     * @param string $key
     * @param string $value
     *
     * @return Request
     */
    public function addUrlParam(string $key, string $value): self
    {
        $this->urlParams[$key] = $value;

        return $this;
    }

    /**
     * Do getResult
     *
     * @param bool $singleResult
     *
     * @return array|object[]|object|null
     * @throws \InvalidArgumentException
     */
    public function execute(bool $singleResult = false)
    {
        if (!$results = $this->apiConnection->execute($this)) {
            return null;
        }

        if ($this->entityRepo && ($mapping = $this->entityRepo->getMapping())) {
            if ($singleResult) {
                return $mapping->handleResult($results);
            }

            return $mapping->handleResults($results);
        }

        return $results;
    }

    /**
     * Get headers
     *
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * Set header
     *
     * @param array $headers
     *
     * @return Request
     */
    public function setHeaders(array $headers): self
    {
        $this->headers = $headers;

        return $this;
    }

    /**
     * Get postFields
     *
     * @return string[]
     */
    public function getPostFields(): array
    {
        return $this->postFields;
    }

    /**
     * Get urlParams
     *
     * @return array
     */
    public function getUrlParams(): array
    {
        return $this->urlParams;
    }

    /**
     * Set urlParams
     *
     * @param array $urlParams
     *
     * @return Request
     */
    public function setUrlParams(array $urlParams): self
    {
        $this->urlParams = $urlParams;

        return $this;
    }

    /**
     * Get urlSuffix
     *
     * @return string
     */
    public function getUrlSuffix(): ?string
    {
        return $this->urlSuffix;
    }

    /**
     * Set urlSuffix
     *
     * @param string $urlSuffix
     *
     * @return Request
     */
    public function setUrlSuffix(string $urlSuffix = null): self
    {
        $this->urlSuffix = $urlSuffix;

        return $this;
    }
}
