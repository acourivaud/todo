<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 18/02/2017
 * Time: 09:10
 */

namespace Actiane\ApiConnectorBundle\Lib;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiConnection;
use Doctrine\Common\Util\Inflector;

/**
 * Class EntityMapping
 *
 * @category   Todo-Todev
 * @package    Actiane\ApiConnectorBundle\Lib
 * @subpackage Actiane\ApiConnectorBundle\Lib
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class EntityMapping
{
    /**
     * @var string
     */
    protected $class;

    /**
     * @var AbstractApiConnection
     */
    protected $apiConnection;

    /**
     * @var AbstractFieldMapping[]
     */
    protected $fields;

    /**
     * @var AbstractFieldMapping[]
     */
    protected $reverseFields;

    /**
     * @var string
     */
    protected $path;

    /**
     * @var string[]
     */
    protected $identifiers;

    /**
     * @var string
     */
    protected $repositoryId;

    /**
     * @var string
     */
    protected $doctrineEntityClass;

    /**
     * @var string
     */
    protected $doctrineEntityRepo;

    /**
     * @var AbstractFieldMapping[]
     */
    protected $doctrineFields;

    /**
     * EntityMapping constructor.
     *
     * @param AbstractApiConnection $apiConnection
     */
    public function __construct(AbstractApiConnection $apiConnection)
    {
        $this->fields = array();
        $this->reverseFields = array();
        $this->doctrineFields = array();
        $this->apiConnection = $apiConnection;
    }

    /**
     * Do addField
     *
     * @param AbstractFieldMapping $mapping
     *
     * @return EntityMapping
     * @throws \InvalidArgumentException
     */
    public function addField(AbstractFieldMapping $mapping): self
    {
        if (array_key_exists($mapping->getAttributeName(), $this->fields)) {
            throw new \InvalidArgumentException('Field ' . $mapping->getAttributeName() . ' already mapped.');
        }
        if (array_key_exists($mapping->name, $this->reverseFields)) {
            throw new \InvalidArgumentException('ReverseField ' . $mapping->getAttributeName() . ' already mapped.');
        }

        $this->fields[$mapping->getAttributeName()] = $mapping;
        $this->reverseFields[$mapping->name] = $mapping;
        if ($mapping->doctrineAttribute) {
            $this->doctrineFields[$mapping->doctrineAttribute] = $mapping;
        }

        return $this;
    }

    /**
     * Do addIdentifier
     *
     * @param string $identifier
     *
     * @return EntityMapping
     */
    public function addIdentifier(string $identifier): self
    {
        $this->identifiers[] = $identifier;

        return $this;
    }

    /**
     * Do fillEntity
     *
     * @param object $entity
     * @param array  $result
     *
     * @return void
     */
    public function fillEntity($entity, array $result): void
    {
        foreach ($result as $key => $value) {
            if (array_key_exists($key, $this->reverseFields)) {
                $this->reverseFields[$key]->fillEntity($entity, $value);
            }
        }
    }

    /**
     * Do fillEntity
     *
     * @param object $entity
     * @param object $entityDoctrine
     * @param bool   $force
     *
     * @return EntityMapping
     */
    public function fillEntityDoctrineInEntity($entity, $entityDoctrine, bool $force = false): self
    {
        foreach ($this->doctrineFields as $doctrineField => $field) {
            if ($force || !$entity->{'get' . ucfirst($field->getAttributeName())}()) {
                $entity->{'set' . ucfirst($field->getAttributeName())}(
                    $entityDoctrine->{'get' . ucfirst($doctrineField)}()
                );
            }
        }

        return $this;
    }

    /**
     * Do fillEntity
     *
     * @param object $entity
     * @param object $entityDoctrine
     * @param bool   $force
     *
     * @return EntityMapping
     */
    public function fillEntityInEntityDoctrine($entity, $entityDoctrine, bool $force = false): self
    {
        foreach ($this->doctrineFields as $doctrineField => $field) {
            $suffixMethod = ucfirst($doctrineField);

            if ($force || !$entityDoctrine->{'get' . $suffixMethod}()) {
                $entityDoctrine->{'set' . $suffixMethod}(
                    $entity->{'get' . ucfirst($field->getAttributeName())}()
                );
            }
        }

        return $this;
    }

    /**
     * Get class
     *
     * @return string
     */
    public function getClass(): string
    {
        return $this->class;
    }

    /**
     * Set class
     *
     * @param string $class
     *
     * @return EntityMapping
     */
    public function setClass(string $class): EntityMapping
    {
        $this->class = $class;

        return $this;
    }

    /**
     * Get doctrineEntityClass
     *
     * @return string
     */
    public function getDoctrineEntityClass(): string
    {
        return $this->doctrineEntityClass;
    }

    /**
     * Set doctrineEntityClass
     *
     * @param string $doctrineEntityClass
     *
     * @return EntityMapping
     */
    public function setDoctrineEntityClass(?string $doctrineEntityClass = null): self
    {
        $this->doctrineEntityClass = $doctrineEntityClass;

        return $this;
    }

    /**
     * Get doctrineEntityRepo
     *
     * @return string
     */
    public function getDoctrineEntityRepo(): string
    {
        return $this->doctrineEntityRepo;
    }

    /**
     * Set doctrineEntityRepo
     *
     * @param string $doctrineEntityRepo
     *
     * @return EntityMapping
     */
    public function setDoctrineEntityRepo(?string $doctrineEntityRepo = null): self
    {
        $this->doctrineEntityRepo = $doctrineEntityRepo;

        return $this;
    }

    /**
     * Get doctrineFields
     *
     * @return AbstractFieldMapping[]
     */
    public function getDoctrineFields(): array
    {
        return $this->doctrineFields;
    }

    /**
     * Set doctrineFields
     *
     * @param AbstractFieldMapping[] $doctrineFields
     *
     * @return EntityMapping
     */
    public function setDoctrineFields(array $doctrineFields): self
    {
        $this->doctrineFields = $doctrineFields;

        return $this;
    }

    /**
     * Do getField
     *
     * @param string $attribute
     *
     * @return AbstractFieldMapping
     * @throws \InvalidArgumentException
     */
    public function getField(string $attribute): AbstractFieldMapping
    {
        if (!array_key_exists($attribute, $this->fields)) {
            throw new \InvalidArgumentException('Attribute ' . $attribute . ' does not exist !');
        }

        return $this->fields[$attribute];
    }

    /**
     * Get fields
     *
     * @return AbstractFieldMapping[]
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * Get identifier
     *
     * @return string[]
     */
    public function getIdentifiers(): ?array
    {
        return $this->identifiers;
    }

    /**
     * Set identifier
     *
     * @param array $identifiers
     *
     * @return EntityMapping
     */
    public function setIdentifiers(array $identifiers): self
    {
        $this->identifiers = $identifiers;

        return $this;
    }

    /**
     * Get identifier name
     *
     * @return string[]
     */
    public function getIdentifiersName(): ?array
    {
        $names = array();

        foreach ($this->identifiers as $identifier) {
            $names[] = $this->fields[$identifier]->name;
        }

        return $names;
    }

    /**
     * Get identifier
     *
     * @return string[]
     * @throws \InvalidArgumentException
     */
    public function getIdentifiersReversed(): ?array
    {
        $identifiersReversed = array();

        foreach ($this->identifiers as $identifier) {
            $identifiersReversed[] = $this->getField($identifier);
        }

        return $identifiersReversed;
    }

    /**
     * Do getIdentifierValue
     *
     * @param object $entity
     *
     * @return array
     * @throws \InvalidArgumentException
     */
    public function getIdentifiersReversedValue($entity): array
    {
        $values = array();

        foreach ($this->identifiers as $identifier) {
            $field = $this->getField($identifier);
            $value = $entity->{'get' . ucfirst($identifier)}();
            if ($field->mType) {
                $value = array(
                    'enum' => $field->mType,
                    'value' => $value,
                );
            }
            if ($field->guid) {
                $value = array(
                    'guid' => $field->guid,
                    'value' => $value,
                );
            }

            $values[$field->name] = $value;
        }

        return $values;
    }

    /**
     * Do getIdentifierValue
     *
     * @param object $entity
     *
     * @return array
     */
    public function getIdentifiersValue($entity): array
    {
        $values = array();
        foreach ($this->identifiers as $identifier) {
            $values[$identifier] = $entity->{'get' . ucfirst($identifier)}();
        }

        return $values;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return EntityMapping
     */
    public function setPath(string $path): EntityMapping
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get repositoryId
     *
     * @return string
     */
    public function getRepositoryId(): string
    {
        return $this->repositoryId;
    }

    /**
     * Set repositoryId
     *
     * @param string $repositoryId
     *
     * @return EntityMapping
     */
    public function setRepositoryId(string $repositoryId): self
    {
        $this->repositoryId = $repositoryId;

        return $this;
    }

    /**
     * Do handleResult
     *
     * @param array $result
     *
     * @return object
     */
    public function handleResult(array $result)
    {
        $class = $this->class;
        /** @var EntityProxyTrait $entity */
        $entity = new $class();

        $this->fillEntity($entity, $result);

        if (method_exists($entity, 'setApiConnection')) {
            $entity->setApiConnection($this->apiConnection);
        }

        return $entity;
    }

    /**
     * Do handleResults
     *
     * @param array $results
     *
     * @return object[]
     */
    public function handleResults(array $results): array
    {
        $entities = array();

        foreach ($results as $result) {
            $entities[] = $this->handleResult($result);
        }

        return $entities;
    }

    /**
     * Do hasIdentifiersValue
     *
     * @param object $entity
     *
     * @return bool
     */
    public function hasIdentifiersValue($entity): bool
    {
        foreach ($this->identifiers as $identifier) {
            if (!$entity->{'get' . ucfirst($identifier)}()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Do moveToIdentifiersReversedValue
     *
     * @param array $identifiersValue
     *
     * @return array
     * @throws \InvalidArgumentException
     */
    public function moveToIdentifiersReversedValue(array $identifiersValue): array
    {
        $identifiersReversedValue = array();

        foreach ($identifiersValue as $key => $value) {
            if (!in_array($key, $this->identifiers, true)) {
                throw new \InvalidArgumentException('Id Field ' . $key . ' does not exist');
            }

            $field = $this->getField($key);
            if ($field->mType) {
                $value = array(
                    'enum' => $field->mType,
                    'value' => $value,
                );
            }
            if ($field->guid) {
                $value = array(
                    'guid' => $field->guid,
                    'value' => $value,
                );
            }
            $identifiersReversedValue[$field->name] = $value;
        }

        return $identifiersReversedValue;
    }

    /**
     * Do getIdentifierValue
     *
     * @param object $entity
     * @param array  $values
     *
     * @return EntityMapping
     * @throws \InvalidArgumentException
     */
    public function setIdentifiersValue($entity, array $values): EntityMapping
    {
        foreach ($this->identifiers as $identifier) {
            if (array_key_exists($this->getField($identifier)->name, $values)) {
                $entity->{'set' . ucfirst($identifier)}($values[$this->getField($identifier)->name]);
            }
        }

        return $this;
    }
}
