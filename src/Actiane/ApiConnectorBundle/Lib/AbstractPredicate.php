<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 17/02/2017
 * Time: 16:15
 */

namespace Actiane\ApiConnectorBundle\Lib;

/**
 * Class AbstractPredicate
 *
 * @category   Todo-Todev
 * @package    Actiane\ApiConnectorBundle
 * @subpackage Actiane\ApiConnectorBundle\Lib
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
abstract class AbstractPredicate
{
}
