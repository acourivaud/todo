<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 16/02/2017
 * Time: 15:17
 */

namespace Actiane\ApiConnectorBundle\Lib\Mapping;

use Actiane\ApiConnectorBundle\Lib\AbstractFieldMapping;

/**
 * Class Column
 *
 * @Annotation
 * @Target({"PROPERTY"})
 *
 * @category   Todo-Todev
 * @package    Actiane\ApiConnectorBundle
 * @subpackage Actiane\ApiConnectorBundle\Lib\Mapping
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class Column extends AbstractFieldMapping
{
    /**
     * The simple type
     *
     * @var string string|int|float|date|datetime|datetimez
     */
    public $type = 'string';

    /**
     * Do fillEntity
     *
     * @param object                 $entity
     * @param array|float|int|string $value
     *
     * @return AbstractFieldMapping
     * @throws \InvalidArgumentException
     */
    public function fillEntity($entity, $value): AbstractFieldMapping
    {
        $method = 'fillEntity' . ucfirst($this->type);

        if (!method_exists($this, $method)) {
            throw new \InvalidArgumentException('type ' . $this->type . '  doesn\'t exist for column ' . $this->name);
        }
        $this->$method($entity, $value);

        return $this;
    }

    /**
     * Do fillEntityDate
     *
     * @param object                 $entity
     * @param string|int|float|array $value
     *
     * @return Column
     */
    public function fillEntityDate($entity, $value): self
    {
        return $this->fillEntityDatetimez($entity, $value);
    }

    /**
     * Do fillEntityDatetime
     *
     * @param object                 $entity
     * @param string|int|float|array $value
     *
     * @return Column
     */
    public function fillEntityDatetime($entity, $value): self
    {
        return $this->fillEntityDatetimez($entity, $value);
    }

    /**
     * Do fillEntityDatetimez
     *
     * @param object                 $entity
     * @param string|int|float|array $value
     *
     * @return Column
     */
    public function fillEntityDatetimez($entity, $value): self
    {
        return $this->setValue($entity, is_null($value) ? null : new \DateTime($value));
    }

    /**
     * Do fillEntityEnum
     *
     * @param object                 $entity
     * @param string|int|float|array $value
     *
     * @return Column
     */
    public function fillEntityEnum($entity, $value): self
    {
        return $this->setValue($entity, (string) $value);
    }

    /**
     * Do fillEntityFloat
     *
     * @param object                 $entity
     * @param string|int|float|array $value
     *
     * @return Column
     */
    public function fillEntityFloat($entity, $value): self
    {
        return $this->setValue($entity, (float) $value);
    }

    /**
     * Do fillEntityInt
     *
     * @param object                 $entity
     * @param string|int|float|array $value
     *
     * @return Column
     */
    public function fillEntityInt($entity, $value): self
    {
        return $this->setValue($entity, (int) $value);
    }

    /**
     * Do fillEntityString
     *
     * @param object                 $entity
     * @param string|int|float|array $value
     *
     * @return Column
     */
    public function fillEntityString($entity, $value): self
    {
        if ($value) {
            return $this->setValue($entity, (string) $value);
        }

        return $this;
    }

    /**
     * @param object                      $entity
     * @param string|int|float|array|bool $value
     *
     * @return Column
     */
    public function fillEntityBool($entity, $value): self
    {
        return $this->setValue($entity, (bool) $value);
    }

    /**
     * Do setValue
     *
     * @param object                 $entity
     * @param string|int|float|array $value
     *
     * @return $this
     */
    public function setValue($entity, $value)
    {
        $entity->{'set' . ucfirst($this->getAttributeName())}($value);

        return $this;
    }
}
