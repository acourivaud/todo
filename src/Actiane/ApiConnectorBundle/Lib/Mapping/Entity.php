<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 16/02/2017
 * Time: 16:32
 */

namespace Actiane\ApiConnectorBundle\Lib\Mapping;

use Actiane\ApiConnectorBundle\Lib\AbstractMapping;
use Actiane\ApiConnectorBundle\Lib\EntityMapping;

/**
 * Class Entity
 *
 * @Annotation
 * @Target({"CLASS"})
 *
 * @category   Todo-Todev
 * @package    Actiane\ApiConnectorBundle
 * @subpackage Actiane\ApiConnectorBundle\Lib\Mapping
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class Entity extends AbstractMapping
{
    /**
     * Path rest to make calls
     *
     * @var string
     */
    public $path;

    /**
     * Repository service id
     *
     * @var string
     */
    public $repositoryId;

    /**
     * Doctrine class
     *
     * @var string
     */
    public $doctrineEntityClass;

    /**
     * Doctrine repo id
     *
     * @var string
     */
    public $doctrineEntityRepo;

    /**
     * Do fillEntityMap
     *
     * @param EntityMapping $mapping
     * @param string        $className
     *
     * @return AbstractMapping
     */
    public function fillEntityMap(EntityMapping $mapping, string $className): AbstractMapping
    {
        $mapping
            ->setClass($className)
            ->setPath($this->path)
            ->setRepositoryId($this->repositoryId)
            ->setDoctrineEntityClass($this->doctrineEntityClass)
            ->setDoctrineEntityRepo($this->doctrineEntityRepo)
        ;

        return $this;
    }
}
