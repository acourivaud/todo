<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 21/02/2017
 * Time: 16:42
 */

namespace Actiane\ApiConnectorBundle\Lib\Mapping;

use Actiane\ApiConnectorBundle\Lib\AbstractMapping;
use Actiane\ApiConnectorBundle\Lib\EntityMapping;

/**
 * Class Id
 *
 * @Annotation
 * @Target({"PROPERTY"})
 *
 * @category   Todo-Todev
 * @package    Actiane\ApiConnectorBundle
 * @subpackage Actiane\ApiConnectorBundle\Lib\Mapping
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class Id extends AbstractMapping
{
    /**
     * Do fillEntityMap
     *
     * @param EntityMapping $mapping
     * @param string        $attributeName
     *
     * @return AbstractMapping
     * @throws \InvalidArgumentException
     */
    public function fillEntityMap(EntityMapping $mapping, string $attributeName): AbstractMapping
    {
        $mapping->addIdentifier($attributeName);

        return $this;
    }
}
