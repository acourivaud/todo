<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 22/02/2017
 * Time: 11:49
 */

namespace Actiane\ApiConnectorBundle\Lib;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiConnection;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class EntityProxyTrait
 *
 * @category   Todo-Todev
 * @package    Actiane\ApiConnectorBundle
 * @subpackage Actiane\ApiConnectorBundle\Lib
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
trait EntityProxyTrait
{
    /**
     * @var AbstractApiConnection
     */
    protected $apiConnection;

    /**
     * Get apiConnection
     *
     * @return AbstractApiConnection
     */
    public function getApiConnection(): AbstractApiConnection
    {
        return $this->apiConnection;
    }

    /**
     * Set apiConnection
     *
     * @param AbstractApiConnection $apiConnection
     *
     * @return EntityProxyTrait
     */
    public function setApiConnection(AbstractApiConnection $apiConnection): self
    {
        $this->apiConnection = $apiConnection;

        return $this;
    }

    /**
     * Do setTrigger
     *
     * @param string $attribute
     * @param mixed  $value
     *
     * @return mixed
     */
    public function setTrigger(string $attribute, $value): self
    {
        $previousValue = null;

        if ($this->apiConnection) {
            $previousValue = $this->$attribute;
        }

        $this->$attribute = $value;

        if ($this->apiConnection && $previousValue !== $this->$attribute) {
            $this->apiConnection->addEntityUpdate($this, $attribute);
        }

        return $this;
    }

    /**
     * Do setTrigger
     *
     * @param string $attribute
     * @param array  $params
     *
     * @return EntityProxyTrait|mixed
     */
    public function setTriggerGuid(string $attribute, array $params): self
    {
        $previousValue = null;

        if ($this->apiConnection) {
            $previousValue = $this->$attribute;
        }

        $this->$attribute = $params;

        if ($this->apiConnection && $previousValue !== $this->$attribute['value']) {
            $this->apiConnection->addEntityUpdate($this, $attribute);
        }

        return $this;
    }
}
