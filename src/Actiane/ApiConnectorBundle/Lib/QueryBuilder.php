<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 17/02/2017
 * Time: 15:58
 */

namespace Actiane\ApiConnectorBundle\Lib;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiConnection;
use Actiane\ApiConnectorBundle\Abstracts\AbstractApiRepository;

/**
 * Class QueryBuilder
 *
 * @category   Todo-Todev
 * @package    Actiane\ApiConnectorBundle
 * @subpackage Actiane\ApiConnectorBundle\Lib
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class QueryBuilder
{
    /**
     * @var string[]
     */
    protected $select;

    /**
     * @var string[]
     */
    protected $orderBy;

    /**
     * @var int
     */
    protected $limit;

    /**
     * @var int
     */
    protected $offset;

    /**
     * @var AbstractPredicate
     */
    protected $predicate;

    /**
     * @var string[]
     */
    protected $customQueries;

    /**
     * @var AbstractApiConnection
     */
    protected $apiConnection;

    /**
     * @var string[]
     */
    protected static $buildRequestSequence = array(
        'select' => 'select',
        'orderBy' => 'orderBy',
        'limit' => 'limit',
        'offset' => 'offset',
        'custom' => 'custom',
    );

    /**
     * @var AbstractApiRepository
     */
    private $apiRepository;

    /**
     * QueryBuilder constructor.
     *
     * @param AbstractApiConnection $apiConnection
     * @param AbstractApiRepository $apiRepository
     */
    public function __construct(AbstractApiConnection $apiConnection, AbstractApiRepository $apiRepository)
    {
        $this->apiConnection = $apiConnection;
        $this->apiRepository = $apiRepository;
    }

    /**
     * Do getRequest
     *
     * @return Request
     * @throws \InvalidArgumentException
     */
    public function getRequest(): Request
    {
        $request = new Request($this->apiConnection, $this->apiRepository);

        $driver = $this->apiConnection->getDriver();

        foreach (static::$buildRequestSequence as $key => $value) {
            $driver->$key($request, $this->{'get' . ucfirst($value)}());
        }

        return $request;
    }

    /**
     * Do getCustom
     *
     * @return array|null
     */
    public function getCustom(): ?array
    {
        return $this->customQueries;
    }

    /**
     * Get select
     *
     * @return string[]
     */
    public function getSelect(): ?array
    {
        return $this->select;
    }

    /**
     * Set select
     *
     * @param string[] $select
     *
     * @return QueryBuilder
     */
    public function setSelect(array $select = null): self
    {
        $this->select = $select;

        return $this;
    }

    /**
     * Get orderBy
     *
     * @return string[]
     */
    public function getOrderBy(): ?array
    {
        return $this->orderBy;
    }

    /**
     * Set orderBy
     *
     * @param string[] $orderBy
     *
     * @return QueryBuilder
     */
    public function setOrderBy(array $orderBy = null): self
    {
        $this->orderBy = $orderBy;

        return $this;
    }

    /**
     * Get limit
     *
     * @return int
     */
    public function getLimit(): ?int
    {
        return $this->limit;
    }

    /**
     * Set limit
     *
     * @param int $limit
     *
     * @return QueryBuilder
     */
    public function setLimit(int $limit = null): self
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * Get offset
     *
     * @return int
     */
    public function getOffset(): ?int
    {
        return $this->offset;
    }

    /**
     * Set offset
     *
     * @param int $offset
     *
     * @return QueryBuilder
     */
    public function setOffset(int $offset = null): self
    {
        $this->offset = $offset;

        return $this;
    }

    /**
     * Get predicate
     *
     * @return AbstractPredicate
     */
    public function getPredicate(): AbstractPredicate
    {
        return $this->predicate;
    }

    /**
     * Set predicate
     *
     * @param AbstractPredicate $predicate
     *
     * @return QueryBuilder
     */
    public function setPredicate(AbstractPredicate $predicate = null): self
    {
        $this->predicate = $predicate;

        return $this;
    }

    /**
     * Get customQuery
     *
     * @return string[]
     */
    public function getCustomQueries(): ?array
    {
        return $this->customQueries;
    }

    /**
     * Do addCustomQuery
     *
     * @param string $key
     * @param string $value
     *
     * @return QueryBuilder
     */
    public function addCustomQuery(string $key, string $value): self
    {
        $this->customQueries[$key] = $value;

        return $this;
    }
}
