<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 17/02/2017
 * Time: 17:20
 */

namespace Actiane\ApiConnectorBundle\Lib\Drivers;

use Actiane\ApiConnectorBundle\Lib\AbstractDriver;
use Actiane\ApiConnectorBundle\Lib\AbstractPredicate;
use Actiane\ApiConnectorBundle\Lib\Request;

/**
 * Class Microsoft
 *
 * @category   Todo-Todev
 * @package    Actiane\ApiConnectorBundle
 * @subpackage Actiane\ApiConnectorBundle\Lib\Drivers
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class Microsoft extends AbstractDriver
{
    /**
     * Do custom
     *
     * @param Request $request
     * @param array   $customQueries
     *
     * @return AbstractDriver
     */
    public function custom(Request $request, ?array $customQueries = null): AbstractDriver
    {
        if ($customQueries) {
            foreach ($customQueries as $key => $value) {
                $request->addUrlParam($key, $value);
            }
        }

        return $this;
    }

    /**
     * Do limit
     *
     * @param Request $request
     * @param int     $limit
     *
     * @return AbstractDriver
     */
    public function limit(Request $request, ?int $limit = null): AbstractDriver
    {
        if ($limit) {
            $request->addUrlParam('$top', (string) $limit);
        }

        return $this;
    }

    /**
     * Do offset
     *
     * @param Request $request
     * @param int     $offset
     *
     * @return AbstractDriver
     */
    public function offset(Request $request, ?int $offset = null): AbstractDriver
    {
        if ($offset) {
            $request->addUrlParam('$skip', (string) $offset);
        }

        return $this;
    }

    /**
     * Do orderBy
     *
     * @param Request $request
     * @param array   $orderBy
     *
     * @return AbstractDriver
     */
    public function orderBy(Request $request, ?array $orderBy = null): AbstractDriver
    {
        if ($orderBy) {
            $orderByUrl = '';
            foreach ($orderBy as $key => $value) {
                if ($orderByUrl) {
                    $orderByUrl .= ',';
                }
                $orderByUrl .= $key;
                if ($value) {
                    $orderByUrl .= ' ' . $value;
                }
            }
            $request->addUrlParam('$orderBy', $orderByUrl);
        }

        return $this;
    }

    /**
     * Do select
     *
     * @param Request $request
     * @param array   $selects
     *
     * @return AbstractDriver
     */
    public function select(Request $request, ?array $selects = null): AbstractDriver
    {
        if ($selects) {
            $request->addUrlParam('$select', implode(',', $selects));
        }

        return $this;
    }

    /**
     * Do where
     *
     * @param Request           $request
     * @param AbstractPredicate $predicate
     *
     * @return AbstractDriver
     */
    public function where(Request $request, AbstractPredicate $predicate): AbstractDriver
    {
        return $this;
    }
}
