<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 18/02/2017
 * Time: 09:32
 */

namespace Actiane\ApiConnectorBundle\Lib;

use Cocur\Slugify\Slugify;

/**
 * Class AbstractFieldMapping
 *
 * @category   Todo-Todev
 * @package    Actiane\ApiConnectorBundle\Lib
 * @subpackage Actiane\ApiConnectorBundle\Lib
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
abstract class AbstractFieldMapping extends AbstractMapping
{
    /**
     * The key value in the target api. By default attribute's slug
     *
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $mType;

    /**
     * @var string
     */
    public $guid;

    /**
     * @var bool
     */
    public $readOnly = false;

    /**
     * @var bool
     */
    public $required = false;

    /**
     * @var string
     */
    public $doctrineAttribute;

    /**
     * @var string
     */
    protected $attributeName;

    /**
     * Do fillEntity
     *
     * @param object                 $entity
     * @param string|int|float|array $value
     *
     * @return AbstractFieldMapping
     */
    abstract public function fillEntity($entity, $value): self;

    /**
     * {@inheritdoc}
     * @throws \InvalidArgumentException
     */
    public function fillEntityMap(EntityMapping $mapping, string $attributeName): AbstractMapping
    {
        $this->attributeName = $attributeName;
        if (!$this->name) {
            $this->name = Slugify::create()->slugify($attributeName);
        }

        $mapping->addField($this);

        return $this;
    }

    /**
     * Get attributeName
     *
     * @return string
     */
    public function getAttributeName(): string
    {
        return $this->attributeName;
    }
}
