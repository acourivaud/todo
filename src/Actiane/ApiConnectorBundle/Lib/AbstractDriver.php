<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 17/02/2017
 * Time: 17:20
 */

namespace Actiane\ApiConnectorBundle\Lib;

/**
 * Class AbstractDriver
 *
 * @category   Todo-Todev
 * @package    Actiane\ApiConnectorBundle\Lib
 * @subpackage Actiane\ApiConnectorBundle\Lib
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
abstract class AbstractDriver
{
    /**
     * @var AbstractDriver
     */
    protected static $instance;

    /**
     * AbstractDriver constructor.
     */
    protected function __construct()
    {
    }

    /**
     * Do getInstance
     *
     * @return AbstractDriver
     */
    public static function getInstance(): AbstractDriver
    {
        if (!static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * Do select
     *
     * @param Request $request
     * @param array   $selects
     *
     * @return AbstractDriver
     */
    abstract public function select(Request $request, array $selects): self;

    /**
     * Do orderBy
     *
     * @param Request $request
     * @param array   $orderBy
     *
     * @return AbstractDriver
     */
    abstract public function orderBy(Request $request, array $orderBy): self;

    /**
     * Do limit
     *
     * @param Request $request
     * @param int     $limit
     *
     * @return AbstractDriver
     */
    abstract public function limit(Request $request, int $limit): self;

    /**
     * Do offset
     *
     * @param Request $request
     * @param int     $offset
     *
     * @return AbstractDriver
     */
    abstract public function offset(Request $request, int $offset): self;

    /**
     * Do where
     *
     * @param Request           $request
     * @param AbstractPredicate $predicate
     *
     * @return AbstractDriver
     */
    abstract public function where(Request $request, AbstractPredicate $predicate): self;

    /**
     * Do custom
     *
     * @param Request $request
     * @param array   $customQueries
     *
     * @return AbstractDriver
     */
    abstract public function custom(Request $request, array $customQueries): self;
}
