<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 18/02/2017
 * Time: 09:10
 */

namespace Actiane\ApiConnectorBundle\Lib;

/**
 * Class AbstractMapping
 *
 * @category   Todo-Todev
 * @package    Actiane\ApiConnectorBundle
 * @subpackage Actiane\ApiConnectorBundle\Lib
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
abstract class AbstractMapping
{
    /**
     * Do fillEntityMap
     *
     * @param EntityMapping $mapping
     * @param string        $attributeName
     *
     * @return AbstractMapping
     */
    abstract public function fillEntityMap(EntityMapping $mapping, string $attributeName): AbstractMapping;
}
