<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 22/03/17
 * Time: 11:08
 */

namespace Actiane\ApiConnectorBundle\Exceptions;

use Actiane\ToolsBundle\Exceptions\PublishedMessageExceptionInterface;

class OAuth2Exception extends \Exception implements PublishedMessageExceptionInterface
{
    /**
     * @return mixed
     */
    public function getMessages()
    {
        parent::getMessage();
    }

    /**
     * @return int
     */
    public function getCustomCode(): int
    {
        return 500;
    }
}
