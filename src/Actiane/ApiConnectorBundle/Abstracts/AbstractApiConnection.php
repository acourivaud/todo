<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 10/02/2017
 * Time: 18:08
 */

namespace Actiane\ApiConnectorBundle\Abstracts;

use Actiane\ApiConnectorBundle\Lib\AbstractDriver;
use Actiane\ApiConnectorBundle\Lib\AbstractMapping;
use Actiane\ApiConnectorBundle\Lib\EntityMapping;
use Actiane\ApiConnectorBundle\Lib\Request;
use Actiane\ToolsBundle\Traits\LoggerTrait;
use Doctrine\Common\Annotations\Reader;
use Monolog\Logger;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Todotoday\CoreBundle\Repository\AbstractEntityRepository;

/**
 * Class AbstractApiConnection
 *
 * @category   Todo-Todev
 * @package    Actiane\ApiConnectorBundle
 * @subpackage Actiane\ApiConnectorBundle\Abstracts
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
abstract class AbstractApiConnection
{
    use LoggerTrait;
    use ContainerAwareTrait;

    /**
     * @var AbstractDriver
     */
    protected $driver;

    /**
     * @var array
     */
    protected $options;

    /**
     * @var AbstractApiRepository[]
     */
    protected $repositories;

    /**
     * @var object[]
     */
    protected $updatedEntities;

    /**
     * @var object[]
     */
    protected $newEntities;

    /**
     * @var object[]
     */
    protected $deleteEntities;

    /**
     * @var EntityMapping[]
     */
    protected $mappingEntities;

    /**
     * @var Reader
     */
    protected $reader;

    /**
     * AbstractApiConnection constructor.
     *
     * @param array          $options
     * @param Logger         $logger
     * @param Reader         $reader
     * @param AbstractDriver $driver
     */
    public function __construct(array $options, Logger $logger, Reader $reader, AbstractDriver $driver)
    {
        $this->driver = $driver;
        $this->logger = $logger;
        $this->options = $options;
        $this->updatedEntities = array();
        $this->newEntities = array();
        $this->deleteEntities = array();
        $this->mappingEntities = array();
        $this->reader = $reader;
    }

    /**
     * Do addApiRepository
     *
     * @param AbstractApiRepository $apiRepository
     *
     * @return AbstractApiConnection
     * @throws \InvalidArgumentException
     */
    public function addApiRepository(AbstractApiRepository $apiRepository): self
    {
        $this->repositories[$apiRepository->getMapping()->getClass()] = $apiRepository;

        return $this;
    }

    /**
     * Do addEntityUpdate
     *
     * @param object $entity
     * @param string $attribute
     *
     * @return AbstractApiConnection
     */
    public function addEntityUpdate($entity, string $attribute): self
    {
        $id = spl_object_hash($entity);
        $entityChanged = array('entity' => $entity);
        if (array_key_exists($id, $this->updatedEntities)) {
            $entityChanged = $this->updatedEntities[$id];
        }
        $entityChanged['attributes'][] = $attribute;

        $this->updatedEntities[$id] = $entityChanged;

        return $this;
    }

    /**
     * Do connect
     *
     * @return AbstractApiToken
     */
    abstract public function connect(): AbstractApiToken;

    /**
     * Do delete
     *
     * @param object $entity
     *
     * @return AbstractApiConnection
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    public function delete($entity): self
    {
        if ($this->getRepository($entity)->getMapping()->hasIdentifiersValue($entity)) {
            $this->deleteEntities[] = $entity;
        }

        return $this;
    }

    /**
     * Do execute
     *
     * @param Request $request
     *
     * @return mixed
     */
    abstract public function execute(Request $request);

    /**
     * Do findOrCreateDoctrineEntityByEntity
     *
     * @param object $entity
     *
     * @return object the doctrine entity object
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     */
    public function findOrCreateDoctrineEntityByEntity($entity)
    {
        $mapping = $this->getMapping(get_class($entity));
        /** @var AbstractEntityRepository $repo */
        $repo = $this->container->get($mapping->getDoctrineEntityRepo());

        $qb = $repo->createQueryBuilder('entity');

        foreach ($mapping->getIdentifiersValue($entity) as $identifier => $value) {
            $field = $mapping->getField($identifier);
            $qb->andWhere('entity.' . $field->doctrineAttribute . ' = :attribute' . $identifier)
                ->setParameter('attribute' . $identifier, $value);
        }

        if ($entityDoctrine = $qb->getQuery()->getOneOrNullResult()) {
            return $entityDoctrine;
        }

        $class = $mapping->getDoctrineEntityClass();
        $entityDoctrine = new $class();

        $mapping->fillEntityInEntityDoctrine($entity, $entityDoctrine);

        return $entityDoctrine;
    }

    /**
     * Do flush
     *
     * @return AbstractApiConnection
     */
    abstract public function flush(): self;

    /**
     * Do formatIdentifierUrl
     *
     * @param array $identifiers
     *
     * @return string
     */
    abstract public function formatIdentifierUrl(array $identifiers): string;

    /**
     * Do getDriver
     *
     * @return AbstractDriver
     */
    public function getDriver(): AbstractDriver
    {
        return $this->driver;
    }

    /**
     * Do buildMapping
     *
     * @param string $entityClass
     *
     * @return EntityMapping
     */
    public function getMapping(string $entityClass): EntityMapping
    {
        if (!array_key_exists($entityClass, $this->mappingEntities)) {
            $reflectClass = new \ReflectionClass($entityClass);

            $mappingEntity = new EntityMapping($this);
            /** @var AbstractMapping[] $properties */
            $properties = $this->reader->getClassAnnotations($reflectClass);
            foreach ($properties as $property) {
                if ($property instanceof AbstractMapping) {
                    $property->fillEntityMap($mappingEntity, $reflectClass->getName());
                }
            }

            foreach ($reflectClass->getProperties() as $reflectProperty) {
                /** @var AbstractMapping[] $properties */
                $properties = $this->reader->getPropertyAnnotations($reflectProperty);
                foreach ($properties as $property) {
                    if ($property instanceof AbstractMapping) {
                        $property->fillEntityMap($mappingEntity, $reflectProperty->getName());
                    }
                }
            }

            $this->mappingEntities[$entityClass] = $mappingEntity;
        }

        return $this->mappingEntities[$entityClass];
    }

    /**
     * Do getRepository
     *
     * @param string|object $entity
     *
     * @return AbstractApiRepository
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    public function getRepository($entity): AbstractApiRepository
    {
        if (is_object($entity)) {
            $entity = get_class($entity);
        }

        $mapping = $this->getMapping($entity);
        /** @var AbstractApiRepository $repo */
        $repo = null;

        try {
            $repo = $this->container->get($mapping->getRepositoryId());
        } catch (ServiceNotFoundException $exception) {
            throw new \InvalidArgumentException(
                'Entity must be a valid instance entity or valid class name managed by this api connection'
            );
        }

        return $repo;
    }

    /**
     * Do persist
     *
     * @param object $entity
     *
     * @return AbstractApiConnection
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \InvalidArgumentException
     */
    public function persist($entity): self
    {
        if (!$this->getRepository($entity)->getMapping()->hasIdentifiersValue($entity)) {
            $this->newEntities[] = $entity;
        }

        return $this;
    }
}
