<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 10/02/2017
 * Time: 18:11
 */

namespace Actiane\ApiConnectorBundle\Abstracts;

use Actiane\ApiConnectorBundle\Lib\EntityMapping;
use Actiane\ApiConnectorBundle\Lib\QueryBuilder;
use Actiane\ApiConnectorBundle\Lib\Request;

/**
 * Class AbstractApiRepository
 *
 * @category   Todo-Todev
 * @package    Actiane\ApiConnectorBundle
 * @subpackage Actiane\ApiConnectorBundle\Abstracts
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
abstract class AbstractApiRepository
{
    /**
     * @var EntityMapping
     */
    protected $entityMapping;

    /**
     * @var string
     */
    private $entityClass;

    /**
     * @var AbstractApiConnection
     */
    private $apiConnection;

    /**
     * AbstractApiRepository constructor.
     *
     * @param string                $entityClass
     * @param AbstractApiConnection $apiConnection
     *
     * @throws \InvalidArgumentException
     */
    public function __construct(string $entityClass, AbstractApiConnection $apiConnection)
    {
        $this->entityClass = $entityClass;
        $this->apiConnection = $apiConnection;
        $apiConnection->addApiRepository($this);
    }

    /**
     * Do createQueryBuilder
     *
     * @return QueryBuilder
     * @throws \InvalidArgumentException
     */
    public function createQueryBuilder(): QueryBuilder
    {
        return new QueryBuilder($this->apiConnection, $this);
    }

    /**
     * Simple function to find with the main identifier the entity
     *
     * @param array $ids The main identifiers with key as the attribute and value as the value
     *
     * @return object
     * @throws \InvalidArgumentException
     */
    public function find(array $ids)
    {
        return $this->getRequestFind($ids)->execute(true);
    }

    /**
     * @param array  $ids
     * @param string $suffix
     *
     * @return array|object|\object[]
     * @throws \InvalidArgumentException
     */
    public function findWithSuffix(array $ids, string $suffix = 'blabla')
    {
        //TODO WIP mais ça risque de ne pas marcher =/
        $request = $this->getRequestFind($ids);
        $request->setUrlSuffix($suffix);

        return $request->execute();
    }

    /**
     * Do findAll
     *
     * @param int $limit
     * @param int $offset
     *
     * @return array|\object[]
     * @throws \InvalidArgumentException
     */
    public function findAll(int $limit = null, int $offset = null)
    {
        return $this
            ->createQueryBuilder()
            ->setLimit($limit)
            ->setOffset($offset)
            ->getRequest()
            ->execute();
    }

    /**
     * Get apiConnection
     *
     * @return AbstractApiConnection
     */
    public function getApiConnection(): AbstractApiConnection
    {
        return $this->apiConnection;
    }

    /**
     * Do buildMapping
     *
     * @return EntityMapping
     * @throws \InvalidArgumentException
     */
    public function getMapping(): EntityMapping
    {
        return $this->apiConnection->getMapping($this->entityClass);
    }

    /**
     * @param array $ids
     *
     * @return Request
     * @throws \InvalidArgumentException
     */
    private function getRequestFind(array $ids): Request
    {
        $request = $this->createQueryBuilder()->getRequest();

        $identifierUrl = $this->apiConnection->formatIdentifierUrl(
            $this->getMapping()->moveToIdentifiersReversedValue($ids)
        );
        $request->setUrlSuffix($request->getUrlSuffix() . $identifierUrl);

        return $request;
    }
}
