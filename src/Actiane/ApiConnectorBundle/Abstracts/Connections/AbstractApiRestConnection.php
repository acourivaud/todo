<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 10/02/2017
 * Time: 18:26
 */

namespace Actiane\ApiConnectorBundle\Abstracts\Connections;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiConnection;

/**
 * Class AbstractApiRestConnection
 *
 * @category   Todo-Todev
 * @package    Actiane\ApiConnectorBundle
 * @subpackage Actiane\ApiConnectorBundle\Abstracts\Connections
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
abstract class AbstractApiRestConnection extends AbstractApiConnection
{
//    abstract protected function delete();
//
//    abstract protected function get();
//
//    abstract protected function patch();
//
//    abstract protected function post();
//
//    abstract protected function put();
}
