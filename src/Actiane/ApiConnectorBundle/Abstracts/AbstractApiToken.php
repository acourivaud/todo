<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 10/02/2017
 * Time: 18:15
 */

namespace Actiane\ApiConnectorBundle\Abstracts;

/**
 * Class AbstractApiToken
 *
 * @category   Todo-Todev
 * @package    Actiane\ApiConnectorBundle
 * @subpackage Actiane\ApiConnectorBundle\Abstracts
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
/**
 * Class AbstractApiToken
 *
 * @package    Actiane\ApiConnectorBundle
 * @subpackage Actiane\ApiConnectorBundle\Abstracts
 */
abstract class AbstractApiToken
{
    /**
     * @var AbstractApiAuthenticator
     */
    protected $apiAuthenticator;

    /**
     * AbstractApiToken constructor.
     *
     * @param AbstractApiAuthenticator $apiAuthenticator
     */
    public function __construct(AbstractApiAuthenticator $apiAuthenticator)
    {
        $this->apiAuthenticator = $apiAuthenticator;
    }

    /**
     * Get apiAuthenticator
     *
     * @return AbstractApiAuthenticator
     */
    public function getApiAuthenticator(): AbstractApiAuthenticator
    {
        return $this->apiAuthenticator;
    }

    /**
     * Do isValid
     *
     * @return bool
     */
    abstract public function isValid(): bool;

    /**
     * Do addRequiredHeaders
     *
     * @param resource $ch
     *
     * @return AbstractApiToken
     */
    abstract public function addRequiredHeaders($ch): self;
}
