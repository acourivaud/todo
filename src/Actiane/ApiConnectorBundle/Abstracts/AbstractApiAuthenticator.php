<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 10/02/2017
 * Time: 18:13
 */

namespace Actiane\ApiConnectorBundle\Abstracts;

use Actiane\ToolsBundle\Traits\LoggerTrait;
use Monolog\Logger;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AbstractApiAuthenticator
 *
 * @category   Todo-Todev
 * @package    Actiane\ApiConnectorBundle
 * @subpackage Actiane\ApiConnectorBundle\Abstracts
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
abstract class AbstractApiAuthenticator
{
    use LoggerTrait;

    /**
     * @var string[]
     */
    protected $options;

    /**
     * AbstractApiAuthenticator constructor.
     *
     * @param array  $options
     *
     * @param Logger $logger
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     */
    public function __construct(array $options, Logger $logger)
    {
        $optionResolver = new OptionsResolver();
        $this->configureSettings($optionResolver);
        $this->options = $optionResolver->resolve($options);
        $this->logger = $logger;
    }

    /**
     * Create token
     *
     * @return AbstractApiToken
     */
    abstract public function createToken(): AbstractApiToken;

    /**
     * Destroy token
     *
     * @param AbstractApiToken $apiToken
     *
     * @return AbstractApiAuthenticator
     */
    abstract public function destroyToken(AbstractApiToken $apiToken): self;

    /**
     * Refresh token, delete and recreate a new token
     *
     * @param AbstractApiToken $apiToken
     *
     * @return AbstractApiToken
     */
    abstract public function refreshToken(AbstractApiToken $apiToken): AbstractApiToken;

    /**
     * Do configureSettings
     *
     * @param OptionsResolver $optionsResolver
     *
     * @return AbstractApiAuthenticator
     */
    abstract protected function configureSettings(OptionsResolver $optionsResolver): self;
}
