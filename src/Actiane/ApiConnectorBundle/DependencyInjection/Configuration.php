<?php declare(strict_types = 1);

namespace Actiane\ApiConnectorBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     * @throws \RuntimeException
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('actiane_api_connector');

        $rootNode->append($this->getConnections());

        return $treeBuilder;
    }

    /**
     * Do getConnections
     *
     * @return ArrayNodeDefinition|NodeDefinition
     * @throws \RuntimeException
     */
    protected function getConnections()
    {
        $treeBuilder = new TreeBuilder();
        $node = $treeBuilder->root('connections');

        /** @var ArrayNodeDefinition|NodeDefinition $connectionNode */
        $connectionNode = $node
            ->requiresAtLeastOneElement()
            ->useAttributeAsKey('name')
            ->prototype('array')
        ;

        $connectionNode
            ->children()
                ->scalarNode('class')->end()
                ->scalarNode('base_folder')->end()
                ->scalarNode('base_url')->end()
                ->booleanNode('cross_company')->defaultFalse()->end()
                ->booleanNode('full_logs')->defaultFalse()->end()
                ->arrayNode('authentication')
                    ->useAttributeAsKey('name')
                    ->prototype('scalar')->end()
                ->end()
            ->end()
        ;

        return $node;
    }
}
