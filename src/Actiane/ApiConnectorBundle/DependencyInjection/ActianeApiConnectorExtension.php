<?php declare(strict_types = 1);

namespace Actiane\ApiConnectorBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class ActianeApiConnectorExtension extends Extension
{
    /**
     * @var ContainerBuilder
     */
    protected $container;

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $this->container = $container;
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');

        $this->processConnections($config['connections']);
    }

    /**
     * Do processConnections
     *
     * @param array $connections
     *
     * @return ActianeApiConnectorExtension
     * @throws \Symfony\Component\DependencyInjection\Exception\InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     */
    public function processConnections(array $connections): self
    {
        $definitions = array();
        foreach ($connections as $key => $connection) {
            $definition = new Definition(
                $connection['class'],
                array(
                    $key,
                    $connection,
                    new Reference('annotation_reader'),
                )
            );
            $definition->setFactory(
                array(new Reference('actiane.api_connector.connection.manager'), 'constructApiConnection')
            );
            $definition->addMethodCall('setContainer', array(new Reference('service_container')));

            $definitions['actiane.api_connector.connection.' . $key] = $definition;
        }

        $this->container->addDefinitions($definitions);

        return $this;
    }
}
