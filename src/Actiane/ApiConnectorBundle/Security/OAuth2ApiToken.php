<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 10/02/2017
 * Time: 18:42
 */

namespace Actiane\ApiConnectorBundle\Security;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiAuthenticator;
use Actiane\ApiConnectorBundle\Abstracts\AbstractApiToken;

/**
 * Class OAuth2ApiToken
 *
 * @category   Todo-Todev
 * @package    Actiane\ApiConnectorBundle
 * @subpackage Actiane\ApiConnectorBundle\Security
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 * @SuppressWarnings(PHPMD)
 */
class OAuth2ApiToken extends AbstractApiToken
{
    /**
     * @var string
     */
    protected $type;

    /**
     * @var int
     */
    protected $expiresIn;

    /**
     * @var int
     */
    protected $extExpiresIn;

    /**
     * @var int
     */
    protected $expiresOn;

    /**
     * @var int
     */
    protected $notBefore;

    /**
     * @var string
     */
    protected $resource;

    /**
     * @var string
     */
    protected $accessToken;

    /**
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * @var string
     */
    protected $authHeader;

    /**
     * @var string
     */
    private $scope;

    /**
     * @var string
     */
    private $refreshToken;

    /**
     * OAuth2ApiToken constructor.
     *
     * @param AbstractApiAuthenticator $apiAuthenticator
     * @param string                   $type
     * @param string                   $scope
     * @param int                      $expiresIn
     * @param int                      $extExpiresIn
     * @param int                      $expiresOn
     * @param int                      $notBefore
     * @param string                   $resource
     * @param string                   $accessToken
     * @param string                   $refreshToken
     *
     */
    public function __construct(
        AbstractApiAuthenticator $apiAuthenticator,
        string $type,
        string $scope,
        int $expiresIn,
        int $extExpiresIn,
        int $expiresOn,
        int $notBefore,
        string $resource,
        string $accessToken,
        string $refreshToken
    ) {
        $this->createdAt = new \DateTime();
        parent::__construct($apiAuthenticator);
        $this->type = $type;
        $this->expiresIn = $expiresIn;
        $this->extExpiresIn = $extExpiresIn;
        $this->expiresOn = $expiresOn;
        $this->notBefore = $notBefore;
        $this->resource = $resource;
        $this->accessToken = $accessToken;
        $this->authHeader = 'Authorization: ' . $type . ' ' . $accessToken;
        $this->scope = $scope;
        $this->refreshToken = $refreshToken;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Get expiresIn
     *
     * @return int
     */
    public function getExpiresIn(): int
    {
        return $this->expiresIn;
    }

    /**
     * Get extExpiresIn
     *
     * @return int
     */
    public function getExtExpiresIn(): int
    {
        return $this->extExpiresIn;
    }

    /**
     * Get expiresOn
     *
     * @return int
     */
    public function getExpiresOn(): int
    {
        return $this->expiresOn;
    }

    /**
     * Get notBefore
     *
     * @return int
     */
    public function getNotBefore(): int
    {
        return $this->notBefore;
    }

    /**
     * Get resource
     *
     * @return string
     */
    public function getResource(): string
    {
        return $this->resource;
    }

    /**
     * @return string|null
     */
    public function getScope(): ?string
    {
        return $this->scope;
    }

    /**
     * @param string|null $scope
     *
     * @return OAuth2ApiToken
     */
    public function setScope(?string $scope): OAuth2ApiToken
    {
        $this->scope = $scope;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getRefreshToken(): ?string
    {
        return $this->refreshToken;
    }

    /**
     * @param string|null $refreshToken
     *
     * @return OAuth2ApiToken
     */
    public function setRefreshToken(?string $refreshToken): OAuth2ApiToken
    {
        $this->refreshToken = $refreshToken;

        return $this;
    }

    /**
     * Get accessToken
     *
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * Do isValid
     *
     * @return bool
     */
    public function isValid(): bool
    {
        // TODO: Implement isValid() method.

        return true;
    }

    /**
     * Do addRequiredHeaders
     *
     * @param resource $ch
     *
     * @return AbstractApiToken
     */
    public function addRequiredHeaders($ch): AbstractApiToken
    {
        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            array(
                $this->authHeader,
                'Accept: application/json;odata=minimalmetadata',
                'Content-Type: application/json;odata=minimalmetadata',
                'Prefer: return=representation',
            )
        );
//        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        return $this;
    }
}
