<?php declare(strict_types = 1);

namespace Actiane\ApiConnectorBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class ActianeApiConnectorBundle
 *
 * @package    Actiane\ApiConnectorBundle
 */
class ActianeApiConnectorBundle extends Bundle
{
}
