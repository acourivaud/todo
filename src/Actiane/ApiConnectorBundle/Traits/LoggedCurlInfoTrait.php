<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by PhpStorm.
 * User: nicolas
 * Date: 15/11/17
 * Time: 12:21
 *
 * @category   Todo-Todev
 *
 * @package    Actiane\ApiConnectorBundle\Traits
 *
 * @subpackage Actiane\ApiConnectorBundle\Traits
 *
 * @author     Nicolas Demay <nicolas.demay@actiane.com>
 */

namespace Actiane\ApiConnectorBundle\Traits;

/**
 * Trait LoggedCurlInfoTrait
 *
 * @package Actiane\ApiConnectorBundle\Traits
 */
trait LoggedCurlInfoTrait
{
    /**
     * @param array  $info
     * @param string $output
     */
    protected function loggedCurlInfo(array $info, string $output): void
    {
        $this->logger->info(
            sprintf(
                '%s Call to partner %s : %s',
                $this->loggerRef,
                $this->key,
                urldecode($info['url'])
            )
        );

        $this->logger->info(
            sprintf(
                'Request %s : elapsed time : %g seconds',
                $this->loggerRef,
                round($info['total_time'], 2)
            )
        );

        $this->logger->info(
            sprintf(
                'Request %s : size transfered : %g Ko',
                $this->loggerRef,
                round(
                    $info['size_download'] /
                    1024,
                    2
                )
            )
        );

        $this->logger->info(
            sprintf(
                'Request %s : speed download : %g Ko/s',
                $this->loggerRef,
                round(
                    $info['speed_download'] /
                    1024,
                    2
                )
            )
        );

        $this->logger->info(
            sprintf('%s Result : %s', $this->loggerRef, $output)
        );
    }
}
