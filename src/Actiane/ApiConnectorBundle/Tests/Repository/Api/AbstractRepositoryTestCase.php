<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 13/03/2017
 * Time: 16:30
 */

namespace Actiane\ApiConnectorBundle\Tests\Repository\Api;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiRepository;
use Actiane\UnitFonctionalTestBundle\Tests\WebTestCase;

/**
 * Class AbstractRepositoryTestCase
 *
 * @category   Todo-Todev
 * @package    Actiane\ApiConnectorBundle
 * @subpackage Actiane\ApiConnectorBundle\Tests\Repository\Api
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
abstract class AbstractRepositoryTestCase extends WebTestCase
{
    /**
     * {@inheritdoc}
     */
    public static function getFixtures(): array
    {
        return array();
    }

    /**
     * Do retailCatalogProvider
     *
     * @return array
     */
    public function entityProvider(): array
    {
        /** @var AbstractApiRepository $repo */
        $repo = $this->getContainer()->get($this->getRepositoryId());

        /** @var object[] $entities */
        $entities = $repo->findAll(5);
        $tmp = array();
        foreach ($entities as $entity) {
            $tmp[] = array($entity);
        }

        return $tmp;
    }

    /**
     * @dataProvider entityProvider
     * @group        ignore
     * @small
     *
     * @param object $entity
     */
    public function testFind($entity): void
    {
        /** @var AbstractApiRepository $repo */
        $repo = $this->getContainer()->get($this->getRepositoryId());

        $findArray = $repo->getMapping()->getIdentifiersValue($entity);

        $entity = $repo->find(
            $findArray
        );

        static::assertNotNull($entity);
        foreach ($findArray as $key => $value) {
            static::assertEquals($value, $entity->{'get' . ucfirst($key)}());
        }
    }

    /**
     * Do testFindAll
     *
     * @group ignore
     * @small
     */
    public function testFindAll(): void
    {
        /** @var AbstractApiRepository $repo */
        $repo = $this->getContainer()->get($this->getRepositoryId());

        /** @var object[] $entities */
        $entities = $repo->findAll(10);

        static::assertNotEmpty($entities);
    }

    /**
     * Do testHasIdentifier
     *
     * @dataProvider entityProvider
     *
     * @group        ignore
     *
     * @param object $entity
     *
     * @small
     * @return void
     */
    public function testHasIdentifier($entity): void
    {
        /** @var AbstractApiRepository $repo */
        $repo = $this->getContainer()->get($this->getRepositoryId());
        $map = $repo->getMapping();

        static::assertTrue($map->hasIdentifiersValue($entity));
    }

    /**
     * Do getRepositoryId
     *
     * @return string
     */
    abstract protected function getRepositoryId(): string;
}
