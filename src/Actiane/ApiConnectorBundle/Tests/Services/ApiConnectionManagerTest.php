<?php declare(strict_types=1);

/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 15/02/2017
 * Time: 15:01
 */

namespace Actiane\ApiConnectorBundle\Tests\Services;

use Actiane\ApiConnectorBundle\Security\OAuth2ApiToken;
use Actiane\ApiConnectorBundle\Services\Connections\Microsoft\MicrosoftDynamicsConnection;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use Symfony\Component\OptionsResolver\Exception\MissingOptionsException;

/**
 * Class ApiConnectionManagerTest
 *
 * @category   Todo-Todev
 * @package    Actiane\ApiConnectorBundle\Tests
 * @subpackage Actiane\ApiConnectorBundle\Tests\Services
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class ApiConnectionManagerTest extends WebTestCase
{
    /**
     * Do testGetConnectionFail
     *
     * @return void
     */
    public function testConstructApiConnectionFail()
    {
        $this->expectException(MissingOptionsException::class);
        $this->getContainer()->get('actiane.api_connector.connection.manager')->constructApiConnection(
            'test',
            array(),
            $this->getContainer()->get('annotation_reader')
        );
    }

    /**
     * Do testGetConnectionFail
     *
     * @large
     *
     * @return void
     */
    public function testConstructMicrosoftApiConnection()
    {
        $token = $this->getContainer()->get('actiane.api_connector.connection.manager')->constructApiConnection(
            'test',
            array(
                'class' => MicrosoftDynamicsConnection::class,
                'base_url' => 'https://todotoday-buildaos.sandbox.ax.dynamics.com/Data',
                'base_folder' => 'Entity/Api/Microsoft',
                'cross_company' => true,
                'authentication' => array(
                    'url_access_token' => 'https://login.microsoftonline.com/todotoday.onmicrosoft.com/oauth2/token',
                    'client_id' => '6ac58463-0769-47e3-9414-c2fdd822eee4',
                    'client_secret' => 'Efq+adZ2AVcgfXT48MA2XE6jYx2JG/rJJIgbc2nMxg4=',
                    'resource' => 'https://todotoday-buildaos.sandbox.ax.dynamics.com',
                    'username' => 'frontserviceaccount@todotoday.com',
                    'password' => '6O7isW?3&i',
                ),
            ),
            $this->getContainer()->get('annotation_reader')
        )->connect();

        static::assertInstanceOf(OAuth2ApiToken::class, $token);
    }

    /**
     * Do testGetConnectionFail
     *
     * @return void
     */
    public function testConstructMicrosoftApiConnectionFail()
    {
        $this->expectException(MissingOptionsException::class);
        $this->getContainer()->get('actiane.api_connector.connection.manager')->constructApiConnection(
            'test',
            array(
                'class' => MicrosoftDynamicsConnection::class,
                'base_url' => 'http://test.test',
                'base_folder' => 'Entity/Api/Microsoft',
                'authentication' => array(),
            ),
            $this->getContainer()->get('annotation_reader')
        )->connect();
    }

    /**
     * Do testGetConnectionFail
     *
     * @return void
     */
    public function testGetConnectionFail()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->getContainer()->get('actiane.api_connector.connection.manager')->getConnection('fail');
    }
}
