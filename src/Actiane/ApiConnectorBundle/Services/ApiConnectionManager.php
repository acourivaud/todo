<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 10/02/2017
 * Time: 18:02
 */

namespace Actiane\ApiConnectorBundle\Services;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiConnection;
use Actiane\ToolsBundle\Traits\LoggerTrait;
use Doctrine\Common\Annotations\Reader;
use Monolog\Logger;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ApiConnectionManager
 *
 * @category   Todo-Todev
 * @package    Actiane\ApiConnectorBundle
 * @subpackage Actiane\ApiConnectorBundle\Services
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class ApiConnectionManager
{
    use LoggerTrait;

    /**
     * @var AbstractApiConnection[]
     */
    protected $apiConnections;

    /**
     * @var OptionsResolver
     */
    protected $optionResolver;

    /**
     * ApiConnectionManager constructor.
     *
     * @param Logger $logger
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     */
    public function __construct(Logger $logger)
    {
        $this->apiConnections = array();
        $this->optionResolver = new OptionsResolver();
        $this->configureSettings($this->optionResolver);
        $this->logger = $logger;
    }

    /**
     * Do constructApiConnection
     *
     * @param string $key
     * @param array  $confApiConnection
     * @param Reader $reader
     *
     * @return AbstractApiConnection
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function constructApiConnection(
        string $key,
        array $confApiConnection,
        Reader $reader
    ): AbstractApiConnection {
        $options = $this->optionResolver->resolve($confApiConnection);

        $apiConnectionClass = $options['class'];
        /** @var AbstractApiConnection $apiConnection */
        $apiConnection = new $apiConnectionClass($options, $this->logger, $reader);

        $this->apiConnections[$key] = $apiConnection;

        return $apiConnection;
    }

    /**
     * Do getConnection
     *
     * @param string $key
     *
     * @return AbstractApiConnection|mixed
     * @throws \InvalidArgumentException
     */
    public function getConnection(string $key)
    {
        if (!array_key_exists($key, $this->apiConnections)) {
            $this->log('error', 'No connection found with key ' . $key, \InvalidArgumentException::class);
        }

        return $this->apiConnections[$key];
    }

    /**
     * Do configureSettings
     *
     * @param OptionsResolver $optionsResolver
     *
     * @return ApiConnectionManager
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    protected function configureSettings(OptionsResolver $optionsResolver): self
    {
        $optionsResolver
            ->setRequired(
                array(
                    'class',
                    'base_folder',
                    'base_url',
                    'authentication',
                    'cross_company',
                    'full_logs',
                )
            )
            ->setAllowedTypes('class', 'string')
            ->setAllowedTypes('base_folder', 'string')
            ->setAllowedTypes('base_url', 'string')
            ->setAllowedTypes('authentication', 'array')
            ->setAllowedTypes('cross_company', 'boolean')
            ->setAllowedTypes('full_logs', 'boolean');

        return $this;
    }
}
