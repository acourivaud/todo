<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 13/02/2017
 * Time: 09:11
 */

namespace Actiane\ApiConnectorBundle\Services\Connections\Microsoft;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiConnection;
use Actiane\ApiConnectorBundle\Abstracts\AbstractApiToken;
use Actiane\ApiConnectorBundle\Lib\AbstractFieldMapping;
use Actiane\ApiConnectorBundle\Lib\Drivers\Microsoft;
use Actiane\ApiConnectorBundle\Lib\EntityProxyTrait;
use Actiane\ApiConnectorBundle\Lib\Request;
use Actiane\ApiConnectorBundle\Security\OAuth2ApiToken;
use Actiane\ApiConnectorBundle\Services\Authentications\OAuth2ApiAuthentication;
use Actiane\ToolsBundle\Enum\LoggerEnum;
use Doctrine\Common\Annotations\Reader;
use Monolog\Logger;

/**
 * Class MicrosoftDynamicsConnection
 *
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @category   Todo-Todev
 * @package    Actiane\ApiConnectorBundle
 * @subpackage Actiane\ApiConnectorBundle\Services\Connections\Microsoft
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class MicrosoftDynamicsConnection extends AbstractApiConnection
{
    /**
     * @var OAuth2ApiAuthentication
     */
    protected $apiAuthentication;

    /**
     * @var OAuth2ApiToken
     */
    protected $apiToken;

    /**
     * @var string[]
     */
    protected $authenticationOptions;

    /**
     * @var string
     */
    protected $baseFolder;

    /**
     * @var string
     */
    protected $baseUrl;

    /**
     * @var bool
     */
    private $crossCompany;

    /**
     * @var bool
     */
    private $fullLogs;

    /**
     * @var string
     */
    private $lastHttpMethodUsed;

    /**
     * @var string
     */
    private $loggerRef;

    /**
     * MicrosoftDynamicsConnection constructor.
     *
     * @param array  $options
     * @param Logger $logger
     * @param Reader $reader
     *
     * @internal param AbstractApiAuthenticator $apiAuthenticator
     */
    public function __construct(array $options, Logger $logger, Reader $reader)
    {
        parent::__construct($options, $logger, $reader, Microsoft::getInstance());

        [
            'base_url' => $this->baseUrl,
            'base_folder' => $this->baseFolder,
            'authentication' => $this->authenticationOptions,
            'cross_company' => $this->crossCompany,
            'full_logs' => $this->fullLogs,
        ] = $options;
    }

    /**
     * {@inheritdoc}
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Psr\Log\InvalidArgumentException
     */
    public function connect(): AbstractApiToken
    {
        if (!$this->apiAuthentication) {
            $this->apiAuthentication = new OAuth2ApiAuthentication($this->authenticationOptions, $this->logger);
        }

        if (!$this->apiToken) {
            $this->apiToken = $this->apiAuthentication->createToken();
        } elseif (!$this->apiToken->isValid()) {
            $this->apiToken = $this->apiAuthentication->refreshToken($this->apiToken);
        }

        return $this->apiToken;
    }

    /**
     * {@inheritdoc}
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \LogicException
     */
    public function execute(Request $request)
    {
        $ch = $this->initCurl();
        curl_setopt($ch, CURLOPT_URL, $this->formatUrl($request));
        // setting a time out for every curl call
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        //        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);

        if (!$oData = $this->executeCurl($ch)) {
            return null;
        }

        if (array_key_exists('value', $oData)) {
            return $oData['value'];
        }

        return $oData;
    }

    /**
     * Do flush
     *
     * @return AbstractApiConnection
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \InvalidArgumentException
     */
    public function flush(): AbstractApiConnection
    {
        $this->flushNewEntities();
        usleep(100000);
        $this->flushUpdatedEntities();
        usleep(100000);
        $this->flushDeleteEntities();

        return $this;
    }

    /**
     * {@inheritdoc}
     * @throws \InvalidArgumentException
     */
    public function formatIdentifierUrl(array $identifiers): string
    {
        $url = '';

        foreach ($identifiers as $identifier => $value) {
            if ($url) {
                $url .= ',';
            }
            if ($value instanceof \DateTime) {
                $url .= $identifier . '=' . $value->format('Y-m-d\TH:i:s\Z');
            } elseif (is_array($value)) {
                if (isset($value['enum'])) {
                    $url .= $identifier . '=' . $value['enum'] . '\'' . $value['value'] . '\'';
                } elseif (isset($value['guid'])) {
                    $url .= $identifier . '=' . $value['value'];
                } else {
                    throw new \InvalidArgumentException('Api connector : The type value is unknown');
                }
            } elseif (is_int($value) || is_float($value)) {
                $url .= $identifier . '=' . $value;
            } else {
                $url .= $identifier . '=\'' . $value . '\'';
            }
        }
        $url = '(' . $url . ')';

        return rawurlencode($url);
    }

    /**
     * Do formatUrl
     *
     * @param Request $request
     *
     * @return string
     */
    public function formatUrl(Request $request): string
    {
        if ($this->crossCompany) {
            $request->addUrlParam('cross-company', 'true');
        }

        $url = $this->baseUrl . '/' . $request->getUrlSuffix();
        $i = 0;
        foreach ($request->getUrlParams() as $key => $value) {
            $separator = $i === 0 ? '?' : '&';
            $url .= $separator . $key . '=' . rawurlencode($value);
            $i++;
        }

        return $url;
    }

    /**
     * @return string
     */
    public function getBaseUrl(): ?string
    {
        return $this->baseUrl;
    }

    /**
     * Do entityToArrayFlush
     *
     * @param object $entity
     *
     * @return array
     * @throws \InvalidArgumentException
     */
    protected function entityToArrayNew($entity): array
    {
        $array = array();
        $entityMapping = $this->getMapping(get_class($entity));
        foreach ($entityMapping->getFields() as $field) {
            $value = $this->formatValue($field, $entity);
            if ((!$field->readOnly || $field->required) && null !== $value) {
                if ($field->guid && is_array($value)) {
                    $field->name = $field->guid . '@odata.bind';
                    $value = '/' . $value['target'] . '(' . $value['value'] . ')';
                }
                $array[$field->name] = $value;
            } elseif ($field->required) {
                throw new \InvalidArgumentException('ApiConnect : ' . $field->getAttributeName() . ' is required.');
            }
        }

        return $array;
    }

    /**
     * Do formatValue
     *
     * @param AbstractFieldMapping $field
     *
     * @param object               $entity
     *
     * @return mixed
     */
    protected function formatValue(AbstractFieldMapping $field, $entity)
    {
        if ('bool' === $field->type) {
            $value = $entity->{'is' . $field->getAttributeName()}();
        } elseif ('date' === $field->type) {
            $value = $entity->{'get' . $field->getAttributeName()}();
            /** @var \DateTime $value */
            $value = null === $value ? null : $value->format('Y-m-d');
        } elseif ('datetimez' === $field->type) {
            $value = $entity->{'get' . $field->getAttributeName()}();
            /** @var \DateTime $value */
            $value = null === $value ? null : $value->format('Y-m-d\TH:i:s\Z');
        } else {
            $value = $entity->{'get' . $field->getAttributeName()}();
        }

        return $value;
    }

    /**
     * Do entityToArrayFlush
     *
     * @param array $entityChanged
     *
     * @return array
     * @throws \InvalidArgumentException
     */
    protected function entityToArrayUpdated(array $entityChanged): array
    {
        $array = array();
        /** @var array $attributes */
        /** @var object $entity */
        ['entity' => $entity, 'attributes' => $attributes] = $entityChanged;
        $entityMapping = $this->getMapping(get_class($entity));
        foreach ($attributes as $attribute) {
            $field = $entityMapping->getField($attribute);
            if (!$field->readOnly) {
                if ($value = $this->formatValue($field, $entity)) {
                    if ($field->guid && is_array($value)) {
                        $field->name = $field->guid . '@odata.bind';
                        $value = '/' . $value['target'] . '(' . $value['value'] . ')';
                    }
                    $array[$field->name] = $value;
                } elseif ($field->required) {
                    throw new \InvalidArgumentException('ApiConnect : ' . $field->getAttributeName() . ' is required.');
                } else {
                    $array[$field->name] = '';
                }
            }
        }

        return $array;
    }

    /**
     * Do executeCurl
     *
     * @param resource $ch
     *
     * @return array
     * @throws \InvalidArgumentException
     */
    protected function executeCurl($ch): ?array
    {
        $this->log(LoggerEnum::INFO, $this->loggerRef . 'Send request to microsoft');

        $output = curl_exec($ch);
        $info = curl_getinfo($ch);

        $this->loggedCurlInfo($this->loggerRef, $info);

        if ($this->fullLogs) {
            $this->log(LoggerEnum::INFO, 'Request ' . $this->loggerRef . 'OUTPUT : ' . $output);
        }

        if ($output === false
            || ($output !== '' && intdiv($info['http_code'], 100) !== 2)
        ) {
            {
                $message = 'Curl return error ' . $info['http_code'] . ' for ' . $info['url'] . ' with message ' .
                    $output;
                $this->log(LoggerEnum::CRITICAL, $this->loggerRef . $message);
                throw new \InvalidArgumentException(
                    $message
                );
            }
        }
        curl_close($ch);

        return json_decode($output, true);
    }

    /**
     * Do flushDeleteEntities
     *
     * @return void
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \InvalidArgumentException
     */
    protected function flushDeleteEntities(): void
    {
        foreach ($this->deleteEntities as $deleteEntity) {
            $ch = $this->initCurl();

            $mapping = $this->getRepository($deleteEntity)->getMapping();

            $feedURL = $this->baseUrl . '/' . $mapping->getPath();
            $feedURL .= $this->formatIdentifierUrl($mapping->getIdentifiersReversedValue($deleteEntity));

            $feedURL = $this->addCrossCompany($feedURL);

            curl_setopt($ch, CURLOPT_URL, $feedURL);

            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
            $this->lastHttpMethodUsed = 'DELETE';
            $this->executeCurl($ch);
        }
        $this->deleteEntities = array();
    }

    /**
     * Do flushNewEntities
     *
     * @return void
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \InvalidArgumentException
     */
    protected function flushNewEntities(): void
    {
        /** @var EntityProxyTrait $newEntity */
        foreach ($this->newEntities as $newEntity) {
            $ch = $this->initCurl();
            $mapping = $this->getRepository($newEntity)->getMapping();

            $feedURL = $this->baseUrl . '/' . $mapping->getPath();

            $feedURL = $this->addCrossCompany($feedURL);

            curl_setopt($ch, CURLOPT_URL, $feedURL);

            curl_setopt($ch, CURLOPT_POST, 1);

            $data = json_encode($this->entityToArrayNew($newEntity));

            $this->lastHttpMethodUsed = 'POST';
            $this->log(LoggerEnum::INFO, 'Request ' . $this->loggerRef . $data);

            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

            if ($oData = $this->executeCurl($ch)) {
                $mapping->fillEntity($newEntity, $oData);
                $newEntity->setApiConnection($this);
            }
        }
        $this->newEntities = array();
    }

    /**
     * Do flushUpdatedEntities
     *
     * @return void
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Psr\Log\InvalidArgumentException
     * @throws \InvalidArgumentException
     */
    protected function flushUpdatedEntities(): void
    {
        foreach ($this->updatedEntities as $updatedEntity) {
            $ch = $this->initCurl();

            $mapping = $this->getRepository($updatedEntity['entity'])->getMapping();

            $feedURL = $this->baseUrl . '/' . $mapping->getPath();
            $feedURL .= $this->formatIdentifierUrl($mapping->getIdentifiersReversedValue($updatedEntity['entity']));
            $feedURL = $this->addCrossCompany($feedURL);

            curl_setopt($ch, CURLOPT_URL, $feedURL);

            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');

            $data = json_encode($this->entityToArrayUpdated($updatedEntity));

            $this->log(LoggerEnum::INFO, 'Request ' . $this->loggerRef . $data);

            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

            $this->lastHttpMethodUsed = 'PATCH';

            $this->executeCurl($ch);
        }
        $this->updatedEntities = array();
    }

    /**
     * Do initCurl
     *
     * @return resource
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Psr\Log\InvalidArgumentException
     */
    protected function initCurl()
    {
        $this->loggerRef = '(' . uniqid() . ') ';

        $apiToken = $this->connect();

        $ch = curl_init();

        $apiToken->addRequiredHeaders($ch);

        return $ch;
    }

    /**
     * @param string $url
     *
     * @return string
     */
    protected function addCrossCompany(string $url): string
    {
        if ($this->crossCompany) {
            $url .= '?cross-company=true';
        }

        return $url;
    }

    /**
     * @param string $loggerRef
     * @param array  $info
     *
     * @throws \Psr\Log\InvalidArgumentException
     */
    private function loggedCurlInfo(string $loggerRef, array $info): void
    {
        $this->log(
            LoggerEnum::INFO,
            $loggerRef . 'URL : ' . $this->lastHttpMethodUsed . '/ ' . urldecode(
                $info['url']
            )
        );
        $this->log(
            LoggerEnum::INFO,
            'Request ' . $loggerRef . ' elapsed time : ' . round($info['total_time'], 2) . ' seconds'
        );
        $this->log(
            LoggerEnum::INFO,
            'Request ' . $loggerRef . ' size transfered : ' . round($info['size_download'] / 1024, 2) . ' Ko'
        );
        $this->log(
            LoggerEnum::INFO,
            'Request ' . $loggerRef . ' speed download : ' . round(
                $info['speed_download'] /
                1024,
                2
            ) . ' Ko/s'
        );
    }
}
