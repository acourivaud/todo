<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 10/02/2017
 * Time: 18:39
 */

namespace Actiane\ApiConnectorBundle\Services\Authentications;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiAuthenticator;
use Actiane\ApiConnectorBundle\Abstracts\AbstractApiToken;
use Actiane\ApiConnectorBundle\Exceptions\OAuth2Exception;
use Actiane\ApiConnectorBundle\Security\OAuth2ApiToken;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class OAuth2ApiAuthentication
 *
 * @category   Todo-Todev
 * @package    Actiane\ApiConnectorBundle
 * @subpackage Actiane\ApiConnectorBundle\Services\Authentications
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class OAuth2ApiAuthentication extends AbstractApiAuthenticator
{
    /**
     * @var OAuth2ApiToken[]
     */
    protected $tokens;

    /**
     * Create token
     *
     * @return AbstractApiToken
     * @throws \Psr\Log\InvalidArgumentException
     */
    public function createToken(): AbstractApiToken
    {
        $options = $this->options;
        unset($options['url_access_token']);
//        $options['grant_type'] = 'client_credentials';
        $options['grant_type'] = 'password';

        return $this->requestToken($options);
    }

    /**
     * Destroy token
     *
     * @param AbstractApiToken $apiToken
     *
     * @return AbstractApiAuthenticator
     */
    public function destroyToken(AbstractApiToken $apiToken): AbstractApiAuthenticator
    {
        if ($key = array_search($apiToken, $this->tokens, true)) {
            unset($this->tokens[$key]);
        }

        return $this;
    }

    /**
     * Refresh token, delete and recreate a new token
     *
     * @param AbstractApiToken|OAuth2ApiToken $apiToken
     *
     * @return AbstractApiToken
     * @throws \Psr\Log\InvalidArgumentException
     */
    public function refreshToken(AbstractApiToken $apiToken): AbstractApiToken
    {
        if (!in_array($apiToken, $this->tokens, true)) {
            $this->log(
                'error',
                'This token is not manage by this authentication or has been already destroyed',
                \InvalidArgumentException::class
            );
        }

        $options = $this->options;
        unset($options['url_access_token']);
        $options['grant_type'] = 'refresh_token';
        $options['refresh_token'] = $apiToken->getAccessToken();
        $token = $this->requestToken($options);
        $this->destroyToken($apiToken);

        return $token;
    }

    /**
     * Do configureSettings
     *
     * @param OptionsResolver $optionsResolver
     *
     * @return AbstractApiAuthenticator
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    protected function configureSettings(OptionsResolver $optionsResolver): AbstractApiAuthenticator
    {
        $optionsResolver
            ->setRequired(
                array(
                    'url_access_token',
                    'client_id',
                    'client_secret',
                    'resource',
                    'username',
                    'password',
                )
            )
            ->setAllowedTypes('url_access_token', 'string')
            ->setAllowedTypes('client_id', 'string')
            ->setAllowedTypes('client_secret', 'string')
            ->setAllowedTypes('resource', 'string')
            ->setAllowedTypes('username', 'string')
            ->setAllowedTypes('password', 'string');

        return $this;
    }

    /**
     * Do formatRequestOptions
     *
     * @param array $options
     *
     * @return string
     */
    protected function formatRequestOptions(array $options): string
    {
        $str = '';

        foreach ($options as $key => $value) {
            if ($str) {
                $str .= '&';
            }

            $str .= $key . '=' . urlencode((string) $value);
        }

        return $str;
    }

    /**
     * Do requestToken
     *
     * @param array $options
     *
     * @return OAuth2ApiToken
     * @throws \Actiane\ApiConnectorBundle\Exceptions\OAuth2Exception
     * @throws \Psr\Log\InvalidArgumentException
     */
    protected function requestToken(array $options): OAuth2ApiToken
    {
        $stsUrl = $this->options['url_access_token'];
//        $options['scope'] = $options['resource'] . '/.default';
//        unset($options['resource']);
        $authenticationRequestBody = $this->formatRequestOptions($options);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $stsUrl);
        // setting a timeout for curl
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $authenticationRequestBody);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        if (!$output = curl_exec($ch)) {
            throw new OAuth2Exception(curl_error($ch));
        }

        $tokenOutput = json_decode($output, true);
        if (isset($tokenOutput['error'])) {
            $this->log('error', $tokenOutput['error_description'], \InvalidArgumentException::class);
        }

        $token = new OAuth2ApiToken(
            $this,
            $tokenOutput['token_type'],
            $tokenOutput['scope'],
            (int) $tokenOutput['expires_in'],
            (int) $tokenOutput['ext_expires_in'],
            (int) $tokenOutput['expires_on'],
            (int) $tokenOutput['not_before'],
            $tokenOutput['resource'],
            $tokenOutput['access_token'],
            $tokenOutput['refresh_token']
        );
        $this->tokens[] = $token;

        return $token;
    }
}
