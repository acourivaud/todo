<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 10/02/2017
 * Time: 18:39
 */

namespace Actiane\ApiConnectorBundle\Services\Authentications;

use Actiane\ApiConnectorBundle\Abstracts\AbstractApiAuthenticator;
use Actiane\ApiConnectorBundle\Abstracts\AbstractApiToken;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class JwtApiAuthentication
 *
 * @category   Todo-Todev
 * @package    Actiane\ApiConnectorBundle
 * @subpackage Actiane\ApiConnectorBundle\Services\Authentications
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class JwtApiAuthentication extends AbstractApiAuthenticator
{
    /**
     * Create token
     *
     * @return AbstractApiToken
     */
    public function createToken(): AbstractApiToken
    {
        // TODO: Implement createToken() method.
    }

    /**
     * Destroy token
     *
     * @param AbstractApiToken $apiToken
     *
     * @return AbstractApiAuthenticator
     */
    public function destroyToken(AbstractApiToken $apiToken): AbstractApiAuthenticator
    {
        // TODO: Implement destroyToken() method.
    }

    /**
     * Refresh token, delete and recreate a new token
     *
     * @param AbstractApiToken $apiToken
     *
     * @return AbstractApiToken
     */
    public function refreshToken(AbstractApiToken $apiToken): AbstractApiToken
    {
        // TODO: Implement refreshToken() method.
    }

    /**
     * Do configureSettings
     *
     * @param OptionsResolver $optionsResolver
     *
     * @return AbstractApiAuthenticator
     */
    protected function configureSettings(OptionsResolver $optionsResolver): AbstractApiAuthenticator
    {
        // TODO: Implement configureSettings() method.
    }
}
