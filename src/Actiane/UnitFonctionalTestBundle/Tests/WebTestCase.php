<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 04/10/2016
 * Time: 19:21
 *
 * @category   Actiane - UnitFonctionalTest
 * @package    CrowdPinion\AppBundle
 * @subpackage CrowdPinion\AppBundle\Test
 * @author     Alexandre Vinet <contact@alexandrevinet.fr>
 */

namespace Actiane\UnitFonctionalTestBundle\Tests;

use Doctrine\Common\DataFixtures\ReferenceRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\SchemaTool;
use Liip\FunctionalTestBundle\Test\WebTestCase as BaseWebTestCase;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class WebTestCase
 *
 * @category   Actiane - UnitFonctionalTest
 * @package    CrowdPinion\AppBundle
 * @subpackage CrowdPinion\AppBundle\Test
 * @author     Alexandre Vinet <contact@alexandrevinet.fr>
 */
abstract class WebTestCase extends BaseWebTestCase
{
    /**
     * @var array
     */
    private static $metaData;

    /**
     * @var ReferenceRepository
     */
    private static $fixturesReferenceRepository;

    /**
     * To know if setUp has to be run or not in the current class
     *
     * @var string
     */
    private static $isSetUp;

    /**
     * If true, execute setUp to init Database for each test
     *
     * @var bool
     */
    private $setUpForEachTestEnabled;

    /**
     * WebTestCase constructor.
     *
     * @param string|null $name
     * @param array       $data
     * @param string      $dataName
     */
    public function __construct(
        string $name = null,
        array $data = [],
        string $dataName = ''
    ) {
        parent::__construct($name, $data, $dataName);

        $this->setUpForEachTestEnabled = false;
    }

    /**
     * Abstract method to get all fixtures needed for the test
     *
     * @return array
     */
    abstract public static function getFixtures(): array;

    /**
     * Gets a service.
     *
     * @param string $id              The service identifier
     * @param int    $invalidBehavior The behavior when the service does not exist
     *
     * @return object The associated service
     *
     * @throws ServiceCircularReferenceException When a circular reference is detected
     * @throws ServiceNotFoundException          When the service is not defined
     *
     * @see Reference
     */
    public function get($id, $invalidBehavior = ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE)
    {
        return $this->getContainer()->get($id, $invalidBehavior);
    }

    /**
     * {@inheritdoc}
     */
    public function setUpBuild(): void
    {
        $doctrineCon = $this->getContainer()->get('doctrine.dbal.default_connection');

        $params = $doctrineCon->getParams();
        $dbName = $params['dbname'];
        unset($params['dbname'], $params['path'], $params['url']);
        $tmpConnection = DriverManager::getConnection($params);
        $tmpConnection->connect();
        if (!in_array($dbName, $tmpConnection->getSchemaManager()->listDatabases(), true)) {
            $tmpConnection->getSchemaManager()->createDatabase($dbName);
        }
        $tmpConnection->close();
        $doctrineCon->close();
        $doctrineCon->connect();
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->resetManager();
        if (!self::$metaData) {
            self::$metaData = $em->getMetadataFactory()->getAllMetadata();
        }
        $schemaTool = new SchemaTool($em);
        $schemaTool->dropDatabase();
        $callback = 'setUpDatabase' . ucfirst($doctrineCon->getDatabasePlatform()->getName());

        if (method_exists(static::class, $callback)) {
            static::$callback($doctrineCon);
        }

        if (self::$metaData) {
            $schemaTool->createSchema(self::$metaData);
        }

        if ($fixtures = static::getFixtures()) {
            $this->postFixtureSetup();
            self::$fixturesReferenceRepository = $this->loadFixtures($fixtures)->getReferenceRepository();
        }
    }

    /**
     * Get fixturesReferenceRepository who contain all fixtures references
     *
     * @return ReferenceRepository
     */
    protected static function getFRR(): ReferenceRepository
    {
        return self::$fixturesReferenceRepository;
    }

    /**
     * Do setUpDatabasePostgresql
     *
     * @param Connection $doctrineCon
     *
     * @return void
     */
    protected static function setUpDatabasePostgresql(Connection $doctrineCon): void
    {
        /** @var array|string[] $schemas */
        $schemas = $doctrineCon->query(
            '
SELECT schema_name
FROM information_schema.schemata 
WHERE schema_name NOT LIKE \'pg_%\' AND schema_name != \'public\' AND schema_name != \'information_schema\';'
        )->fetchAll()
        ;
        foreach ($schemas as $schema) {
            $doctrineCon->query('DROP SCHEMA ' . $schema['schema_name'] . ' CASCADE;');
        }
    }

    /**
     * Get Doctrine
     *
     * @return RegistryInterface
     */
    protected function getDoctrine(): RegistryInterface
    {
        return $this->getContainer()->get('doctrine');
    }

    /**
     * Gets a named entity manager.
     *
     * @param string $name The entity manager name (null for the default one)
     *
     * @return ObjectManager|EntityManagerInterface
     */
    protected function getEntityManager(string $name = null): EntityManagerInterface
    {
        return $this->getDoctrine()->getManager($name);
    }

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        parent::setUp();

        if ($this->setUpForEachTestEnabled || self::$isSetUp !== static::class) {
            $this->setUpBuild();
            $this->setUpAcl();
            self::$isSetUp = static::class;
        }
    }

    /**
     * Init ACL
     *
     * @return void
     */
    protected function setUpAcl(): void
    {
        $this->runCommand('actiane:api:init-acl');
        $this->runCommand('sonata:admin:setup-acl');
    }

    /**
     * Do tearDown
     */
    protected function tearDown(): void
    {
        $this->getContainer()->get('doctrine.orm.entity_manager')->getConnection()->close();

        parent::tearDown();
    }

    /**
     * @param UserInterface $user
     * @param string        $firewallName
     *
     * @return $this
     */
    protected function tokenAs(UserInterface $user, string $firewallName)
    {
        $token = new UsernamePasswordToken($user, null, $firewallName, $user->getRoles());
        $this->getContainer()->get('security.token_storage')->setToken($token);

        return $this;
    }
}
