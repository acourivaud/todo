<?php declare(strict_types=1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 14/06/2017
 * Time: 15:52
 */

return [
    '\PHPUnit_Framework_TestCase' => \PHPUnit\Framework\TestCase::class,
    '\PHPUnit_Framework_Constraint' => \PHPUnit\Framework\Constraint\Constraint::class,
];
