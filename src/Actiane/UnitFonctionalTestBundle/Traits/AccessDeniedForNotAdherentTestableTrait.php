<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 30/04/17
 * Time: 18:27
 */

namespace Actiane\UnitFonctionalTestBundle\Traits;

use Todotoday\AccountBundle\DataFixtures\ORM\LoadAccountData;

/**
 * Class AccessDeniedForNotAdherentTestableTrait
 * @package Actiane\UnitFonctionalTestBundle\Traits
 */
trait AccessDeniedForNotAdherentTestableTrait
{
    /**
     * @return array
     */
    public function notAdherentProvider(): array
    {
        $provider = array(
            'microsoft' => array('microsoft'),
            'noosphere' => array('noosphere'),
            'admin' => array('admin'),
            'root' => array('root'),
        );

        foreach (LoadAccountData::getConciergesSlugs() as $slug) {
            $provider[$slug] = array($slug);
        }

        return $provider;
    }

    /**
     * @param string      $slug
     * @param string      $firewall
     * @param string      $url
     * @param null|string $agency
     */
    public function accessDeniedForNotAdherent(
        string $slug,
        string $firewall,
        string $url,
        ?string $agency = null
    ): void {
        /** @var Account $account */
        $account = self::getFRR()->getReference($slug);
        $this->loginAs($account, $firewall);
        $client = $this->makeClient();
        $client->request('GET', $url, [], [], array('HTTP_AGENCY' => $agency));

        $this->assertStatusCode(403 || 302, $client);
    }
}
