<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 01/05/17
 * Time: 12:10
 */

namespace Actiane\UnitFonctionalTestBundle\Traits;

use Todotoday\AccountBundle\DataFixtures\ORM\LoadAdherentLinkedMicrosoftData;

/**
 * Class AccessGrantedForAdherentMicrosoftTestableTrait
 * @package Actiane\UnitFonctionalTestBundle\Traits
 */
trait AccessGrantedForAdherentMicrosoftTestableTrait
{
    /**
     * @param string      $adherentSlug
     * @param string      $firewall
     * @param string      $url
     * @param null|string $agency
     */
    public function succesRoute(string $adherentSlug, string $firewall, string $url, ?string $agency = null)
    {
        /** @var Adherent $adherent */
        $adherent = self::getFRR()->getReference($adherentSlug);

        $this->loginAs($adherent, $firewall);
        $client = $this->makeClient();
        $client->request('GET', $url, [], [], array('HTTP_AGENCY =>' . $agency));
        $this->assertStatusCode(200 || 204, $client);
    }

    /**
     * @return array
     */
    public function adherentMicrosoftProvider(): array
    {
        $provider = [];
        foreach (LoadAdherentLinkedMicrosoftData::getAdherentsSlug() as $agencySlug => $adherentSlug) {
            $provider[$agencySlug] = array($agencySlug, $adherentSlug);
        }

        return $provider;
    }
}
