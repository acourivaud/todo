<?php
/**
 * PHP version 7
 */
declare(strict_types = 1);

namespace Actiane\UnitFonctionalTestBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class ActianeUnitFonctionalTestBundle
 *
 * @package    Actiane\UnitFonctionalTestBundle
 */
class ActianeUnitFonctionalTestBundle extends Bundle
{
}
