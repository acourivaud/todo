<?php
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 12/01/2017
 * Time: 11:08
 *
 * @category   Todo-Todev
 * @package    Actiane\ApiDocBundle\Services
 * @subpackage Actiane\ApiDocBundle\Services
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
declare(strict_types = 1);

namespace Actiane\ApiDocBundle\Services;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Nelmio\ApiDocBundle\Extractor\ApiDocExtractor as BaseApiDocExtractor;
use Symfony\Component\Routing\Route;

/**
 * Class ApiDocExtractor
 *
 * @category   Todo-Todev
 * @package    Actiane\ApiDocBundle\Services
 * @subpackage Actiane\ApiDocBundle\Services
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class ApiDocExtractor extends BaseApiDocExtractor
{
    /**
     * Extracts annotations from all known routes
     *
     * @return array
     * @throws \Symfony\Component\DependencyInjection\Exception\InvalidArgumentException
     * @throws \InvalidArgumentException
     */
    public function allViews()
    {
        $routes = $this->getRoutes();
        $views = array();
        foreach ($routes as $route) {
            if (!$route instanceof Route) {
                throw new \InvalidArgumentException(
                    sprintf('All elements of $routes must be instances of Route. "%s" given', gettype($route))
                );
            }

            if ($method = $this->getReflectionMethod($route->getDefault('_controller'))) {
                /** @var ApiDoc $annotation */
                $annotation = $this->reader->getMethodAnnotation($method, static::ANNOTATION_CLASS);
                if ($annotation && $annotation->getViews()) {
                    foreach ($annotation->getViews() as $view) {
                        if (!in_array($view, $views, true) && $view !== ApiDoc::DEFAULT_VIEW) {
                            $views[] = $view;
                        }
                    }
                }
            }
        }

        return $views;
    }
}
