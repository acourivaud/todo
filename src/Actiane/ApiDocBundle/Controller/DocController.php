<?php
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 12/01/2017
 * Time: 09:12
 *
 * @category   Todo-Todev
 * @package    Actiane\ApiDocBundle\Controller
 * @subpackage Actiane\ApiDocBundle\Controller
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
declare(strict_types = 1);

namespace Actiane\ApiDocBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration as SFConf;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class DocController
 *
 * @category   Todo-Todev
 * @package    Actiane\ApiDocBundle\Controller
 * @subpackage Actiane\ApiDocBundle\Controller
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class DocController extends Controller
{
    /**
     * Do homeAction
     *
     * @SFConf\Route("/")
     * @SFConf\Template()
     *
     * @return array
     * @throws \InvalidArgumentException
     */
    public function homeAction(): array
    {
        $views = $this->get('actiane_api_doc.extractor')->allViews();

        return array('views' => $views);
    }
}
