<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 01/02/17
 * Time: 18:08
 */

namespace Actiane\ApiDocBundle\Tests;

use Liip\FunctionalTestBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Client;

/**
 * Class ApiDocTest
 *
 * @package Actiane\ApiDocBundle\Tests
 */
class ApiDocTest extends WebTestCase
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $baseUrl;

    /**
     * @return array
     */
    public function getAllLinks(): array
    {
        $url = $this->getUrl('actiane_apidoc_doc_home');
        $client = $this->makeClient();
        $crawler = $client->request('GET', $url);
        $links = [];
        if ($client->getResponse()->getStatusCode() === 200) {
            foreach ($crawler->filter('a')->links() as $link) {
                $links[$link->getNode()->textContent] = array($link->getUri());
            }
        }

        return $links;
    }

    /**
     * Do testHomepage
     *
     * @dataProvider getAllLinks
     *
     * Test if homepage for api.%domain%/doc works
     * @small
     *
     * @param $url
     *
     * @return void
     */

    public function testAllLinks($url): void
    {
        $this->client->request('GET', $url);
        $this->assertStatusCode('200', $this->client);
    }

    /**
     * Do testHomepage
     *
     * Test if homepage for api.%domain%/doc works
     *
     * @small
     *
     * @return void
     */
    public function testHomepage(): void
    {
        $this->client->request('GET', $this->baseUrl);
        $this->assertStatusCode('200', $this->client);
    }

    /**
     * setup method
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->client = $this->makeClient();
        $this->baseUrl = $this->getUrl('actiane_apidoc_doc_home');
    }
}
