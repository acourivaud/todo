<?php
/**
 * PHP version 7
 */
declare(strict_types = 1);

namespace Actiane\ApiDocBundle;

use Actiane\ApiDocBundle\DependencyInjection\AnnotationsProviderCompilerPass;
use Actiane\ApiDocBundle\DependencyInjection\ExtractorHandlerCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class ActianeApiDocBundle
 *
 * @package    Actiane\ApiDocBundle
 */
class ActianeApiDocBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new AnnotationsProviderCompilerPass());
        $container->addCompilerPass(new ExtractorHandlerCompilerPass());
    }
}
