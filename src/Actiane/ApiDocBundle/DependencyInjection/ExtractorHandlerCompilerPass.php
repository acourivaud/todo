<?php
/**
 * PHP version 7
 */
declare(strict_types = 1);

namespace Actiane\ApiDocBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Exception\OutOfBoundsException;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class ExtractorHandlerCompilerPass
 *
 * @package    Nelmio\ApiDocBundle
 * @subpackage Nelmio\ApiDocBundle\DependencyInjection
 */
class ExtractorHandlerCompilerPass implements CompilerPassInterface
{
    /**
     * {@inheritDoc}
     * @throws OutOfBoundsException
     * @throws ServiceNotFoundException
     */
    public function process(ContainerBuilder $container)
    {
        $handlers = array();
        $services = array_keys($container->findTaggedServiceIds('nelmio_api_doc.extractor.handler'));
        foreach ($services as $id) {
            $handlers[] = new Reference($id);
        }

        $container
            ->getDefinition('actiane_api_doc.extractor')
            ->replaceArgument(5, $handlers);
    }
}
