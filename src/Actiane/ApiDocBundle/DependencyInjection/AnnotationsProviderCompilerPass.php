<?php
/**
 * PHP version 7
 */
declare(strict_types = 1);

/*
 * This file is part of the NelmioApiDocBundle.
 *
 * (c) Nelmio <hello@nelm.io>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Actiane\ApiDocBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Exception\OutOfBoundsException;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Symfony\Component\DependencyInjection\Reference;

/**
 * AnnotationsProvider compiler pass.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
class AnnotationsProviderCompilerPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     * @throws ServiceNotFoundException
     * @throws OutOfBoundsException
     */
    public function process(ContainerBuilder $container)
    {
        $annotationsProviders = array();
        $services = array_keys($container->findTaggedServiceIds('nelmio_api_doc.extractor.annotations_provider'));
        foreach ($services as $id) {
            $annotationsProviders[] = new Reference($id);
        }

        $container
            ->getDefinition('actiane_api_doc.extractor')
            ->replaceArgument(6, $annotationsProviders);
    }
}
