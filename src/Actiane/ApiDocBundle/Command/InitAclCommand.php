<?php declare(strict_types = 1);
/**
 * PHP version 7
 *
 * Created by
 * User: alexj
 * Date: 03/02/2017
 * Time: 15:04
 */

namespace Actiane\ApiDocBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Acl\Domain\Entry;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\RoleSecurityIdentity;
use Symfony\Component\Security\Acl\Exception\AclNotFoundException;
use Symfony\Component\Security\Acl\Model\MutableAclInterface;
use Symfony\Component\Security\Acl\Model\SecurityIdentityInterface;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;

/**
 * Class InitAclCommand
 *
 * @category   Todo-Todev
 * @package    Actiane\ApiDocBundle\Command
 * @subpackage Actiane\ApiDocBundle\Command
 * @author     Alexandre Vinet <alexandre.vinet@actiane.com>
 */
class InitAclCommand extends ContainerAwareCommand
{
    /**
     * @var array
     */
    protected static $maskTable = array(
        'VIEW' => MaskBuilder::MASK_VIEW,
        'CREATE' => MaskBuilder::MASK_CREATE,
        'EDIT' => MaskBuilder::MASK_EDIT,
        'DELETE' => MaskBuilder::MASK_DELETE,
        'UNDELETE' => MaskBuilder::MASK_UNDELETE,
        'OPERATOR' => MaskBuilder::MASK_OPERATOR,
        'MASTER' => MaskBuilder::MASK_MASTER,
        'OWNER' => MaskBuilder::MASK_OWNER,
    );

    /**
     * @var OutputInterface
     */
    protected $output;

    /**
     * @var InputInterface
     */
    protected $input;

    /**
     * {@inheritdoc}
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function configure()
    {
        $this->setName('actiane:api:init-acl')
            ->setDescription('Init api acl by all controller api')
            ->addOption('dry-run', 'dr', InputOption::VALUE_NONE, 'option to test without consequences');
    }

    /**
     * {@inheritdoc}
     * @throws \InvalidArgumentException
     * @throws \LogicException
     * @throws \Symfony\Component\DependencyInjection\Exception\InvalidArgumentException
     * @throws \Symfony\Component\Security\Acl\Exception\AclAlreadyExistsException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\Security\Acl\Exception\AclNotFoundException
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $this->input = $input;

        $this->processAcl(
            '/Controller/Api',
            '/^([A-Z0-9-_]+)ApiController.php/i',
            'ApiController',
            '\\Controller\\Api\\'
        );
        $this->processAcl('/Entity', '/^([A-Z0-9-_]+).php/i', '', '\\Entity\\');
    }

    /**
     * Do processAcl
     *
     * @param string $dirTarget
     * @param string $patternTarget
     * @param string $suffix
     * @param string $namespace
     *
     * @return void
     * @throws \Symfony\Component\DependencyInjection\Exception\InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\Security\Acl\Exception\AclAlreadyExistsException
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function processAcl(string $dirTarget, string $patternTarget, string $suffix, string $namespace)
    {
        /** @var string[] $bundles */
        $bundles = $this->getContainer()->getParameter('kernel.bundles');
        foreach ($bundles as $bundle) {
            $reflection = new \ReflectionClass($bundle);
            $entityDirectory = dirname($reflection->getFileName()) . $dirTarget;
            if (file_exists($entityDirectory)) {
                $d = dir($entityDirectory);
                while (false !== ($entry = $d->read())) {
                    if (preg_match($patternTarget, $entry, $matchesEntity)) {
                        preg_match('#(.*)Bundle#', $reflection->getNamespaceName(), $matchesBundle);

                        $entity = $matchesEntity[1];
                        $roleNamespace = str_replace('\\', '_', strtoupper($matchesBundle[1]));
                        $roleEntity = strtoupper($entity);
                        $prefixRole = 'ROLE_' . $roleNamespace . '_API_' . $roleEntity . '_';

                        $aclProvider = $this->getContainer()->get('security.acl.provider');

                        $oid = new ObjectIdentity(
                            'class',
                            $reflection->getNamespaceName() . $namespace . $entity . $suffix
                        );
                        try {
                            $acl = $aclProvider->findAcl($oid);
                            $this->output->writeln('<info>[update] ACL </info> ' . $oid->getType());
                        } catch (AclNotFoundException $e) {
                            $acl = $aclProvider->createAcl($oid);
                            $this->output->writeln('<info>[create] ACL </info> ' . $oid->getType());
                        }

                        $this->output->writeln('<info>[update/insert]</info> ' . $prefixRole);
                        foreach (static::$maskTable as $role => $mask) {
                            $securityIdentity = new RoleSecurityIdentity($prefixRole . $role);
                            $status = 'update';
                            if (!$this->updateAce($acl, $securityIdentity, $mask)) {
                                $acl->insertClassAce($securityIdentity, $mask);
                                $status = 'insert';
                            }
                            $this->output->writeln('<info>[' . $status . ']</info> ' . $role);
                        }
                        if (!$this->input->getOption('dry-run')) {
                            $aclProvider->updateAcl($acl);
                        }
                    }
                }
                $d->close();
            }
        }
    }

    /**
     * Update Ace
     *
     * @param MutableAclInterface       $acl
     * @param SecurityIdentityInterface $securityIdentity
     * @param int                       $mask
     *
     * @return bool
     */
    protected function updateAce(MutableAclInterface $acl, SecurityIdentityInterface $securityIdentity, int $mask): bool
    {
        /** @var Entry $classAce */
        foreach ($acl->getClassAces() as $index => $classAce) {
            if ($classAce->getSecurityIdentity()->equals($securityIdentity)) {
                $acl->updateClassAce($index, $mask);

                return true;
            }
        }

        return false;
    }
}
